const rimraf = require('rimraf');

const clean = dir =>
  rimraf(dir, { disableGlob: true }, (error) => {
    if (error) {
      console.log(`Error: ${error}`);
    } else {
      console.log(`Directory Cleaned: ${dir}`);
    }
  });
/**
 * Clean build directory
 */
Promise.all([
  clean(require('./config/project.crc.config').paths.dist()),
  clean(require('./config/project.crcholiday17.config').paths.dist()),
  clean(require('./config/project.crcblog.config').paths.dist()),
  clean(require('./config/project.crcblog.config').paths.cssDist()),
  clean(require('./config/project.crctimeline.config').paths.dist()),
  clean(require('./config/project.crctimeline.config').paths.cssDist()),
]);
