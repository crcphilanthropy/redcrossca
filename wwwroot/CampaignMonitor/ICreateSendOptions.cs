﻿using System;

namespace CampaignMonitor
{
    public interface ICreateSendOptions
    {
        string BaseUri { get; set; }
        string BaseOAuthUri { get; set; }
        string VersionNumber { get; }
    }
}
