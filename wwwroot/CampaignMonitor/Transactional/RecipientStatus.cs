﻿using System;

namespace CampaignMonitor.Transactional
{
    public class RecipientStatus
    {
        public Guid MessageId { get; set; }
        public EmailAddress Recipient { get; set; }
        public string Status { get; set; }
    }
}
