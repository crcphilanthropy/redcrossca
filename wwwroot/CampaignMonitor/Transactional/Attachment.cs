﻿
namespace CampaignMonitor.Transactional
{
    public class Attachment
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
    }
}
