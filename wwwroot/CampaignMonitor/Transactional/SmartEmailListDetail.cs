﻿using System;

namespace CampaignMonitor.Transactional
{
    public class SmartEmailListDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string Status { get; set; }
    }
}
