﻿
namespace CampaignMonitor.Transactional
{
    public enum MessageListStatus
    {
        All,
        Delivered,
        Bounced,
        Spam,
    }
}
