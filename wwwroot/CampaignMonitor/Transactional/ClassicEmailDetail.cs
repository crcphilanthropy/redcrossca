﻿using System;

namespace CampaignMonitor.Transactional
{
    public class ClassicEmailDetail
    {
        public string Group { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }
}
