﻿
namespace CampaignMonitor.Transactional
{
    public enum SmartEmailListStatus
    {
        All,
        Active,
        Draft,
    }
}
