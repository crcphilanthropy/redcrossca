﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CampaignMonitor.Transactional
{
    public class AttachmentMetadata
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
