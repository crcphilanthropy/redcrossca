﻿
namespace CampaignMonitor.Transactional
{
    public class MessageBody
    {
        public string Html { get; set; }
        public string Text { get; set; }
    }
}
