﻿using System;

namespace CampaignMonitor
{
    public class CustomCreateSendOptions : ICreateSendOptions
    {
        public string BaseUri
        {
            get;
            set;
        }

        public string BaseOAuthUri
        {
            get;
            set;
        }

        public string VersionNumber
        {
            get { return CreateSendOptions.VersionNumber; }
        }
    }
}