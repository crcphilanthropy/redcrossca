﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CampaignMonitor
{
    public class BasicTemplate
    {
        public string TemplateID { get; set; }
        public string Name { get; set; }
        public string PreviewURL { get; set; }
        public string ScreenshotURL { get; set; }
    }
}