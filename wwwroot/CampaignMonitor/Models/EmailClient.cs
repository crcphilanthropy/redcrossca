﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CampaignMonitor
{
    public class EmailClient
    {
        public string Client { get; set; }
        public string Version { get; set; }
        public double Percentage { get; set; }
        public int Subscribers { get; set; }
    }
}
