﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CampaignMonitor
{
    public class OAuthTokenDetails
    {
        public string access_token;
        public int expires_in;
        public string refresh_token;
    }
}
