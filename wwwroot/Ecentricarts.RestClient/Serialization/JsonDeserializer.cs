﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecentricarts.RestClient.Serialization
{
    public class JsonDeserializer : IDeserializer
    {
        #region "Public properties"

        public CultureInfo Culture { get; set; }

        public string DateFormat { get; set; }

        public string Namespace { get; set; }

        public string RootElement { get; set; }

        #endregion

        #region "Public methods"

        public JsonDeserializer()
        {
            Culture = CultureInfo.InvariantCulture;
        }

        public T Deserialize<T>(IRestResponse response)
        {
            T result = default(T);

            if (!ReferenceEquals(response, null))
            {
                var settings = Json.DefaultSettings;

                settings.Culture = Culture;
                settings.DateFormatString = DateFormat;

                result = JsonConvert.DeserializeObject<T>(response.Content, settings);
            }

            return result;
        }

        #endregion
    }
}
