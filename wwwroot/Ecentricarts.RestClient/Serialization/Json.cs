﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ecentricarts.RestClient.Serialization
{
    public class Json
    {
        public static JsonSerializerSettings DefaultSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    // DefaultValueHandling = DefaultValueHandling.Ignore,
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
            }
        }
    }
}
