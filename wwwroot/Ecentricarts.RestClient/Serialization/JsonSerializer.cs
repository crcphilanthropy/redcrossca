﻿using Newtonsoft.Json;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecentricarts.RestClient.Serialization
{
    public class JsonSerializer : ISerializer
    {
        #region "Public properties"

        public string ContentType { get; set; }

        public string DateFormat { get; set; }

        public string Namespace { get; set; }

        public string RootElement { get; set; }

        #endregion

        #region "Public methods"

        public JsonSerializer()
        {
            ContentType = "application/json";
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, Json.DefaultSettings);
        }

        #endregion
    }
}
