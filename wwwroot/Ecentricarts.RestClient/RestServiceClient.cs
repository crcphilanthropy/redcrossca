﻿using System.Threading.Tasks;
using Ecentricarts.RestClient.Extensions;
using Ecentricarts.RestClient.Interface;
using Ecentricarts.RestClient.Serialization;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp.Authenticators;

namespace Ecentricarts.RestClient
{
    public class RestServiceClient : IRestServiceClient
    {
        #region "Private fields"

        private readonly RestSharp.RestClient _client;

        #endregion

        #region "Public methods"

        public RestServiceClient(
            string baseUrl,
            string authHeader = null)
        {
            _client = new RestSharp.RestClient()
            {
                BaseUrl = new Uri(baseUrl)
            };

            // Header parameters
            if (!String.IsNullOrEmpty(authHeader))
            {
                _client.ReplaceDefaultHeader("Authorization", authHeader);
            }

            _client.ReplaceDefaultHeader("Accept", "application/json");
            _client.ReplaceDefaultHeader("Content-Type", "application/json");

            _client.ReplaceHandler("application/json", new JsonDeserializer());
        }

        public RestRequest BuildGetRequest(
            string url,
            IDictionary<string, string> parameters = null)
        {
            return CreateRequest(Method.GET, url, parameters);
        }

        public RestRequest BuildPostRequest(
            string url,
            IDictionary<string, string> parameters = null,
            string data = null)
        {
            return CreateRequest(Method.POST, url, parameters, data);
        }

        public RestRequest BuildPutRequest(
            string url,
            IDictionary<string, string> parameters = null,
            string data = null)
        {
            return CreateRequest(Method.PUT, url, parameters, data);
        }

        public RestRequest BuildDeleteRequest(
            string url,
            IDictionary<string, string> parameters = null)
        {
            return CreateRequest(Method.DELETE, url, parameters);
        }

        public T SendRequest<T>(RestRequest request)
            where T : new()
        {
            var response = _client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }

            return response.Data;
        }

        public Task<T> SendRequesAsync<T>(RestRequest request) where T : new()
        {
            var taskCompletionSource = new TaskCompletionSource<T>();

            _client.ExecuteAsync<T>(request, (response) => taskCompletionSource.SetResult(response.Data));

            return taskCompletionSource.Task;
        }

        #endregion

        #region "Private methods"

        private RestRequest CreateRequest(
            Method method,
            string url,
            IDictionary<string, string> parameters = null,
            string data = null)
        {
            var request = new RestRequest()
            {
                Method = method,
                Resource = url,
                RequestFormat = DataFormat.Json,
                JsonSerializer = new JsonSerializer()
            };

            if (!ReferenceEquals(parameters, null) && parameters.Any())
            {
                foreach (var parameter in parameters)
                {
                    request.AddParameter(parameter.Key, parameter.Value);
                }
            }

            if (!String.IsNullOrWhiteSpace(data))
            {
                request.AddParameter("application/json", data, ParameterType.RequestBody);
            }

            return request;
        }

        #endregion
    }
}
