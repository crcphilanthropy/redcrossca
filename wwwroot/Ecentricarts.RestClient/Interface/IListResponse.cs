﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecentricarts.RestClient.Interface
{
    public interface IListResponse<T>
        where T : class
    {
        IList<T> Items { get; set; }

        T GetBestMatch();
    }
}
