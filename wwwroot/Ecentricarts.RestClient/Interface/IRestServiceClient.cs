﻿using RestSharp;
using System.Collections.Generic;

namespace Ecentricarts.RestClient.Interface
{
    public interface IRestServiceClient
    {
        RestRequest BuildGetRequest(string url, IDictionary<string, string> parameters);
        RestRequest BuildPostRequest(string url, IDictionary<string, string> parameters, string data);
        RestRequest BuildPutRequest(string url, IDictionary<string, string> parameters, string data);
        RestRequest BuildDeleteRequest(string url, IDictionary<string, string> parameters);

        T SendRequest<T>(RestRequest request) where T : new();
    }
}
