﻿using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecentricarts.RestClient.Extensions
{
    public static class RestClientExtensions
    {
        public static void ReplaceDefaultHeader(this IRestClient restClient, string name, string value)
        {
            restClient.RemoveDefaultParameter(name);

            restClient.AddDefaultParameter(name, value, ParameterType.HttpHeader);
        }

        public static void ReplaceHandler(this IRestClient restClient, string contentType, IDeserializer deserializer)
        {
            restClient.RemoveHandler(contentType);

            restClient.AddHandler(contentType, deserializer);
        }
    }
}
