﻿using System;
using System.Linq;
using CampaignMonitor;
using CRC.EmailClient.Interfaces;

namespace CRC.EmailClient.Clients
{
    public class CampaignMonitorEmailClient : BaseEmailClient, IEmailClient<string>
    {
        public CampaignMonitorEmailClient(IEmailClientConfig config) : base(config)
        {
        }

        public bool Subscribe(IEmailContact<string> contact)
        {
            try
            {
                var subscriber = new Subscriber(new ApiKeyAuthenticationDetails(EmailClientConfig.ApiKey), contact.ListKey);
                subscriber.Add(contact.Email, string.IsNullOrWhiteSpace(contact.FullName) ? contact.Email : contact.FullName, contact.CustomFields.Select(
                            item =>
                            new SubscriberCustomField
                            {
                                Key = item.Key,
                                Value = item.Value
                            }).ToList(), true);
                return true;
            }
            catch (Exception ex)
            {
                LogError(this, ex);
            }

            return false;
        }

        public bool AlreadySubscribe(IEmailContact<string> contact)
        {
            try
            {
                var subscriber = new Subscriber(new ApiKeyAuthenticationDetails(EmailClientConfig.ApiKey),
                    contact.ListKey);
                var subscriberDetail = subscriber.Get(contact.Email);
                return subscriberDetail != null &&
                       subscriberDetail.State.Equals("Active", StringComparison.OrdinalIgnoreCase);
            }
            catch (CreatesendException ex)
            {
                if(ex.Error.Code != "203")
                    LogError(this, ex);
            }
            catch (Exception ex)
            {
                LogError(this, ex);
            }

            return false;
        }
    }
}
