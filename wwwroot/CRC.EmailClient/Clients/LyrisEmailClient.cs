﻿using System;
using System.Linq;
using System.Net;
using CRC.EmailClient.Interfaces;
using LyrisProxy;

namespace CRC.EmailClient.Clients
{
    public class LyrisEmailClient : BaseEmailClient, IEmailClient<string>
    {
        public LyrisEmailClient(IEmailClientConfig config)
            : base(config)
        {
        }

        public bool Subscribe(IEmailContact<string> contact)
        {
            try
            {
                using (var client = new lmapi { Url = EmailClientConfig.ServiceUrl, Credentials = new NetworkCredential(EmailClientConfig.UserName, EmailClientConfig.Password) })
                {
                    var memberId = client.CreateSingleMember(contact.Email, string.IsNullOrWhiteSpace(contact.FullName) ? contact.Email : contact.FullName, contact.ListKey);
                    if (memberId != 0)
                    {
                        client.UpdateMemberStatus(
                            new SimpleMemberStruct
                                {
                                    EmailAddress = contact.Email,
                                    ListName = contact.ListKey,
                                    MemberID = memberId
                                }, MemberStatusEnum.needshello);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(this, ex);
            }
            return false;
        }

        public bool AlreadySubscribe(IEmailContact<string> contact)
        {
            try
            {
                using (var client = new lmapi { Url = EmailClientConfig.ServiceUrl, Credentials = new NetworkCredential(EmailClientConfig.UserName, EmailClientConfig.Password) })
                {
                    var userLists = client.EmailOnWhatLists(contact.Email) ?? new string[0];
                    return userLists.Select(item => item.ToLower()).Contains(contact.ListKey.ToLower());
                }
            }
            catch (Exception ex)
            {
                LogError(this, ex);
                return false;
            }
        }
    }
}
