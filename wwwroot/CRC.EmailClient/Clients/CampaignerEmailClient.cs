﻿using System;
using System.Collections.Generic;
using CRC.EmailClient.Campaigner.ContactManagement;
using CRC.EmailClient.Interfaces;
using System.Linq;

namespace CRC.EmailClient.Clients
{
    public class CampaignerEmailClient : BaseEmailClient, IEmailClient<List<int>>
    {
        public CampaignerEmailClient(IEmailClientConfig config)
            : base(config)
        {
        }

        public bool Subscribe(IEmailContact<List<int>> contact)
        {
            try
            {
                
                var contactData = new ContactData
                    {
                        ContactKey = new ContactKey { ContactUniqueIdentifier = contact.Email },
                        EmailAddress = new NullableElement { IsNull = false, Value = contact.Email },
                        FirstName = new NullableElement { IsNull = string.IsNullOrWhiteSpace(contact.FirstName ?? contact.FullName), Value = contact.FirstName ?? contact.FullName },
                        LastName = new NullableElement { IsNull = string.IsNullOrWhiteSpace(contact.LastName), Value = contact.LastName },
                        AddToGroup = contact.ListKey.ToArray(),
                        CustomAttributes = contact.CustomFields.Select(
                            item =>
                            new CustomAttribute
                                {
                                    Id = ConvertToInt(item.Key),
                                    IsNull = string.IsNullOrWhiteSpace(item.Value),
                                    Value = item.Value
                                }).ToArray()
                    };


                using (var client = new ContactManagementSoapClient())
                {
                    client.Endpoint.Address = new System.ServiceModel.EndpointAddress(EmailClientConfig.ServiceUrl);
                    UploadResultData[] resultData;
                    client.ImmediateUpload(new Authentication { Username = EmailClientConfig.UserName, Password = EmailClientConfig.Password }, true, true, new[] { contactData }, null, null, out resultData);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(this, ex);
            }
            return false;
        }

        private static int ConvertToInt(string itemKey)
        {
            int toReturn;
            int.TryParse(itemKey, out toReturn);
            return toReturn;
        }

        public bool AlreadySubscribe(IEmailContact<List<int>> contact)
        {
            try
            {
                using (var client = new ContactManagementSoapClient())
                {
                    client.Endpoint.Address = new System.ServiceModel.EndpointAddress(EmailClientConfig.ServiceUrl);

                    ContactsData resultData;
                    client.GetContacts(
                        new Authentication { Username = EmailClientConfig.UserName, Password = EmailClientConfig.Password },
                        new ContactsDataFilter
                            {
                                ContactKeys = new[] { new ContactKey() { ContactUniqueIdentifier = contact.Email } }
                            },
                        new contactInformationFilter
                            {
                                IncludeGroupMembershipData = true
                            },
                        out resultData);

                    if (resultData.ContactData == null) return false;

                    if (contact.ListKey.All(item => resultData.ContactData.Any(contactData => contactData.GroupMembershipData.Any(mailList => mailList.Id == item))))
                        return true;

                }
            }
            catch (Exception ex)
            {
                LogError(this, ex);
            }
            return false;
        }
    }
}
