﻿using System;
using CRC.EmailClient.Interfaces;

namespace CRC.EmailClient.Clients
{
    public abstract class BaseEmailClient
    {
        internal readonly IEmailClientConfig EmailClientConfig;
        public event EventHandler<UnhandledExceptionEventArgs> Error;

        protected BaseEmailClient(IEmailClientConfig config)
        {
            EmailClientConfig = config;
        }

        internal void LogError(object sender, Exception ex)
        {
            if (Error != null)
                Error(sender, new UnhandledExceptionEventArgs(ex,false));
        }
    }
}
