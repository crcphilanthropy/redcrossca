﻿using System.Security;

namespace CRC.EmailClient.Interfaces
{
    public interface IEmailClientConfig
    {
        string ServiceUrl { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string ApiKey { get; set; }
    }
}
