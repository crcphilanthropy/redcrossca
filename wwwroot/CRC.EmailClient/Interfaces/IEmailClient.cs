﻿using System;

namespace CRC.EmailClient.Interfaces
{
    public interface IEmailClient<T>
    {
        bool Subscribe(IEmailContact<T> contact);
        bool AlreadySubscribe(IEmailContact<T> contact);
        event EventHandler<UnhandledExceptionEventArgs> Error;
    }
}
