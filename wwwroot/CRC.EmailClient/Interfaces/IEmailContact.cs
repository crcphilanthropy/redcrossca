﻿using System.Collections.Generic;
using CRC.EmailClient.Models;

namespace CRC.EmailClient.Interfaces
{
    public interface IEmailContact<T>
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string FullName { get; set; }
        string Email { get; set; }
        T ListKey { get; set; }
        bool LearnMore { get; set; }
        string List { get; }
        List<EmailContactCustomField> CustomFields { get; set; }
    }
}
