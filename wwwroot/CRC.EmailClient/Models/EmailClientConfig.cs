﻿using System.Security;
using CRC.EmailClient.Interfaces;

namespace CRC.EmailClient.Models
{
    public class EmailClientConfig : IEmailClientConfig
    {
        public string ServiceUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
    }

    public class EmailContactCustomField
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
