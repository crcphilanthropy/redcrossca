﻿using CMS.EventLog;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly IPhoto _photo;
        private readonly ICatalogService _catalog;
        private readonly IForm _form;

        public PhotoService()
        {
            _photo = new PhotoRepository();
            _catalog = new CatalogService();
            _form = new FormRepository();
        }

        public IEnumerable<Photo> SearchPhotos(string categories, string regions, string culture)
        {
            var searchresults = new List<Photo>();
            try
            {
                if (string.IsNullOrEmpty(categories) && string.IsNullOrEmpty(regions))
                {
                    //var searchresults = _cache.CacheAllPhotos();
                    searchresults = _photo.GetAll(culture).ToList();
                    if (searchresults != null)
                    {
                        foreach (var item in searchresults)
                        {
                            _catalog.CleanPhotoImageData(item, true);
                        }
                    }
                    return searchresults;
                }
                else
                {
                    //Do cleaning/get ID's of category and region string
                    var categoryIds = _catalog.CleanCategoryUri(categories);
                    var regionIds = _catalog.CleanCategoryUri(regions);
                    //var searchresults = _cache.CachePhotosSearch(categories, regions);
                    searchresults = _photo.PhotoQuery(categoryIds, regionIds, culture).ToList();
                        if (searchresults != null)
                        {
                            foreach (var item in searchresults)
                            {
                                _catalog.CleanPhotoImageData(item, true);
                            }
                        }
                        return searchresults;
                }

            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("PhotoService", "SearchPhotos", ex, 0 , "A given argument is invalid");
                else
                    EventLogProvider.LogException("PhotoService", "SearchPhotos", ex);
                return searchresults;
            }
        }

        public IEnumerable<Photo> GetAllPhotos(string culture)
        {
            var result = new List<Photo>();
            try
            {
                result = _photo.GetAll(culture).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        _catalog.CleanPhotoImageData(item);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("PhotoService", "GetAllPhotos", ex, 0, ex.Message);
                return result;
            }

        }

        public Photo GetById(int id, string culture)
        {
            if (id > 0)
            {
                try
                {
                    //var result = _cache.CachePhoto(id);
                    var result = _photo.Get(id, culture);
                    if (result != null)
                    {
                        result = _catalog.CleanPhotoImageData(result);
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("PhotoService", "GetById", ex, 0, "Given Id argument is invalid");
                    else
                        EventLogProvider.LogException("PhotoService", "GetById", ex, 0, ex.Message);
                    return null;
                }
            }
            else
            {
                return null;
            }
            
        }

        public Form SaveForm(Form data)
        {
            try
            {
                var result = _form.SavePhotoForm(data);
                //result.Email = HttpUtility.HtmlEncode(result.Email);
                //result.Name = HttpUtility.HtmlEncode(result.Name);
                //result.Publication = HttpUtility.HtmlEncode(result.Publication);
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("PhotoService", "SaveForm", ex, 0, "Given form data is invalid");
                else if(ex is ArgumentNullException)
                    EventLogProvider.LogException("PhotoService", "SaveForm", ex, 0, "Form data is null");
                else if (ex is NullReferenceException)
                    EventLogProvider.LogException("PhotoService", "SaveForm", ex, 0, "Cannot derefrence null object when trying to save form data");
                else
                    EventLogProvider.LogException("PhotoService", "SaveForm", ex, 0, ex.Message);
                return null;
            }
        }

    }
}