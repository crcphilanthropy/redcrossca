﻿using CMS.DataEngine;
using CMS.EventLog;
using CMS.SiteProvider;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;

namespace MultiMediaApi.Services
{
    public class TrackingCodeService : ITrackingCodeService
    {
        private readonly IBanner _banner;
        private readonly Random _random;
        private readonly string _trackingCodes;
        private readonly ICatalogService _catalog;

        public TrackingCodeService()
        {
            _banner = new BannerRepository();
            _random = new Random();
            _trackingCodes = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CRCSettings.TrackingCode");
            _catalog = new CatalogService();
        }

        public string CreateTrackingCode(string companyname, Banner banner, string trackingurl, string campaign_name)
        {
            var code = string.Empty;
            //check if string is empty
            if (string.IsNullOrEmpty(companyname))
            {
                EventLogProvider.LogException("TrackingCodeService", "CreateTrackingCode", null, 0, "Given company name is invalid");
                return null;
            }
            //Check if banner id is < 0
            if (banner.BannerID < 0)
            {
                EventLogProvider.LogException("TrackingCodeService", "CreateTrackingCode", null, 0, "Given banner is invalid");
                return null;
            }
            try
            {
                companyname = this.cleanTrackingUrlInputs(companyname);
                campaign_name = this.cleanTrackingUrlInputs(campaign_name);
                //Get banner by id and check if banner already has embed link
                if (banner != null)
                {
                    if (string.IsNullOrEmpty(banner.TrackingCode))
                    {
                        //Generate tracking code for banner from company name/ according to provided format - check if implementation exists already
                        var trackingcode = string.Format(_trackingCodes, companyname, banner.BannerType);
                        code = HttpUtility.HtmlEncode(trackingurl + trackingcode + campaign_name);
                    }
                    else
                    {
                        code = HttpUtility.HtmlEncode(banner.TrackingCode);
                    }
                }
                return code;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("TrackingCodeService", "CreateTrackingCode", ex, 0, "Given banner ID is invalid");
                else
                    EventLogProvider.LogException("TrackingCodeService", "CreateTrackingCode", ex, 0, ex.Message);

                return null;
            }
        }

        private string TrackingCodeGenerator(int length)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException("length");

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        private string cleanTrackingUrlInputs(string input)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(input))
            {
                var temp = _catalog.CleanStringNormal(input);
                result = Regex.Replace(temp, @"(\s+)", "_", RegexOptions.None);
                return result;
            }
            else
                return result;
        }
    }
}