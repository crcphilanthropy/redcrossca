﻿using CMS.DocumentEngine;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface IVideoService
    {
        IEnumerable<Video> SearchVideos(string categories, string regions, string path, string culture);
        IEnumerable<Video> GetAllVideos(string path, string culture);
        Video GetById(int id, string culture);
        Video GetByGuid(Guid id);
        IEnumerable<Video> GetRelatedVideos(Guid id, List<int> categories, List<int> regions, string path, DateTime? date, string culture);
        List<Video> GetDetailsRelatedVideos(Guid id, List<int> catids, List<int> regids, DateTime created, TreeNode CurrentDocument, string parentPath, string culture);
        Form SaveForm(Form data);
    }
}
