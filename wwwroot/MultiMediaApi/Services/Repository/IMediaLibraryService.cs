﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface IMediaLibraryService
    {
        FileResult GetMedia(string path, string code);
    }
}
