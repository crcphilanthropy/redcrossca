﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface ITrackingCodeService
    {
        string CreateTrackingCode(string name, Banner banner, string trackingurl, string campaign_name);
    }
}
