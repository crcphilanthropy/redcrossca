﻿using CRCWebBanners;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface IBannerService
    {
        void CreateBanner(Banner b);
        void CreateBannerSet(BannerSet b);

        Banner GetBannerById(int Id, string culture);
        BannerSet GetBannerSetById(int Id, string culture);
        IEnumerable<BannerSet> GetAllBannerSets(string culture);
        IEnumerable<Banner> GetBanners(int Id, string culture);
        IEnumerable<BannerSet> GetBannersWithLinks(string name, string culture);
        IEnumerable<BannerSet> GetBannerSetsNoLinks(string culture);


        Company SaveCompanyName(string name);
    }
}
