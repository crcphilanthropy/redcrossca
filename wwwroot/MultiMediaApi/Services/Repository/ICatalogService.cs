﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface ICatalogService
    {
        string CleanStringRegx(string value);
        string CleanStringNormal(string dirtyString);
        List<int> CleanCategoryUri(string input);
        IEnumerable<Category> Categories(string culture);
        IEnumerable<Region> Regions(string culture);
        Tags GetTags(string culture);
        string CleanPath(string image);
        string CleanFilePath(string path);
        IEnumerable<Category> CategoriesById(List<int> ids, string culture);
        IEnumerable<Region> RegionsById(List<int> ids, string culture);
        string GetDisplayName(string codedName);
        List<int> GetCategoryIds(string categories);
        List<int> GetRegionIds(string regions);
        string GetHoursStringForVideoDetails(DateTime date);
        string GetMinsStringForVideoDetails(DateTime date);
        string GetSecondsStringForVideoDetails(DateTime date);
        Photo CleanPhotoImageData(Photo entity, bool excludeHighRes = false);
        Video CleanVideoImageData(Video entity);
        MediaAlertForm SaveAlertForm(string classname, MediaAlertForm data);
        bool CheckIdValue(int id);

        string VideoDetailsDuration(DateTime? date, string durationFormat);
    }
}
