﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface IEmbedLinkService
    {
        string CreateEmbedLinks(Banner banner, string embedcode);
    }
}
