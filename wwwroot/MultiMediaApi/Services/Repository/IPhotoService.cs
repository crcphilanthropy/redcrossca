﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Services.Repository
{
    public interface IPhotoService
    {
        IEnumerable<Photo> SearchPhotos(string categories, string regions, string culture);
        IEnumerable<Photo> GetAllPhotos(string culture);
        Photo GetById(int id, string culture);

        Form SaveForm(Form data);
    }
}
