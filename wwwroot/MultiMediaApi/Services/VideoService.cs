﻿using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Services
{
    public class VideoService : IVideoService
    {
        private readonly IVideo _video;
        private readonly IMediaLibraryService _media;
        private readonly ICatalogService _catalog;
        private readonly IForm _form;

        public VideoService()
        {
            _video = new VideoRepository();
            _media = new MediaLibraryService();
            _catalog = new CatalogService();
            _form = new FormRepository();
        }

        public IEnumerable<Video> SearchVideos(string categories, string regions, string path, string culture)
        {
            var searchresults = new List<Video>();
            if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(culture))
            {
                try
                {
                    path = _catalog.CleanFilePath(path);
                    if (string.IsNullOrEmpty(categories) && string.IsNullOrEmpty(regions))
                    {
                        searchresults = _video.GetAll(path, culture).ToList();
                        if (searchresults != null)
                        {
                            foreach (var item in searchresults)
                            {
                                if (item.Thumbnail != null)
                                {
                                    var thumbnail = GetThumbnail(item);
                                    item.Thumbnail = thumbnail;
                                }
                            }
                        }
                        return searchresults;
                    }
                    else
                    {
                        //Do cleaning/get ID's of category and region string
                        var categoryIds = _catalog.CleanCategoryUri(categories);
                        var regionIds = _catalog.CleanCategoryUri(regions);

                        searchresults = _video.VideoQuery(categoryIds, regionIds, path, culture).ToList();
                        if (searchresults != null)
                        {
                            foreach (var item in searchresults)
                            {
                                if (item.Thumbnail != null)
                                {
                                    var thumbnail = GetThumbnail(item);
                                    item.Thumbnail = thumbnail;
                                }
                            }
                        }

                        return searchresults;
                    }

                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("VideoService", "SearchVideos", ex, 0, "A given argument is invalid");
                    else
                        EventLogProvider.LogException("VideoService", "SearchVideos", ex, 0, ex.Message);
                    return searchresults;
                }
            }
            else
            {
                return searchresults;
            }

        }

        public IEnumerable<Video> GetAllVideos(string path, string culture)
        {
            try
            {
                //path = _catalog.CleanFilePath(path);
                var result = _video.GetAll(path, culture);
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.Thumbnail != null)
                        {
                            var thumbnail = GetThumbnail(item);
                            item.Thumbnail = thumbnail;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    throw new ArgumentException(ex.Message);
                else
                    throw new Exception(ex.Message);
            }

        }

        public IEnumerable<Video> GetRelatedVideos(Guid id, List<int> categories, List<int> regions, string path, DateTime? date, string culture)
        {
            var results = new List<Video>();
            try
            {
                results = _video.RelatedVideoQuery(id, categories, regions, path, date.Value, culture).ToList();
                if (results != null)
                {
                    foreach (var item in results)
                    {
                        if (item.Thumbnail != null)
                        {
                            var thumbnail = GetThumbnail(item);
                            item.Thumbnail = thumbnail;
                        }
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("VideoService", "GetRelatedVideos", ex, 0, "A given argument is invalid");
                else
                    EventLogProvider.LogException("VideoService", "GetRelatedVideos", ex, 0, ex.Message);
                return results;
            }
        }

        public List<Video> GetDetailsRelatedVideos(Guid id, List<int> catids, List<int> regids, DateTime created, TreeNode CurrentDocument, string parentPath, string culture)
        {
            var result = new List<Video>();
            try
            {
                var relatedGuids = CurrentDocument.GetValue("RelatedItems", string.Empty).Split(new[] { ";", "|", "," }, StringSplitOptions.RemoveEmptyEntries).Select(item => ValidationHelper.GetGuid(item, Guid.Empty)).ToList();
                if (relatedGuids != null && relatedGuids.Count() > 0)
                {
                    foreach (var item in relatedGuids)
                    {
                        Guid clean = ValidationHelper.GetGuid(item, Guid.Empty);
                        if (clean != Guid.Empty && clean != id)
                        {
                            var video = this.GetByGuid(clean);
                            if (video != null)
                            {
                                result.Add(video);
                            }
                        }
                    }
                }
                //Get related videos through api service if no related guids exist
                if (relatedGuids.Count() <= 0)
                {
                    if (!string.IsNullOrEmpty(_catalog.CleanFilePath(parentPath)))
                    {

                        var relatedVideos = this.GetRelatedVideos(id, catids, regids, parentPath, created, culture).ToList();
                        if (relatedVideos != null)
                        {
                            result = relatedVideos;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("VideoService", "GetDetailsRelatedVideos", ex, 0, "A given argument is invalid");
                else
                    EventLogProvider.LogException("VideoService", "GetDetailsRelatedVideos", ex, 0, ex.Message);
                return result;
            }

        }

        public Video GetById(int id, string culture)
        {
            if (id > 0)
            {
                try
                {
                    var result = _video.GetById(id, culture);
                    if (result.Thumbnail != null)
                    {
                        var thumbnail = GetThumbnail(result);
                        result.Thumbnail = thumbnail;
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("VideoService", "GetById", ex, 0, "A given argument is invalid");
                    else
                        EventLogProvider.LogException("VideoService", "GetById", ex);
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public Video GetByGuid(Guid id)
        {
            if (id != Guid.Empty)
            {
                try
                {
                    var result = _video.GetByGuid(id);
                    if (result.Thumbnail != null)
                    {
                        var thumbnail = GetThumbnail(result);
                        result.Thumbnail = thumbnail;
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("VideoService", "GetByGuid", ex, 0, "Given id is invalid");
                    else
                        EventLogProvider.LogException("VideoService", "GetByGuid", ex);
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public Form SaveForm(Form data)
        {
            try
            {
                //data.Name = _catalog.CleanFormInput(data.Name);
                //data.Publication = _catalog.CleanFormInput(data.Publication);
                var result = _form.SaveVideoForm(data);
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("VideoService", "SaveForm", ex, 0, "Given form data is invalid");
                else if (ex is ArgumentNullException)
                    EventLogProvider.LogException("VideoService", "SaveForm", ex, 0, "Form data is null");
                else if (ex is NullReferenceException)
                    EventLogProvider.LogException("VideoService", "SaveForm", ex, 0, "Cannot derefrence null object when trying to save form data");
                else
                    EventLogProvider.LogException("VideoService", "SaveForm", ex, 0, ex.Message);
                return null;
            }
        }

        private string GetThumbnail(Video entity)
        {
            var temp = entity.Thumbnail;
            //var image = HttpUtility.HtmlEncode(temp.Replace("~", ""));
            var image = temp.Replace("~", "");
            return image;
        }
    }
}