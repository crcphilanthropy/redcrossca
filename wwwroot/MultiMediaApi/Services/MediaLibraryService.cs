﻿using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Services
{
    public class MediaLibraryService : IMediaLibraryService
    {
        private readonly IFileResult _file;

        public MediaLibraryService()
        {
            _file = new FileRepository();
        }

        public FileResult GetMedia(string path, string code)
        {
            try
            {
                var library = _file.GetLibrary(code);
                var file = _file.GetFile(library, path);
                return file;

            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    throw new ArgumentException("Missing arguments in request.");
                else
                    throw new Exception(ex.Message);
            }

        }
    }
}