﻿using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MultiMediaApi.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly ICatalog _catalog;
        private readonly IForm _form;
        private readonly CultureInfo _culture;

        public CatalogService()
        {
            _catalog = new CatalogRepository();
            _form = new FormRepository();
            _culture = LocalizationContext.CurrentCulture;
        }

        public string CleanStringRegx(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                EventLogProvider.LogException("CatalogService", "CleanStringRegx", null, 0, "Method value is null or empty");
                return null;
            }
            
            try
            {
                return Regex.Replace(value, @"(\+|@|\'|\^|\(|\)|\<|\>|\#|\/|\!|\$|\%|\*)", "", RegexOptions.None);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("CatalogService", "CleanStringRegx", ex, 0, "Given argument is invalid");
                else
                    EventLogProvider.LogException("CatalogService", "CleanStringRegx", ex, 0, ex.Message);

                return null;
            }

        }

        public List<int> CleanCategoryUri(string input)
        {
            var Ids = new List<int>();
            var temp = new List<string>();
            if (!string.IsNullOrEmpty(input))
            {
                temp = input.Split(',').ToList();
            }
            if (temp != null && temp.Count() > 0)
            {
                foreach (var item in temp)
                {
                    var templist = item.Split('_').ToList();
                    foreach (var value in templist)
                    {
                        int returnValue;
                        if (int.TryParse(value, out returnValue))
                        {
                            Ids.Add(returnValue);
                        }
                    }
                }
            }
            return Ids;
        }

        public string CleanFilePath(string path)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    return Regex.Replace(path, @"(\s+|@|\&|\'|\^|\(|\)|\<|\>|\#|\\|\!|\$|\%|\*)", "", RegexOptions.None);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("CatalogService", "CleanFilePath", ex, 0, ex.Message);
                    return null;
                }
            }
            else
            {
                EventLogProvider.LogException("CatalogService", "CleanFilePath", null, 0, "Method value is null or empty");
                return result;
            }

        }

        public string CleanStringNormal(string dirtyString)
        {
            if (string.IsNullOrEmpty(dirtyString))
            {
                EventLogProvider.LogException("CatalogService", "CleanStringNormal", null, 0, "Method value is null or empty");
                return null;
            }
            try
            {
                return Regex.Replace(dirtyString, @"(\+|@|\'|\.|\-|\^|\&|\(|\)|\<|\>|\#|\/|\!|\$|\%|\*)", "", RegexOptions.None);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("CatalogService", "CleanStringNormal", ex, 0, "Given argument is invalid");
                else
                    EventLogProvider.LogException("CatalogService", "CleanStringNormal", ex, 0, ex.Message);

                return null;
            }
        }

        public IEnumerable<Category> CategoriesById(List<int> ids, string culture)
        {
            var results = new List<Category>();
            if (ids.Count() != 0 && ids != null)
            {
                try
                {
                    foreach (var item in ids)
                    {
                        var category = _catalog.GetCategoryById(item, culture);
                        //Get the culture appropriate display name
                        if (category != null)
                        {
                            results.Add(category);
                        }
                    }
                    return results;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("CatalogService", "CategoriesById", ex, 0, "Given ID is invalid");
                    else
                        EventLogProvider.LogException("CatalogService", "CategoriesById", ex, 0, ex.Message);

                    return results;
                }

            }
            else
            {
                EventLogProvider.LogException("CatalogService", "CategoriesById", null, 0, "Method value is null or empty");
                return results;
            }


        }

        public IEnumerable<Region> RegionsById(List<int> ids, string culture)
        {
            var results = new List<Region>();
            if (ids.Count() != 0 || ids != null)
            {
                try
                {
                    foreach (var item in ids)
                    {
                        var category = _catalog.GetRegionById(item, culture);
                        //Get the culture appropriate display name
                        if (category != null)
                        {
                            results.Add(category);
                        }
                    }
                    return results;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("CatalogService", "RegionsById", ex, 0, "Given ID is invalid");
                    else
                        EventLogProvider.LogException("CatalogService", "RegionsById", ex, 0, ex.Message);

                    return results;
                }
            }
            else
            {
                EventLogProvider.LogException("CatalogService", "RegionsById", null, 0, "Method value is null or empty");
                return results;
            }



        }

        public List<int> GetCategoryIds(string categories)
        {
            var result = new List<int>();
            if (!string.IsNullOrEmpty(categories))
            {
                try
                {
                    result = _catalog.GetCategoryIds(categories);
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("CatalogService", "GetCategoryIds", ex, 0, "Given categories is invalid");
                    else
                        EventLogProvider.LogException("CatalogService", "GetCategoryIds", ex, 0, ex.Message);
                    return result;
                }
                
            }
            else
            {
                EventLogProvider.LogException("CatalogService", "GetCategoryIds", null, 0, "Method value is null or empty");
                return result;
            }

        }
        public List<int> GetRegionIds(string regions)
        {
            var result = new List<int>();
            if (!string.IsNullOrEmpty(regions))
            {
                try
                {
                    result = _catalog.GetCategoryIds(regions);
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("CatalogService", "GetRegionIds", ex, 0, "Given regions is invalid");
                    else
                        EventLogProvider.LogException("CatalogService", "GetRegionIds", ex, 0, ex.Message);
                    return result;
                }
            }
            else
            {
                EventLogProvider.LogException("CatalogService", "GetRegionIds", null, 0, "Method value is null or empty");
                return result;
            }

        }

        public IEnumerable<Category> Categories(string culture)
        {
            var results = new List<Category>();
            try
            {
                results = _catalog.GetAllCategories(culture).ToList();
                return results;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("CatalogService", "Categories", ex, 0, ex.Message);
                return results;
            }
        }

        public IEnumerable<Region> Regions(string culture)
        {
            var results = new List<Region>();
            try
            {
                results = _catalog.GetAllRegions(culture).ToList();
                return results;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("CatalogService", "Regions", ex, 0, ex.Message);
                return results;
            }
        }

        public Tags GetTags(string culture)
        {
            try
            {
                var result = new Tags();
                var regions = _catalog.GetAllRegions(culture);
                var categories = _catalog.GetAllCategories(culture);
                if (regions != null)
                    result.Regions = regions;
                if (categories != null)
                    result.Categories = categories;

                return result;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("CatalogService", "GetTags", ex, 0, ex.Message);
                return null;
            }
        }

        public string GetDisplayName(string codedName)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(codedName))
            {
                result = ResHelper.LocalizeString(codedName);
            }
            return result;
        }

        public string CleanPath(string path)
        {
            var temp = string.Empty;
            var result = string.Empty;
            if (!string.IsNullOrEmpty(path))
            {
                temp = path;
                try
                {
                    result = temp.Replace("~", "");
                    return result;
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("CatalogService", "CleanPath", ex, 0, ex.Message);
                    return result;
                }
            }
            else
            {
                EventLogProvider.LogException("CatalogService", "CleanPath", null, 0, "Method value is null or empty");
                return result;
            }
                

        }

        public string GetHoursStringForVideoDetails(DateTime date)
        {
            DateTime temp;
            var result = string.Empty;
            if (DateTime.TryParse(date.ToString("G"), out temp))
            {
                if (date.Hour < 2 && date.Hour > 0)
                    result = ResHelper.GetString("CRC.Hour");
                else if (date.Hour >= 2)
                    result = ResHelper.GetString("CRC.Hours");
                else if (date.Hour <= 0)
                    return result;
            }                     
            return result;
        }

        public string GetMinsStringForVideoDetails(DateTime date)
        {
            var result = string.Empty;
            DateTime temp;
            if (DateTime.TryParse(date.ToString("G"), out temp))
            {
                if (date.Minute < 2 && date.Minute > 0)
                    result = ResHelper.GetString("CRC.Minute");
                else if (date.Minute >= 2)
                    result = ResHelper.GetString("CRC.Minutes");
                else if (date.Minute <= 0)
                    return result;
            }          
            return result;
        }

        public string GetSecondsStringForVideoDetails(DateTime date)
        {
            var result = string.Empty;
            DateTime temp;
            if (DateTime.TryParse(date.ToString("G"), out temp))
            {
                if (date.Second < 2 && date.Second > 0)
                    result = ResHelper.GetString("CRC.Second");
                else if (date.Second <= 0 && (date.Minute > 0 || date.Hour > 0))
                    return result;
                else
                    result = ResHelper.GetString("CRC.Seconds");
            }
            return result;
        }

        public Video CleanVideoImageData(Video entity)
        {
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Thumbnail))
                    entity.Thumbnail = entity.Thumbnail.Replace("~", "");
            }           
            return entity;
        }

        public Photo CleanPhotoImageData(Photo entity, bool excludeHighRes)
        {
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Image))
                    entity.Image = entity.Image.Replace("~", "");
                if (!string.IsNullOrEmpty(entity.Thumbnail))
                    entity.Thumbnail = entity.Thumbnail.Replace("~", "");
                if (!string.IsNullOrEmpty(entity.HighResImage) && excludeHighRes == false)
                    entity.HighResImage = entity.HighResImage.Replace("~", "");
                else
                    entity.HighResImage = string.Empty;
            }           
            return entity;
        }

        public MediaAlertForm SaveAlertForm(string classname, MediaAlertForm data)
        {
            try
            {
                var result = _form.SaveAlert(classname, data);
                //result.Email = HttpUtility.HtmlEncode(result.Email);
                //result.Name = HttpUtility.HtmlEncode(result.Name);
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("CatalogService", "SaveAlertForm", ex, 0, "Given form data is invalid");
                else if (ex is ArgumentNullException)
                    EventLogProvider.LogException("CatalogService", "SaveAlertForm", ex, 0, "Form data is null");
                else if (ex is NullReferenceException)
                    EventLogProvider.LogException("CatalogService", "SaveAlertForm", ex, 0, "Cannot derefrence null object when trying to save form data");
                else
                    EventLogProvider.LogException("CatalogService", "SaveAlertForm", ex, 0, ex.Message);
                return null;
            }
        }

        public bool CheckIdValue(int id)
        {
            var property = id.ToString();
            int returnValue;
            if (int.TryParse(property, out returnValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string VideoDetailsDuration(DateTime? date, string durationFormat)
        {
            var duration = string.Empty;
            DateTime outputdate;
            if (date.HasValue && DateTime.TryParse(date.Value.ToString("G"), out outputdate))
            {
                var hour = outputdate.Hour <= 0 ? string.Empty : outputdate.Hour.ToString();
                var minutes = outputdate.Minute <= 0 ? string.Empty : outputdate.Minute.ToString();
                var seconds = outputdate.Second <= 0 && (outputdate.Minute > 0 || outputdate.Hour > 0) ? string.Empty : outputdate.Second.ToString();

                string hoursString = this.GetHoursStringForVideoDetails(outputdate);
                string minutesString = this.GetMinsStringForVideoDetails(outputdate);
                string secondsString = this.GetSecondsStringForVideoDetails(outputdate);

                duration = string.Format(durationFormat, hour, hoursString, minutes, minutesString, seconds, secondsString);
            }
            return duration;
        }
    }
}