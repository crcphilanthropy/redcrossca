﻿using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.SiteProvider;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace MultiMediaApi.Services
{
    public class EmbedLinkService : IEmbedLinkService
    {
        private readonly IBanner _banner;
        private readonly ICatalogService _catalog;

        public EmbedLinkService()
        {
            _banner = new BannerRepository();
            _catalog = new CatalogService();
        }

        public string CreateEmbedLinks(Banner banner, string trackingurl)
        {
            var link = string.Empty;
            //Check if banner id is < 0
            if (banner.BannerID < 0)
            {
                EventLogProvider.LogException("EmbedLinkService", "CreateEmbedLinks", null, 0, "Given banner is invalid");
                return null;
            }
            try
            {
                if (banner != null)
                {
                    if (string.IsNullOrEmpty(banner.EmbedLink))
                    {
                        //Generate embedlink for banner from according to provided format 
                        // Initialize StringWriter instance.
                        StringWriter stringWriter = new StringWriter();
                        //Current site name
                        var current_site_name = SiteContext.CurrentSiteName;
                        // Get the site info
                        SiteInfo currentSite = SiteInfoProvider.GetSiteInfo(current_site_name);
                        //get site domain
                        var siteDomain = URLHelper.CorrectDomainName(currentSite.DomainName);
                        var absoluteDomain = URLHelper.GetApplicationUrl(siteDomain);

                        // Put HtmlTextWriter in using block because it needs to call Dispose.
                        using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
                        {
                            // Some strings for the attributes.
                            string urlValue = trackingurl;
                            string imageValue = _catalog.CleanPath(absoluteDomain+banner.Image);
                            var length = banner.BannerType.Split('*');

                            // The important part:
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, urlValue.Trim(), false);
                            writer.RenderBeginTag(HtmlTextWriterTag.A); // Begin #1

                            writer.AddAttribute(HtmlTextWriterAttribute.Src, imageValue.Trim(), false);
                            writer.AddAttribute(HtmlTextWriterAttribute.Width, length[0].Trim(), false);
                            writer.AddAttribute(HtmlTextWriterAttribute.Height, length[1].Trim(), false);
                            writer.AddAttribute(HtmlTextWriterAttribute.Alt, banner.AltText.Trim(), false);

                            writer.RenderBeginTag(HtmlTextWriterTag.Img); // Begin #2
                            writer.RenderEndTag(); // End #2
                            writer.RenderEndTag(); // End #1
                            writer.Write(link);
                            
                        }
                        link = stringWriter.ToString().Replace("\"", "'");
                    }
                    else
                    {
                        link = banner.EmbedLink;
                    }
                }
                return link;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("EmbedLinkService", "CreateEmbedLinks", ex, 0, "Given banner ID is invalid");
                else
                    EventLogProvider.LogException("EmbedLinkService", "CreateEmbedLinks", ex, 0, ex.Message);

                return null;
            }
        }
    }
}