﻿using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using MultiMediaApi.Models;
using MultiMediaApi.Repository;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Services
{
    public class BannerService : IBannerService
    {
        private readonly IBanner _banner;
        private readonly IBannerSet _bannerset;
        private readonly IEmbedLinkService _embedlink;
        private readonly ITrackingCodeService _trackingcode;
        private readonly ICatalogService _catalog;
        private readonly ICompany _company;
        private readonly CultureInfo _culture;

        public BannerService()
        {
            _banner = new BannerRepository();
            _bannerset = new BannerSetRepository();
            _embedlink = new EmbedLinkService();
            _trackingcode = new TrackingCodeService();
            _catalog = new CatalogService();
            _company = new CompanyRepository();
            _culture = LocalizationContext.CurrentCulture;
        }

        #region Create
        public void CreateBanner(Banner b)
        {
            if (b != null)
            {
                try
                {
                    _banner.Create(b);
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }
        public void CreateBannerSet(BannerSet b)
        {
            if (b != null)
            {
                try
                {
                    _bannerset.Create(b);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        #endregion

        #region Get
        public IEnumerable<BannerSet> GetAllBannerSets(string culture)
        {
            var result = new List<BannerSet>();
            try
            {
                result = _bannerset.GetAll(culture).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        var banners = this.GetBanners(item.BannerSetID, culture);
                        item.Banners = banners;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("BannerService", "GetAllBannerSets", ex, 0, ex.Message);
                return result;
            }
        }

        public IEnumerable<BannerSet> GetBannerSetsNoLinks(string culture)
        {
            var results = new List<BannerSet>();
            try
            {
                //results = CacheHelper.Cache(
                //       cs =>
                //       {
                //           // Get all bannersets;
                //           var result = _bannerset.GetAll(culture).ToList();
                //           if (result != null)
                //           {
                //               foreach (var item in result)
                //               {
                //                   var banners = this.GetBanners(item.BannerSetID, culture);
                //                   foreach (var banner in banners)
                //                   {
                //                       banner.EmbedLink = "";
                //                       banner.TrackingCode = "";
                //                   }
                //                   item.Banners = banners;
                //               }
                //           }
                //           // Setup the cache dependencies only when caching is active
                //           if (cs.Cached)
                //           {
                //               cs.CacheDependency = CacheHelper.GetCacheDependency("crcwebbanners.bannerset|all|" + culture.ToLower());
                //           }
                //           return result;
                //       },
                //       new CacheSettings(_cache.GetCacheMinutes(), _cache.GetCacheKey("multimediabannersets"))
                //   );
                // Get all bannersets;
                results = _bannerset.GetAll(culture).ToList();
                if (results != null)
                {
                    foreach (var item in results)
                    {
                        item.CampaignName = null;
                        item.TrackingUrl = null;
                        var banners = this.GetBanners(item.BannerSetID, culture);
                        foreach (var banner in banners)
                        {
                            banner.EmbedLink = string.Empty;
                            banner.TrackingCode = string.Empty;
                        }
                        item.Banners = banners;
                    }
                }

                return results;
            }
            catch (Exception ex)
            {
                if (ex is NullReferenceException)
                    EventLogProvider.LogException("BannerService", "GetBannerSetsNoLinks", ex, 0, "Cannot dereference null object when trying get banners");
                else
                    EventLogProvider.LogException("BannerService", "GetBannerSetsNoLinks", ex, 0, ex.Message);

                return results;
            }
        }

        public IEnumerable<BannerSet> GetBannersWithLinks(string companyname, string culture)
        {
            //Save embed link and tracking code in banner entity
            var link = string.Empty;
            var code = string.Empty;
            var bannersets = new List<BannerSet>();
            try
            {
                //bannersets = CacheHelper.Cache(
                //    cs =>
                //    {
                //        // Get all bannersets; filter by companyname
                //        var result = this.GetAllBannerSets(culture).ToList();
                //        if (result != null)
                //        {
                //            foreach (var item in result)
                //            {
                //                foreach (var banner in item.Banners)
                //                {
                //                    //Create embed link
                //                    if (!string.IsNullOrEmpty(companyname))
                //                    {
                //                        //create tracking code
                //                        code = _trackingcode.CreateTrackingCode(companyname, banner);
                //                        if (!string.IsNullOrEmpty(code))
                //                        {
                //                            banner.TrackingCode = code;
                //                        }
                //                        //create Embed Link
                //                        link = _embedlink.CreateEmbedLinks(banner);
                //                        if (!string.IsNullOrEmpty(link))
                //                        {
                //                            banner.EmbedLink = link;
                //                        }
                //                    }

                //                }
                //            }
                //        }

                //        // Setup the cache dependencies only when caching is active
                //        if (cs.Cached)
                //        {
                //            cs.CacheDependency = CacheHelper.GetCacheDependency("crcwebbanners.bannerset|crcwebbanners.company|"+companyname+"|"+culture.ToLower());
                //        }

                //        return result;
                //    },
                //        new CacheSettings(_cache.GetCacheMinutes(), _cache.GetCacheKey("multimediabannersetswith_"+companyname))
                // );
                bannersets = this.GetAllBannerSets(culture).ToList();
                if (bannersets != null)
                {
                    foreach (var item in bannersets)
                    {
                        foreach (var banner in item.Banners)
                        {
                            //Create embed link
                            if (!string.IsNullOrEmpty(companyname))
                            {
                                //create tracking code
                                code = _trackingcode.CreateTrackingCode(companyname, banner, item.TrackingUrl, item.CampaignName);
                                if (!string.IsNullOrEmpty(code))
                                {
                                    banner.TrackingCode = code;
                                }
                                //create Embed Link
                                link = _embedlink.CreateEmbedLinks(banner, banner.TrackingCode);
                                if (!string.IsNullOrEmpty(link))
                                {
                                    banner.EmbedLink = link;
                                }
                            }
                        }
                        item.CampaignName = null;
                        item.TrackingUrl = null;
                    }
                }

                return bannersets;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("BannerService", "GetBannersWithLinks", ex, 0, "Given banner id or company name is invalid");
                else
                    EventLogProvider.LogException("BannerService", "GetBannersWithLinks", ex, 0, ex.Message);

                return bannersets;
            }

        }

        public IEnumerable<Banner> GetBanners(int SetId, string culture)
        {
            var result = new List<Banner>();
            try
            {
                result = _banner.GetBannersBySet(SetId, culture).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        item.Image = _catalog.CleanPath(item.Image);
                        item.BannerType = item.BannerType;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    EventLogProvider.LogException("BannerService", "GetBanners", ex, 0, "Given ID is invalid");
                else
                    EventLogProvider.LogException("BannerService", "GetBanners", ex, 0, ex.Message);

                return result;
            }
        }

        public Banner GetBannerById(int Id, string culture)
        {
            if (Id > 0)
            {
                try
                {
                    var result = _banner.GetById(Id, culture);
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("BannerService", "GetBannerById", ex, 0, "Given banner id is invalid");
                    else
                        EventLogProvider.LogException("BannerService", "GetBannerById", ex, 0, ex.Message);

                    return null;
                }
            }
            else
            {
                EventLogProvider.LogException("BannerService", "GetBannerSetById", null, 0, "Given company id is invalid");
                return null;
            }

        }

        public BannerSet GetBannerSetById(int Id, string culture)
        {
            if (Id > 0)
            {
                try
                {
                    //var result = _cache.CacheBannerSet(Id);
                    var result = _bannerset.GetById(Id, culture);
                    if (result != null)
                    {
                        result.Banners = this.GetBanners(result.BannerSetID, culture);
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        EventLogProvider.LogException("BannerService", "GetBannerSetById", ex, 0, "Given bannerset id is invalid");
                    else
                        EventLogProvider.LogException("BannerService", "GetBannerSetById", ex, 0, ex.Message);

                    return null;
                }
            }
            else
            {
                EventLogProvider.LogException("BannerService", "GetBannerSetById", null, 0, "Given company id is invalid");
                return null;
            }


        }

        #endregion

        public Company SaveCompanyName(string name)
        {
            var company = new Company();
            if (string.IsNullOrEmpty(name))
            {
                EventLogProvider.LogException("BannerService", "SaveCompanyName", null, 0, "Given company name is invalid");
                return null;
            }
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var cleanInput = _catalog.CleanStringRegx(name);
                    //Check if company name already exists in database
                    //if not - create new company
                    if (!string.IsNullOrEmpty(cleanInput))
                    {
                        if (!_company.CompanyExists(cleanInput))
                        {
                            company.CompanyName = cleanInput.Trim();
                            _company.Create(company);
                            company = _company.GetByName(cleanInput);
                        }else
                        {
                            company = _company.GetByName(cleanInput);
                        }
                    }
                }
                return company;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("BannerService", "SaveCompanyName", ex, 0, ex.Message);
                return null;
            }


        }

        #region helpers

        #endregion
    }
}