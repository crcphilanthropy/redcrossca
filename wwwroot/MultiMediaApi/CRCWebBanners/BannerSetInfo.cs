using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using CRCWebBanners;
using System.Collections.Generic;

[assembly: RegisterObjectType(typeof(BannerSetInfo), BannerSetInfo.OBJECT_TYPE)]

namespace CRCWebBanners
{
    /// <summary>
    /// BannerSetInfo data container class.
    /// </summary>
	[Serializable]
    public class BannerSetInfo : AbstractInfo<BannerSetInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "crcwebbanners.bannerset";


        /// <summary>
        /// Type information.
        /// </summary>

        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(BannerSetInfoProvider), OBJECT_TYPE, "CRCWebBanners.BannerSet", "BannerSetID", null, null, "BannerSetID", "Title", null, null, null, null)
        {
            ModuleName = "CRCWebBanners",
            TouchCacheDependencies = true,
            OrderColumn = "BannerSetOrder",
            ImportExportSettings =
            {
                IsExportable = true,
                AllowSingleExport = true,
                ObjectTreeLocations = new List<ObjectTreeLocation>()
                {
                  // Adds the custom class into a new category in the Global objects section of the export tree
                  new ObjectTreeLocation(GLOBAL, "CRCWebBanners"),
                }
            },
            LogSynchronization = SynchronizationTypeEnum.LogSynchronization, // Enables logging of staging tasks for changes made to Office objects
            SynchronizationObjectTreeLocations = new List<ObjectTreeLocation>()
            {
                //Creates a new category in the 'Global objects' section of the staging object tree
                new ObjectTreeLocation(GLOBAL, "CRCWebBanners")
            }
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Banner set ID
        /// </summary>
        [DatabaseField]
        public virtual int BannerSetID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("BannerSetID"), 0);
            }
            set
            {
                SetValue("BannerSetID", value);
            }
        }


        /// <summary>
        /// Title
        /// </summary>
        [DatabaseField]
        public virtual string Title
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title"), String.Empty);
            }
            set
            {
                SetValue("Title", value, String.Empty);
            }
        }


        /// <summary>
        /// Title FR
        /// </summary>
        [DatabaseField]
        public virtual string Title_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title_FR"), String.Empty);
            }
            set
            {
                SetValue("Title_FR", value, String.Empty);
            }
        }


        /// <summary>
        /// Description
        /// </summary>
        [DatabaseField]
        public virtual string Description
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Description"), String.Empty);
            }
            set
            {
                SetValue("Description", value, String.Empty);
            }
        }


        /// <summary>
        /// Description FR
        /// </summary>
        [DatabaseField]
        public virtual string Description_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Description_FR"), String.Empty);
            }
            set
            {
                SetValue("Description_FR", value, String.Empty);
            }
        }

        [DatabaseField]
        public virtual int BannerSetOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("BannerSetOrder"), 0);
            }
            set
            {
                SetValue("BannerSetOrder", value, 0);
            }
        }

        /// <summary>
        /// Display Banner Set in English only.
        /// </summary>
        [DatabaseField]
        public virtual bool ShowEnglish
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("ShowEnglish"), true);
            }
            set
            {
                SetValue("ShowEnglish", value);
            }
        }


        /// <summary>
        /// Display Banner Set in French only
        /// </summary>
        [DatabaseField]
        public virtual bool ShowFrench
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("ShowFrench"), true);
            }
            set
            {
                SetValue("ShowFrench", value);
            }
        }


        /// <summary>
        /// Archive/disable Banner Set
        /// </summary>
        [DatabaseField]
        public virtual bool Archive
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Archive"), false);
            }
            set
            {
                SetValue("Archive", value);
            }
        }


        /// <summary>
        /// BannerSet tracking code URL
        /// </summary>
        [DatabaseField]
        public virtual string TrackingUrl
        {
            get
            {
                return ValidationHelper.GetString(GetValue("TrackingUrl"), String.Empty);
            }
            set
            {
                SetValue("TrackingUrl", value, String.Empty);
            }
        }


        /// <summary>
        /// Campaign name
        /// </summary>
        [DatabaseField]
        public virtual string CampaignName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("CampaignName"), String.Empty);
            }
            set
            {
                SetValue("CampaignName", value, String.Empty);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            BannerSetInfoProvider.DeleteBannerSetInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            BannerSetInfoProvider.SetBannerSetInfo(this);
        }

        #endregion


        #region "Constructors"

        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public BannerSetInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty BannerSetInfo object.
        /// </summary>
        public BannerSetInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new BannerSetInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public BannerSetInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}