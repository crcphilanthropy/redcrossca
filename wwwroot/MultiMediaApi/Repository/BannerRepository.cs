﻿using MultiMediaApi.Models;
using System.Collections.Generic;
using System.Linq;
using CMS.Base;
using AutoMapper;
using CRCWebBanners;
using CMS.Localization;
using CMS.Helpers;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class BannerRepository : IBanner
    {
        public BannerRepository()
        {

        }

        public void Create(Banner b)
        {
            var entity = Mapper.Map<Banner, BannerInfo>(b);
            BannerInfoProvider.SetBannerInfo(entity);
        }

        public void Update(Banner b)
        {
            var entity = Mapper.Map<Banner, BannerInfo>(b);
            BannerInfoProvider.SetBannerInfo(entity);
        }

        public void Delete(int id)
        {
            BannerInfoProvider.DeleteBannerInfo(id);
        }

        public Banner GetById(int id, string culture)
        {
            var entity = BannerInfoProvider.GetBannerInfo(id);
            var result = new Banner();
            if (entity != null)
            {
                if (!entity.Archive)
                {
                    result = Mapper.Map<BannerInfo, Banner>(entity, dest =>
                    {
                        dest.AfterMap((src, dst) =>
                        {
                            dst.AltText = this.GetCultureAltText(entity, culture);
                            dst.Title = this.GetCultureTitle(entity, culture);
                        });
                    });
                }               
                return result;
            }else
            {
                return null;
            }
        }

        public IEnumerable<Banner> GetAll()
        {
            var entity = BannerInfoProvider.GetBanners().ToList();
            var result = new List<Banner>();
            if (entity != null)
            {
                foreach (var item in entity)
                {
                    var banner = Mapper.Map<BannerInfo, Banner>(item, dest =>
                    {
                        dest.AfterMap((src, dst) =>
                        {
                            //dst.AltText = this.GetCultureAltText(item);
                            //dst.Title = this.GetCultureTitle(item);
                        });
                    });
                    result.Add(banner);
                }
                
                return result;
            }else
            {
                return null;
            }
        }

        public IEnumerable<Banner> GetBannersBySet(int id, string culture)
        {
            var result = new List<Banner>();
            var entity = BannerInfoProvider.GetBanners().Where(b => !b.Archive && b.BannerSetID.Equals(id)).OrderBy(b => b.BannerOrder).ToList();
            if (entity != null)
            {
                foreach (var item in entity)
                {
                    var banner = Mapper.Map<BannerInfo, Banner>(item, dest =>
                    {
                        dest.AfterMap((src, dst) =>
                        {
                            dst.AltText = this.GetCultureAltText(item, culture);
                            dst.Title = this.GetCultureTitle(item, culture);
                        });
                    });
                    result.Add(banner);
                }
                return result;
            }else
            {
                return null;
            }           
        }

        #region helpers
        private string GetCultureTitle(BannerInfo item, string culture)
        {
            var title = string.Empty;
            if (item != null)
            {
                if (culture.ToLower() == "en-ca")
                    title = item.Title;
                else if (culture.ToLower() == "fr-ca")
                    title = item.Title_FR;
            }
           
            return ValidationHelper.GetString(title, string.Empty);
        }

        private string GetCultureAltText(BannerInfo item, string culture)
        {
            var title = string.Empty;
            if (item != null)
            {
                if (culture.ToLower() == "en-ca")
                    title = item.AltText;
                else if (culture.ToLower() == "fr-ca")
                    title = item.AltText_FR;
            }
            return ValidationHelper.GetString(title, string.Empty);
        }
        #endregion
    }
}