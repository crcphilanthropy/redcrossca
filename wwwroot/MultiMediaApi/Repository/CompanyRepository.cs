﻿using AutoMapper;
using CRCWebBanners;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class CompanyRepository : ICompany
    {
        
        public CompanyRepository()
        {

        }

        public void Create(Company company)
        {
            var entity = new CompanyInfo() { CompanyName = company.CompanyName };
            //var entity = Mapper.Map<CompanyInfo>(company);
            CompanyInfoProvider.SetCompanyInfo(entity);
        }

        public Company GetById(int id)
        {
            var entity = CompanyInfoProvider.GetCompanyInfo(id);
            if (entity != null)
            {
                var result = Mapper.Map<CompanyInfo, Company>(entity);
                return result;
            }
            else
                return null; 

        }

        public Company GetByName(string name)
        {
            var entity = CompanyInfoProvider.GetCompanies().Where(c => c.CompanyName.ToLower().Trim() == name.ToLower().Trim()).FirstOrDefault();
            if (entity != null)
            {
                var result = Mapper.Map<CompanyInfo, Company>(entity);
                return result;
            }
            else
                return null;
        }

        public IEnumerable<Company> GetAll()
        {
            var entity = CompanyInfoProvider.GetCompanies().ToList();
            if (entity != null)
            {
                var result = Mapper.Map<List<Company>>(entity);
                return result;
            }
            else
                return null;
        }

        public bool CompanyExists(string name)
        {
            //Check if company name exists in database
            var exists = CompanyInfoProvider.GetCompanies().Count(c => c.CompanyName.ToLower().Trim() == name.ToLower().Trim()) > 0;
            return exists;
        }
    }
}