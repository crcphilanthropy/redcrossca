﻿using AutoMapper;
using CMS.CustomTables;
using CMS.DocumentEngine;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.Taxonomy;
using CRCPhotosForMedia;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class CatalogRepository : ICatalog
    {
        //private readonly IMappingEngine _mapper;
        public CatalogRepository()
        {

        }

        public Category GetCategoryById(int id, string culture)
        {
            var entity = CategoryInfoProvider.GetCategoryInfo(id);
            if (entity != null)
            {
                var result = Mapper.Map<CategoryInfo, Category>(entity);
                //Get the culture appropriate display name
                result.CategoryDisplayName = this.GetDisplayName(result.CategoryDisplayName, culture);
                result.CategoryName = this.GetName(result.CategoryDisplayName);
                return result;
            }
            else
                return null;

        }

        public Region GetRegionById(int id, string culture)
        {
            var entity = CategoryInfoProvider.GetCategoryInfo(id);
            if (entity != null)
            {
                var result = Mapper.Map<CategoryInfo, Region>(entity);
                //Get the culture appropriate display name
                result.CategoryDisplayName = GetDisplayName(result.CategoryDisplayName, culture);
                result.CategoryName = this.GetName(result.CategoryDisplayName);
                return result;
            }
            else
                return null;

        }

        public List<int> GetCategoryIds(string categories)
        {
            var result = new List<int>();
            if (!string.IsNullOrEmpty(categories))
            {
                var temp = categories.Split(';');
                if (temp != null || temp.Count() > 0)
                {
                    result = temp.Select(int.Parse).ToList();
                }
                return result;
            }
            else
            {
                return result;
            }

        }
        public List<int> GetRegionIds(string regions)
        {
            var result = new List<int>();
            if (!string.IsNullOrEmpty(regions))
            {
                var temp = regions.Split(';');
                if (temp != null || temp.Count() > 0)
                {
                    //result = Array.ConvertAll(temp, int.Parse).ToList();
                    result = temp.Select(int.Parse).ToList();
                }
                return result;
            }
            else
            {
                return result;
            }

        }

        public IEnumerable<Category> GetAllCategories(string culture)
        {
            var type = "Topics";
            //get parent Id for Topics categories
            var query = this.QueryBuilder(type).FirstOrDefault();
            //Get Topics that should be excluded
            var filter = this.GetQuery(type);
            //Get filtered categories
            var entity = CategoryInfoProvider.GetCategories().Where(c =>
                          c.CategoryParentID.Equals(query) 
                          && !filter.Contains(c.CategoryID)).ToList();

            if (entity != null)
            {
                var result = Mapper.Map<List<Category>>(entity);
                foreach (var item in result)
                {
                    item.CategoryDisplayName = this.GetDisplayName(item.CategoryDisplayName, culture);
                    item.CategoryName = this.GetName(item.CategoryDisplayName);
                }
                result = result.OrderBy(c => c.CategoryDisplayName).ToList();
                return result;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Region> GetAllRegions(string culture)
        {
            var query = this.GetQuery("Regions");
            var entity = CategoryInfoProvider.GetCategories().Where(c => query.Contains(c.CategoryParentID)).ToList();
            var result = new List<Region>();
            if (entity != null)
            {
                var tempRegionList = new List<CategoryInfo>();
                var tempProvinceList = new List<CategoryInfo>();
                //Split entity list by object category category name
                foreach (var item in entity)
                {
                    //assign split list values to 2 temp lists
                    switch (item.CategoryDisplayName.Contains("Regions"))
                    {
                        case true:
                            tempRegionList.Add(item);
                            break;
                        case false:
                            tempProvinceList.Add(item);
                            break;
                        default:
                            break;
                    }
                }
                //map both lists to 2 temp result region objects
                //add list to final result object: add provinces first then regions
                var tempProvinceResult = Mapper.Map<List<Region>>(tempProvinceList);
                var tempResult1 = new List<Region>();
                foreach (var item2 in tempProvinceResult)
                {
                    item2.CategoryDisplayName = this.GetDisplayName(item2.CategoryDisplayName, culture);
                    item2.CategoryName = this.GetName(item2.CategoryDisplayName);
                    tempResult1.Add(item2);
                    tempResult1 = tempResult1.OrderBy(c => c.CategoryDisplayName).ToList();
                }
                foreach (var r in tempResult1)
                {
                    result.Add(r);
                }
                var tempRegionResult = Mapper.Map<List<Region>>(tempRegionList);
                var tempResult2 = new List<Region>();
                foreach (var item in tempRegionResult)
                {
                    item.CategoryDisplayName = this.GetDisplayName(item.CategoryDisplayName, culture);
                    item.CategoryName = this.GetName(item.CategoryDisplayName);
                    tempResult2.Add(item);
                    tempResult2 = tempResult2.OrderBy(c => c.CategoryDisplayName).ToList();
                }
                foreach (var r in tempResult2)
                {
                    result.Add(r);
                }

                return result;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Category> GetCategoriesById(List<int> ids, string culture)
        {
            var results = new List<Category>();
            if (ids.Count() != 0 || ids != null)
            {
                foreach (var item in ids)
                {
                    var category = this.GetCategoryById(item, culture);
                    //Get the culture appropriate display name
                    if (category != null)
                    {
                        category.CategoryDisplayName = this.GetDisplayName(category.CategoryDisplayName, culture);
                        category.CategoryName = this.GetName(category.CategoryDisplayName);
                        results.Add(category);
                    }
                }
            }

            return results;

        }
        public IEnumerable<Region> GetRegionsById(List<int> ids, string culture)
        {
            var results = new List<Region>();
            if (ids.Count() != 0 || ids != null)
            {
                foreach (var item in ids)
                {
                    var category = this.GetRegionById(item, culture);
                    //Get the culture appropriate display name
                    if (category != null)
                    {
                        category.CategoryDisplayName = this.GetDisplayName(category.CategoryDisplayName, culture);
                        category.CategoryName = this.GetName(category.CategoryDisplayName);
                        results.Add(category);
                    }
                }
                return results;
            }

            return results;


        }

        public IEnumerable<Category> GetVideoCategories(TreeNode item, string culture)
        {
            var categories = new List<Category>();
            if (item != null && item.GetValue("Categories") != null)
            {
                if (!string.IsNullOrEmpty(item.GetValue("Categories").ToString()))
                {
                    var tempcat = this.GetCategoryIds(item.GetValue("Categories").ToString());
                    categories = this.GetCategoriesById(tempcat, culture).ToList();
                }
            }

            return categories;
        }

        public IEnumerable<Region> GetVideoRegions(TreeNode item, string culture)
        {
            var regions = new List<Region>();
            if (item != null && item.GetValue("Regions") != null)
            {
                if (!string.IsNullOrEmpty(item.GetValue("Regions").ToString()))
                {
                    var tempreg = this.GetRegionIds(item.GetValue("Regions").ToString());
                    regions = this.GetRegionsById(tempreg, culture).ToList();
                }
            }

            return regions;
        }

        public IEnumerable<Category> GetPhotoCategories(PhotoInfo entity, string culture)
        {
            var categories = new List<Category>();
            if (entity != null && !string.IsNullOrEmpty(entity.Categories))
            {
                var tempcat = this.GetCategoryIds(entity.Categories);
                categories = this.GetCategoriesById(tempcat, culture).ToList();
            }
            return categories;
        }

        public IEnumerable<Region> GetPhotoRegions(PhotoInfo entity, string culture)
        {
            var regions = new List<Region>();
            if (entity != null && !string.IsNullOrEmpty(entity.Regions))
            {
                var tempreg = this.GetRegionIds(entity.Regions);
                regions = this.GetRegionsById(tempreg, culture).ToList();
            }
            return regions;
        }

        public MediaAlertForm GetMediaAlertFormData(CustomTableItem item, string tablename)
        {
            if (item != null && !string.IsNullOrEmpty(tablename))
            {
                var id = ValidationHelper.GetGuid(item.GetValue("ItemGUID"), Guid.Empty);
                // Gets the first custom table record whose value in the 'Name' field is equal to "data.Name"
                //Move this to catalog repository
                if (id != Guid.Empty)
                {
                    CustomTableItem form = CustomTableItemProvider.GetItems(tablename)
                                                                    .WhereEquals("ItemGUID", id)
                                                                    .FirstObject;
                    if (form != null)
                    {
                        var result = Mapper.Map<CustomTableItem, MediaAlertForm>(form, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.ItemID = ValidationHelper.GetInteger(form.GetValue("ItemId"), int.MinValue);
                                dst.Name = ValidationHelper.GetString(form.GetValue("Name"), string.Empty);
                                dst.Email = ValidationHelper.GetString(form.GetValue("Email"), string.Empty);
                            });
                        });
                        return result;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

        }

        public Form GetFormData(CustomTableItem item, string tablename)
        {
            if (item != null && !string.IsNullOrEmpty(tablename))
            {
                var id = ValidationHelper.GetGuid(item.GetValue("ItemGUID"), Guid.Empty);
                // Gets the first custom table record whose value in the 'Name' field is equal to "data.Name"
                //Move this to catalog repository
                if (id != Guid.Empty)
                {
                    CustomTableItem form = CustomTableItemProvider.GetItems(tablename)
                                                                    .WhereEquals("ItemGUID", id)
                                                                    .FirstObject;
                    if (form != null)
                    {
                        var result = Mapper.Map<CustomTableItem, Form>(form, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Name = ValidationHelper.GetString(form.GetValue("Name"), string.Empty);
                                dst.Email = ValidationHelper.GetString(form.GetValue("Email"), string.Empty);
                                dst.Publication = ValidationHelper.GetString(form.GetValue("Publication"), string.Empty);
                            });
                        });
                        return result;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

        }


        #region Helpers
        //Build query for retreiving region parent ids
        private IQueryable<int> GetQuery(string type)
        {
            IQueryable<int> query = null;
            //Get the category info based on the type
            var temp = this.GetCategoryInfo(type);
            //Build filter list string
            var filter = this.BuildFilter(temp, type);
            //Make query to get categories
            if (type.ToLower() == "regions")
            {
                query = (from x in CategoryInfoProvider.GetCategories()
                            where filter.Contains(x.CategoryName)
                            select x.CategoryID)
                            .Union(from x in CategoryInfoProvider.GetCategories()
                                   where x.CategoryName.ToLower().Equals(type.ToLower())
                                   select x.CategoryID);
            }
            else if (type.ToLower() == "topics")
            {
                query = from x in CategoryInfoProvider.GetCategories()
                            where filter.Contains(x.CategoryName)
                            select x.CategoryID;
            }
            return query;
        }

        private IQueryable<int> QueryBuilder(string type)
        {
            var category = from x in CategoryInfoProvider.GetCategories()
                           where x.CategoryName == type
                           select x.CategoryID;
            return category;
        }

        private List<CategoryInfo> GetCategoryInfo(string type)
        {
            var temp = new List<CategoryInfo>();
            if (!string.IsNullOrEmpty(type))
            {
                if (type.ToLower() == "regions")
                {
                    var category = this.QueryBuilder(type);
                    temp = CategoryInfoProvider.GetCategories().Where(c => category.Contains(c.CategoryParentID)).ToList();
                }
                else if (type.ToLower() == "topics")
                {
                    var category = QueryBuilder(type).FirstOrDefault();
                    temp = CategoryInfoProvider.GetCategories().Where(c => c.CategoryParentID.Equals(category)).ToList();
                }
            }
            
            return temp;
        }

        private List<string> BuildFilter(List<CategoryInfo> list, string type)
        {
            var filter = new List<string>();
            if (list != null && list.Count() > 0 && !string.IsNullOrEmpty(type))
            {
                if (type.ToLower() == "topics")
                {
                    var temp = new string[] { "800104342", "800102597", "RefugeeArrival" };
                    foreach (var item in list)
                    {
                        if (temp.Contains(item.CategoryName))
                        {
                            filter.Add(item.CategoryName);
                        }
                    }
                }
                else if (type.ToLower() == "regions")
                {
                    foreach (var item in list)
                    {
                        filter.Add(item.CategoryName);
                    }
                }
            }
            
            return filter;
        }

        //Method to get the actual display name
        private string GetDisplayName(string codedName, string culture)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(codedName))
            {
                result = ResHelper.LocalizeString(codedName, culture);
            }
            return result;
        }

        private string GetName(string displayname)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(displayname))
            {
                result = URLHelper.GetSafeFileName(displayname, CMS.SiteProvider.SiteContext.CurrentSiteName).Replace(",","").ToLower();
            }
            return result;
        }

        #endregion
    }
}