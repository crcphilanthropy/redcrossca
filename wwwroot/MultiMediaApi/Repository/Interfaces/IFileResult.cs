﻿using CMS.MediaLibrary;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface IFileResult
    {
        MediaLibraryInfo GetLibrary(string libarycode);
        FileResult GetFile(MediaLibraryInfo Library, string filePath);
    }
}
