﻿using MultiMediaApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface IPhoto
    {
        //void Create(Photo photo);
        //void Update(Photo photo);
        //void Delete(int id);
        IEnumerable<Photo> GetAll(string culture);
        Photo Get(int id, string culture);
        IEnumerable<Photo> PhotoQuery(List<int> categories, List<int> regions, string culture);
        
    }
}
