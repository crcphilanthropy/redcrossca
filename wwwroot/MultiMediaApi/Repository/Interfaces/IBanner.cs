﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface IBanner
    {
        void Create(Banner banner);
        void Update(Banner banner);
        void Delete(int id);

        Banner GetById(int id, string culture);
        IEnumerable<Banner> GetAll();
        IEnumerable<Banner> GetBannersBySet(int id, string culture);

    }
}
