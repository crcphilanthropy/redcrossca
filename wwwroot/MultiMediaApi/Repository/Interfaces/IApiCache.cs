﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiMediaApi.Repository.Interfaces
{
    public interface IApiCache
    {
        Tags GetCacheTags(Tags tags);
    }
}
