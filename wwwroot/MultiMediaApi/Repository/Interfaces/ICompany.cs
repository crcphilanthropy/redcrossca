﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface ICompany
    {
        void Create(Company company);
        Company GetById(int id);
        Company GetByName(string name);
        IEnumerable<Company> GetAll();
        bool CompanyExists(string name);
    }
}
