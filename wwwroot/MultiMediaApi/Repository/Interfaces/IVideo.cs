﻿using CMS.DataEngine;
using CMS.DocumentEngine;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface IVideo
    {
        //void Create(Video video);
        //void Update(Video video);
        //void Delete(int id);
        IEnumerable<Video> GetAll(string path, string culture);
        IEnumerable<Video> GetRelated(string path, string culture);
        Video GetById(int id, string culture);
        Video GetByGuid(Guid id);
        IEnumerable<Video> VideoQuery(List<int> categories, List<int> regions, string path, string culture);
        IEnumerable<Video> RelatedVideoQuery(Guid id, List<int> categories, List<int> regions, string path, DateTime? date, string culture);

    }
}
