﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiMediaApi.Repository
{
    public interface IForm
    {
        MediaAlertForm SaveAlert(string name, MediaAlertForm form);
        Form SavePhotoForm(Form data);
        Form SaveVideoForm(Form data);
    }
}
