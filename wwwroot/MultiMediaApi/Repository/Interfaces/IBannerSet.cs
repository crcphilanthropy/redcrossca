﻿using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMediaApi.Repository
{
    public interface IBannerSet
    {
        BannerSet GetById(int id, string culture);
        IEnumerable<BannerSet> GetAll(string culture);

        void Create(BannerSet banner);
        void Update(BannerSet banner);
    }
}
