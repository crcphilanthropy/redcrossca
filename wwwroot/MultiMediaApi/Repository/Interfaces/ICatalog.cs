﻿using CMS.CustomTables;
using CMS.DocumentEngine;
using CRCPhotosForMedia;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiMediaApi.Repository
{
    public interface ICatalog
    {
        IEnumerable<Category> GetAllCategories(string culture);
        IEnumerable<Region> GetAllRegions(string culture);

        Category GetCategoryById(int id, string culture);
        Region GetRegionById(int id, string culture);

        IEnumerable<Category> GetCategoriesById(List<int> ids, string culture);
        IEnumerable<Region> GetRegionsById(List<int> ids, string culture);

        List<int> GetCategoryIds(string categories);
        List<int> GetRegionIds(string regions);

        IEnumerable<Category> GetVideoCategories(TreeNode item, string culture);
        IEnumerable<Region> GetVideoRegions(TreeNode item, string culture);
        IEnumerable<Category> GetPhotoCategories(PhotoInfo entity, string culture);
        IEnumerable<Region> GetPhotoRegions(PhotoInfo entity, string culture);

        Form GetFormData(CustomTableItem item, string tablename);
        MediaAlertForm GetMediaAlertFormData(CustomTableItem item, string tablename);

    }
}
