﻿using CMS.CustomTables;
using CMS.DataEngine;
using CMS.Helpers;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class FormRepository : IForm
    {
        //private readonly string _alerttableclassname;
        private readonly string _phototableclassname;
        private readonly string _videotableclassname;
        private readonly ICatalog _catalog;
        public FormRepository()
        {
           // _alerttableclassname = "CRC.MediaAlertRegistrants";
            _phototableclassname = "CRC.MultiMediaPhotosForm";
            _videotableclassname = "CRC.MultiMediaVideosForm";
            _catalog = new CatalogRepository();
        }
        public MediaAlertForm SaveAlert(string tableClassName, MediaAlertForm data)
        {
            DataClassInfo table = DataClassInfoProvider.GetDataClassInfo(tableClassName.ToLower());
            if (table != null)
            {
                // Creates a new custom table instance
                CustomTableItem formItem = CustomTableItem.New(tableClassName.ToLower());
                // Sets the values for the fields of the custom table (ItemText in this case)
                formItem.SetValue("Name", data.Name);
                formItem.SetValue("Email", data.Email);
                // Save the new custom table record into the database
                formItem.Insert();
                //Get results of the saved data mapped to Form DTO
                var result = _catalog.GetMediaAlertFormData(formItem, tableClassName.ToLower());
                return result;
            }
            else
                return null;
        }

        public Form SavePhotoForm(Form data)
        {
            DataClassInfo table = DataClassInfoProvider.GetDataClassInfo(_phototableclassname.ToLower());
            if (table != null)
            {
                // Creates a new custom table item
                CustomTableItem formItem = CustomTableItem.New(_phototableclassname.ToLower());

                // Sets the values for the fields of the custom table (ItemText in this case)
                formItem.SetValue("Name", data.Name);
                formItem.SetValue("Email", data.Email);
                formItem.SetValue("Publication", data.Publication);

                // Save the new custom table record into the database
                formItem.Insert();
                //Get results of the saved data mapped to Form DTO
                var result = _catalog.GetFormData(formItem, _phototableclassname.ToLower());
                
                return result;
            }
            else
                return null;
        }

        public Form SaveVideoForm(Form data)
        {
            DataClassInfo table = DataClassInfoProvider.GetDataClassInfo(_videotableclassname.ToLower());
            if (table != null)
            {
                // Creates a new custom table item
                CustomTableItem formItem = CustomTableItem.New(_videotableclassname.ToLower());

                // Sets the values for the fields of the custom table (ItemText in this case)
                formItem.SetValue("Name", data.Name);
                formItem.SetValue("Email", data.Email);
                formItem.SetValue("Publication", data.Publication);
                formItem.SetValue("DetailId", ValidationHelper.GetInteger(data.DetailId, 0));

                // Save the new custom table record into the database
                formItem.Insert();
                //Get results of the saved data mapped to Form DTO
                var result = _catalog.GetFormData(formItem, _videotableclassname.ToLower());
                return result;
            }
            else
                return null;
        }
    }
}