﻿using AutoMapper;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class VideoRepository : IVideo
    {
        //private readonly IMappingEngine _mapper;
        private readonly ICatalog _catalog;
        private readonly string[] _columns;
        private readonly CultureInfo culture;

        public VideoRepository()
        {
            _catalog = new CatalogRepository();
            _columns = new string[] { "VideoDetailID", "VideoEmbedID", "Credits",
                                      "VideoSummary", "VideoLength", "Subtitles",
                                      "Transcript", "Categories", "Regions", "Thumbnail",
                                       "DownloadAvailable", "VideoTitle", "DocumentGUID",
                                       "AltText", "DownloadLink", "DocumentUrlPath", "DocumentCreatedWhen", "DocumentCulture" };

            culture = LocalizationContext.CurrentCulture;
        }

        #region CRUD 
        //public void Create(Video v)
        //{
        //    context.Videos.Add(v);
        //    context.SaveChanges();
        //}

        //public void Update(Video v)
        //{
        //    context.Entry(v);
        //    context.SaveChanges();
        //}

        //public void Delete(int id)
        //{
        //    var video = context.Videos.Find(id);
        //    var result = context.Videos.Remove(video);
        //    context.SaveChanges();
        //}
        #endregion

        public Video GetById(int id, string currentCulture)
        {
            var page = DocumentHelper.GetDocuments("CRC.VideoDetail")
                                .OnCurrentSite()
                                .Columns(_columns)
                                .Where("VideoDetailID", QueryOperator.Equals, id)
                                .And()
                                .Where("DocumentCulture", QueryOperator.Like, currentCulture.ToLower())
                                .Culture(currentCulture.ToLower())
                                .CombineWithDefaultCulture()
                                .Published()
                                .LatestVersion()
                                .TopN(1)
                                .FirstObject;
            if (page != null)
            {
                var categories = _catalog.GetVideoCategories(page, currentCulture);
                var regions = _catalog.GetVideoRegions(page, currentCulture);
                var vidId = ValidationHelper.GetInteger(page.GetValue("VideoDetailID"), int.MinValue);
                var embed = ValidationHelper.GetString(page.GetValue("VideoEmbedID"), string.Empty);
                var credits = ValidationHelper.GetString(page.GetValue("Credits"), string.Empty);
                var summary = ValidationHelper.GetString(page.GetValue("VideoSummary"), string.Empty);
                var length = ValidationHelper.GetInteger(page.GetValue("VideoLength"), int.MinValue);
                var subtitles = ValidationHelper.GetBoolean(page.GetValue("Subtitles"), false);
                var download = ValidationHelper.GetBoolean(page.GetValue("DownloadAvailable"), false);
                var transcript = ValidationHelper.GetString(page.GetValue("Transcript"), string.Empty);
                var thumbnail = ValidationHelper.GetString(page.GetValue("Thumbnail"), string.Empty);
                var videoTitle = ValidationHelper.GetString(page.GetValue("VideoTitle"), string.Empty);
                var altText = ValidationHelper.GetString(page.GetValue("AltText"), string.Empty);
                var downloadLink = ValidationHelper.GetString(page.GetValue("DownloadLink"), string.Empty);
                var detailsPageUrl = ValidationHelper.GetString(page.GetValue("DocumentUrlPath"), string.Empty);

                var video = Mapper.Map<TreeNode, Video>(page, dest =>
                {
                    dest.AfterMap((src, dst) =>
                    {
                        dst.VideoDetailID = vidId;
                        dst.VideoEmbedID = embed;
                        dst.Credits = credits;
                        dst.VideoSummary = summary;
                        dst.VideoLength = length;
                        dst.Subtitles = subtitles;
                        dst.DownloadAvailable = download;
                        dst.Transcript = transcript;
                        dst.Categories = categories;
                        dst.Regions = regions;
                        dst.Thumbnail = thumbnail;
                        dst.VideoTitle = videoTitle;
                        dst.AltText = altText;
                        dst.DownloadLink = downloadLink;
                        dst.DetailsPageLink = detailsPageUrl;

                    });
                });

                return video;
            }
            else
                return null;

        }

        public Video GetByGuid(Guid id)
        {
            var page = DocumentHelper.GetDocuments("CRC.VideoDetail")
                                .OnCurrentSite()
                                .Columns(_columns)
                                .Where("DocumentGUID", QueryOperator.Equals, id)
                                .Culture(culture.CultureCode.ToLower())
                                .CombineWithDefaultCulture()
                                .Published()
                                .LatestVersion()
                                .TopN(1)
                                .FirstObject;
            if (page != null)
            {
                var categories = _catalog.GetVideoCategories(page, culture.CultureCode.ToLower());
                var regions = _catalog.GetVideoRegions(page, culture.CultureCode.ToLower());
                var vidId = ValidationHelper.GetInteger(page.GetValue("VideoDetailID"), int.MinValue);
                var embed = ValidationHelper.GetString(page.GetValue("VideoEmbedID"), string.Empty);
                var credits = ValidationHelper.GetString(page.GetValue("Credits"), string.Empty);
                var summary = ValidationHelper.GetString(page.GetValue("VideoSummary"), string.Empty);
                var length = ValidationHelper.GetInteger(page.GetValue("VideoLength"), int.MinValue);
                var subtitles = ValidationHelper.GetBoolean(page.GetValue("Subtitles"), false);
                var download = ValidationHelper.GetBoolean(page.GetValue("DownloadAvailable"), false);
                var transcript = ValidationHelper.GetString(page.GetValue("Transcript"), string.Empty);
                var thumbnail = ValidationHelper.GetString(page.GetValue("Thumbnail"), string.Empty);
                var videoTitle = ValidationHelper.GetString(page.GetValue("VideoTitle"), string.Empty);
                var altText = ValidationHelper.GetString(page.GetValue("AltText"), string.Empty);
                var downloadLink = ValidationHelper.GetString(page.GetValue("DownloadLink"), string.Empty);
                var detailsPageUrl = ValidationHelper.GetString(page.GetValue("DocumentUrlPath"), string.Empty);

                var video = Mapper.Map<TreeNode, Video>(page, dest =>
                {
                    dest.AfterMap((src, dst) =>
                    {
                        dst.VideoDetailID = vidId;
                        dst.VideoEmbedID = embed;
                        dst.Credits = credits;
                        dst.VideoSummary = summary;
                        dst.VideoLength = length;
                        dst.Subtitles = subtitles;
                        dst.DownloadAvailable = download;
                        dst.Transcript = transcript;
                        dst.Categories = categories;
                        dst.Regions = regions;
                        dst.Thumbnail = thumbnail;
                        dst.VideoTitle = videoTitle;
                        dst.AltText = altText;
                        dst.DownloadLink = downloadLink;
                        dst.DetailsPageLink = detailsPageUrl;
                    });
                });

                return video;
            }
            else
                return null;

        }


        public IEnumerable<Video> GetAll(string path, string currentCulture)
        {          
            var results = new List<Video>();
            var pages = DocumentHelper.GetDocuments("CRC.VideoDetail")
                                .Path(path + "%")
                                .OnCurrentSite()
                                .Columns(_columns)
                                .Where("DocumentCulture", QueryOperator.Like, currentCulture.ToLower())
                                .OrderByDescending("DocumentCreatedWhen")
                                .Culture(currentCulture.ToLower())
                                .CombineWithDefaultCulture()
                                .Published()
                                .LatestVersion()
                                .TypedResult;

            if (pages != null)
            {
                foreach (var item in pages)
                {
                    var detailsPageUrl = ValidationHelper.GetString(item.GetValue("DocumentUrlPath"), string.Empty);
                    var categories = _catalog.GetVideoCategories(item, currentCulture);
                    var regions = _catalog.GetVideoRegions(item, currentCulture);
                    var id = ValidationHelper.GetInteger(item.GetValue("VideoDetailID"), int.MinValue);
                    var embed = item.GetValue("VideoEmbedID").ToString();
                    var credits = item.GetValue("Credits").ToString();
                    var summary = item.GetValue("VideoSummary").ToString();
                    var length = ValidationHelper.GetInteger(item.GetValue("VideoLength"), int.MinValue);
                    var subtitles = ValidationHelper.GetBoolean(item.GetValue("Subtitles"), false);
                    var download = ValidationHelper.GetBoolean(item.GetValue("DownloadAvailable"), false);
                    var transcript = ValidationHelper.GetString(item.GetValue("Transcript"), string.Empty);
                    var thumbnail = ValidationHelper.GetString(item.GetValue("Thumbnail"), string.Empty);
                    var videoTitle = ValidationHelper.GetString(item.GetValue("VideoTitle"), string.Empty);
                    var altText = ValidationHelper.GetString(item.GetValue("AltText"), string.Empty);

                    var video = Mapper.Map<TreeNode, Video>(item, dest =>
                    {
                        dest.AfterMap((src, dst) =>
                        {
                            dst.VideoDetailID = id;
                            dst.VideoEmbedID = embed;
                            dst.Credits = credits;
                            dst.VideoSummary = summary;
                            dst.VideoLength = length;
                            dst.Subtitles = subtitles;
                            dst.DownloadAvailable = download;
                            dst.Transcript = transcript;
                            dst.Categories = categories;
                            dst.Regions = regions;
                            dst.Thumbnail = thumbnail;
                            dst.VideoTitle = videoTitle;
                            dst.AltText = altText;
                            dst.DetailsPageLink = detailsPageUrl;
                        });
                    });
                    results.Add(video);

                }
                return results;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Video> GetRelated(string path, string currentCulture)
        {
            var results = new List<Video>();
            var pages = DocumentHelper.GetDocuments("CRC.VideoDetail")
                                .Path(path + "%")
                                .OnCurrentSite()
                                .Columns(_columns)
                                .Where("DocumentCulture", QueryOperator.Like, currentCulture.ToLower())
                                .OrderByDescending("DocumentCreatedWhen")
                                .Culture(currentCulture.ToLower())
                                .CombineWithDefaultCulture()
                                .Published()
                                .LatestVersion()
                                .TypedResult;

            if (pages != null)
            {
                foreach (var item in pages)
                {
                    var detailsPageUrl = ValidationHelper.GetString(item.GetValue("DocumentUrlPath"), string.Empty);
                    var categories = _catalog.GetVideoCategories(item, currentCulture);
                    var regions = _catalog.GetVideoRegions(item, currentCulture);
                    var id = ValidationHelper.GetInteger(item.GetValue("VideoDetailID"), int.MinValue);
                    var embed = item.GetValue("VideoEmbedID").ToString();
                    var credits = item.GetValue("Credits").ToString();
                    var summary = item.GetValue("VideoSummary").ToString();
                    var length = ValidationHelper.GetInteger(item.GetValue("VideoLength"), int.MinValue);
                    var subtitles = ValidationHelper.GetBoolean(item.GetValue("Subtitles"), false);
                    var download = ValidationHelper.GetBoolean(item.GetValue("DownloadAvailable"), false);
                    var transcript = ValidationHelper.GetString(item.GetValue("Transcript"), string.Empty);
                    var thumbnail = ValidationHelper.GetString(item.GetValue("Thumbnail"), string.Empty);
                    var videoTitle = ValidationHelper.GetString(item.GetValue("VideoTitle"), string.Empty);
                    var altText = ValidationHelper.GetString(item.GetValue("AltText"), string.Empty);
                    var created = ValidationHelper.GetDate(item.GetValue("DocumentCreatedWhen"), DateTime.MinValue);
                    var docid = ValidationHelper.GetGuid(item.GetValue("DocumentGUID"), Guid.Empty);

                    var video = Mapper.Map<TreeNode, Video>(item, dest =>
                    {
                        dest.AfterMap((src, dst) =>
                        {
                            dst.VideoDetailID = id;
                            dst.VideoEmbedID = embed;
                            dst.Credits = credits;
                            dst.VideoSummary = summary;
                            dst.VideoLength = length;
                            dst.Subtitles = subtitles;
                            dst.DownloadAvailable = download;
                            dst.Transcript = transcript;
                            dst.Categories = categories;
                            dst.Regions = regions;
                            dst.Thumbnail = thumbnail;
                            dst.VideoTitle = videoTitle;
                            dst.AltText = altText;
                            dst.DetailsPageLink = detailsPageUrl;
                            dst.DocumentCreatedWhen = created;
                            dst.DocumentGuid = docid;
                        });
                    });
                    results.Add(video);

                }
                return results;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Video> VideoQuery(List<int> categories, List<int> regions, string path, string currentCulture)
        {
            var search = this.GetAll(path, currentCulture).AsQueryable().Where(p =>
                            p.Categories.Any(x => categories.Contains(x.CategoryID)) ||
                            p.Regions.Any(x => regions.Contains(x.CategoryID))
                            ).ToList();

            return search;
        }

        public IEnumerable<Video> RelatedVideoQuery(Guid id, List<int> categories, List<int> regions, string path, DateTime? date, string currentCulture)
        {
            var search = this.GetRelated(path, currentCulture).AsQueryable().Where(p =>
                            p.DocumentGuid != id &&
                            (p.Regions.Any(x => regions.Contains(x.CategoryID)) ||
                            p.DocumentCreatedWhen.Value.Date.Equals(date.Value.Date) ||
                            p.Categories.Any(x => categories.Contains(x.CategoryID)))
                            ).Take(4).ToList();

            return search;
        }

        #region helpers

        private bool CheckVideoBoolValue(TreeNode item)
        {
            var property = item.GetValue("Subtitles").ToString();
            bool returnProperty;
            if (bool.TryParse(property, out returnProperty))
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckVideoIdValue(TreeNode item)
        {
            var property = item.GetValue("VideoDetailID").ToString();
            int returnValue;
            if (int.TryParse(property, out returnValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}