﻿using AutoMapper;
using CMS.Helpers;
using CMS.Localization;
using CRCWebBanners;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class BannerSetRepository : IBannerSet
    {
        private CultureCode culturecode;
        public BannerSetRepository()
        {

        }

        public BannerSet GetById(int id, string culture)
        {
            var result = new BannerSet();
            var entity = BannerSetInfoProvider.GetBannerSetInfo(id);

            if (entity != null)
            {
                var showEn = entity.ShowEnglish;
                var archive = entity.Archive;
                var showFr = entity.ShowFrench;
                if (!string.IsNullOrEmpty(culture) && Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
                {
                    if ((showEn && culturecode.Equals(CultureCode.enca)) && !archive)
                    {
                        result = Mapper.Map<BannerSetInfo, BannerSet>(entity, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Title = this.GetCultureTitle(entity, culture);
                                dst.Description = this.GetCultureDescription(entity, culture);
                            });
                        });
                    }
                    else if ((showFr && culturecode.Equals(CultureCode.frca)) && !archive)
                    {
                        result = Mapper.Map<BannerSetInfo, BannerSet>(entity, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Title = this.GetCultureTitle(entity, culture);
                                dst.Description = this.GetCultureDescription(entity, culture);
                            });
                        });
                    }
                }

                return result;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<BannerSet> GetAll(string culture)
        {
            if (Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var entity = BannerSetInfoProvider.GetBannerSets()
            .Where(b => !b.Archive && (
                    (b.ShowEnglish && culturecode.Equals(CultureCode.enca)) || (b.ShowFrench && culturecode.Equals(CultureCode.frca)))
                  )
            .OrderBy(b => b.BannerSetOrder)
            .ToList();

                var result = new List<BannerSet>();
                if (entity != null)
                {
                    foreach (var item in entity)
                    {
                        var bannerset = Mapper.Map<BannerSetInfo, BannerSet>(item, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Title = this.GetCultureTitle(item, culture);
                                dst.Description = this.GetCultureDescription(item, culture);
                            });
                        });

                        result.Add(bannerset);
                    }

                    return result;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;


        }

        public void Create(BannerSet set)
        {
            var entity = Mapper.Map<BannerSet, BannerSetInfo>(set);
            BannerSetInfoProvider.SetBannerSetInfo(entity);
        }

        public void Update(BannerSet set)
        {
            var entity = Mapper.Map<BannerSet, BannerSetInfo>(set);
            BannerSetInfoProvider.SetBannerSetInfo(entity);
        }


        #region helpers
        private string GetCultureTitle(BannerSetInfo item, string culture)
        {
            var title = string.Empty;
            if (item != null)
            {
                if (culture.ToLower() == "en-ca")
                    title = item.Title;
                else if (culture.ToLower() == "fr-ca")
                    title = item.Title_FR;
            }

            return ValidationHelper.GetString(title, string.Empty);
        }

        private string GetCultureDescription(BannerSetInfo item, string culture)
        {
            var description = string.Empty;
            if (item != null)
            {
                if (culture.ToLower() == "en-ca")
                    description = item.Description;
                else if (culture.ToLower() == "fr-ca")
                    description = item.Description_FR;
            }

            return ValidationHelper.GetString(description, string.Empty);
        }
        #endregion
    }
}