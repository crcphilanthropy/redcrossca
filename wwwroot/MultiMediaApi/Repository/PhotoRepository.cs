﻿using AutoMapper;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CRCPhotosForMedia;
using MultiMediaApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class PhotoRepository : IPhoto
    {
        //private readonly IMappingEngine _mapper;
        private readonly ICatalog _catalog;
        private CultureCode culturecode;

        public PhotoRepository()
        {
            _catalog = new CatalogRepository();
        }
        #region CUD for testing
        //public void Create(Photo p)
        //{
        //    context.Photos.Add(p);
        //    context.SaveChanges();
        //}

        //public void Update(Photo p)
        //{
        //    context.Entry(p);
        //    context.SaveChanges();
        //}

        //public void Delete(int id)
        //{
        //    var photo = context.Photos.Find(id);
        //    var result = context.Photos.Remove(photo);
        //    context.SaveChanges();
        //}

        #endregion

        public Photo Get(int id, string culture)
        {
            //find out what gets sent back from photo info image
            var entity = PhotoInfoProvider.GetPhotoInfo(id);
            var result = new Photo();
            if (entity != null)
            {
                var showEn = entity.ShowEnglish;
                var archive = entity.Archive;
                var showFr = entity.ShowFrench;
                if (!string.IsNullOrEmpty(culture) && Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
                {
                    var categories = _catalog.GetPhotoCategories(entity, culture);
                    var regions = _catalog.GetPhotoRegions(entity, culture);
                    var title = this.GetCultureTitle(entity, culture);
                    var description = this.GetCultureDescription(entity, culture);
                    var alt = this.GetCultureAltText(entity, culture);
                    var credits = this.GetCultureCredits(entity, culture);
                    //Map result plus categories and regions to photo DTO
                    if ((showEn && culturecode.Equals(CultureCode.enca)) && !archive)
                    {
                        result = Mapper.Map<PhotoInfo, Photo>(entity, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Categories = categories;
                                dst.Regions = regions;
                                dst.Title = title;
                                dst.Description = description;
                                dst.AltText = alt;
                                dst.PhotoCredits = credits;
                            });
                        });
                    }
                    else if ((showFr && culturecode.Equals(CultureCode.frca)) && !archive)
                    {
                        result = Mapper.Map<PhotoInfo, Photo>(entity, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Categories = categories;
                                dst.Regions = regions;
                                dst.Title = title;
                                dst.Description = description;
                                dst.AltText = alt;
                                dst.PhotoCredits = credits;
                            });
                        });
                    }
                    return result;
                }
                else
                    return null;
                
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Photo> GetAll(string culture)
        {
            if (Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var entity = PhotoInfoProvider.GetPhotoes()
                .Where(b => !b.Archive && (
                    (b.ShowEnglish && culturecode.Equals(CultureCode.enca)) || (b.ShowFrench && culturecode.Equals(CultureCode.frca)))
                  )
                .OrderByDescending(p => p.DateCreated)
                .ToList();
                var result = new List<Photo>();
                if (entity != null)
                {
                    foreach (var item in entity)
                    {
                        var categories = _catalog.GetPhotoCategories(item, culture);
                        var regions = _catalog.GetPhotoRegions(item, culture);
                        var title = this.GetCultureTitle(item, culture);
                        var description = this.GetCultureDescription(item, culture);
                        var alt = this.GetCultureAltText(item, culture);
                        var credits = this.GetCultureCredits(item, culture);

                        //Map results plus categories and regions to photo DTO
                        var photo = Mapper.Map<PhotoInfo, Photo>(item, dest =>
                        {
                            dest.AfterMap((src, dst) =>
                            {
                                dst.Categories = categories;
                                dst.Regions = regions;
                                dst.Title = title;
                                dst.Description = description;
                                dst.AltText = alt;
                                dst.PhotoCredits = credits;
                            });
                        });

                        result.Add(photo);
                    }
                    return result;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;

        }

        public IEnumerable<Photo> PhotoQuery(List<int> categories, List<int> regions, string culture)
        {
            var search = this.GetAll(culture).AsQueryable().Where(p =>
                            p.Categories.Any(x => categories.Contains(x.CategoryID)) ||
                            p.Regions.Any(x => regions.Contains(x.CategoryID))
                            ).ToList();
            return search;

        }


        #region helpers
        private string GetCultureTitle(PhotoInfo item, string culture)
        {
            var title = "";
            if (culture.ToLower() == "en-ca")
                title = item.Title_EN;
            else if (culture.ToLower() == "fr-ca")
                title = item.Title_FR;

            return ValidationHelper.GetString(title, string.Empty);
        }

        private string GetCultureDescription(PhotoInfo item, string culture)
        {
            var description = "";
            if (culture.ToLower() == "en-ca")
                description = item.Description_EN;
            else if (culture.ToLower() == "fr-ca")
                description = item.Description_FR;

            return ValidationHelper.GetString(description, string.Empty);
        }

        private string GetCultureAltText(PhotoInfo item, string culture)
        {
            var alt = "";
            if (culture.ToLower() == "en-ca")
                alt = item.AltText_EN;
            else if (culture.ToLower() == "fr-ca")
                alt = item.AltText_FR;

            return ValidationHelper.GetString(alt, string.Empty);
        }

        private string GetCultureCredits(PhotoInfo item, string culture)
        {
            var credits = "";
            if (culture.ToLower() == "en-ca")
                credits = item.PhotoCredits_EN;
            else if (culture.ToLower() == "fr-ca")
                credits = item.PhotoCredits_FR;

            return ValidationHelper.GetString(credits, string.Empty);
        }
        #endregion
    }
}