﻿using AutoMapper;
using CMS.MediaLibrary;
using CMS.SiteProvider;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiMediaApi.Repository
{
    public class FileRepository : IFileResult
    {
        //private readonly IMappingEngine _mapper;

        public FileRepository()
        {

        }

        public MediaLibraryInfo GetLibrary(string code)
        {
            // Gets the media library
            MediaLibraryInfo library = MediaLibraryInfoProvider.GetMediaLibraryInfo(code, SiteContext.CurrentSiteName);
            //If library is not null - Map library to FileResult api model
            if (library != null)
            {
                var result = library;
                return result;
            }
            else
            {
                return new MediaLibraryInfo();
            }
        }

        public FileResult GetFile(MediaLibraryInfo library, string path)
        {
            MediaFileInfo file = MediaFileInfoProvider.GetMediaFileInfo(library.LibraryID, path);

            //If file is not null - Map file to FileResult api model
            if (file != null)
            {
                var result = Mapper.Map<MediaFileInfo, FileResult>(file);
                return result;
            }
            else
            {
                return new FileResult();
            }

        }

    }
}