using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace CRCPhotosForMedia
{    
    /// <summary>
    /// Class providing PhotoInfo management.
    /// </summary>
    public class PhotoInfoProvider : AbstractInfoProvider<PhotoInfo, PhotoInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public PhotoInfoProvider()
            : base(PhotoInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the PhotoInfo objects.
        /// </summary>
        public static ObjectQuery<PhotoInfo> GetPhotoes()
        {
            return ProviderObject.GetPhotoesInternal();
        }


        /// <summary>
        /// Returns PhotoInfo with specified ID.
        /// </summary>
        /// <param name="id">PhotoInfo ID</param>
        public static PhotoInfo GetPhotoInfo(int id)
        {
            return ProviderObject.GetPhotoInfoInternal(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PhotoInfo.
        /// </summary>
        /// <param name="infoObj">PhotoInfo to be set</param>
        public static void SetPhotoInfo(PhotoInfo infoObj)
        {
            ProviderObject.SetPhotoInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified PhotoInfo.
        /// </summary>
        /// <param name="infoObj">PhotoInfo to be deleted</param>
        public static void DeletePhotoInfo(PhotoInfo infoObj)
        {
            ProviderObject.DeletePhotoInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes PhotoInfo with specified ID.
        /// </summary>
        /// <param name="id">PhotoInfo ID</param>
        public static void DeletePhotoInfo(int id)
        {
            PhotoInfo infoObj = GetPhotoInfo(id);
            DeletePhotoInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the PhotoInfo objects.
        /// </summary>
        protected virtual ObjectQuery<PhotoInfo> GetPhotoesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns PhotoInfo with specified ID.
        /// </summary>
        /// <param name="id">PhotoInfo ID</param>        
        protected virtual PhotoInfo GetPhotoInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified PhotoInfo.
        /// </summary>
        /// <param name="infoObj">PhotoInfo to be set</param>        
        protected virtual void SetPhotoInfoInternal(PhotoInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified PhotoInfo.
        /// </summary>
        /// <param name="infoObj">PhotoInfo to be deleted</param>        
        protected virtual void DeletePhotoInfoInternal(PhotoInfo infoObj)
        {
            DeleteInfo(infoObj);
        }

        public static void MovePhototUp(PhotoInfo info)
        {
            info.Generalized.MoveObjectUp();
        }

        public static void MovePhotoDown(PhotoInfo info)
        {
            info.Generalized.MoveObjectDown();
        }

        #endregion
    }
}