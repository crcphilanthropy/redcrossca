﻿//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Http.Description;
//using MultiMediaApi.Models;
//using MultiMediaApi.Services.Repository;

//namespace MultiMediaApi.Controllers
//{
//    public class BannerSetsController : ApiController
//    {
//        private IBannerService service;

//        public BannerSetsController(IBannerService _service)
//        {
//            this.service = _service;
//        }

//        // GET: api/BannerSets
//        [ResponseType(typeof(BannerSet))]
//        public IHttpActionResult GetBannerSets()
//        {
//            var result = service.GetBannerSetsNoLinks();
//            if (result == null)
//            {
//                NotFound();
//            }
//            else if (result.Count() == 0)
//            {
//                var message = "No Content available";
//                Request.CreateErrorResponse(HttpStatusCode.NoContent, message);
//            }

//            return Ok(result);

//        }

//        // GET: api/BannerSet/5
//        [ResponseType(typeof(BannerSet))]
//        public IHttpActionResult GetBannerSet(int id)
//        {
//            if (id == 0 || id == int.MinValue)
//            {
//                var message = "Invalid Id";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
//            }
//            else
//            {
//                var result = service.GetBannerSetById(id);
//                if (result == null)
//                {
//                    return NotFound();
//                }

//                return Ok(result);
//            }

//        }

//        //POST: api/BannerSets
//        [ResponseType(typeof(BannerSet))]
//        public IHttpActionResult PostBannerSet([FromBody]string name)
//        {
//            if (string.IsNullOrEmpty(name))
//            {
//                var message = "No company name entered";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
//            }
//            else
//            {
//                //save company name
//                service.SaveCompanyName(name);
//                var result = service.GetBannersWithLinks(name);
//                if (result == null)
//                {
//                    return NotFound();
//                }

//                return Ok(result);
//            }

//        }


//        // POST: api/BannerSets
//        //[ResponseType(typeof(BannerSet))]
//        //public IHttpActionResult PostBannerSet(BannerSet bannerSet)
//        //{
//        //    if (!ModelState.IsValid)
//        //    {
//        //        return BadRequest(ModelState);
//        //    }

//        //    service.CreateBannerSet(bannerSet);

//        //    return CreatedAtRoute("DefaultApi", new { id = bannerSet.Id }, bannerSet);
//        //}

//        #region Async methods
//        //Async method for unit tests - Get all bannersets
//        [Route("GetAll")]
//        public async Task<IHttpActionResult> GetAllAsync()
//        {
//            return await Task.FromResult(GetBannerSets());
//        }
//        #endregion
//    }
//}