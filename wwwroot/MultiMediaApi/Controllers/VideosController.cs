﻿//using MultiMediaApi.Models;
//using MultiMediaApi.Services.Repository;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Http.Description;

//namespace MultiMediaApi.Controllers
//{
//    public class VideosController : ApiController
//    {
//        private readonly IVideoService service;

//        public VideosController(IVideoService _service)
//        {
//            service = _service;
//        }

//        // GET: api/Videos
//        [ResponseType(typeof(Video))]
//        public IHttpActionResult GetVideos()
//        {
//            var result = service.GetAllVideos();
//            if (result == null)
//            {
//                return NotFound();
//            }
//            else if (result.Count() == 0)
//            {
//                var message = "No Content available";
//                Request.CreateErrorResponse(HttpStatusCode.NoContent, message);
//            }

//            return Ok(result);

//        }

//        // GET: api/Video/5
//        [ResponseType(typeof(Video))]
//        public IHttpActionResult GetVideo(int id)
//        {
//            if (id == 0 || id == int.MinValue)
//            {
//                var message = "Invalid Id";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
//            }
//            else
//            {
//                var result = service.GetById(id);
//                if (result == null)
//                {
//                    return NotFound();
//                }

//                return Ok(result);
//            }

//        }

//        // GET: api/VideoSearch/categories&regions
//        [Route("GetVideoSearch")]
//        [ResponseType(typeof(Video))]
//        public IHttpActionResult GetVideoSearch(List<Category> categories, List<Region> regions)
//        {
//            var result = service.SearchVideos(categories, regions);
//            if (result == null)
//            {
//                return NotFound();
//            }
//            else if (result.Count() == 0)
//            {
//                var message = "No Search results for your query";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.NoContent, message));
//            }

//            return Ok(result);

//        }

//        #region Async methods
//        //Async method for unit tests - Get all bannersets
//        [Route("GetAll")]
//        public async Task<IHttpActionResult> GetAllAsync()
//        {
//            return await Task.FromResult(GetVideos());
//        }
//        #endregion
//    }
//}
