﻿//using MultiMediaApi.Services;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using MultiMediaApi.Services.Repository;
//using MultiMediaApi.Models;
//using System.Web.Http.Description;
//using System.Threading.Tasks;

//namespace MultiMediaApi.Controllers
//{
//    public class PhotosController : ApiController
//    {
//        private IPhotoService service;
//        //private PhotoService service;

//        public PhotosController(IPhotoService _service)
//        {
//            service = _service;
//        }

//        // GET: api/Photos
//        [ResponseType(typeof(Photo))]
//        public IHttpActionResult GetPhotos()
//        {
//            var result = service.GetAllPhotos();
//            if (result == null)
//            {
//                return NotFound();
//            }
//            else if (result.Count() == 0)
//            {
//                var message = "No Content available";
//                Request.CreateErrorResponse(HttpStatusCode.NoContent, message);
//            }

//            return Ok(result);

//        }

//        // GET: api/Photo/5
//        [ResponseType(typeof(Photo))]
//        public IHttpActionResult GetPhoto(int id)
//        {
//            if (id == 0 || id == int.MinValue)
//            {
//                var message = "Invalid Id";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
//            }
//            else
//            {
//                var result = service.GetById(id);
//                if (result == null)
//                {
//                    return NotFound();
//                }

//                return Ok(result);
//            }

//        }

//        // GET: api/PhotoSearch/categories&regions
//        [Route("GetPhotoSearch")]
//        [ResponseType(typeof(Photo))]
//        public IHttpActionResult GetPhotoSearch(List<Category> categories, List<Region> regions)
//        {
//            var result = service.SearchPhotos(categories, regions);
//            if (result == null)
//            {
//                return NotFound();
//            }
//            else if (result.Count() == 0)
//            {
//                var message = "No Search results for your query";
//                throw new HttpResponseException(
//                    Request.CreateErrorResponse(HttpStatusCode.NoContent, message));
//            }

//            return Ok(result);

//        }

//        #region Async methods
//        //Async method for unit tests - Get all bannersets
//        [Route("GetAll")]
//        public async Task<IHttpActionResult> GetAllAsync()
//        {
//            return await Task.FromResult(GetPhotos());
//        }
//        #endregion
//    }
//}
