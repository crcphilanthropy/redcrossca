﻿using AutoMapper;
//using CMS.DataEngine;
using CMS.MediaLibrary;
using CRCWebBanners;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MultiMediaApi.Models
{
    public class Banner
    {
        public int BannerID { get; set; }
        public string Title { get; set; }
        public string TrackingCode { get; set; }
        public string EmbedLink { get; set; }
        public string Image { get; set; }
        public string AltText { get; set; }
        public int BannerSetID { get; set; }
        public string BannerType { get; set; }
    }

    public class BannerSet
    {
        public int BannerSetID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TrackingUrl { get; set; }
        public string CampaignName { get; set; }
        public IEnumerable<Banner> Banners { get; set; }
    }

    public class Company
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string token { get; set; }
    }

    public class FileResult
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }


    public class BannerProfile : Profile
    {
        protected override void Configure()
        {
            //Create BannerInfo/banner maps
            CreateMap<BannerInfo, Banner>()
                .ForMember(dest => dest.AltText, opts => opts.Ignore())
                .ForMember(dest => dest.Title, opts => opts.Ignore());

            CreateMap<Banner, BannerInfo>()
                .ForMember(dest => dest.BannerID, opts => opts.Ignore());

            //Create BannerSetInfo/bannerset maps
            CreateMap<BannerSetInfo, BannerSet>()
                .ForMember(dest => dest.Description, opts => opts.Ignore())
                .ForMember(dest => dest.Banners, opts => opts.Ignore())
                .ForMember(dest => dest.Title, opts => opts.Ignore());

            CreateMap<BannerSet, BannerSetInfo>()
                .ForMember(dest => dest.BannerSetID, opts => opts.Ignore());
        }
    }

    public class CompanyProfile : Profile
    {
        protected override void Configure()
        {
            //Create Company/companyinfo maps
            CreateMap<CompanyInfo, Company>();
            CreateMap<Company, CompanyInfo>()
                .ForMember(dest => dest.CompanyID, opts => opts.Ignore());

        }
    }

    public class FileProfile : Profile
    {
        protected override void Configure()
        {
            //Create mediaFile/fileresult maps
            CreateMap<MediaFileInfo, FileResult>();
            CreateMap<FileResult, MediaFileInfo>()
                .ForMember(dest => dest.FileID, opts => opts.Ignore());

            // Use CreateMap... Etc.. here (Profile methods are the same as configuration methods)
        }
    }

}