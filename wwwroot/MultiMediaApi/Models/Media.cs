﻿using AutoMapper;
using CMS.CustomTables;
using CMS.DocumentEngine;
using CMS.Taxonomy;
using CRCPhotosForMedia;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MultiMediaApi.Models
{
    public class Photo
    {
        public int PhotoID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string AltText { get; set; }
        public string HighResImage { get; set; }
        public bool Download { get; set; }
        public string PhotoCredits { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Region> Regions { get; set; }
    }

    public class Video
    {
        public int VideoDetailID { get; set; }
        public string VideoEmbedID { get; set; }
        public string VideoSummary { get; set; }
        public string VideoTitle { get; set; }
        public string Credits { get; set; }
        public string AltText { get; set; }
        public string DownloadLink { get; set; }
        public string DetailsPageLink { get; set; }
        public int VideoLength { get; set; }
        public string Thumbnail { get; set; }
        public bool Subtitles { get; set; }
        public bool DownloadAvailable { get; set; }
        public string Transcript { get; set; }
        public DateTime? DocumentCreatedWhen { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Region> Regions { get; set; }
        public Guid DocumentGuid { get; set; }
    }

    public class Tags
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Region> Regions { get; set; }
    }

    public class Videos
    {
        public IEnumerable<Video> Video { get; set; }
    }

    public class Category
    {
        public int CategoryID { get; set; }
        public string CategoryDisplayName { get; set; }
        public string CategoryName { get; set; }
        public string SearchLink { get; set; }
    }

    public class Region
    {
        public int CategoryID { get; set; }
        public string CategoryDisplayName { get; set; }
        public string CategoryName { get; set; }
        public string SearchLink { get; set; }
    }

    public class Form
    {
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$")]
        [StringLength(200)]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")]
        public string Email { get; set; }

        [StringLength(200)]
        public string Publication { get; set; }

        public int DetailId { get; set; }
        public string Culture { get; set; }
    }

    public class MediaAlertForm
    {
        public int ItemID { get; set; }
        [RegularExpression(@"^[a-zA-Z''-']{1,40}$")]
        [StringLength(200)]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")]
        public string Email { get; set; }
    }

    public enum CultureCode
    {
        enca,
        frca
    }

    public class MediaProfile : Profile
    {
        protected override void Configure()
        {
            
            CreateMap<CategoryInfo, Category>()
                .ForMember(dest => dest.SearchLink, opts => opts.Ignore());
            CreateMap<CategoryInfo, Region>()
                .ForMember(dest => dest.SearchLink, opts => opts.Ignore());

            CreateMap<CustomTableItem, Form>()
                .ForMember(dest => dest.Name, opts => opts.Ignore())
                .ForMember(dest => dest.Email, opts => opts.Ignore())
                .ForMember(dest => dest.Publication, opts => opts.Ignore());

            CreateMap<CustomTableItem, MediaAlertForm>();
            // Use CreateMap... Etc.. here (Profile methods are the same as configuration methods)
        }
    }

    public class PhotoProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PhotoInfo, Photo>()
                .ForMember(dest => dest.Categories, opts => opts.Ignore())
                .ForMember(dest => dest.Title, opts => opts.Ignore())
                .ForMember(dest => dest.Description, opts => opts.Ignore())
                .ForMember(dest => dest.AltText, opts => opts.Ignore())
                .ForMember(dest => dest.PhotoCredits, opts => opts.Ignore())
                .ForMember(dest => dest.Regions, opts => opts.Ignore());

            CreateMap<Photo, PhotoInfo>();

        }
    }

    public class VideoProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TreeNode, Video>()
                .ForMember(dest => dest.Categories, opts => opts.Ignore())
                .ForMember(dest => dest.VideoDetailID, opts => opts.Ignore())
                .ForMember(dest => dest.VideoEmbedID, opts => opts.Ignore())
                .ForMember(dest => dest.Subtitles, opts => opts.Ignore())
                .ForMember(dest => dest.VideoSummary, opts => opts.Ignore())
                .ForMember(dest => dest.Thumbnail, opts => opts.Ignore())
                .ForMember(dest => dest.VideoLength, opts => opts.Ignore())
                .ForMember(dest => dest.Credits, opts => opts.Ignore())
                .ForMember(dest => dest.Transcript, opts => opts.Ignore())
                .ForMember(dest => dest.Regions, opts => opts.Ignore())
                .ForMember(dest => dest.DownloadAvailable, opts => opts.Ignore())
                .ForMember(dest => dest.AltText, opts => opts.Ignore())
                .ForMember(dest => dest.DownloadLink, opts => opts.Ignore())
                .ForMember(dest => dest.DocumentCreatedWhen, opts => opts.Ignore())
                .ForMember(dest => dest.DocumentGuid, opts => opts.Ignore())
                .ForMember(dest => dest.VideoTitle, opts => opts.Ignore());

        }
    }


}

