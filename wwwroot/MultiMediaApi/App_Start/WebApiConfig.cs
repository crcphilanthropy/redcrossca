﻿using AutoMapper;
using MultiMediaApi.Repository;
using MultiMediaApi.Services;
using MultiMediaApi.Services.Repository;
using System.Web.Http;

namespace MultiMediaApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //Unity IOC
            //var container = new UnityContainer();
            //container.RegisterType<IBanner, BannerRepository>();
            //container.RegisterType<IBannerSet, BannerSetRepository>();
            //container.RegisterType<ICompany, CompanyRepository>();
            //container.RegisterType<IPhoto, PhotoRepository>();
            //container.RegisterType<IFileResult, FileRepository>();
            ////services
            //container.RegisterType<IBannerService, BannerService>();
            //container.RegisterType<ICatalog, CatalogService>();
            //container.RegisterType<IEmbedLinkService, EmbedLinkService>();
            //container.RegisterType<ITrackingCodeService, TrackingCodeService>();
            //container.RegisterType<IPhotoService, PhotoService>();
            //container.RegisterType<IMediaLibraryService, MediaLibraryService>();
            ////Automapper
            //container.RegisterType<IMappingEngine, MappingEngine>();

            //config.DependencyResolver = new UnityResolver(container);

            //DryIOC
            //var container = new Container();
            ////repositories
            //container.Register<IBanner, BannerRepository>();
            //container.Register<IBannerSet, BannerSetRepository>();
            //container.Register<ICompany, CompanyRepository>();
            //container.Register<IPhoto, PhotoRepository>();
            //container.Register<IVideo, VideoRepository>();
            //container.Register<IFileResult, FileRepository>();
            ////services
            //container.Register<IBannerService, BannerService>();
            //container.Register<ICatalog, CatalogService>();
            //container.Register<IEmbedLinkService, EmbedLinkService>();
            //container.Register<ITrackingCodeService, TrackingCodeService>();
            //container.Register<IPhotoService, PhotoService>();
            //container.Register<IVideoService, VideoService>();

            //container.RegisterDelegate(r => Mapper.Engine);

            //container.WithWebApi(config);

            //// Web API routes
            //GlobalConfiguration.Configuration.Routes.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Always return results in json format
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(
                new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
        }
    }
}
