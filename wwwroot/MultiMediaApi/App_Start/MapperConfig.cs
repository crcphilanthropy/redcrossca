﻿using AutoMapper;
using MultiMediaApi.Models;

namespace MultiMediaApi
{
    public static class MapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<BannerProfile>();
                cfg.AddProfile<MediaProfile>();
            });
        }
    }
}