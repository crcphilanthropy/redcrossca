﻿using AttributeRouting.Web.Http;
using CMS.EventLog;
using MultiMediaApi.Models;
using MultiMediaApi.Services;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CMS.Controllers
{
    /// <summary>
    /// Multimedia Catalog controller
    /// </summary>
    public class CatalogController : ApiController
    {
        private readonly ICatalogService _service;
        private CultureCode culturecode;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param></param>
        public CatalogController()
        {
            _service = new CatalogService();
        }

        /// <summary>
        /// Gets categories 
        /// </summary>
        /// <param></param>
        /// <returns> List of categories</returns>
        [GET("api/categories")]
        public IEnumerable<Category> GetCategories(string culture)
        {
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("CatalogController", "GetCategories()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            var result = _service.Categories(culture);
            if (result == null)
            {
                var message = "Categories Not found";
                EventLogProvider.LogException("CatalogController", "GetCategories()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }

            return result;

        }

        /// <summary>
        /// Gets regions 
        /// </summary>
        /// <param></param>
        /// <returns> List of regions</returns>
        [GET("api/regions")]
        public IEnumerable<Region> GetRegions(string culture)
        {
            //async task
            //var task = Task.Run(() => service.Regions());
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("CatalogController", "GetRegions()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            var result = _service.Regions(culture);
            if (result == null)
            {
                var message = "Regions Not found";
                EventLogProvider.LogException("CatalogController", "GetRegions()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }

            return result;

        }

        /// <summary>
        /// Gets regions and categories ie Tags
        /// </summary>
        /// <param></param>
        /// <returns> List of regions and categories</returns>
        [GET("api/tags")]
        public Tags GetTags(string culture)
        {
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("CatalogController", "GetTags()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            var result = _service.GetTags(culture);
            if (result == null)
            {
                var message = "Tags Not found";
                EventLogProvider.LogException("CatalogController", "GetTags()", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }

            return result;

        }
    }
}
