﻿using CMS.EventLog;
using CMS.Localization;
using MultiMediaApi.Models;
using MultiMediaApi.Services;
using MultiMediaApi.Services.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace CMS.Controllers
{
    /// <summary>
    /// Multimedia videos controller
    /// </summary>
    public class VideosController : ApiController
    {
        private readonly IVideoService service;
        private readonly ICatalogService catalog;
        private CultureCode culturecode;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public VideosController()
        {
            service = new VideoService();
            catalog = new CatalogService();
        }

        /// <summary>
        /// Gets video by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="culture">culture</param>
        /// <returns>single photo</returns>
        public Video Get(int id, string culture)
        {
            if (!catalog.CheckIdValue(id))
            {
                var message = "Invalid input";
                EventLogProvider.LogException("VideosController", "Get", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("VideosController", "Get", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (id <= 0)
            {
                var message = "Invalid input";
                EventLogProvider.LogException("VideosController", "Get(id)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                var result = service.GetById(id, culture);
                if (result == null || result.VideoDetailID <= 0)
                {
                    var message = "No video found";
                    EventLogProvider.LogException("VideosController", "Get(id)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }

                return result;
            }

        }

        /// <summary>
        /// Gets videos based on search criteria
        /// </summary>
        /// <param name="category"></param>
        /// <param name="region"></param>
        /// <param name="path">path</param>
        /// <param name="culture">culture</param>
        /// <returns> List of filterd videos</returns>
        public IEnumerable<Video> Get(string culture, [FromUri]string path, [FromUri] string category = null, [FromUri] string region = null)
        {
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("VideosController", "Get", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (string.IsNullOrEmpty(path))
            {
                var message = "Invalid input";
                EventLogProvider.LogException("VideosController", "Get(path,categories,regions)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                var result = service.SearchVideos(category, region, path, culture);
                if (result == null)
                {
                    var message = "Videos Not found";
                    EventLogProvider.LogException("VideosController", "Get(path,categories,regions)", null, 0, message);
                    throw new HttpResponseException(
                   Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }

                return result;
            }

        }

        /// <summary>
        /// Post video form data
        /// </summary>
        /// <param></param>
        /// <returns> httpstatus code ok</returns>
        public Video Post([FromBody]Form data)
        {
            if (!ModelState.IsValid)
            {
                var message = "Invalid form data";
                EventLogProvider.LogException("VideosController", "Post(form)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }

            if (string.IsNullOrEmpty(data.Name) || string.IsNullOrEmpty(data.Email) || string.IsNullOrEmpty(data.Publication) || data.DetailId <= 0)
            {
                var message = "Invalid form data";
                EventLogProvider.LogException("VideosController", "Post(form)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                var form = service.SaveForm(data);
                if (form == null)
                {
                    var message = "Somthing went wrong, could not save form data";
                    EventLogProvider.LogException("VideosController", "Post(form)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NoContent, message));
                }
                else
                {
                    var result = this.Get(data.DetailId, data.Culture);
                    return result;
                }
            }

        }

    }
}
