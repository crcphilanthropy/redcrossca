﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MultiMediaApi.Models;
using MultiMediaApi.Services.Repository;
using AttributeRouting;
using AttributeRouting.Web.Http;
using System.Collections;
using System.Collections.Generic;
using MultiMediaApi.Services;
using CMS.EventLog;

namespace CMS.Controllers
{
    /// <summary>
    /// Multimedia BannerSet controller
    /// </summary>
    public class BannerSetsController : ApiController
    {
        private readonly IBannerService service;
        private readonly ICatalogService catalog;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param></param>
        public BannerSetsController()
        {
            service = new BannerService();
            catalog = new CatalogService();
        }

        // GET: api/BannerSets
        /// <summary>
        /// Gets all Banner Sets.
        /// </summary>
        /// <param></param>
        /// <returns> All available bannersets</returns>
        [GET("api/BannerSets")]
        public IEnumerable<BannerSet> Get(string culture)
        {
            var result = service.GetBannerSetsNoLinks(culture);
            if (result == null)
            {
                var message = "No Bannersets found";
                EventLogProvider.LogException("BannerSetsController", "Get()", null, 0, message);
                throw new HttpResponseException(
                   Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }
            
            return result;

        }

        // GET: api/BannerSet/5
        /// <summary>
        /// Gets single bannerset by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="culture"></param>
        /// <returns> Single bannerset</returns>
        [GET("api/BannerSets/{id}")]
        public BannerSet Get(int id, string culture)
        {
            if (!catalog.CheckIdValue(id))
            {
                var message = "Invalid input";
                EventLogProvider.LogException("BannerSetsController", "Get(id)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (id <= 0)
            {
                var message = "Invalid input";
                EventLogProvider.LogException("BannerSetsController", "Get(id)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                var result = service.GetBannerSetById(id, culture);
                if (result == null || result.BannerSetID <= 0)
                {
                    var message = "Bannerset Not found";
                    EventLogProvider.LogException("BannerSetsController", "Get()", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }
                return result;
            }

        }

        //POST: api/BannerSets
        /// <summary>
        /// Post.
        /// </summary>
        /// <param></param>
        /// <returns> All multimedia photos</returns>
        [POST("api/BannerSets")]
        public IEnumerable<BannerSet> Post([FromUri]string culture, [FromBody]string name)
        {           
            if (string.IsNullOrEmpty(name))
            {
                var message = "No company name entered";
                EventLogProvider.LogException("BannerSetsController", "Post(name)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                //save company name
                var company = service.SaveCompanyName(name);
                if (company == null || company.CompanyID <= 0)
                {
                    var message = "Something went wrong! Could not save company name, please try again.";
                    EventLogProvider.LogException("BannerSetsController", "Post(name)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }
                //Get bannersets with embed link and tracking code in banners
                var result = service.GetBannersWithLinks(name, culture);
                if (result == null)
                {
                    var message = "Bannersets Not found";
                    EventLogProvider.LogException("BannerSetsController", "Post(name)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }

                return result;
            }

        }
    }
}