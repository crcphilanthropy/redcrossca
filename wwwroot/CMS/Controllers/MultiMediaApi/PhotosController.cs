﻿using MultiMediaApi.Services;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MultiMediaApi.Services.Repository;
using MultiMediaApi.Models;
using CMS.EventLog;
using System;
using System.Linq;

namespace CMS.Controllers
{
    /// <summary>
    /// Multimedia photos controller
    /// </summary>
    public class PhotosController : ApiController
    {
        private readonly IPhotoService service;
        private readonly ICatalogService catalog;
        private CultureCode culturecode;
        private const string MessageInvalidInput = "Invalid input";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public PhotosController()
        {
            service = new PhotoService();
            catalog = new CatalogService();
        }

        /// <summary>
        /// Gets photo by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="culture">culture</param>
        /// <returns>single photo</returns>
        public Photo Get(int id, string culture)
        {
            if (!catalog.CheckIdValue(id))
            {
                EventLogProvider.LogException("PhotosController", "Get(id)", null, 0, MessageInvalidInput);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, MessageInvalidInput));
            }
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-","").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("PhotosController", "Get(id)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (id <= 0)
            {
                var message = "Invalid Id";
                EventLogProvider.LogException("PhotosController", "Get(id)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                //var result = cacheService.CachePhoto(id);
                var result = service.GetById(id, culture);
                if (result == null || result.PhotoID <= 0)
                {
                    var message = "No Photo found matching request";
                    EventLogProvider.LogException("PhotosController", "Get(int id)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                }

                return result;
            }

        }

        /// <summary>
        /// Gets photos based on search criteria
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="regions"></param>
        /// <param name="culture">culture</param>
        /// <returns> List of filterd photos</returns>

        public IEnumerable<Photo> Get(string culture, [FromUri] string categories = null, [FromUri] string regions = null)
        {
            if (string.IsNullOrEmpty(culture) || !Enum.TryParse(culture.Replace("-", "").ToLower(), out culturecode))
            {
                var message = "Invalid culture provided";
                EventLogProvider.LogException("PhotosController", "Get", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            var result = service.SearchPhotos(categories, regions, culture);
            if (result == null)
            {
                var message = "No Photos found";
                EventLogProvider.LogException("PhotosController", "Get(categories, regions)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }

            return result;

        }

    /// <summary>
    /// Post photos form data
    /// </summary>
    /// <param name="data"></param>
    /// <returns> httpstatus code ok</returns>
    public IEnumerable<Photo> Post([FromBody]Form data)
        {
            if (!ModelState.IsValid)
            {
                var message = "Data model is invalid";
                EventLogProvider.LogException("PhotosController", "Post(form)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            if (string.IsNullOrEmpty(data.Name) || string.IsNullOrEmpty(data.Email) || string.IsNullOrEmpty(data.Publication))
            {
                var message = "Invalid form data";
                EventLogProvider.LogException("PhotosController", "Post(form)", null, 0, message);
                throw new HttpResponseException(
                 Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            else
            {
                var form = service.SaveForm(data);
                if (form == null)
                {
                    var message = "Somthing went wrong, could not save data";
                    EventLogProvider.LogException("PhotosController", "Post(form)", null, 0, message);
                    throw new HttpResponseException(
                     Request.CreateErrorResponse(HttpStatusCode.NoContent, message));
                }
                else
                {
                    if (string.IsNullOrEmpty(data.Culture) || !Enum.TryParse(data.Culture.Replace("-", "").ToLower(), out culturecode))
                    {
                        var message = "Invalid culture provided";
                        EventLogProvider.LogException("PhotosController", "Get(id)", null, 0, message);
                        throw new HttpResponseException(
                         Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
                    }
                    var result = service.GetAllPhotos(data.Culture);
                    if (result == null || result.Count() == 0)
                    {
                        var message = "No Photos found";
                        EventLogProvider.LogException("PhotosController", "Post(form)", null, 0, message);
                        throw new HttpResponseException(
                         Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
                    }
                    return result;
                }
            }

        }

    }
}
