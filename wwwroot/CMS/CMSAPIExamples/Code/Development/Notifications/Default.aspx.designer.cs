﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Development_Notifications_Default {
    
    /// <summary>
    /// headCreateNotificationGateway control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateNotificationGateway;
    
    /// <summary>
    /// apiCreateNotificationGateway control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateNotificationGateway;
    
    /// <summary>
    /// apiGetAndUpdateNotificationGateway control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateNotificationGateway;
    
    /// <summary>
    /// apiGetAndBulkUpdateNotificationGateways control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateNotificationGateways;
    
    /// <summary>
    /// headCreateNotificationTemplate control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateNotificationTemplate;
    
    /// <summary>
    /// apiCreateNotificationTemplate control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateNotificationTemplate;
    
    /// <summary>
    /// apiGetAndUpdateNotificationTemplate control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateNotificationTemplate;
    
    /// <summary>
    /// apiGetAndBulkUpdateNotificationTemplates control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateNotificationTemplates;
    
    /// <summary>
    /// headCreateNotificationTemplateText control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateNotificationTemplateText;
    
    /// <summary>
    /// apiCreateNotificationTemplateText control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateNotificationTemplateText;
    
    /// <summary>
    /// apiGetAndUpdateNotificationTemplateText control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateNotificationTemplateText;
    
    /// <summary>
    /// apiGetAndBulkUpdateNotificationTemplateTexts control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateNotificationTemplateTexts;
    
    /// <summary>
    /// headCreateNotificationSubscription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateNotificationSubscription;
    
    /// <summary>
    /// apiCreateNotificationSubscription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateNotificationSubscription;
    
    /// <summary>
    /// apiGetAndUpdateNotificationSubscription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateNotificationSubscription;
    
    /// <summary>
    /// apiGetAndBulkUpdateNotificationSubscriptions control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateNotificationSubscriptions;
    
    /// <summary>
    /// headDeleteNotificationSubscription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteNotificationSubscription;
    
    /// <summary>
    /// apiDeleteNotificationSubscription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteNotificationSubscription;
    
    /// <summary>
    /// headDeleteNotificationTemplateText control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteNotificationTemplateText;
    
    /// <summary>
    /// apiDeleteNotificationTemplateText control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteNotificationTemplateText;
    
    /// <summary>
    /// headDeleteNotificationTemplate control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteNotificationTemplate;
    
    /// <summary>
    /// apiDeleteNotificationTemplate control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteNotificationTemplate;
    
    /// <summary>
    /// headDeleteNotificationGateway control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteNotificationGateway;
    
    /// <summary>
    /// apiDeleteNotificationGateway control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteNotificationGateway;
}
