﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Documents_Workflow_Advanced_Default {
    
    /// <summary>
    /// headPreparation control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headPreparation;
    
    /// <summary>
    /// apiCreateExampleObjects control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateExampleObjects;
    
    /// <summary>
    /// headEditDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headEditDocument;
    
    /// <summary>
    /// apiCheckOut control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCheckOut;
    
    /// <summary>
    /// apiEditDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiEditDocument;
    
    /// <summary>
    /// apiCheckIn control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCheckIn;
    
    /// <summary>
    /// apiUndoCheckout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiUndoCheckout;
    
    /// <summary>
    /// headWorkflowProcess control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headWorkflowProcess;
    
    /// <summary>
    /// apiMoveToNextStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiMoveToNextStep;
    
    /// <summary>
    /// apiMoveToPreviousStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiMoveToPreviousStep;
    
    /// <summary>
    /// apiPublishDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiPublishDocument;
    
    /// <summary>
    /// apiArchiveDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiArchiveDocument;
    
    /// <summary>
    /// headVersioning control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headVersioning;
    
    /// <summary>
    /// apiRollbackVersion control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiRollbackVersion;
    
    /// <summary>
    /// apiDeleteVersion control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteVersion;
    
    /// <summary>
    /// apiDestroyHistory control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDestroyHistory;
    
    /// <summary>
    /// headCleanUp control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCleanUp;
    
    /// <summary>
    /// apiDeleteExampleObjects control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteExampleObjects;
}
