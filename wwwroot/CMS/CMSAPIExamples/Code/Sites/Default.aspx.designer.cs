﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Sites_Default {
    
    /// <summary>
    /// headCreateSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateSite;
    
    /// <summary>
    /// apiCreateSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateSite;
    
    /// <summary>
    /// apiGetAndUpdateSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateSite;
    
    /// <summary>
    /// apiGetAndBulkUpdateSites control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateSites;
    
    /// <summary>
    /// headCreateCultureSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headCreateCultureSite;
    
    /// <summary>
    /// apiAddCultureToSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiAddCultureToSite;
    
    /// <summary>
    /// headDomainAliasOnSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDomainAliasOnSite;
    
    /// <summary>
    /// apiAddDomainAliasToSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiAddDomainAliasToSite;
    
    /// <summary>
    /// headSiteActions control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headSiteActions;
    
    /// <summary>
    /// apiRunSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiRunSite;
    
    /// <summary>
    /// apiStopSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiStopSite;
    
    /// <summary>
    /// headDeleteSiteDomainAlias control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteSiteDomainAlias;
    
    /// <summary>
    /// apiDeleteSiteDomainAlias control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteSiteDomainAlias;
    
    /// <summary>
    /// headDeleteCultureSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteCultureSite;
    
    /// <summary>
    /// apiRemoveCultureFromSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiRemoveCultureFromSite;
    
    /// <summary>
    /// headDeleteSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedHeading headDeleteSite;
    
    /// <summary>
    /// apiDeleteSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteSite;
}
