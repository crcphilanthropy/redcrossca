﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSApp.CRC.Entities
{
    public class CrcQuizQuestion
    {
        public string QuestionText, ResponseRationale;
        public int ResponseId;
        public List<string> ResponseList = new List<string>();
    }
}