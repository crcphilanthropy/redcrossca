﻿using System;

namespace CMSApp.CRC.Entities
{
    public class UrlMapping
    {
        public int SourceId { get; set; }
        public string TargetUrl { get; set; }
        public string LangCode { get; set; }
    }
}