﻿namespace CMSApp.CRC.Entities
{
    public class ImportExcel
    {
        public string OldUrl { get; set; }

        public string NewUrl { get; set; }
    }
}