﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace CMSApp.CRC.Entities
{
    public class CrcNewsItem
    {
        public string DocumentPageTemplate
        {
            get { return "CRC.NewsDetail"; }
        }

        public string DocumentName { get; set; }

        public int BraftonID { get; set; }
        public string NewsTitle { get; set; }
        public DateTime NewsReleaseDate { get; set; }
        public string NewsSummary { get; set; }
        public string NewsText { get; set; }
        public List<int> Categories { get; set; }
        public DateTime BraftonLastModifiedDate { get; set; }
        public string BraftonThumbnailImageUrl { get; set; }
        public string BraftonLargeImageUrl { get; set; }
        public string ImageAltText { get; set; }

        public string DocumentUrlPath { get; set; }
        public string DocumentPageTitle{ get; set; }
        public string DocumentPageKeyWords{ get; set; }
        public string DocumentPageDescription{ get; set; }
    }
}