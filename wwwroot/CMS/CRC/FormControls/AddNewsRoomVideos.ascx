﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewsRoomVideos.ascx.cs" Inherits="CMSApp.CRC.FormControls.AddNewsRoomVideos" %>
<style>
    .playlist
    {
        border: 1px solid #ccc;
        width: 800px;
        padding: 7px;
    }

        .playlist fieldset
        {
            background: none;
            border: 1px solid green;
        }

        /*.playlist fieldset.draggable
            {
                cursor: ns-resize;
            }*/

        .playlist legend
        {
            width: 99%;
            border: 1px solid green;
            background: #e9e9e9;
            padding: 3px;
        }

            .playlist legend span, .playlist .form legend
            {
                font-weight: bold;
                font-size: 16px;
            }

                .playlist legend span.controls
                {
                    float: right;
                }

        .playlist label
        {
            font-weight: bold;
            width: 100px;
            display: inline-block;
            text-align: right;
        }

        .playlist input, .playlist textarea
        {
            width: 450px;
        }

        .playlist .error
        {
            color: red;
        }

        .playlist .message
        {
            color: green;
        }

        .playlist .buttons
        {
            padding: 20px 0 10px 100px;
        }

        .playlist .form
        {
            border: 1px solid green;
            background: #f9f9f9;
        }

        .playlist #sortable
        {
            padding: 0 30px;
        }

    /*.active.ng-enter,
    .active.ng-leave
    {
        -webkit-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
        -moz-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
        -ms-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
        -o-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
        transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
        position: relative;
        display: block;
    }

        .active.ng-enter.ng-enter-active,
        .active.ng-leave
        {
            opacity: 1;
            top: 0;
            height: 30px;
        }

            .active.ng-leave.ng-leave-active,
            .active.ng-enter
            {
                opacity: 0;
                top: -50px;
                height: 0px;
            }*/
</style>
<div data-ng-app="newsroomVideoApp" data-ng-controller="NewsroomVideoController" class="playlist" id="playlist">
    <h2>Playlist</h2>
    <asp:HiddenField runat="server" ID="hidValue" />
    <div>
        <label for="list-title">List Title:</label>
        <input type="text" name="list-title" id="list-title" data-ng-model="RSS.Channel.Title" /><br />

        <label for="list-link">List Link:</label>
        <input type="text" name="list-link" id="list-link" data-ng-model="RSS.Channel.Link" />
    </div>
    <br />
    <fieldset class="form">
        <legend>{{FormTitle}}</legend>
        <ul class="error" data-ng-show="errors">
            <li data-ng-repeat="error in errors">{{error}}</li>
        </ul>
        <div data-ng-show="message" class="message">
            {{message}}
        </div>
        <label for="item-title">Title: </label>
        <input id="item-title" type="text" data-ng-model="newItem.Title" /><br />

        <label for="item-content-url">Content Url: </label>
        <input id="item-content-url" type="text" data-ng-model="newItem.Content.url" /><br />

        <label for="item-content-type">Content Type: </label>
        <input id="item-content-type" type="text" data-ng-model="newItem.Content.type" /><br />

        <label for="item-content-duration">Content Duration: </label>
        <input id="item-content-duration" type="text" data-ng-model="newItem.Content.duration" /><br />

        <label for="item-thumbnail">Thumbnail: </label>
        <input id="item-thumbnail" type="text" data-ng-model="newItem.Thumbnail.url" /><br />

        <label for="item-credit">Credit: </label>
        <input id="item-credit" type="text" data-ng-model="newItem.Credit" /><br />

        <label for="item-title">Link: </label>
        <input id="item-link" type="text" data-ng-model="newItem.Link" /><br />
        <br />

        <label for="item-desc">Description: </label>
        <textarea id="item-desc" data-ng-model="newItem.Description"></textarea><br />
        <div class="buttons">
            <button data-ng-click="saveNew()" onclick="return false;" data-ng-show="showSaveNew">Save New</button>
            <button data-ng-click="update()" onclick="return false;" data-ng-show="showUpdate">Update</button>
            <button data-ng-click="clear(); refreshItems(itemId)" onclick="return false;">Clear</button>
        </div>
    </fieldset>
    <p>
        <a data-ng-click="showHideList()" href="#">Show/Hide List</a>
    </p>
    <div data-ng-show="showList" data-ng-model="RSS.Channel.Items" id="sortable">
        <p>
            <strong>Filter:</strong>
            <input type="text" name="list-filter" id="list-filter" data-ng-model="query" /><br />
        </p>
        <fieldset data-ng-repeat="item in RSS.Channel.Items | filter : query" class="draggable">
            <legend><span class="title">{{item.Title}}</span>
                <span class="controls">
                    <%--<a href="#playlist" data-ng-click="updateSort()" title="Edit">
                            <img src="http://i1-news.softpedia-static.com/images/news2/Newly-Updated-Mac-Apps-Rucksack-CleanMyMac-Cocktail-2.png" height="20" /></a>&nbsp;--%>
                    <span data-ng-show="!query">
                        <a href="#playlist" data-ng-click="moveUp($index)" title="Move up" data-ng-show="!$first">
                            <img src="/a.aspx?cmsimg=/ug/Up.png" /></a>&nbsp;
                    <a href="#playlist" data-ng-click="moveDown($index)" title="Move down" data-ng-show="!$last">
                        <img src="/a.aspx?cmsimg=/ug/Down.png" /></a>&nbsp;</span>
                    <a href="#playlist" data-ng-click="edit(item)" title="Edit">
                        <img src="/a.aspx?cmsimg=/ug/Edit.png" /></a>&nbsp;
                    <a href="#playlist" data-ng-click="copy(item)" title="Copy">
                        <img src="/CMSPages/GetResource.ashx?image=%5bImages.zip%5d%2fCMSModules%2fCMS_Content%2fMenu%2fCopy.png" /></a>&nbsp;
                    <a href="#playlist" data-ng-click="removeItem($index)" title="Delete">
                        <img src="/a.aspx?cmsimg=/ug/Delete.png" /></a>
                </span>
            </legend>
            <label>Content:</label>
            {{item.Content.url}} | {{item.Content.type}} | {{item.Content.duration}}<br />
            <label>Thumbnail:</label>
            {{item.Thumbnail.url}}<br />
            <label>Credit:</label>
            {{item.Credit}}<br />
            <label>Link:</label>
            {{item.Link}}<br />
            <label>Description:</label>
            {{item.Description}}
        </fieldset>
    </div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular-animate.min.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
    var newsroomVideoApp = angular.module('newsroomVideoApp', ['ngAnimate'])
        .factory('newsroomFactory', function ($http) {
            return {
                apiPath: '/api/NewsroomVideo/GetRss',
                getAllItems: function (id) {
                    return $http.get(this.apiPath + "?id=" + id);
                },
                saveItem: function (rss, id) {
                    return $http.post(this.apiPath + "Update?id=" + id, rss);
                }
            };
        })
        .controller('NewsroomVideoController', ['$scope', '$filter', '$http', '$rootScope', '$log', 'newsroomFactory', function ($scope, $filter, $http, $rootScope, $log, newsroomFactory) {
            $scope.RSS = [];
            $scope.$log = $log;
            $scope.itemId = '<%= Value %>';
            $scope.newsroomFactory = newsroomFactory;
            $scope.newItem = new Object();
            $scope.showList = true;
            $scope.showUpdate = false;
            $scope.showSaveNew = true;
            $scope.errors = [];
            $scope.message = '';
            $scope.FormTitle = "Add New Video";

            $scope.refreshItems = function (id) {
                $scope.error = '';
                $scope.newsroomFactory.getAllItems(id).then(function (result) {
                    $scope.RSS = result.data;
                },
                    function (error) {
                        $scope.error = error.data.ExceptionMessage;
                        $scope.$log.error($scope.error);
                    });
            };

            $scope.refreshItems($scope.itemId);

            $scope.saveItem = function (rss, msg, refresh) {
                $scope.error = '';
                $scope.newsroomFactory.saveItem(rss, $scope.itemId).then(function () {
                    $scope.message = msg;
                    if (refresh)
                        $scope.refreshItems($scope.itemId);
                },
                    function (error) {
                        $scope.error = error.data.ExceptionMessage;
                        $scope.$log.error($scope.error);
                    });
            };

            $scope.saveNew = function () {
                $scope.error = '';
                if ($scope.isValidItem($scope.newItem)) {
                    $scope.RSS.Channel.Items.unshift($scope.newItem);
                    $scope.saveItem($scope.RSS, 'Save successfully');
                    $scope.clear();
                }
            };

            $scope.edit = function (item) {
                $scope.clear();
                $scope.FormTitle = "Edit Video";
                $scope.newItem = item;
                $scope.showUpdate = true;
                $scope.showSaveNew = false;
            };

            $scope.copy = function (item) {
                $scope.clear();
                $scope.newItem = angular.copy(item);
            };

            $scope.update = function () {
                $scope.error = '';
                if ($scope.isValidItem($scope.newItem)) {
                    $scope.saveItem($scope.RSS, 'Update successfully');
                }
            };

            $scope.updateSort = function () {
                $scope.error = '';
                $scope.saveItem($scope.RSS, 'Update successfully');
            };

            $scope.clear = function () {
                $scope.error = '';
                $scope.newItem = new Object();
                $scope.showUpdate = false;
                $scope.showSaveNew = true;
                $scope.message = '';
                $scope.FormTitle = "Add New Video";
            };

            $scope.isValidItem = function (item) {
                $scope.errors = [];
                if (item.Title == '' || item.Title == undefined)
                    $scope.errors.push("Please enter title");

                if (item.Content == undefined || item.Content.url == '' || item.Content.url == undefined)
                    $scope.errors.push("Please enter content url");

                if (item.Content == undefined || item.Content.type == '' || item.Content.type == undefined)
                    $scope.errors.push("Please enter content type");

                if (item.Content == undefined || item.Content.duration == '' || item.Content.duration == undefined)
                    $scope.errors.push("Please enter content duration");

                if (item.Thumbnail == undefined || item.Thumbnail.url == '' || item.Thumbnail.url == undefined)
                    $scope.errors.push("Please enter thumbnail url");

                if (item.Credit == '' || item.Credit == undefined)
                    $scope.errors.push("Please enter credit");

                if (item.Link == '' || item.Link == undefined)
                    $scope.errors.push("Please enter link");

                if (item.Description == '' || item.Description == undefined)
                    $scope.errors.push("Please enter description");

                return $scope.errors.length == 0;
            };

            $scope.removeItem = function (index) {
                if (confirm("Are you sure you want to remove this item?")) {
                    if (index > -1) {
                        $scope.RSS.Channel.Items.splice(index, 1);
                    }
                    $scope.saveItem($scope.RSS, 'Delete successfully');
                }
                event.preventDefault();
            };

            $scope.showHideList = function () {
                $scope.showList = !$scope.showList;
                event.preventDefault();
            };

            $scope.moveUp = function (index) {
                
                if (index - 1 < 0) return;

                moveItem(index, index - 1, true);
                event.preventDefault();
            };

            $scope.moveDown = function (index) {
                if (index + 1 >= $scope.RSS.Channel.Items.length) return;

                moveItem(index, index + 1, true);
                event.preventDefault();
            };

            function moveItem(fromIndex, toIndex, saveAfterMove) {
                $scope.clear();
                var element = $scope.RSS.Channel.Items[fromIndex];
                $scope.RSS.Channel.Items.splice(fromIndex, 1);
                $scope.RSS.Channel.Items.splice(toIndex, 0, element);
                if (saveAfterMove)
                    $scope.saveItem($scope.RSS, 'Order changed successfully', false);
            }

            //----------------------

            $scope.dragStart = function (e, ui) {
                ui.item.data('start', ui.item.index());
            };

            $scope.dragEnd = function (e, ui) {
                var start = ui.item.data('start'),
                    end = ui.item.index();

                moveItem(start, end, false);

                //$scope.$apply();
            };

            //jQuery('#sortable').sortable({
            //    containment: "parent",
            //    start: $scope.dragStart,
            //    update: $scope.dragEnd
            //});
        }]);

</script>
