using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.Localization;

namespace CMSApp.CRC.FormControls
{
    public partial class HiddenFieldCurrentLanguage : FormEngineUserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            hidLanguage.Value = LocalizationContext.PreferredCultureCode;
        }

        public override object Value
        {
            get
            {
                return hidLanguage.Value;
            }
            set
            {
                hidLanguage.Value = value as string;
            }
        }
    }
}