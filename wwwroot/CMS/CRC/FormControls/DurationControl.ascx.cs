﻿using System;

using CMS.FormControls;
using CMS.Helpers;

namespace CMSApp.FormControls
{
    public partial class DurationControl : FormEngineUserControl
    {
        /// <summary>
        /// Gets or sets field value. You need to override this method to make the control work properly with the form.
        /// </summary>
        public override object Value
        {
            get
            {
                var duration = 0;
                int hours = 0, minutes = 0, seconds = 0;

                if (!string.IsNullOrEmpty(this.txtHours.Text))
                {
                    if (int.TryParse(this.txtHours.Text, out hours))
                    {
                        duration += (hours * 3600);
                    }
                    else
                    {
                        this.txtHours.Text = "0";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtMinutes.Text))
                {
                    if (int.TryParse(this.txtMinutes.Text, out minutes))
                    {
                        duration += (minutes * 60);
                    }
                    else
                    {
                        this.txtMinutes.Text = "0";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtSeconds.Text))
                {
                    if (int.TryParse(this.txtSeconds.Text, out seconds))
                    {
                        duration += seconds;
                    }
                    else
                    {
                        this.txtSeconds.Text = "0";
                    }
                }

                return duration;
            }

            set
            {
                var seconds = ValidationHelper.GetInteger(value, 0);

                this.txtHours.Text = (seconds / 3600).ToString();
                this.txtMinutes.Text = ((int)(seconds / 60) % 60).ToString();
                this.txtSeconds.Text = (seconds%60).ToString();
            }
        }

        /// <summary>
        /// Load event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


        }

        /// <summary>
        /// Returns true if entered data is valid. If data is invalid, it returns false and displays an error message.
        /// </summary>
        /// <returns></returns>
        public override bool IsValid()
        {
            // If empty and required
            if (!FieldInfo.AllowEmpty && string.IsNullOrEmpty(this.txtHours.Text) && string.IsNullOrEmpty(this.txtMinutes.Text) && string.IsNullOrEmpty(this.txtSeconds.Text))
            {
                this.ValidationError = GetString("CRC_DurationControl.Required");
                return false;
            }

            if (!string.IsNullOrEmpty(this.txtHours.Text) && !string.IsNullOrEmpty(this.txtMinutes.Text) && !string.IsNullOrEmpty(this.txtSeconds.Text))
            {
                int hours = 0, minutes = 0, seconds = 0;
                if (!int.TryParse(this.txtHours.Text, out hours))
                {
                    this.ValidationError = GetString("CRC_DurationControl.IntegerRequired");
                    return false;
                }
                else if (!int.TryParse(this.txtMinutes.Text, out minutes))
                {
                    this.ValidationError = GetString("CRC_DurationControl.IntegerRequired");
                    return false;
                }
                else if (!int.TryParse(this.txtSeconds.Text, out seconds))
                {
                    this.ValidationError = GetString("CRC_DurationControl.IntegerRequired");
                    return false;
                }
            }

            return true;
        }
    }
}