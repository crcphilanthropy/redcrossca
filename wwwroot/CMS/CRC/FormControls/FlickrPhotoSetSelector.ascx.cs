﻿using System.Web.UI.WebControls;
using CMS.FormControls;
using CMSApp.CRC;

namespace CRC.FormControls
{
    public partial class FlickrPhotoSetSelector : FormEngineUserControl
    {

        private string _value = string.Empty;
        public override object Value
        {
            get { return Page.IsPostBack ? ddlPhotoSets.SelectedValue : _value; }
            set
            {
                _value = value as string ?? string.Empty;
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            BindDropDownList();
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            ddlPhotoSets.SelectedIndex = ddlPhotoSets.Items.IndexOf(ddlPhotoSets.Items.FindByValue(Value.ToString()));
        }

        private void BindDropDownList()
        {
            ddlPhotoSets.DataSource = Flickr.Instance.PhotosetsGetList(Flickr.FlickrUserID);
            ddlPhotoSets.DataTextField = "Title";
            ddlPhotoSets.DataValueField = "PhotosetId";
            ddlPhotoSets.DataBind();

            ddlPhotoSets.Items.Insert(0, new ListItem("Please select...", string.Empty));
        }
    }
}