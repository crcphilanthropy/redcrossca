using System;
using System.Net;
using CMS.EventLog;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMSApp.CRC.APIControllers;
using CMS.Helpers;

namespace CMSApp.CRC.FormControls
{
    public partial class AddNewsRoomVideos : FormEngineUserControl
    {
        public override object Value
        {
            get
            {
                if (IsPostBack && ValidationHelper.GetGuid(Request.Form[hidValue.UniqueID], Guid.Empty) != Guid.Empty)
                    return Request.Form[hidValue.UniqueID];

                if (ValidationHelper.GetGuid(hidValue.Value, Guid.Empty) == Guid.Empty)
                    hidValue.Value = Guid.NewGuid().ToString();

                return hidValue.Value;
            }
            set
            {
                if (IsPostBack) return;

                var val = ValidationHelper.GetGuid(value, Guid.Empty);
                if (val == Guid.Empty)
                {
                    val = Guid.NewGuid();
                    SaveOldVideos(val);
                }

                hidValue.Value = val.ToString();
            }
        }

        private const string ParentPath = "/crc/video-playlist/";
        private void SaveOldVideos(Guid id)
        {

            var fileGuid = ValidationHelper.GetGuid(Form.GetFieldValue("PlayList"), Guid.Empty);
            if (fileGuid == Guid.Empty) return;

            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(ParentPath)))
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(ParentPath));

            var filePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("{0}{1}.xml", ParentPath, id));

            try
            {
                var url = new Uri(Request.Url, string.Format("/CMSPages/GetFile.aspx?guid={0}", fileGuid)).AbsoluteUri;
                new WebClient().DownloadFile(url, filePath);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("AddNewsRoomVideos", "SaveOldVideos", ex);
                try
                {
                    var url = new Uri(string.Format("http://www.redcross.ca/CMSPages/GetFile.aspx?guid={0}", fileGuid)).AbsoluteUri;
                    new WebClient().DownloadFile(url, filePath);
                }
                catch (Exception exception)
                {
                    EventLogProvider.LogException("AddNewsRoomVideos", "SaveOldVideos", exception);
                }
            }
        }
    }
}