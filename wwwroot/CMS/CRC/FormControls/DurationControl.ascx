﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DurationControl.ascx.cs" Inherits="CMSApp.FormControls.DurationControl" %>

<asp:Label ID="lblHours" runat="server" Style="font-weight:bold;padding-top:5px;" AssociatedControlID="txtHours">Hours</asp:Label>
<cms:CMSTextBox ID="txtHours" runat="server" CssClass="form-control" Width="5em"/>

<asp:Label ID="lblMinutes" runat="server" Style="font-weight:bold;padding-top:5px;" AssociatedControlID="txtMinutes">Minutes</asp:Label>
<cms:CMSTextBox ID="txtMinutes" runat="server" CssClass="form-control" Width="5em" />

<asp:Label ID="lblSeconds" runat="server" Style="font-weight:bold;padding-top:5px;" AssociatedControlID="txtSeconds">Seconds</asp:Label>
<cms:CMSTextBox ID="txtSeconds" runat="server" CssClass="form-control" Width="5em" />