using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.FormControls
{
    public partial class BranchSelector : FormEngineUserControl
    {
        private string _selectedValue = string.Empty;
        public override object Value
        {
            get { return ddlBranches.SelectedValue; }
            set
            {
                _selectedValue = ValidationHelper.GetString(value, string.Empty);
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            InitBranches();
        }

        private void InitBranches()
        {
            ddlBranches.DataSource = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, false, "CRC.Branch", string.Empty, "Title ASC", TreeProvider.ALL_LEVELS, true, -1);
            ddlBranches.DataTextField = "Title";
            ddlBranches.DataValueField = "DocumentGUID";
            ddlBranches.DataBind();

            ddlBranches.Items.Insert(0, new ListItem("Select...", string.Empty));
            ddlBranches.SelectedIndex = ddlBranches.Items.IndexOf(ddlBranches.Items.FindByValue(_selectedValue));
        }
    }
}