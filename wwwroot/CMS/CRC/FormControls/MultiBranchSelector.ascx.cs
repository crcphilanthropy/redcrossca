using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using System.Linq;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.FormControls
{
    public partial class MultiBranchSelector : FormEngineUserControl
    {
        private IEnumerable<string> _selectedValues = new List<string>();
        public override object Value
        {
            get
            {
                return string.Join("|", lbBranches.GetSelectedIndices().Select(index => lbBranches.Items[index].Value));
            }
            set
            {
                _selectedValues = ValidationHelper.GetString(value, string.Empty).Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitBranches();
        }

        private void InitBranches()
        {
            lbBranches.DataSource = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, false, "CRC.Branch", string.Empty, "Title ASC", TreeProvider.ALL_LEVELS, true, -1);
            lbBranches.DataTextField = "Title";
            lbBranches.DataValueField = "DocumentGUID";
            lbBranches.DataBind();
            
            lbBranches.Items.Cast<ListItem>().ToList().ForEach(item => { item.Selected = _selectedValues.Contains(item.Value); });
        }
    }
}