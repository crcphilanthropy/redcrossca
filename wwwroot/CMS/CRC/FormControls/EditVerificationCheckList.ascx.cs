using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.FormControls;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using System.Data;
using CMS.GlobalHelper;
using CMS.CustomTables;
using CMS.Membership;
using CMS.DataEngine;
using CMS.Helpers;

namespace CMSApp.CRC.FormControls
{
    public partial class EditVerificationCheckList : FormEngineUserControl
    {
        private bool _displayedQuestions = false;

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        public override Object Value
        {
            get
            {
                return IsAllItemsChecked();
            }
            set
            {
                DisplayQuestions();
                if (value != null && value.ToString().ToLower() == "true")
                {
                    // set all checkboxes to true
                    foreach (ListItem chk in lstEditVerification.Items)
                    {
                        chk.Selected = true;
                    }
                }
            }

        }

        public override bool IsValid()
        {
            
            if(!IsAllItemsChecked()){
                this.ValidationError = "Please verify all checkbox fields are checked.";
                return false;
            }
            else 
                return true;
        }

        private void DisplayQuestions()
        {

            if (!_displayedQuestions)
            {
                
                string customTableClassName = "CRC.ContentVerification";

                // Check if Custom table 'Sample table' exists
                DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);

                if (customTable != null)
                {
                    // Prepare the parameters 
                    string where = "Active = 1";

                    // Get the data set according to the parameters 
                    DataSet dataSet = CustomTableItemProvider.GetItems(customTableClassName, where, "ItemOrder");

                    if (!DataHelper.DataSourceIsEmpty(dataSet))
                    {
                        foreach (DataRow row in dataSet.Tables[0].Rows)
                        {
                            // Get the custom table item ID
                            string itemText = row["Question"].ToString();
                            lstEditVerification.Items.Add(new ListItem(itemText, itemText, true));
                        }
                    }

                }
                _displayedQuestions = true;
            }
        }
        private bool IsAllItemsChecked()
        {
            foreach (ListItem chk in lstEditVerification.Items)
            {
                if (!chk.Selected)
                {
                    return false;
                }
            }
            return true;
        }

    }
}