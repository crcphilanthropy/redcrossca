using System;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMSApp.CRC;
using TreeNode = CMS.DocumentEngine.TreeNode;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMS.Localization;

public partial class CMSFormControls_Basic_TextBoxControl
{

    private bool WarnOnDuplicateTitle
    {
        get
        {
            return SettingsKeyInfoProvider.GetBoolValue(string.Format("{0}.WarnOnDuplicateTitle", SiteContext.CurrentSiteName));
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (WarnOnDuplicateTitle && Form != null)
            OnWarnOnDuplicateTitle();
    }

    void OnWarnOnDuplicateTitle()
    {
        if (!String.IsNullOrWhiteSpace(DocumentName) && DuplicateDocument != null)
        {
            Controls.AddAt(0, new Literal
                {
                    Text = String.Format("<div class='duplicate-title alert alert-warning'>{0} <a href='{1}' class='duplicate-link'>({2})</a></div>", ResHelper.GetString("CRC.DuplicateTitleWarning"), DuplicateDocument.NodeAliasPath, DocumentName)
                });
            txtText.CssClass += " alert-input-warning";
            Page.Header.Controls.Add(new Literal { Text = "<link rel='stylesheet' href='/CRC/css/cmsdesk.css' media='screen, print' />" });
        }
    }

    private string DocumentName
    {
        get
        {
            return ValidationHelper.GetString(Form.GetFieldValue("DocumentName"), string.Empty);
        }
    }

    private string WarnOnDuplicateTitleClasses
    {
        get
        {
            return SettingsKeyInfoProvider.GetValue(string.Format("{0}.WarnOnDuplicateTitleClasses", SiteContext.CurrentSiteName));
        }
    }

    private Guid DocumentGuid
    {
        get
        {
            if (Form == null) return Guid.Empty;
            return ValidationHelper.GetGuid(Form.GetFieldValue("DocumentGUID"), Guid.Empty);
        }
    }

    private string ClassName
    {
        get
        {
            var currentNode = Form.EditedObject as TreeNode;
            return currentNode != null ? currentNode.ClassName : string.Empty;
        }
    }

    private TreeNode _duplicateDocument;

    private TreeNode DuplicateDocument
    {
        get { return _duplicateDocument ?? (_duplicateDocument = GetDuplicateDocument()); }
    }

    private TreeNode GetDuplicateDocument()
    {
        var dci = DataClassInfoProvider.GetDataClassInfo(ClassName);
        if (dci == null || !dci.ClassIsDocumentType) return null;

        if (!WarnOnDuplicateTitleClasses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Contains(ClassName, new StringEqualityComparer()))
            return null;

        var documentSourceFieldName = dci.ClassNodeNameSource;
        if (string.IsNullOrWhiteSpace(documentSourceFieldName))
            documentSourceFieldName = "DocumentName";

        if (!FieldInfo.Name.Equals(documentSourceFieldName, StringComparison.OrdinalIgnoreCase)) return null;

        var data = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, false, WarnOnDuplicateTitleClasses, string.Format("DocumentName = '{0}' and DocumentGUID <> '{1}'", DocumentName.Replace("'", "''"), DocumentGuid), null, -1, true, 1);

        return DataHelper.DataSourceIsEmpty(data) ? null : data.Items.FirstItem;
    }
}