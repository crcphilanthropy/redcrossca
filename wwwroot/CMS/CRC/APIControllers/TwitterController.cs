﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Linq;
using CMS.EventLog;
using CMSApp.CRC.Model;
using LinqToTwitter;
using AttributeRouting.Web.Http;

namespace CMSApp.CRC.APIControllers
{
    public class TwitterController : ApiController
    {
        private TwitterContext _twitterCtx;

        private SingleUserAuthorizer _singleUserAuthorizer;

        private SingleUserAuthorizer SingleUserAuthorizer
        {
            get
            {
                return _singleUserAuthorizer ?? (_singleUserAuthorizer = new SingleUserAuthorizer
                {
                    Credentials = new InMemoryCredentials
                    {
                        ConsumerKey = "EhixlONZyHguy4Ektr7tg",
                        ConsumerSecret = "YzzF9hbHbe3L4gGoyyh3272YDIpcoGFKqoD2WtuzI",
                        OAuthToken = "1510930550-x5ziYCMctb2PbF8z7lT25vltJ1FWu1RUm2JWLPy",
                        AccessToken = "31WgXE74DqpYhcZLGZSylgTOrxYN8TmdItIMJA7cA"
                    }
                });
            }
        }

        [GET("api/twitter")]
        public IEnumerable<Tweet> Get(string id, [FromUri]int tweetCount = 0)
        {
            var toReturn = HttpRuntime.Cache.Get(string.Format("Tweets_{0}", id).ToLower()) as List<Tweet>;
            if (toReturn == null)
            {
                _twitterCtx = new TwitterContext(SingleUserAuthorizer);
                var statusTweets = (from tweet in _twitterCtx.Status
                                    orderby tweet.CreatedAt descending
                                    where tweet.Type == StatusType.User && tweet.ScreenName == id
                                    select tweet).ToList();

                toReturn = statusTweets.Select(tweet => new Tweet
                {
                    created_at = tweet.CreatedAt.ToLocalTime().ToString("dd MMM"),
                    time = tweet.CreatedAt,
                    time_span = GetTimeSpan(tweet.CreatedAt, true),
                    id_str = tweet.StatusID,
                    text = tweet.Text,
                    images = tweet.Entities == null || tweet.Entities.MediaEntities == null
                                           ? null
                                           : tweet.Entities.MediaEntities.Select(
                                               item => new TweetImage { image_url = item.MediaUrl }).ToList(),
                    username = tweet.ScreenName,
                    displayName = tweet.User.Name,
                    userPic = tweet.User.ProfileImageUrlHttps,
                    everything = tweet

                }).ToList();
                HttpRuntime.Cache.Insert(string.Format("Tweets_{0}", id).ToLower(), toReturn, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);

            }

            return toReturn.OrderByDescending(item => item.time).Take(tweetCount);
        }

        [GET("api/twitter/search")]
        public IEnumerable<Tweet> Search(string searchText, [FromUri] int tweetCount = 0)
        {
            var toReturn = HttpRuntime.Cache.Get(string.Format("Tweets_{0}", searchText).ToLower()) as List<Tweet>;
            if (toReturn == null)
            {
                _twitterCtx = new TwitterContext(SingleUserAuthorizer);
                try
                {
                    var srch =
                        (from search in _twitterCtx.Search
                         where search.Type == SearchType.Search &&
                               search.Query == searchText
                         select search).SingleOrDefault();

                    //Console.WriteLine("\nQuery: {0}\n", srch.SearchMetaData.Query);
                    if (srch != null && srch.Statuses.Any())
                    {
                        toReturn = srch.Statuses.Select(
                            tweet =>
                            new Tweet
                                {
                                    created_at =
                                        tweet.CreatedAt.ToUniversalTime().ToString(CultureInfo.InvariantCulture),
                                    id_str = tweet.StatusID,
                                    text = tweet.Text,
                                    images =
                                        tweet.Entities == null || tweet.Entities.MediaEntities == null
                                            ? null
                                            : tweet.Entities.MediaEntities.Select(
                                                item => new TweetImage { image_url = item.MediaUrl }).ToList()
                                }).ToList();

                        HttpRuntime.Cache.Insert(string.Format("Tweets_{0}", searchText).ToLower(), toReturn, null,
                                                 DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration,
                                                 CacheItemPriority.Low, null);
                    }
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("TwitterController", "Search", ex);
                }

            }

            return toReturn == null ? new List<Tweet>() : toReturn.Take(tweetCount);
        }

        [GET("api/twitter/gettweet")]
        public Tweet GetTweet([FromUri] string id)
        {
            Int64 val;
            if (!Int64.TryParse(id, out val)) return null;

            var toReturn = HttpRuntime.Cache.Get(string.Format("Tweets_{0}", id).ToLower()) as List<Tweet>;
            if (toReturn == null)
            {
                try
                {
                    _twitterCtx = new TwitterContext(SingleUserAuthorizer);
                    var statusTweets = (from tweet in _twitterCtx.Status
                                        orderby tweet.CreatedAt descending
                                        where tweet.Type == StatusType.Show && tweet.ID == id
                                        select tweet).ToList();

                    if (!statusTweets.Any()) return null;

                    toReturn = statusTweets.Select(tweet => new Tweet
                    {
                        created_at = tweet.CreatedAt.ToLocalTime().ToString("dd MMM yyyy"),
                        time_span = GetTimeSpan(tweet.CreatedAt),
                        id_str = tweet.StatusID,
                        text = tweet.Text,
                        displayName = tweet.User.Name,
                        username = tweet.User.Identifier.ScreenName,
                        userPic = tweet.User.ProfileImageUrlHttps,
                        favourite_count = tweet.FavoriteCount.GetValueOrDefault(),
                        retweet_count = tweet.RetweetCount
                    }).ToList();

                    HttpRuntime.Cache.Insert(string.Format("Tweets_{0}", id).ToLower(), toReturn, null, DateTime.Now.AddDays(1), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("TwitterController", "GetTweet", ex);
                    return null;
                }
            }

            return toReturn.FirstOrDefault();
        }

        private string GetTimeSpan(DateTime tweetTime, bool isShortForm = false)
        {
            var timeSpan = DateTime.Now.Subtract(tweetTime.ToLocalTime());
            var about = isShortForm ? string.Empty : string.Format("{0} ", ResourceStringHelper.GetString("CRC.About", defaultText: "About"));

            if (timeSpan.Days > 0)
                return string.Format("{0}{1} {2}", about, timeSpan.Days, ResourceStringHelper.GetString("CRC.DaysAgo"));

            if (timeSpan.Hours > 0)
                return string.Format("{0}{1} {2}", about, timeSpan.Hours, ResourceStringHelper.GetString("CRC.HoursAgo"));

            return string.Format("{0}{1} {2}", about, timeSpan.Minutes == 0 ? 1 : timeSpan.Minutes, ResourceStringHelper.GetString("CRC.MinutesAgo"));
        }
    }
}
