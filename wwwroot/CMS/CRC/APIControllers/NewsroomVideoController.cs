using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml.Serialization;
using CMS.GlobalHelper;
using CMS.Helpers;
using AttributeRouting.Web.Http;

namespace CMSApp.CRC.APIControllers
{
    public class NewsroomVideoController : ApiController
    {
        private const string ParentPath = "/crc/video-playlist/";

        //[HttpGet]
        //public string Get()
        //{
        //    return "Testing 1 2 3";
        //}

        [GET("api/newsroomvideo/getrss")]
        public Rss GetRss([FromUri]string id)
        {
            var filePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("{0}{1}.xml", ParentPath, id));
            if (!System.IO.File.Exists(filePath))
            {
                var rssStr = CustomTableHelper.GetItemValue("CRC.NewsroomVideo", string.Format("ItemGUID = '{0}'", id), "Rss") as string;
                return string.IsNullOrWhiteSpace(rssStr) ? new Rss() : SerializationHelper.Deserialize<Rss>(rssStr);
            }
            return SerializationHelper.Deserialize<Rss>(System.IO.File.ReadAllText(filePath));
        }

        [GET("api/newsroomvideo/getitem")]
        public HttpResponseMessage GetItem([FromUri]string id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
            response.Content = new StringContent(CustomTableHelper.GetItemValue("CRC.NewsroomVideo", string.Format("ItemGUID = '{0}'", id), "Rss") as string, Encoding.UTF8);
            return response;
        }

        [HttpPost]
        public void Update(Guid id, Rss rss)
        {
            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(ParentPath)))
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(ParentPath));

            var filePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("{0}{1}.xml", ParentPath, id));
            SerializationHelper.Serialize(filePath, rss);

            var rssString = SerializationHelper.SerializeToString(rss);
            var item = CustomTableHelper.GetItems("CRC.NewsroomVideo", string.Format("ItemGUID = '{0}'", id), null, 1);
            if (DataHelper.DataSourceIsEmpty(item))
            {
                CustomTableHelper.InsertCustomTableItem("CRC.NewsroomVideo",
                                                        new Dictionary<string, object>
                                                            {
                                                                {"RSS", rssString},
                                                                {"ItemGUID", id}
                                                            });
            }
            else
            {
                CustomTableHelper.UpdateCustomTableItem("CRC.NewsroomVideo",
                                                        new Dictionary<string, object> { { "RSS", rssString } },
                                                        item.Tables[0].Rows[0]);
            }
        }
    }

    public class NewsroomVideo
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "content", Namespace = "http://search.yahoo.com/mrss/")]
        public MediaContent Content { get; set; }

        [XmlElement(ElementName = "thumbnail", Namespace = "http://search.yahoo.com/mrss/")]
        public MediaContent Thumbnail { get; set; }

        [XmlElement(ElementName = "credit", Namespace = "http://search.yahoo.com/mrss/")]
        public string Credit { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }
    }

    public class Channel
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }

        [XmlElement(ElementName = "link")]
        public string Link { get; set; }

        [XmlElement(ElementName = "item")]
        public List<NewsroomVideo> Items { get; set; }
    }

    [XmlRoot(ElementName = "rss")]
    public class Rss
    {
        public Rss()
        {
            xmlns = new XmlSerializerNamespaces();
            Channel = new Channel
            {
                Items = new List<NewsroomVideo>()
            };
            xmlns.Add("media", "http://search.yahoo.com/mrss/");
        }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces xmlns { get; set; }

        [XmlElement(ElementName = "channel")]
        public Channel Channel { get; set; }
    }

    public class MediaContent
    {
        [XmlAttribute]
        public string url { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string duration { get; set; }
    }
}