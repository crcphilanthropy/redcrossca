using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMSApp.CRC.Model;
using CultureInfo = System.Globalization.CultureInfo;
using CMS.Helpers;
using CMS.Localization;
using CMS.DataEngine;

namespace CMSApp.CRC.APIControllers
{
    public class BlogMonthController : ApiController
    {
        private string _culture;
        private string CultureCode
        {
            get
            {
                if (_culture == null)
                {
                    var culture = QueryHelper.GetString(URLHelper.LanguageParameterName, LocalizationContext.PreferredCultureCode);
                    if (!CultureSiteInfoProvider.IsCultureAllowed(culture, SiteContext.CurrentSiteName))
                    {
                        culture = LocalizationContext.PreferredCultureCode;
                    }
                    _culture = culture;
                }

                return _culture;
            }
        }

        [Queryable]
        [HttpGet]
        public IQueryable<BlogMonth> Get()
        {
            var ds = ConnectionHelper.ExecuteQuery("CRC.BlogYear.BlogArchives", new QueryDataParameters() { { "NodeAliasPath", "/Blog/%" }, { "DocumentCulture", CultureCode } }, string.Empty, string.Empty);

            if (DataHelper.DataSourceIsEmpty(ds))
                return new List<BlogMonth>().AsQueryable();

            var list = new List<BlogMonth>();
            ds.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(item => list.Add(new BlogMonth
                {
                    Count = ValidationHelper.GetInteger(item["NumberOfPost"], 0),
                    Year = ValidationHelper.GetInteger(item["Year"], DateTime.Now.Year),
                    MonthText = string.Format("{0} {1} ({2})", ValidationHelper.GetDateTime(item["BlogMonthStartingDate"], DateTime.Now).ToString("MMMM", CultureInfo.GetCultureInfo(CultureCode)), item["year"], item["NumberOfPost"]),
                    Month = ValidationHelper.GetInteger(item["Month"], 0),
                    Url = string.IsNullOrWhiteSpace(item["DocumentUrlPath"].ToString()) ? item["NodeAliasPath"].ToString() : item["DocumentUrlPath"].ToString()
                }));

            return list.AsQueryable();
        }
    }
}