using System;
using System.Web.Http;
using CMS.DataEngine;
using System.Data;
using CMS.CustomTables;
using CMS.DocumentEngine;
using CMS.Helpers;

namespace CMSApp.CRC.APIControllers
{
    public class QuizDataController : ApiController
    {
        [HttpGet]
        public string Get()
        {
            return "Testing 1 2 3";
        }

        /// <summary>
        /// Increments StartsTotal when a quiz is started 
        /// AND The record is created the first time a quiz is responded to, using the quiz’s Document ID as the unique identifier
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        public void Post([FromUri]int id)
        {
            const string customTableClassName = "CRC.QuizData";

            // Get data class using custom table name
            var customTableClassInfo = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTableClassInfo == null)
            {
                throw new Exception("Given custom table does not exist.");
            }

            // Initialize custom table item provider with current user info and general connection

            // Get custom table items
            var dsItems = CustomTableItemProvider.GetItems(customTableClassInfo.ClassName, "QuizId = " + id);

            // Check if DataSet is not empty
            if (!DataHelper.DataSourceIsEmpty(dsItems))
            {
                // Get the custom table item ID
                var itemID = ValidationHelper.GetInteger(dsItems.Tables[0].Rows[0][0], 0);

                // Get the custom table item
                var updateCustomTableItem = CustomTableItemProvider.GetItem(itemID, customTableClassName);

                if (updateCustomTableItem != null)
                {
                    var itemText = ValidationHelper.GetString(updateCustomTableItem.GetValue("StartsTotal"), string.Empty);
                    var quizNode = new TreeProvider().SelectSingleDocument(id);

                    // Set new values
                    updateCustomTableItem.SetValue("StartsTotal", int.Parse(itemText) + 1);
                    updateCustomTableItem.SetValue("QuizTitle", quizNode != null ? quizNode.DocumentName : string.Format("Quiz ID: {0}", id));
                    updateCustomTableItem.SetValue("LastQuizStartDate", DateTime.Now);

                    // Save the changes
                    updateCustomTableItem.Update();
                }
            }
            else
            {
                var quizNode = new TreeProvider().SelectSingleDocument(id);
                // create a new record
                // Create new custom table item for given class of custom table
                var item = CustomTableItem.New(customTableClassName);

                // Set value of a custom table item field

                item.SetValue("QuizId", id);
                item.SetValue("QuizTitle", quizNode != null ? quizNode.DocumentName : string.Format("Quiz ID: {0}", id));
                item.SetValue("StartsTotal", 1);
                item.SetValue("CompletedTotal", 0);
                item.SetValue("QuizCreatedDate", DateTime.Now);
                item.SetValue("LastQuizStartDate", DateTime.Now);
                // Insert the item
                item.Insert();

            }
        }


        /// <summary>
        /// Increments StartsTotal when a quiz is started 
        /// AND The record is created the first time a quiz is responded to, using the quiz’s Document ID as the unique identifier
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        public void QuizComplete([FromUri]string id)
        {
            string customTableClassName = "CRC.QuizData";

            // Get data class using custom table name
            DataClassInfo customTableClassInfo = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTableClassInfo == null)
            {
                throw new Exception("Given custom table does not exist.");
            }

            // Initialize custom table item provider with current user info and general connection

            // Get custom table items
            DataSet dsItems = CustomTableItemProvider.GetItems(customTableClassInfo.ClassName, "QuizId = " + id, null);

            // Check if DataSet is not empty
            if (!DataHelper.DataSourceIsEmpty(dsItems))
            {
                // Get the custom table item ID
                int itemID = ValidationHelper.GetInteger(dsItems.Tables[0].Rows[0][0], 0);

                // Get the custom table item
                CustomTableItem updateCustomTableItem = CustomTableItemProvider.GetItem(itemID, customTableClassName);

                if (updateCustomTableItem != null)
                {
                    string itemText = ValidationHelper.GetString(updateCustomTableItem.GetValue("CompletedTotal"), "");

                    // Set new values
                    updateCustomTableItem.SetValue("CompletedTotal", int.Parse(itemText) + 1);
                    updateCustomTableItem.SetValue("LastQuizStartDate", DateTime.Now);

                    // Save the changes
                    updateCustomTableItem.Update();


                }

            }

        }
    }
}