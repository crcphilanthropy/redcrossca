using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using CMS.CMSHelper;
using CMS.SiteProvider;

namespace CMSApp.CRC.APIControllers
{
    public class DynamicCssController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {
            if (System.Web.HttpContext.Current != null)
            {
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.BufferOutput = true;
            }

            var response = new HttpResponseMessage(HttpStatusCode.OK);

            var sb = new StringBuilder();
            sb.AppendLine(".donate-form{");

            var backgroundImage = CMS.DataEngine.SettingsKeyInfoProvider.GetStringValue(String.Format("{0}.DonateFormBackgroundImage", SiteContext.CurrentSiteName));
            if (!string.IsNullOrWhiteSpace(backgroundImage))
                sb.AppendLine(string.Format("background-image: url('{0}');", backgroundImage.Replace("~", string.Empty)));

            sb.AppendLine("background-color: #cccccc;");
            sb.AppendLine("}");

            response.Content = new StringContent(sb.ToString(), Encoding.UTF8, "text/css");
            return response;
        }
    }
}