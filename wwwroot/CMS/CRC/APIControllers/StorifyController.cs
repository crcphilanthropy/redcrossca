﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Script.Serialization;
using CMSApp.CRC.Model;

namespace CMSApp.CRC.APIControllers
{
    public class StorifyController : ApiController
    {
        public List<Element> Get(string username, string storyName, int page = 1, int perPage = 50)
        {
            var cacheKey = string.Format("Storify_{0}_{1}_{2}_{3}", username, storyName, page, perPage).ToLower();
            var toReturn = HttpRuntime.Cache.Get(cacheKey) as List<Element>;

            if (toReturn == null)
            {
                var rxTwitter = new Regex(@"http://twitter.com/(?<username>.*)/status/(?<twitterId>\d*)", RegexOptions.IgnoreCase);
                var story = new WebClient { Encoding = Encoding.UTF8 }.DownloadString(string.Format("http://api.storify.com/v1/stories/{0}/{1}?page={2}&per_page={3}", username, storyName, page, perPage));
                var storifyObj = new JavaScriptSerializer().Deserialize<RootObject>(story);


                toReturn = new List<Element>();
                storifyObj.content.elements.ForEach(element =>
                    {
                        var twitterController = new TwitterController();
                        //var tweetItem = new WebClient { Encoding = Encoding.UTF8 }.DownloadString(element.data.link.rest_api_url);
                        element.id = rxTwitter.Match(element.permalink).Groups["twitterId"].Value;
                        var tweet = twitterController.GetTweet(element.id);
                            //?? twitterController.Search(String.Format("@{0} {1}", element.attribution.username,
                            //        element.data.link.description.Replace("Twitter / ", string.Empty).Replace("...", string.Empty).Replace(string.Format("{0}:", element.attribution.username), string.Empty)), 1).FirstOrDefault();

                        element.data.text = tweet != null ? tweet.text : element.data.link.description;
                        toReturn.Add(element);
                    });

                HttpRuntime.Cache.Insert(cacheKey, toReturn, null, DateTime.Now.AddMinutes(3), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);

            }

            return toReturn;
        }
    }
}