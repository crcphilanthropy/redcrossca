using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Scheduler;
using CMS.SiteProvider;
using CMS.Search;

namespace CMSApp.CRC
{
    public class RebuildIndexTask : ITask
    {
        private List<string> RebuildIndex(List<string> indexes)
        {
            var toReturn = new List<string>();
            indexes.ForEach(indexName =>
            {
                // Get the search index
                var index = SearchIndexInfoProvider.GetSearchIndexInfo(indexName);
                if (index == null) return;

                // Create rebuild task 
                SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Rebuild, index.IndexType, null, null, index.IndexID);
                toReturn.Add(indexName);
            });
            return toReturn;
        }

        public string Execute(TaskInfo task)
        {
            var indexes = task.TaskData.Split(new[] { ";", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList();
            var successIndexes = RebuildIndex(indexes);
            return successIndexes.Count < indexes.Count ? string.Format("SUCCESSS INDEXES: {0} -- FAILED INDEXES: {1}", string.Join("|", successIndexes), string.Join("|", indexes.Where(index => !successIndexes.Contains(index)))) : string.Format("SUCCESSS INDEXES: {0}", string.Join("|", successIndexes));
        }
    }
}