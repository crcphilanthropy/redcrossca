﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DonateForm.ascx.cs" Inherits="CMSApp.CRC.webparts.DonateForm" ViewStateMode="Disabled" %>
<%@ Import Namespace="CMSApp.CRC" %>

<asp:Panel runat="server" Visible="<%# IsMobile %>" >
       
    <asp:Repeater runat="server" ID="rptTablet">
        <HeaderTemplate>
            <div class="row ctas">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="large-4 medium-6 columns end">
                <a class="cta donate-track-tablet" data-link="<%# Eval("FormUrl") %>" data-language="<%# CultureHelper.GetPreferredCulture() %>" >
                    <div class="panel">
                        <div runat="server" visible='<%# !String.IsNullOrEmpty(Eval("DonateAlertText").ToString()) %>' class="donate-alert">
                            <%# Eval("DonateAlertText") %>
                        </div>
                        <img src='<%# Eval("Image") %>' alt="<%# Eval("ImageAltText") %>" />
                        <h3><%# (string.IsNullOrWhiteSpace(Eval("LPDropDownTitle").ToString()) ? Eval("Name") : Eval("LPDropDownTitle")) %></h3>
                        <p><%# Eval("Description") %></p>

                        
                        <p class="cta-button">
                            <span class="btn-submit button"><%# ResourceStringHelper.GetString("CRC.Donate.SubmitButtonText", defaultText: "Donate") %></span>
                        </p>
                    </div>
                </a>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div><!--end row ctas-->
        </FooterTemplate>
    </asp:Repeater>


</asp:Panel>

<!--<link rel="stylesheet" type="text/css" href="/api/dynamiccss" />-->
<asp:Panel runat="server" DefaultButton="btnSubmit" Visible="<%# !IsMobile %>">
    <div class="form-container">
        <div class="form-contents clearfix">
            <div class="donate-form">
                <%-- DONATE FORM BEGIN --%>
                <div class="fieldset-fix">
                    <fieldset>
                        <legend><%# ResourceStringHelper.GetString("CRC.Donate.DonateForm", defaultText: "Donate Form") %></legend>
                        <h2>
                            <label for="<%# ddlAppeals.ClientID %>">
                                <%# ResourceStringHelper.GetString("CRC.Donate.DonateTo", defaultText: "Donate To") %>
                            </label>
                        </h2>
                        <asp:Repeater runat="server" ID="rptTiledAppeals">
                            <HeaderTemplate>
                                <ul class="donate-select-nav">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li data-value='<%# Eval("DocumentGuid") %>'>
                                    <a href='<%# string.Format("{0}{1}#{2}",AppealBaseUrl, Eval("CodeName"), Eval("DocumentGuid")) %>'><span><%# Eval("AppealTitle") %></span></a>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                        <asp:DropDownList runat="server" ID="ddlAppeals" CssClass="ddl-appeals" />

                        <fieldset class="no-padding">
                            <legend><%# ResourceStringHelper.GetString("CRC.Donate.DonationAmountInfo", defaultText:"Select donation amount") %></legend>
						    <ul class="donate-price-points-nav">
	   					        <li class="price-point-1" >
	   						        <a href="javascript:void(0)"><span></span></a>
	   					        </li>
	   					        <li class="price-point-2">
	   						        <a href="javascript:void(0)"><span></span></a>
	   					        </li>
	   					        <li class="price-point-3">
	   						        <a href="javascript:void(0)"><span></span></a>
	   					        </li>
	   					        <li class="other" data-value="Other">
	   						        <a href="javascript:void(0)"><span><%# ResourceStringHelper.GetString("CRC.Other", defaultText: "Other") %></span></a>
	   					        </li>
	   					        <li class="textinput">
	   						        <input type="text" class="donate-price-point-value" />
	   					        </li>
						    </ul>
                            <ul class="price-points">
                                <li>
                                    <input type="radio" runat="server" id="rbPrice1" name="pricepoint" class="price-point-1 CheckBoxField" />
                                    <label for="<%# rbPrice1.ClientID %>" class="price-point-1"><span class="value"></span></label>
                                </li>
                                <li>
                                    <input type="radio" runat="server" id="rbPrice2" name="pricepoint" value="150" class="price-point-2 CheckBoxField" />
                                    <label for="<%# rbPrice2.ClientID %>" class="price-point-2"><span class="value"></span></label>
                                </li>
                                <li>
                                    <input type="radio" runat="server" id="rbPrice3" name="pricepoint" value="200" class="price-point-3 CheckBoxField" />
                                    <label for="<%# rbPrice3.ClientID %>" class="price-point-3"><span class="value"></span></label>
                                </li>
                                <li>
                                
                                    <input type="radio" runat="server" id="rbPrice4" name="pricepoint" value="0" class="price-point-4 CheckBoxField" />
                                    <label class="other-text" for="<%# rbPrice4.ClientID %>">
                                        <span class="value">
                                            <%# ResourceStringHelper.GetString("CRC.Other", defaultText: "Other") %>
                                        </span>
                                    </label>
                               
                                    <asp:PlaceHolder runat="server" ID="phOther">
                                        <asp:TextBox runat="server" ID="txtOther" CssClass="txt-other"></asp:TextBox>
                                    </asp:PlaceHolder>
                                
                                </li>
                            </ul>
                        </fieldset>
                    
                        <fieldset class="no-padding">
                            <legend><%# ResourceStringHelper.GetString("CRC.Donate.DonationFrequency", defaultText: "Frequency of Donation") %>"></legend>
                            <ul class="checkboxes">
                                <li>
                                    <asp:CheckBox runat="server" ID="chkMonthly" Text="Make this a Monthly Donation" CssClass="chk-monthly checkbox" />
                                    <i class="fa fa-question-circle grey tooltip" title="<%# ResourceStringHelper.GetString("CRC.Donate.MonthlyCheckboxTooltip", defaultText: "") %>"></i>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="chkInHonour" Text="In Honour" CssClass="chk-in-honour checkbox" />
                                    <i class="fa fa-question-circle grey tooltip" title="<%# ResourceStringHelper.GetString("CRC.Donate.InHonourCheckboxTooltip", defaultText: "") %>"></i>
                                </li>
                            </ul>
                        </fieldset>
                
                        <input type="hidden" id="hidDonateBarGAHash" runat="server" class="donate-bar-hash" />
                        <asp:Button runat="server" ID="btnSubmit" Text="Donate" CssClass="btn-submit donate-track btn-donate radius expand" />

                    </fieldset>
                </div>
                <%-- DONATE FORM END --%>
            </div>
            <%-- LEARN MORE PANEL BEGIN --%>
            <div class="learn-more-container-wrap">
                <div class="learn-more-container">
                    <span class="close">X <%# ResourceStringHelper.GetString("CRC.Donate.Close", defaultText: "Close") %></span>
                    <div>
                        <a class="learn-more-tab">
                            <img src="<%# ResourceStringHelper.GetString("CRC.Donate.LearnMoreTabImage", defaultText: "") %>" alt="learn more"  />
                        </a>
                    </div>
                    <div class="learn-more-panel">
                        <%-- LEARN MORE DESCRIPTIONS --%>

                        <asp:Repeater runat="server" ID="rptLearnMoreDescriptions">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li class="row appeal-description <%# Eval("DocumentGuid") %>" id='appeal-<%# Eval("DocumentGuid") %>'>
                                    <h4><%# Eval("AppealTitle") %></h4>
                                    <%# Eval("LearnMoreTab") %>                   
                                </li>

                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>

                    </div>
                </div>
            <%-- LEARN MORE PANEL END --%>
            </div>
        </div>
    </div>
    <%-- PRICE POINT ICONS BEGIN --%>
    <div class="row price-point-zone">
        <asp:Repeater runat="server" ID="rptAppealDesciptions">
            <ItemTemplate>
                <p class="show-hide-appeal appeal-desc <%# Eval("DocumentGuid") %>"><%# Eval("Description") %></p>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater runat="server" ID="rptPricePoints">
            <ItemTemplate>
                <div class="show-hide-appeal <%# Eval("DocumentGuid") %>">
                    <div class="row">
                        <asp:PlaceHolder runat="server" ID="phPricePoint1" Visible='<%# ShowHidePricePoint(Container.DataItem,1)  %>'>
                            <div class="small-4 columns  price-point <%# ShowHideOnDevice(Container.DataItem, "PricePoint", 1) %>">
                                <div>
                                    <span class="money"><%# GetDoubleValue(Container.DataItem, "PricePointAmount1", 75).ToString("C0") %></span>
                                    <img src="<%# Eval("PricePointImage1") %>" alt="<%# Eval("PricePointImage1AltText") %>" />
                                </div>
                                <p><%# Eval("PricePointDescription1") %></p>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="phPricePoint2" Visible='<%# ShowHidePricePoint(Container.DataItem,2)  %>'>
                            <div class="small-4 columns price-point <%# ShowHideOnDevice(Container.DataItem, "PricePoint", 2) %>">
                                <div>
                                    <span class="money"><%# GetDoubleValue(Container.DataItem, "PricePointAmount2", 150).ToString("C0") %></span>
                                    <img src="<%# Eval("PricePointImage2") %>" alt="<%# Eval("PricePointImage2AltText") %>" />
                                </div>
                                <p><%# Eval("PricePointDescription2") %></p>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="phPricePoint3" Visible='<%# ShowHidePricePoint(Container.DataItem,3)  %>'>
                            <div class="small-4 columns price-point <%# ShowHideOnDevice(Container.DataItem, "PricePoint", 3) %>">
                                <div>
                                    <span class="money"><%# GetDoubleValue(Container.DataItem, "PricePointAmount3", 200).ToString("C0") %></span>
                                    <img src="<%# Eval("PricePointImage3") %>" alt="<%# Eval("PricePointImage3AltText") %>" />
                                </div>
                                <p><%# Eval("PricePointDescription3") %></p>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </div>

            </ItemTemplate>
        </asp:Repeater>
    </div>
    <%-- PRICE POINT ICONS END --%>
</asp:Panel>

<asp:Panel runat="server" Visible="<%# !IsMobile %>">
<%-- IMPACT ICONS BEGIN --%>
<hr />
<div class="row center impact-zone">
    <h2><%# ResourceStringHelper.GetString("CRC.Donate.DonateImpactHeader", defaultText: "Canadian Red Cross impacts on a domestic scale 2013-2014") %></h2>
    <asp:Repeater runat="server" ID="rptImpacts">
        <ItemTemplate>
            <div class="show-hide-appeal <%# Eval("DocumentGuid") %>">
                <div class="row">
                    <div class="small-4 columns impact <%# ShowHideOnDevice(Container.DataItem, "Impact", 1) %>">
                        <img src="<%# ImpactImage1 %>" alt="<%# ImpactImage1AltText %>" />
                        <p>
                            <strong><span class="money"><%# Eval("ImpactAmount1") %></span></strong><br />
                            <%# Eval("ImpactDescription1") %>
                        </p>
                    </div>
                    <div class="small-4 columns impact <%# ShowHideOnDevice(Container.DataItem, "Impact", 2) %>">
                        <img src="<%# ImpactImage2 %>" alt="<%# ImpactImage2AltText %>" />
                        <p>
                            <strong><span class="money "><%# Eval("ImpactAmount2") %></span> </strong>
                            <br />
                            <%# Eval("ImpactDescription2") %>
                        </p>
                    </div>
                    <div class="small-4 columns impact<%# ShowHideOnDevice(Container.DataItem, "Impact", 3) %>">
                        <img src="<%# ImpactImage3 %>" alt="<%# ImpactImage3AltText %>" />
                        <p>
                            <strong><span class="money"><%# Eval("ImpactAmount3") %></span></strong><br />
                            <%# Eval("ImpactDescription3") %>
                        </p>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<%-- DONATE IMPACT ICONS END --%>
</asp:Panel>