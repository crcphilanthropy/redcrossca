﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSignUpWidget.ascx.cs" Inherits="CMSApp.CRC.webparts.EmailSignUpWidget" %>
<asp:Panel runat="server" DefaultButton="btnSignUp" ID="email" CssClass="email-CTA" ClientIDMode="Static">

    <% if (CurrentDocument.NodeAlias == "Home")
       { %>
    <div class="panel subscribe" data-equalizer-watch>
        <div class="svg-wrapper">
            <img src="/crc/images/icon-email.svg" alt="Email Icon" class="left" onerror="this.onerror=null; this.src='/crc/images/icon-email.png'" />
        </div>
        <h2><%= Title %></h2>
        <hr />
        <p><%#Subtitle %></p>

        <% }
       else
       { %>
        <div class=" hide-for-medium-down">
            <h3 class='h2'><%=Title %></h3>
            <% } %>

            <input type="text" placeholder='<%# ResHelper.GetString("CRC.EnterYourEmailHere") %>' runat="server" id="SignUpEmailTextBox" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="EmailSignUp" class="trim" />
            <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUp" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUp" CssClass="error" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
            <label class="button">
                <asp:Button runat="server" ID="btnSignUp" CssClass="submit-btn radius lyrisForm" ValidationGroup="EmailSignUp" data-validation-group="EmailSignUp" data-event="generic-event" data-category="form submission" data-event-action="signup" />
            </label>
        </div>
</asp:Panel>
