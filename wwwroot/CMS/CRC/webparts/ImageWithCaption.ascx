﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageWithCaption.ascx.cs" Inherits="CMSApp.CRC.webparts.ImageWithCaption" %>
<span class="image-with-caption <%# CssClass %>" style="width: <%# ImageWidth %>px;">
    <img src="<%# ImageUrl %>" alt="<%# ImageAltText %>" title="<%# ImageTitle %>" />
    <p>
        <%# ImageCaption %>
    </p>
</span>