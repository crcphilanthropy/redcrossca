using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalControls;
using CMSApp.CRC.Model;
using CMSApp.CRC.webparts.Controls;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Localization;
using CMS.DocumentEngine;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class CRCLeftNav : CMSAbstractWebPart
    {
        //public string DocumentTypes
        //{
        //    get { return ValidationHelper.GetString(GetValue("DocumentTypes"), string.Empty); }
        //    set { SetValue("DocumentTypes", value); }
        //}

        public string PathToExpand
        {
            get { return ValidationHelper.GetString(GetValue("PathToExpand"), string.Empty); }
            set { SetValue("PathToExpand", value); }
        }

        public bool ShowOnlyExpandedPath
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowOnlyExpandedPath"), false); }
            set { SetValue("ShowOnlyExpandedPath", value); }
        }

        private readonly List<string> _excludedDocTypes = new List<string> { "CRC.NavHeading", "CRC.NavigationSubheading" };
        private string ClassNames
        {
            get
            {
                return
                    _pageTypes.Select(item => string.Format("'{0}'", item))
                         .Where(item => !_excludedDocTypes.Contains(item, new StringEqualityComparer()))
                         .Join(",");
            }
        }


        private readonly List<string> _pageTypes = new List<string> { "CRC.Page", "CRC.Quiz", "CRC.Event", "CRC.Appeal", "CRC.IYCBranchFolder", "CRC.Province" };


        private DataSet _nodes;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _nodes = GetDatset();

            var currentRelativePath = CurrentRelativePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault() ?? string.Empty;
            currentRelativePath = currentRelativePath.ToLower().Replace(".aspx", string.Empty);

            var dv = _nodes.Tables[0].DefaultView;
            var whereClause = string.Format("NodeLevel = 2 AND (NodeAliasPath LIKE '/{0}%' OR NodeAliasPath LIKE '/{1}%')", currentRelativePath, CurrentDocument.NodeAliasPath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
            if (ShowOnlyExpandedPath)
                whereClause += string.Format(" AND (NodeAliasPath = '{0}' or DocumentUrlPath = '{0}')", PathToExpand);

            dv.RowFilter = whereClause;

            rptLeftNav.ItemDataBound += RptLeftNavItemDataBound;
            rptLeftNav.DataSource = dv;
            rptLeftNav.DataBind();

            phScript.DataBind();
        }

        private DataSet GetDatset()
        {
            return CacheHelper.Cache(() =>
            {
                var parameters = new QueryDataParameters { { "@SiteName", SiteContext.CurrentSiteName }, { "@DocumentCulture", LocalizationContext.PreferredCultureCode } };
                return new DataQuery("CRC.CustomTransformations.LeftNavSelectAll")
                    .Where(String.Format("ClassName in ({0}) AND NodeLevel < 5", ClassNames), parameters)
                    .Execute();
            }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("leftnav_{0}_{1}", LocalizationContext.PreferredCultureCode, string.Join("_", _pageTypes.OrderBy(item => item)).ToLower())));
        }

        void RptLeftNavItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            var phSubNav = e.Item.FindControl("phSubNav");
            var subNavRepeater = new UserControl().LoadControl("~/CRC/WebParts/Controls/NavTemplate.ascx") as NavTemplate;
            if (phSubNav == null || subNavRepeater == null) return;

            subNavRepeater.NavItemRepeater.ItemDataBound += RptLeftNavItemDataBound;

            var dv = _nodes.Tables[0].DefaultView;
            var whereClause = string.Format("NodeLevel = {0} AND NodeParentID = {1}",
                                            ValidationHelper.GetInteger(DataBinder.Eval(e.Item.DataItem, "NodeLevel"), 0) + 1,
                                            ValidationHelper.GetInteger(DataBinder.Eval(e.Item.DataItem, "NodeID"), 0));

            if (!IsActive(ValidationHelper.GetString(DataBinder.Eval(e.Item.DataItem, "NodeAliasPath"), string.Empty), ValidationHelper.GetString(DataBinder.Eval(e.Item.DataItem, "DocumentUrlPath"), string.Empty), PathToExpand))
                return;

            dv.RowFilter = whereClause;
            dv.Sort = "NodeLevel, NodeOrder";

            subNavRepeater.NavItemRepeater.DataSource = dv;

            if (dv.Count > 0)
                phSubNav.Controls.Add(subNavRepeater);

            phSubNav.DataBind();
        }

        private static string CurrentRelativePath
        {
            get
            {
                return LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? RequestContext.CurrentRelativePath.ToLower().Replace("en-canada", "au-canada") : RequestContext.CurrentRelativePath;
            }
        }

        public static string GetActive(string nodeAliasPath, string documentUrlPath, string pathToExpand)
        {
            return IsActive(nodeAliasPath, documentUrlPath, pathToExpand) ? "active" : string.Empty;
        }

        public static bool IsActive(string nodeAliasPath, string documentUrlPath, string pathToExpand)
        {
            return string.Equals(nodeAliasPath, CurrentRelativePath, StringComparison.OrdinalIgnoreCase)
                   || CurrentRelativePath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase)
                   || string.Equals(nodeAliasPath, pathToExpand, StringComparison.OrdinalIgnoreCase)
                   || string.Equals(nodeAliasPath, DocumentContext.CurrentDocument.NodeAliasPath, StringComparison.OrdinalIgnoreCase)
                   || DocumentContext.CurrentDocument.NodeAliasPath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase)
                   || DocumentContext.CurrentDocument.DocumentUrlPath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase);
        }

        public static string GetOnClick(string javascript)
        {
            return string.IsNullOrWhiteSpace(javascript) ? string.Empty : string.Format("onclick=\"{0}\"", javascript);
        }
    }
}