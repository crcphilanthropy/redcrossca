﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobList.ascx.cs" Inherits="CMSApp.CRC.webparts.JobList" %>
<%@ Register TagPrefix="crc" Namespace="CMSApp.CRC" Assembly="CMSApp" %>
<%@ Register Src="~/CRC/webparts/Controls/CRCPager.ascx" TagPrefix="uc1" TagName="CRCPager" %>
<asp:UpdatePanel runat="server" ID="udp1" class="job-list" UpdateMode="Always">
    <ContentTemplate>

        <div class="row">
            <h2 class="columns large-6 medium-12"><%# ResHelper.GetString("CRC.CurrentOpportunities") %></h2>

            <div class="columns large-6 medium-12">
                <asp:DropDownList runat="server" ID="ddlSortBy" AutoPostBack="True" CssClass="hide-for-large-up" />
                <asp:PlaceHolder runat="server" ID="phFilters" EnableViewState="False">
                    <asp:DropDownList runat="server" ID="ProvinceDropDown" AutoPostBack="True" DataTextField="Text" DataValueField="Value" />
                </asp:PlaceHolder>
            </div>
        </div>
        <div class="no-more-tables jobs-list">
            <table id="tblJobList">
                <thead class="noclick-mobile">
                    <tr>
                        <th>
                            <crc:TableHeaderLink runat="server" ID="lnkTitle" OrderByField="Title" ResString="CRC.JobTitle" OnClick="HeaderClick"></crc:TableHeaderLink>
                        </th>
                        <asp:PlaceHolder runat="server" ID="phType" Visible="False">
                            <th>
                                <crc:TableHeaderLink runat="server" ID="lnkType" OrderByField="CareerTypeName" ResString="CRC.Type" OnClick="HeaderClick"></crc:TableHeaderLink>
                            </th>
                        </asp:PlaceHolder>
                        <th>
                            <crc:TableHeaderLink runat="server" ID="lnkDatePosted" OrderByField="DocumentModifiedWhen" ResString="CRC.DatePosted" OnClick="HeaderClick" CssClass="DESC"></crc:TableHeaderLink>
                        </th>
                        <th>
                            <crc:TableHeaderLink runat="server" ID="lnkLocation" OrderByField="Location" ResString="CRC.Location" OnClick="HeaderClick"></crc:TableHeaderLink>
                        </th>
                        <th>
                            <crc:TableHeaderLink runat="server" ID="lnkAppDeadline" OrderByField="ApplicationDeadline" ResString="CRC.ApplicationDeadline" OnClick="HeaderClick"></crc:TableHeaderLink>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="rptJobs">
                    </asp:Repeater>
                </tbody>
            </table>
            <asp:PlaceHolder runat="server" ID="phNoResults" Visible="False">
                <p>
                    <%# ResourceStringHelper.GetString("CRC.Job.NoResults","There are currently no results that match your criteria.") %>
                </p>
            </asp:PlaceHolder>
            <uc1:CRCPager runat="server" ID="CRCPager" PageSize="2" GroupSize="3" IsLateBinding="True" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
