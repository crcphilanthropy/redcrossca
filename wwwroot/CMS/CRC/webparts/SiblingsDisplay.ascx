﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiblingsDisplay.ascx.cs" Inherits="CMSApp.CRC.webparts.SiblingsDisplay" EnableViewState="false"%>

<%# this.ContainerTitle %>
<ul>
    <cms:CMSRepeater runat="server" ID="rptSiblings">
        <ItemTemplate>
            <li>
                <asp:Literal runat="server" ID="litNode"></asp:Literal></li>
        </ItemTemplate>
    </cms:CMSRepeater>
</ul>
