﻿using System;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class TwitterSingleTweet : CMSAbstractWebPart
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //TransformationHelper.RegisterStartUpScript("<script src='/CMSPages/GetResource.ashx?scriptfile=/CRC/js/twitter-helper.js'></script>", Page, "twitter-helper", false);
            TransformationHelper.RegisterStartUpScript(string.Format("$(document).ready(function(){{ selectSingleTweet('{0}'); }});", Variables.GlobalTweetId), Page, "selectSingleTweet");
            DataBind();
        }
    }
}