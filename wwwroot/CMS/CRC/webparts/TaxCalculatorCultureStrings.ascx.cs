using System;
using CMS.PortalControls;

public partial class TaxCalculatorCultureStrings : CMSAbstractWebPart
{
    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {

        }

    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
}
