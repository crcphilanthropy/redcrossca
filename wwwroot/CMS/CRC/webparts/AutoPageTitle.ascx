<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.webparts.AutoPageTitle" CodeBehind="AutoPageTitle.ascx.cs" %>
<%@ Register Src="~/CRC/WebParts/Cloned/EditableText.ascx" TagName="EditableText" TagPrefix="crc" %>
<h1 runat="server" id="crcPageTitle">
    <crc:EditableText ID="ucEditableText" runat="server" />
    <asp:Literal runat="server" ID="litCategory"></asp:Literal>
    <asp:Literal runat="server" ID="litPageNumber"></asp:Literal>
</h1>
<asp:PlaceHolder runat="server" ID="phStyle" Visible="False">
    <style>
        h1 input
        {
            font-size: 1em;
        }
    </style>
</asp:PlaceHolder>
