using CMS.Helpers;
using CMS.PortalControls;
using CMS.DocumentEngine;


namespace CMSApp.CRC.webparts
{
    public partial class CRCQuiz : CMSAbstractWebPart
    {
        #region "Properties"

        public string ThankYouText
        {
            get { return ValidationHelper.GetString(GetValue("ThankYouText"), string.Empty); }
            set { SetValue("ThankYouText", value); }
        }

        public string IntroductionText
        {
            get { return ValidationHelper.GetString(GetValue("IntroductionText"), string.Empty); }
            set { SetValue("IntroductionText", value); }
        }

        public string BeginButtonText
        {
            get { return ValidationHelper.GetString(GetValue("BeginButtonText"), string.Empty); }
            set { SetValue("BeginButtonText", value); }
        }

        #endregion


        #region "Methods"

        /// <summary>
        /// Content loaded event handler
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
        }


        /// <summary>
        /// Initializes the control properties
        /// </summary>
        protected void SetupControl()
        {
            if (this.StopProcessing)
            {
                // Do not process
            }
            else
            {
                // output Quiz docType properties, and then child QuizQuestion properties
                ltrThankYou.Text = DocumentContext.CurrentDocument.GetStringValue("ThankYou", ThankYouText);
                ltrIntroduction.Text = DocumentContext.CurrentDocument.GetStringValue("Introduction", IntroductionText);

                if (!string.IsNullOrWhiteSpace(BeginButtonText))
                {
                    TransformationHelper.RegisterStartUpScript(string.Format("var quizBeginButtonText = '{0}'", BeginButtonText.Replace("'", "\'")), Page, "BeginButtonText");
                }
            }
        }


        /// <summary>
        /// Reloads the control data
        /// </summary>
        public override void ReloadData()
        {
            base.ReloadData();

            SetupControl();
        }

        #endregion
    }
}



