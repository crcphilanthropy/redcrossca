﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramBreadcrumb.ascx.cs" Inherits="CMSApp.CRC.WebParts.ProgramBreadcrumb" EnableViewState="false" %>
<%@ Import Namespace="CMSApp.CRC" %>
<ul class="inline-list breadcrumb">
    <asp:Repeater runat="server" ID="rptBC" EnableViewState="False">
        <ItemTemplate>
            <li><a href="<%# Eval("Key") %>"><%# Eval("Value") %></a></li>
        </ItemTemplate>
    </asp:Repeater>
    <%--<li><a href="<%# TransformationHelper.GetRegionPath(CMSContext.PreferredCultureCode) %>/<%# TransformationHelper.FormatCategoryDisplayName(Province.CategoryDisplayName,CMSContext.PreferredCultureCode,false).Replace(" ","-") %>"><%# TransformationHelper.FormatCategoryDisplayName(Province.CategoryDisplayName,CMSContext.PreferredCultureCode) %></a></li>--%>
    <%--<li><%# ResHelper.GetString(City.CategoryDisplayName) %></li>--%>
</ul>
