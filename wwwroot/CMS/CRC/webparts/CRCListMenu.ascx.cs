using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMS.Localization;
using System.Reflection;

namespace CMSApp.CRC.webparts
{
  public partial class CRCListMenu : CMSAbstractWebPart
  {
    private DataTable _nodes;
    public string Path
    {
      get { return ValidationHelper.GetString(GetValue("Path"), "./%"); }
      set { SetValue("Path", value); }
    }

    public string DocumentTypes
    {
      get { return ValidationHelper.GetString(GetValue("DocumentTypes"), string.Empty); }
      set { SetValue("DocumentTypes", value); }
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      rptListMenu.ItemDataBound += RptListMenuItemDataBound;
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      var where = new List<string>();
      where.Add("(DocumentMenuItemHideInNavigation = 0 AND [Group] like '%main%' AND NodeLevel = 1)");
      where.Add("(DocumentMenuItemHideInNavigation = 0 AND ([Group] <> 'exclude' or [Group] is null) AND NodeLevel = 2)");
      where.Add("([Group] like '%drop-down-nav%' and DocumentMenuItemHideInNavigation = 0 AND NodeLevel = 3)");

      var ds = CacheHelper.Cache(cs => TreeHelper.GetDocuments(SiteContext.CurrentSiteName,
        Path,
        LocalizationContext.PreferredCultureCode,
        false,
        DocumentTypes,
        "(DocumentWorkflowStepID > 1 OR DocumentWorkflowStepID IS NULL) AND" + where.Join(" OR "),
        "NodeOrder ASC",
        3,
        true,
        -1,
        "Published, DocumentWorkflowStepID, DocumentUrlPath, NodeAliasPath, DocumentMenuClass, DocumentMenuCaption, DocumentName, DocumentMenuCaption, DocumentUrlPath, NodeLevel, NodeOrder, NodeParentID, NodeID, DocumentMenuRedirectUrl"),
      new CacheSettings(60, string.Format("MainMenu_{0}", LocalizationContext.PreferredCultureCode)));


      _nodes = GetMergedTables(ds);
      if (DataHelper.DataSourceIsEmpty(_nodes)) return;

      var dv = _nodes.DefaultView;
      dv.RowFilter = "NodeLevel = 1";
      dv.Sort = "NodeOrder ASC";

      rptListMenu.DataSource = dv;
      rptListMenu.DataBind();
    }

    private bool _currentTabIsDouble = false;
    void RptListMenuItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
      {
        var cssClasses = ValidationHelper.GetString(DataBinder.Eval(e.Item.DataItem, "DocumentMenuClass"), string.Empty).ToLower()
                            .Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

        _currentTabIsDouble = cssClasses.Contains("double");
        BindLevel2Items(e);
      }
    }

    private void BindLevel2Items(RepeaterItemEventArgs e)
    {
      var rptLevel2 = e.Item.FindControl("rptLevel2") as Repeater;
      if (rptLevel2 == null) return;

      rptLevel2.ItemDataBound += RptLevel2ItemDataBound;

      var dv = _nodes.DefaultView;
      dv.RowFilter = "NodeLevel = 2 AND NodeParentID = " + DataBinder.Eval(e.Item.DataItem, "NodeID");
      dv.Sort = "NodeOrder ASC";

      if (dv.Count == 0)
        return;

      _splitIndex = _currentTabIsDouble ? Convert.ToInt32(Math.Ceiling(dv.Count / 2.0)) : -1;

      rptLevel2.DataSource = dv;
      rptLevel2.DataBind();
    }

    private int _splitIndex = -1;
    void RptLevel2ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      switch (e.Item.ItemType)
      {
        case ListItemType.AlternatingItem:
        case ListItemType.Item:
          var rptLevel3 = e.Item.FindControl("rptLevel3") as Repeater;
          if (rptLevel3 == null) return;

          var dv = _nodes.DefaultView;
          dv.RowFilter = "NodeLevel = 3 AND NodeParentID = " + DataBinder.Eval(e.Item.DataItem, "NodeID");
          dv.Sort = "NodeOrder ASC";

          if (dv.Count == 0)
            return;

          rptLevel3.DataSource = dv;
          rptLevel3.DataBind();
          break;
        case ListItemType.Separator:
          var phSeparator = e.Item.FindControl("phSeparator") as PlaceHolder;
          if (phSeparator == null) return;

          phSeparator.Visible = _currentTabIsDouble && e.Item.ItemIndex == _splitIndex - 1;
          break;
      }
    }

    protected string GetActive(string nodeAliasPath)
    {
      if (string.Equals(nodeAliasPath, DocumentContext.CurrentDocument.NodeAliasPath, StringComparison.OrdinalIgnoreCase) || DocumentContext.CurrentDocument.NodeAliasPath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase))
        return "currrent";

      return string.Empty;
    }

    private DataTable GetMergedTables(DataSet ds)
    {
      if (DataHelper.DataSourceIsEmpty(ds)) return null;

      var table1 = ds.Tables[0].Copy();
      if (ds.Tables.Count > 1)
        for (var i = 1; i < ds.Tables.Count; i++)
        {
          table1.Merge(ds.Tables[i], true, MissingSchemaAction.Add);
        }

      foreach (DataRow row in table1.Rows)
      {
        var stepid = row["DocumentWorkflowStepID"].ToString();
        int newStepId;
        if (int.TryParse(stepid, out newStepId))
        {
          if (newStepId <= 1)
            row.Delete();
        }
      }

      return table1;
    }

  }
}