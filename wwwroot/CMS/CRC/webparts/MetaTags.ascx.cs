using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.Helpers;
using CMS.Taxonomy;
using CMS.DocumentEngine;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class MetaTags : CMSAbstractWebPart
    {
        private const string RegionMetaTag = "<meta name='ga_custom_var' content='3^Region^{0}^3' />\r\n";
        private const string TopicMetaTag = "<meta name='ga_custom_var' content='2^Topic^{0}^3' />\r\n";
        private const string VisitorMetaTag = "<meta name='ga_custom_var' content='1^Visitor-Type^{0}^1' />\r\n";

        public bool IncludeTopics
        {
            get { return ValidationHelper.GetBoolean(GetValue("IncludeTopics"), false); }
            set { SetValue("IncludeTopics", value); }
        }

        public bool IncludeRegions
        {
            get { return ValidationHelper.GetBoolean(GetValue("IncludeRegions"), false); }
            set { SetValue("IncludeRegions", value); }
        }

        public bool IncludeVisitor
        {
            get { return ValidationHelper.GetBoolean(GetValue("IncludeVisitor"), false); }
            set { SetValue("IncludeVisitor", value); }
        }

        public string VisitorType
        {
            get { return ValidationHelper.GetString(GetValue("VisitorType"), "Visitor"); }
            set { SetValue("VisitorType", value); }
        }

        public string CustomTags
        {
            get { return ValidationHelper.GetString(GetValue("CustomTags"), String.Empty); }
            set { SetValue("CustomTags", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            AddTopics();
            AddRegions();
            //AddVisitorTag();
            AddCustomTags();
            Page.Header.Controls.AddAt(1, new Literal() { Text = "\r\n" });
        }

        private void AddCustomTags()
        {
            if (string.IsNullOrWhiteSpace(CustomTags)) return;
            Page.Header.Controls.AddAt(2, new Literal() { Text = CustomTags });
        }

        private void AddVisitorTag()
        {
            if (!IncludeVisitor || string.IsNullOrWhiteSpace(VisitorType)) return;
            Page.Header.Controls.AddAt(1, new Literal() { Text = string.Format(VisitorMetaTag, VisitorType) });
        }

        private void AddRegions()
        {
            if (!IncludeRegions) return;
            var regions = Categories.DefaultView;
            regions.RowFilter = "CategoryParentID = 21 or CategoryParentID = 156 and CategoryLevel = 2";
            AddCategories(regions, RegionMetaTag);
        }

        private void AddTopics()
        {
            if (!IncludeTopics) return;
            var topics = Categories.DefaultView;
            topics.RowFilter = "CategoryIDPath like '/00000007/%' and CategoryLevel > 0";
            AddCategories(topics, TopicMetaTag);
        }

        private void AddCategories(IEnumerable categories, string metaTagTemplate)
        {
            if (categories == null || string.IsNullOrWhiteSpace(metaTagTemplate))
                return;

            foreach (DataRowView category in categories)
            {
                Page.Header.Controls.AddAt(1,
                                           new Literal()
                                               {
                                                   Text = string.Format(metaTagTemplate, category["CategoryDisplayName"])
                                               });
            }
        }

        private DataTable _categories;
        private DataTable Categories
        {
            get
            {
                if (_categories == null)
                {
                    var cats = CacheHelper.Cache(() => CategoryInfoProvider.GetCategories(string.Format(
                        "(CategoryID in (Select distinct CategoryID from CMS_DocumentCategory Where DocumentID = {0}) or CategoryID in (Select c.CategoryParentID from [CMS_Category] c where c.CategoryLevel = 3 and c.CategoryID in (Select distinct CategoryID from CMS_DocumentCategory Where DocumentID = {0})))",
                        DocumentContext.CurrentDocument.DocumentID), "CategoryDisplayName"), new CacheSettings(Variables.CacheTimeInMinutes, string.Format("metatags_documentcategores")));
                
                    
                    if (cats != null && cats.Tables.Count > 0) _categories = cats.Tables[0];

                }
                return _categories;
            }
        }

        public List<int> GetCategoryIdsFromPath(string categoryIdPath)
        {
            var list = categoryIdPath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            return list.ToList().Select(item => Convert.ToInt32(item)).ToList();
        }
    }
}