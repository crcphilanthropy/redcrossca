﻿using System;
using CMS.Helpers;
using CMS.PortalControls;

namespace CMSApp.CRC.webparts
{
    public partial class InfographicFallback : CMSAbstractWebPart
    {
        public string Image
        {
            get { return ValidationHelper.GetString(GetValue("Image"), string.Empty); }
            set { SetValue("Image", value); }
        }

        public string ImageAltText
        {
            get { return ValidationHelper.GetString(GetValue("ImageAltText"), string.Empty); }
            set { SetValue("ImageAltText", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            DataBind();
        }

        protected string GetResizedImage(string imagePath, int imageWidth)
        {

            imagePath = URLHelper.AddParameterToUrl(imagePath, "width", imageWidth.ToString());
            imagePath = URLHelper.RemoveParameterFromUrl(imagePath, "height");
            imagePath = imagePath.TrimStart('~');

            return imagePath;
        }
        
    }
}