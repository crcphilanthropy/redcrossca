﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnlineForm.ascx.cs" Inherits="CMSApp.CRC.webparts.OnlineForm" %>
<asp:PlaceHolder runat="server" ID="phErrorLabel">
    <div class="form">
        <span class="EditingFormErrorLabel">
            <br />
            <cms:LocalizedLiteral runat="server" ID="litFormError" ResourceString="CRC.FormError"></cms:LocalizedLiteral>
        </span>
    </div>
</asp:PlaceHolder>
<cms:BizForm ID="viewBiz" runat="server" IsLiveSite="true" />
