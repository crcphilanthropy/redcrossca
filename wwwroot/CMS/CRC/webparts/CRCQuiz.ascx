<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRCQuiz.ascx.cs" Inherits="CMSApp.CRC.webparts.CRCQuiz" %>
<div id="quiz">
    <div id="intro" class="panel">
        <div id="intro-content">
            <asp:Literal runat="server" ID="ltrIntroduction"></asp:Literal>
        </div>
        <br />
        <a class="next button" href="#"><span>Begin</span></a>
    </div>

    <div id="quiz-content" class="panel">content</div>

    <div id="thankyou" class="panel">
        <div class="result">result</div>
        <br />
        <div class="thankyou">
            <asp:Literal runat="server" ID="ltrThankYou"></asp:Literal>
        </div>
    </div>
</div>

