﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.CRC.webparts.Controls
{
    public partial class NavTemplate : System.Web.UI.UserControl
    {
        public Repeater NavItemRepeater { get { return rptNavItems; } }
        public string PathToExpand { get; set; }
    }
}