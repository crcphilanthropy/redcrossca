﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavTemplate.ascx.cs" Inherits="CMSApp.CRC.webparts.Controls.NavTemplate" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:Repeater runat="server" ID="rptNavItems">
    <HeaderTemplate>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li class='<%# CMSApp.CRC.webparts.CRCLeftNav.GetActive(ValidationHelper.GetString(Eval("NodeAliasPath"),string.Empty),ValidationHelper.GetString(Eval("DocumentUrlPath"),string.Empty) ,PathToExpand) %> <%# Eval("DocumentMenuClass") %> level-<%# Eval("NodeLevel") %>'>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# !ValidationHelper.GetBoolean(Eval("DocumentMenuItemInactive"),false) %>'>
                <a href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath"), null, Eval("DocumentMenuRedirectUrl") as string) %>" <%# CMSApp.CRC.webparts.CRCLeftNav.GetOnClick(ValidationHelper.GetString(Eval("DocumentMenuJavascript"),string.Empty)) %>>
                    <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                </a>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%# ValidationHelper.GetBoolean(Eval("DocumentMenuItemInactive"),false) %>'>
                <span onclick="return false;" style=""><%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %></span>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phSubNav"></asp:PlaceHolder>
        </li>
    </ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
