<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="CMSApp.CRC.webparts.Controls.SearchResults" CodeBehind="SearchResults.ascx.cs" %>
<%@ Import Namespace="CMS.Search" %>

<p>
    <asp:Literal runat="server" ID="litTotalItems"></asp:Literal>
</p>

<div class="search-options">
    <asp:DropDownList runat="server" ID="ddlFilter" AutoPostBack="True">
        <asp:ListItem Value="">All</asp:ListItem>
        <asp:ListItem Value="+_type:cms.document">General</asp:ListItem>
        <asp:ListItem Value="+_myfiletype:pdf">PDF</asp:ListItem>
        <asp:ListItem Value="+_myfiletype:(doc docx xls xlsx)">Word/Excel</asp:ListItem>
        <asp:ListItem Value="+_myfiletype:(ppt pptx)">PowerPoint</asp:ListItem>
        <asp:ListItem Value="+classname:cms.blogpost">From the Blog</asp:ListItem>
    </asp:DropDownList>
    <asp:DropDownList runat="server" ID="ddlItemsPerPage" AutoPostBack="True">
        <asp:ListItem Value="10">10 Per Page</asp:ListItem>
        <asp:ListItem Value="20">20 Per Page</asp:ListItem>
        <asp:ListItem Text="50 Per Page" Value="50"></asp:ListItem>
        <asp:ListItem Text="100 Per Page" Value="100"></asp:ListItem>
    </asp:DropDownList>
</div>
<asp:Repeater runat="server" ID="rptRelevantLinks">
    <HeaderTemplate>
        <div class="recommended">
            <h5><%# ResHelper.GetString("CRC.RecommendedLinks") %></h5>
    </HeaderTemplate>
    <ItemTemplate>
        <article>
            <a href="<%# Eval("Url") %>">
                <h5><%# Eval("Title") %></h5>
            </a>
            <p><%# Eval("Description") %></p>
            <a class="link" href="<%# Eval("Url") %>"><%# Eval("Url") %></a>
        </article>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
<asp:Panel runat="server" ID="pnlSearchResults">
    <div class="pagination">
      <!--  <p class="current-page">
            <asp:Literal runat="server" ID="litDisplayingOfTop"></asp:Literal>
        </p>
        <p class="navigation">
            <cms:UniPager runat="server" ID="pgrSearch" PageControl="repSearchResults" PageSize="20">
                <PageNumbersSeparatorTemplate>&nbsp;&nbsp;</PageNumbersSeparatorTemplate>
                <PreviousPageTemplate><a href="<%# Eval("PreviousURL") %>">&lt;&lt;</a>&nbsp;</PreviousPageTemplate>
                <CurrentPageTemplate><%# Eval("Page") %></CurrentPageTemplate>
                <PageNumbersTemplate><a href="<%# Eval("PageURL") %>"><%# Eval("Page") %></a></PageNumbersTemplate>
                <NextPageTemplate>&nbsp;<a href="<%# Eval("NextURL") %>">&gt;&gt;</a></NextPageTemplate>
            </cms:UniPager>
        </p>-->
    </div>
    <div class="repeater">
        <asp:PlaceHolder runat="server" ID="plcBasicRepeater"></asp:PlaceHolder>
    </div>
    <cms:LocalizedLabel runat="server" ID="lblNoResults" CssClass="ContentLabel" Visible="false"
        EnableViewState="false" />
    <div class="pagination">
       <!-- <p class="current-page">
            <asp:Literal runat="server" ID="litDisplayingOfBottom"></asp:Literal>
        </p> -->
        <p class="navigation">
            <cms:UniPager runat="server" ID="pgrSearchBottom" PageControl="repSearchResults" PageSize="20">
                <PageNumbersSeparatorTemplate>&nbsp;</PageNumbersSeparatorTemplate>
                <PreviousPageTemplate><a href="<%# Eval("PreviousURL") %>">&lt;&lt;</a>&nbsp;</PreviousPageTemplate>
                <CurrentPageTemplate> <span> <%# Eval("Page") %> </span> </CurrentPageTemplate>
                <PageNumbersTemplate><a href="<%# Eval("PageURL") %>"><%# Eval("Page") %></a></PageNumbersTemplate>
                <NextPageTemplate>&nbsp;<a href="<%# Eval("NextURL") %>">&gt;&gt;</a></NextPageTemplate>
            </cms:UniPager>
        </p>
    </div>
    <cms:MessagesPlaceHolder ID="plcMess" runat="server" />
</asp:Panel>
