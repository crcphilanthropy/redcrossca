﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts.Controls
{
    public partial class CRCPager : UserControl
    {
        private int _stopIndex;

        protected int CurrentPageIndex = 0;
        public int CurrentPage
        {
            get { return CurrentPageIndex + 1; }
        }

        protected bool IsFirstPage
        {
            get { return CurrentPageIndex == 0; }
        }

        protected bool IsLastPage { get; set; }
        protected int PageCount { get; set; }


        protected PagedDataSource PagedDataSource;

        public Repeater Repeater { get; set; }
        public System.Collections.IEnumerable DataSource { get; set; }
        public int GroupSize { get; set; }
        public int PageSize { get; set; }
        public bool IsLateBinding { get; set; }

        private ScriptManager _scriptManager;
        public ScriptManager ScriptManager
        {
            get { return _scriptManager ?? (_scriptManager = ScriptManager.GetCurrent(Page)); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            rptPager.ItemDataBound += RptPagerItemDataBound;

            if (IsLateBinding)
            {
                btnPageChange.Click += (sender, args) =>
                    {
                        CurrentPageIndex = ValidationHelper.GetInteger(Request.Form["__EVENTARGUMENT"], 0);
                        TransformationHelper.RegisterStartUpScript("scrollUp();", Page, "scrollUp");
                    };
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (IsLateBinding && Visible)
                TransformationHelper.RegisterStartUpScript("InitPagerPostBack();", Page, "InitPagerPostBack");
        }

        void RptPagerItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            foreach (Control control in e.Item.Controls)
            {
                if (control is LinkButton)
                    ScriptManager.RegisterAsyncPostBackControl(control);
            }
        }

        public override void DataBind()
        {
            base.DataBind();
            if (DataSource == null) return;

            PagedDataSource = new PagedDataSource()
            {
                AllowPaging = true,
                DataSource = DataSource,
                PageSize = PageSize,
                CurrentPageIndex = CurrentPageIndex
            };

            Repeater.DataSource = PagedDataSource;
            Repeater.DataBind();

            var index = CurrentPageIndex + 1 - ((CurrentPageIndex) % GroupSize);
            if (index == 0) index++;

            _stopIndex = index + GroupSize;
            var pages = new List<int>();
            while (index < _stopIndex && index < PagedDataSource.PageCount + 1)
            {
                pages.Add(index++);
            }


            PageCount = PagedDataSource.PageCount;
            IsLastPage = PagedDataSource.IsLastPage;

            rptPager.DataSource = pages;
            rptPager.DataBind();

            rptPager.Visible = PagedDataSource.PageCount > 1;
            Repeater.Visible = PagedDataSource.DataSourceCount > 0;
        }

        public void DataBindWithoutDataSource(long totalNumberOfItems)
        {
            var index = CurrentPageIndex + 1 - ((CurrentPageIndex) % GroupSize);
            if (index == 0) index++;

            _stopIndex = index + GroupSize;
            var pages = new List<int>();

            PageCount = Convert.ToInt32(totalNumberOfItems / PageSize);
            if (totalNumberOfItems%PageSize > 0)
                PageCount += 1;

            while (index < _stopIndex && index <= PageCount)
            {
                pages.Add(index++);
            }

            IsLastPage = CurrentPageIndex == PageCount - 1;

            rptPager.DataSource = pages;
            rptPager.DataBind();

            rptPager.Visible = PageCount > 1;
        }

        protected void PagerCommand(Object sender, CommandEventArgs e)
        {
            CurrentPageIndex = ValidationHelper.GetInteger(e.CommandArgument, 0);
            TransformationHelper.RegisterStartUpScript("scrollUp();", Page, "scrollUp");
        }

        protected int GetPreviousGroup()
        {
            if (_stopIndex > GroupSize)
                return _stopIndex - (GroupSize + 2);

            return -1;
        }

        protected int GetNextGroup()
        {
            return PageCount - GroupSize >= _stopIndex - GroupSize ? _stopIndex - 1 : -1;
        }
    }
}