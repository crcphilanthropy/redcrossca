﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRCPager.ascx.cs" Inherits="CMSApp.CRC.webparts.Controls.CRCPager" %>
<div class="pre-loader" style="display: none;">
    <img src="/CRC/images/pre-loader.svg" alt=""/>
</div>
<asp:Repeater runat="server" ID="rptPager">
    <HeaderTemplate>
        <div class="pagination-wrap text-center">
            <ul class="pagination">
                <li class="arrow <%# IsFirstPage ? "unavailable" : "" %>">
                    <asp:LinkButton runat="server" CommandName="Page" CommandArgument="<%# IsFirstPage ? 0 : CurrentPageIndex - 1 %>" OnCommand="PagerCommand" data-page="<%# IsFirstPage ? 0 : CurrentPageIndex - 1 %>">
                    &laquo;
                    </asp:LinkButton>
                </li>
                <li class="unavailable dots">
                    <asp:LinkButton runat="server" CommandName="Page" CommandArgument="<%# GetPreviousGroup() %>" OnCommand="PagerCommand" Visible='<%# GetPreviousGroup() > -1 %>' data-page="<%# GetPreviousGroup() %>">
                    &hellip;
                    </asp:LinkButton>
                </li>
    </HeaderTemplate>
    <ItemTemplate>
        <li class="<%# Convert.ToInt32(Container.DataItem) - 1 == CurrentPageIndex ? "current" : "" %>">
            <a runat="server" visible='<%# Convert.ToInt32(Container.DataItem) - 1 == CurrentPageIndex %>'><%# Container.DataItem %></a>
            <asp:LinkButton ID="lnkPagerItem" runat="server" Visible='<%# Convert.ToInt32(Container.DataItem) - 1 != CurrentPageIndex %>' CommandName="Page" CommandArgument="<%# Convert.ToInt32(Container.DataItem) - 1 %>" OnCommand="PagerCommand" data-page="<%# Convert.ToInt32(Container.DataItem) - 1 %>"><%# Container.DataItem %></asp:LinkButton>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        <li class="unavailable dots">
            <asp:LinkButton runat="server" CommandName="Page" CommandArgument="<%# GetNextGroup() %>" OnCommand="PagerCommand" Visible='<%# GetNextGroup() > -1 %>' data-page="<%# GetNextGroup() %>">
                    &hellip;
            </asp:LinkButton>
        </li>
        <li class="arrow <%# IsLastPage ? "unavailable" : "" %>">
            <asp:LinkButton runat="server" CommandName="Page" CommandArgument="<%#  IsLastPage ? PageCount - 1 : CurrentPageIndex + 1 %>" OnCommand="PagerCommand" data-page="<%#  IsLastPage ? PageCount - 1 : CurrentPageIndex + 1 %>">
                    &raquo;
            </asp:LinkButton>
        </li>
        </ul></div>
    </FooterTemplate>
</asp:Repeater>
<asp:Button runat="server" ID="btnPageChange" Style="display: none;" />

<script type="text/javascript">
    function InitPagerPostBack() {
        $('.pagination li a').click(function () {

            if (($(this).data('page') != undefined && $(this).data('page') != '') || $(this).data('page') == 0) {
                $('.pre-loader').show();
                __doPostBack("<%= btnPageChange.UniqueID %>", $(this).data('page'));
            }
            return false;
        });
    }
</script>
