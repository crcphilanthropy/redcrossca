﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuizBuilder.ascx.cs" Inherits="CMSApp.CRC.webparts.QuizBuilder" %>
<script type="text/javascript">
    var quizQuestions = <asp:Literal runat="server" id="ltrQuizQuestions"></asp:Literal>;
 //   var quizIntroductionText = "<asp:Literal runat="server" id="ltrIntroduction"></asp:Literal>";
 //   var quizThankYouText = "<asp:Literal runat="server" id="ltrThankYou"></asp:Literal>";
    var quizBeginButtonText = "<asp:Literal runat="server" id="ltrBegin"></asp:Literal>";
    var quizId = <asp:Literal runat="server" id="ltrQuizId"></asp:Literal>;
    // localized strings here
    var quizNextText = '<cms:LocalizedLiteral runat="server" ResourceString="CRC.QuizNext"></cms:LocalizedLiteral>';
    var quizCompletedText = '<cms:LocalizedLiteral ResourceString="CRC.QuizTotals" runat="server"></cms:LocalizedLiteral>';
    //initQuiz(quizQuestions);
</script>

