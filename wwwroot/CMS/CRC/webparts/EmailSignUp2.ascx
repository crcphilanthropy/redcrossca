﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSignUp2.ascx.cs" Inherits="CMSApp.CRC.webparts.EmailSignUp2" %>
<asp:Panel runat="server" DefaultButton="btnSignUp2" ID="pnlEmail" CssClass="standard-column font-size grey-callout" ClientIDMode="Static">
    <div class="email-cta" id="email">
        
        <img class="email-signup-icon" src="~/CRC/images/icons/i-icon.png" />
            <%# (CurrentDocument.NodeAlias =="Home") ? "<h2 class='email-signup-title'>"+ Title +"</h2>" : "<h5>"+Title+"</h5>" %>
        <p class="email-signup-intro"><%# ResHelper.GetString("CRC.Form.EmailSubtitle") %> </p>
        <input type="text" placeholder='<%# ResHelper.GetString("CRC.EnterYourEmailHere") %>' runat="server" id="SignUpEmailTextBox2" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="EmailSignUp2" class="trim" /><br />
        <div class="error-group">
            <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailTextBox2" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUp2" CssClass="text-error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailTextBox2" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUp2" CssClass="text-error" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
        </div>
        <asp:Button runat="server" ID="btnSignUp2" CssClass="submit-btn email-signup-btn" ValidationGroup="EmailSignUp2" />
    </div>
</asp:Panel>
