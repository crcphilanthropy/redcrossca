using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.Helpers;
using CMS.Localization;
namespace CMSApp.CRC.webparts
{
    public partial class CRCServicesListMenu : CMSAbstractWebPart
    {
        #region "Document properties"

        /// <summary>
        /// Gets or sets the value that indicates whether text can be wrapped or space is replaced with non breakable space.
        /// </summary>
        public bool WordWrap
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("WordWrap"), menuElem.WordWrap);
            }
            set
            {
                this.SetValue("WordWrap", value);
                menuElem.WordWrap = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether document menu item properties are applied.
        /// </summary>
        public bool ApplyMenuDesign
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("ApplyMenuDesign"), menuElem.ApplyMenuDesign);
            }
            set
            {
                this.SetValue("ApplyMenuDesign", value);
                menuElem.ApplyMenuDesign = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether item image is displayed for highlighted item when highlighted image is not specified.
        /// </summary>
        public bool UseItemImagesForHiglightedItem
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("UseItemImagesForHiglightedItem"), menuElem.UseItemImagesForHiglightedItem);
            }
            set
            {
                this.SetValue("UseItemImagesForHiglightedItem", value);
                menuElem.UseItemImagesForHiglightedItem = value;
            }
        }


        /// <summary>
        /// Gets or sets the cache minutes.
        /// </summary>
        public override int CacheMinutes
        {
            get
            {
                return base.CacheMinutes;
            }
            set
            {
                base.CacheMinutes = value;
                menuElem.CacheMinutes = value;
            }
        }


        /// <summary>
        /// Gets or sets the cache item dependencies.
        /// </summary>
        public override string CacheDependencies
        {
            get
            {
                return base.CacheDependencies;
            }
            set
            {
                base.CacheDependencies = value;
                menuElem.CacheDependencies = value;
            }
        }


        /// <summary>
        /// Gets or sets the name of the cache item. If not explicitly specified, the name is automatically 
        /// created based on the control unique ID
        /// </summary>
        public override string CacheItemName
        {
            get
            {
                return base.CacheItemName;
            }
            set
            {
                base.CacheItemName = value;
                menuElem.CacheItemName = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether permissions are checked.
        /// </summary>
        public bool CheckPermissions
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("CheckPermissions"), menuElem.CheckPermissions);
            }
            set
            {
                
                this.SetValue("CheckPermissions", value);
               menuElem.CheckPermissions = value;
            }
        }


        /// <summary>
        /// Gets or sets the class names.
        /// </summary>
        public string ClassNames
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Classnames"), menuElem.ClassNames), menuElem.ClassNames);
            }
            set
            {
                this.SetValue("ClassNames", value);
                menuElem.ClassNames = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether selected documents are combined with default culture.
        /// </summary>
        public bool CombineWithDefaultCulture
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("CombineWithDefaultCulture"), menuElem.CombineWithDefaultCulture);
            }
            set
            {
                this.SetValue("CombineWithDefaultCulture", value);
                menuElem.CombineWithDefaultCulture = value;
            }
        }


        /// <summary>
        /// Gets or sets the culture code.
        /// </summary>
        public string CultureCode
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("CultureCode"), menuElem.CultureCode), menuElem.CultureCode);
            }
            set
            {
                this.SetValue("CultureCode", value);
                menuElem.CultureCode = value;
            }
        }


        /// <summary>
        /// Gets or sets the maximal relative level.
        /// </summary>
        public int MaxRelativeLevel
        {
            get
            {
                return ValidationHelper.GetInteger(this.GetValue("MaxRelativeLevel"), menuElem.MaxRelativeLevel);
            }
            set
            {
                this.SetValue("MaxRelativeLevel", value);
                menuElem.MaxRelativeLevel = value;
            }
        }


        /// <summary>
        /// Gets or sets order by clause.
        /// </summary>
        public string OrderBy
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OrderBy"), menuElem.OrderBy), menuElem.OrderBy);
            }
            set
            {
                this.SetValue("OrderBy", value);
                menuElem.OrderBy = value;
            }
        }


        /// <summary>
        /// Gets or sets the nodes path.
        /// </summary>
        public string Path
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Path"), menuElem.Path), menuElem.Path);
            }
            set
            {
                this.SetValue("Path", value);
                menuElem.Path = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether selected documents must be published.
        /// </summary>
        public bool SelectOnlyPublished
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("SelectOnlyPublished"), menuElem.SelectOnlyPublished);
            }
            set
            {
                this.SetValue("SelctOnlyPublished", value);
                menuElem.SelectOnlyPublished = value;
            }
        }


        /// <summary>
        /// Gets or sets the site name.
        /// </summary>
        public string SiteName
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("SiteName"), menuElem.SiteName), menuElem.SiteName);
            }
            set
            {
                this.SetValue("SiteName", value);
                menuElem.SiteName = value;
            }
        }


        /// <summary>
        /// Gets or sets the where condition.
        /// </summary>
        public string WhereCondition
        {
            get
            {
                return DataHelper.GetNotEmpty(this.GetValue("WhereCondition"), menuElem.WhereCondition);
            }
            set
            {
                this.SetValue("WhereCondition", value);
                menuElem.WhereCondition = value;
            }
        }

        #endregion


        #region "Public properties"

        /// <summary>
        /// Gets or sets the css prefix. For particular levels can be used several values separated with semicolon (;).
        /// </summary>
        public string CSSPrefix
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("CSSPrefix"), menuElem.CSSPrefix), menuElem.CSSPrefix);
            }
            set
            {
                this.SetValue("CSSPrefix", value);
                menuElem.CSSPrefix = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicate whether all items in path will be highlighted.
        /// </summary>
        public bool HighlightAllItemsInPath
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("HighlightAllItemsInPath"), menuElem.HighlightAllItemsInPath);
            }
            set
            {
                this.SetValue("HighlightAllItemsInPath", value);
                menuElem.HighlightAllItemsInPath = value;
            }
        }


        /// <summary>
        /// Gets or sets the nodes path which indicates path, where items in this path are highligted (at default current alias path).
        /// </summary>
        public string HighlightedNodePath
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("HighlightedNodePath"), menuElem.HighlightedNodePath), menuElem.HighlightedNodePath);
            }
            set
            {
                this.SetValue("HighlightedNodePath", value);
                menuElem.HighlightedNodePath = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether alternating styles are used.
        /// </summary>
        public bool UseAlternatingStyles
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("UseAlternatingStyles"), menuElem.UseAlternatingStyles);
            }
            set
            {
                this.SetValue("UseAlternatingStyles", value);
                menuElem.UseAlternatingStyles = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether highlighted item is displayed as link.
        /// </summary>
        public bool DisplayHighlightedItemAsLink
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("DisplayHighlightedItemAsLink"), menuElem.DisplayHighlightedItemAsLink);
            }
            set
            {
                this.SetValue("DisplayHighlightedItemAsLink", value);
                menuElem.DisplayHighlightedItemAsLink = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether only submenu under highlighted item could be render, otherwise all submenus are rendered.
        /// </summary>
        public bool DisplayOnlySelectedPath
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("DisplayOnlySelectedPath"), menuElem.DisplayOnlySelectedPath);
            }
            set
            {
                this.SetValue("DisplayOnlySelectedPath", value);
                menuElem.DisplayOnlySelectedPath = value;
            }
        }


        /// <summary>
        /// Gets or sets the first item CSS class.
        /// </summary>
        public string FirstItemCssClass
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("FirstItemCssClass"), menuElem.FirstItemCssClass), menuElem.FirstItemCssClass);
            }
            set
            {
                this.SetValue("FirstItemCssClass", value);
                menuElem.FirstItemCssClass = value;
            }
        }


        /// <summary>
        /// Gets or sets last item CSS class.
        /// </summary>
        public string LastItemCssClass
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("LastItemCssClass"), menuElem.LastItemCssClass), menuElem.LastItemCssClass);
            }
            set
            {
                this.SetValue("LastItemCssClass", value);
                menuElem.LastItemCssClass = value;
            }
        }


        /// <summary>
        /// Gets or sets onmouseout script.
        /// </summary>
        public string OnMouseOutScript
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OnMouseOutScript"), menuElem.OnMouseOutScript), menuElem.OnMouseOutScript);
            }
            set
            {
                this.SetValue("OnMouseOutScript", value);
                menuElem.OnMouseOutScript = value;
            }
        }


        /// <summary>
        /// Gets or sets onmouseover script.
        /// </summary>
        public string OnMouseOverScript
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OnMouseOverScript"), menuElem.OnMouseOverScript), menuElem.OnMouseOverScript);
            }
            set
            {
                this.SetValue("OnMouseOverScript", value);
                menuElem.OnMouseOverScript = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether CSS classes will be rendered.
        /// </summary>
        public bool RenderCssClasses
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("RenderCssClasses"), menuElem.RenderCssClasses);
            }
            set
            {
                this.SetValue("RenderCssClasses", value);
                menuElem.RenderCssClasses = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether item ID will be rendered.
        /// </summary>
        public bool RenderItemID
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("RenderItemID"), menuElem.RenderItemID);
            }
            set
            {
                this.SetValue("RenderItemID", value);
                menuElem.RenderItemID = value;
            }
        }


        /// <summary>
        /// Gets or sets the url to the image which is assigned as sub menu indicator.
        /// </summary>
        public string SubMenuIndicator
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("SubmenuIndicator"), menuElem.SubmenuIndicator), menuElem.SubmenuIndicator);
            }
            set
            {
                this.SetValue("SubmenuIndicator", value);
                menuElem.SubmenuIndicator = value;
            }
        }


        /// <summary>
        /// Gets or sets the link url target.
        /// </summary>
        public string UrlTarget
        {
            get
            {
                return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("UrlTarget"), menuElem.UrlTarget), menuElem.UrlTarget);
            }
            set
            {
                this.SetValue("UrlTarget", value);
                menuElem.UrlTarget = value;
            }
        }


        /// <summary>
        /// Gets or sets the css class name, which create envelope around menu and automatically register LIHover.htc script (IE6 required this).
        /// </summary>
        public string HoverCssClassName
        {
            get
            {
                return DataHelper.GetNotEmpty(this.GetValue("HoverCSSClassName"), menuElem.HoverCSSClassName);
            }
            set
            {
                this.SetValue("HoverCSSClassName", value);
                menuElem.HoverCSSClassName = value;
            }
        }


        /// <summary>
        /// Gets or sets the item ID prefix.
        /// </summary>
        public string ItemIDPrefix
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue("ItemIdPrefix"), menuElem.ItemIdPrefix);
            }
            set
            {
                this.SetValue("ItemIdPrefix", value);
                menuElem.ItemIdPrefix = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether alternate text for image will be rendered.
        /// </summary>
        public bool RenderImageAlt
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("RenderImageAlt"), menuElem.RenderImageAlt);
            }
            set
            {
                this.SetValue("RenderImageAlt", value);
                menuElem.RenderImageAlt = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether link title will be rendered.
        /// </summary>
        public bool RenderLinkTitle
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("RenderLinkTitle"), menuElem.RenderLinkTitle);
            }
            set
            {
                this.SetValue("RenderLinkTitle", value);
                menuElem.RenderLinkTitle = value;
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether control should be hidden if no data found.
        /// </summary>
        public bool HideControlForZeroRows
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("HideControlForZeroRows"), menuElem.HideControlForZeroRows);
            }
            set
            {
                this.SetValue("HideControlForZeroRows", value);
                menuElem.HideControlForZeroRows = value;
            }
        }


        /// <summary>
        /// Gets or sets the text which is displayed for zero rows results.
        /// </summary>
        public string ZeroRowsText
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue("ZeroRowsText"), menuElem.ZeroRowsText);
            }
            set
            {
                this.SetValue("ZeroRowsText", value);
                menuElem.ZeroRowsText = value;
            }
        }


        /// <summary>
        /// Filter name.
        /// </summary>
        public string FilterName
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue("FilterName"), menuElem.FilterName);
            }
            set
            {
                this.SetValue("FilterName", value);
                menuElem.FilterName = value;
            }
        }


        /// <summary>
        /// Gets or sets property which indicates if menu caption should be HTML encoded.
        /// </summary>
        public bool EncodeMenuCaption
        {
            get
            {
                return ValidationHelper.GetBoolean(this.GetValue("EncodeMenuCaption"), menuElem.EncodeMenuCaption);
            }
            set
            {
                this.SetValue("EncodeMenuCaption", value);
                menuElem.EncodeMenuCaption = value;
            }
        }


        /// <summary>
        /// Gets or sets the columns to be retrieved from database.
        /// </summary>  
        public string Columns
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue("Columns"), menuElem.Columns);
            }
            set
            {
                this.SetValue("Columns", value);
                menuElem.Columns = value;
            }
        }

        #endregion


        /// <summary>
        /// Content loaded event handler.
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
        }


        /// <summary>
        /// Reloads data.
        /// </summary>
        public override void ReloadData()
        {
            base.ReloadData();
            SetupControl();
            menuElem.ReloadData(true);
        }


        /// <summary>
        /// Initializes the control properties.
        /// </summary>
        protected void SetupControl()
        {
            if (this.StopProcessing)
            {
                menuElem.StopProcessing = true;
            }
            else
            {
                menuElem.ControlContext = this.ControlContext;

                // Set properties from Webpart form
                menuElem.ApplyMenuDesign = this.ApplyMenuDesign;
                menuElem.UseItemImagesForHiglightedItem = this.UseItemImagesForHiglightedItem;
                menuElem.CacheItemName = this.CacheItemName;
                menuElem.CacheDependencies = this.CacheDependencies;
                menuElem.CacheMinutes = this.CacheMinutes;
                menuElem.CheckPermissions = this.CheckPermissions;
                menuElem.ClassNames = this.ClassNames;
                menuElem.MaxRelativeLevel = this.MaxRelativeLevel;
                menuElem.OrderBy = this.OrderBy;
                menuElem.Path = TransformationHelper.GetRegionPath(LocalizationContext.PreferredCultureCode);
                menuElem.SelectOnlyPublished = this.SelectOnlyPublished;
                menuElem.SiteName = this.SiteName;
                menuElem.WhereCondition = this.WhereCondition;
                menuElem.CultureCode = this.CultureCode;
                menuElem.CombineWithDefaultCulture = this.CombineWithDefaultCulture;

                menuElem.CSSPrefix = this.CSSPrefix;
                menuElem.HighlightAllItemsInPath = this.HighlightAllItemsInPath;
                menuElem.HighlightedNodePath = this.HighlightedNodePath;

                menuElem.DisplayHighlightedItemAsLink = this.DisplayHighlightedItemAsLink;
                menuElem.DisplayOnlySelectedPath = this.DisplayOnlySelectedPath;
                menuElem.FirstItemCssClass = this.FirstItemCssClass;
                menuElem.LastItemCssClass = this.LastItemCssClass;
                menuElem.HoverCSSClassName = this.HoverCssClassName;

                menuElem.OnMouseOutScript = this.OnMouseOutScript;
                menuElem.OnMouseOverScript = this.OnMouseOverScript;

                menuElem.RenderCssClasses = this.RenderCssClasses;
                menuElem.RenderItemID = this.RenderItemID;
                menuElem.ItemIdPrefix = this.ItemIDPrefix;
                menuElem.SubmenuIndicator = this.SubMenuIndicator;
                menuElem.UrlTarget = this.UrlTarget;
                menuElem.UseAlternatingStyles = this.UseAlternatingStyles;
                menuElem.RenderLinkTitle = this.RenderLinkTitle;
                menuElem.RenderImageAlt = this.RenderImageAlt;

                menuElem.WordWrap = this.WordWrap;

                menuElem.HideControlForZeroRows = this.HideControlForZeroRows;
                menuElem.ZeroRowsText = this.ZeroRowsText;
                menuElem.EncodeMenuCaption = this.EncodeMenuCaption;

                menuElem.FilterName = this.FilterName;

                menuElem.Columns = this.Columns;
            }
        }


        /// <summary>
        /// OnPrerender override (Set visibility).
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.Visible = menuElem.Visible;

            if (DataHelper.DataSourceIsEmpty(menuElem.DataSource) && (menuElem.HideControlForZeroRows))
            {
                this.Visible = false;
            }
        }
    }
}