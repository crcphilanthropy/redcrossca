﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvinceSelection.ascx.cs" Inherits="CMSApp.CRC.webparts.ProvinceSelection" %>
<div class="province-dd-wrap">
    <ul>
        <li class="province-dd">
            <asp:PlaceHolder runat="server" ID="phHeaderSelection" Visible="False">
                <asp:Literal runat="server" ID="litYourProvince"></asp:Literal>:
                <a tabindex="15">
                    <strong>
                        <asp:Literal runat="server" ID="litProvince"></asp:Literal></strong>
                    <span>
                        <img src="/CRC/images/arrow.png" />
                    </span>
                </a>
                <div class="sub-nav">
                    <ul class="sub-nav-group clearfix">
                        <asp:Repeater runat="server" ID="rptProvinces">
                            <ItemTemplate>
                                <li>
                                    <asp:LinkButton runat="server" CssClass="cp-track" CommandName="update" CommandArgument='<%# Eval("ProvinceCode") %>' OnCommand="UpdateProvince" data-event="generic-event" data-category="province selector" data-action="province change" data-label='<%# Eval("ProvinceCode") %>'><%# Eval("Name") %></asp:LinkButton>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phFooterSelection" Visible="False">
                <div class="btn-province-dd">
                    <span>
                        <asp:Literal runat="server" ID="litFoooterYourProvince"></asp:Literal>:</span>
                    <a class="">
                        <strong>
                            <asp:Literal runat="server" ID="litFoooterProvince"></asp:Literal></strong>
                    </a>
                </div>
                <ul>
                    <asp:Repeater runat="server" ID="rptFooterProvinces">
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="cp-track" CommandName="update" CommandArgument='<%# Eval("ProvinceCode") %>' OnCommand="UpdateProvince" data-event="generic-event" data-category="province selector" data-action="province change" data-label='<%# Eval("ProvinceCode") %>'><%# Eval("ProvinceDisplayCode") %></asp:LinkButton>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </asp:PlaceHolder>
        </li>
    </ul>
</div>
