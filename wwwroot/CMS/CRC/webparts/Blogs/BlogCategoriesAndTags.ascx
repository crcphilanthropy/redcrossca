﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogCategoriesAndTags.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogCategoriesAndTags" %>
<%@ Import Namespace="CMSApp.CRC" %>

<div class="post-footer">
    <div class="post-linked-info">
        <asp:Repeater runat="server" ID="rptCategories">
            <HeaderTemplate>
                <div class="post-categories">
                    <ul class="horizontal-list">
                        <li>
                            <div class="header">
                                <span><%# ResHelper.GetString("CRC.Blog.Categories") %></span>:
                            </div>
                        </li>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <a href="<%# TransformationHelper.GetBlogFilterPath("category",EvalText("CategoryName"),BlogNode) %>">
                        <span><%# Eval("CategoryDisplayName") %></span>
                    </a>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
                </div>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" ID="rptTags">
            <HeaderTemplate>

                <div class="post-tags">

                    <ul class="horizontal-list">
                        <li>
                            <div class="header">
                                <span><%# ResHelper.GetString("CRC.Blog.Tags") %></span>:
                            </div>
                        </li>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <a href="<%# TransformationHelper.GetBlogFilterPath("tag",EvalText("TagName"),BlogNode) %>">
                        <span><%# ToTitleCase(EvalText("TagName")) %></span>
                    </a>
                </li>
            </ItemTemplate>
            <FooterTemplate></ul></div></FooterTemplate>
        </asp:Repeater>
    </div>
</div>

