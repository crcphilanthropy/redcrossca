﻿using System;
using System.Collections.Generic;
using CMS.Helpers;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class BlogPager : System.Web.UI.UserControl
    {
        private int _stopIndex;
        public int CurrentPage
        {
            get { return QueryHelper.GetInteger("page", 1); }
        }

        protected bool IsFirstPage
        {
            get { return CurrentPage == 1; }
        }

        protected bool IsLastPage { get; set; }
        protected int PageCount { get; set; }
        public int GroupSize { get; set; }
        public int PageSize { get; set; }

        protected int GetPreviousGroup()
        {
            if (_stopIndex > GroupSize)
                return _stopIndex - (GroupSize + 1);

            return -1;
        }

        protected int GetNextGroup()
        {
            return PageCount - GroupSize >= _stopIndex - GroupSize ? _stopIndex : -1;
        }

        public void DataBind(long totalNumberOfItems)
        {
            base.DataBind();
            var index = CurrentPage - ((CurrentPage - 1) % GroupSize);
            if (index == 0) index++;

            _stopIndex = index + GroupSize;
            var pages = new List<int>();

            PageCount = Convert.ToInt32(totalNumberOfItems / PageSize);
            if (totalNumberOfItems % PageSize > 0)
                PageCount += 1;

            while (index < _stopIndex && index <= PageCount)
            {
                pages.Add(index++);
            }

            IsLastPage = CurrentPage == PageCount;

            rptPager.DataSource = pages;
            rptPager.DataBind();

            rptPager.Visible = PageCount > 1;
        }
    }
}