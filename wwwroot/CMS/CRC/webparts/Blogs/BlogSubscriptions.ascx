<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogSubscriptions.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogSubscriptions" %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Register Src="~/CMSWebParts/Syndication/Documents/CMSRSSFeed.ascx" TagPrefix="uc1" TagName="CMSRSSFeed" %>
<%@ Import Namespace="CMS.Localization" %>

<div class="cta with-theme transparent">
    <div class="body">
        <ul class="info-links">
            <li><a class="icon-link large hide-for-medium-down uppercase rss" href="<%# TransformationHelper.GetSubSiteNode().DocumentUrlPath %>?rss=feed">
                <span><%# ResHelper.GetString("CRC.Blog.RssFeed") %></span>
            </a></li>
            <li><a class="icon-link large uppercase subscribe" href="http://feedburner.google.com/fb/a/mailverify?uri=<%# LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? FrenchFeedBurner : EnglishFeedBurner %>&amp;loc=<%# LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? "fr-Fr" : LocalizationContext.PreferredCultureCode %>" target="_blank"><span><%# ResHelper.GetString("CRC.Blog.SubscribeToBlog") %></span> </a></li>
        </ul>
    </div>
</div>
<span style="display: none;">
    <uc1:CMSRSSFeed runat="server" ID="feed" CssClass="icon-link large uppercase subscribe" AdditonalCssClass="icon-link large uppercase rss" TransformationName="cms.blogpost.rssitem" ClassNames="cms.blogpost" OrderBy="BlogPostDate DESC" SelectOnlyPublished="True" MaxRelativeLevel="-1" Path="/Blog/%" QueryStringKey="rss" SelectTopN="50" />
</span>
