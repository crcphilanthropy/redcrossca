using System;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.DocumentEngine;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class Gallery : CMSAbstractWebPart
    {
        public bool ShowDescription
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowDescription"), false); }
            set { SetValue("ShowDescription", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            var data = TreeHelper.SelectNodes(string.Format("{0}/%", DocumentContext.CurrentDocument.NodeAliasPath), false,
                                              "CRC.BlogPostImage", string.Empty, "NodeOrder ASC", -1, true);

            rptTabs.DataSource = data;
            rptSlides.DataSource = data;
            DataBind();
        }
    }
}