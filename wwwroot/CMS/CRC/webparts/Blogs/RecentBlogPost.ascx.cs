using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class FromTheBlog : CMSAbstractWebPart
    {
        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
            set { SetValue("Title", value); }
        }

        public int NumberOfItems
        {
            get { return ValidationHelper.GetInteger(GetValue("NumberOfItems"), 3); }
            set { SetValue("NumberOfItems", value); }
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            base.OnPreRender(e);
            //rptBlogPosts.SelectTopN = NumberOfItems;
            //rptBlogPosts.Path = "/Blog/%";
            //rptBlogPosts.ClassNames = "CMS.BlogPost";
            //rptBlogPosts.SelectOnlyPublished = true;
            //rptBlogPosts.CombineWithDefaultCulture = false;
            //rptBlogPosts.OrderBy = "BlogPostDate DESC";
            //rptBlogPosts.SiteName = CMSContext.CurrentSiteName;
            rptBlogPosts.TransformationName = "crc.blogyear.blogpostrightraillistitem";

            var items = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/Blog/%", LocalizationContext.PreferredCultureCode,
                                                false, "CMS.BlogPost", string.Empty, "BlogPostDate DESC", -1, true,
                                                NumberOfItems);
            rptBlogPosts.DataSource = items;
            rptBlogPosts.DataBind();
            DataBind();
        }
    }
}