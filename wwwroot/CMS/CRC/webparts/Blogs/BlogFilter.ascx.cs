using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.Controls;
using CMS.EventLog;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using System.Web;
using CMS.Helpers;
using CMS.Taxonomy;
using CMS.Membership;
using CMS.DocumentEngine;
using CMS.Localization;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class BlogFilter : CMSAbstractQueryFilterControl
    {

        private string _pageTitle = string.Empty;
        private string _metaDescription = string.Empty;
        private readonly List<string> _monthDocTypes = new List<string> { "CRC.BlogYear", "CMS.BlogMonth" };
        private string _pageNumberTitle = string.Empty;

        protected override void OnInit(EventArgs e)
        {
            var pageNumber = QueryHelper.GetInteger("page", 0);

            if (pageNumber > 1)
                _pageNumberTitle = string.Format(" ({0} {1})", ResourceStringHelper.GetString("CRC.Page", defaultText: "Page"), pageNumber);

            SetFilter();
            SetMetaData();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            SetMetaData();
            base.OnPreRender(e);
        }

        private void SetFilter()
        {
            var listOfFilters = new List<string>();

            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("category", string.Empty)))
            {
                var category = CategoryInfoProvider.GetCategoryInfo(QueryHelper.GetString("category", string.Empty), SiteContext.CurrentSiteName);
                if (category != null)
                {
                    if (category.CategoryLevel == 0)
                        RedirectToHome();

                    litTitle.Text = string.Format("{0}{1}", category.CategoryDisplayName, _pageNumberTitle);
                    litDescription.Text = category.CategoryDescription;
                    Page.Header.Controls.Add(new Literal { Text = string.Format("<meta name='category' content=\"{0}\" />", category.CategoryDisplayName) });

                    phFilterTitle.Visible = true;
                    listOfFilters.Add(string.Format("DocumentID in (select dc.DocumentID from CMS_DocumentCategory dc where dc.documentID = DocumentID AND dc.CategoryID = {0})", category.CategoryID));

                    _pageTitle = string.Format("{0} - {2}{1}", category.CategoryDisplayName, _pageNumberTitle, ResHelper.GetString("CRC.Blog.SiteName"));
                    _metaDescription = category.CategoryDescription;
                }
                else
                {
                    RedirectToError();
                }
            }

            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("author", string.Empty)))
            {
                var user = UserInfoProvider.GetUserInfo(QueryHelper.GetString("author", string.Empty));
                if (user != null)
                {
                    litTitle.Text = string.Format("{0}{1}", user.FullName, _pageNumberTitle);
                    litDescription.Text = user.UserSignature;
                    phFilterTitle.Visible = true;
                    Page.Header.Controls.Add(new Literal
                        {
                            Text = string.Format("\r\n<meta name='author' content=\"{0}\" />\r\n", user.FullName)
                        });

                    _pageTitle = string.Format("{0} - {2}{1}", user.FullName, _pageNumberTitle, ResHelper.GetString("CRC.Blog.SiteName"));
                    _metaDescription = user.UserDescription;
                }
                else
                {
                    RedirectToError();
                }

                listOfFilters.Add(string.Format("NodeOwnerUserName = N'{0}'", Server.UrlDecode(QueryHelper.GetString("author", string.Empty))));
            }

            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("tag", string.Empty)))
            {
                var tag = QueryHelper.GetString("tag", string.Empty);
                var decodedTag = HttpUtility.UrlDecode(tag);
                var queryTags = decodedTag.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                var firstOrDefault = queryTags.FirstOrDefault();
                var queryList = new List<string>();
                if (firstOrDefault != null)
                {
                    var lastOrDefault = queryTags.LastOrDefault();
                    if (lastOrDefault != null)
                    {
                        queryList = new List<string>
                                {
                                    string.Format("TagName like '{0}%'", firstOrDefault.Replace("'", "''")),
                                    string.Format("TagName like '%{0}'", lastOrDefault.Replace("'", "''"))
                                };
                        queryTags.ForEach(item => queryList.Add(string.Format("TagName like '%{0}%'", item.Replace("'", "''"))));
                    }
                }

                var tags = TagInfoProvider.GetTags(string.Format("({0}) and TagGroupID = {1}", queryList.Join(" AND "), TagGroup.TagGroupID), string.Empty, 1);
                if (!DataHelper.DataSourceIsEmpty(tags))
                {
                    litTitle.Text = string.Format("{0}{1}",
                        System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ValidationHelper.GetString(tags.Tables[0].Rows[0]["TagName"], decodedTag)),
                        _pageNumberTitle);

                    phFilterTitle.Visible = true;
                    Page.Header.Controls.Add(new Literal
                        {
                            Text = string.Format("\r\n<meta name='category' content=\"{0}\" />\r\n", litTitle.Text)
                        });
                    listOfFilters.Add(
                        string.Format(
                            "DocumentID in (Select dt.DocumentID from CMS_DocumentTag dt where dt.DocumentID = DocumentID AND dt.TagId = {0})",
                            tags.Tables[0].Rows[0]["TagID"]));
                    _pageTitle = string.Format("{0}{1} - {2}", tags.Tables[0].Rows[0]["TagName"], _pageNumberTitle,
                                               ResHelper.GetString("CRC.Blog.SiteName"));
                }
                else
                {
                    RedirectToError();
                }
            }

            WhereCondition = listOfFilters.Join(" AND ");

            RaiseOnFilterChanged();
        }

        private void RedirectToHome()
        {
            try
            {
                Response.RedirectPermanent(LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase) ? "/blogue" : "/blog", true);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogWarning("BlogFilter", "RedirectToHome", ex, SiteContext.CurrentSiteID, null);
            }
        }

        private void RedirectToError()
        {
            throw new HttpException(404, "Not Found");
        }

        private void SetMetaData()
        {
            if (_monthDocTypes.Contains(DocumentContext.CurrentDocument.ClassName, new StringEqualityComparer()))
            {
                var blogMonthStartingDate = DocumentContext.CurrentDocument.GetDateTimeValue("BlogMonthStartingDate", DateTime.MinValue);
                if (blogMonthStartingDate > DateTime.MinValue)
                {
                    var month = blogMonthStartingDate.ToString("MMMM yyyy", System.Globalization.CultureInfo.GetCultureInfo(LocalizationContext.PreferredCultureCode));
                    _pageTitle = string.Format("{0} - {2}{1}", month, _pageNumberTitle, ResHelper.GetString("CRC.Blog.SiteName"));
                    litTitle.Text = string.Format("{0}{1}", month, _pageNumberTitle);
                    phFilterTitle.Visible = true;

                }
            }

            if (!String.IsNullOrWhiteSpace(_pageTitle))
            {
                Page.Title = _pageTitle;
                DocumentContext.CurrentTitle = _pageTitle;
            }
            else
            {
                phFilterTitle.Visible = true;
                DocumentContext.CurrentTitle = Page.Title = litTitle.Text = string.Format("{0} - {2}{1}", CurrentDocument.DocumentName, _pageNumberTitle, ResHelper.GetString("CRC.Blog.SiteName"));
            }

            if (!string.IsNullOrWhiteSpace(_metaDescription))
            {
                DocumentContext.CurrentDescription = _metaDescription;
            }

        }

        private TagGroupInfo _tagGroupInfo;

        private TagGroupInfo TagGroup
        {
            get
            {
                return _tagGroupInfo ?? (_tagGroupInfo = TagGroupInfoProvider.GetTagGroupInfo(string.Format("CRC.TagGroup.Blog.{0}.content", LocalizationContext.PreferredCultureCode), SiteContext.CurrentSiteID));
            }
        }
    }
}