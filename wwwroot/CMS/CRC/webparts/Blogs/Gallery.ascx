﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Gallery.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.Gallery" %>
<div class='slideshow' id="<%# ClientID %>">
    <div class='slides'>
        <asp:Repeater runat="server" ID="rptTabs">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='#tabs-<%# Container.ItemIndex + 1 %>'><%# Eval("Title") %></a></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
        <asp:Repeater runat="server" ID="rptSlides">
            <ItemTemplate>
                <div class='slide' id='tabs-<%# Container.ItemIndex + 1 %>'>
                    <img src='<%# Eval("Image") %>' alt="<%# Eval("AltText") %>" />
                    <asp:PlaceHolder runat="server" Visible='<%# ShowDescription %>'>
                        <div class='slide-content'><%# Eval("Description") %></div>
                    </asp:PlaceHolder>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
