using System;
using System.Data;
using System.Linq;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using CultureInfo = System.Globalization.CultureInfo;
using CMS.Taxonomy;
using CMS.Helpers;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class BlogCategoriesAndTags : CMSAbstractWebPart
    {

        private TreeNode _blogNode;
        protected TreeNode BlogNode
        {
            get { return _blogNode ?? (_blogNode = TransformationHelper.GetSubSiteNode()); }
        }

        private int _documentId;
        public int DocumentId { get { return _documentId > 0 ? _documentId : DocumentContext.CurrentDocument.DocumentID; } set { _documentId = value; } }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryID in (select dc.categoryid from CMS_DocumentCategory dc where dc.documentid = {0})", DocumentId), "CategoryDisplayName asc", -1, "CategoryDisplayName, CategoryName");

            if (!DataHelper.DataSourceIsEmpty(categories))
            {
                rptCategories.DataSource = categories;
                rptCategories.DataBind();
            }

            var tags = TagInfoProvider.GetTags(DocumentId, string.Empty, "TagName ASC", -1, "TagName");
            if (!DataHelper.DataSourceIsEmpty(tags))
            {
                rptTags.DataSource = tags;
                rptTags.DataBind();
            }
        }

        public string ToTitleCase(string str)
        {
            if (!string.IsNullOrWhiteSpace(str) && str.Length > 1)
                return string.Format("{0}{1}", str[0].ToString(CultureInfo.InvariantCulture).ToUpper(), str.Substring(1));

            return str;
        }

    }
}