﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogFilter.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogFilter" %>
<asp:PlaceHolder runat="server" ID="phFilterTitle" Visible="False">
    <div class="main-content-header">
        <h1>
            <asp:Literal runat="server" ID="litTitle"></asp:Literal></h1>
        <p>
            <asp:Literal runat="server" ID="litDescription"></asp:Literal>
        </p>
    </div>
</asp:PlaceHolder>
