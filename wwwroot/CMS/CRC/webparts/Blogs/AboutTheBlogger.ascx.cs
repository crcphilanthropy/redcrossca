using System;
using CMS.CMSHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.Membership;
using CMS.DocumentEngine;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class AboutTheBlogger : CMSAbstractWebPart
    {
        private UserInfo _blogger;

        protected UserInfo Blogger
        {
            get
            {
                return _blogger ?? (_blogger = UserInfoProvider.GetUserInfo(DocumentContext.CurrentDocument.NodeOwner));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (Blogger != null)
                phAboutTheBloger.DataBind();
        }
    }
}