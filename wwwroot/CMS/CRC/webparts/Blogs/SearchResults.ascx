<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.WebParts.Blogs.SearchResults" CodeBehind="SearchResults.ascx.cs" %>
<%@ Register Src="~/CMSModules/SmartSearch/Controls/SearchResults.ascx" TagName="SearchResults" TagPrefix="cms" %>

<div class="main-content-header">
    <h1>
        <asp:Literal runat="server" ID="litTitle" />
    </h1>
    <p>
        <span id="search-query-text">
            <asp:Literal runat="server" ID="litDescription" />
        </span>
    </p>
</div>

<div class="blog-post-listing left-right-padding">

    <div class="section top-bottom-padding">
        <h4 class="section-header">
	        <span>
		        <asp:Literal ID="litResultsHeader" runat="server" />
	        </span>
        </h4>
        <div class="section-body">
            <cms:SearchResults ID="srchResults" runat="server" />
            <asp:Literal ID="litNoResult" runat="server" />
        </div>
    </div>

</div>
