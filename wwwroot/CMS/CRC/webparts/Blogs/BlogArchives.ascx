﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogArchives.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogArchives" %>
<div class="cta plain-text hide-for-medium-down toggle blog-archives">
    <h3 class="header"> 
        <span  id="blog-archive-toggle">
            <%# ResHelper.GetString("CRC.Blog.BlogArchives") %>
        </span>
    </h3>
    <div class="body">
        <ul class="links">

        </ul>
    </div>
</div>
