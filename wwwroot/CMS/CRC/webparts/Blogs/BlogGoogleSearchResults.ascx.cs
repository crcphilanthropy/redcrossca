﻿using System;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using Ecentricarts.GoogleSiteSearch.Model;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class BlogGoogleSearchResults : CMSAbstractWebPart
    {
        /// <summary>
        ///  On page prerender.
        /// </summary>
        /// <param name="e">Event argument</param>
        protected override void OnPreRender(EventArgs e)
        {
            //Search();
            long totalItems = 0;
            GoogleSearch(out totalItems);

            litDescription.Visible = !string.IsNullOrWhiteSpace(SearchText);
            litTitle.Text = ResHelper.GetString("CRC.Blog.SearchResults");
            litResultsHeader.Text = ResHelper.GetString("CRC.Blog.ResultsHeaderText");
            litDescription.Text = string.Format(ResHelper.GetString("CRC.Blog.SearchResultsDescriptionFormat"), Server.HtmlEncode(SearchText), totalItems);


            base.OnPreRender(e);
        }

        private void GoogleSearch(out long totalItems)
        {
            var fileType = string.Empty;
            var query = SearchText;
            var siteSearch = LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase)
                                 ? "www.croixrouge.ca/blogue"
                                 : "www.redcross.ca/blog";

            var data = new CMSAppAppCode.Old_App_Code.CRC.GoogleCustomSearch(10).Search(query, CRCPager.CurrentPage - 1, string.Format("lang_{0}", LocalizationContext.PreferredCultureCode.Substring(0, 2)), siteSearch, fileType);
            rptGoogleSearch.DataSource = data.Items;
            rptGoogleSearch.DataBind();

            totalItems = data.SearchInformation != null && data.SearchInformation.TotalResults.HasValue ? data.SearchInformation.TotalResults.Value : 0;

            CRCPager.DataBind(totalItems);
        }


        public string SearchText
        {
            get
            {
                return QueryHelper.GetString("searchtext", "");
            }
        }

        protected string GetSearchMetaTag(object dataItem, string tagname, string defaultValue)
        {
            var searchItem = dataItem as Result;
            if (searchItem == null || searchItem.Pagemap == null || !searchItem.Pagemap.ContainsKey("metatags")) return string.Empty;
            var metaTags = searchItem.Pagemap["metatags"][0];
            return metaTags != null && metaTags.ContainsKey(tagname) ? metaTags[tagname] as string : defaultValue;
        }

        protected string GetImagePath(object dataItem)
        {
            var searchItem = dataItem as Result;
            if (searchItem == null || searchItem.Pagemap == null || !searchItem.Pagemap.ContainsKey("cse_image")) return string.Empty;
            var image = searchItem.Pagemap["cse_image"][0];
            return image != null && image.ContainsKey("src") ? image["src"] as string : string.Empty;
        }

        protected string GetBlogOwnerInfo(object dataItem)
        {
            var searchItem = dataItem as Result;
            if (searchItem == null) return string.Empty;
            var author = GetSearchMetaTag(dataItem, "author", null);
            var authorUrl = GetSearchMetaTag(dataItem, "search:author_url", null);
            var authorSignature = GetSearchMetaTag(dataItem, "search:author_signature", null);

            if (!string.IsNullOrWhiteSpace(author) && !string.IsNullOrWhiteSpace(authorUrl))
            {
                return string.Format(
                       "<p class='post-metadata'>{0} <a href='{1}'>{2}</a>{3}</p>",
                       ResHelper.GetString("CRC.Blog.By"),
                       authorUrl,
                       author,
                       string.IsNullOrWhiteSpace(authorSignature) ? string.Empty : string.Format(", {0}", authorSignature)
                       );
            }

            return string.Empty;
        }
    }
}