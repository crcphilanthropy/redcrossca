﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutTheBlogger.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.AboutTheBlogger" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:PlaceHolder runat="server" ID="phAboutTheBloger">
    <div class="section featured post-author">

        <h4 class="section-header">
            <span><%# ResHelper.GetString("CRC.Blog.AboutTheBlogger") %></span>
        </h4>

        <div class="section-body divide">

            <div class="float-right per-80">
                <div class="section-content">

                    <h2 class="author-name color-red"><a href="<%# TransformationHelper.GetBlogFilterPath("author",Blogger.UserName) %>"><%# Blogger.FullName %></a>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# !string.IsNullOrWhiteSpace(Blogger.GetStringValue("UserTwitterAccount",string.Empty)) %>'>
                            <a class="twitter-handle" href="https://twitter.com/<%# Blogger.GetStringValue("UserTwitterAccount",string.Empty) %>" target="_blank"><span>@<%# Blogger.GetStringValue("UserTwitterAccount",string.Empty) %></span></a>
                        </asp:PlaceHolder>
                    </h2>

                    <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%# !string.IsNullOrWhiteSpace(Blogger.UserSignature) %>'>
                        <p class="author-metadata">
                            <%# Blogger.UserSignature %>
                        </p>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="PlaceHolder3" runat="server" Visible='<%# !string.IsNullOrWhiteSpace(Blogger.UserDescription) %>'>
                        <p class="author-bio">
                            <%# Blogger.UserDescription %>
                        </p>
                    </asp:PlaceHolder>
                </div>
            </div>

            <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrWhiteSpace(TransformationHelper.GetUserAvatar(Blogger)) %>'>
                <div class="float-right per-20">
                    <img class="block" src="<%# TransformationHelper.GetUserAvatar(Blogger) %>" alt="<%# Blogger.FullName %>" />
                </div>
            </asp:PlaceHolder>
        </div>

    </div>
</asp:PlaceHolder>

