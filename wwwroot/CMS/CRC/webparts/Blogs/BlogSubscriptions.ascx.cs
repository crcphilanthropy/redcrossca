using System;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class BlogSubscriptions : CMSAbstractWebPart
    {
        public string EnglishFeedBurner
        {
            get { return ValidationHelper.GetString(GetValue("EnglishFeedBurner"), string.Empty); }
            set { SetValue("EnglishFeedBurner", value); }
        }

        public string FrenchFeedBurner
        {
            get { return ValidationHelper.GetString(GetValue("FrenchFeedBurner"), string.Empty); }
            set { SetValue("FrenchFeedBurner", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            InitRssFeed();
            DataBind();
            base.OnInit(e);
        }

        private void InitRssFeed()
        {
            feed.LinkText = ResHelper.GetString("CRC.Blog.RssFeed");
            feed.FeedTitle = ResHelper.GetString("CRC.Blog.FeedTitle");
            feed.FeedDescription = ResHelper.GetString("CRC.Blog.FeedDescription");
        }
    }
}