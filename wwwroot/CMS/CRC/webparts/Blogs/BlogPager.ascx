﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogPager.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogPager" %>
<asp:Repeater runat="server" ID="rptPager">
    <HeaderTemplate>
        <div class="horizontal-list content-pager">
            <ul>
                <li class="previous">
                    <a href="<%# URLHelper.AddParameterToUrl(CMS.Helpers.RequestContext.CurrentURL, "page", IsFirstPage ? "1" : (CurrentPage - 1).ToString()) %>" data-page="<%# IsFirstPage ? 1 : CurrentPage - 1 %>" class="page-prev">&laquo;
                    </a>
                </li>
                <li class="unavailable dots" runat="server" visible='<%# GetPreviousGroup() > 0 %>'>
                    <a href='<%# URLHelper.AddParameterToUrl(CMS.Helpers.RequestContext.CurrentURL, "page", GetPreviousGroup().ToString()) %>' data-page="<%# GetPreviousGroup() %>">&hellip;
                    </a>
                </li>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <a runat="server" visible="<%# Convert.ToInt32(Container.DataItem) == CurrentPage %>" class="page-active"><%# Container.DataItem %></a>
            <a runat="server" visible="<%# Convert.ToInt32(Container.DataItem) != CurrentPage %>" href='<%# URLHelper.AddParameterToUrl(CMS.Helpers.RequestContext.CurrentURL, "page", Convert.ToInt32(Container.DataItem).ToString()) %>' data-page="<%# Convert.ToInt32(Container.DataItem) - 1 %>"><%# Container.DataItem %></a>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        <li class="unavailable dots" runat="server" visible='<%# GetNextGroup() > GroupSize %>'>
            <a href='<%# URLHelper.AddParameterToUrl(CMS.Helpers.RequestContext.CurrentURL, "page", GetNextGroup().ToString()) %>' data-page="<%# GetNextGroup() %>">&hellip;
            </a>
        </li>
        <li class="next">
            <a class="page-last" href="<%# URLHelper.AddParameterToUrl(CMS.Helpers.RequestContext.CurrentURL, "page", (IsLastPage ? CurrentPage : CurrentPage + 1).ToString()) %>" data-page="<%#  IsLastPage ? CurrentPage : CurrentPage + 1 %>">&raquo;
            </a>
        </li>
        </ul></div>
    </FooterTemplate>
</asp:Repeater>
