﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogGoogleSearchResults.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.BlogGoogleSearchResults" %>
<%@ Register Src="~/CRC/webparts/Blogs/BlogPager.ascx" TagPrefix="uc1" TagName="CRCPager" %>
<div class="main-content-header">
    <h1>
        <asp:Literal runat="server" ID="litTitle" />
    </h1>
    <p>
        <span id="search-query-text">
            <asp:Literal runat="server" ID="litDescription" />
        </span>
    </p>
</div>
<div class="blog-post-listing left-right-padding">

    <div class="section top-bottom-padding">
        <h4 class="section-header">
            <span>
                <asp:Literal ID="litResultsHeader" runat="server" />
            </span>
        </h4>
        <div class="section-body no-more-tables">
            <asp:Panel runat="server" ID="pnlSearchResults">

                <asp:PlaceHolder runat="server" ID="plcBasicRepeater"></asp:PlaceHolder>
                <asp:Repeater runat="server" ID="rptGoogleSearch">
                    <ItemTemplate>
                        <div class="blog-post <%# string.IsNullOrWhiteSpace(GetImagePath(Container.DataItem)) ? string.Empty : "with-thumbnail" %>">
                            <div class="post-body">
                                <div class="post-header">
                                    <h2>
                                        <a href="<%# Eval("Link") %>"><%# GetSearchMetaTag(Container.DataItem,"search:title",EvalText("Title")) %></a>
                                    </h2>
                                    <%# GetBlogOwnerInfo(Container.DataItem) %>
                                </div>
                                <div class="post-content">
                                    <p><%# Eval("Snippet").ToString().Replace(Environment.NewLine,string.Empty) %></p>
                                </div>
                            </div>
                            <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%# !string.IsNullOrWhiteSpace(GetImagePath(Container.DataItem))  %>'>
                                <div class="post-image">
                                    <img src="<%# GetImagePath(Container.DataItem) %>" alt="" />
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <cms:LocalizedLabel runat="server" ID="lblNoResults" CssClass="ContentLabel" Visible="false"
                    EnableViewState="false" />
            </asp:Panel>
        </div>
    </div>

</div>
<uc1:CRCPager runat="server" ID="CRCPager" PageSize="10" GroupSize="4" IsLateBinding="false" />

