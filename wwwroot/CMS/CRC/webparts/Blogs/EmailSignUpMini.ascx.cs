using System;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;

namespace CMSApp.CRC.webparts.Blogs
{
  public partial class EmailSignUpMini : CMSAbstractWebPart
  {

    public string DestinationUrl
    {
      get { return ValidationHelper.GetString(GetValue("DestinationUrl"), string.Empty); }
      set { SetValue("DestinationUrl", value); }
    }

    public string Tags
    {
      get { return ValidationHelper.GetString(GetValue("Tags"), string.Empty); }
      set { SetValue("Tags", value); }
    }

    public DateTime DateJoined
    {
      get { return DateTime.Today; }
      set { SetValue("DateJoined", value); }
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      InitText();
      btnSignUp.Click += BtnSignUpServerClick;
      regExEmailAddress.ErrorMessage = reqEmailAddress.ErrorMessage = ResHelper.GetString("CRC.Blog.Error.Email");
      regExEmailAddress.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
      DataBind();
    }

    private void InitText()
    {
      SignUpEmailTextBox.Attributes.Add("placeholder", ResHelper.GetString("CRC.Blog.EmailPlaceholder"));
    }

    void BtnSignUpServerClick(object sender, EventArgs e)
    {
      SessionHelper.SetValue("email", SignUpEmailTextBox.Value);

      var joined = Server.UrlEncode(DateJoined.Date.ToString("yyyy/MM/dd"));
      var tag = Server.UrlEncode(Tags);
      var encodeEmail = Server.UrlEncode(SignUpEmailTextBox.Value);

      if (!string.IsNullOrWhiteSpace(DestinationUrl))
      {
        //Build redirect url
        var redirectUrl = URLHelper.AddParameterToUrl(DestinationUrl + ".aspx", "email", encodeEmail);
        redirectUrl = URLHelper.AppendQuery(redirectUrl, "tag=" + tag);
        redirectUrl = URLHelper.AppendQuery(redirectUrl, "date=" + joined);

        if (LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase))
        {
          var node = new TreeProvider().SelectSingleNode(SiteContext.CurrentSiteName, DestinationUrl,
                                                         LocalizationContext.PreferredCultureCode, false, null, null,
                                                         null, -1, true);
          var tempNodeUrl = URLHelper.AddParameterToUrl(node.RelativeURL + ".aspx", "email", encodeEmail);
          tempNodeUrl = URLHelper.AppendQuery(tempNodeUrl, "tag=" + tag);
          tempNodeUrl = URLHelper.AppendQuery(tempNodeUrl, "date=" + joined);
          Response.Redirect(tempNodeUrl);
        }
        Response.Redirect(redirectUrl);
      }


    }
  }
}