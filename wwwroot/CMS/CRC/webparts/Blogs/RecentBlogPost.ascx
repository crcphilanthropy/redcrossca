﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentBlogPost.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.FromTheBlog" %>
<div class="cta plain-text recent-posts toggle blog-cta">
    <h3 class="header">
        <span>
            <%# Title %>
        </span>
    </h3>
    <div class="body">
        <cms:CMSRepeater runat="server" ID="rptBlogPosts"/>
    </div>
</div>
