﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSignUpMini.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.EmailSignUpMini" %>
<asp:Panel runat="server" DefaultButton="btnSignUp" ID="pnlEmail" CssClass="cta with-theme blue-theme" ClientIDMode="Static">

    <h3 class="header">
        <span><%# ResHelper.GetString("CRC.Blog.EmailSignUpTitle") %></span>
    </h3>
    <div class="body">
        <p>
            <%# ResHelper.GetString("CRC.Blog.EmailSignUpSummary") %>
        </p>
        <div class="form-input text">
            <input type="text" class="textbox" placeholder='<%# ResHelper.GetString("CRC.Blog.EnterYourEmailHere") %>' runat="server" id="SignUpEmailTextBox" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="EmailSignUp" /><br />
        </div>
        <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Blog.Error.Email") %>' ValidationGroup="EmailSignUp" CssClass="text-error" ForeColor="red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Blog.Error.Email") %>' ValidationGroup="EmailSignUp" CssClass="text-error" ForeColor="red" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
        <div class="form-input right">
            <asp:LinkButton class="button large red arrow-right" runat="server" id="btnSignUp" validationgroup="EmailSignUp">
                <span>
                    <%# ResHelper.GetString("CRC.Blog.Form.SignupNow") %> 
                </span>
            </asp:LinkButton>
        </div>
    </div>
</asp:Panel>
