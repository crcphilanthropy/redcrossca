using System;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.webparts.Blogs
{
    public partial class SearchBox : CMSAbstractWebPart
    {
        public string SearchUrl
        {
            get { return ValidationHelper.GetString(GetValue("SearchUrl"), string.Empty); }
            set { SetValue("SearchUrl", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnSearch.Click += BtnSearchClick;
            btnSearch.Text = ResHelper.GetString("CRC.Blog.Search");
            txtSearch.Attributes.Add("placeholder", ResHelper.GetString("CRC.Blog.SearchPlaceholder"));
        }

        void BtnSearchClick(object sender, EventArgs e)
        {
            var textSearch = txtSearch.Text.Trim().Replace('?', '-')
                                                  .Replace('\\', '-')
                                                  .Replace('/','-')
                                                  .Replace('.','-')
                                                  .Replace('%','-')
                                                  .Replace('$','-')
                                                  .Replace('#','-')
                                                  .Replace('!', '-')
                                                  .Replace('@', '-')
                                                  .Replace('^', '-')
                                                  .Replace('*', '-').Replace('&','-');

            if (string.IsNullOrWhiteSpace(textSearch)) return;

            var url = TreeHelper.GetDocument(SiteContext.CurrentSiteName, SearchUrl, LocalizationContext.PreferredCultureCode, false, null, true);
            
            Response.Redirect(URLHelper.AddParameterToUrl(url.DocumentUrlPath, "searchtext", Server.UrlEncode(textSearch)));
        }
    }
}