﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchBox.ascx.cs" Inherits="CMSApp.CRC.webparts.Blogs.SearchBox" %>

<div class="search-box">
    
    <asp:PlaceHolder ID="phlGlobalSearch" runat="server">
        <asp:Panel id="pnlSearchBox" runat="server" DefaultButton="btnSearch">
            <div class="form-input text with-button">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="txtSearchBox" />
                <asp:Button runat="server" ID="btnSearch" CssClass="btn search btnBlogSearch" CausesValidation="false" />
            </div>
        </asp:Panel>
    </asp:PlaceHolder>

</div>
