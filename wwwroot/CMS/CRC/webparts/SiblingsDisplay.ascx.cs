using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.TreeEngine;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Localization;
using CultureInfo = System.Globalization.CultureInfo;

namespace CMSApp.CRC.webparts
{
    public partial class SiblingsDisplay : CMSAbstractWebPart
    {
        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("TransformationName"), string.Empty); }
            set { SetValue("TransformationName", value); }
        }

        public string ClassNames
        {
            get { return ValidationHelper.GetString(GetValue("ClassNames"), string.Empty); }
            set { SetValue("ClassNames", value); }
        }

        public string OrderBy
        {
            get { return ValidationHelper.GetString(GetValue("OrderBy"), string.Empty); }
            set { SetValue("OrderBy", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitRepeater();
            DataBind();
        }

        private void InitRepeater()
        {
            rptSiblings.DataSource = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, string.Format("{0}/%", DocumentContext.CurrentDocumentParent.NodeAliasPath),
                                                             LocalizationContext.PreferredCultureCode, false, ClassNames, string.Format("NodeParentID = {0} and ClassName not in ('CRC.NavHeading','CRC.NavigationSubheading','CMS.Folder')", DocumentContext.CurrentDocumentParent.NodeID), OrderBy, TreeProvider.ALL_LEVELS, true, -1);
            rptSiblings.ItemDataBound += RptSiblingsItemDataBound;
            rptSiblings.DataBind();
        }

        static void RptSiblingsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var litNode = e.Item.FindControl("litNode") as Literal;
                if (litNode == null) return;

                litNode.Text = string.Equals(DocumentContext.CurrentDocument.NodeID.ToString(CultureInfo.InvariantCulture), DataBinder.Eval(e.Item.DataItem, "NodeID").ToString(), StringComparison.OrdinalIgnoreCase) ? DocumentContext.CurrentDocument.DocumentName : string.Format("<a href='{0}'>{1}</a>", CMS.Controls.TransformationHelper.HelperObject.GetDocumentUrl(DataBinder.Eval(e.Item.DataItem, "DocumentID")), DataBinder.Eval(e.Item.DataItem, "DocumentName"));
            }
        }
    }
}