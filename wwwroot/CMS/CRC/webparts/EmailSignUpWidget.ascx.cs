using System;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
  public partial class EmailSignUpWidget : CMSAbstractWebPart
  {
    public string Title
    {
      get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
      set { SetValue("Title", value); }
    }

    public string Subtitle
    {
      get { return ValidationHelper.GetString(GetValue("Subtitle"), string.Empty); }
      set { SetValue("Subtitle", value); }
    }

    public string ButtonText
    {
      get { return ValidationHelper.GetString(GetValue("ButtonText"), string.Empty); }
      set { SetValue("ButtonText", value); }
    }

    public string DestinationUrl
    {
      get { return ValidationHelper.GetString(GetValue("DestinationUrl"), string.Empty); }
      set { SetValue("DestinationUrl", value); }
    }

    public string EmailPlaceHolder
    {
      get { return ValidationHelper.GetString(GetValue("EmailPlaceHolder"), string.Empty); }
      set { SetValue("EmailPlaceHolder", value); }
    }

    public string Tags
    {
      get { return ValidationHelper.GetString(GetValue("Tags"), string.Empty); }
      set { SetValue("Tags", value); }
    }

    public DateTime DateJoined
    {
      get { return ValidationHelper.GetDateTime(GetValue("DateJoined"), DateTime.Today); }
      set { SetValue("DateJoined", value); }
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      InitText();
      btnSignUp.Click += BtnSignUpServerClick;
      btnSignUp.Text = ButtonText;
      regExEmailAddress.ErrorMessage = reqEmailAddress.ErrorMessage = ResHelper.GetString("CRC.Error.Email");
      regExEmailAddress.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
      DataBind();

      btnSignUp.Attributes.Add("data-event-label", CurrentDocument.NodeAlias.Equals("home", StringComparison.OrdinalIgnoreCase) ? "home" : "sidebar");
    }

    private void InitText()
    {
      btnSignUp.Text = ButtonText;
      SignUpEmailTextBox.Attributes.Add("placeholder", EmailPlaceHolder);
    }

    void BtnSignUpServerClick(object sender, EventArgs e)
    {
      var joined = Server.UrlEncode(DateJoined.Date.ToString("yyyy/MM/dd"));
      var tag = Server.UrlEncode(Tags);
      var encodeEmail = Server.UrlEncode(SignUpEmailTextBox.Value);

      var redirectUrl = URLHelper.AddParameterToUrl(DestinationUrl + ".aspx", "email", encodeEmail);
      redirectUrl = URLHelper.AppendQuery(redirectUrl, "tag=" + tag);
      redirectUrl = URLHelper.AppendQuery(redirectUrl, "date=" + joined);

      //The email should be encoded as its passed in the url - Toks
      //Response.Redirect(URLHelper.AddParameterToUrl(DestinationUrl + ".aspx", "email", SignUpEmailTextBox.Value));
      Response.Redirect(redirectUrl);
    }
  }
}