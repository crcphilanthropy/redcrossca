﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DonateBar.ascx.cs" Inherits="CMSApp.CRC.webparts.DonateBar" %>

<div class="left">
    <p>
        <%# ResHelper.GetString("CRC.DonateBar.InfoText") %>
    </p>
</div>
<div class="right">
    <asp:Panel runat="server" ID="donateBar" CssClass="form" DefaultButton="btnDonate">

        <asp:TextBox runat="server" ID="DonationAmount" min="0" step="5" ValidationGroup="DonationBar" CssClass="donate-bar-textbox"></asp:TextBox>
        <input type="radio" class="radio-btn" id="rbSingleDonation" name="DonationType" value="single" runat="server" clientidmode="Static" /><label class="radio-btn" for="rbSingleDonation"><%# ResHelper.GetString("CRC.DonateBar.OneTime") %></label>
        <input type="radio" class="radio-btn donate-bar-monthlychk" id="rbMonthlyDonation" name="DonationType" value="monthly" runat="server" clientidmode="Static" /><label class="radio-btn" for="rbMonthlyDonation"><%# ResHelper.GetString("CRC.DonateBar.Monthly") %></label>
        <div class="hide">
            <input type="text" name="donateTelNum1" id="donateTelNum1" class="require" />
        </div>

        <asp:LinkButton runat="server" CssClass="donate-track button small secondary" ID="btnDonate" Text="Give Now" ValidationGroup="DonationBar" ClientIDMode="Static" OnClientClick="return false;"/>
        <input type="hidden" id="hidDonateBarGAHash" runat="server" class="donate-bar-hash" />
    </asp:Panel>
</div>
<script type="text/javascript">

    var pageDonateLoadAt = new Date();
    function initDonationBarTracking() {
        var btnDonation = '<%# btnDonate.ClientID %>';
        var donateUrl = '<%# DonationFormUrl %>';
        var langPref = '<%# CMS.Localization.LocalizationContext.PreferredCultureCode %>';

        var rdoQuick = $('#rbSingleDonation').attr('name');
        var frequencyOfDonation = '';

        $("#" + btnDonation).click(function (event) {

            if (Math.abs(new Date() - pageDonateLoadAt) < 1000) return false;
            if ($('#donateTelNum1').val().length > 0) return false;

            var getFrequency = $('input[type="radio"][name="' + rdoQuick + '"]:checked').val();

            if (getFrequency === "monthly") {
                frequencyOfDonation = "Monthly";
            } else if (getFrequency === "single") {
                frequencyOfDonation = "One Time";
            } else {
                frequencyOfDonation = "Frequency not set";
            }

            var getAmount = $('.donate-bar-textbox').val();
            
            getAmount = GetValidatedAmount(getAmount);
            if (getAmount > 0) {
                //var donateBarValue = document.getElementById(txtAmount);
                //donateBarValue.value = getAmount;       // set the textbox value to the "possibly" updated new amount
                
                dataLayer.push({ "event": "generic-event", "eventCategory": "Donate Bar Widget", "eventAction": frequencyOfDonation, "eventLabel": getAmount, "eventValue": getAmount });
                
                // _ga parameter name included
                var _ga = ga.getAll()[0].get('linkerParam');

                var redirectUrl = donateUrl + 
                  '&type=' + getFrequency +
                  '&amount=' + getAmount +
                  '&langpref=' + langPref +
                   _ga;

                // redirect to donation page
                window.location.href = redirectUrl;

                //donateButton.off("click");
                //setTimeout(function () { // after 1 second, submit the form
                //    donateButton.trigger("click");
                //}, 1000);
            }
            else {
                event.preventDefault();
            }

            return false;
        });
    }
</script>
