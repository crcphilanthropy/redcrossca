﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InfographicFallback.ascx.cs" Inherits="CMSApp.CRC.webparts.InfographicFallback" %>
<div class="hidden-for-large-up">
    <div class="fallback-image">
        <img src='<%# Image %>'
            srcset='<%# Image %> 1024w, <%# GetResizedImage(Image, 550) %> 640w, <%# GetResizedImage(Image, 450) %> 320w'
            sizes="(min-width: 960px) 100vw"
            alt='<%# ImageAltText %>'>
    </div>
</div>