﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwitterSingleTweet.ascx.cs" Inherits="CMSApp.CRC.webparts.TwitterSingleTweet" %>
<%@ Import Namespace="CMSApp.CRC" %>
<div class="cta twitter" data-twitter-ready="false">
  <div class="tweet-header">
    <div class="profile-icon">
    </div>
    <div class="profile">
      <div class="display-name">
      </div>
      <div class="username">
      </div>
    </div>
    <div class="follow-button">
    </div>
  </div>
  <div class="tweet"></div>
  <div class="tweet-footer">
    <div class="tweet-action">
      <div class="reply"></div>
      <div class="retweet"></div>
      <div class="favourite"></div>
    </div>
    <div class="date"></div>
  </div>
</div>
