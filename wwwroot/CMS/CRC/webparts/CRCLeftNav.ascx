﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRCLeftNav.ascx.cs" Inherits="CMSApp.CRC.webparts.CRCLeftNav" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:PlaceHolder runat="server" ID="phScript">

</asp:PlaceHolder>
<asp:Repeater runat="server" ID="rptLeftNav">
    <HeaderTemplate>
        <ul class="side-nav border-top">
    </HeaderTemplate>
    <ItemTemplate>
        <li class='<%# GetActive(ValidationHelper.GetString(Eval("NodeAliasPath"),string.Empty),ValidationHelper.GetString(Eval("DocumentUrlPath"),string.Empty) ,PathToExpand) %> <%# Eval("DocumentMenuClass") %> level-<%# Eval("NodeLevel") %>'>
            <asp:PlaceHolder runat="server" Visible='<%# !EvalBool("DocumentMenuItemInactive") %>'>
                <a href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath"),null,EvalText("DocumentMenuRedirectUrl")) %>" <%# GetOnClick(ValidationHelper.GetString(Eval("DocumentMenuJavascript"),string.Empty)) %>>
                    <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                </a>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" Visible='<%# EvalBool("DocumentMenuItemInactive") %>'>
                <span onclick="return false;" style=""><%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %></span>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phSubNav"></asp:PlaceHolder>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
