﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRCNestedList.ascx.cs" Inherits="CMSApp.CRC.webparts.CRCNestedList" ViewStateMode="Disabled" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:Repeater runat="server" ID="rptLeftNav">
    <HeaderTemplate>
        <section class="region" style="display: block;">
            <h2><%# Title %></h2>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:PlaceHolder runat="server" Visible='<%# !EvalBool("DocumentMenuItemInactive") %>'>
            <a href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath")) %>">
                <h3 class='<%# GetActive(TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath")) ,PathToExpand) %> <%# Eval("DocumentMenuClass") %>'>
                    <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                </h3>
            </a>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" Visible='<%# EvalBool("DocumentMenuItemInactive") %>'>
            <h3 class='<%# GetActive(TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath")) ,PathToExpand) %> <%# Eval("DocumentMenuClass") %>'>
                <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
            </h3>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phSubNav"></asp:PlaceHolder>
    </ItemTemplate>
    <FooterTemplate>
        </section>
    </FooterTemplate>
</asp:Repeater>
