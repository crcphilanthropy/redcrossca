using System;
using System.Collections.Generic;
using System.Linq;
using CMS.DocumentEngine;
using CMS.PortalControls;
using CMSApp.CRC;
using CMS.Membership;
using CMS.Helpers;
using TreeNode = CMS.DocumentEngine.TreeNode;

public partial class CMSApp_CRC_webparts_LanguageSwitch : CMSAbstractWebPart
{
    //readonly TreeProvider _treeProvider = new TreeProvider(MembershipContext.AuthenticatedUser);
    readonly string _lang = ResHelper.GetString("CRC.Master.TranslateCultureCode");
    private readonly TreeProvider _treeProvider = new TreeProvider();

    public int TabIndex
    {
        get { return ValidationHelper.GetInteger(GetValue("TabIndex"), -1); }
        set { SetValue("TabIndex", value); }
    }

    #region "Document properties"




    #endregion
    #region "Stop processing"

    /// <summary>
    /// Returns true if the control processing should be stopped.
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;


        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing) return;

        LanguageSwitchLink.Text = ResHelper.GetString("CRC.Master.Translate");
        phDocumentMissingMessage.Visible = false;
        if (Node != null) return;

        phLanguageToggle.Visible = false;
        phDocumentMissingMessage.Visible = true;
        litDocumentMissingMessage.Text = string.Format("<span data-tooltip aria-haspopup='true' class='has-tip' title='{0}'>{1}</span>", ResHelper.GetString("CRC.Master.TranslateMissingDocumentMessage"), ResHelper.GetString("CRC.Master.Translate"));

    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        LanguageSwitchLink.Click += LanguageSwitchLinkClick;
        if (TabIndex > -1)
        {
            short tabIndex = 0;
            short.TryParse(TabIndex.ToString(), out tabIndex);
            LanguageSwitchLink.TabIndex = tabIndex;
        }
    }

    private string _domainName;
    protected string DomainName
    {
        get
        {
            return _domainName ?? (_domainName = TransformationHelper.GetAlternateLanguageDomain());
        }
    }

    private TreeNode _node;
    protected TreeNode Node
    {
        get
        {
            //return _node ?? (_node =DocumentContext.CurrentDocument.CultureVersions.FirstOrDefault(item => item.DocumentCulture.Equals(_lang, StringComparison.OrdinalIgnoreCase)));
            return _node ?? (_node = _treeProvider.SelectSingleNode(DocumentContext.CurrentDocument.NodeID, _lang));
        }
    }

    void LanguageSwitchLinkClick(object sender, EventArgs e)
    {
        if (Node == null) return;

        var nodeAliasPath = Node.NodeAliasPath.Equals("/Home", StringComparison.OrdinalIgnoreCase) ? "/" : Node.NodeAliasPath;
        var documentUrlPath = Node.DocumentUrlPath;
        if (!string.IsNullOrWhiteSpace(documentUrlPath))
        {
            documentUrlPath = documentUrlPath.ToLower()
                                             .Replace("{province}", GetProvinceName())
                                             .Replace("{city}", Request.QueryString["city"]);
        }
        var documentAlias = !string.IsNullOrWhiteSpace(documentUrlPath) && !nodeAliasPath.Equals("/") ? documentUrlPath : nodeAliasPath;

        var ignoreList = new List<string> { "aliaspath", "province", "city", "location", "lat", "long", "category", "subcategory" };
        documentAlias = Request.QueryString.Keys.Cast<string>().Where(key => !ignoreList.Contains(key, new StringEqualityComparer()))
                .Aggregate(documentAlias, (current, key) => URLHelper.AddParameterToUrl(current, key, Request.QueryString[key]));

        documentAlias = string.IsNullOrWhiteSpace(DomainName) ? URLHelper.AddParameterToUrl(documentAlias, "lang", _lang) : URLHelper.AddParameterToUrl("http://" + DomainName + documentAlias, "lang", _lang);

        documentAlias = URLHelper.AddParameterToUrl(documentAlias, "_ga", (hidLanguageToggleHash.Value ?? string.Empty).ToLower().Replace("_ga=", string.Empty));

        Response.Redirect(documentAlias, true);
    }

    private string GetProvinceName()
    {
        var provinceName = Request.QueryString["province"] ?? string.Empty;
        var uiCode = string.Format("CRC.Province.{0}", provinceName.Replace("-", string.Empty));
        var translation = ResHelper.GetString(uiCode, _lang);
        if (translation.Equals(uiCode, StringComparison.OrdinalIgnoreCase))
            return provinceName;

        return TransformationHelper.ReplaceAccentCharacters(translation).Replace(" ", "-");
    }

    /// <summary>
    /// Event risen when the source filter has changed
    /// </summary>
    protected void FilterControl_OnFilterChanged()
    {
        // Override previously set visibility. Control's visibility is managed in the PreRender event.
        Visible = true;
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();

    }


    /// <summary>
    /// Clears cache.
    /// </summary>
    public override void ClearCache()
    {

    }

}
