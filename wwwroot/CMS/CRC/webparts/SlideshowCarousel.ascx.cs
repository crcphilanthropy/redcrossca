using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using CMS.DocumentEngine;
using CMS.Localization;
using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.Controls;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using System.Data;
using System.Web.UI.WebControls;
using CMS.Helpers;

public partial class CMSApp_CRC_webparts_SlideshowCarousel : CMSAbstractWebPart
{
    #region "Document properties"

    protected CMSDocumentsDataSource mDataSourceControl = null;



    #endregion
    #region "Stop processing"

    public string Path
    {
        get { return ValidationHelper.GetString(GetValue("Path"), string.Empty); }
        set { SetValue("Path", value); }
    }
    /// <summary>
    /// Returns true if the control processing should be stopped.
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;


        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing || String.IsNullOrWhiteSpace(Path)) return;


        var data = TreeHelper.GetDocuments(CurrentSiteName, Path,
                                           LocalizationContext.PreferredCultureCode, false, "crc.slide", null,
                                           "NodeOrder", -1, true, 4,
                                           "TextLocation,TextBackground,Image,ImageAltText,Content,URL,LinkText,NodeAlias,NavigationIcon,NavigationText");

        if (DataHelper.DataSourceIsEmpty(data)) return;

        var navButtons = new List<string>();
        data.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(row => navButtons.Add(string.Format("['{0}','{1}']",ValidationHelper.GetString(row["NavigationIcon"], string.Empty), ValidationHelper.GetString(row["NavigationText"], string.Empty))));

        CMSApp.CRC.TransformationHelper.RegisterStartUpScript(string.Format("var btns = [{0}];", navButtons.Join(",")), Page, "SliderNavButtons");

        rptSlider.DataSource = data;
        rptSlider.DataBind();


    }
    /// <summary>
    /// Event risen when the source filter has changed
    /// </summary>
    protected void FilterControl_OnFilterChanged()
    {
        // Override previously set visibility. Control's visibility is managed in the PreRender event.
        Visible = true;
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();

    }


    /// <summary>
    /// Clears cache.
    /// </summary>
    public override void ClearCache()
    {
        rptSlider.ClearCache();
    }
}

