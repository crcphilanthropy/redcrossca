<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsletter.ascx.cs" Inherits="CMSApp.CRC.webparts.Newsletter" %>
<%@ Import Namespace="CMS.DocumentEngine" %>
<asp:Panel ID="pnlForm" runat="server" Visible="True" CssClass="form newsletter" DefaultButton="btnEmail">

    <p>
        <%# ResHelper.GetString("CRC.Form.Newsletter.Intro") %>
    </p>

    <div class="row">
        <div class="columns">
            <p class="required right"><%# ResHelper.GetString("CRC.RequiredField") %></p>
        </div>
    </div>

    <asp:PlaceHolder ID="plcSalutation" runat="server" Visible="false">
        <div class="row">
            <div class="medium-3 columns">
                <label for="<%# drpNewsletterSalutation.ClientID %>"><%# ResHelper.GetString("CRC.Form.Salutation") %></label>
            </div>
            <div class="medium-9 columns">
                <cms:LocalizedDropDownList runat="server" ValidationGroup="LyrisForm"
                    ID="drpNewsletterSalutation" ClientIDMode="Static">
                    <asp:ListItem ID="defaultSelection" runat="server" Value=""></asp:ListItem>
                </cms:LocalizedDropDownList>
            </div>
        </div>

    </asp:PlaceHolder>
    <div class="row">
        <div class="medium-3 columns">
            <label for="<%# NewsletterFirstnameTextBox.ClientID %>"><%# ResHelper.GetString("CRC.Form.FirstName") %></label>
        </div>
        <div class="medium-9 columns">
            <asp:TextBox runat="server" ValidationGroup="LyrisForm"
                ID="NewsletterFirstnameTextBox" ClientIDMode="Static" />
        </div>
    </div>
    <asp:PlaceHolder ID="plcLastName" runat="server" Visible="false">
        <div class="row">
            <div class="medium-3 columns" id="thFrenchLastName" runat="server">
                <label for="<%# NewsletterLastnameTextBox.ClientID %>"><%# ResHelper.GetString("CRC.Form.LastName") %></label>
            </div>
            <div class="medium-9 columns">
                <asp:TextBox runat="server" ValidationGroup="LyrisForm"
                    ID="NewsletterLastnameTextBox" ClientIDMode="Static" />
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="row">
        <div class="medium-3 columns">
            <label for="<%# NewsletterEmailTextBox.ClientID  %>"><span class="required">*</span> <%# ResHelper.GetString("CRC.Form.Email") %></label>
        </div>
        <div class="medium-9 columns">
            <asp:TextBox runat="server" ValidationGroup="LyrisForm"
                ID="NewsletterEmailTextBox" data-input-type="email" data-change-type="true" CssClass="trim" />
            <asp:RegularExpressionValidator runat="server" ID="rxEmail" CssClass="text-error" ControlToValidate="NewsletterEmailTextBox" Display="Dynamic" ErrorMessage='<%# ResHelper.GetString("CRC.Form.Error.Email") %>' ValidationGroup="LyrisForm" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" CssClass="text-error" Display="Dynamic" ErrorMessage='<%# ResHelper.GetString("CRC.Form.Error.Email") %>' Text='<%# ResHelper.GetString("CRC.Form.Error.Email") %>' ControlToValidate="NewsletterEmailTextBox" ValidationGroup="LyrisForm"></asp:RequiredFieldValidator>


        </div>
    </div>


    <asp:LinkButton runat="server" ValidationGroup="LyrisForm"
        CssClass="button right lyrisForm"
        ID="btnEmail"
        Text='<%# ResHelper.GetString("CRC.Form.SignupNow") %>'
        OnClick="BtnEmailClick" data-event-label="newsletter" data-event-action="complete" data-validation-group="LyrisForm" />


    <div class="row">
        <div class="columns">
            <p><%# ResHelper.GetString("CRC.Form.PrivacyPolicy") %></p>
        </div>
    </div>

</asp:Panel>
<asp:Panel ID="pnlThankYou" runat="server" Visible="False" CssClass="form newsletter">
    <div class="row">
        <div class="columns">
            <%# ResHelper.GetString("CRC.Form.Newsletter.ThankYou") %>
        </div>
        <script type="text/javascript">
            dataLayer.push({ 'event': 'newsletter-subscribed' });
        </script>
    </div>
</asp:Panel>
