﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideshowCarousel.ascx.cs" Inherits="CMSApp_CRC_webparts_SlideshowCarousel" EnableViewState="false" %>


<cms:CMSRepeater runat="server" ID="rptSlider" DataBindByDefault="False" TransformationName="crc.slide.slide" CacheDependencies="node|crc|/home/slides|childnodes" >
    <HeaderTemplate>
        <div class="slider">
    </HeaderTemplate>
    <FooterTemplate></div></FooterTemplate>
</cms:CMSRepeater>
<%--<script>
    var btns = [
        ["images/icon-slide-example.svg", "Red cross responding to the ebola outbreak"],
        ["images/icon-slide-example.svg", "Red cross responding to the ebola outbreak"],
        ["images/icon-slide-example.svg", "Red cross responding to the ebola outbreak"],
        ["images/icon-slide-example.svg", "Red cross responding to the ebola outbreak"]];
</script>--%>