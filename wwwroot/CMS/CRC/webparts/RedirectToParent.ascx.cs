using System;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class RedirectToParent : CMSAbstractWebPart
    {
        public string DocumentTypes
        {
            get { return ValidationHelper.GetString(GetValue("DocumentTypes"), string.Empty); }
            set { SetValue("DocumentTypes", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            if (DocumentTypes.Contains(CurrentDocument.ClassName)) Response.Redirect(CurrentDocument.Parent.RelativeURL, true);
            base.OnInit(e);
        }
    }
}