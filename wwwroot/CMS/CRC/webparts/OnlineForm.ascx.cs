using System;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.WebAnalytics;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class OnlineForm : CMSAbstractWebPart
    {
        #region "Properties"

        /// <summary>
        /// Gets or sets the form name of BizForm.
        /// </summary>
        public string BizFormName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("BizFormName"), "");
            }
            set
            {
                SetValue("BizFormName", value);
            }
        }


        /// <summary>
        /// Gets or sets the alternative form full name (ClassName.AlternativeFormName).
        /// </summary>
        public string AlternativeFormName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AlternativeFormName"), "");
            }
            set
            {
                SetValue("AlternativeFormName", value);
            }
        }

        public string RedirectToUrl
        {
            get
            {
                return ValidationHelper.GetString(GetValue("RedirectToUrl"), "");
            }
            set
            {
                SetValue("RedirectToUrl", value);
            }
        }


        /// <summary>
        /// Gets or sets the site name.
        /// </summary>
        public string SiteName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SiteName"), "");
            }
            set
            {
                SetValue("SiteName", value);
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether the WebPart use colon behind label.
        /// </summary>
        public bool UseColonBehindLabel
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("UseColonBehindLabel"), true);
            }
            set
            {
                SetValue("UseColonBehindLabel", value);
            }
        }


        /// <summary>
        /// Gets or sets the message which is displayed after validation failed.
        /// </summary>
        public string ValidationErrorMessage
        {
            get
            {
                return ValidationHelper.GetString(GetValue("ValidationErrorMessage"), "");
            }
            set
            {
                SetValue("ValidationErrorMessage", value);
            }
        }


        /// <summary>
        /// Gets or sets the conversion track name used after successful registration.
        /// </summary>
        public string TrackConversionName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("TrackConversionName"), "");
            }
            set
            {
                if (value.Length > 400)
                {
                    value = value.Substring(0, 400);
                }
                SetValue("TrackConversionName", value);
            }
        }


        /// <summary>
        /// Gets or sets the conversion value used after successful registration.
        /// </summary>
        public double ConversionValue
        {
            get
            {
                return ValidationHelper.GetDouble(GetValue("ConversionValue"), 0);
            }
            set
            {
                SetValue("ConversionValue", value);
            }
        }

        #endregion


        #region "Methods"

        protected override void OnLoad(EventArgs e)
        {
            viewBiz.OnAfterSave += viewBiz_OnAfterSave;
            viewBiz.OnValidationFailed += viewBiz_OnValidationFailed;
            phErrorLabel.Visible = false;

            TransformationHelper.RegisterStartUpScript("$('span.ErrorLabel').remove();", Page);
            base.OnLoad(e);
        }

        void viewBiz_OnValidationFailed(object sender, EventArgs e)
        {
            phErrorLabel.Visible = true;
        }


        /// <summary>
        /// Content loaded event handler.
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
        }


        /// <summary>
        /// Reloads data for partial caching.
        /// </summary>
        public override void ReloadData()
        {
            base.ReloadData();
            SetupControl();
        }


        /// <summary>
        /// Initializes the control properties.
        /// </summary>
        protected void SetupControl()
        {
            if (StopProcessing)
            {
                // Do nothing
                viewBiz.StopProcessing = true;
            }
            else
            {
                // Set BizForm properties
                viewBiz.FormName = BizFormName;
                viewBiz.SiteName = SiteName;
                viewBiz.UseColonBehindLabel = UseColonBehindLabel;
                viewBiz.AlternativeFormFullName = AlternativeFormName;
                viewBiz.ValidationErrorMessage = ValidationErrorMessage;
                viewBiz.FormRedirectToUrl = RedirectToUrl;

                // Set the live site context
                viewBiz.ControlContext.ContextName = CMS.ExtendedControls.ControlContext.LIVE_SITE;
            }
        }


        void viewBiz_OnAfterSave(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TrackConversionName)) return;
            var siteName = SiteContext.CurrentSiteName;

            if (AnalyticsHelper.AnalyticsEnabled(siteName) && AnalyticsHelper.TrackConversionsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, RequestContext.UserHostAddress))
            {
                HitLogProvider.LogConversions(SiteContext.CurrentSiteName, LocalizationContext.PreferredCultureCode, TrackConversionName, 0, ConversionValue);
            }
        }

        #endregion
    }
}