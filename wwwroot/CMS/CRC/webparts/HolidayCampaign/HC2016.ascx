﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HC2016.ascx.cs" Inherits="CMSApp.CRC.webparts.HolidayCampaign.HC2016" %>
<asp:PlaceHolder ID="phVersionNumber" runat="server">
    <script type="text/javascript">
        var ab_test_version = <%# VersionNumber %>;
    </script>
</asp:PlaceHolder>
<%--<asp:Button ID="btnHidden" runat="server" style="display: none" UseSubmitBehavior="False"  ClientIDMode="Static" />--%>
<!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-KTJ6QC" height="0" width="0" style="display: none; visibility: hidden"></iframe>
</noscript>
<!-- End Google Tag Manager -->


<main>
    <section id="problem" class="screen" data-start="z-index: 100" data-400p="z-index:0">

        <header class="hero-image">
            <div class="logo-social-share">
                <div class="logo">
                    <a id="logo" class="show" target="_blank" data-bind="attr: { href: LogoLink }">
                        <img alt="Red Cross" src="/crc/holiday-2016/img/ui/logo-<%# IsFrench ? "fr" : "en" %>.svg" />
                    </a>
                </div>

                <div class="social-share">
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                        <ul class="">
                            <li>
                                <p data-bind="text: ResolveCultureString(UIStrings.ShareText); "></p>
                            </li>
                            <li>
                                <a title="facebook" class="addthis_button_facebook" data-event="social-event" data-social-action="share" data-social-network="facebook" data-social-target="http://<%# Request.Url.Host %><%# CurrentUrl %>" addthis:url="http://<%# Request.Url.Host %><%# CurrentUrl %>" target="_blank" rel="nofollow">
                                    <img alt="" src="/crc/holiday-2016/img/ui/icon-facebook.svg" />
                                </a>
                            </li>
                            <li>
                                <a title="twitter" class="addthis_button_twitter" data-event="social-event" data-social-action="share" data-social-network="twitter" data-social-target="<%# BitlyUrl %>" addthis:url="<%# BitlyUrl %>" target="_blank" rel="nofollow">
                                    <img alt="" src="/crc/holiday-2016/img/ui/icon-twitter.svg" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div class="problem-headline">
            <div class="headline screen-interactive-element" data-25p="opacity: 1" data-125p="opacity: 0">
                <h1 data-bind="text: ResolveCultureString(CurrentCampaign.problemHeader)"></h1>
                <h2 data-bind="text: ResolveCultureString(CurrentCampaign.problemSubheader)"></h2>
                <p data-bind="text: ResolveCultureString(CurrentCampaign.problemBody)"></p>
            </div>


            <div class="arrow-down screen-interactive-element" data-start="top: 75%" data-15p="top: 120%">
                <div class="arrow" data-bind="click: TriggerUIScroll.bind($data, 2)">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="35px" height="35px" viewBox="0 0 35 35" style="enable-background: new 0 0 35 35;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #ED1C24;
                            }

                            .st1 {
                                fill: #FFFFFF;
                            }
                        </style>
                        <!-- <circle class="st0" cx="17.5" cy="17.5" r="17.5"/> -->
                        <polygon class="st0" points="25.3,15.4 17.5,23.2 17.5,23.1 17.5,23.2 9.7,15.4 11.8,13.3 17.5,19 23.2,13.3 " />
                    </svg>
                </div>
            </div>
        </div>

    </section>
    <asp:MultiView ID="mvViews" runat="server">
        <asp:View runat="server" ID="vControlVersion">
            <!-- v1 - control version -->

            <section id="donate" class="screen" data-start="z-index: 0" data-700p="z-index: 0" data-900p="z-index: 120">

                <%--<div class="logo-social-share">
                    <div class="logo">
                        <a id="logo" class="show" target="_blank" data-bind="attr: { href: LogoLink }">
                            <img alt="Red Cross" src="/crc/holiday-2016/img/ui/logo-<%# IsFrench ? "fr" : "en" %>.svg" />
                        </a>
                    </div>

                    <div class="social-share">
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <ul class="">
                                <li>
                                    <p data-bind="text: ResolveCultureString(UIStrings.ShareText); "></p>
                                </li>
                                <li>
                                    <a title="facebook" class="addthis_button_facebook" data-event="social-event" data-social-action="share" data-social-network="facebook" data-social-target="http://<%# Request.Url.Host %><%# CurrentUrl %>" addthis:url="http://<%# Request.Url.Host %><%# CurrentUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-facebook.svg" />
                                    </a>
                                </li>
                                <li>
                                    <a title="twitter" class="addthis_button_twitter" data-event="social-event" data-social-action="share" data-social-network="twitter" data-social-target="<%# BitlyUrl %>" addthis:url="<%# BitlyUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-twitter.svg" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>--%>


                <div class="container">
                    <div class="headline align-top screen-interactive-element row column" data-950p="opacity: 0" data-1050p="opacity: 1">
                        <h1 data-bind="text: ResolveCultureString(CurrentCampaign.donateHeader)"></h1>
                    </div>

                    <div class="row column mobile-match-subhead">
                        <div class="match-icon">
                            <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                        </div>

                        <p class="desc">
                            <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                            <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                        </p>
                    </div>
                    <div class="donate-container group donation-cart-container screen-interactive-element row" data-1050p="top: 120%" data-1250p="top: 0%">

                        <div class="row column mobile-instruction">
                            <p data-bind="text: ResolveCultureString(UIStrings.DonationItemsInstructionsMobileText)"></p>
                        </div>

                        <div class="panel-left screen-interactive-element">
                            <div class="row column">
                                <p class="instruction hide-on-mobile" data-bind="text: ResolveCultureString(UIStrings.DonationItemsInstructionText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.DonationItemsMobileItemsInstructionText)"></p>
                            </div>

                            <div class="row gift-items">
                                <div class="small-6 medium-12 large-12 columns international">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeInternationalText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(0,3), as: 'donationItem' }">
                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="css: { added: donationItem.count() > 0 }, click: $root.AddDonationItem, clickBubble: false, visible: $index() <= 2">

                                                <div class="front inactive-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() === 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">
                                                    <span class="add">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            width="35px" height="35px" viewBox="0 0 35 35" style="enable-background: new 0 0 35 35;" xml:space="preserve">
                                                            <style type="text/css">
                                                                .st0 {
                                                                    fill: #ED1C24;
                                                                }

                                                                .st1 {
                                                                    fill: #FFFFFF;
                                                                }
                                                            </style>
                                                            <circle class="st0" cx="17.5" cy="17.5" r="17.5" />
                                                            <rect x="16" y="10.5" class="st1" width="3" height="14" />
                                                            <rect x="10.5" y="16" class="st1" width="14" height="3" />
                                                        </svg>
                                                    </span>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                                <div class="back active-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.activeImage) }">
                                                    <span class="add" data-bind="text: donationItem.count"></span>
                                                    <span class="remove" data-bind="css: { active: donationItem.count() > 0 }, click: $root.RemoveDonationItem, clickBubble: false">-</span>
                                                    <span class="details" data-bind="text: donationItem.donationMessage"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="small-6 medium-12 large-12 columns domestic">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeDomesticText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(3,6), as: 'donationItem' }">

                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="css: { added: donationItem.count() > 0 }, click: $root.AddDonationItem, clickBubble: false">

                                                <div class="front inactive-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() === 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">
                                                    <span class="add">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            width="35px" height="35px" viewBox="0 0 35 35" style="enable-background: new 0 0 35 35;" xml:space="preserve">
                                                            <style type="text/css">
                                                                .st0 {
                                                                    fill: #ED1C24;
                                                                }

                                                                .st1 {
                                                                    fill: #FFFFFF;
                                                                }
                                                            </style>
                                                            <circle class="st0" cx="17.5" cy="17.5" r="17.5" />
                                                            <rect x="16" y="10.5" class="st1" width="3" height="14" />
                                                            <rect x="10.5" y="16" class="st1" width="14" height="3" />
                                                        </svg>
                                                    </span>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                                <div class="back active-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.activeImage) }">
                                                    <span class="add" data-bind="text: donationItem.count"></span>
                                                    <span class="remove" data-bind="css: { active: donationItem.count() > 0 }, click: $root.RemoveDonationItem, clickBubble: false">-</span>
                                                    <span class="details" data-bind="text: donationItem.donationMessage"></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel-right">
                            <div class="row column">
                                <p class="hide-on-mobile" data-bind="text: ResolveCultureString(UIStrings.GiftBoxInstructionText)"></p>
                                <p class="instruction hide-on-mobile" data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountText)"></p>

                                <div class="gift-box-panel group">

                                    <ul class="gift-box-amounts" data-bind="foreach: { data: GiftBoxAmounts, as: 'amount' }">
                                        <li class="gift-box-amount" data-bind="click: $root.SetPresetGiftBoxAmount, css: $index()+1">
                                            <span data-bind="text: $root.FormatCurrencyString(amount)"></span>
                                        </li>
                                    </ul>

                                    <div class="gift-box-user-defined-amount">
                                        <label data-bind="css: CurrentCulture">
                                            <span class="currency-symbol" data-bind="text: ResolveCultureString(UIStrings.CurrencySymbol)"></span>
                                            <input id="txtUserDefineGiftBoxAmount" data-bind="value: GiftBoxUserAmount, attr: { placeholder: ResolveCultureString(UIStrings.GiftBoxPlaceholderText) }" type="text">
                                            <span class="gift-box-check" data-bind="css: { active: GiftBoxUserAmount() > 0 }"><i class="fa fa-check"></i></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row column hide-on-mobile">
                                <div id="gift-box-container">
                                    <div class="gift-box" data-bind="css: { added: DonationCart.GiftBoxIsActive() }">
                                        <div class="front inactive-state">
                                            <img data-bind="attr: { src: GiftBoxInactiveImage, alt: ResolveCultureString(UIStrings.GiftBoxInactiveImageAltText) }" />
                                        </div>
                                        <div class="back active-state">
                                            <img src="/crc/holiday-2016/img/items/box-with-supplies-active.jpg" alt="Box with Supplies" />
                                            <span class="desc" data-bind="text: ResolveCultureString(UIStrings.GiftBoxActiveText)"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttons group row" data-1050p="top: 130%" data-1250p="top:0%">
                        <div class="small-12 medium-12 large-7 columns">
                            <div class="your-donation row">
                                <div class="match small-12 medium-7 large-7 columns hide-on-mobile">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                                        <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                                    </p>

                                    <div class="match-icon">
                                        <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                                    </div>

                                </div>

                                <div class="small-12 medium-5 large-5 columns mobile-donation responsive-donation">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartYourDonationText)"></span>
                                        <span class="reset" data-bind="text: ResolveCultureString(UIStrings.DonationCartResetText), click: ResetDonationCart, css: { active: DonationCart.IsActive }"></span>
                                    </p>

                                    <span class="total" data-bind="text: FormatCurrencyString(DonationCart.Total()), css: { active: DonationCart.IsActive }"></span>
                                </div>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-5 columns">
                            <button type="button" class="complete flex" data-bind="css: { active: DonationCart.IsActive }, enable: DonationCart.IsActive, click: OpenLightbox" data-open="complete-donation">
                                <p>
                                    <span data-bind="text: ResolveCultureString(UIStrings.CompleteDonationText)"></span>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </p>
                            </button>
                        </div>
                    </div>
                    <div class="reveal" id="complete-donation" data-reveal>
                        <div class="row column">
                            <h2 class="reveal-head" data-bind="text: ResolveCultureString(UIStrings.DonationTypeHeader)"></h2>
                        </div>
                        <div class="donate-options">
                            <div class="donate-box">
                                <button class="donation-type-button" value="self" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/hand.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text:ResolveCultureString(UIStrings.DonationTypeSelfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button class="donation-type-button" value="giftPrinted" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/print-card.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button class="donation-type-button" value="giftEcard" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/ecard.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfEcard)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>

                        <div class="row column note-row">
                            <span class="note"
                                data-bind="text: ResolveCultureString(UIStrings.DonationTypeTaxReturnText)"></span>
                        </div>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- end reveal -->
                </div>

                <footer class="footer-element footer-info" data-1200p="opacity: 0;" data-1250p="opacity: 1">
                    <p>
                        <span data-bind="html: ResolveCultureString(UIStrings.FooterCopyText)"></span>
                        <a href="http://<%# AlternateLanguageDomain %><%# AlternateLanguageUrlPath %>"><%# IsFrench ? "English" : "Français" %></a> | <a target="_blank" href="/<%# IsFrench ? "protection-des-renseignements-personnels" : "privacy-policy" %>"><%# IsFrench ? "Politique de confidentialité" : "Privacy Policy"%></a> &copy; <%# IsFrench ? "Croix-Rouge canadienne" : "Red Cross Canada" %> <span data-bind="text: new Date().getFullYear()"></span>
                    </p>
                </footer>
            </section>


            <!-- end v1 -->
        </asp:View>
        <asp:View runat="server" ID="vPSTest">
            <!-- v2 - pseudo-set test -->

            <section id="donate" class="screen" data-start="z-index: 0" data-700p="z-index: 0" data-900p="z-index: 120">

                <%--<div class="logo-social-share">
                    <div class="logo">
                        <a id="logo" class="show" target="_blank" data-bind="attr: { href: LogoLink }">
                            <img alt="Red Cross" src="/crc/holiday-2016/img/ui/logo-<%# IsFrench ? "fr" : "en" %>.svg" />
                        </a>
                    </div>

                    <div class="social-share">
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <ul class="">
                                <li>
                                    <p data-bind="text: ResolveCultureString(UIStrings.ShareText); "></p>
                                </li>
                                <li>
                                    <a title="facebook" class="addthis_button_facebook" data-event="social-event" data-social-action="share" data-social-network="facebook" data-social-target="http://<%# Request.Url.Host %><%# CurrentUrl %>" addthis:url="http://<%# Request.Url.Host %><%# CurrentUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-facebook.svg" />
                                    </a>
                                </li>
                                <li>
                                    <a title="twitter" class="addthis_button_twitter" data-event="social-event" data-social-action="share" data-social-network="twitter" data-social-target="<%# BitlyUrl %>" addthis:url="<%# BitlyUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-twitter.svg" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>--%>

                <div class="container">
                    <div class="headline ps-headline align-top screen-interactive-element row column" data-950p="opacity: 0" data-1050p="opacity: 1">
                        <h1 data-bind="text: ResolveCultureString(CurrentCampaign.donatePSTestHeader)"></h1>
                    </div>

                    <div class="row column mobile-match-subhead">
                        <div class="match-icon">
                            <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                        </div>
                        <p class="desc">
                            <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                            <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                        </p>
                    </div>

                    <div class="donate-container group donation-cart-container screen-interactive-element row" data-1050p="top: 120%" data-1250p="top: 0%">

                        <div class="panel-left screen-interactive-element">
                            <div class="row column">
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.DonationItemsPSTestInstructionsMobiileText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountText)"></p>
                            </div>

                            <div class="row column">
                                <div class="pseudo-mobile-user-defined-amount flex">
                                    <label data-bind="css: CurrentCulture" class="gift-box-amount">
                                        <p><span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountTextPS)"></span></p>
                                        <div class="test-user-defined-amount">
                                            <span class="currency-symbol" data-bind="text: ResolveCultureString(UIStrings.CurrencySymbol)"></span>
                                            <input id="txtUserDefineGiftBoxAmount" data-bind="value: GiftBoxUserAmount, event:{ submit: UpdateInputChart}" type="text" autocomplete="off">
                                            <span class="gift-box-check" data-bind="css: { active: GiftBoxUserAmount() > 0 }"><i class="fa fa-check"></i></span>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row column">
                                <p class="instruction hide-on-mobile" data-bind="text: ResolveCultureString(UIStrings.DonationItemsPSTestInstructionsText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.DonationItemsMobileItemsInstructionText)"></p>
                            </div>

                            <div class="row gift-items">
                                <div class="small-6 medium-12 large-12 columns international">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeInternationalText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(0,3), as: 'donationItem' }">

                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="click: $root.AddDonationItem, clickBubble: false">

                                                <div class="front inactive-state ps-test-front" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">

                                                    <div class="ps-test-add square" data-bind="css: { active: donationItem.count() > 0 }">
                                                        <span class="checked"><i class="fa fa-check"></i></span>
                                                    </div>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="small-6 medium-12 large-12 columns domestic">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeDomesticText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(3,6), as: 'donationItem' }">

                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="click: $root.AddDonationItem, clickBubble: false">

                                                <div class="front inactive-state ps-test-front" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">

                                                    <div class="ps-test-add square" data-bind="css: { active: donationItem.count() > 0 }">
                                                        <span class="checked"><i class="fa fa-check"></i></span>
                                                    </div>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row column select-buttons hide-on-mobile">
                                <div class="select-all-button">
                                    <button type="button" data-bind="click: SelectAll" class="select-all selectAll">
                                        <p>
                                            <span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAllText)"></span>
                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </p>
                                    </button>
                                </div>

                                <div class="pseudo-test-user-defined-amount">
                                    <label data-bind="css: CurrentCulture" class="gift-box-amount flex">
                                        <p><span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountTextPS)"></span></p>
                                        <div class="test-user-defined-amount">
                                            <span class="currency-symbol" data-bind="text: ResolveCultureString(UIStrings.CurrencySymbol)"></span>
                                            <input id="txtUserDefineGiftBoxAmount" data-bind="value: GiftBoxUserAmount, event:{ submit: UpdateInputChart}" type="text" autocomplete="off">
                                            <span class="gift-box-check" data-bind="css: { active: GiftBoxUserAmount() > 0 }"><i class="fa fa-check"></i></span>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row column select-all-link">
                                <button type="button" data-bind="click: SelectAll" class="selectAll">
                                    <p>
                                        <span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAllText)"></span>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </p>
                                </button>
                            </div>
                        </div>

                        <div class="panel-right hide-on-mobile">
                            <div class="item-tooltip">
                                <p class="donationMsg"></p>
                                <p class="user-defined-amount"></p>
                            </div>

                            <div id="map-box-container">
                                <div class="map-box">
                                    <div class="map">
                                        <div class="chart">
                                            <canvas id="myChart" width="350" height="350">
                                                <desc>Outline of the globe</desc>
                                            </canvas>
                                        </div>

                                        <div class="map-text" aria-live="polite" role="region">
                                            <p data-bind="text: ResolveCultureString(UIStrings.GlobalSurvivalKit)"></p>
                                            <h2 class="fill-number"></h2>
                                            <p class="is-full"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row column small-screen-tooltip">
                        <div class="item-tooltip">
                            <p class="donationMsg"></p>
                            <p class="user-defined-amount"></p>
                        </div>
                    </div>

                    <div class="callout mobile-tooltip" id="mobileTooltip">
                        <p class="donationMsg"></p>
                        <p class="user-defined-amount"></p>
                        <button class="close-button" aria-label="Dismiss tooltip" type="button" data-bind="click: closeMobileTooltip">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="buttons group row" data-1050p="top: 130%" data-1250p="top:0%">
                        <div class="small-12 medium-12 large-7 columns">
                            <div class="your-donation row">


                                <div class="mobile-progress small-12 columns">
                                    <p class="mobileProgressBar"></p>
                                    <p class="mobile-progress-instruction" data-bind="text: ResolveCultureString(UIStrings.MobileProgressInstruction)"></p>
                                    <div class="primary progress" role="progressbar" tabindex="0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="200">
                                        <div class="progress-meter" data-bind="style: $root.calculateWidth() "></div>
                                    </div>
                                </div>
                                <div class="match small-12 medium-7 large-7 columns hide-on-mobile">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                                        <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                                    </p>

                                    <div class="match-icon">
                                        <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                                    </div>

                                </div>
                                <div class="small-12 medium-12 large-5 columns mobile-donation responsive-donation">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartYourDonationText)"></span>
                                        <span class="reset" data-bind="text: ResolveCultureString(UIStrings.DonationCartResetText), click: ResetDonationCart, css: { active: DonationCart.IsActive }"></span>
                                    </p>

                                    <span class="total" data-bind="text: FormatCurrencyString(DonationCart.Total()), css: { active: DonationCart.IsActive }"></span>
                                </div>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-5 columns">
                            <button type="button" class="complete flex" data-bind="css: { active: DonationCart.IsActive }, enable: DonationCart.IsActive, click: OpenLightbox" data-open="complete-donation-ps">
                                <p>
                                    <span data-bind="text: ResolveCultureString(UIStrings.CompleteDonationText)"></span>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </p>
                            </button>
                        </div>
                    </div>

                    <div class="reveal" id="complete-donation-ps" data-reveal>
                        <div class="row column">
                            <h2 class="reveal-head" data-bind="text: ResolveCultureString(UIStrings.DonationTypeHeader)"></h2>
                        </div>
                        <div class="donate-options">
                            <div class="donate-box">
                                <button value="selfPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/hand.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text:ResolveCultureString(UIStrings.DonationTypeSelfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button value="giftPrintedPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/print-card.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button value="giftEcardPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/ecard.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfEcard)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>

                        <div class="row column note-row">
                            <span class="note"
                                data-bind="text: ResolveCultureString(UIStrings.DonationTypeTaxReturnText)"></span>
                        </div>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- end reveal -->
                </div>

                <footer class="footer-element footer-info" data-1200p="opacity: 0;" data-1250p="opacity: 1">
                    <p>
                        <span data-bind="html: ResolveCultureString(UIStrings.FooterCopyText)"></span>
                        <a href="http://<%# AlternateLanguageDomain %><%# AlternateLanguageUrlPath %>"><%# IsFrench ? "English" : "Français" %></a> | <a target="_blank" href="/<%# IsFrench ? "protection-des-renseignements-personnels" : "privacy-policy" %>"><%# IsFrench ? "Politique de confidentialité" : "Privacy Policy"%></a> &copy; <%# IsFrench ? "Croix-Rouge canadienne" : "Red Cross Canada" %> <span data-bind="text: new Date().getFullYear()"></span>
                    </p>
                </footer>
            </section>

            <!-- end v2 -->
        </asp:View>
        <asp:View runat="server" ID="vPSNonTest">
            <!-- v3 - non-pseudo-set test -->

            <section id="donate" class="screen" data-start="z-index: 0" data-700p="z-index: 0" data-900p="z-index: 120">
                <%--<div class="logo-social-share">
                    <div class="logo">
                        <a id="logo" class="show" target="_blank" data-bind="attr: { href: LogoLink }">
                            <img alt="Red Cross" src="/crc/holiday-2016/img/ui/logo-<%# IsFrench ? "fr" : "en" %>.svg" />
                        </a>
                    </div>

                    <div class="social-share">
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <ul class="">
                                <li>
                                    <p data-bind="text: ResolveCultureString(UIStrings.ShareText); "></p>
                                </li>
                                <li>
                                    <a title="facebook" class="addthis_button_facebook" data-event="social-event" data-social-action="share" data-social-network="facebook" data-social-target="http://<%# Request.Url.Host %><%# CurrentUrl %>" addthis:url="http://<%# Request.Url.Host %><%# CurrentUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-facebook.svg" />
                                    </a>
                                </li>
                                <li>
                                    <a title="twitter" class="addthis_button_twitter" data-event="social-event" data-social-action="share" data-social-network="twitter" data-social-target="<%# BitlyUrl %>" addthis:url="<%# BitlyUrl %>" target="_blank" rel="nofollow">
                                        <img alt="" src="/crc/holiday-2016/img/ui/icon-twitter.svg" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>--%>
                <div class="container">
                    <div class="headline align-top screen-interactive-element row column" data-950p="opacity: 0" data-1050p="opacity: 1">
                        <h1 data-bind="text: ResolveCultureString(CurrentCampaign.donateHeader)"></h1>
                    </div>


                    <div class="row column mobile-match-subhead">
                        <div class="match-icon">
                            <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                        </div>

                        <p class="desc">
                            <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                            <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                        </p>
                    </div>
                    <div class="donate-container group donation-cart-container screen-interactive-element row" data-1050p="top: 120%" data-1250p="top: 0%">
                        <div class="panel-left screen-interactive-element">

                            <div class="row column">
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.DonationItemsNonPSTestInstructionsText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountText)"></p>
                            </div>

                            <div class="row column">
                                <div class="pseudo-mobile-user-defined-amount flex">
                                    <label data-bind="css: CurrentCulture" class="gift-box-amount">
                                        <p><span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountTextPS)"></span></p>
                                        <div class="test-user-defined-amount">
                                            <span class="currency-symbol" data-bind="text: ResolveCultureString(UIStrings.CurrencySymbol)"></span>
                                            <input id="txtUserDefineGiftBoxAmount" data-bind="value: GiftBoxUserAmount, event:{ submit: UpdateInputChart}" type="text" autocomplete="off">
                                            <span class="gift-box-check" data-bind="css: { active: GiftBoxUserAmount() > 0 }"><i class="fa fa-check"></i></span>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row column">
                                <p class="instruction hide-on-mobile" data-bind="text: ResolveCultureString(UIStrings. DonationItemsNonPSTestInstructionsText)"></p>
                                <p class="mobile-instruction" data-bind="text: ResolveCultureString(UIStrings.DonationItemsMobileItemsInstructionText)"></p>
                            </div>

                            <div class="row gift-items">
                                <div class="small-6 medium-12 large-12 columns international">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeInternationalText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(0,3), as: 'donationItem' }">
                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="css: { added: donationItem.count() > 0 }, click: $root.AddDonationItem, clickBubble: false, visible: $index() <= 2">

                                                <div class="front-ps inactive-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() === 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">
                                                    <span class="add">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            width="35px" height="35px" viewBox="0 0 35 35" style="enable-background: new 0 0 35 35;" xml:space="preserve">
                                                            <style type="text/css">
                                                                .st0 {
                                                                    fill: #ED1C24;
                                                                }

                                                                .st1 {
                                                                    fill: #FFFFFF;
                                                                }
                                                            </style>
                                                            <circle class="st0" cx="17.5" cy="17.5" r="17.5" />
                                                            <rect x="16" y="10.5" class="st1" width="3" height="14" />
                                                            <rect x="10.5" y="16" class="st1" width="14" height="3" />
                                                        </svg>
                                                    </span>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                                <div class="ps-active" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.activeImage) }">
                                                    <span class="add" data-bind="text: donationItem.count"></span>
                                                    <span class="remove" data-bind="css: { active: donationItem.count() > 0 }, click: $root.RemoveDonationItem, clickBubble: false">-</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="small-6 medium-12 large-12 columns domestic">
                                    <div class="row column">
                                        <span class="donation-type-text" data-bind="text: ResolveCultureString(UIStrings.DonationItemTypeDomesticText)"></span>
                                    </div>

                                    <div id="item-collection" class="items group row" data-bind="foreach: { data: DonationCart.DonationItemsCollection.Items.slice(3,6), as: 'donationItem' }">

                                        <div class="item-container small-12 medium-4 large-4 columns">
                                            <div class="item" data-bind="css: { added: donationItem.count() > 0 }, click: $root.AddDonationItem, clickBubble: false">

                                                <div class="front-ps inactive-state" data-bind="css: donationItem.codeName, css: { active: donationItem.count() === 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.defaultImage) }">
                                                    <span class="add">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            width="35px" height="35px" viewBox="0 0 35 35" style="enable-background: new 0 0 35 35;" xml:space="preserve">
                                                            <style type="text/css">
                                                                .st0 {
                                                                    fill: #ED1C24;
                                                                }

                                                                .st1 {
                                                                    fill: #FFFFFF;
                                                                }
                                                            </style>
                                                            <circle class="st0" cx="17.5" cy="17.5" r="17.5" />
                                                            <rect x="16" y="10.5" class="st1" width="3" height="14" />
                                                            <rect x="10.5" y="16" class="st1" width="14" height="3" />
                                                        </svg>
                                                    </span>

                                                    <div class="details">
                                                        <span class="gift-amount" data-bind="text: $root.FormatCurrencyString(donationItem.price) "></span>
                                                        <span class="gift-name" data-bind="text: donationItem.name"></span>
                                                    </div>
                                                </div>
                                                <!-- end front -->

                                                <div class="ps-active" data-bind="css: donationItem.codeName, css: { active: donationItem.count() > 0 }, style: { backgroundImage: $root.GetDonationItemImageUrl(donationItem.activeImage) }">
                                                    <span class="add" data-bind="css: { active: donationItem.count() > 0 }, text: donationItem.count"></span>
                                                    <span class="remove" data-bind="css: { active: donationItem.count() > 0 }, click: $root.RemoveDonationItem, clickBubble: false">-</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row select-buttons hide-on-mobile">
                                <div class="nonpseudo-test-user-defined-amount">
                                    <label data-bind="css: CurrentCulture" class="gift-box-amount flex">
                                        <p><span data-bind="text: ResolveCultureString(UIStrings.GiftBoxSelectAmountTextPS)"></span></p>
                                        <div class="test-user-defined-amount">
                                            <span class="currency-symbol" data-bind="text: ResolveCultureString(UIStrings.CurrencySymbol)"></span>
                                            <input id="txtUserDefineGiftBoxAmount" data-bind="value: GiftBoxUserAmount, event:{ submit: showAllPins}" type="text" autocomplete="off">
                                            <span class="gift-box-check" data-bind="css: { active: GiftBoxUserAmount() > 0 }"><i class="fa fa-check"></i></span>
                                        </div>
                                    </label>
                                </div>
                            </div>

                        </div>


                        <div class="panel-right hide-on-mobile">
                            <div class="item-tooltip">
                                <p class="donationMsg"></p>
                                <p class="user-defined-amount"></p>
                                <p></p>
                            </div>

                            <div id="map-box-container-nonps">
                                <div class="map-box">
                                    <div class="map-nonps">
                                        <div class="pin pin-1">
                                            <img src="/crc/holiday-2016/img/ui/pin1.png" alt="">
                                        </div>
                                        <div class="pin pin-2">
                                            <img src="/crc/holiday-2016/img/ui/pin2.png" alt="">
                                        </div>
                                        <div class="pin pin-3">
                                            <img src="/crc/holiday-2016/img/ui/pin2.png" alt="">
                                        </div>
                                        <div class="pin pin-4">
                                            <img src="/crc/holiday-2016/img/ui/pin4.png" alt="">
                                        </div>
                                        <div class="pin pin-5">
                                            <img src="/crc/holiday-2016/img/ui/pin5.png" alt="">
                                        </div>
                                        <div class="pin pin-6">
                                            <img src="/crc/holiday-2016/img/ui/pin6.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row column small-screen-tooltip">
                        <div class="item-tooltip">
                            <p class="donationMsg"></p>
                            <p class="user-defined-amount"></p>
                        </div>
                    </div>

                    <div class="callout mobile-tooltip tooltip-nonps" id="mobileTooltip">
                        <p class="donationMsg"></p>
                        <p class="user-defined-amount"></p>
                        <button class="close-button" aria-label="Dismiss tooltip" type="button" data-bind="click: closeMobileTooltip">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="buttons group row" data-1050p="top: 130%" data-1250p="top:0%">
                        <div class="small-12 medium-12 large-7 columns">
                            <div class="your-donation row">

                                <div class="match small-12 medium-7 large-7 columns hide-on-mobile">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchText)"></span>
                                        <span data-bind="text: ResolveCultureString(UIStrings.DonationCartMatchSubbHead)"></span>
                                    </p>

                                    <div class="match-icon">
                                        <img src="/crc/holiday-2016/img/ui/BMO.png" alt="">
                                    </div>

                                </div>
                                <div class="small-12 medium-12 large-5 columns mobile-donation responsive-donation">
                                    <p class="desc">
                                        <span class="label" data-bind="text: ResolveCultureString(UIStrings.DonationCartYourDonationText)"></span>
                                        <span class="reset" data-bind="text: ResolveCultureString(UIStrings.DonationCartResetText), click: ResetDonationCart, css: { active: DonationCart.IsActive }"></span>
                                    </p>

                                    <span class="total" data-bind="text: FormatCurrencyString(DonationCart.Total()), css: { active: DonationCart.IsActive }"></span>
                                </div>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-5 columns">
                            <button type="button" class="complete flex" data-bind="css: { active: DonationCart.IsActive }, enable: DonationCart.IsActive, click: OpenLightbox" data-open="complete-donation-nonps">
                                <p>
                                    <span data-bind="text: ResolveCultureString(UIStrings.CompleteDonationText)"></span>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </p>
                            </button>
                        </div>
                    </div>

                    <div class="reveal" id="complete-donation-nonps" data-reveal>
                        <div class="row column">
                            <h2 class="reveal-head" data-bind="text: ResolveCultureString(UIStrings.DonationTypeHeader)"></h2>
                        </div>
                        <div class="donate-options">
                            <div class="donate-box">
                                <button value="selfNonPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/hand.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text:ResolveCultureString(UIStrings.DonationTypeSelfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button value="giftPrintedNonPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/print-card.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfText)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>

                            <div class="donate-box">
                                <button value="giftEcardNonPS" data-bind="click: CompleteDonation">
                                    <div class="row">
                                        <div class="small-3 medium-12 large-12 columns">
                                            <div class="donate-icon">
                                                <img src="/crc/holiday-2016/img/ui/ecard.png" alt="">
                                            </div>
                                        </div>
                                        <div class="small-9 medium-12 large-we columns">
                                            <p data-bind="text: ResolveCultureString(UIStrings.DonationTypeOnBehalfEcard)"></p>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>

                        <div class="row column note-row">
                            <span class="note"
                                data-bind="text: ResolveCultureString(UIStrings.DonationTypeTaxReturnText)"></span>
                        </div>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- end reveal -->
                </div>

                <footer class="footer-element footer-info" data-1200p="opacity: 0;" data-1250p="opacity: 1">
                    <p>
                        <span data-bind="html: ResolveCultureString(UIStrings.FooterCopyText)"></span>
                        <a href="http://<%# AlternateLanguageDomain %><%# AlternateLanguageUrlPath %>"><%# IsFrench ? "English" : "Français" %></a> | <a target="_blank" href="/<%# IsFrench ? "protection-des-renseignements-personnels" : "privacy-policy" %>"><%# IsFrench ? "Politique de confidentialité" : "Privacy Policy"%></a> &copy; <%# IsFrench ? "Croix-Rouge canadienne" : "Red Cross Canada" %> <span data-bind="text: new Date().getFullYear()"></span>
                    </p>
                </footer>
            </section>

            <!-- end v3 -->
        </asp:View>
    </asp:MultiView>
</main>


<!--// JavaScript //-->

<script src="/CMSPages/GetResource.ashx?scriptfile=/CRC/holiday-2016/js/libs.js"></script>
<script src="/CMSPages/GetResource.ashx?scriptfile=/CRC/holiday-2016/js/app.min.js"></script>
<script src="/CMSPages/GetResource.ashx?scriptfile=/CRC/holiday-2016/js/main.js"></script>


<!--// AddThis //-->
<script type="text/javascript">

    var addthis_config = {
        data_track_clickback: false
    };

    var addthis_share =
    {
        templates: {
            twitter: '<%# IsFrench ? $"La façon idéale de transformer des vies! Offrez un cadeau de la @croixrouge_qc à un proche ou collègue cette année {BitlyUrl}" : $"The easiest way to make an impact! Give a @redcrosscanada gift to family, friends and colleagues this year {BitlyUrl}" %>'
        }
    }

    $('form').submit(function(event) {
        event.preventDefault();
    });

</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55df54636d74c94e" async="async"></script>
