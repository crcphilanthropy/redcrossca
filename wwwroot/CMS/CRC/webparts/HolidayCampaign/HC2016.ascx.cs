﻿using System;
using System.Configuration;
using CMS.Controls;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using Microsoft.Ajax.Utilities;

namespace CMSApp.CRC.webparts.HolidayCampaign
{
    public partial class HC2016 : CMSAbstractQueryFilterControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetVersion();
            DataBind();
        }

        private void SetVersion()
        {            
            if (CurrentUrl.Contains("?"))
                CurrentUrl = CurrentUrl.SubstringUpToFirst('?');

            switch (CurrentUrl)
            {
                case "/holidaykits.aspx":
                case "/holidaykits":
                case "/troussedesfetes":
                case "/troussedesfetes.aspx":
                    mvViews.SetActiveView(vPSNonTest);
                    VersionNumber = 3;
                    break;
                case "/buildyourkit.aspx":
                case "/buildyourkit":
                case "/creezvotretrousse.aspx":
                case "/creezvotretrousse":
                    mvViews.SetActiveView(vPSTest);
                    VersionNumber = 2;
                    break;
                default:
                    mvViews.SetActiveView(vControlVersion);
                    VersionNumber = 1;
                    break;
            }
        }

        protected string Lang { get { return LocalizationContext.PreferredCultureCode.Substring(0, 2); } }
        protected int VersionNumber { get; set; }

        private string _alternateLanguageDomain;
        protected string AlternateLanguageDomain
        {
            get
            {
                return _alternateLanguageDomain ?? (_alternateLanguageDomain = TransformationHelper.GetAlternateLanguageDomain());
            }
        }

        protected string AlternateLanguageUrlPath
        {
            get
            {
                switch (VersionNumber)
                {
                    case 3:
                        return IsFrench ? "/holidaykits" : "/troussedesfetes";
                    case 2:
                        return IsFrench ? "/buildyourkit" : "/creezvotretrousse";
                    default:
                        return IsFrench ? "/yourgift" : "/votredon";
                }
                
            }
        }

        protected bool IsFrench
        {
            get
            {
                return LocalizationContext.PreferredCultureCode.EndsWith("fr-ca", StringComparison.OrdinalIgnoreCase);
            }
        }

        protected string BitlyUrl
        {
            get
            {
                switch (VersionNumber)
                {
                    case 3:
                        return IsFrench ? "http://bit.ly/2fhGo61" : "http://bit.ly/2eYpO01";
                    case 2:
                        return IsFrench ? "http://bit.ly/2eYp1fc" : "http://bit.ly/2eYkLwl";
                    default:
                        return IsFrench ? "http://bit.ly/2gv3YkM" : "http://bit.ly/2fAHmvY";
                }
            }
        }

        protected string CurrentUrl { get; private set; } = RequestContext.CurrentURL.ToLower();
    }
}