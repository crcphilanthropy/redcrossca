<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxCalculatorCultureStrings.ascx.cs" Inherits="TaxCalculatorCultureStrings" %>

<asp:PlaceHolder ID="phlContent" runat="server">
    <script type="text/javascript">
        var rcTaxCalc = rcTaxCalc || {};
        rcTaxCalc.cultureStrings = {};
        rcTaxCalc.cultureStrings.provinces = {};
        
        rcTaxCalc.cultureStrings.calculateTaxTitle = '<% = ResHelper.GetString("CRC.TaxCalc.CalculateTaxTitle") %>';
        rcTaxCalc.cultureStrings.calculateTaxIntro = '<% = ResHelper.GetString("CRC.TaxCalc.CalculateTaxIntro") %>';
        rcTaxCalc.cultureStrings.selectProvince = '<% = ResHelper.GetString("CRC.TaxCalc.SelectProvince") %>';
        rcTaxCalc.cultureStrings.pleaseSelect = '<% = ResHelper.GetString("CRC.TaxCalc.PleaseSelect") %>';
        rcTaxCalc.cultureStrings.donationAmount = '<% = ResHelper.GetString("CRC.TaxCalc.DonationAmount") %>';
        rcTaxCalc.cultureStrings.calculate = '<% = ResHelper.GetString("CRC.TaxCalc.Calculate") %>';
        rcTaxCalc.cultureStrings.results = '<% = ResHelper.GetString("CRC.TaxCalc.Results") %>';
        rcTaxCalc.cultureStrings.federalTaxCredit = '<% = ResHelper.GetString("CRC.TaxCalc.FederalTaxCredit") %>';
        rcTaxCalc.cultureStrings.provincialTaxCredit = '<% = ResHelper.GetString("CRC.TaxCalc.ProvincialTaxCredit") %>';
        rcTaxCalc.cultureStrings.combinedTaxCredit = '<% = ResHelper.GetString("CRC.TaxCalc.CombinedTaxCredit") %>';
        rcTaxCalc.cultureStrings.firtTimeDonorTitle = '<% = ResHelper.GetString("CRC.TaxCalc.FirtTimeDonorTitle") %>';
        rcTaxCalc.cultureStrings.firtTimeDonorIntro = '<% = ResHelper.GetString("CRC.TaxCalc.FirtTimeDonorIntro") %>';
        rcTaxCalc.cultureStrings.areYouFirstTimeDonor = '<% = ResHelper.GetString("CRC.TaxCalc.AreYouFirstTimeDonor") %>';
        rcTaxCalc.cultureStrings.yes = '<% = ResHelper.GetString("CRC.TaxCalc.Yes") %>';
        rcTaxCalc.cultureStrings.no = '<% = ResHelper.GetString("CRC.TaxCalc.No") %>';
        rcTaxCalc.cultureStrings.bonusTaxCredit = '<% = ResHelper.GetString("CRC.TaxCalc.BonusTaxCredit") %>';
        rcTaxCalc.cultureStrings.totalMoneyBack = '<% = ResHelper.GetString("CRC.TaxCalc.TotalMoneyBack") %>';
        rcTaxCalc.cultureStrings.donateNowIntro1 = '<% = ResHelper.GetString("CRC.TaxCalc.DonateNowIntro1") %>';
        rcTaxCalc.cultureStrings.donateNowIntro2 = '<% = ResHelper.GetString("CRC.TaxCalc.DonateNowIntro2") %>';
        rcTaxCalc.cultureStrings.donateNowIntro3 = '<% = ResHelper.GetString("CRC.TaxCalc.DonateNowIntro3") %>';
        rcTaxCalc.cultureStrings.donateNowButton = '<% = ResHelper.GetString("CRC.TaxCalc.DonateNowButton") %>';
        rcTaxCalc.cultureStrings.footnote = '<% = ResHelper.GetString("CRC.TaxCalc.Footnote") %>';
        rcTaxCalc.cultureStrings.footnoteLink = '<% = ResHelper.GetString("CRC.TaxCalc.FootnoteLink") %>';
        rcTaxCalc.cultureStrings.donateNowLink = '<% = ResHelper.GetString("CRC.TaxCalc.DonateNowLink") %>';
        rcTaxCalc.cultureStrings.cultureCode = '<% = ResHelper.GetString("CRC.TaxCalc.CultureCode") %>';
        
        rcTaxCalc.cultureStrings.provinces.AB = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.AB") %>';
        rcTaxCalc.cultureStrings.provinces.BC = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.BC") %>';
        rcTaxCalc.cultureStrings.provinces.MB = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.MB") %>';
        rcTaxCalc.cultureStrings.provinces.NB = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.NB") %>';
        rcTaxCalc.cultureStrings.provinces.NL = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.NL") %>';
        rcTaxCalc.cultureStrings.provinces.NS = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.NS") %>';
        rcTaxCalc.cultureStrings.provinces.NT = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.NT") %>';
        rcTaxCalc.cultureStrings.provinces.NU = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.NU") %>';
        rcTaxCalc.cultureStrings.provinces.ON = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.ON") %>';
        rcTaxCalc.cultureStrings.provinces.PE = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.PE") %>';
        rcTaxCalc.cultureStrings.provinces.QC = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.QC") %>';
        rcTaxCalc.cultureStrings.provinces.SK = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.SK") %>';
        rcTaxCalc.cultureStrings.provinces.YT = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.YT") %>';
        rcTaxCalc.cultureStrings.provinces.OUT = '<% = ResHelper.GetString("CRC.TaxCalc.Provinces.OUT") %>';
    </script>
</asp:PlaceHolder>