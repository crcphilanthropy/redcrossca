﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmartSearchResults.ascx.cs" Inherits="CMSApp.CRC.webparts.SmartSearchResults" EnableViewState="false" %>
<%@ Register src="~/CRC/webparts/Controls/SearchResults.ascx" tagname="SearchResults" tagprefix="crc" %>
<crc:SearchResults ID="srchResults" runat="server" />