﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CMS.Helpers;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using System.Linq;

namespace CMSApp.CRC.webparts
{
    public partial class ProvinceSelection : CMSAbstractWebPart
    {
        public bool IsInFooter
        {
            get { return ValidationHelper.GetBoolean(GetValue("IsInFooter"), false); }
            set { SetValue("IsInFooter", value); }
        }

        private IEnumerable<IProvince> _provinces;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            litFoooterYourProvince.Text = litYourProvince.Text = ResourceStringHelper.GetString("CRC.IYC.YourProvince", defaultText: "Your Province");
            _provinces = new ProvinceProvider().GetAllProvinces().OrderBy(item => item.NodeOrder);
            rptProvinces.DataSource = _provinces;
            rptProvinces.DataBind();

            rptFooterProvinces.DataSource = _provinces;
            rptFooterProvinces.DataBind();

            phFooterSelection.Visible = IsInFooter;
            phHeaderSelection.Visible = !IsInFooter;
        }

        protected void UpdateProvince(object sender, CommandEventArgs e)
        {
            Variables.RegionCode = e.CommandArgument as string;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            var selectedProvince = _provinces
                                        .Where(item => !string.IsNullOrWhiteSpace(item.ProvinceCode))
                                        .FirstOrDefault(item => item.ProvinceCode.Equals(Variables.RegionCode, StringComparison.OrdinalIgnoreCase));

            litProvince.Text = selectedProvince != null ? selectedProvince.ProvinceDisplayCode : ResourceStringHelper.GetString("CRC.Select", defaultText: "Select...");
            litFoooterProvince.Text = selectedProvince != null ? selectedProvince.ProvinceDisplayCode : ResourceStringHelper.GetString("CRC.Select", defaultText: "Select...");
        }
    }
}