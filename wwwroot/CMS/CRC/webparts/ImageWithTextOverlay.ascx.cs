using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class ImageWithTextOverlay : CMSAbstractWebPart
    {
        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
            set { SetValue("Title", value); }
        }

        public string SubTitle
        {
            get { return ValidationHelper.GetString(GetValue("SubTitle"), string.Empty); }
            set { SetValue("SubTitle", value); }
        }

        public string LinkText
        {
            get { return ValidationHelper.GetString(GetValue("LinkText"), string.Empty); }
            set { SetValue("LinkText", value); }
        }

        public string Url
        {
            get { return ValidationHelper.GetString(GetValue("Url"), string.Empty); }
            set { SetValue("Url", value); }
        }

        public string Image
        {
            get { return ValidationHelper.GetString(GetValue("Image"), string.Empty); }
            set { SetValue("Image", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //DataBind();
            imgBanner.ImageUrl = this.Image;
            imgBanner.AlternateText = this.Title;
            litTitle.Text = this.Title;
            litSubTitle.Text = this.SubTitle;
            lnkDestination.Text = this.LinkText;
            lnkDestination.NavigateUrl = TransformationHelper.getCultureDocumentUrl(this.Url);
            lnkDestination.Visible = !string.IsNullOrWhiteSpace(Url);
        }
    }
}