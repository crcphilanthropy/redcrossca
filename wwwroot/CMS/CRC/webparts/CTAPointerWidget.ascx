﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CTAPointerWidget.ascx.cs" Inherits="CMSApp.CRC.webparts.CTAPointerWidget" EnableViewState="false" %>
<asp:PlaceHolder runat="server" ID="phBannerTop">
    <section class="promo-banner">
        <script>
            dataLayer.push({
                'event': 'generic-event',
                'eventCategory': 'promo banner',
                'eventAction': 'impression',
                'eventLabel': '<%# IsEmailCTA ? "signup" :  CTADocumentName %>',
                'nonInteractive':true
            });
        </script>
        <div class="row">
</asp:PlaceHolder>
<cms:CMSRepeater runat="server" ID="rptCTA" EnableViewState="False" />


<asp:PlaceHolder runat="server" ID="phBannerBottom">
    <span class="closeBanner eca-form-track" data-event="generic-event" data-category="promo banner" data-action="close" data-label="signup" data-path="<%# Path %>" data-hidepromotionndays="<%# HidePromotionNDays %>" data-showpromotionntimes="<%# ShowPromotionNTimes %>"><i class="fa fa-times"></i></span>
    </div>
</section>
</asp:PlaceHolder>
