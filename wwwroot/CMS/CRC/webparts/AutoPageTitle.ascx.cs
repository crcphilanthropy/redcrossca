using System;
using System.Collections.Generic;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.PortalEngine;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class AutoPageTitle : CMSAbstractEditableWebPart
    {
        #region "Public properties"

        /// <summary>
        /// Gets the url of the page which ensures editing of the web part's editable content in the On-Site editing mode.
        /// </summary>
        public override string EditPageUrl
        {
            get
            {
                return ucEditableText.EditPageUrl;
            }
        }

        public string TitleWidth
        {
            get
            {
                string titleWidth = string.Format("{0}", GetValue("TitleWidth"));
                if (titleWidth.Trim() != "")
                {
                    return titleWidth;

                }
                return "80";
            }
        }

        public string H1Class
        {
            get { return GetStringValue("H1Class", string.Empty); }
            set { SetValue("H1Class", value); }
        }

        public bool ShowCategory
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowCategory"), false); }
            set { SetValue("ShowCategory", value); }
        }

        #endregion


        /// <summary>
        /// Content loaded event handler.
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
        }


        /// <summary>
        /// Initializes the control properties.
        /// </summary>
        protected void SetupControl()
        {
            // Do not hide for roles in edit or preview mode
            switch (PortalContext.ViewMode)
            {
                case ViewModeEnum.Edit:
                case ViewModeEnum.EditLive:
                case ViewModeEnum.EditNotCurrent:
                    ucEditableText.ViewMode = ViewModeEnum.Edit;
                    phStyle.Visible = true;
                    SetValue("DisplayToRoles", String.Empty);
                    break;
                case ViewModeEnum.EditDisabled:
                case ViewModeEnum.Design:
                case ViewModeEnum.DesignDisabled:
                case ViewModeEnum.Preview:
                    SetValue("DisplayToRoles", String.Empty);
                    break;
            }

            ucEditableText.StopProcessing = StopProcessing;

            if (!StopProcessing)
            {
                ucEditableText.ContentID = WebPartID;
                ucEditableText.DataControl = this;
                ucEditableText.PageManager = PageManager;
                ucEditableText.PagePlaceholder = PagePlaceholder;
                crcPageTitle.Attributes.Add("class", H1Class);

                //custom set
                ucEditableText.DefaultText = GetAutoTitleText();
                ucEditableText.DialogHeight = 50;
                ucEditableText.SetupControl();

            }
        }

        private string GetAutoTitleText()
        {
            var documentTitle = CurrentDocument.GetStringValue("Title", String.Empty);
            if (String.IsNullOrWhiteSpace(documentTitle))
                documentTitle = CurrentDocument.GetStringValue("NewsTitle", String.Empty);

            if (String.IsNullOrWhiteSpace(documentTitle))
                documentTitle = CurrentDocument.GetStringValue("BlogPostTitle", String.Empty);

            documentTitle = !String.IsNullOrWhiteSpace(documentTitle) ? documentTitle : CurrentDocument.DocumentName;

            return documentTitle;
        }


        /// <summary>
        /// Overridden CreateChildControls method.
        /// </summary>
        protected override void CreateChildControls()
        {
            SetupControl();
            base.CreateChildControls();
        }


        /// <summary>
        /// Loads the control content.
        /// </summary>
        /// <param name="content">Content to load</param>
        /// <param name="forceReload">If true, the content is forced to reload</param>
        public override void LoadContent(string content, bool forceReload)
        {
            if (!StopProcessing)
            {
                ucEditableText.LoadTitle(content, forceReload);

                if (!string.IsNullOrEmpty(ucEditableText.DefaultText))
                {
                    // Default image defined => content is not empty
                    EmptyContent = false;
                }
            }
        }

        /// <summary>
        /// Returns true if entered data is valid. If data is invalid, it returns false and displays an error message.
        /// </summary>
        public override bool IsValid()
        {
            return ucEditableText.IsValid();
        }


        /// <summary>
        /// Gets the current control content.
        /// </summary>
        public override string GetContent()
        {
            if (!StopProcessing)
            {
                return ucEditableText.GetContent();
            }

            return null;
        }


        /// <summary>
        /// Returns the array list of the field IDs (Client IDs of the inner controls) that should be spell checked.
        /// </summary>
        public override List<string> GetSpellCheckFields()
        {
            return ucEditableText.GetSpellCheckFields();
        }


        /// <summary>
        /// Try initialize control
        /// </summary>
        protected override void OnInit(EventArgs e)
        {

            switch (ViewMode)
            {
                case ViewModeEnum.LiveSite:
                case ViewModeEnum.Preview:
                    var pageTitle = string.IsNullOrWhiteSpace(CurrentDocument.DocumentContent[ID])
                        ? GetAutoTitleText() : CurrentDocument.DocumentContent[ID];
                    DocumentContext.CurrentPageInfo.DocumentPageTitle = pageTitle;

                    break;
            }

            SetupControl();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DisplayCategory();
            DisplayPageNumber();
        }

        private void DisplayCategory()
        {
            var category = CurrentDocument.CurrentCategory();
            litCategory.Visible = ShowCategory && category != null;
            litCategory.Text = string.Format(" : {0}", category.LocalizedDisplayName());
        }

        private void DisplayPageNumber()
        {
            var pageNumber = QueryHelper.GetInteger("page", 0);

            litPageNumber.Visible = false;
            if (pageNumber <= 1) return;
            litPageNumber.Visible = true;
            litPageNumber.Text = string.Format(" ({0} {1})", ResourceStringHelper.GetString("CRC.Page", defaultText: "Page"), pageNumber);
        }
    }
}