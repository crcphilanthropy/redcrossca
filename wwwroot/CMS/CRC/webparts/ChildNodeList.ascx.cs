using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.TreeEngine;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class ChildNodeList : CMSAbstractWebPart
    {
        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("TransformationName"), string.Empty); }
            set { SetValue("TransformationName", value); }
        }

        public int TopN
        {
            get { return ValidationHelper.GetInteger(GetValue("TopN"), 10); }
            set { SetValue("TopN", value); }
        }

        public string ClassNames
        {
            get { return ValidationHelper.GetString(GetValue("ClassNames"), string.Empty); }
            set { SetValue("ClassNames", value); }
        }

        public string OrderBy
        {
            get { return ValidationHelper.GetString(GetValue("OrderBy"), "NodeOrder ASC"); }
            set { SetValue("OrderBy", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitRepeater();
        }

        private void InitRepeater()
        {
            rptChildNodes.TransformationName = TransformationName;
            rptChildNodes.DataSource = TreeHelper.GetDocuments(
                                                    SiteContext.CurrentSiteName,
                                                    string.Format("{0}/%", DocumentContext.CurrentDocument.NodeAliasPath),
                                                    LocalizationContext.PreferredCultureCode,
                                                    false,
                                                    ClassNames,
                                                    string.Format("NodeParentID = {0} and ClassName not in ('CRC.NavHeading','CRC.NavigationSubheading','CMS.Folder')", DocumentContext.CurrentDocument.NodeID),
                                                    OrderBy,
                                                    TreeProvider.ALL_LEVELS,
                                                    true,
                                                    TopN);
            rptChildNodes.DataBind();
        }
    }
}