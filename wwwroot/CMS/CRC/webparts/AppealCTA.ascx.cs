using System;
using CMS.PortalControls;
using CMS.Controls;
using System.Data;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMS.DataEngine;
using CMS.Localization;
using CMSAppAppCode.Old_App_Code.CRC;

public partial class CMSApp_CRC_webparts_AppealCTA : CMSAbstractWebPart
{
    #region "Document properties"

    protected CMSDocumentsDataSource DataSourceControl = null;
    protected string FormUrl = "";
    protected bool InMainContent = false;
    /// <summary>
    /// Button Url
    /// </summary>
    public string LinkTo
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("LinkTo"), null), null);
        }
        set
        {
            SetValue("LinkTo", value);

        }
    }
    public bool HideInactive
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideInactive"), false);
        }
        set
        {
            SetValue("HideInactive", value);

        }
    }
    public bool UseMainContentLayout
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("UseMainContentLayout"), false);
        }
        set
        {
            SetValue("UseMainContentLayout", value);

        }
    }
    public bool PreferCategoryAppeal
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("PreferCategoryAppeal"), false);
        }
        set
        {
            SetValue("PreferCategoryAppeal", value);

        }
    }

    public string TitleOverride
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("TitleOverride"), null), null);
        }
        set
        {
            SetValue("TitleOverride", value);

        }
    }

    public string TitleOverride2
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("TitleOverride2"), null), null);
        }
        set
        {
            SetValue("TitleOverride2", value);

        }
    }

    public string SubtitleOverride
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SubtitleOverride"), null), null);
        }
        set
        {
            SetValue("SubtitleOverride", value);

        }
    }

    public string PricePoint1
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("PricePoint1"), null), null);
        }
        set
        {
            SetValue("PricePoint1", value);

        }
    }

    public string PricePoint2
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("PricePoint2"), null), null);
        }
        set
        {
            SetValue("PricePoint2", value);

        }
    }

    public string PricePoint3
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("PricePoint3"), null), null);
        }
        set
        {
            SetValue("PricePoint3", value);

        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (Visible)
        {
            CMSApp.CRC.TransformationHelper.RegisterStartUpScript("$(document).ready(function () { initAppealTracking(); });", Page, "initAppealTracking");
        }

    }

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing) return;

        InitTranslations();

        var showAppeal = true;

        string mGuid = LinkTo;
        InMainContent = ParentZone.ID == "ContentZone" | ParentZone.ID == "CTAZone" | UseMainContentLayout;
        // Check if the control is in the main content webpart zone. 
        if (InMainContent)
        {

            if (DocumentContext.CurrentDocument.NodeClassName == "CRC.Appeal")
            {
                mGuid = DocumentContext.CurrentDocument.NodeGUID.ToString();
            }

        }


        // 
        var node = GetDocumentByGuid(mGuid);

        if (PreferCategoryAppeal)
        {
            var relevantAppeal = GetRelevantAppeal();

            if (relevantAppeal != null)
            {

                node = relevantAppeal;
            }
        }

        if (node != null)
        {


            var isActive = ValidationHelper.GetBoolean(node["IsActive"], false);

            // if the control has hide inactive checked and the appeal document is not active, hide the appeal
            if (HideInactive & !isActive)
            {
                showAppeal = false;
            }
            if (showAppeal)
            {

                var title = ValidationHelper.GetString(node["Name"], "");
                var defaultMonthly = ValidationHelper.GetBoolean(node["SetDefaultMonthlyDonation"], false);
                var defaultSingle = ValidationHelper.GetBoolean(node["SetDefaultSingleDonation"], false);
                var subtitle = ValidationHelper.GetString(node["Subtitle"], "");
                FormUrl = ValidationHelper.GetString(node["FormUrl"], "").Trim();
                var imagePath = ValidationHelper.GetString(node["Image"], "");
                var rightRailTitle = ValidationHelper.GetString(node["RightRailTitle"], string.Empty);

                if (defaultMonthly)
                {
                    RadioMonthlyDonation.Checked = true;
                    DonationTypeRadioList.Attributes["class"] = "unstyled displayNone";
                    RadioAmount1.Text = GetPricePoint("PricePoint1", ResHelper.GetString("CRC.MonthlyDonationAmount1"));
                    RadioAmount2.Text = GetPricePoint("PricePoint2", ResHelper.GetString("CRC.MonthlyDonationAmount2"));
                    RadioAmount3.Text = GetPricePoint("PricePoint3", ResHelper.GetString("CRC.MonthlyDonationAmount3"));
                    if (InMainContent)
                    {
                        plcMonthlyDonationTitle.Visible = true;
                    }
                }
                if (defaultSingle)
                {
                    RadioSingleDonation.Checked = true;
                    DonationTypeRadioList.Attributes["class"] = "unstyled displayNone";
                    RadioAmount1.Text = GetPricePoint("PricePoint1", ResHelper.GetString("CRC.DonationAmount1"));
                    RadioAmount2.Text = GetPricePoint("PricePoint2", ResHelper.GetString("CRC.DonationAmount2"));
                    RadioAmount3.Text = GetPricePoint("PricePoint3", ResHelper.GetString("CRC.DonationAmount3"));
                    if (InMainContent)
                    {
                        plcSingleDonationTitle.Visible = true;
                    }
                }

                if (!InMainContent)
                {
                    H3Title.Visible = true;

                }
                else if (ParentZone.ID != "ContentZone")
                {
                    H2Title.Visible = true;
                }
                H2Title.InnerText = title;
                H3Title.InnerText = title;
                if (subtitle != "")
                {
                    H4Subtitle.Visible = true;
                    H4Subtitle.InnerText = subtitle;
                }


                if (InMainContent)
                {
                    Image.Visible = true;
                    Image.ImageUrl = imagePath;
                    Image.AlternateText = title;
                    DonationsCTAContainer.Attributes["class"] = "donationCTAContainer mainContent";
                }

                if (!String.IsNullOrWhiteSpace(ValidationHelper.GetString(TitleOverride, string.Empty)))
                {
                    H2Title.InnerText = rightRailTitle = TitleOverride;
                }

                if (!String.IsNullOrWhiteSpace(rightRailTitle))
                {
                    H3Title.InnerText = rightRailTitle;
                }

                if (!String.IsNullOrWhiteSpace(ValidationHelper.GetString(SubtitleOverride, string.Empty)))
                {
                    H4Subtitle.Visible = true;
                    H4Subtitle.InnerText = SubtitleOverride;
                }

            }
            else
            {
                DonationsCTAContainer.Visible = false;
            }
        }
        else
        {
            DonationsCTAContainer.Visible = false;
        }
    }

    private string GetPricePoint(string fieldName, string defaultValue)
    {
        var toReturn = GetStringValue(fieldName, defaultValue);
        return string.IsNullOrWhiteSpace(toReturn) || ValidationHelper.GetDouble(toReturn, 0) <= 0 ? defaultValue : ValidationHelper.GetDouble(toReturn, 0).ToString("C0");
    }

    private void InitTranslations()
    {
        lblErrorMessageDonationType.Text = ResHelper.GetString("CRC.AppealWebpart.DonationType.Error");
        lblErrorMessageEmpty.Text = ResHelper.GetString("CRC.AppealWebpart.AmountTextBoxInvalid.Error");
        RadioSingleDonation.Text = ResHelper.GetString("CRC.AppealWebpart.SingleDonation");
        RadioMonthlyDonation.Text = ResHelper.GetString("CRC.AppealWebpart.MonthlyDonation");
        RadioAmount4.Text = ResHelper.GetString("CRC.Other");
        BtnDonation.Text = ResHelper.GetString("CRC.AppealWebpart.DonateNow");
        h3MonthlyTitle.InnerText = ResHelper.GetString("CRC.AppealMonthlyDonateTitle");
        h3SingleTitle.InnerText = ResHelper.GetString("CRC.AppealSingleDonateTitle");
        RadioAmount1.Text = GetPricePoint("PricePoint1", ResHelper.GetString("CRC.DonationAmount1"));
        RadioAmount2.Text = GetPricePoint("PricePoint2", ResHelper.GetString("CRC.DonationAmount2"));
        RadioAmount3.Text = GetPricePoint("PricePoint3", ResHelper.GetString("CRC.DonationAmount3"));
        SingleRadioAmount1.Text = ResHelper.GetString("CRC.DonationAmount1");
        SingleRadioAmount2.Text = ResHelper.GetString("CRC.DonationAmount2");
        SingleRadioAmount3.Text = ResHelper.GetString("CRC.DonationAmount3");
        MonthlyRadioAmount1.Text = ResHelper.GetString("CRC.MonthlyDonationAmount1");
        MonthlyRadioAmount2.Text = ResHelper.GetString("CRC.MonthlyDonationAmount2");
        MonthlyRadioAmount3.Text = ResHelper.GetString("CRC.MonthlyDonationAmount3");

        if (DocumentContext.CurrentDocumentCulture.CultureCode == "fr-CA")
        {
            dollarSignFR.Visible = true;
        }
        else
        {
            dollarSignEn.Visible = true;
        }
    }

    protected string GetSelectedDonationType()
    {
        string donationType = "";
        if (RadioSingleDonation.Checked)
        {
            donationType = "single";
        }
        else if (RadioMonthlyDonation.Checked)
        {
            donationType = "monthly";
        }
        return donationType;
    }
    protected double GetSelectedDonationAmount()
    {
        double amount = 0.0;
        if (RadioAmount1.Checked)
        {
            amount = CMSApp.CRC.TransformationHelper.ConvertDonationValueToDouble(RadioAmount1.Text);
        }
        else if (RadioAmount2.Checked)
        {
            amount = CMSApp.CRC.TransformationHelper.ConvertDonationValueToDouble(RadioAmount2.Text);
        }
        else if (RadioAmount3.Checked)
        {
            amount = CMSApp.CRC.TransformationHelper.ConvertDonationValueToDouble(RadioAmount3.Text);
        }
        else if (RadioAmount4.Checked)
        {
            amount = CMSApp.CRC.TransformationHelper.ConvertDonationValueToDouble(TxtOtherAmount.Text);
        }
        return amount;
    }


    protected DataRow GetDocumentByGuid(string guid)
    {
        return CacheHelper.Cache(() =>
            {
                DataRow returnedDocument = null;

                var queryParam = new QueryDataParameters
                    {
                        {"@NodeGUID", ValidationHelper.GetGuid(guid, Guid.Empty)},
                        {"@LANG", DocumentContext.CurrentDocumentCulture.CultureCode}
                    };
                var ds = ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByGUID", queryParam, null, null);
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    returnedDocument = ds.Tables[0].Rows[0];
                }

                return returnedDocument;
            }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("CRC.Appeal.SelectByGUID_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, guid)));

    }

    /// <summary>
    /// The following function finds the top first appeal document who's category matches the current document's category.  
    /// </summary>
    /// <returns></returns>
    protected DataRow GetRelevantAppeal()
    {

        DataRow displayedAppeal = null;

        const int regionID = 5;
        const int topicID = 7;
        const int eventID = 59;
        var topicMatch = false;
        var eventMatch = false;
        var provinceCode = Request.QueryString["province"];
        DataSet releventAppealDocs;

        var categoryQueryParamRegion = new QueryDataParameters { { "@Lang", DocumentContext.CurrentDocumentCulture.CultureCode } };

        if (string.IsNullOrWhiteSpace(provinceCode))
        {
            releventAppealDocs = CacheHelper.Cache(() =>
                {
                    categoryQueryParamRegion.Add("@DocumentID", DocumentContext.CurrentDocument.DocumentID);
                    return ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByCurrentDocumentCategory",
                                                         categoryQueryParamRegion, null, null);
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("CRC.Appeal.SelectByCurrentDocumentCategory_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, DocumentContext.CurrentDocument.DocumentID)));
        }
        else
        {
            releventAppealDocs = CacheHelper.Cache(() =>
                {
                    categoryQueryParamRegion.Add("@CategoryName", provinceCode);
                    return ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByProvince", categoryQueryParamRegion, null, null);
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("CRC.Appeal.SelectByProvince_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, provinceCode)));
        }

        if (!DataHelper.DataSourceIsEmpty(releventAppealDocs))
        {
            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {

                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == eventID)
                {
                    displayedAppeal = appeal;
                    eventMatch = true;
                    break;
                }
            }

            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {

                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == topicID)
                {
                    if (eventMatch && ValidationHelper.GetInteger(displayedAppeal["DocumentID"], 0) == ValidationHelper.GetInteger(appeal["DocumentID"], 0))
                    {
                        displayedAppeal = appeal;
                        topicMatch = true;
                        break;
                    }
                }
                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && eventMatch == false)
                {
                    displayedAppeal = appeal;
                    break;
                }
            }
            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {
                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == regionID)
                {

                    if (topicMatch && ValidationHelper.GetInteger(displayedAppeal["DocumentID"], 0) == ValidationHelper.GetInteger(appeal["DocumentID"], 0))
                    {
                        displayedAppeal = appeal;
                        break;
                    }
                    if (topicMatch == false & eventMatch == false)
                    {
                        displayedAppeal = appeal;
                        break;
                    }
                }
            }
        }
        return displayedAppeal;
    }

    /// <summary>
    /// Event risen when the source filter has changed
    /// </summary>
    protected void FilterControl_OnFilterChanged()
    {
        // Override previously set visibility. Control's visibility is managed in the PreRender event.
        Visible = true;
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();

    }


    /// <summary>
    /// Clears cache.
    /// </summary>
    public override void ClearCache()
    {

    }

    protected void BtnDonationClick(object sender, EventArgs e)
    {
        var amount = GetSelectedDonationAmount();
        var donationType = GetSelectedDonationType();

        var url = URLHelper.AddParameterToUrl(FormUrl, "type", donationType);
        url = URLHelper.AddParameterToUrl(url, "amount", amount.ToString().Replace(',', '.'));        // amount.ToString(CultureInfo.InvariantCulture)
        url = URLHelper.AddParameterToUrl(url, "langpref", LocalizationContext.PreferredCultureCode);
        url = URLHelper.AddParameterToUrl(url, "_ga", (hidDonateBarGAHash.Value ?? string.Empty).ToLower().Replace("_ga=", string.Empty));

        Response.Redirect(url, true);

    }
}

