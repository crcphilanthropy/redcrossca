﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNavLinks.ascx.cs" Inherits="CMSApp.CRC.webparts.TopNavLinks" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:Repeater runat="server" ID="rptLink">
    <HeaderTemplate>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li class="<%# Eval("DocumentMenuClass") %>">
            <a href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath")) %>">
                <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
            </a>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
