using System;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.Localization;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class BlogPosts : CMSAbstractWebPart
    {
        public string RssFeedUrl
        {
            get
            {
                var toReturn = ValidationHelper.GetString(GetValue("RssFeedUrl"), string.Empty);
                if (string.IsNullOrWhiteSpace(toReturn))
                    toReturn = ResHelper.GetString("CRC.BlogFeed");

                return toReturn;

            }
            set { SetValue("RssFeedUrl", value); }
        }

        public string Title
        {
            get
            {
                var toReturn = ValidationHelper.GetString(GetValue("Title"), string.Empty);
                if (string.IsNullOrWhiteSpace(toReturn))
                    toReturn = ResHelper.GetString("CRC.BlogPostTitle");

                return toReturn;
            }
            set { SetValue("Title", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            var cacheManager = new CacheManager { Enabled = false, TimeInMinutes = 60 };
            var feedCacheKey = string.Format("{0}_Blog", LocalizationContext.PreferredCultureCode);
            cacheManager.OnRetriving += cacheManager_OnRetriving;
            var feed = cacheManager.GetObject<SyndicationFeed>(feedCacheKey);

            rptItems.DataSource = feed.Items.OrderByDescending(item => item.PublishDate).Take(5);
            DataBind();
        }

        object cacheManager_OnRetriving(object sender, EventArgs args)
        {
            var reader = XmlReader.Create(RssFeedUrl);
            var feed = SyndicationFeed.Load(reader);
            reader.Close();
            return feed;
        }

        public string GetItemUrl(object dataItem)
        {
            var item = dataItem as SyndicationItem;
            if (item == null || !item.Links.Any()) return string.Empty;

            return item.Links.First().Uri.ToString();
        }
    }
}