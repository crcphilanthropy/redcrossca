using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.WebParts
{
    public partial class FlickrGallery : CMSAbstractWebPart
    {
        public string PhotoSetID
        {
            get { return ValidationHelper.GetString(GetValue("PhotoSetID"), string.Empty); }
            set { SetValue("PhotoSetID", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                rptFlickr.DataSource = Flickr.Instance.PhotosetsGetPhotos(PhotoSetID);
                rptFlickr.DataBind();
            }
            catch (Exception)
            {
            }
        }
    }
}