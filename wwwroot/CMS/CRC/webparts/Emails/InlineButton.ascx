<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InlineButton.ascx.cs" Inherits="InlineButton" %>

<asp:PlaceHolder ID="PlaceHolder1" runat="server">
<table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" align="<%: GetValue("ButtonAlignment") %>"  style="border-collapse: separate !important; border-top-left-radius: 0px;border-top-right-radius: 0px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: <%: GetValue("ButtonColour") %>;">
		<tbody>
 		<tr>
 			<td align="center" class="mcnButtonContent" style="font-family: Arial;font-size: 16px;padding: 10px 10px 10px 10px; position: relative;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle">
                <a class="mcnButton <%: GetValue("ButtonColour") %>" href="<%: WidgetLinkIsExternal ? String.Empty : AbsoluteUrl %><%: Page.ResolveUrl(GetValue("ButtonLink").ToString()) %>" style="font-weight: bold; display:block;letter-spacing: normal; line-height:100%; text-transform:uppercase; text-align: center; background-color: <%: GetValue("ButtonColour") %>; text-decoration: none;color: #FFFFFF;word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank" title=" <%: GetValue("ButtonText") %>">
				    <%: GetValue("ButtonText") %>  <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-arrow.png"  style="padding-left:8px;"/>
 				</a>
 			</td>
 		</tr>

	</tbody>
</table>
</asp:PlaceHolder>

