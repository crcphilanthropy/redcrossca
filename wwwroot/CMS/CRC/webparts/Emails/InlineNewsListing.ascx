<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InlineNewsListing.ascx.cs" Inherits="CMSApp.CRC.webparts.Emails.InlineNewsListing" %>

<table cellspacing="0" cellpadding="0" width="100%" border="0" style="margin-top: 20px;">
            <tbody>
                <tr>
                    <td valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="news-list">
                            <tbody>
                            <asp:Repeater runat="server" ID="rptNewsListing" OnItemDataBound="rptEmail_ItemDataBound" >
                                <ItemTemplate>
         
                                <tr>
                                    <asp:PlaceHolder runat="server" ID="plcThumbs" Visible="true">
                                        <td valign="top" align="center" style="padding-right:10px;" class="mobile-12">
                                            <img class="lowerContentImage" style="max-width:150px;" src='<asp:Literal ID="ltrThumb" runat="server"></asp:Literal>'>
                                            <br>
                                        </td>
                                    </asp:PlaceHolder>
                                    <td valign="top" class="mobile-12">
                                                                        
                                        <div class="lowerContentCopy">
                                            <h3>
                                                <a target="_blank" style="text-decoration:none;" href="<%= AbsoluteUrl %><%# Eval("Link") %>"><span style="color:#ed2024"><%# Eval("Title") %></span>&nbsp;<img src="<%= AbsoluteUrl %>/crc/img/emails/icon-red-arrow.png" /></a>
                                            </h3> 
                                            <p style="margin: .5em 0 1em 0;"><%# Eval("Teaser") %></p>
                                        </div>
                                    </td>
                               </tr>
                                <asp:PlaceHolder ID="plcDots" runat="server">
                                    <tr>
                                        <td colspan="3" style="padding-top:10px; padding-bottom:20px;">
                                            <img class="hide-last" src="<%= AbsoluteUrl %>/CRC/img/Emails/dots.gif" style="width:100%; max-width: 100%;" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                               </ItemTemplate>
                            </asp:Repeater>
                         </tbody>
                        </table>
                    </td>
                </tr>                                           
            </tbody>
        </table>