using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using System.Web;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.Membership;

namespace CMSApp.CRC.webparts.Emails
{
    public partial class EmailLayoutGenerator : CMSAbstractWebPart
    {

        public string Button1Color
        {
            get
            {
                var btnColor = ValidationHelper.GetString(CurrentDocument.GetValue("Button1Color"), string.Empty);
                return btnColor == "Red" ? "#ed2024" : "#027db6";
            }
            set { SetValue("Button1Color", value); }
        }

        public string Button2Color
        {
            get
            {
                var btnColor = ValidationHelper.GetString(CurrentDocument.GetValue("Button2Color"), string.Empty);
                return btnColor == "Red" ? "#ed2024" : "#027db6";
            }
            set { SetValue("Button2Color", value); }
        }

        public string FacebookURL
        {
            get
            {
                var facebookURL = ValidationHelper.GetString(CurrentDocument.GetValue("FacebookURL"), ResHelper.GetString("CRC.Social.FacebookURL"));
                return facebookURL;
            }
            set { SetValue("FacebookURL", value); }
        }

        public string TwitterURL
        {
            get
            {
                var twitterURL = ValidationHelper.GetString(CurrentDocument.GetValue("TwitterURL"), ResHelper.GetString("CRC.Social.TwitterURL"));
                return twitterURL;
            }
            set { SetValue("TwitterURL", value); }
        }

        public string InstagramURL
        {
            get
            {
                var instagramURL = ValidationHelper.GetString(CurrentDocument.GetValue("InstagramURL"), ResHelper.GetString("CRC.Social.InstagramURL"));
                return instagramURL;
            }
            set { SetValue("InstagramURL", value); }
        }

        public string LogoURLTrackingCode
        {
            get
            {
                var logoURLTrackingCode = ValidationHelper.GetString(CurrentDocument.GetValue("LogoURLTrackingCode"), String.Empty);
                return logoURLTrackingCode;
            }
            set { SetValue("LogoURLTrackingCode", value); }
        }

        public bool ShouldRenderContent(string docTypeField)
        {
            var doctypeValue = ValidationHelper.GetString(CurrentDocument.GetValue(docTypeField), string.Empty);
            return !String.IsNullOrWhiteSpace(doctypeValue) ? true : false;
        }

        public string ShouldRenderOnMobile(string docTypeField)
        {
            var mobileVisibility = ValidationHelper.GetBoolean(CurrentDocument.GetValue(docTypeField), false);
            return mobileVisibility ? "" : "hide-on-mobile";
        }

        public string ModTopHeader { get; set; }

        public string ColumnWidth
        {
            get
            {
                var rightRailImage1 = ValidationHelper.GetString(CurrentDocument.GetValue("RightRail1Image"), string.Empty);
                return !String.IsNullOrWhiteSpace(rightRailImage1) || !String.IsNullOrEmpty(rightRailImage1) ? "390" : "600";
            }
        }

        /// <summary>
        /// Will be set by code below that parses out the LogoSelector field value. 
        /// Will be set to true if the custom table ItemID for the logo cannot be parsed out of the value, in this case we assume the old behaviour of the value being the actual logo URL string will apply.
        /// </summary>
        public bool FallbackToLogoSelectorUrlString { get; set; }

        /// <summary>
        /// The actual value stored in the document type for the LogoSelector field: this is either the logo's URL string 
        /// (previous behaviour, associated with old records), or else the logo's custom table ItemID (the new behaviour).
        /// </summary>
        object logoSelectorActualValue = null;

        /// <summary>
        /// A reference to the custom table record hosting the logo. This will remain null if <see cref="FallbackToLogoSelectorUrlString"/> is true; 
        /// </summary>
        CMS.CustomTables.CustomTableItem logoItem = null;

        /// <summary>
        /// ID of the custom table record hosting the logo data.
        /// </summary>
        public int Logo_ItemID { get { return FallbackToLogoSelectorUrlString ? default(int) : ValidationHelper.GetInteger(logoItem["MediaSelection"], -1); } }

        /// <summary>
        /// The URL of the logo image. 
        /// </summary>
        public string MediaSelection
        {
            get
            {
                string urlvalue = FallbackToLogoSelectorUrlString
                    ? logoSelectorActualValue.ToString()
                    : ValidationHelper.GetString(logoItem["MediaSelection"], string.Empty);

                return AbsoluteUrl + (urlvalue ?? string.Empty).TrimStart('~');
            }
        }

        /// <summary>
        /// Becomes an alias for the <see cref="MediaSelection"/> property in order to maintain backward compatibility for any associated .ascx declarative files that use this property. 
        /// </summary>
        public string LogoSelector { get { return this.MediaSelection; } }

        /// <summary>
        /// HTML alt text for the logo image.
        /// </summary>
        public string LogoAltText { get { return FallbackToLogoSelectorUrlString ? string.Empty : ValidationHelper.GetString(logoItem["LogoAltText"], string.Empty); } }

        /// <summary>
        /// URL the logo links to.
        /// </summary>
        public string LogoUrl { get { return FallbackToLogoSelectorUrlString ? string.Empty : ValidationHelper.GetString(logoItem["LogoUrl"], string.Empty); } }

        public string HeaderImage { get; set; }
        public string RightRail1Image { get; set; }
        public string RightRail2Image { get; set; }
        public string FDRightImage { get; set; }

        public bool Button1LinkIsExternal
        {
            get { return ValidationHelper.GetBoolean(CurrentDocument.GetValue("Button1LinkIsExternal"), false); }
        }
        public bool Button2LinkIsExternal
        {
            get { return ValidationHelper.GetBoolean(CurrentDocument.GetValue("Button2LinkIsExternal"), false); }
        }
        public bool RightRail1LinkIsExternal
        {
            get { return ValidationHelper.GetBoolean(CurrentDocument.GetValue("RightRail1LinkIsExternal"), false); }
        }
        public bool RightRail2LinkIsExternal
        {
            get { return ValidationHelper.GetBoolean(CurrentDocument.GetValue("RightRail2LinkIsExternal"), false); }
        }
        public bool HeaderLinkIsExternal
        {
            get { return ValidationHelper.GetBoolean(CurrentDocument.GetValue("HeaderLinkIsExternal"), false); }
        }
        public bool IsDm
        {
            get
            {
                var currentDocumentType = DocumentContext.CurrentDocument.ClassName;
                return currentDocumentType == "CRC.EmailDMTwoColumnWithHeaderImage";
            }
        }

        public bool IsLast { get; set; }

        public string BodyText
        {
            get
            {
                return MacroContext.CurrentResolver.ResolveMacros(ValidationHelper.GetString(CurrentDocument.GetValue("Body"), string.Empty));

            }
        }


        public string AbsoluteUrl
        {
            get { return "http://" + HttpContext.Current.Request.Url.Authority.ToLower(); } // CMSContext.CurrentSite.DomainName; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var currentDocumentType = DocumentContext.CurrentDocument.ClassName;
            var data = TreeHelper.SelectNodes(string.Format("{0}", DocumentContext.CurrentDocument.NodeAliasPath), false,
                                              currentDocumentType, string.Empty, "", -1, true);

            var topHeadStringBuilder = new StringBuilder(ValidationHelper.GetString(CurrentDocument.GetValue("TopHeader"), string.Empty));
            var currentPage = "http://" + HttpContext.Current.Request.Url.Authority.ToLower() +
                              DocumentContext.CurrentDocument.DocumentUrlPath;

            string alternativeLink = GetAltLanguageUrl();  // "http://" + TransformationHelper.GetAlternateLanguageDomain() + CMSContext.CurrentDocument.DocumentUrlPath;
            ModTopHeader = topHeadStringBuilder
                 .Replace("[pageLink]", currentPage)
                 .Replace("[frenchURL]", alternativeLink)
                 .Replace("[englishURL]", alternativeLink)
                 .ToString();

            rptEmail.DataSource = data;
            rptEmail.DataSource = data;
            rptEmail.ItemDataBound += rptEmail_ItemDataBound;
            DataBind();

        }

        private void rptEmail_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item)
            {
                var placeHolder = e.Item.FindControl("plcBody");
                var literal = e.Item.FindControl("ltrBody") as Literal;
                if (literal != null) literal.Text = BodyText;
                CMS.ExtendedControls.ControlsHelper.ResolveDynamicControls(placeHolder);
            }

            // expect the logo selecto value to be the ItemID of the custom table record hosting the logo data. 
            logoSelectorActualValue = ((DataRowView)(e.Item.DataItem)).DataView[0]["LogoSelector"];


            // Look up the custom table item having the parsed ID
            logoItem = CustomTableHelper.GetItem("CRC.EmailLogos", ValidationHelper.GetInteger(logoSelectorActualValue, -1));
            if (logoItem == null)
                FallbackToLogoSelectorUrlString = true;

            HeaderImage = AbsoluteUrl +
                        ((DataRowView)(e.Item.DataItem)).DataView[0]["HeaderImage"].ToString().TrimStart('~');
            RightRail1Image = AbsoluteUrl +
                        ((DataRowView)(e.Item.DataItem)).DataView[0]["RightRail1Image"].ToString().TrimStart('~');
            RightRail2Image = AbsoluteUrl +
                       ((DataRowView)(e.Item.DataItem)).DataView[0]["RightRail2Image"].ToString().TrimStart('~');
            FDRightImage = AbsoluteUrl +
                       ((DataRowView)(e.Item.DataItem)).DataView[0]["FDRightImage"].ToString().TrimStart('~');
        }

        private string GetAltLanguageUrl()
        {
            string alternativeLink = "http://" + TransformationHelper.GetAlternateLanguageDomain();
            // get current Node, and then node of alt. lang if exists
            string lang = ResHelper.GetString("CRC.Master.TranslateCultureCode");
            TreeProvider treeProvider = new TreeProvider(MembershipContext.AuthenticatedUser);
            CMS.DocumentEngine.TreeNode currNode = treeProvider.SelectSingleNode(DocumentContext.CurrentDocument.NodeID, lang);
            if (currNode != null)
            {
                alternativeLink = alternativeLink + currNode.DocumentUrlPath;
            }
            return alternativeLink;
        }

    }
}


