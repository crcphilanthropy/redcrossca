using System;
using System.Data;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using FlickrNet;
using System.Web;
using CMS.Helpers;

namespace CMSApp.CRC.webparts.Emails
{
    public partial class InlineNewsListing : CMSAbstractWebPart
    {
        public string AbsoluteUrl
        {
            get { return "http://" + HttpContext.Current.Request.Url.Authority.ToLower(); }
        }

        public string GetAbsolutePath(string docTypeField)
        {
            var doctypeValue = ValidationHelper.GetString(CurrentDocument.GetValue(docTypeField), string.Empty);
            return AbsoluteUrl + doctypeValue;
        }

        public string Thumbnail { get; set; }
        public bool ShowThumbs { get; set; }
        public int CountItems { get; set; }

        public string PathToFolder
        {
            get { return ValidationHelper.GetString(GetValue("PathToFolder"), ""); }
            set { SetValue("PathToFolder", value); }
        }

        public bool IsLast { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CMS.DocumentEngine.TreeNodeDataSet data = TreeHelper.SelectNodes(string.Format("{0}", PathToFolder), false,
                "CRC.EmailTeaserItem", string.Empty, "NodeLevel, NodeOrder, NodeName", -1, true);

            ShowThumbs = true;
            foreach (CMS.DocumentEngine.TreeNode treeNode in data)
            {
                var hasThumb = treeNode.GetValue("Thumbnail");

                if (hasThumb == null || hasThumb == "")
                {
                    ShowThumbs = false;
                }
            }

            // set the count, so that when iterate through items, can hide last placeholder for spacing
            if (data != null)
            {
                CountItems = data.Items.Count;
            }

            rptNewsListing.DataSource = data;
            rptNewsListing.DataSource = data;
            DataBind();

        }

        private void Page_Load(Object Sender, EventArgs e)
        {
           
        }

        public void rptEmail_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            
            string thumbnail = ((DataRowView) (e.Item.DataItem)).DataView[e.Item.ItemIndex]["Thumbnail"].ToString().TrimStart('~');
            string thumbnailAbsolutePath = AbsoluteUrl + thumbnail;

            Literal ltrThumb = (Literal) e.Item.FindControl("ltrThumb");
            
            if (ltrThumb != null)
            {
                ltrThumb.Text = thumbnailAbsolutePath;
            }

            PlaceHolder plcThumbs = (PlaceHolder)e.Item.FindControl("plcThumbs");
            if (plcThumbs != null)
            {
                plcThumbs.Visible = ShowThumbs;
            }

            // if last item, hide padding
            if (e.Item.ItemIndex + 1 == CountItems)
            {
                PlaceHolder plcPadding = (PlaceHolder)e.Item.FindControl("plcDots");
                if (plcPadding != null)
                {
                    plcPadding.Visible = false;
                }
            }

        }

       

    }

}