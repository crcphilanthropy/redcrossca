<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.webparts.Emails.EmailLayoutGenerator" CodeBehind="EmailLayoutGenerator.ascx.cs"%>


<asp:Repeater runat="server" ID="rptEmail">
    <ItemTemplate>
        
        <!-- COPY MARKUP STARTING HERE -->

        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
      
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title>*|MC:SUBJECT|*</title>


        <style type="text/css">
        body, #bodyTable, #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        table {
            border-collapse: collapse;
        }

        img, a img {
            border: 0;
            outline: none;
            text-decoration: none;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
        }

        p {
            margin: 1em 0;
            padding: 0;
        }

        a {
            word-wrap: break-word;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        #outlook a {
            padding: 0;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        body, table, td, p, a, li, blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        #bodyCell {
            padding: 20px;
        }

        .mcnImage {
            vertical-align: bottom;
        }

        .mcnTextContent img {
            height: auto !important;
        }

        body, #bodyTable {

        }

        #bodyCell {
            border-top: 0;
        }

        #templateContainer {
            border: 0;
        }

        h1 {
            color: #606060 !important;
            display: block;
            font-family: Helvetica;
            font-size: 40px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: -1px;
            margin: 0;
            text-align: left;
        }
        h2 {
            color: #404040 !important;
            display: block;
            font-family: Helvetica;
            font-size: 26px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: -.75px;
            margin: 0;
            text-align: left;
        }

        h3 {
            color: #606060 !important;
            display: block;
            font-family: Helvetica;
            font-size: 18px;
            font-style: normal;
            font-weight: bold;
            letter-spacing: -.5px;
            margin: 0;
            text-align: left;
        }

        h4 {
            color: #808080 !important;
            display: block;
            font-family: Helvetica;
            font-size: 16px;
            font-style: normal;
            font-weight: bold;
            line-height: 125%;
            letter-spacing: normal;
            margin: 0;
            text-align: left;
        }

        #templatePreheader {
            border-top: 0;
            border-bottom: 0;
        }

        .preheaderContainer .mcnTextContent, .preheaderContainer .mcnTextContent p {
            color: #333;
            font-family: Helvetica;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
        }

        .preheaderContainer .mcnTextContent a {
            color: #0270a4;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateHeader {
            background-color: #FFFFFF;
            border-top: 0;
            border-bottom: 0;
        }

        .headerContainer .mcnTextContent, .headerContainer .mcnTextContent p {
            color: #606060;
            font-family: Helvetica;
            font-size: 15px;
            line-height: 150%;
            text-align: left;
        }

        .headerContainer .mcnTextContent a {
            color: #0270a4;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateBody {
            background-color: #FFFFFF;
            border-top: 0;
            border-bottom: 0;
        }

        #templateSidebar {
            border-left: 0;
        }

        .bodyContainer .mcnTextContent, .bodyContainer .mcnTextContent p {
            color: #333;
            font-family:Arial, Helvetica;
            font-size: 14px;
            line-height: 150%;
            text-align: left;
        }

        .bodyContainer img {
            max-width: 100%;
            width: auto;
        }

        .bodyContainer .mcnTextContent a {
            color: #0270a4;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateSidebar {
            background-color: #FFFFFF;
        }

        #templateSidebarInner {
            border-left: 0;
        }

        .sidebarContainer .mcnTextContent, .sidebarContainer .mcnTextContent p {
            color: #606060;
            font-family: Helvetica;
            font-size: 13px;
            line-height: 125%;
            text-align: left;
        }


        .sidebarContainer .mcnTextContent a {
            color: #6DC6DD;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateFooter {
            background-color: #ebece9;
            border-top: 1px solid #ebece9;
            border-bottom: 0;
        }

        .footerContainer .mcnTextContent, .footerContainer .mcnTextContent p {
            color: #333;
            font-family: Helvetica;
            font-size: 12px;
            line-height: 125%;
            text-align: center;
        }

        .footerContainer .mcnTextContent a {
            color: #333;
            font-weight: normal;
            text-decoration: underline;
        }

        hr.hide-last:last-child {
            display: none;
        }

        table.news-list tr:last-child img.hide-last{
            display: none;
        }

        /***************** eca ************************/

        @media only screen and (max-width: 480px) {
            .mobile-12{
                width: 90% !important;
                display:block !important;
            }
        }


        /***************** eca ************************/



        @media only screen and (max-width: 480px) {
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
            }

        }
        @media only screen and (max-width: 480px) {
            .hide-on-mobile{
                display: none !important;
            }
        }
        @media only screen and (max-width: 480px) {
            body {
                width: 100% !important;
                min-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[id=bodyCell] {
                padding: 10px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnTextContentContainer] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnBoxedTextContentContainer] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcpreview-image-uploader] {
                width: 100% !important;
                display: none !important;
            }

        }

        @media only screen and (max-width: 480px) {
            img[class=mcnImage] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnImageGroupContentContainer] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnImageGroupContent] {
                padding: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnImageGroupBlockInner] {
                padding-bottom: 0 !important;
                padding-top: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            tbody[class=mcnImageGroupBlockOuter] {
                padding-bottom: 9px !important;
                padding-top: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnCaptionTopContent], table[class=mcnCaptionBottomContent] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnCaptionLeftTextContentContainer], table[class=mcnCaptionRightTextContentContainer], table[class=mcnCaptionLeftImageContentContainer], table[class=mcnCaptionRightImageContentContainer], table[class=mcnImageCardLeftTextContentContainer], table[class=mcnImageCardRightTextContentContainer] {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnImageCardLeftImageContent], td[class=mcnImageCardRightImageContent] {
                padding-right: 18px !important;
                padding-left: 18px !important;
                padding-bottom: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnImageCardBottomImageContent] {
                padding-bottom: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnImageCardTopImageContent] {
                padding-top: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent], table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent] {
                padding-top: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent] {
                padding-top: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnBoxedTextContentColumn] {
                padding-left: 18px !important;
                padding-right: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=sectionContainer] {
                display: block !important;
                max-width: 600px !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=mcnTextContent] {
                padding-right: 18px !important;
                padding-left: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {

            table[id=templateContainer], table[id=templatePreheader], table[id=templateHeader], table[id=templateBody], table[id=templateBodyInner], table[id=templateSidebar], table[id=templateSidebarInner], table[id=templateFooter] {
            max-width: 600px !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            h1 {
                font-size: 24px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            h2 {
                font-size: 20px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            h3 {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            h4 {
                font-size: 16px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent], td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[id=templatePreheader] {
                display: block !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=preheaderContainer] td[class=mcnTextContent], td[class=preheaderContainer] td[class=mcnTextContent] p {
                font-size: 14px !important;
                line-height: 115% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=headerContainer] td[class=mcnTextContent], td[class=headerContainer] td[class=mcnTextContent] p {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=bodyContainer] td[class=mcnTextContent], td[class=bodyContainer] td[class=mcnTextContent] p {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=sidebarContainer] td[class=mcnTextContent], td[class=sidebarContainer] td[class=mcnTextContent] p {
                font-size: 18px !important;
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[id=templateSidebar] {
                border-left: 0 !important;
                border-right: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            table[id=templateSidebarInner] {
                border-left: 0 !important;
                border-right: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=footerContainer] td[class=mcnTextContent], td[class=footerContainer] td[class=mcnTextContent] p {
                font-size: 14px !important;
                line-height: 115% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            td[class=footerContainer] a[class=utilityLink] {
                display: block !important;
            }

        }</style>
        </head>

        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                    <tr>
                        <td align="center" valign="top" id="bodyCell">
                            <!-- BEGIN TEMPLATE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- BEGIN PREHEADER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                                            <tr>
                                                <td valign="top" class="preheaderContainer" style="padding-top:9px;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                                                        <tbody class="mcnTextBlockOuter">
                                                            <tr>
                                                                <td valign="top" class="mcnTextBlockInner">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" class="mcnTextContent" style="padding: 9px 18px;color: #333;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 10px;">
                                                                                    <%# ModTopHeader %>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // END PREHEADER -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- BEGIN HEADER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                            <tr>
                                                <td valign="top" class="headerContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
                                                        <tbody class="mcnImageCardBlockOuter">
                                                            <tr>
                                                                <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px; ">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <!-- BEGIN LOGO -->
                                                                                <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px; width:40%;">
                                                                                    <a href="<%: LogoUrl %><%# LogoURLTrackingCode %>" style="text-decoration: none;" target="_blank">
                                                                                        <img alt="<%: LogoAltText %>" src="<%: LogoSelector %>" width="134" style="max-width:134px; min-width: 100px;" class="mcnImage">
                                                                                    </a><!-- FallbackToLogoSelectorUrlString? <%: FallbackToLogoSelectorUrlString %> -->
                                                                                &nbsp;</td>
                                                                                <!-- END LOGO -->

                                                                                <!-- BEGIN BUTTONS -->
                                                                                <td class="mcnImageCardBottomImageContent" align="right" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px; min-width: 190px;">
                                                                                    <table align="right">
                                                                                        <!-- BEGIN BUTTON 2 -->
                                                                                        <tr style="height:50px;">
                                                                                            <td align="right">
                                                                                                <asp:PlaceHolder runat="server" ID="plcButton2" Visible='<%# ShouldRenderContent("Button2Text") %>'>
                                                                                                    <table cellspacing="0" align="right" cellpadding="0" border="0" class="<%# ShouldRenderOnMobile("Button2EnableInMobile") %>" style="border-collapse: separate !important;border: 2px none #707070;border-top-left-radius: 0px;border-top-right-radius: 0px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;background-color:<%= Button2Color %>; margin: 8px 0 0; min-width: 145px;" class="mcnButtonContentContainer">
                                                                                                    <tr>
                                                                                                        <td valign="middle" align="center" style="font-family: Arial; font-size: 15px; min-width: 145px; padding: 7px 14px;" class="mcnButtonContent">
                                                                                                            <a href="<%= Button2LinkIsExternal ? String.Empty : AbsoluteUrl %><%# Page.ResolveUrl(Eval("Button2Link").ToString()) %>" title=" <%# Eval("Button2Text") %>" style="display:block; font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none; text-transform:uppercase; color: #ffffff;" target="_blank" class="mcnButton ">
                                                                                                                <%# Eval("Button2Text") %>
                                                                                                                <span style="padding-left: 5px;">
                                                                                                                    <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-arrow.png" />
                                                                                                                </span>
                                                                                                            </a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                </asp:PlaceHolder>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <!-- END BUTTON 2 -->

                                                                                        <!-- BEGIN BUTTON 1 -->
                                                                                        <asp:PlaceHolder runat="server" ID="plcButton1" Visible='<%# ShouldRenderContent("Button1Text") %>'>
                                                                                            <tr>
                                                                                                <td align="right">
                                                                                                    <table cellspacing="0" cellpadding="0" align="right" border="0" class="<%# ShouldRenderOnMobile("Button1EnableInMobile") %>" style="border-collapse: separate !important;border: 2px none #ed2024;border-top-left-radius: 0px;border-top-right-radius: 0px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;background-color: <%= Button1Color %>; margin: 8px 0 0; min-width: 145px;" class="mcnButtonContentContainer">
                                                                                                        <tr>
                                                                                                            <td valign="middle" align="center" style="font-family: Arial; font-size: 15px;  min-width: 145px; padding: 7px 14px;" class="mcnButtonContent">
                                                                                                                <a href="<%= Button1LinkIsExternal ? String.Empty : AbsoluteUrl %><%# Page.ResolveUrl(Eval("Button1Link").ToString()) %>" title="<%# Eval("Button1Text") %>" style="font-weight: normal;letter-spacing: normal; display:block;line-height: 100%;text-align: center;text-decoration: none; text-transform:uppercase; color: #ffffff;" target="_blank" class="mcnButton ">
                                                                                                                    <%# Eval("Button1Text") %>
                                                                                                                    <span style="padding-left: 5px;">
                                                                                                                        <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-arrow.png" />
                                                                                                                    </span></a>
                                                                                                           </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </asp:PlaceHolder>
                                                                                         <!-- END BUTTON 1 -->
                                                                                    </table>
                                                                                </td>
                                                                                <!-- END BUTTONS -->
                                                                            </tr>
                                                                         </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock">
                                                        <tbody class="mcnImageBlockOuter">
                                                            <tr>
                                                                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0"
                                                                           class="mcnImageContentContainer">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0;">

                                                                                <!-- BEGIN HEADER LINK OPEN TAG -->
                                                                                <asp:PlaceHolder runat="server" Visible='<%# ShouldRenderContent("HeaderLink") %>'>
                                                                                    <a href="<%= HeaderLinkIsExternal ? String.Empty : AbsoluteUrl %><%# Page.ResolveUrl(Eval("HeaderLink").ToString()) %>" id="lnkHeaderImage" target="_blank" title="<%# Eval("HeaderAltText") %>">
                                                                                </asp:PlaceHolder>
                                                                                <!-- END HEADER LINK OPEN TAG -->

                                                                                <!-- BEGIN HEADER IMAGE -->
                                                                                <asp:PlaceHolder runat="server" Visible='<%# ShouldRenderContent("HeaderImage") %>'>
                                                                                    <img align="left" alt="<%# Eval("HeaderAltText") %>" src="<%: HeaderImage %>" width="600" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                                                </asp:PlaceHolder>
                                                                                <!-- END HEADER IMAGE -->
                                                                                
                                                                                <!-- BEGIN HEADER LINK CLOSE TAG -->
                                                                                <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# ShouldRenderContent("HeaderLink") %>'>
                                                                                    </a>
                                                                                </asp:PlaceHolder>
                                                                                <!-- END HEADER LINK CLOSE TAG -->

                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // END HEADER -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- BEGIN BODY // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                            <tr>
                                                <!-- BEGIN MAIN COLUMN -->
                                                <td align="center" valign="top" width="<%# ColumnWidth %>" class="sectionContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBodyInner">
                                                        <tr>
                                                            <td valign="top" class="bodyContainer">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                                                                    <tbody class="mcnTextBlockOuter">
                                                                        <tr>
                                                                            <td valign="top" class="mcnTextBlockInner">

                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="<%# ColumnWidth %>" class="mcnTextContentContainer">
                                                                                    <tbody>
                                                                                        <tr>

                                                                                            <td valign="top" class="mcnTextContent" style="padding: 9px 18px;color: #333;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 14px;">
                                                                                                
                                                                                                <asp:PlaceHolder runat="server" ID="plcBody">
                                                                                                    <asp:Literal runat="server" id="ltrBody"></asp:Literal>    
                                                                                                </asp:PlaceHolder> 
                                                                                                
                                                                                                
                                                                                                 <!-- BEGIN DM ARTICLE LISTING -->
                                                                                                 <cms:CMSRepeater runat="server" ID="rptDMListing" Visible="<%# IsDm %>" Path="./%" ClassNames="CRC.EmailDMArticle" OrderBy="NodeOrder" >
                                                                                                     <ItemTemplate>
                                                                                                         <div style="padding-top:20px;">
                                                                                                            <%# Eval("Content") %>
                                                                                                         </div>
                                                                                                     <hr class="hide-last" style="border-width: 1px; border-top:none; border-left:none; border-right:none; border-color: #cbccce; margin-bottom: 25px;" />
                                                                                                     </ItemTemplate>
                                                                                                 </cms:CMSRepeater>
                                                                                                <!-- END DM ARTICLE LISTING -->

                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>

                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <!-- END MAIN COLUMN -->
                                                
                                               

                                                <!-- BEGIN RIGHT RAIL -->
                                                <asp:PlaceHolder runat="server" ID="plcRightRail" Visible='<%# ShouldRenderContent("RightRail1Image") %>'>
                                                    <td align="center" valign="top" width="200" class="sectionContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateSidebar">
                                                            
                                                           <!-- BEGIN FIRST CTA -->
                                                             <tr class="<%# ShouldRenderOnMobile("RightRail1EnableOnMobile") %>">
                                                                <td align="right" valign="top">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateSidebarInner">
                                                                        <tr>
                                                                            <td valign="top" class="sidebarContainer">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                                                                    <tbody class="mcnCaptionBlockOuter">
                                                                                    <tr>
                                                                                        <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:9px 9px 9px 9px; min-width:168px;">
                                                                                                            <a href="<%= RightRail1LinkIsExternal ? String.Empty : AbsoluteUrl %><%# Page.ResolveUrl(Eval("RightRail1Link").ToString()) %>" target="_blank" style="text-decoration:none;">
                                                                                                                <img alt="" src="<%: RightRail1Image %>" width="168" style="max-width:194px;" class="mcnImage">
                                                                                                            </a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="mcnTextContent" valign="top"style="padding:0 9px 0 9px;" width="168"></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <!-- BEGIN END CTA -->

                                                            <!-- BEGIN SECOND CTA -->
                                                            <asp:PlaceHolder runat="server" ID="plcCta2" Visible='<%# ShouldRenderContent("RightRail2Image") %>'>
                                                                <tr class="<%# ShouldRenderOnMobile("RightRail2EnableOnMobile") %>">
                                                                    <td align="right" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="Table1">
                                                                            <tr>
                                                                                <td valign="top" class="sidebarContainer">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                                                                        <tbody class="mcnCaptionBlockOuter">
                                                                                        <tr>
                                                                                            <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:9px 9px 9px 9px; min-width:168px;">
                                                                                                                <a href="<%= RightRail2LinkIsExternal ? String.Empty : AbsoluteUrl %><%# Page.ResolveUrl(Eval("RightRail2Link").ToString()) %>" target="_blank" style="text-decoration:none;">
                                                                                                                    <img alt="" src="<%: RightRail2Image %>" width="168" style="max-width:194px;" class="mcnImage">
                                                                                                                </a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="mcnTextContent" valign="top"style="padding:0 9px 0 9px;" width="168"></td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </asp:PlaceHolder>
                                                            <!-- END SECOND CTA -->
                                                        </table>
                                                    </td>
                                                </asp:PlaceHolder>
                                                <!-- END RIGHT RAIL -->
                                            </tr>
                                        </table>
                                        <!-- // END BODY -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- BEGIN FOOTER // -->
                                        <table width="600" cellspacing="0" cellpadding="0" border="0" id="templateFooter">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" style="padding-bottom:9px;" class="footerContainer">
                                                        <!-- BEGIN FD FOOTER - OPTIONAL -->
                                                        <asp:PlaceHolder runat="server" ID="fdFooter" Visible='<%# ShouldRenderContent("FDFooterContent") %>'>
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0" class="mcnBoxedTextBlock">
                                                                <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td valign="top" class="mcnBoxedTextBlockInner">

                                                                            <table cellspacing="0" cellpadding="0" width="600" border="0" align="left" class="mcnBoxedTextContentContainer">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="">
                                                                                            <table cellspacing="0" cellpadding="18" width="100%" border="0" class="mcnTextContentContainer" style="border: 1px solid #D8D8D8;background-color: #D8D8D8;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td width="80%" valign="top" align="center" class="mcnTextContent mobile-12" style="color: #444444;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: left; padding-left: 40px; max-width:80%;">
                                                                                                            <%# Eval("FDFooterContent") %>
                                                                                                        </td>

                                                                                                        <asp:PlaceHolder runat="server" ID="plcFdFooterImageRight" Visible='<%# ShouldRenderContent("FDRightImage") %>'>
                                                                                                            <td width="20%" valign="top" align="center" style="padding-right: 40px;" class="mobile-12 <%# ShouldRenderOnMobile("FDEnableRightImageOnMobile") %>">
                                                                                                                <img width="61" height="61" style="max-width: 70px;" src="<%: FDRightImage %>">
                                                                                                            </td>
                                                                                                        </asp:PlaceHolder>
                                
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:PlaceHolder>
                                                        <!-- END FD FOOTER - OPTIONAL -->
                                                        
                                                        <!-- BEGIN SOCIAL MEDIA - NOT EDITABLE -->
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnFollowBlock">
                                                            <tbody class="mcnFollowBlockOuter">
                                                                <tr>
                                                                    <td valign="top" align="center" class="mcnFollowBlockInner" style="">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnFollowContentContainer">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border: 1px solid #9B9D9F;background-color: #9B9D9F;" class="mcnFollowContent">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign="top" align="center" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td valign="top">
                                                                                                                        <table align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top" class="mcnFollowContentItemContainer"style="padding-right:10px; padding-bottom:9px;">
                                                                                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnFollowContentItem">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td valign="middle" align="left" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                                        <table width="" align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                                            <tbody>
                                                                                                                                                                <tr>
                                                                                                                                                                    <td width="32" valign="middle" align="center" class="mcnFollowIconContent">
                                                                                                                                                                        <a target="_blank" href='<%# FacebookURL %>' style="text-decoration: none; color: white; font-size: 24px;">
                                                                                                                                                                            <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-facebook.gif" alt="facebook"/>
                                                                                                                                                                         </a>
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </tbody>
                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>

                                                                                                                        <!--[if gte mso 6]>
                                                                                                                        </td>
                                                                                                                        <td align="left" valign="top">
                                                                                                                        <![endif]-->


                                                                                                                        <table align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top" class="mcnFollowContentItemContainer" style="padding-right:10px; padding-bottom:9px;">
                                                                                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnFollowContentItem">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td valign="middle" align="left" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                                        <table width="" align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                                            <tbody>
                                                                                                                                                                <tr>
                                                                                                                                                                    <td width="32" valign="middle" align="center" class="mcnFollowIconContent">
                                                                                                                                                                        <a target="_blank" href='<%# TwitterURL %>' style="text-decoration: none; color: white; font-size: 24px;">
                                                                                                                                                                            <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-twitter.gif" alt="twitter"/>
                                                                                                                                                                        </a>
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </tbody>
                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>

                                                                                                                        <!--[if gte mso 6]>
                                                                                                                        </td>
                                                                                                                        <td align="left" valign="top">
                                                                                                                        <![endif]-->

                                                                                                                        <table align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td valign="top" class="mcnFollowContentItemContainer" style="padding-right:0; padding-bottom:9px;">
                                                                                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnFollowContentItem">
                                                                                                                                            <tbody>
                                                                                                                                                <tr>
                                                                                                                                                    <td valign="middle" align="left" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                                        <table width="" align="left" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                                            <tbody>
                                                                                                                                                                <tr>
                                                                                                                                                                    <td width="32" valign="middle" align="center" class="mcnFollowIconContent">
                                                                                                                                                                        <a target="_blank" href='<%# InstagramURL %>' style="text-decoration: none; color: white; font-size: 24px;">
                                                                                                                                                                            <img src="<%= AbsoluteUrl %>/CRC/img/Emails/icon-instagram.gif" alt="instagram"/>
                                                                                                                                                                        </a>
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
                                                                                                                                                            </tbody>
                                                                                                                                                        </table>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>

                                                                                                                        <!--[if gte mso 6]>
                                                                                                                        </td>
                                                                                                                        <td align="left" valign="top">
                                                                                                                        <![endif]-->

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- END SOCIAL MEDIA - NOT EDITABLE -->
                                                        
                                                        <!-- BEGIN FOOTER TEXT -->
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnTextBlock">
                                                            <tbody class="mcnTextBlockOuter">
                                                                <tr>
                                                                    <td valign="top" class="mcnTextBlockInner">

                                                                        <table width="600" align="left" cellspacing="0" cellpadding="0" border="0" class="mcnTextContentContainer">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" style="padding: 9px 18px; text-align: center;" class="mcnTextContent">
                                                                                       <%# Eval("FooterContent") %>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- END FOOTER TEXT -->

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- // END FOOTER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // END TEMPLATE -->
                        </td>
                    </tr>
                </table>
            </center>
        </body>
        </html>
        
        <!-- COPY MARKUP ENDING HERE -->
    </ItemTemplate>
</asp:Repeater>