using CMS.CMSHelper;
using CMS.PortalControls;
using System.Web;
using CMS.Helpers;

public partial class InlineButton : CMSAbstractWebPart
{
    #region "Properties"

    public string AbsoluteUrl
    {
        get { return "http://" + HttpContext.Current.Request.Url.Authority.ToLower(); }
    }
    public bool WidgetLinkIsExternal
    {
        get { return ValidationHelper.GetBoolean(GetValue("WidgetLinkIsExternal"), false); }
    }
    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Set WebPart properties
            // PLEASE PUT YOUR CODE HERE
        }
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        // Reload data in all controls if needed
        // PLEASE PUT YOUR CODE HERE
    }

    #endregion
}
