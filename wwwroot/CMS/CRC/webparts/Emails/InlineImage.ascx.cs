using System;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using System.Web;
using CMS.Helpers;

public partial class InlineImage : CMSAbstractWebPart
{

    public string AbsoluteUrl
    {
        get { return "http://" + HttpContext.Current.Request.Url.Authority.ToLower(); }
    }

    public bool ImageLinkIsExternal
    {
        get { return ValidationHelper.GetBoolean(GetValue("ImageLinkIsExternal"), false); }
    }

    public bool ShouldRenderContent(string field)
    {
        var doctypeValue = DataHelper.GetNotEmpty(GetValue(field), string.Empty);
        return !String.IsNullOrWhiteSpace(doctypeValue) ? true : false;
    }

    public string ShouldRenderOnMobile(string field)
    {
        var mobileVisibility = ValidationHelper.GetBoolean(GetValue(field), false);
        return mobileVisibility ? "" : "hide-on-mobile";
    }

    public string ImageStyle { get; set; }
    public string Image { get; set; }

    #region "Methods"

    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            var checkIfImageFullWidth = ValidationHelper.GetBoolean(GetValue("FullWidth"), false);
            ImageStyle = checkIfImageFullWidth ? "width:100%; min-width:100%;" : "";

            var imagePath = ValidationHelper.GetString(GetValue("Image"), string.Empty);
            Image = AbsoluteUrl + imagePath.TrimStart('~');

            // only display link on front end if the link property was set
            plcHref1.Visible = ShouldRenderContent("ImageLink");
            plcHref2.Visible = ShouldRenderContent("ImageLink");
        }
    }

    #endregion
}
