<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InlineImage.ascx.cs" Inherits="InlineImage" %>


    <div class="<%: ShouldRenderOnMobile("ShowOnMobile") %>">
        <asp:PlaceHolder runat="server" ID="plcHref1" >
            <a href="<%: ImageLinkIsExternal ? String.Empty : AbsoluteUrl %><%: Page.ResolveUrl(GetValue("ImageLink").ToString()) %>" title="<%: GetValue("ImageAlt")%>" target="_blank" >
        </asp:PlaceHolder>
        
        <img src="<%: Image %>" alt="<%: GetValue("ImageAlt")%>" style='<%: ImageStyle %>'/>  

        <asp:PlaceHolder runat="server" ID="plcHref2" >
            </a>
        </asp:PlaceHolder>
    </div>
