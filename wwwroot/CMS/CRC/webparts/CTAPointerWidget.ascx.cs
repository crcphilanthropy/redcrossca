using System;
using System.Web;
using System.Web.Script.Serialization;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Localization;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.SiteProvider;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Models;

namespace CMSApp.CRC.webparts
{
    public partial class CTAPointerWidget : CMSAbstractWebPart
    {
        public string CTADocumentName { get; set; }

        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("LayoutTransformationDropdown"), string.Empty); }
            set { SetValue("LayoutTransformationDropdown", value); }
        }

        public string Path
        {
            get { return ValidationHelper.GetString(GetValue("Path"), string.Empty); }
            set { SetValue("Path", value); }
        }

        public int ShowPromotionNTimes
        {
            get { return ValidationHelper.GetInteger(GetValue("ShowPromotionNTimes"), 5); }
            set { SetValue("ShowPromotionNTimes", value); }
        }

        public int HidePromotionNDays
        {
            get { return ValidationHelper.GetInteger(GetValue("HidePromotionNDays"), 30); }
            set { SetValue("HidePromotionNDays", value); }
        }

        public bool IsPromotionalBanner
        {
            get { return ValidationHelper.GetBoolean(GetValue("IsPromotionalBanner"), false); }
            set { SetValue("IsPromotionalBanner", value); }
        }

        public bool IsEmailCTA
        {
            get { return ValidationHelper.GetBoolean(GetValue("IsEmailCTA"), false); }
            set { SetValue("IsEmailCTA", value); }
        }

        private bool _visible = true;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            phBannerTop.Visible = phBannerBottom.Visible = IsPromotionalBanner;
            phBannerBottom.DataBind();

            if (IsPromotionalBanner)
                _visible = ShowPromotionalBanner();

            if (_visible || !IsPromotionalBanner)
                InitCTA();
        }

        private bool ShowPromotionalBanner()
        {
            if (ValidationHelper.GetBoolean(SessionHelper.GetValue("PromoBannerClosed"), false) || AlreadySubcribe())
                return false;

            var serializer = new JavaScriptSerializer();
            const string cookieName = "promo_banner";
            var cookie = Request.Cookies[cookieName];
            PromotionalCookie promoCookie = null;

            if (cookie != null)
            {
                try
                {
                    promoCookie = serializer.Deserialize<PromotionalCookie>(cookie.Value);
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogWarning("CTAPointerWidget", "ShowPromotionalBanner", ex, SiteContext.CurrentSiteID, null);
                }

                if (promoCookie == null || !promoCookie.CTAPath.Equals(Path, StringComparison.OrdinalIgnoreCase))
                {
                    RemovePromoBannerEmailCookie();
                    promoCookie = CreatePromotionCookie();
                    cookie.Expires = promoCookie.DateTime.AddDays(HidePromotionNDays);
                }

                if (promoCookie.CloseCounter >= ShowPromotionNTimes) return false;

            }
            else
            {
                cookie = new HttpCookie(cookieName);

                promoCookie = CreatePromotionCookie();
                cookie.Expires = promoCookie.DateTime.AddDays(HidePromotionNDays);

            }

            cookie.Value = serializer.Serialize(promoCookie);

            Response.Cookies.Add(cookie);
            return true;
        }

        private bool AlreadySubcribe()
        {
            const string cookieName = "promo_banner_email";
            var cookie = HttpContext.Current.Request.Cookies[cookieName] ?? new HttpCookie(cookieName);
            return cookie != null && (cookie.Value ?? string.Empty).Equals("1") && IsEmailCTA;
        }

        private void RemovePromoBannerEmailCookie()
        {
            if (Request.Cookies["promo_banner_email"] != null)
            {
                var myCookie = new HttpCookie("promo_banner_email") { Expires = DateTime.Now.AddDays(-1d) };
                Response.Cookies.Add(myCookie);
            }
        }

        private PromotionalCookie CreatePromotionCookie()
        {
            return new PromotionalCookie
            {
                CTAPath = Path,
                DateTime = DateTime.Now
            };
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            Visible = _visible || !IsPromotionalBanner;
        }

        private void InitCTA()
        {
            rptCTA.TransformationName = TransformationName;

            var data = CacheHelper.Cache(() =>
                {
                    var selectCtaQuery = new DataQuery("crc.cta", "selectcta")
                    {
                        Parameters = new QueryDataParameters
                                    {
                                        {"@DocumentCulture", LocalizationContext.PreferredCultureCode},
                                        {"@SiteID", SiteContext.CurrentSiteID},
                                        {"@NodeAliasPath", Path}
                                    }
                    };
                    return selectCtaQuery.Execute();
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("ctapointerwidget_{0}_{1}_{2}", LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName, Path)));


            if (!DataHelper.DataSourceIsEmpty(data) && IsPromotionalBanner)
            {
                CTADocumentName = ValidationHelper.GetString(data.Tables[0].Rows[0]["NodeAlias"], "Text");
                phBannerTop.DataBind();
            }

            rptCTA.DataSource = data;

            rptCTA.DataBind();
        }
    }
}