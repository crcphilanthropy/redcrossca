using System;
using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class TwitterFeed : CMSAbstractWebPart
    {
        /// <summary>
        /// Username.
        /// </summary>
        public string Username
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Username"), string.Empty);
            }
            set
            {
                SetValue("Username", value);
            }
        }


        /// <summary>
        /// Number of tweets to display.
        /// </summary>
        public int NumberOfTweets
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("NumberOfTweets"), 5);
            }
            set
            {
                SetValue("NumberOfTweets", value);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataBind();
            //TransformationHelper.RegisterStartUpScript("<script src='/CMSPages/GetResource.ashx?scriptfile=/CRC/js/twitter-helper.js'></script>", Page, "twitter-helper", false);
            TransformationHelper.RegisterStartUpScript(string.Format("$(document).ready(function(){{ loadLatestTweet('{0}',{1}); }});", Username, NumberOfTweets), Page);
        }
    }
}