using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class ImageWithCaption : CMSAbstractWebPart
    {
        public string ImageUrl
        {
            get { return ValidationHelper.GetString(GetValue("ImageUrl"), string.Empty); }
            set { SetValue("ImageUrl", value); }
        }

        public string ImageTitle
        {
            get { return ValidationHelper.GetString(GetValue("ImageTitle"), string.Empty); }
            set { SetValue("ImageTitle", value); }
        }

        public string ImageAltText
        {
            get { return ValidationHelper.GetString(GetValue("ImageAltText"), string.Empty); }
            set { SetValue("ImageAltText", value); }
        }

        public string ImageCaption
        {
            get { return ValidationHelper.GetString(GetValue("ImageCaption"), string.Empty); }
            set { SetValue("ImageCaption", value); }
        }

        public override string CssClass
        {
            get { return ValidationHelper.GetString(GetValue("CssClass"), string.Empty); }
            set { SetValue("CssClass", value); }
        }

        protected int ImageWidth
        {
            get
            {
                if (ImageUrl.Contains("?") && !ImageUrl.EndsWith("?"))
                    return ValidationHelper.GetInteger(HttpUtility.ParseQueryString(ImageUrl.Substring(ImageUrl.IndexOf("?") + 1)).Get("width"), 100);

                return 100;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            DataBind();
        }
    }
}