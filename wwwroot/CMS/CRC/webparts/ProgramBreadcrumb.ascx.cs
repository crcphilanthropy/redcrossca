using System;
using System.Collections.Generic;
using System.Linq;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using TreeNode = CMS.DocumentEngine.TreeNode;
using CMS.Localization;
using CMS.Helpers;
using CMS.Taxonomy;

namespace CMSApp.CRC.WebParts
{
    public partial class ProgramBreadcrumb : CMSAbstractWebPart
    {
        private TreeNode _node;
        protected TreeNode Node
        {
            get
            {
                return (_node ??
                    (_node = new TreeProvider().SelectSingleNode(SiteContext.CurrentSiteName, GetProvinceNodeAliasPath(LocalizationContext.PreferredCultureCode), LocalizationContext.PreferredCultureCode) ??
                    new TreeProvider().SelectSingleNode(SiteContext.CurrentSiteName, GetProvinceNodeAliasPath(LocalizationContext.PreferredCultureCode.Equals("en-CA", StringComparison.OrdinalIgnoreCase) ? "fr-CA" : "en-CA"), LocalizationContext.PreferredCultureCode)
                    ?? GetSingleNode(GetProvinceNodeAliasPath(LocalizationContext.PreferredCultureCode), LocalizationContext.PreferredCultureCode)
                    ));
            }
        }

        private TreeNode GetSingleNode(string path, string lang)
        {
            var data = new TreeProvider().SelectNodes(SiteContext.CurrentSiteName, "/%", lang, false, null,
                                                      string.Format("DocumentUrlPath Like '{0}'", path), null,
                                                      TreeProvider.ALL_LEVELS, true, 1);

            return DataHelper.DataSourceIsEmpty(data) ? null : data.FirstOrDefault();
        }

        readonly Dictionary<string, string> _crumbs = new Dictionary<string, string>();

        private CategoryInfo _city;
        public CategoryInfo City
        {
            get
            {
                if (_city == null)
                {
                    var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryLevel = 3 AND CategoryName = '{0}'", (Request.QueryString["city"] ?? string.Empty)), null, 1, null);
                    _city = !DataHelper.DataSourceIsEmpty(categories) ? new CategoryInfo(categories.Tables[0].Rows[0]) : new CategoryInfo();
                }

                return _city;
            }
        }

        private CategoryInfo _province;
        public CategoryInfo Province
        {
            get
            {
                if (_province == null)
                {
                    var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryLevel = 2 AND CategoryID = {0}", City.CategoryParentID), null, 1, null);
                    _province = !DataHelper.DataSourceIsEmpty(categories) ? new CategoryInfo(categories.Tables[0].Rows[0]) : new CategoryInfo();
                }

                return _province;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Node != null)
            {
                var current = Node;
                while (current != null && current.NodeAliasPath != "/")
                {
                    if (!ValidationHelper.GetBoolean(current.GetProperty("DocumentMenuItemHideInNavigation"), true))
                        _crumbs.Add(string.IsNullOrWhiteSpace(current.DocumentUrlPath) ? current.NodeAliasPath : current.DocumentUrlPath, string.IsNullOrWhiteSpace(current.DocumentMenuCaption) ? current.DocumentName : current.DocumentMenuCaption);

                    current = current.Parent;
                }

                rptBC.DataSource = _crumbs.Reverse();
                rptBC.DataBind();
            }

            DataBind();
        }

        public string GetProvinceNodeAliasPath(string lang)
        {
            return string.Format("{0}/{1}", TransformationHelper.GetRegionPath(lang), TransformationHelper.ReplaceAccentCharacters(GetProvinceName(lang)));
        }

        private string GetProvinceName(string lang)
        {
            var provinceName = Request.QueryString["province"] ?? string.Empty;
            var uiCode = string.Format("CRC.Province.{0}", provinceName.Replace("-", string.Empty));
            var translation = ResHelper.GetString(uiCode, lang);
            if (translation.Equals(uiCode, StringComparison.OrdinalIgnoreCase))
                return provinceName;

            return translation.Replace(" ", "-");
        }
    }
}