using System;
using CMS.EventLog;
using CMS.Localization;
using CMS.PortalControls;
using CMS.Helpers;
using CMSAppAppCode.Old_App_Code.CRC;
using ECA.FormControls.MultiDocumentSelector;

namespace CMSApp.CRC.webparts
{
    public partial class TopNavLinks : CMSAbstractWebPart
    {
        public string SelectedDocuments
        {
            get { return ValidationHelper.GetString(GetValue("SelectedDocuments"), string.Empty); }
            set { SetValue("SelectedDocuments", value); }
        }

        protected override void OnInit(System.EventArgs e)
        {
            try
            {
                base.OnInit(e);
                var data =
                    CacheHelper.Cache(() => SortableDocumentProvider.GetSortedTreeNodeDataSet(SelectedDocuments,
                                                                                              columns: "NodeID, NodeGUID, DocumentGuid, DocumentCulture, DocumentMenuClass, DocumentUrlPath, NodeAliasPath, DocumentMenuCaption, DocumentName, DocumentMenuCaption"),
                                      new CacheSettings(Variables.CacheTimeInMinutes, string.Format("topnavlinks_{0}_{1}", SelectedDocuments, LocalizationContext.PreferredCultureCode)));


                if (DataHelper.DataSourceIsEmpty(data)) return;
                rptLink.DataSource = data;
                rptLink.DataBind();
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("TopNavLinks", "OnInit", ex);
            }
        }
    }
}