﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageWithTextOverlay.ascx.cs" Inherits="CMSApp.CRC.webparts.ImageWithTextOverlay" EnableViewState="false" %>
<div class="banner bottom">
    <asp:Image ID="imgBanner" runat="server" EnableViewState="False"/>
    <div class="clearfix dark">
        <div class="columns medium-8 content">
            <p>
               <cms:LocalizedLiteral ID="litTitle" runat="server" EnableViewState="False" />

               <cms:LocalizedLiteral ID="litSubTitle" runat="server" EnableViewState="False" />    
            </p>
        </div>
        
       <div class="columns medium-4">
            <cms:LocalizedHyperlink ID="lnkDestination" runat="server" CssClass="button" EnableViewState="False" />
       </div>
    </div>
</div>
