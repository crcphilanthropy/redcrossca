﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSwitch.ascx.cs" Inherits="CMSApp_CRC_webparts_LanguageSwitch" %>
<asp:PlaceHolder runat="server" ID="phLanguageToggle">
    <% if(CurrentDocument.NodeAlias != "Donate"){ %>
    <li class="language-switch">
    <% } %>
        <asp:LinkButton runat="server" ID="LanguageSwitchLink" CssClass="languageSwitchLink"></asp:LinkButton>
        <input type="hidden" id="hidLanguageToggleHash" runat="server" class="language-toggle-hash" />
    <% if(CurrentDocument.NodeAlias != "Donate"){ %>
    </li>
    <% } %>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phDocumentMissingMessage" Visible="False">
    <li class="language-switch">
        <asp:Literal ID="litDocumentMissingMessage" runat="server" Visible="true"></asp:Literal>
    </li>
</asp:PlaceHolder>
