﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DonationValidation.ascx.cs" Inherits="CMSApp.CRC.webparts.DonationValidation" %>
<script type="text/javascript">
    var _DonateValidateTooHigh = "<cms:LocalizedLiteral runat="server" ResourceString="CRC.Donate.ValidateTooHigh"></cms:LocalizedLiteral>";
    var _DonateValidateTooLow = "<cms:LocalizedLiteral runat="server" ResourceString="CRC.Donate.ValidateTooLow"></cms:LocalizedLiteral>";
    var _DonateValidateNonNumeric = "<cms:LocalizedLiteral runat="server" ResourceString="CRC.Donate.ValidateNumericOnly"></cms:LocalizedLiteral>";

    // validate donation amounts, partially written by Daniel Shields from Cardinal Path
    function GetValidatedAmount(Amount) {
        /*
        * purpose- validate inputs before code execution
        * custom function to prevent robots
        * form checks for - positive numbers, numerical input, and limits donation size.
        * validation called from onsubmit in form tag or onclick for submit button
        */
        inputFormValidatedA = Amount.replace("$", "");
        inputFormValidatedB = inputFormValidatedA.replace(",", ".");
        inputFormValidatedC =  inputFormValidatedB; //inputFormValidatedB.replace(".", "");

        //logical tests and alerts
        if (inputFormValidatedC <= 0) {
            alert(_DonateValidateTooLow);
            return;
        }

        if (!inputFormValidatedC.match(/^[1-9]\d*(\.\d+)?$/)) {
            alert(_DonateValidateNonNumeric);
            return;
        }

        if (inputFormValidatedC > 99999) {
            alert(_DonateValidateTooHigh);
            return;
        }
        return inputFormValidatedC;
        // end of validation
    }
</script>
