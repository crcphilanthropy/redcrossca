﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindLocalServicesResults.ascx.cs" Inherits="CMSApp.CRC.webparts.InYourCommunity.FindLocalServicesResults" ViewStateMode="Disabled"  %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Import Namespace="CMSAppAppCode.Old_App_Code.CRC" %>
<%@ Register Src="~/CRC/webparts/Controls/CRCPager.ascx" TagPrefix="uc1" TagName="CRCPager" %>
<section class="iyc-result" id="iyc-result">
    <asp:PlaceHolder runat="server" ID="phResultPivotDescription">
        <p class="result-pivot cp-refinement" data-value="search refinement"  style="display: none;">
            <%# ResourceStringHelper.GetString(
                Variables.MaxDistance == 0 ? "CRC.IYC.ServiceResultDescription" : "CRC.IYC.ServiceResultPivotDescription", 
                defaultText:Variables.MaxDistance == 0 
                ? "Showing results near <strong class=\"result-pivot-location\">Cityname, Province</strong>.  Looking for services in a different location?  Enter a postal code or city and province in the field above."
                : "Showing results within <strong class=\"result-pivot-max-distance\">km</strong> of <strong class=\"result-pivot-location\">Cityname, Province</strong>.  Looking for services in a different location?  Enter a postal code or city and province in the field above.") %>
        </p>
        <p class="no-result-pivot cp-refinement" data-value="search refinement"  style="display: none;">
            <%# ResourceStringHelper.GetString(Variables.MaxDistance == 0 ? "CRC.IYC.ServiceNoResultDescription" : "CRC.IYC.ServiceNoResultPivotDescription", 
            defaultText:Variables.MaxDistance == 0 
            ? "Your search criteria returned 0 results near <strong class=\"result-pivot-location\">Cityname, Province</strong>.  Looking for services in a different location?  Enter a postal code or city and province in the field above."
            : "Your search criteria returned 0 results within <strong class=\"result-pivot-max-distance\">km</strong> of \"<strong class=\"result-pivot-location\">Cityname, Province</strong>\". Looking for services in a different location? Enter a postal code or city and province in the field above.") %>
        </p>
    </asp:PlaceHolder>
    <div class="no-more-tables">
        <asp:Repeater runat="server" ID="rptItems">
            <HeaderTemplate>
                <input type="hidden"/>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><%# ResourceStringHelper.GetString("CRC.IYC.Service", defaultText:"Branch and Service") %></th>
                            <th><%# ResourceStringHelper.GetString("CRC.IYC.ServiceType", defaultText:"Service Type") %></th>
                            <th><%# ResourceStringHelper.GetString("CRC.IYC.Distance", defaultText:"Dist.") %></th>
                            <th><%# ResourceStringHelper.GetString("CRC.IYC.LocalBranch", defaultText:"Address") %></th>
                            <th><%# ResourceStringHelper.GetString("CRC.IYC.Contact", defaultText:"Contact") %></th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td data-content="<%# ResourceStringHelper.GetString("CRC.IYC.Service", defaultText:"Service") %>">
                        <strong>
                            <a href="<%# Eval("DocumentLiveUrl") %>" class="cp-track" data-category="find local services" data-action="search result click" data-label='<%# Eval("Name") %>'><%# Eval("Name") %></a>
                        </strong>
                    </td>
                    <td data-content="<%# ResourceStringHelper.GetString("CRC.IYC.ServiceType", defaultText:"Service Type") %>">
                        <a href="<%# Eval("CategoryUrl") %>" class="cp-track" data-category="find local services" data-action="search result click" data-label='<%# Eval("CategoryName") %>'>
                            <img src="<%# Eval("CategoryIconPath") %>" alt="<%# Eval("CategoryName") %>" class="svg" /></a>
                        <asp:PlaceHolder runat="server" ID="phSubCategory" Visible='<%# !string.IsNullOrWhiteSpace(Eval("SubCategoryName") as string) %>'>
                            <a href="<%# Eval("SubCategoryUrl") %>" class="cp-track" data-category="find local services" data-action="search result click" data-label='<%# Eval("SubCategoryName") %>'>
                                <img src="<%# Eval("SubCategoryIconPath") %>" alt="<%# Eval("SubCategoryName") %>" class="svg" /></a>
                        </asp:PlaceHolder>
                    </td>
                    <td data-content="<%# ResourceStringHelper.GetString("CRC.IYC.Distance", defaultText:"Dist.") %>">
                        <p>
                            <%# Eval("Distance","{0:0 KM}") %>
                        </p>
                    </td>
                    <td data-content="<%# ResourceStringHelper.GetString("CRC.IYC.LocalBranch", defaultText:"Local Branch") %>">
                        <p>
                            <%# TransformationHelper.ReplaceNewLine(Eval("Address") as string) %>
                        </p>
                    </td>
                    <td data-content="<%# ResourceStringHelper.GetString("CRC.IYC.Contact", defaultText:"Contact") %>">
                        <p>
                            <%# TransformationHelper.AddClickToCall(TransformationHelper.ReplaceNewLine(Eval("Contact") as string)) %>
                        </p>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <uc1:CRCPager runat="server" ID="CRCPager" PageSize="1" GroupSize="10" IsLateBinding="true" />
</section>
