﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindLocalServicesFilter.ascx.cs" Inherits="CMSApp.CRC.webparts.InYourCommunity.FindLocalServicesFilter" ViewStateMode="Disabled" %>
<%@ Import Namespace="CMSApp.CRC" %>
<div class="search-wrap disabled optional-field iyc-filter" id="iyc-filter">
    <h3 class="h2"><%# ResourceStringHelper.GetString("CRC.IYC.FindLocalServices", defaultText:"Find Local Services") %></h3>

    <p><%# ResourceStringHelper.GetString("CRC.IYC.FindLocalServicesInstructions", defaultText:"Use the fields below to find services in your area") %></p>

    <div class="row clearfix">
    
        <div class="medium-5 small-12 columns">

            <fieldset>
                <label><%# ResourceStringHelper.GetString("CRC.IYC.ServicesInProvince", defaultText:"Services in Your Province") %></label>
                <asp:DropDownList runat="server" ID="ddlCategories" CssClass="category-select "  />
            </fieldset>

            <div class="search-cat">
                <fieldset>
                    <label><%# ResourceStringHelper.GetString("CRC.IYC.ServiceSubCategory", defaultText:"Service Sub-Category") %></label>
                    <asp:DropDownList runat="server" ID="ddlSubCategories" CssClass="subcategory-select" />
                </fieldset>
            </div>

        </div>

        <div class="medium-2 small-12 columns and-or">
            <p><%# ResourceStringHelper.GetString("CRC.IYC.AndOr", defaultText:"and/or") %></p>
        </div>

        <div class="medium-5 small-12 columns">

            <fieldset>
                <label><%# ResourceStringHelper.GetString("CRC.IYC.CityPostalCode", defaultText:"City or Postal Code") %></label>
                <asp:TextBox runat="server" ID="txtLocation" CssClass="iyc-location" placeholder='<%# ResourceStringHelper.GetString("CRC.IYC.CityProvincePlaceholder", defaultText:"e.g. City, Province") %>'></asp:TextBox>
            </fieldset>
        </div>
        <div class="columns search-field">  
            <div class="wrap">      
               <a class="button iyc-filter-button eca-form-track" data-event="generic-event" data-category="find local services" data-action="search" data-filter="on" > <span><%# ResourceStringHelper.GetString("CRC.IYC.FindServicesNearYou", defaultText:"Find Service Near You") %></span></a>
               <asp:Button runat="server" ID="btnSearch" CssClass="iyc-filter-button-submit" Style="display: none;" />
            </div>
         </div>

    </div>    
    <asp:HiddenField runat="server" ID="hidLatitude" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidLongitude" ClientIDMode="Static" />
    <span class="location-error error" style="display: none;">
      <%# ResourceStringHelper.GetString("CRC.IYC.InvalidAddress",defaultText:"Please enter a valid City or Postal Code") %>
   </span>
</div>

