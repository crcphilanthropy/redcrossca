﻿using System;
using System.Globalization;
using System.Linq;
using CMS.Helpers;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSApp.CRC.webparts.InYourCommunity
{
    public partial class FindLocalServicesResults : CMSAbstractWebPart
    {
        /// <summary>
        /// Gets or sets the number of records to display on a page.
        /// </summary>
        public int PageSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PageSize"), 20);
            }
            set
            {
                SetValue("PageSize", value);
            }
        }


        /// <summary>
        /// Gets or sets the number of pages displayed for current page range.
        /// </summary>
        public int GroupSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("GroupSize"), 10);
            }
            set
            {
                SetValue("GroupSize", value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CRCPager.PageSize = PageSize;
            CRCPager.GroupSize = GroupSize;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            BindRepeater();
            phResultPivotDescription.DataBind();
        }

        private void BindRepeater()
        {
            decimal lat;
            decimal lng;
            decimal.TryParse(Request.QueryString["lat"], NumberStyles.Any, CultureInfo.InvariantCulture, out lat);
            decimal.TryParse(Request.QueryString["long"], NumberStyles.Any, CultureInfo.InvariantCulture, out lng);

            var searchConfig = new ServiceSearchConfig
                {
                    CategoryDocumentGuid = QueryHelper.GetGuid("category", Guid.Empty),
                    SubCategoryDocumentGuid = QueryHelper.GetGuid("subcategory", Guid.Empty),
                    Latitude = lat,
                    Longitude = lng,
                    MaxDistance = Variables.MaxDistance
                };

            if (searchConfig.IsEmpty()) return;

            if (lat == 0 && lng == 0)
            {
                var provider = new ServiceCategoryProvider();
                if (provider.ProvinceNode != null)
                {
                    searchConfig.Latitude = provider.ProvinceNode.Latitude;
                    searchConfig.Longitude = provider.ProvinceNode.Longitude;
                }
            }
            var items = new ServiceProvider().Search(searchConfig).ToList();

            CRCPager.DataSource = items;
            CRCPager.Repeater = rptItems;
            CRCPager.DataBind();

            if (items.Any())
                TransformationHelper.RegisterStartUpScript(string.Format("iyc.setPivotLocation({0},{1});", searchConfig.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")), searchConfig.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA"))), Page, "setPivotLocation");
            else
                TransformationHelper.RegisterStartUpScript(string.Format("iyc.setNoResult({0},{1},{2});", searchConfig.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")), searchConfig.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")), Variables.MaxDistance), Page, "setNoResult");

        }
    }
}