﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.Helpers;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSApp.CRC.webparts.InYourCommunity
{
    public partial class FindLocalBranchesResults : CMSAbstractWebPart
    {
        /// <summary>
        /// Gets or sets the number of records to display on a page.
        /// </summary>
        public int PageSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PageSize"), 20);
            }
            set
            {
                SetValue("PageSize", value);
            }
        }


        /// <summary>
        /// Gets or sets the number of pages displayed for current page range.
        /// </summary>
        public int GroupSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("GroupSize"), 10);
            }
            set
            {
                SetValue("GroupSize", value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CRCPager.PageSize = PageSize;
            CRCPager.GroupSize = GroupSize;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            BindRepeater();
            phResultPivotDescription.DataBind();
        }

        private void BindRepeater()
        {
            decimal lat;
            decimal lng;

            decimal.TryParse(Request.QueryString["lat"], NumberStyles.Any, CultureInfo.InvariantCulture, out lat);
            decimal.TryParse(Request.QueryString["long"], NumberStyles.Any, CultureInfo.InvariantCulture, out lng);

            if (lat == 0 && lng == 0)
                return;

            var items = new BranchProvider().Search(lat, lng, Variables.MaxDistance).ToList();
            rptItems.ItemDataBound += rptItems_ItemDataBound;

            CRCPager.DataSource = items;
            CRCPager.Repeater = rptItems;
            CRCPager.DataBind();

            if (items.Any())
                TransformationHelper.RegisterStartUpScript(string.Format("iyc.setPivotLocation({0},{1});", lat.ToString(CultureInfo.GetCultureInfo("en-CA")), lng.ToString(CultureInfo.GetCultureInfo("en-CA"))), Page, "setPivotLocation");
            else

                TransformationHelper.RegisterStartUpScript(string.Format("iyc.setNoResult({0},{1},{2});", lat.ToString(CultureInfo.GetCultureInfo("en-CA")), lng.ToString(CultureInfo.GetCultureInfo("en-CA")), Variables.MaxDistance), Page, "setNoResult");
        }

        void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            var rptServices = e.Item.FindControl("rptServices") as Repeater;
            if (rptServices == null) return;

            var branch = e.Item.DataItem as IBranch;
            if (branch == null) return;

            rptServices.DataSource = branch.UseCategories ? branch.GetCategories(string.Format("/{0}/%", branch.NodeAliasPath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Take(2).Join("/")), "CRC.IYCSimpleCategory") : branch.Services;
            rptServices.DataBind();
        }
    }
}