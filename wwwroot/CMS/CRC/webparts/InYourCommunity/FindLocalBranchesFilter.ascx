﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindLocalBranchesFilter.ascx.cs" Inherits="CMSApp.CRC.webparts.InYourCommunity.FindLocalBranchesFilter" ViewStateMode="Disabled" %>
<%@ Import Namespace="CMSApp.CRC" %>
<div class="search-wrap local-branch iyc-filter" id="iyc-filter">
    <h3 class="h2"><%# ResourceStringHelper.GetString("CRC.IYC.FindYourLocalBranch", defaultText:"Find Your Local Branch") %></h3>

    <div class="clearfix">

        <div class=" left large-6 medium-6 small-12">
            <fieldset>
                <label><%# ResourceStringHelper.GetString("CRC.IYC.CityPostalCode", defaultText:"City or Postal Code") %></label>
                <asp:TextBox runat="server" ID="txtLocation" CssClass="iyc-location" placeholder='<%# ResourceStringHelper.GetString("CRC.IYC.CityProvincePlaceholder", defaultText:"e.g. City, Province") %>'  ></asp:TextBox>
            </fieldset>
        </div>

        <div class=" left large-6 medium-6 small-12 submit">
            <fieldset>
                <a class="button iyc-filter-button eca-form-track" data-event="generic-event" data-category="contact a branch<%# CurrentDocument.NodeAliasPath.Contains("In-Your-Community") ? " - community":"" %>" data-filter="on"  data-action="search" ><span><%# ResourceStringHelper.GetString("CRC.IYC.FindLocalBranch", defaultText:"Find Local Branch") %></span></a>
                <asp:Button runat="server" ID="btnSearch" CssClass="iyc-filter-button-submit" Text="Search" Style="display: none;" />
            </fieldset>
        </div>

    </div>
    <asp:HiddenField runat="server" ID="hidLatitude" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidLongitude" ClientIDMode="Static" />
</div>
<span class="location-error error" style="display: none;"><%# ResourceStringHelper.GetString("CRC.IYC.InvalidAddress",defaultText:"Please enter a valid City or Postal Code") %></span>
