﻿using System;
using System.Linq;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSApp.CRC.webparts.InYourCommunity
{
    public partial class FindLocalServicesFilter : CMSAbstractWebPart
    {
        private readonly ServiceCategoryProvider _provider = new ServiceCategoryProvider();

        private string _currentProvinceUrl;
        public string ResultUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_currentProvinceUrl))
                {
                    var currentProvince = new ProvinceProvider().CurrentProvince;
                    if (currentProvince != null)
                        _currentProvinceUrl = currentProvince.GetStringValue("FindServicesResultPage", string.Empty);
                }

                return _currentProvinceUrl;
            }
            set
            {
                SetValue("ResultUrl", value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnSearch.Click += BtnSearchClick;
            TransformationHelper.RegisterStartUpScript(String.Format("<script type='text/javascript' src='{0}'></script>", Variables.GoogleMapsAPIUrl), Page, "googlemaps", false);
            TransformationHelper.RegisterStartUpScript("<script type='text/javascript' src='/CMSPages/GetResource.ashx?scriptfile=/crc/js/iyc.js'></script>", Page, "iyc.js", false);
            TransformationHelper.RegisterStartUpScript("document.addEventListener('DOMContentLoaded', function() { iyc.init(); });", Page, "iycInit");

            BindCategories();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            BindSubCategories();
            ResetValuesAfterRedirect();
            DataBind();
        }

        private void ResetValuesAfterRedirect()
        {
            if (IsPostBack) return;

            var category = QueryHelper.GetString("category", string.Empty);
            var subCategory = QueryHelper.GetString("subcategory", string.Empty);

            if (string.IsNullOrWhiteSpace(category) && CurrentDocument.ClassName.Equals("CRC.IYCCategory", StringComparison.OrdinalIgnoreCase))
                category = CurrentDocument.DocumentGUID.ToString();

            if (string.IsNullOrWhiteSpace(category) && CurrentDocument.ClassName.Equals("CRC.IYCSubCategory", StringComparison.OrdinalIgnoreCase))
                category = CurrentDocument.Parent.DocumentGUID.ToString();

            if (string.IsNullOrWhiteSpace(subCategory) && CurrentDocument.ClassName.Equals("CRC.IYCSubCategory", StringComparison.OrdinalIgnoreCase))
                subCategory = CurrentDocument.DocumentGUID.ToString();

            ddlCategories.SelectedIndex = ddlCategories.Items.IndexOf(ddlCategories.Items.FindByValue(category ?? string.Empty));
            ddlSubCategories.SelectedIndex = ddlSubCategories.Items.IndexOf(ddlSubCategories.Items.FindByValue(subCategory ?? string.Empty));
            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("location", string.Empty)))
            {
                txtLocation.Text = QueryHelper.GetString("location", string.Empty);
            }
        }

        private void BindSubCategories()
        {
            var categoryGuid = QueryHelper.GetGuid("category", Guid.Empty);
            if (categoryGuid == Guid.Empty && CurrentDocument.ClassName.Equals("CRC.IYCSubCategory", StringComparison.OrdinalIgnoreCase))
                categoryGuid = CurrentDocument.Parent.DocumentGUID;

            if (categoryGuid == Guid.Empty && CurrentDocument.ClassName.Equals("CRC.IYCCategory", StringComparison.OrdinalIgnoreCase))
                categoryGuid = CurrentDocument.DocumentGUID;

            if (categoryGuid == Guid.Empty)
                return;

            var items = _provider.GetSubCategories(categoryGuid).OrderBy(item => item.NodeOrder);

            ddlSubCategories.DataSource = items;
            ddlSubCategories.DataTextField = "Name";
            ddlSubCategories.DataValueField = "DocumentGuid";
            ddlSubCategories.DataBind();
            ddlSubCategories.SelectedIndex = ddlSubCategories.Items.IndexOf(ddlSubCategories.Items.FindByValue(Request.Form[ddlSubCategories.UniqueID] ?? string.Empty));
        }

        private void BindCategories()
        {
            var path = _provider.ProvinceNode == null
                           ? "/%"
                           : string.Format("{0}/%", _provider.ProvinceNode.NodeAliasPath);

            var items = _provider.GetCategories(path).OrderBy(item => item.NodeOrder).ToList();
            items.Insert(0, new PageMenu() { Name = ResourceStringHelper.GetString("CRC.IYC.SelectServiceType", defaultText: "Select Service Type"), DocumentGuid = Guid.Empty });
            ddlCategories.DataSource = items;
            ddlCategories.DataTextField = "Name";
            ddlCategories.DataValueField = "DocumentGuid";
            ddlCategories.DataBind();
        }

        private void BtnSearchClick(object sender, EventArgs e)
        {
            var url = string.IsNullOrWhiteSpace(ResultUrl) ? RequestContext.CurrentURL : ResultUrl;
            if (url.Contains("{0}"))
                url = string.Format(url, new ServiceCategoryProvider().ProvinceNode.NodeAliasPath);

            if (!url.Equals(RequestContext.CurrentURL, StringComparison.OrdinalIgnoreCase))
            {
                var langNode = TreeHelper.GetDocument(CurrentSiteName, url, LocalizationContext.PreferredCultureCode, false, null, true);
                if (langNode != null)
                    url = DocumentURLProvider.GetUrl(langNode.NodeAliasPath, langNode.DocumentUrlPath);
            }

            url = URLHelper.RemoveParametersFromUrl(url, "lat", "long", "location", "category", "subcategory");

            if (!string.IsNullOrWhiteSpace(hidLatitude.Value))
                url = URLHelper.AddParameterToUrl(url, "lat", hidLatitude.Value);

            if (!string.IsNullOrWhiteSpace(hidLongitude.Value))
                url = URLHelper.AddParameterToUrl(url, "long", hidLongitude.Value);

            if (!string.IsNullOrWhiteSpace(txtLocation.Text))
                url = URLHelper.AddParameterToUrl(url, "location", txtLocation.Text);

            if (!string.IsNullOrWhiteSpace(ddlCategories.SelectedValue) && ValidationHelper.GetGuid(Request.Form[ddlCategories.UniqueID], Guid.Empty) != Guid.Empty)
                url = URLHelper.AddParameterToUrl(url, "category", ddlCategories.SelectedValue);

            if (!string.IsNullOrWhiteSpace(Request.Form[ddlSubCategories.UniqueID]) && ValidationHelper.GetGuid(Request.Form[ddlSubCategories.UniqueID], Guid.Empty) != Guid.Empty)
                url = URLHelper.AddParameterToUrl(url, "subcategory", Request.Form[ddlSubCategories.UniqueID]);

            if (!url.ToLower().Contains("#iyc-filter"))
                url += "#iyc-filter";

            Response.Redirect(url, true);
        }
    }
}