﻿using System;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSApp.CRC.webparts.InYourCommunity
{
    public partial class FindLocalBranchesFilter : CMSAbstractWebPart
    {
        public string ResultUrl
        {
            get { return ValidationHelper.GetString(GetValue("ResultUrl"), string.Empty); }
            set { SetValue("ResultUrl", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnSearch.Click += BtnSearchClick;
            TransformationHelper.RegisterStartUpScript(String.Format("<script type='text/javascript' src='{0}'></script>", Variables.GoogleMapsAPIUrl), Page, "googlemaps", false);
            TransformationHelper.RegisterStartUpScript("<script type='text/javascript' src='/CMSPages/GetResource.ashx?scriptfile=/crc/js/iyc.js'></script>", Page, "iyc.js", false);
            TransformationHelper.RegisterStartUpScript("document.addEventListener('DOMContentLoaded', function() { iyc.init(); });", Page, "iycInit");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ResetValuesAfterRedirect();
            DataBind();
        }

        void BtnSearchClick(object sender, EventArgs e)
        {
            var url = string.IsNullOrWhiteSpace(ResultUrl) ? RequestContext.CurrentURL : ResultUrl;
            if (url.Contains("{0}"))
                url = string.Format(url, new ServiceCategoryProvider().ProvinceNode.NodeAliasPath);

            if (!url.Equals(RequestContext.CurrentURL, StringComparison.OrdinalIgnoreCase))
            {
                var langNode = TreeHelper.GetDocument(CurrentSiteName, url, LocalizationContext.PreferredCultureCode, false, null, true);
                if (langNode != null)
                    url = DocumentURLProvider.GetUrl(langNode.NodeAliasPath, langNode.DocumentUrlPath);
            }

            url = URLHelper.RemoveParametersFromUrl(url, "lat", "long", "location");

            url = URLHelper.AddParameterToUrl(url, "lat", hidLatitude.Value);
            url = URLHelper.AddParameterToUrl(url, "long", hidLongitude.Value);

            if (!string.IsNullOrWhiteSpace(txtLocation.Text))
                url = URLHelper.AddParameterToUrl(url, "location", txtLocation.Text);

            if (!url.ToLower().Contains("#iyc-filter"))
                url += "#iyc-filter";

            Response.Redirect(url, true);
        }

        private void ResetValuesAfterRedirect()
        {
            if (IsPostBack) return;

            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("location", string.Empty)))
            {
                txtLocation.Text = QueryHelper.GetString("location", string.Empty);
            }
        }
    }
}