﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentNews.ascx.cs" Inherits="CMSApp.CRC.webparts.RecentNews" EnableViewState="false" %>
<div class="news">
    <h2><%# Title %></h2>
    <cms:CMSRepeater runat="server" ID="rptNews" DataBindByDefault="False">
    </cms:CMSRepeater>
    <asp:PlaceHolder runat="server" EnableViewState="False" Visible="False" ID="phNoData">
        <%# EmptyResultText %>
    </asp:PlaceHolder>
    <div>
        <cms:LocalizedHyperlink ID="lnkMore" runat="server" Visible="false" CssClass="button" />
    </div>
</div>
