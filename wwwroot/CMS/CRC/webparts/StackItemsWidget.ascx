﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StackItemsWidget.ascx.cs" Inherits="CMSApp.CRC.webparts.StackItemsWidget" EnableViewState="false" %>
<div class="<%# CssClass %>">
    <%# Title %>
    <cms:CMSRepeater runat="server" ID="rptStackItems" DataBindByDefault="False">
    </cms:CMSRepeater>
    <asp:PlaceHolder runat="server" EnableViewState="False" Visible="False" ID="phNoData">
        <%# EmptyResultText %>
    </asp:PlaceHolder>
    <div>
        <cms:LocalizedHyperlink ID="lnkMore" runat="server" Visible="false" CssClass="submit-btn" />
    </div>
</div>
