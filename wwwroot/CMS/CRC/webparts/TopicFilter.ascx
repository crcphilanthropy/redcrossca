<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopicFilter.ascx.cs" Inherits="CMSApp.CRC.webparts.TopicFilter" EnableViewState="false"%>
<asp:Label ID="lblInfo" runat="server" Visible="false" CssClass="InfoLabel" />
<a href="<%#  CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(DocumentListUrl) %>"><%# ResHelper.GetString("CRC.AllTopics") %></a><br/>
<cms:BasicRepeater ID="rptCategoryList" runat="server">
 </cms:BasicRepeater>
<asp:Literal ID="ltlList" runat="server"></asp:Literal>