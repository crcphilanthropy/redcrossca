using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalControls;
using CMSApp.CRC.webparts.Controls;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Localization;
using CMS.DocumentEngine;

namespace CMSApp.CRC.webparts
{
    public partial class CRCNestedList : CMSAbstractWebPart
    {
        public string DocumentTypes
        {
            get { return ValidationHelper.GetString(GetValue("DocumentTypes"), string.Empty); }
            set { SetValue("DocumentTypes", value); }
        }

        public string PathToExpand
        {
            get { return ValidationHelper.GetString(GetValue("PathToExpand"), string.Empty); }
            set { SetValue("PathToExpand", value); }
        }

        public string Path
        {
            get { return ValidationHelper.GetString(GetValue("Path"), string.Empty); }
            set { SetValue("Path", value); }
        }

        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
            set { SetValue("Title", value); }
        }

        public bool ShowOnlyExpandedPath
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowOnlyExpandedPath"), false); }
            set { SetValue("ShowOnlyExpandedPath", value); }
        }

        private string ClassNames
        {
            get
            {
                return
                    DocumentTypes.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                         .Select(item => string.Format("'{0}'", item))
                         .Join(",");
            }
        }

        private DataSet _nodes;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var parameters = new QueryDataParameters { { "@SiteName", SiteContext.CurrentSiteName }, { "@DocumentCulture", LocalizationContext.PreferredCultureCode } };

            //_nodes = ConnectionHelper.ExecuteQuery("CRC.CustomTransformations.LeftNavSelectAll", parameters, string.Empty, "NodeOrder Asc");
            _nodes = new DataQuery("CRC.CustomTransformations.LeftNavSelectAll")
               .Where(null, parameters)
               .Execute();

            var topPath = Path.Replace("/%", string.Empty);
            var topLevelRow =
                _nodes.Tables[0].Rows.Cast<DataRow>()
                                .FirstOrDefault(
                                    row =>
                                    row["NodeAliasPath"].ToString().Equals(topPath, StringComparison.OrdinalIgnoreCase));

            var nodeLevel = 0;
            if (topLevelRow != null)
                nodeLevel = ValidationHelper.GetInteger(topLevelRow["NodeLevel"], 0);

            var dv = _nodes.Tables[0].DefaultView;
            var whereClause = string.Format("(NodeLevel = {0} AND NodeAliasPath LIKE '{1}')", nodeLevel + 1, Path);
            if (ShowOnlyExpandedPath)
                whereClause += string.Format(" AND (NodeAliasPath = '{0}' or DocumentUrlPath = '{0}')", PathToExpand);

            dv.RowFilter = whereClause;

            rptLeftNav.ItemDataBound += RptLeftNavItemDataBound;
            rptLeftNav.DataSource = dv;
            rptLeftNav.DataBind();
        }

        void RptLeftNavItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            var phSubNav = e.Item.FindControl("phSubNav");
            var subNavRepeater = new UserControl().LoadControl("~/CRC/WebParts/Controls/NavTemplate.ascx") as NavTemplate;
            if (phSubNav == null || subNavRepeater == null) return;

            subNavRepeater.NavItemRepeater.ItemDataBound += RptLeftNavItemDataBound;

            var dv = _nodes.Tables[0].DefaultView;
            var whereClause = string.Format("NodeLevel = {0} AND  (NodeAliasPath LIKE '{1}%' OR DocumentUrlPath LIKE '{1}%')", ValidationHelper.GetInteger(DataBinder.Eval(e.Item.DataItem, "NodeLevel"), 0) + 1, DataBinder.Eval(e.Item.DataItem, "NodeAliasPath"));
            dv.RowFilter = whereClause;

            subNavRepeater.NavItemRepeater.DataSource = dv;
            //subNavRepeater.NavItemRepeater.DataBind();
            if (dv.Count > 0)
                phSubNav.Controls.Add(subNavRepeater);

            phSubNav.DataBind();
        }

        private static string CurrentRelativePath
        {
            get
            {
                return LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? RequestContext.CurrentRelativePath.ToLower().Replace("en-canada", "au-canada") : RequestContext.CurrentRelativePath;
            }
        }

        public static string GetActive(string nodeAliasPath, string pathToExpand)
        {
            if (string.Equals(nodeAliasPath, CurrentRelativePath, StringComparison.OrdinalIgnoreCase)
                || CurrentRelativePath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase)
                || string.Equals(nodeAliasPath, pathToExpand, StringComparison.OrdinalIgnoreCase)
                || string.Equals(nodeAliasPath, DocumentContext.CurrentDocument.NodeAliasPath, StringComparison.OrdinalIgnoreCase)
                || DocumentContext.CurrentDocument.NodeAliasPath.StartsWith(nodeAliasPath, StringComparison.OrdinalIgnoreCase))
                return "HighLighted";

            return string.Empty;
        }
    }
}