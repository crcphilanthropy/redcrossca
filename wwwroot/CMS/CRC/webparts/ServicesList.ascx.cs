using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CMS.DocumentEngine;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.Taxonomy;
using CMS.Helpers;
using CMS.Localization;
using CMS.DataEngine;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.WebParts
{
    public partial class ServicesList : CMSAbstractWebPart
    {
        private readonly CacheManager _cacheManager = new CacheManager();
        IEnumerable<TreeNode> _branches = new TreeNodeDataSet();

        public TreeNode Program
        {
            get
            {
                return DocumentContext.CurrentDocument;
            }
        }

        private CategoryInfo _city;
        public CategoryInfo City
        {
            get
            {
                if (_city == null)
                {
                    var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryLevel = 3 AND CategoryName = '{0}'", (Request.QueryString["city"] ?? string.Empty)), null, 1, null);
                    _city = !DataHelper.DataSourceIsEmpty(categories) ? new CategoryInfo(categories.Tables[0].Rows[0]) : new CategoryInfo();
                }

                return _city;
            }
        }

        private CategoryInfo _province;
        public CategoryInfo Province
        {
            get
            {
                if (_province == null)
                {
                    var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryLevel = 2 AND CategoryID = {0}", City.CategoryParentID), null, 1, null);
                    _province = !DataHelper.DataSourceIsEmpty(categories) ? new CategoryInfo(categories.Tables[0].Rows[0]) : new CategoryInfo();
                }

                return _province;
            }
        }

        private ProvinceContext _provinceContext;
        public ProvinceContext ProvinceContext
        {
            get
            {
                return _provinceContext ?? (_provinceContext = GetProvinceContext(Province.CategoryID));
            }
        }

        public ProvinceContext GetProvinceContext(int provinceId)
        {
            return _cacheManager.GetObject<ProvinceContext, int>(string.Format("ProvinceContext_{0}", provinceId), provinceId) ?? new ProvinceContext();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if ((City.CategoryID == 0 && !string.IsNullOrWhiteSpace(Request.QueryString["city"])) || string.IsNullOrWhiteSpace(Request.QueryString["province"]))
                Response.Redirect("/404-page.aspx", true);


            Response.Redirect(string.Format("{0}/{1}", TransformationHelper.GetRegionPath(CurrentDocument.DocumentCulture), Request.QueryString["Province"]), true);

            _cacheManager.OnRetriving += ProvinceContext.CacheManagerOnRetriving;

            if (City.CategoryID > 0 && !DataHelper.DataSourceIsEmpty(ProvinceContext.Branches))
            {
                //_branches = ProvinceContext.Branches.Where(item => ValidationHelper.GetString(item.GetProperty("Cities"), string.Empty).Contains(City.CategoryID.ToString(CultureInfo.InvariantCulture))).ToList();

                var whereClause = string.Format("(DocumentID IN (SELECT DocumentID FROM CMS_DocumentCategory WHERE CategoryID in ({0})))", City.CategoryID);
                var branches = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, true, "CRC.Branch", whereClause, string.Empty, TreeProvider.ALL_LEVELS, true, -1);

                if (!DataHelper.DataSourceIsEmpty(branches))
                    _branches = branches.Items;

                rptS.DataSource = GetServices();
                rptS.DataBind();

                rptC.DataSource = GetCities();
                rptC.DataBind();
            }

            InitLiterals();
        }

        private void InitLiterals()
        {
            litServices.Text = ResHelper.GetString("CRC.Services");
            litCity.Text = ResHelper.LocalizeString(City.CategoryDisplayName);
            litContactHeader.Text = ResHelper.GetString("CRC.ContactLocalBranch");
            litElseWhere.Text = String.Format(ResHelper.GetString("CRC.ElseWhereIn"), TransformationHelper.FormatCategoryDisplayName(Province.CategoryDisplayName, LocalizationContext.PreferredCultureCode));
            litProgramDescription.Text = Program != null ? ValidationHelper.GetString(Program.GetProperty("Description"), string.Empty) : "N/A";

            var branch = DataHelper.DataSourceIsEmpty(_branches) ? null : _branches.FirstOrDefault();
            if (branch != null)
            {
                litBranchContactInfo.Text = ValidationHelper.GetString(branch.GetProperty("ContactInformation"), string.Empty);
                litBranchDescription.Text = ValidationHelper.GetString(branch.GetProperty("Description"), string.Empty);
            }
        }

        private object GetServices()
        {
            if (!_branches.Any())
                return new BindingList<string>();

            var branchGuids = _branches.Select(item => string.Format("fkBranches Like '%{0}%'", item.DocumentGUID));

            var services = ProvinceContext.Services.Tables[0].DefaultView;
            services.RowFilter = string.Format("({0}) AND NodeParentID = {1}", string.Join(" OR ", branchGuids), Program.NodeID);

            return services;
        }

        private object GetCities()
        {
            if (DataHelper.DataSourceIsEmpty(ProvinceContext.Cities))
                return new BindingList<string>();

            var branchGuids = new List<string>();
            ProvinceContext.Services.Where(item => item.Parent.NodeID == DocumentContext.CurrentDocument.NodeID).ToList().ForEach(item => branchGuids.AddRange(ValidationHelper.GetString(item.GetProperty("fkBranches"), string.Empty).Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Distinct()));

            var branchIds = new List<string>();
            ProvinceContext.Branches.Where(item => branchGuids.Contains(item.DocumentGUID.ToString())).ToList().ForEach(item => branchIds.Add(item.DocumentID.ToString()));

            if (!branchIds.Any()) return null;

            return ConnectionHelper.ExecuteQuery("CRC.CustomTransformations.SelectCategories", null, String.Format("dc.DocumentID in({0}) and c.CategoryID not in ({1})", branchIds.Join(","), City.CategoryID), "c.CategoryDisplayName Asc");
        }

        public string GetCityPath(string city)
        {
            return (DocumentContext.CurrentDocument.DocumentUrlPath ?? string.Empty).ToLower().Replace("{province}", Request.QueryString["province"]).Replace("{city}", city);
        }
    }
}