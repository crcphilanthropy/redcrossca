<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesList.ascx.cs" Inherits="CMSApp.CRC.WebParts.ServicesList" EnableViewState="false" %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Import Namespace="CMS.Localization" %>
<h2 class="black">
    <asp:Literal runat="server" ID="litCity" />
</h2>
<asp:Literal runat="server" ID="litBranchDescription"></asp:Literal>
<div class="services">
    <h3 class="black">
        <asp:Literal runat="server" ID="litServices"></asp:Literal></h3>
    <asp:Repeater runat="server" ID="rptS">
        <ItemTemplate>
            <article class="service">
                <h4 class="black"><%# Eval("Title") %></h4>
                <%# Eval("Description") %>
            </article>
        </ItemTemplate>
    </asp:Repeater>
</div>
<section>
    <h3 class="black">
        <asp:Literal runat="server" ID="litContactHeader" /></h3>
    <asp:Literal runat="server" ID="litBranchContactInfo" />
</section>
<div class="elsewhere">
    <h3 class="black">
        <asp:Literal runat="server" ID="litElseWhere" /></h3>
    <p>
        <asp:Literal runat="server" ID="litProgramDescription"></asp:Literal>
    </p>
    <asp:Repeater runat="server" ID="rptC">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <a href="<%# GetCityPath( TransformationHelper.FormatCategoryDisplayName(EvalText("CategoryName"),"en-CA")) %>"><%# TransformationHelper.FormatCategoryDisplayName(EvalText("CategoryDisplayName"),LocalizationContext.PreferredCultureCode) %></a>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>
