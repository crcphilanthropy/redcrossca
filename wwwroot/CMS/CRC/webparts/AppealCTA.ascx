﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AppealCTA.ascx.cs" ViewStateMode="Disabled" Inherits="CMSApp_CRC_webparts_AppealCTA" %>

<asp:Panel ID="DonationsCTAContainer" CssClass="donationCTAContainer donation-CTA hide-for-medium-down sidebar" EnableViewState="false"  runat="server" DefaultButton="BtnDonation">
    <h2 id="H2Title" runat="server" visible="false" enableviewstate="False"></h2>
    <h3 id="H3Title" class="h2" runat="server" visible="false" enableviewstate="False"></h3>
    <h4 id="H4Subtitle" runat="server" visible="false" enableviewstate="False"></h4>

    <asp:Image ID="Image" runat="server" Visible="false" EnableViewState="False" />
    <asp:PlaceHolder ID="plcMonthlyDonationTitle" runat="server" Visible="false">
        <h3 id="h3MonthlyTitle" runat="server" class="monthlyTitle"></h3>

    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcSingleDonationTitle" runat="server" Visible="false">
        <h3 id="h3SingleTitle" runat="server" class="monthlyTitle"></h3>

    </asp:PlaceHolder>
    <ul id="DonationTypeRadioList" runat="server" class="donationOptions unstyled">
        <li>
            <asp:RadioButton ID="RadioSingleDonation" CssClass="RadioSingleDonation" Text="Single donation" GroupName="DonationType"
                runat="server" /></li>
        <li>
            <asp:RadioButton ID="RadioMonthlyDonation" CssClass="RadioMonthlyDonation" Text="Monthly donation" GroupName="DonationType"
                runat="server" />
            <div id="divDescription" class="monthlyDonationDescription" runat="server" visible="False">
            </div>
        </li>
        <li>
            <div id="divErrorMessageDonationType" runat="server" visible="false" enableviewstate="False">
                <asp:Label ID="lblErrorMessageDonationType" CssClass="text-error" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </li>
    </ul>
    <div class="hide">
        <asp:Label ID="MonthlyRadioAmount1" runat="server" CssClass="monthlyRadio1"></asp:Label>
        <asp:Label ID="MonthlyRadioAmount2" runat="server" CssClass="monthlyRadio2"></asp:Label>
        <asp:Label ID="MonthlyRadioAmount3" runat="server" CssClass="monthlyRadio3"></asp:Label>
        <asp:Label ID="SingleRadioAmount1" runat="server" CssClass="singleRadio1"></asp:Label>
        <asp:Label ID="SingleRadioAmount2" runat="server" CssClass="singleRadio2"></asp:Label>
        <asp:Label ID="SingleRadioAmount3" runat="server" CssClass="singleRadio3"></asp:Label>
    </div>
    <ul class="large-block-grid-3">
        <li>
            <asp:RadioButton ID="RadioAmount1" CssClass="radioAmount1" runat="server" GroupName="DonationAmount" Text="$75" /></li>
        <li>
            <asp:RadioButton ID="RadioAmount2" CssClass="radioAmount2" runat="server" GroupName="DonationAmount" Text="$150" /></li>
        <li>
            <asp:RadioButton ID="RadioAmount3" CssClass="radioAmount3" runat="server" GroupName="DonationAmount" Text="$200" /></li>
        <li class="otherAmount">
            <asp:RadioButton ID="RadioAmount4" runat="server" GroupName="DonationAmount" Text="Other" CssClass="donate-widget-other-radio" />
            <span>
                <asp:Literal ID="dollarSignEn" Visible="false" runat="server"><span>$</span></asp:Literal><asp:TextBox ID="TxtOtherAmount" runat="server" GroupName="DonationAmount" CssClass="donate-widget-other"></asp:TextBox>
                <asp:Literal ID="dollarSignFR" Visible="false" runat="server"><span>$</span></asp:Literal></span>

        </li>
    </ul>
    <div id="divErrorMessageEmpty" class="error-block" runat="server" visible="false" enableviewstate="False">
        <asp:Label ID="lblErrorMessageEmpty" CssClass="text-error" runat="server" EnableViewState="False"></asp:Label>
    </div>
    <div class="hide">
        <input type="text" name="appealTelNum1" id="appealTelNum1" class="require"/>
    </div>
    <label class="button">
        <asp:Button ID="BtnDonation" CssClass="donate-track donate-widget-button" Text="Donate Now" runat="server" OnClick="BtnDonationClick" />
    </label>
    <input type="hidden" id="hidDonateBarGAHash" runat="server" class="donate-bar-hash" />
</asp:Panel>
<asp:PlaceHolder ID="donateWidgetGA" runat="server" Visible="true" ViewStateMode="Disabled">
    <script type="text/javascript">
       

        var pageAppealLoadAt = new Date();
        function initAppealTracking() {

            $('.donate-widget-other').keyup(function () {                
                $('#<%= RadioAmount4.ClientID %>').prop('checked', $(this).val().length > 0);
            });

            //executes with JQuery mousedown on button in form 
            $('.donate-widget-button').click(function (event) {
                if (Math.abs(new Date() - pageAppealLoadAt) < 1000) return false;
                if ($('#appealTelNum1').val().length > 0) return false;

                event.preventDefault();
                var donationWidgetValue = "";
                var donationWidgetLabel = "";
                var donationWidgetCycle = "";
                //gathers information on the Donation Cycle. 
                var donationType = $('#<%= RadioSingleDonation.ClientID %>').attr('name');
                var selectedCycle = $("input[type='radio'][name='" + donationType + "']:checked");
                if (selectedCycle.length > 0) {
                    donationWidgetCycle = "One Time";
                } else {
                    donationWidgetCycle = "Monthly";
                }

                //gathers informtion on the label 
                var getInputAmountText = $('.donate-widget-other').val();
                if (getInputAmountText.length > 0 && $('#<%= RadioAmount4.ClientID %>').is(':checked')) {
                    // something was entered in OTher text area.  Ensure it's valid
                    var getAmount = GetValidatedAmount(getInputAmountText);
                    if (getAmount > 0) {
                        donationWidgetLabel = "Entered";
                        donationWidgetValue = Math.round(getAmount);
                        //$('.donate-widget-other').val(getAmount);      // set the textbox value to the "possibly" updated new amount
                    }
                    else {
                        return;
                    }
                } else {
                    donationWidgetLabel = "Selected";
                    var donationAmount = $('#<%= RadioAmount1.ClientID %>').attr('name');
                    var selectedValue = $("input[type='radio'][name='" + donationAmount + "']:checked");
                    if (selectedValue.length > 0) {
                        donationRadio = selectedValue.val();
                        donationWidgetValue = donationRadio;
                    }
                }

                //sends event into GA 
                dataLayer.push({ 'event': 'generic-event', 'eventCategory': '<%= ( this.ParentZone.ID == "ContentZone" | this.ParentZone.ID == "CTAZone" | UseMainContentLayout) ? "Donate Main Form Widget": "Donate Right Rail Widget"  %>', 'eventAction': donationWidgetCycle, 'eventLabel': donationWidgetValue, 'eventValue': donationWidgetValue });
                $('.donate-bar-hash').val(ga.getAll()[0].get('linkerParam'));
                $('.donate-widget-button').off("click");
                setTimeout(function () { // after 1 second, submit the form
                    $('.donate-widget-button').trigger("click");
                }, 1000);
            });
        }
    </script>
</asp:PlaceHolder>