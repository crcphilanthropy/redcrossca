using System;
using System.Text.RegularExpressions;
using System.Web;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.GlobalHelper;
using CMS.IO;
using CMS.PortalControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

using CMSApp.CRC.Entities;
using CMS.Localization;
using CMS.Helpers;
using CMS.DocumentEngine;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts.Admin
{
    public partial class ErrorRedirect : CMSAbstractWebPart
    {
        private readonly CacheManager _cacheManager = new CacheManager { Enabled = true, TimeInMinutes = 60 };

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            CheckMapping(HttpContext.Current);
        }

        /// <summary>
        /// Checks the mapping.
        /// </summary>
        /// <param name="context">The context.</param>
        private void CheckMapping(HttpContext context)
        {
            // target only the filename portion (can be any kind of check, even a simple regex)
            switch (Path.GetFileName(context.Request.Path).ToLowerInvariant())
            {
                // When we encounter one of these three files
                case "article.asp":
                case "print.asp":
                case "main.asp":

                    // attempt to parse out the id from them and redirect to the equivalent Kentico page 

                    // extract oldid from the query portion
                    var mcoll = Regex.Matches(HttpContext.Current.Request.Url.ToString(), @"[&|?]id=(?<id>\d+)");
                    if (mcoll.Count != 0)
                    {
                        int oldId;
                        if (int.TryParse(mcoll[0].Groups["id"].Value, out oldId)) // protect against too large of a number
                        {
                            // extract the QueryString values removing the id parameter
                            var newQueryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.Url.Query);
                            newQueryString.Remove("id");
                            newQueryString.Remove("tid"); // Issue CRC_PostLaunch-47: http://bugs.ecentricarts.com:8080/issue/CRC_PostLaunch-47

                            // check cache 
                            // - if already in cache then redirect to cached target url
                            // - else if not in cache yet then look it up in the db custom table and put in cache
                            var mapping = _cacheManager.GetObject<UrlMapping>(string.Format("UrlMapping_{0}", oldId));

                            if (mapping == null)
                            {
                                mapping = GetCustomTableItem(oldId);

                                if (mapping != null)
                                {
                                    // Add this item to the cache
                                    _cacheManager.AddItemToCache(string.Format("UrlMapping_{0}", oldId), mapping);
                                }
                            }

                            if (mapping != null)
                            {
                                var queryString = newQueryString.ToString();

                                // We need to check if the TargetUrl already has query string parameters
                                var site = SiteInfoProvider.GetSiteInfo(SiteContext.CurrentSiteID);

                                if (site != null)
                                {
                                    var uri = new Uri(HttpContext.Current.Request.Url.Scheme + "://" + site.DomainName + mapping.TargetUrl.TrimStart(new[] { '~' }));

                                    var targetQueryString = HttpUtility.ParseQueryString(uri.Query);

                                    queryString = !string.IsNullOrEmpty(targetQueryString.ToString())
                                                      ? (!string.IsNullOrEmpty(queryString) ? "&" + queryString : string.Empty)
                                                      : (!string.IsNullOrEmpty(queryString) ? "?" + queryString : string.Empty);

                                    // Set the status code so that this appears as a 301 redirect
                                    context.Response.StatusCode = 301;


                                    // redirect to new page...
                                    HttpContext.Current.Response.RedirectPermanent((uri + queryString).ToLower());
                                }
                            }
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Gets the custom table item.
        /// </summary>
        /// <param name="sourceId">The source id.</param>
        /// <returns></returns>
        private UrlMapping GetCustomTableItem(int sourceId)
        {
            var parameters = new QueryDataParameters
            {
                new DataParameter("sourceId", sourceId),
                new DataParameter("culture", LocalizationContext.PreferredCultureCode)
            };

            // The target url in the database may have changed in Kentico since the import or this could be a dynamic url
            // so we need to try to get the alias path and url path using the source id. If these values are empty then 
            // we can use the target url that is in the database.
            var ds = ConnectionHelper.ExecuteQuery("crc_GetUrlMapping", parameters, QueryTypeEnum.StoredProcedure, false);

            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                var aliasPath = ValidationHelper.GetString(ds.Tables[0].Rows[0]["NodeAliasPath"], string.Empty);
                var urlPath = ValidationHelper.GetString(ds.Tables[0].Rows[0]["DocumentUrlPath"], string.Empty);

                var targetUrl = string.Empty;
                if (!string.IsNullOrEmpty(aliasPath) && !string.IsNullOrEmpty(urlPath))
                     targetUrl = DocumentURLProvider.GetUrl(aliasPath, urlPath);

                if (string.IsNullOrEmpty(targetUrl))
                    targetUrl = ValidationHelper.GetString(ds.Tables[0].Rows[0]["TargetUrl"], string.Empty);

                // Construct a URL Mapping object
                var mapping = new UrlMapping
                {
                    SourceId = ValidationHelper.GetInteger(ds.Tables[0].Rows[0]["SourceId"], 0),
                    TargetUrl = targetUrl,
                    LangCode = ValidationHelper.GetString(ds.Tables[0].Rows[0]["LangCode"], string.Empty)
                };

                return mapping;
            }

            return null;
        }
    }
}