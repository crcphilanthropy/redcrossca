using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

using CMSApp.CRC.Entities;

using LinqToExcel;
using LinqToExcel.Domain;
using NLog;
using OfficeOpenXml;
using CMS.Helpers;
using CMS.CustomTables;
using CMS.Membership;

namespace CMSApp.CRC.webparts.Admin
{
    public partial class URLImport : CMSAbstractWebPart
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static Logger _log;

        /// <summary>
        /// Gets the logger.
        /// </summary>
        /// <value>
        /// The logger.
        /// </value>
        internal static Logger Logger
        {
            get { return _log ?? (_log = LogManager.GetCurrentClassLogger()); }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Handles the Click event of the btnImport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload1.HasFile)
                {
                    var file = FileUpload1.PostedFile;

                    var extension = Path.GetExtension(file.FileName).ToLower();

                    if (string.Equals(extension, ".xlsx"))
                    {
                        var now = DateTime.Now;
                        const string path = "~/CRC/files/import";

                        // Save the file on the server
                        var filename = Server.MapPath(string.Format("{0}/301 URL Mappings {1:yyyy-MM-dd hhmmss}.xlsx", path, now));

                        FileUpload1.PostedFile.SaveAs(filename);

                        // Open the file and read it's contents
                        var excel = new ExcelQueryFactory(filename) {DatabaseEngine = DatabaseEngine.Ace};
                        excel.AddMapping<ImportExcel>(u => u.OldUrl, "CURRENT SITE PAGE ID");
                        excel.AddMapping<ImportExcel>(u => u.NewUrl, "NEW SITE URL");

                        // Get a list of all mappings
                        var mappings = from m in excel.Worksheet<ImportExcel>("Mappings")
                                       select m;

                        if (mappings.Count() > 0)
                        {
                            // Create a list to store any of the dynamic urls
                            var dynamicUrls = new List<ImportExcel>();

                            // Create a list to store any failures
                            var failures = new List<ImportExcel>();

                            var tree = new TreeProvider();

                            var language = rblLanguage.SelectedValue;

                            // Regular expression to extract the id parameter from the old url query string
                            var regex = new Regex(@"[&|?]id=(?<id>\d+)", RegexOptions.IgnoreCase);
                            
                            foreach (var map in mappings)
                            {
                                try
                                {
                                    // Make sure that both properties are set
                                    if (!string.IsNullOrEmpty(map.OldUrl) && !string.IsNullOrEmpty(map.NewUrl))
                                    {
                                        // First make sure that the OldUrl property contains either article.asp, print.asp or main.asp
                                        var page = Path.GetFileName(map.OldUrl).ToLowerInvariant();

                                        if (page.Contains("article.asp") || page.Contains("print.asp") ||
                                            page.Contains("main.asp"))
                                        {
                                            var match = regex.Match(map.OldUrl);

                                            if (match.Success)
                                            {
                                                // Use the regular expression to get the source id
                                                var source = match.Groups[1].Value;

                                                // Make sure that the source id is an integer
                                                var sourceId = 0;

                                                if (int.TryParse(source, out sourceId))
                                                {
                                                    // Now use the NewUrl property to get the document id of the new page in Kentico. This will be set as the TargetId in the custom table
                                                    var newUrl = map.NewUrl;
                                                    if (!newUrl.Contains("://"))
                                                        newUrl = "http://" + newUrl;
                                                    var uri = new Uri(newUrl);

                                                    string url;
                                                    if (uri.Host.ToLower().Contains("redcross.ca") ||
                                                        (uri.Host.ToLower().Contains("croixrouge.ca")))
                                                    {
                                                        url = newUrl.Replace(uri.Scheme + "://", string.Empty).Replace(uri.Host, string.Empty);
                                                    }
                                                    else
                                                    {
                                                        url = newUrl;
                                                    }

                                                    // Use the alias to get the document
                                                    var document = DocumentHelper.GetDocument(SiteContext.CurrentSiteName,
                                                                                              url, language,
                                                                                              false, string.Empty,
                                                                                              string.Empty, string.Empty, -1,
                                                                                              false, string.Empty, tree);

                                                    int? targetId = null;
                                                    if (document != null)
                                                    {
                                                        targetId = document.DocumentID;
                                                    }
                                                    else
                                                    {
                                                        // Querying the NodeAliasPath returns null so try the document alias table
                                                        var parameters = new QueryDataParameters
                                                        {
                                                            new DataParameter("aliasUrl", url),
                                                            new DataParameter("culture", language)
                                                        };

                                                        var ds = ConnectionHelper.ExecuteQuery("crc_GetDocumentUsingAlias", parameters, QueryTypeEnum.StoredProcedure, false);

                                                        if (!DataHelper.DataSourceIsEmpty(ds))
                                                        {
                                                            targetId = int.Parse(ds.Tables[0].Rows[0]["DocumentID"].ToString());
                                                        }
                                                        else
                                                        {
                                                            // Querying the document alias table returns null so try the DocumentURLPath
                                                            ds = ConnectionHelper.ExecuteQuery("CMS.Document.selectdocuments", null, string.Format("((NodeSiteID = {0}) AND (DocumentCulture = N'{1}')) AND (DocumentUrlPath = N'{2}')", SiteContext.CurrentSiteID, language, url), string.Empty, 1, "DocumentID");

                                                            if (!DataHelper.DataSourceIsEmpty(ds))
                                                            {
                                                                targetId = int.Parse(ds.Tables[0].Rows[0]["DocumentID"].ToString());
                                                            }
                                                            else
                                                            {
                                                                // Log this case - the new url does not exist in Kentico i.e. this is most likely a dynamic url
                                                                dynamicUrls.Add(map);
                                                            }
                                                        }
                                                    }

                                                    // We now have the Source ID, Target ID (might be null), Target URL and language. A record can now be created in the custom table
                                                    if (CreateCustomTableItem(sourceId, targetId, url, language))
                                                    {
                                                        // Log this case - Success
                                                        LogInfo(string.Format("URL Mapping custom table item added:- Source ID: {0} | Target Id: {1} | Target URL: {2} | Language: {3}", sourceId, targetId, map.NewUrl, language));
                                                    }
                                                    else
                                                    {
                                                        // Log this case - Error adding the custom table value
                                                        LogInfo(string.Format("***** Error Adding custom table item:- Source ID: {0} | Target Id: {1} | Target URL: {2} | Language: {3} *****", sourceId, targetId, map.NewUrl, language));
                                                    }
                                                }
                                                else
                                                {
                                                    // Log this case - id parameter is not an integer
                                                    failures.Add(map);
                                                }
                                            }
                                            else
                                            {
                                                // Log this case - No id parameter
                                                failures.Add(map);
                                            }
                                        }
                                        else
                                        {
                                            // Log this case - old url doesn't contain either main.asp, article.asp or print.asp
                                            failures.Add(map);
                                        }
                                    }
                                    else
                                    {
                                        // Log this case - One of the parameters missing in the spreadsheet
                                        failures.Add(map);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogError(ex);
                                    throw;
                                }
                            }

                            if (dynamicUrls.Count > 0)
                            {
                                var name = string.Format("{0}/logs/Dynamic URLs ({1}) {2:yyyy-MM-dd_hh-mm-ss}.xlsx",
                                                         path, string.Equals(language, "en-CA") ? "English" : "French", now);
                                var dynamicFilename = Server.MapPath(name);
                                SaveExcelFile(dynamicFilename, dynamicUrls, "Dynamic URLs");

                                ResultPanel.Visible = true;
                                lnkDynamicUrls.Visible = true;
                                lnkDynamicUrls.NavigateUrl = name;
                            }

                            if (failures.Count > 0)
                            {
                                var name = string.Format("{0}/logs/Failures ({1}) {2:yyyy-MM-dd_hh-mm-ss}.xlsx", path,
                                                         string.Equals(language, "en-CA") ? "English" : "French", now);
                                var failuresFilename = Server.MapPath(name);
                                SaveExcelFile(failuresFilename, failures, "Failures");

                                ResultPanel.Visible = true;
                                lnkFailures.Visible = true;
                                lnkFailures.NavigateUrl = name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);

                throw;
            }
        }

        /// <summary>
        /// Saves the excel file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="list">The list.</param>
        /// <param name="sheetname">The sheetname.</param>
        private void SaveExcelFile(string filename, IEnumerable<ImportExcel> list, string sheetname)
        {
            var newFile = new FileInfo(filename);

            using (var xlPackage = new ExcelPackage(newFile))
            {
                var worksheet = xlPackage.Workbook.Worksheets.Add(sheetname);

                var dataSet = ToDataSet(list);

                if (dataSet != null)
                {
                    if (dataSet.Tables.Count > 0)
                    {
                        dataSet.Tables[0].Columns[0].Caption = "CURRENT SITE PAGE ID";
                        dataSet.Tables[0].Columns[1].Caption = "NEW SITE URL";

                        worksheet.Cells["A1"].LoadFromDataTable(dataSet.Tables[0], true);

                        xlPackage.Save();
                    }
                }
            }
        }

        /// <summary>
        /// Creates the custom table item.
        /// </summary>
        /// <param name="sourceId">The source id.</param>
        /// <param name="targetId">The target id.</param>
        /// <param name="targetUrl">The target URL.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        private bool CreateCustomTableItem(int sourceId, int? targetId, string targetUrl, string language)
        {
            
            // Prepare the parameters
            const string tableClassName = "CRC.UrlMapping";

            // Checks if Custom table 'Sample table' exists
            var customTable = DataClassInfoProvider.GetDataClassInfo(tableClassName);
            if (customTable != null)
            {
                // Creates new custom table item
                var newCustomTableItem = CustomTableItem.New(tableClassName);

                // Sets the ItemText field value
                newCustomTableItem.SetValue("SourceId", sourceId);
                newCustomTableItem.SetValue("TargetId", targetId);
                newCustomTableItem.SetValue("TargetUrl", targetUrl);
                newCustomTableItem.SetValue("LangCode", language);

                // Inserts the custom table item into database
                newCustomTableItem.Insert();

                return true;
            }

            return false;
        }

        /// <summary>
        /// To the data set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        private DataSet ToDataSet<T>(IEnumerable<T> list)
        {
            var elementType = typeof(T);
            var ds = new DataSet();
            var t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                var colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, colType);
            }

            //go through each property on T and add each value to the table
            foreach (var item in list)
            {
                var row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="ex">The ex.</param>
        private void LogError(Exception ex)
        {
            if (ex.InnerException != null)
            {
                Logger.Error(GetExceptionMsgStack(ex.InnerException), ex.InnerException);
            }
            else
            {
                Logger.Error(GetExceptionMsgStack(ex), ex);
            }
        }

        /// <summary>
        /// Logs the info.
        /// </summary>
        /// <param name="message">The message.</param>
        private void LogInfo(string message)
        {
            Logger.Info(message);
        }

        /// <summary>
        /// Gets the entire stack of error messages
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetExceptionMsgStack(Exception ex)
        {
            return ex.InnerException == null ? ex.Message : string.Format("{0} - ({1})", ex.Message, GetExceptionMsgStack(ex.InnerException));
        }
    }
}