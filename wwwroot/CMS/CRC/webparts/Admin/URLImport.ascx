﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="URLImport.ascx.cs" Inherits="CMSApp.CRC.webparts.Admin.URLImport" %>

<b>Please Select File:</b>
<asp:FileUpload ID="FileUpload1" runat="server" />

<br/><br/>

<b>Please Select Language:</b>
<asp:RadioButtonList ID="rblLanguage" runat="server">
    <asp:ListItem Text="English" Value="en-CA" Selected="True"></asp:ListItem>
    <asp:ListItem Text="French" Value="fr-CA"></asp:ListItem>
</asp:RadioButtonList>

<br/>

<asp:Button ID="btnImport" runat="server" Text="Import" OnClick="btnImport_Click" OnClientClick="ShowLoader();" ClientIDMode="Static" />

<br/>

<div id="loading">
    <asp:Image ID="imgWaitIcon" runat="server" ImageAlign="Left" ImageUrl="~/CRC/images/loader.gif" ClientIDMode="Static" />
</div>

<br/>

<asp:Panel ID="ResultPanel" runat="server" Visible="False" ClientIDMode="Static">
    <b>The import was successful. Please use the links below to download the generated file(s)</b><br/><br/>
    <asp:HyperLink ID="lnkDynamicUrls" runat="server" Visible="False"><img src="/CRC/images/icon_excel.png" alt="Excel" />&nbsp;Dynamic URLs >></asp:HyperLink>
    <br/>
    <asp:HyperLink ID="lnkFailures" runat="server" Visible="False"><img src="/CRC/images/icon_excel.png" alt="Excel" />&nbsp;Failures >></asp:HyperLink>
</asp:Panel>

<script type="text/javascript">
    function ShowLoader() {
        $j('#loading').show();
        $j('#ResultPanel').hide();

        setTimeout("$j(\"#btnImport\").attr(\"disabled\", \"true\");", 1000);
    }
    
    function pageLoad(sender, args) {
        $j('#loading').hide();
    }

</script>