﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS.CustomTables;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using Ecentricarts.GoogleSiteSearch.Model;

namespace CMSApp.CRC.webparts
{
    public partial class GoogleSearchResults : CMSAbstractWebPart
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            BindDropDowns();

            ddlFilter.SelectedIndexChanged += DdlFilterSelectedIndexChanged;

            ddlFilter.SelectedIndex = ddlFilter.Items.IndexOf(ddlFilter.Items.FindByValue(Filter));

            if (Session["SearchText"] as string != SearchText)
                Filter = null;

            Session["SearchText"] = SearchText;
        }


        private void BindDropDowns()
        {
            var list = new Dictionary<string, string>
                {
                    {string.Empty, ResHelper.GetString("CRC.All")},
                    {"general", ResHelper.GetString("CRC.General")},
                    {"pdf", ResHelper.GetString("CRC.PDF")},
                    {"doc", ResHelper.GetString("CRC.WordOrExcel")},
                    {"ppt", ResHelper.GetString("CRC.PowerPoint")},
                    {"blog", ResHelper.GetString("CRC.FromTheBlogSearch")}
                };


            ddlFilter.DataSource = list;
            ddlFilter.DataValueField = "Key";
            ddlFilter.DataTextField = "Value";
            ddlFilter.DataBind();
        }

        void DdlFilterSelectedIndexChanged(object sender, EventArgs e)
        {
            Filter = ddlFilter.SelectedValue;
        }


        /// <summary>
        ///  On page prerender.
        /// </summary>
        /// <param name="e">Event argument</param>
        protected override void OnPreRender(EventArgs e)
        {
            //Search();
            long totalItems = 0;
            GoogleSearch(out totalItems);
            BindRelevantLinks();
            litTotalItems.Text = string.Format(ResHelper.GetString("CRC.Pager.ResultCount"), Server.HtmlEncode(SearchText).Substring(0, Math.Min(Server.HtmlEncode(SearchText).Length, 200)), totalItems);
            litTotalItems.Visible = !string.IsNullOrWhiteSpace(SearchText);
            base.OnPreRender(e);
        }

        private void GoogleSearch(out long totalItems)
        {
            var fileType = string.Empty;
            var query = SearchText;
            var siteSearch = LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase)
                                 ? "www.croixrouge.ca"
                                 : "www.redcross.ca";

            switch (ddlFilter.SelectedValue.ToLower())
            {
                case "blog":
                    siteSearch = LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase)
                                 ? "www.croixrouge.ca/blogue"
                                 : "www.redcross.ca/blog";
                    break;
                case "pdf":
                    fileType = "pdf";
                    break;
                case "doc":
                    query = string.Format("{0} filetype:doc OR filetype:docx OR filetype:xls OR filetype:xlsx", query);
                    break;
                case "ppt":
                    query = string.Format("{0} filetype:ppt OR filetype:pptx", query);
                    break;
                case "general":
                    query = string.Format("{0} -filetype:doc -filetype:docx -filetype:xls -filetype:xlsx -filetype:ppt -filetype:pptx", query);
                    break;

            }

            var data = new CMSAppAppCode.Old_App_Code.CRC.GoogleCustomSearch(10).Search(query, CRCPager.CurrentPage - 1, string.Format("lang_{0}", LocalizationContext.PreferredCultureCode.Substring(0, 2)), siteSearch, fileType);
            rptGoogleSearch.DataSource = data.Items;
            rptGoogleSearch.DataBind();

            totalItems = data.SearchInformation != null && data.SearchInformation.TotalResults.HasValue ? data.SearchInformation.TotalResults.Value : 0;

            CRCPager.DataBindWithoutDataSource(totalItems);
        }

        private void BindRelevantLinks()
        {
            if (string.IsNullOrWhiteSpace(SearchText)) return;

            var where = string.Join(" or ", SearchText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(item =>
                    string.Format("Tags like '% {0} %' OR Tags like '%\"{0}\"%' OR Tags like '% {0}' OR Tags like '{0} %' OR Tags = '{0}' OR Tags like '{0}, %' OR Tags like '% {0}, %' OR Tags like '% {0}\", %' OR Tags like '% \"{0} %'", item.Replace("'", "''"))
                    )
                );

            var relLinks = CustomTableItemProvider.GetItems("CRC.RelevantLinks", String.Format("CultureCode = '{0}' and ({1})", LocalizationContext.PreferredCultureCode, where), "ItemOrder ASC");
            rptRelevantLinks.DataSource = relLinks.Tables[0];
            rptRelevantLinks.DataBind();
            rptRelevantLinks.Visible = relLinks.Tables.Count > 0 && relLinks.Tables[0].Rows.Count > 0;
        }

        public string Filter
        {
            get { return Session["SearchFilter"] as string; }
            set { Session["SearchFilter"] = value; }
        }

        public string SearchText
        {
            get
            {
                return QueryHelper.GetString("searchtext", "");
            }
        }

        protected string GetSearchMetaTag(object dataItem, string tagname, string defaultValue)
        {
            var searchItem = dataItem as Result;
            if (searchItem == null || searchItem.Pagemap == null || !searchItem.Pagemap.ContainsKey("metatags")) return string.Empty;
            var metaTags = searchItem.Pagemap["metatags"][0];
            return metaTags != null && metaTags.ContainsKey(tagname) ? metaTags[tagname] as string : defaultValue;
        }
    }
}