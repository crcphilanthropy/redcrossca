
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.PortalControls;
using CMS.Synchronization;
using System;
using System.Linq;
using CMS.SiteProvider;

public partial class crc_webparts_RemoveHistory : CMSAbstractWebPart
{
    #region "Properties"



    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            var user = MembershipContext.AuthenticatedUser;
            if (!user.IsGlobalAdministrator)
            {
                // This page should only be accessible to global admins
                RequestHelper.Respond404();
                return;
            }
        }
    }
    private void DeleteDocumentHistory(UserInfo user)
    {
        // Get ids of all documents that have version information
        var docQuery = new DataQuery()
            .From("CMS_VersionHistory")
            .Column("DocumentID")
            .Distinct();

        var docIds = docQuery
            .Select(row => int.Parse(row[0].ToString()))
            .ToArray();

        // Use the version manager to destroy document history
        var tree = new TreeProvider(user);
        var versionManager = VersionManager.GetInstance(tree);

        var total = docIds.Length;

        for (var x = 0; x < total; x++)
        {
            if (x % 100 == 0)
            {
                EventLogProvider.LogEvent(EventType.INFORMATION, "[REMOVE HISTORY] Documents", "REMOVEHISTORY", string.Format("Removing Document History {0} out of {1}", x, total));
            }
            versionManager.DeleteOlderVersions(docIds[x],SiteContext.CurrentSiteName);
        }
    }

    private void DeleteObjectHistory(UserInfo user)
    {
        // Get ids and types of all objects that have version information
        var objectQuery = new DataQuery()
            .From("CMS_ObjectVersionHistory")
            .Columns("VersionObjectType", "VersionObjectID")
            .Distinct();

        var objectVersions = objectQuery
            .Select(row => new
            {
                ObjectType = row[0].ToString(),
                ObjectId = int.Parse(row[1].ToString())
            })
            .ToArray();

        var total = objectVersions.Length;

        for (var x = 0; x < total; x++)
        {
            var objectVersion = objectVersions[x];
            if (x % 100 == 0)
            {
                EventLogProvider.LogEvent(EventType.INFORMATION, "[REMOVE HISTORY] Objects", "REMOVEHISTORY", string.Format("Removing Object History {0} out of {1}", x, total));
            }

            ObjectVersionManager.DeleteOlderVersions(objectVersion.ObjectType, objectVersion.ObjectId,SiteContext.CurrentSiteName);
        }

    }

    protected void DeleteDocumentHistory_Click(object sender, EventArgs e)
    {
        var user = MembershipContext.AuthenticatedUser;
        DeleteDocumentHistory(user);
    }

    protected void DeleteObjectHistory_Click(object sender, EventArgs e)
    {
        var user = MembershipContext.AuthenticatedUser;
        DeleteObjectHistory(user);
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



