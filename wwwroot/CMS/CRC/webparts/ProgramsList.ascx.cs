using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.Taxonomy;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.DataEngine;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.WebParts
{
    public partial class ProgramsList : CMSAbstractWebPart
    {
        private readonly CacheManager _cacheManager = new CacheManager();

        private CategoryInfo _province;
        public CategoryInfo Province
        {
            get
            {
                if (_province == null)
                {
                    var categories = CategoryInfoProvider.GetCategories(string.Format("CategoryLevel = 2 AND CategoryID IN (SELECT CategoryID FROM CMS_DocumentCategory WHERE DocumentID = {0})", DocumentContext.CurrentDocument.DocumentID), null, 1, null);
                    _province = !DataHelper.DataSourceIsEmpty(categories) ? new CategoryInfo(categories.Tables[0].Rows[0]) : new CategoryInfo();
                }

                return _province;
            }
        }

        private ProvinceContext _provinceContext = null;
        public ProvinceContext ProvinceContext
        {
            get
            {
                return _provinceContext ?? (_provinceContext = GetProvinceContext(Province.CategoryID));
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _cacheManager.OnRetriving += ProvinceContext.CacheManagerOnRetriving;

            if (Province.CategoryID != 0 && !DataHelper.IsEmpty(ProvinceContext.Programs) && ProvinceContext.Programs.Tables.Count > 0)
            {
                rptP.ItemDataBound += RptPItemDataBound;
                rptP.DataSource = ProvinceContext.Programs;
                rptP.DataBind();
            }

            litTitle.Text = String.Format(ResHelper.GetString("CRC.ProvinceProgramsAndServices"), TransformationHelper.FormatCategoryDisplayName(Province.CategoryDisplayName, LocalizationContext.PreferredCultureCode));
        }

        void RptPItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var rptC = e.Item.FindControl("rptC") as Repeater;
            if (rptC == null) return;

            var cities = GetCities(ValidationHelper.GetGuid(DataBinder.Eval(e.Item.DataItem, "DocumentGUID"), Guid.Empty)) as IEnumerable<DataRow>;
            e.Item.Visible = cities != null && cities.Any();
            rptC.DataSource = cities;
            rptC.DataBind();
        }

        private object GetCities(Guid documentGuid)
        {

            if (DataHelper.DataSourceIsEmpty(ProvinceContext.Branches) || DataHelper.DataSourceIsEmpty(ProvinceContext.Cities) || DataHelper.DataSourceIsEmpty(ProvinceContext.Services))
                return new BindingList<String>();

            var branchGuids = new List<string>();
            ProvinceContext.Services.Where(item => item.Parent.DocumentGUID == documentGuid).ToList().ForEach(item => branchGuids.AddRange(ValidationHelper.GetString(item.GetProperty("fkBranches"), string.Empty).Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Distinct()));

            var branchIds = new List<string>();
            ProvinceContext.Branches.Where(item => branchGuids.Contains(item.DocumentGUID.ToString())).ToList().ForEach(item => branchIds.Add(item.DocumentID.ToString()));

            if (!branchIds.Any()) return null;

            var toReturn = ConnectionHelper.ExecuteQuery("CRC.CustomTransformations.SelectCategories", null, String.Format("dc.DocumentID in({0})", branchIds.Join(",")), "c.CategoryDisplayName Asc");

            return DataHelper.DataSourceIsEmpty(toReturn) ? null : toReturn.Tables[0].Rows.Cast<DataRow>();
        }

        public ProvinceContext GetProvinceContext(int provinceId)
        {
            return _cacheManager.GetObject<ProvinceContext, int>(string.Format("ProvinceContext_{0}", provinceId), provinceId) ?? new ProvinceContext();
        }

        private string _provinceNodeAliasPath;
        public string ProvinceNodeAliasPath
        {
            get
            {
                return _provinceNodeAliasPath ?? (_provinceNodeAliasPath = string.Format("{0}/{1}", TransformationHelper.GetRegionPath(LocalizationContext.PreferredCultureCode), GetProvinceName(LocalizationContext.PreferredCultureCode, Province.CategoryDisplayName)));
            }
        }

        public string GetProvinceName(string lang, string provinceName)
        {
            var translation = string.Empty;
            if (provinceName.StartsWith("{$")) translation = ResHelper.LocalizeString(provinceName);
            if (String.IsNullOrWhiteSpace(translation))
            {
                var uiCode = string.Format("CRC.Province.{0}", provinceName.Replace("-", string.Empty));
                translation = ResHelper.GetString(uiCode, lang);

                if (translation.Equals(uiCode, StringComparison.OrdinalIgnoreCase))
                    return provinceName.Replace(" ", "-"); ;
            }

            return translation.Replace(" ", "-");
        }

        public string GetCityPath(string city, string documentUrlPath)
        {
            var province = TransformationHelper.ReplaceAccentCharacters(GetProvinceName(LocalizationContext.PreferredCultureCode, Province.CategoryDisplayName));
            return documentUrlPath.ToLower().Replace("{province}", province).Replace("{city}", city);
        }
    }
}