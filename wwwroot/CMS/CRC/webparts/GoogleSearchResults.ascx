﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleSearchResults.ascx.cs" Inherits="CMSApp.CRC.webparts.GoogleSearchResults" %>
<%@ Register Src="~/CRC/webparts/Controls/CRCPager.ascx" TagPrefix="uc1" TagName="CRCPager" %>

<p>
    <asp:Literal runat="server" ID="litTotalItems"></asp:Literal>
</p>

<div class="search-options">
    <asp:DropDownList runat="server" ID="ddlFilter" AutoPostBack="True">
    </asp:DropDownList>
</div>
<asp:Repeater runat="server" ID="rptRelevantLinks">
    <HeaderTemplate>
        <div class="recommended">
            <h5><%# ResHelper.GetString("CRC.RecommendedLinks") %></h5>
    </HeaderTemplate>
    <ItemTemplate>
        <article>
            <a href="<%# Eval("Url") %>">
                <h5><%# Eval("Title") %></h5>
            </a>
            <p><%# Eval("Description") %></p>
            <a class="link" href="<%# Eval("Url") %>"><%# Eval("Url") %></a>
        </article>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
<asp:Panel runat="server" ID="pnlSearchResults">
    <div class="repeater no-more-tables">
        <asp:PlaceHolder runat="server" ID="plcBasicRepeater"></asp:PlaceHolder>
        <asp:Repeater runat="server" ID="rptGoogleSearch">
            <ItemTemplate>
                <article>
                    <a href="<%# Eval("Link") %>">
                        <h5><%# GetSearchMetaTag(Container.DataItem,"search:title",EvalText("Title")) %></h5>
                    </a>
                    <asp:PlaceHolder ID="Placeholder1" runat="server" Visible='<%# !string.IsNullOrWhiteSpace(GetSearchMetaTag(Container.DataItem,"author",string.Empty)) %>'>
                        <p class="post-metadata">
                            <%# ResHelper.GetString("CRC.Blog.By")%>&nbsp;
                            <%# GetSearchMetaTag(Container.DataItem,"author",string.Empty) %>
                        </p>
                    </asp:PlaceHolder>
                    <p>
                        <%# Eval("Snippet").ToString().Replace(Environment.NewLine,string.Empty) %>
                    </p>
                </article>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <cms:LocalizedLabel runat="server" ID="lblNoResults" CssClass="ContentLabel" Visible="false"
        EnableViewState="false" />
    <uc1:CRCPager runat="server" ID="CRCPager" PageSize="10" GroupSize="4" IsLateBinding="true" />
</asp:Panel>