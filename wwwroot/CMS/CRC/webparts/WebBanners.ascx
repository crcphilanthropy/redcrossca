<%@ Control Language="C#" AutoEventWireup="true" Inherits="CRC_webparts_WebBanners" CodeBehind="~/CRC/webparts/WebBanners.ascx.cs" %>
<asp:PlaceHolder runat="server" EnableViewState="false">
  <div id="web-banners" data-mm-web-banners
    data-culture='<%= CMS.DocumentEngine.DocumentContext.CurrentDocumentCulture.CultureCode %>'
    data-form-title='<%= ValidationHelper.GetString(GetValue("FormTitle"), "") %>'
    data-form-description='<%= ValidationHelper.GetString(GetValue("FormDescription"),"") %>'
    data-form-footer-description='<%= ValidationHelper.GetString(GetValue("FormFooterDescription"),"") %>'
    data-form-submit-text='<%= ValidationHelper.GetString(GetValue("FormSubmitText"),"") %>'
    data-form-success-message='<%= ValidationHelper.GetString(GetValue("FormSuccessMessage"),"") %>'
    data-company-name-label='<%= ValidationHelper.GetString(GetValue("CompanyNameLabel"),"") %>'
    data-company-name-placeholder='<%= ValidationHelper.GetString(GetValue("CompanyNamePlaceholder"),"") %>'
    data-company-name-error='<%= ValidationHelper.GetString(GetValue("CompanyNameFieldError"),"") %>'
    data-get-codes-description='<%= ValidationHelper.GetString(GetValue("GetCodeDescription"),"") %>'
    data-get-scroll-link-text='<%= ValidationHelper.GetString(GetValue("LinkToFormText"),"") %>'
    data-tracking-label='<%= ValidationHelper.GetString(GetValue("TrackingFieldTitle"),"") %>'
    data-embed-label='<%= ValidationHelper.GetString(GetValue("EmbedFieldTitle"),"") %>' />
</asp:PlaceHolder>
