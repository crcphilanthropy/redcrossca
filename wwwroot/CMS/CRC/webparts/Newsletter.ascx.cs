using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;
using CMS.Helpers;
using CMS.Localization;
using CRC.EmailClient.Models;
using CMSAppAppCode.Old_App_Code.CRC.Helpers;

namespace CMSApp.CRC.webparts
{
  public partial class Newsletter : CMSAbstractWebPart
  {
    /// <summary>
    /// Gets or sets the name of the english list.
    /// </summary>
    /// <value>
    /// The name of the english list.
    /// </value>
    public string EnglishListName
    {
      get
      {
        return DataHelper.GetNotEmpty(GetValue("ListName"), string.Empty);
      }
      set
      {
        SetValue("ListName", value);
      }
    }

    /// <summary>
    /// Gets or sets the name of the french list.
    /// </summary>
    /// <value>
    /// The name of the french list.
    /// </value>
    public string FrenchListName
    {
      get
      {
        return DataHelper.GetNotEmpty(GetValue("ListNameFr"), string.Empty);
      }
      set
      {
        SetValue("ListNameFr", value);
      }
    }

    /// <summary>
    /// Gets or sets the redirect to URL.
    /// </summary>
    /// <value>
    /// The redirect to URL.
    /// </value>
    public string RedirectToUrl
    {
      get
      {
        return DataHelper.GetNotEmpty(GetValue("RedirectToUrl"), string.Empty);
      }
      set
      {
        SetValue("RedirectToUrl", value);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:Init" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      SetupControl();
    }

    /// <summary>
    /// Raises the <see cref="E:PreRender" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
      if (!IsPostBack)
      {
        if (ValidationHelper.GetString(Request.QueryString["email"], "") != "")
        {
          NewsletterEmailTextBox.Text = ValidationHelper.GetString(Request.QueryString["email"], "");

        }
        else
        {
          NewsletterEmailTextBox.Text = ValidationHelper.GetString(SessionHelper.GetValue("email"), "");

        }
        if (ValidationHelper.GetString(Request.QueryString["status"], "") == "thank-you" | ValidationHelper.GetString(Request.QueryString["status"], "") == "merci")
        {
          pnlForm.Visible = false;
          pnlThankYou.Visible = true;
        }
      }
    }

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
      btnEmail.Click += BtnEmailClick;

      defaultSelection.Text = ResHelper.GetString("CRC.SelectOne");
      drpNewsletterSalutation.Items.Add(new ListItem(ResHelper.GetString("CRC.Mr")));
      drpNewsletterSalutation.Items.Add(new ListItem(ResHelper.GetString("CRC.Mrs")));

      pnlForm.DataBind();
      pnlThankYou.DataBind();

      if (CMS.DocumentEngine.DocumentContext.CurrentDocument.DocumentCulture == "fr-CA")
      {
        plcSalutation.Visible = true;
        plcLastName.Visible = true;

      }
      if (!IsPostBack)
      {
        NewsletterEmailTextBox.Text = Request.QueryString["email"];
        if (ValidationHelper.GetString(Request.QueryString["status"], "") == "thank-you" | ValidationHelper.GetString(Request.QueryString["status"], "") == "merci")
        {
          pnlForm.Visible = false;
          pnlThankYou.Visible = true;
        }
      }
    }

    /// <summary>
    /// BTNs the email click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    public void BtnEmailClick(object sender, EventArgs e)
    {
      Page.Validate("LyrisForm");
      if (!Page.IsValid)
      {
        return;
      }

      var tag = ValidationHelper.GetString(Request.QueryString["tag"], string.Empty);
      var date = ValidationHelper.GetString(Request.QueryString["date"], string.Empty); // make this default to DateTime.Today.Date.ToString("yyyy/MM/dd")

      if (SubmitToLyris(string.Format("{0} {1} {2}", drpNewsletterSalutation.SelectedValue, NewsletterFirstnameTextBox.Text, NewsletterLastnameTextBox.Text).Trim(), NewsletterEmailTextBox.Text, tag, date))
      {
        const string cookieName = "promo_banner_email";
        var cookie = HttpContext.Current.Request.Cookies[cookieName] ?? new HttpCookie(cookieName);

        cookie.Value = "1";
        cookie.Expires = DateTime.Now.AddYears(1);
        HttpContext.Current.Response.Cookies.Add(cookie);

        Response.Redirect(RedirectToUrl + "?status=" + ResHelper.GetString("CRC.ThankYouQueryString"), true);
      }
    }

    /// <summary>
    /// Submits to lyris.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="email">The email.</param>
    /// <param name="section">The email.</param>
    /// <param name="date">The email.</param>
    /// <returns></returns>
    private bool SubmitToLyris(string name, string email, string section, string date)
    {
      //add new parmas: path and date and section
      var paths = RequestContext.CurrentURL.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

      var siteSection = string.Empty;
      switch (SectionHelper.GetCurrentSection())
      {
        case SectionHelper.SiteSection.Blog:
          siteSection = "Blog";
          break;

        case SectionHelper.SiteSection.History:
            siteSection = "History";
          break;
      }

      switch (section.ToLower())
      {
        case "blog":
        case "blogue":
          siteSection = "Blog";
          break;

        case "history":
        case "histoire":
          siteSection = "History";
          break;

        case "swimming":
          siteSection = "Swimming";
          break;

        case "firstaid":
          siteSection = "First Aid";
          break;

        case "general":
          siteSection = "General";
          break;

        case "internationaloperations":
          siteSection = "International Operations";
          break;

        case "disastermanagement":
          siteSection = "Disaster Management";
          break;

        case "stories":
          siteSection = "Stories";
          break;

        case "violenceabuseprevention":
          siteSection = "Violence and Abuse Prevention";
          break;

        case "communityhealth":
          siteSection = "Community Health";
          break;

        case "migrationandrefugee":
          siteSection = "Migration and Refugee";
          break;
      }

      var subscriptionItem = new SubscriptionItem<string>
      {
        Email = email,
        FullName = name,
        ListKey = CRCEmailClientProvider.IsInTestMode ? "91388018c19eded9f08615bc975f82ad" : string.Equals(LocalizationContext.PreferredCultureCode, "en-CA") ? EnglishListName : FrenchListName,
        CustomFields =
                  new List<EmailContactCustomField>
                      {
                                new EmailContactCustomField {Key = "SiteSection", Value = siteSection},
                                new EmailContactCustomField {Key = "DateJoined", Value = date}
                      }
      };

      var listKeys = (string.Equals(LocalizationContext.PreferredCultureCode, "en-CA") ? EnglishListName : FrenchListName).Split(new[] { ",", " ", "|" }, StringSplitOptions.RemoveEmptyEntries).Select(item => ValidationHelper.GetInteger(item, 0)).ToList();

      var result = CRCEmailClientProvider.CurrentEmailClient == CRCEmailClientProvider.EmailClientType.Campaigner ? CRCEmailClientProvider.Subscribe(SubscriptionItem<List<int>>.Convert(subscriptionItem, listKeys)) : CRCEmailClientProvider.Subscribe(subscriptionItem);

      return result == CRCEmailClientProvider.EmailClientResult.Subscribed || result == CRCEmailClientProvider.EmailClientResult.Duplicate;
    }
  }
}