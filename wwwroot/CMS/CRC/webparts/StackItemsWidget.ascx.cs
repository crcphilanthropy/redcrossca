using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.Helpers;
using CMS.Taxonomy;
using CMS.Membership;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class StackItemsWidget : CMSAbstractWebPart
    {
        //WebPart property names in Kentico.
        const string vDocRoot = "DocumentRoot";
        const string vCat = "Category";
        const string vDocCat = "UseCategory";
        const string vCount = "Count";
        const string vBtnTxt = "ButtonText";
        const string vBtnUrl = "ButtonLink";

        #region Page Properties
        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
            set { SetValue("Title", value); }
        }

        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("TransformationName"), string.Empty); }
            set { SetValue("TransformationName", value); }
        }

        public string EmptyResultText
        {
            get { return ValidationHelper.GetString(GetValue("EmptyResultText"), string.Empty); }
            set { SetValue("EmptyResultText", value); }
        }

        public string OrderBy
        {
            get { return ValidationHelper.GetString(GetValue("OrderBy"), string.Empty); }
            set { SetValue("OrderBy", value); }
        }

        public string DocumentType
        {
            get { return ValidationHelper.GetString(GetValue("DocumentType"), string.Empty); }
            set { SetValue("DocumentType", value); }
        }

        /// <summary>
        /// Only consider documents below the following node
        /// </summary>
        public string DocumentRoot
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue(vDocRoot), string.Empty);
            }
            set
            {
                SetValue(vDocRoot, value);
            }
        }

        /// <summary>
        /// The category to pull news items for
        /// </summary>
        public string Category
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue(vCat), string.Empty);
            }
            set
            {
                SetValue(vCat, value);
            }
        }

        /// <summary>
        /// Determine if the page category should be checked before the specified category
        /// </summary>
        public bool UseCategory
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue(vDocCat), false);
            }
            set
            {
                SetValue(vDocCat, value);
            }
        }

        /// <summary>
        /// How many documents to show.
        /// </summary>
        public int Count
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue(vCount), 10);
            }
            set
            {
                SetValue(vCount, value);
            }
        }

        /// <summary>
        /// Url of the "more news" button
        /// </summary>
        public string ButtonLink
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue(vBtnUrl), string.Empty);
            }
            set
            {
                SetValue(vBtnUrl, value);
            }
        }

        /// <summary>
        /// Text of the "more news" button
        /// </summary>
        public string ButtonText
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue(vBtnTxt), string.Empty);
            }
            set
            {
                SetValue(vBtnTxt, value);
            }
        }

        public bool HideWhenEmpty
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("HideWhenEmpty"), true);
            }
            set
            {
                SetValue("HideWhenEmpty", value);
            }
        }

        /// <summary>
        /// Returns true if the control processing should be stopped.
        /// </summary>
        public override bool StopProcessing
        {
            get
            {
                return base.StopProcessing;
            }
            set
            {
                base.StopProcessing = value;
            }
        }

        #endregion

        /// <summary>
        /// Calculated category used to pull nodes.
        /// </summary>
        public string WhereClause
        {
            get
            {
                var selected = string.Empty;
                var sql = "DocumentID IN (SELECT DocumentID FROM CMS_DocumentCategory WHERE {0})";
                if (UseCategory)
                {
                    //Find document categories.
                    DataSet cats = CategoryInfoProvider.GetCategories(
                        string.Format("CategoryID in (Select distinct CategoryID from CMS_DocumentCategory Where DocumentID = {0}) or CategoryParentID in (Select distinct CategoryID from CMS_DocumentCategory Where DocumentID = {0})", DocumentContext.CurrentDocument.DocumentID),
                        "CategoryDisplayName"
                     );

                    //Build SQL statement.
                    for (var i = 0; i < cats.Tables[0].Rows.Count; i++)
                    {
                        selected += i > 0 ? " OR " : "";
                        selected += string.Format("CategoryID = {0}", cats.Tables[0].Rows[i]["CategoryID"]);
                    }
                }
                else if (!string.IsNullOrEmpty(this.Category))
                {
                    selected = string.Format("CategoryID = {0}", CategoryInfoProvider.GetCategoryInfo(this.Category, SiteContext.CurrentSiteName).CategoryID);
                }


                //Return the assembled string.
                return string.IsNullOrEmpty(selected)
                    ? string.Empty
                    : string.Format(sql, selected);
            }
        }


        /// <summary>
        /// Content loaded event handler.
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
            DataBind();
        }


        /// <summary>
        /// Initializes the control properties.
        /// </summary>
        protected void SetupControl()
        {
            if (StopProcessing) return;

            phNoData.Visible = false;
            rptStackItems.TransformationName = TransformationName;
            //Fill repeater.
            var tree = new TreeProvider(MembershipContext.AuthenticatedUser);
            var nodes = tree.SelectNodes(
                SiteContext.CurrentSiteName,
                this.DocumentRoot,
                LocalizationContext.PreferredCultureCode,
                false,
                DocumentType,
                this.WhereClause,
                OrderBy,
                -1,
                true,
                Count
                );

            if (nodes != null && nodes.Tables.Count > 0)
            {
                rptStackItems.DataSource = nodes.Tables[0].DefaultView;
                rptStackItems.DataBind();
            }
            else
            {
                phNoData.Visible = true;
                if (HideWhenEmpty)
                    Visible = false;
            }

            //Render link.
            if (!string.IsNullOrEmpty(TransformationHelper.getCultureDocumentUrl(ButtonLink)) && !string.IsNullOrEmpty(ButtonText))
            {
                lnkMore.Visible = true;
                lnkMore.Text = ButtonText;
                lnkMore.NavigateUrl = ButtonLink;
            }
        }
    }
}
