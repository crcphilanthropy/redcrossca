using System;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;

namespace CMSApp.CRC.webparts
{
    public partial class EmailSignUp2 : CMSAbstractWebPart
    {
        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
            set { SetValue("Title", value); }
        }
        public string Subtitle
        {
            get { return ValidationHelper.GetString(GetValue("Subtitle"), string.Empty); }
            set { SetValue("Subtitle", value); }
        }

        public string ButtonText
        {
            get { return ValidationHelper.GetString(GetValue("ButtonText"), string.Empty); }
            set { SetValue("ButtonText", value); }
        }

        public string DestinationUrl
        {
            get { return ValidationHelper.GetString(GetValue("DestinationUrl"), string.Empty); }
            set { SetValue("DestinationUrl", value); }
        }

        public string EmailPlaceHolder
        {
            get { return ValidationHelper.GetString(GetValue("EmailPlaceHolder"), string.Empty); }
            set { SetValue("EmailPlaceHolder", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitText();
            btnSignUp2.Click += BtnSignUpServerClick;
            btnSignUp2.Text = ButtonText;
            regExEmailAddress.ErrorMessage = reqEmailAddress.ErrorMessage = ResHelper.GetString("CRC.Error.Email");
            regExEmailAddress.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
            DataBind();
        }

        private void InitText()
        {
            btnSignUp2.Text = ButtonText;
            //btnSignUp.InnerText = string.Format("{0} >", ButtonText);
            SignUpEmailTextBox2.Attributes.Add("placeholder", EmailPlaceHolder);
        }

        void BtnSignUpServerClick(object sender, EventArgs e)
        {
            Response.Redirect(URLHelper.AddParameterToUrl(DestinationUrl + ".aspx", "email", SignUpEmailTextBox2.Value));
        }

    }
}