<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="CMSApp.CRC.webparts.CategoryList" Codebehind="CategoryList.ascx.cs" %>
<asp:Label ID="lblInfo" runat="server" Visible="false" CssClass="InfoLabel" />
<cms:BasicRepeater ID="rptCategoryList" runat="server" />
<asp:Literal ID="ltlList" runat="server"></asp:Literal>