
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMS.Localization;
using CMS.MacroEngine;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.SiteProvider;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class OGMetaTags : CMSAbstractWebPart
    {
        private const string TagFormat = "\t\t<meta property=\"{0}\" content=\"{1}\" />{2}";
        private readonly List<string> _postDocTypes = new List<string> { "cms.blogpost", "crc.news" };

        public string DefaultImage
        {
            get { return GetStringValue("DefaultImage", string.Empty); }
            set { SetValue("DefaultImage", value); }
        }

        public bool IncludeTwitterCard
        {
            get { return ValidationHelper.GetBoolean(GetValue("IncludeTwitterCard"), false); }
            set { SetValue("IncludeTwitterCard", value); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var description = GetDescription();
            var title = GetTitle();

            var tags = new Dictionary<string, string>
                {
                    {"og:title", title},
                    {"search:title", GetSearchTitle()},
                    {"og:image", GetImage()},
                    {"og:url", URLHelper.GetAbsoluteUrl(RequestContext.CurrentURL)},
                    {"og:site_name", ResHelper.GetString("CRC.RedCrossCanada")},
                    {"og:type", string.IsNullOrWhiteSpace(CurrentDocument.GetStringValue("OGType", string.Empty))
                                    ? "website"
                                    : CurrentDocument.GetStringValue("OGType", string.Empty)
                    },
                    {"og:description", description},
                    {"search:description", description}
                };

            if (IncludeTwitterCard)
            {
                tags.Add("twitter:card", "summary_large_image");
                tags.Add("twitter:site", LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase) ? "@croixrouge_qc" : "@redcrosscanada");
                tags.Add("twitter:title", title);
                tags.Add("twitter:description", description);
                tags.Add("twitter:image", GetImage("TwitterImage"));
            }


            if (_postDocTypes.Contains(CurrentDocument.ClassName, new StringEqualityComparer()))
            {
                var newsReleaseDate = CurrentDocument.GetDateTimeValue("NewsReleaseDate", DateTime.MinValue);
                if (newsReleaseDate == DateTime.MinValue)
                    newsReleaseDate = CurrentDocument.GetDateTimeValue("BlogPostDate", DateTime.MinValue);

                if (newsReleaseDate != DateTime.MinValue)
                    tags.Add("date", newsReleaseDate.ToString(ResHelper.GetString("CRC.Blog.DateTimeFormat")));


                var author = CurrentDocument.GetStringValue("Author", string.Empty);
                if (string.IsNullOrWhiteSpace(author) && CurrentDocument.ClassName.Equals("cms.blogpost", StringComparison.OrdinalIgnoreCase) && CurrentDocument.Owner != null)
                {
                    author = CurrentDocument.Owner.FullName;
                    tags.Add("search:author_url", TransformationHelper.GetBlogFilterPath("author", CurrentDocument.Owner.UserName));
                    tags.Add("search:author_signature", CurrentDocument.Owner.UserSignature);
                }

                if (!string.IsNullOrWhiteSpace(author))
                    tags.Add("author", author);
            }

            Page.Header.Controls.Add(new Literal { Text = "\r\n\t<!-- OPEN GRAPH TAGS BEGIN -->\r\n" });
            tags.Where(tag => !string.IsNullOrWhiteSpace(tag.Value)).ToList().ForEach(tag => Page.Header.Controls.Add(new Literal { Text = string.Format(TagFormat, tag.Key, Server.HtmlDecode(tag.Value), Environment.NewLine) }));
            Page.Header.Controls.Add(new Literal { Text = "\t<!-- OPEN GRAPH TAGS END -->\r\n\r\n" });
        }

        private string GetSearchTitle()
        {
            var title = MacroContext.GlobalResolver.ResolveCustomMacro("GetCustomPageTitle", "GetCustomPageTitle").Result.ToString();
            if (CurrentDocument.ClassName.Equals("cms.blogpost", StringComparison.OrdinalIgnoreCase))
                title = string.Format("{0}: {1}", ResHelper.GetString("CRC.FromTheBlogSearch"), title);
            return MacroResolver.Resolve(title);
        }

        private string GetDescription()
        {
            return GetFirstNonEmptyValue(new[]
                {
                    CurrentDocument.GetStringValue("OGDescription", string.Empty),
                    TextHelper.LimitLength(DocumentContext.CurrentDescription,160,wholeWords:true),
                    CurrentDocument.GetStringValue("NewsSummary",string.Empty),
                    CurrentDocument.GetStringValue("Summary", string.Empty),
                    CurrentDocument.GetStringValue("BlogPostSummary", string.Empty)
                }, CurrentDocument.DocumentName);
        }

        private string GetImage(string socialMediaField = "OGImage")
        {
            var thumbnail = GetAttachmentImage("Thumbnail");
            var blogPostTeaser = GetAttachmentImage("BlogPostTeaser");
            var newsImage = GetAttachmentImage("NewsImage");
            var newsThumbnail = GetAttachmentImage("NewsThumbnail");
            var braftonLargeImage = CurrentDocument.GetStringValue("BraftonLargeImageUrl", string.Empty);
            var timelineImage = CurrentDocument.GetStringValue("MainImage", string.Empty);
            if (!string.IsNullOrWhiteSpace(braftonLargeImage) && !braftonLargeImage.Contains("/"))
                braftonLargeImage = string.Format("/CRC/Brafton/{0}", braftonLargeImage);

            var toReturn = GetFirstNonEmptyValue(new[]
            {
                CurrentDocument.GetStringValue(socialMediaField, string.Empty),
                CurrentDocument.GetStringValue("Image", string.Empty),
                thumbnail,
                blogPostTeaser,
                newsImage,
                newsThumbnail,
                braftonLargeImage,
                CurrentDocument.GetStringValue("BraftonThumbnailImageUrl", string.Empty),
                timelineImage,
                DefaultImage
            },
            string.Empty);

            if (!string.IsNullOrWhiteSpace(toReturn) && ValidationHelper.GetGuid(toReturn, Guid.Empty) == Guid.Empty)
                toReturn = URLHelper.GetAbsoluteUrl(toReturn);

            if ((!string.IsNullOrWhiteSpace(toReturn) && !toReturn.ToLower().Contains("/cmspages/")) || (!string.IsNullOrWhiteSpace(toReturn) && toReturn.ToLower().Contains("/cmspages/getfile.aspx?guid=")))
                return toReturn;

            return CurrentDocument.DocumentCulture.Equals("fr-CA", StringComparison.OrdinalIgnoreCase)
                       ? "http://www.redcross.ca/crc/assets/logo-fr-fb.gif"
                       : "http://www.redcross.ca/crc/assets/header_logo-fb.gif";
        }

        private string GetTitle()
        {
            return GetFirstNonEmptyValue(new[] { CurrentDocument.GetStringValue("OGTitle", string.Empty), DocumentContext.CurrentTitle, CurrentDocument.GetStringValue("DocumentMenuCaption", string.Empty) }, CurrentDocument.DocumentName);
        }

        private string GetFirstNonEmptyValue(IEnumerable<string> values, string defaultValue)
        {
            foreach (var value in values.Where(value => !string.IsNullOrWhiteSpace(value)))
            {
                return Server.HtmlEncode(HTMLHelper.StripTags(value).Replace("\"", ""));
            }
            return Server.HtmlEncode(HTMLHelper.StripTags(defaultValue).Replace("\"", ""));
        }

        private string GetAttachmentImage(string fieldName)
        {
            var attGuid = DocumentContext.CurrentDocument.GetGuidValue(fieldName, Guid.Empty);
            if (attGuid == Guid.Empty)
                return string.Empty;

            return CacheHelper.Cache(() =>
                {
                    var image = AttachmentInfoProvider.GetAttachmentInfo(attGuid, SiteContext.CurrentSiteName);

                    return image != null ? URLHelper.GetAbsoluteUrl(string.Format("/CMSPages/GetFile.aspx?guid={0}", image.AttachmentGUID)) : string.Empty;
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("getattachmentimage_{0}_{1}_{2}", LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName, attGuid)));

        }
    }
}