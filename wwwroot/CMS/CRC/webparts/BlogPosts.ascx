﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogPosts.ascx.cs" Inherits="CMSApp.CRC.webparts.BlogPosts" %>
<h5><%# Title %></h5>
<cms:CMSRepeater runat="server" ID="rptItems" DataBindByDefault="False">
    <ItemTemplate>
        <article>
            <h6><%# Eval("PublishDate",string.Format("{{0:{0}}}", CMSApp.CRC.TransformationHelper.DateFormat)) %></h6>
            <a href='<%# GetItemUrl(Container.DataItem) %>' target="_blank"><%# Eval("Title.Text") %></a>
        </article>
    </ItemTemplate>
</cms:CMSRepeater>
