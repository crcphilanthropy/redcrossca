using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMS.PortalControls;
using System.Linq;
using CMSAppAppCode.Old_App_Code.CRC;
using ECA.FormControls.MultiDocumentSelector;
using TreeNode = CMS.DocumentEngine.TreeNode;
using CMS.Helpers;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class DonateForm : CMSAbstractWebPart
    {
        private string _siteName = ResHelper.GetString("CRC.Master.SiteName");
        public string SelectedDocuments
        {
            get { return GetStringValue("SelectedDocuments", string.Empty); }
            set { SetValue("SelectedDocuments", value); }
        }

        public string ImpactImage1
        {
            get { return GetStringValue("ImpactImage1", string.Empty); }
            set { SetValue("ImpactImage1", value); }
        }

        public string ImpactImage2
        {
            get { return GetStringValue("ImpactImage2", string.Empty); }
            set { SetValue("ImpactImage2", value); }
        }

        public string ImpactImage3
        {
            get { return GetStringValue("ImpactImage3", string.Empty); }
            set { SetValue("ImpactImage3", value); }
        }

        public string ImpactImage1AltText
        {
            get { return GetStringValue("ImpactImage1AltText", string.Empty); }
            set { SetValue("ImpactImage1AltText", value); }
        }

        public string ImpactImage2AltText
        {
            get { return GetStringValue("ImpactImage2AltText", string.Empty); }
            set { SetValue("ImpactImage2AltText", value); }
        }

        public string ImpactImage3AltText
        {
            get { return GetStringValue("ImpactImage3AltText", string.Empty); }
            set { SetValue("ImpactImage3AltText", value); }
        }

        public string LearnMoreTab
        {
            get { return GetStringValue("LearnMoreTab", string.Empty); }
            set { SetValue("LearnMoreTab", value); }
        }
        public bool IsMobile
        {
            get { return CMS.PortalEngine.DeviceContext.CurrentDevice.IsMobile; }
        }

        public string AppealBaseUrl
        {
            get
            {
                return LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase)
                           ? "/faites-un-don/appel/"
                           : "/donate/appeal/";
            }
        }

        private TreeNodeDataSet _appeals;
        private TreeNodeDataSet Appeals
        {
            get
            {
                return _appeals ?? (_appeals = GetData());
            }
        }

        private TreeNodeDataSet GetData()
        {
            return CacheHelper.Cache(() => SortableDocumentProvider.GetSortedTreeNodeDataSet(SelectedDocuments,
                                                                                             columns: "*, Case when LPDropDownTitle is null  then Name when LPDropDownTitle = '' then Name else LPDropDownTitle end as AppealTitle",
                                                                                             @where: "IsActive = 1"), new CacheSettings(Variables.CacheTimeInMinutes, String.Format("donateform_{0}_{1}", LocalizationContext.PreferredCultureCode, SelectedDocuments)));
        }

        private TreeNode _selectedAppeal;
        public TreeNode SelectedAppeal
        {
            get
            {
                var selectedAppealGuid = ValidationHelper.GetGuid(ddlAppeals.SelectedValue, Guid.Empty);
                return _selectedAppeal ??
                       (_selectedAppeal =
                        Appeals.FirstOrDefault(item => item.DocumentGUID == selectedAppealGuid));
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitTranslations();
            BindDropDown();
            BindRepeaters();
            RenderJsonData();
            this.CacheMinutes = 0;

            TransformationHelper.RegisterStartUpScript("$(document).ready(function () { donateForm.init(); });", Page, "initDonateForm");

            btnSubmit.Click += BtnSubmitClick;
            DataBind();
            SetSelectedAppeal();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase))
                phOther.Controls.AddAt(1, new Literal { Text = "<span class='french-dollar'></span>" });
            else
                phOther.Controls.AddAt(0, new Literal { Text = "<span class='can-dollar'></span>" });

            //SetSelectedAppeal();
        }

        private void SetSelectedAppeal()
        {
            var codeName = Request.QueryString["codeName"];
            if (string.IsNullOrWhiteSpace(codeName)) return;

            var selectedAppeal = Appeals.Items.FirstOrDefault(item => item.GetStringValue("CodeName", string.Empty).Equals(codeName, StringComparison.OrdinalIgnoreCase));

            if (selectedAppeal == null) return;

            DocumentContext.CurrentPageInfo.DocumentPageDescription = selectedAppeal.GetStringValue("Description", DocumentContext.CurrentPageInfo.DocumentPageDescription);
            DocumentContext.CurrentPageInfo.DocumentPageTitle = selectedAppeal.GetStringValue("AppealTitle", selectedAppeal.DocumentName);
            ddlAppeals.SelectedIndex = ddlAppeals.Items.IndexOf(ddlAppeals.Items.FindByValue(selectedAppeal.DocumentGUID.ToString(), true));

        }

        private void InitTranslations()
        {
            btnSubmit.Text = ResourceStringHelper.GetString("CRC.Donate.SubmitButtonText", defaultText: "Donate");
            chkInHonour.Text = ResourceStringHelper.GetString("CRC.Donate.InHonour", defaultText: "In Honour");
            chkMonthly.Text = ResourceStringHelper.GetString("CRC.Donate.MonthlyDonation", defaultText: "Make this a monthly Donation");
        }

        private void RenderJsonData()
        {
            var data = new List<dynamic>();
            Appeals.ToList().ForEach(item => data.Add(new
            {
                DocumentGuid = item.DocumentGUID.ToString(),
                PricePoint1 = GetDoubleValue(item, "PricePointAmount1", 75),
                PricePoint2 = GetDoubleValue(item, "PricePointAmount2", 150),
                PricePoint3 = GetDoubleValue(item, "PricePointAmount3", 200),
                PricePoint1Label = GetDoubleValue(item, "PricePointAmount1", 75).ToString("C0"),
                PricePoint2Label = GetDoubleValue(item, "PricePointAmount2", 150).ToString("C0"),
                PricePoint3Label = GetDoubleValue(item, "PricePointAmount3", 200).ToString("C0"),
                DisableMonthlyOption = item.GetBooleanValue("SetDefaultMonthlyDonation", false) || item.GetBooleanValue("SetDefaultSingleDonation", false),
                IsMonthlyDonationOnly = item.GetBooleanValue("SetDefaultMonthlyDonation", false),
                CodeName = item.GetStringValue("CodeName", string.Empty).ToLower(),
                PageTitle = string.Format("{0} - {1}", item.GetStringValue("AppealTitle", item.DocumentName), _siteName),
                AllowMonthlyInHonour = item.GetBooleanValue("AllowMonthlyInHonour", false),
                EnableInHonour = string.IsNullOrEmpty(item.GetStringValue("DonationInHonourUrl", "")) ? false : true,
                BackgroundImage = item.GetStringValue("Image", "")
            }));

            var json = new JavaScriptSerializer().Serialize(data);
            TransformationHelper.RegisterStartUpScript(string.Format("donateForm.appeals = {0};", json), Page, "appealsData");
            TransformationHelper.RegisterStartUpScript(string.Format("donateForm.appealBaseUrl = '{0}';", AppealBaseUrl), Page, "appealBaseUrl");
        }

        void BtnSubmitClick(object sender, EventArgs e)
        {
            if (SelectedAppeal == null)
                throw new FileNotFoundException("Could not find the selected appeal");

            var amount = GetSelectedDonationAmount();
            var donationType = chkMonthly.Checked || SelectedAppeal.GetBooleanValue("SetDefaultMonthlyDonation", false) ? "monthly" : "single";
            var donationUrlField = chkInHonour.Checked ? "DonationInHonourURL" : "FormUrl";

            // using code provided by Cardinal Path (client side) to populate "destinationUrl".  
            var url = URLHelper.AddParameterToUrl(SelectedAppeal.GetStringValue(donationUrlField, RequestContext.CurrentURL), "type", donationType);

            url = URLHelper.AddParameterToUrl(url, "amount", amount.ToString().Replace(',', '.'));

            url = URLHelper.AddParameterToUrl(url, "langpref", LocalizationContext.PreferredCultureCode);

            url = URLHelper.AddParameterToUrl(url, "_ga", (hidDonateBarGAHash.Value ?? string.Empty).ToLower().Replace("_ga=", string.Empty));

            Response.Redirect(url, true);
        }

        protected double GetSelectedDonationAmount()
        {
            var amount = 0.0;

            if (amount.Equals(0.0) && !string.IsNullOrWhiteSpace(Request.Form[rbPrice1.UniqueID.Replace(rbPrice1.ID, "pricepoint")]))
            {
                amount = ValidationHelper.GetDouble(Request.Form[rbPrice1.UniqueID.Replace(rbPrice1.ID, "pricepoint")], 0.0);
            }

            if (rbPrice4.Checked || (!string.IsNullOrWhiteSpace(Request.Form[txtOther.UniqueID]) && string.IsNullOrWhiteSpace(Request.Form[rbPrice1.UniqueID.Replace(rbPrice1.ID, "pricepoint")])))
            {
                //amount = ValidationHelper.GetDouble((Request.Form[txtOther.UniqueID] ?? string.Empty).Trim(new[] { '$', ' ' }), 0.0, CultureInfo.CreateSpecificCulture("en-CA"));
                amount = TransformationHelper.ConvertDonationValueToDouble((Request.Form[txtOther.UniqueID] ?? string.Empty).Trim(new[] { '$', ' ' }));
            }
            return amount;
        }

        private void BindRepeaters()
        {
            rptLearnMoreDescriptions.DataSource = Appeals;
            rptLearnMoreDescriptions.DataBind();

            rptPricePoints.DataSource = Appeals;
            rptPricePoints.DataBind();

            rptAppealDesciptions.DataSource = Appeals;
            rptAppealDesciptions.DataBind();

            rptImpacts.DataSource = Appeals;
            rptImpacts.DataBind();

            rptTiledAppeals.DataSource = Appeals;
            rptTiledAppeals.DataBind();

            rptTablet.DataSource = Appeals;
            rptTablet.DataBind();
        }

        private void BindDropDown()
        {
            ddlAppeals.DataSource = Appeals;
            ddlAppeals.DataTextField = "AppealTitle";
            ddlAppeals.DataValueField = "DocumentGuid";
            ddlAppeals.DataBind();
        }
        protected string ShowHideOnDevice(object dataItem, string type, int i)
        {
            var toReturn = new List<string>();

            if (ValidationHelper.GetBoolean(DataBinder.Eval(dataItem, string.Format("{0}HideOnMobile{1}", type, i)), false))
                toReturn.Add("hide-mobile");

            if (ValidationHelper.GetBoolean(DataBinder.Eval(dataItem, string.Format("{0}HideOnDeskTop{1}", type, i)), false))
                toReturn.Add("hide-desktop");

            return toReturn.Join(" ");
        }

        protected bool ShowHidePricePoint(object dataItem, int index)
        {
            return !String.IsNullOrWhiteSpace(DataBinder.Eval(dataItem, string.Format("PricePointImage{0}", index)) as string) &&
                   !String.IsNullOrWhiteSpace(DataBinder.Eval(dataItem, string.Format("PricePointDescription{0}", index)) as string);
        }

        public double GetDoubleValue(TreeNode appeal, string fieldName, double defaultValue)
        {
            var amount = appeal.GetDoubleValue(fieldName, defaultValue);
            if (amount.Equals(0.0))
                amount = defaultValue;

            return amount;
        }

        public double GetDoubleValue(object dataItem, string fieldName, double defaultValue)
        {
            var amount = ValidationHelper.GetDouble(DataBinder.Eval(dataItem, fieldName), defaultValue);
            if (amount.Equals(0.0))
                amount = defaultValue;

            return amount;
        }
    }
}