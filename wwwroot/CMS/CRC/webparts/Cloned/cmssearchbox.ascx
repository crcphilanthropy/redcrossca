<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="CMSApp.CRC.Webparts.Cloned.Searchbox" CodeBehind="cmssearchbox.ascx.cs" %>
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnImageButton" CssClass="">

    <asp:Label ID="lblSearch" runat="server" AssociatedControlID="txtWord" EnableViewState="false" />
    <cms:CMSTextBox ID="txtWord" runat="server" ProcessMacroSecurity="false" TabIndex="20" />

    <asp:LinkButton runat="server" ID="btnImageButton" CssClass="search-btn" OnClick="btnImageButton_Click" TabIndex="20">
        <img src="/CRC/images/icon-search.png" alt="Search Icon">
    </asp:LinkButton>

</asp:Panel>

