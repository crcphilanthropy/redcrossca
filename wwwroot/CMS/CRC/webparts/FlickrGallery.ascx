﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="FlickrGallery.ascx.cs" Inherits="CMSApp.CRC.WebParts.FlickrGallery" %>
<asp:Repeater runat="server" ID="rptFlickr">
    <ItemTemplate>
        <div class="flickr-photo">
            <img src='<%# Eval("Medium640Url") %>' alt='<%# Eval("Title") %>' />
            <div class="photo-meta-data">
                <%# Eval("Title") %> <br />
                <%# Eval("Description") %>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
