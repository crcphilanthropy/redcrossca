<%@ Control Language="C#" AutoEventWireup="true" Inherits="crc_webparts_Multimedia_VideoListing" CodeBehind="~/crc/webparts/Multimedia/VideoListing.ascx.cs" %>
<asp:PlaceHolder runat="server">
    <div 
        data-mm-video-listing 
        data-culture='<%= CMS.DocumentEngine.DocumentContext.CurrentDocumentCulture.CultureCode %>'
        data-path='<%= ValidationHelper.GetString(GetValue("Path"), "") %>'
        data-category-label='<%= ValidationHelper.GetString(GetValue("CategoriesTitle"),"") %>'
        data-category-placeholder='<%= ValidationHelper.GetString(GetValue("CategoriesPlaceholder"),"") %>'
        data-region-label='<%= ValidationHelper.GetString(GetValue("RegionsTitle"),"") %>'
        data-region-placeholder='<%= ValidationHelper.GetString(GetValue("RegionsPlaceholder"),"") %>'
        data-page-size='<%= ValidationHelper.GetString(GetValue("PageSize"), "") %>'
        data-hide-summary='<%= ValidationHelper.GetString(GetValue("HideSummary"), "") %>'
        data-tags-title='<%= ValidationHelper.GetString(GetValue("TagsTitle"), "") %>'
        data-show-more-text='<%= ValidationHelper.GetString(GetValue("ShowMoreText"), "") %>'
        data-show-less-text='<%= ValidationHelper.GetString(GetValue("ShowLessText"), "") %>'
        data-filter-button-text='<%= ValidationHelper.GetString(GetValue("FilterButtonText"),"")%>'
        data-filter-change-analytics='<%= ValidationHelper.GetString(GetValue("FilterChangeAnalyticsEvent"), "") %>'
        data-tag-click-analytics='<%= ValidationHelper.GetString(GetValue("TagClickAnalyticsEvent"), "") %>'
    />
</asp:PlaceHolder>