<%@ Control Language="C#" AutoEventWireup="true" Inherits="CRC_webparts_Multimedia_MediaAlerts" CodeBehind="~/CRC/webparts/Multimedia/MediaAlerts.ascx.cs" %>
<%@ Import Namespace="CMS.DocumentEngine" %>

<asp:Panel runat="server" DefaultButton="btnSignUp" ID="pnlMediaAlert" ClientIDMode="Static">

    <div class="media-alerts-cta" id="mediaAlerts">
        <h3 class="h2">
            <cms:LocalizedLiteral runat="server" ResourceString="CRC.MediaAlertTitle"></cms:LocalizedLiteral>
        </h3>
        <p>
            <cms:LocalizedLiteral runat="server" ID="alertSuccess" Visible="false" ResourceString="CRC.MediaAlertSuccess"></cms:LocalizedLiteral>
        </p>

        <div id="alertDefault" runat="server">
            <p>
                <cms:LocalizedLiteral runat="server" ID="defaultDescription" ResourceString="CRC.MediaAlertDescription"></cms:LocalizedLiteral>
            </p>
            

            <label for="SignUpNameTextBox"><span>* </span><cms:LocalizedLiteral runat="server" ResourceString="CRC.MediaAlertFormName"></cms:LocalizedLiteral></label>
            <input type="text" required placeholder='<%# ResHelper.GetString("CRC.EnterYourName") %>' runat="server" id="SignUpNameTextBox" clientidmode="Static" data-input-type="text" data-change-type="true" validationgroup="MediaAlertSignUp" />
            <div class="error-group">
                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="SignUpNameTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Media.Error.Name") %>' ValidationGroup="MediaAlertSignUp" CssClass="text-error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>

            <label for="SignUpEmailTextBox"><span>* </span><cms:LocalizedLiteral runat="server" ResourceString="CRC.MediaAlertFormEmail"></cms:LocalizedLiteral></label>
            <input type="text" required placeholder='<%# ResHelper.GetString("CRC.EnterYourEmailHere") %>' runat="server" id="SignUpEmailTextBox" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="MediaAlertSignUp" class="trim" />            
            <div class="error-group">
                <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="MediaAlertSignUp" CssClass="text-error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailTextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="MediaAlertSignUp" CssClass="text-error" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
            </div>

            <label class="button">
                <asp:Button runat="server" ID="btnSignUp" CssClass="submit-btn email-signup-btn" text="Submit" OnClick="BtnSignUp" ValidationGroup="MediaAlertSignUp" />
            </label>
        </div>

    </div>
</asp:Panel>
