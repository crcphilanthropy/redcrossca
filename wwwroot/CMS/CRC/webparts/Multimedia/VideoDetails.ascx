<%@ Control Language="C#" AutoEventWireup="true" Inherits="CRC_webparts_Multimedia_VideoDetails" CodeBehind="~/CRC/webparts/Multimedia/VideoDetails.ascx.cs" %>
<%@ Import Namespace="CMS.DocumentEngine" %>
<%@ Import Namespace="CMS.Helpers" %>
<%@ Import Namespace="CMS.Localization" %>

<asp:PlaceHolder runat="server">
  <div class="video-player">
    <asp:Literal runat="server" ID="litVideoEmbedID" />
    <script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
    <div class="wistia_responsive_padding" style="padding: 56.25% 0 0 0; position: relative;">
      <div class="wistia_responsive_wrapper" style="height: 100%; left: 0; position: absolute; top: 0; width: 100%;">
        <div class="wistia_embed wistia_async_<%# VideoEmbedID %> videoFoam=true" style="height: 100%; width: 100%">&nbsp;</div>
      </div>
    </div>
  </div>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server">
  <div class="video-tags">
    <strong>
      <cms:LocalizedLiteral runat="server" ResourceString="CRC.Blog.Tags"></cms:LocalizedLiteral>:</strong>
    <asp:Repeater runat="server" ID="categorytags">
      <ItemTemplate>
        <span>
          <asp:HyperLink runat="server" NavigateUrl='<%# Eval("SearchLink") %>'
            Text='<%# Eval("CategoryDisplayName") %>' />
        </span>
      </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater runat="server" ID="regiontags">
      <ItemTemplate>
        <span>
          <asp:HyperLink runat="server" NavigateUrl='<%# Eval("SearchLink") %>' Text='<%# Eval("CategoryDisplayName") %>' />
        </span>
      </ItemTemplate>
    </asp:Repeater>
  </div>
</asp:PlaceHolder>

<h2>
  <cms:LocalizedLiteral runat="server" ResourceString="CRC.VideoSummary"></cms:LocalizedLiteral>:</h2>

<div class="video-summary">
  <p>
    <asp:Literal runat="server" ID="litVideoSummary"></asp:Literal>
  </p>
  <p class="video-credits">
    <cms:LocalizedLiteral runat="server" ResourceString="CRC.MediaCredits"></cms:LocalizedLiteral>:
        <asp:Literal runat="server" ID="litCredits"></asp:Literal>
  </p>
</div>

<asp:PlaceHolder runat="server">
  <div class="video-info">
    <p>
      <strong>
        <cms:LocalizedLiteral runat="server" ResourceString="CRC.VideoLength"></cms:LocalizedLiteral>:</strong>
      <asp:Literal runat="server" ID="litLength"></asp:Literal>
    </p>
    <p>
      <strong>
        <cms:LocalizedLiteral runat="server" ResourceString="CRC.DatePosted"></cms:LocalizedLiteral>:</strong> <%= DocumentContext.CurrentDocument.GetDateTimeValue("DocumentLastPublished",DocumentContext.CurrentDocument.GetDateTimeValue("DocumentModifiedWhen",DateTime.Now)).ToString(CMSApp.CRC.TransformationHelper.DateFormat) %>
    </p>
    <p>
      <strong>
        <cms:LocalizedLiteral runat="server" ResourceString="CRC.Subtitles"></cms:LocalizedLiteral>:</strong> <%= DocumentContext.CurrentDocument.GetBooleanValue("Subtitles", false) ? ResHelper.GetString("CRC.Available") :  ResHelper.GetString("CRC.NotAvailable") %>
    </p>
  </div>
</asp:PlaceHolder>


<asp:PlaceHolder runat="server" Visible='<%# ValidationHelper.GetBoolean(!string.IsNullOrEmpty(litTranscript.Text), false) %>'>
  <div class="collapsible">
    <div class="transcript-item" aria-expanded="false">
      <button class="transcript-button" data-title='<%= DocumentContext.CurrentDocument.DocumentName %>'
        data-transcript-analytics-event='<%= DocumentContext.CurrentDocument.GetIntegerValue("VideoType", -1) == 1 
              ? "e_pubVideosTranscript" 
              : DocumentContext.CurrentDocument.GetIntegerValue("VideoType", -1) == 2 
                ? "e_videosTranscript" 
                : "" %>'>
        <span class="show-text">
          <cms:LocalizedLiteral runat="server" ResourceString="CRC.ShowTranscript"></cms:LocalizedLiteral>
          <i class="fa fa-plus" aria-hidden="true"></i></span>
        <span class="hide-text">
          <cms:LocalizedLiteral runat="server" ResourceString="CRC.HideTranscript"></cms:LocalizedLiteral>
          <i class="fa fa-minus" aria-hidden="true"></i></span>
      </button>
      <div class="transcript-text" aria-hidden="true">
        <asp:Literal runat="server" ID="litTranscript"></asp:Literal>
      </div>
    </div>
  </div>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" Visible='<%# DocumentContext.CurrentDocument.GetBooleanValue("DownloadAvailable", false) %>'>
  <div id="video-download-form"
    data-page-title='<%= DocumentContext.CurrentDocument.DocumentName %>'
    data-id='<%= DocumentContext.CurrentDocument.GetIntegerValue("VideoDetailID", -1) %>'
    data-culture='<%= DocumentContext.CurrentDocumentCulture.CultureCode %>'
    data-form-title='<%= ValidationHelper.GetString(GetValue("FormTitle"), "") %>'
    data-form-description='<%= ValidationHelper.GetString(GetValue("FormDescription"), "") %>'
    data-form-footer-description='<%= ValidationHelper.GetString(GetValue("FormFooterDescription"), "") %>'
    data-form-submit-text='<%= ValidationHelper.GetString(GetValue("FormSubmitText"), "") %>'
    data-form-success-message='<%= ValidationHelper.GetString(GetValue("FormSuccessMessage"), "") %>'
    data-form-success-button-text='<%= ValidationHelper.GetString(GetValue("FormSuccessButtonText"), "") %>'
    data-name-label='<%= ValidationHelper.GetString(GetValue("NameLabel"), "") %>'
    data-name-placeholder='<%= ValidationHelper.GetString(GetValue("NamePlaceholder"), "") %>'
    data-name-error='<%= ValidationHelper.GetString(GetValue("NameFieldError"), "") %>'
    data-email-label='<%= ValidationHelper.GetString(GetValue("EmailLabel"), "") %>'
    data-email-placeholder='<%= ValidationHelper.GetString(GetValue("EmailPlaceholder"), "") %>'
    data-email-error='<%= ValidationHelper.GetString(GetValue("EmailFieldError"), "") %>'
    data-publication-label='<%= ValidationHelper.GetString(GetValue("PublicationLabel"), "") %>'
    data-publication-placeholder='<%= ValidationHelper.GetString(GetValue("PublicationPlaceholder"), "")%>'
    data-publication-error='<%= ValidationHelper.GetString(GetValue("PublicationFieldError"), "") %>'>
  </div>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server">
  <div runat="server" id="relatedVideos" data-show-more-text='<%= ResHelper.GetString("CRC.SeeMore") %>' data-show-less-text='<%= ResHelper.GetString("CRC.SeeLess") %>'></div>
</asp:PlaceHolder>


<asp:PlaceHolder runat="server">
  <div data-mm-back-button data-button-text='<%= ResHelper.GetString("CRC.BackToVideos") %>'></div>
</asp:PlaceHolder>

