using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.Helpers;
using MultiMediaApi.Services;
using MultiMediaApi.Services.Repository;
using MultiMediaApi.Models;

public partial class CRC_webparts_Multimedia_MediaAlerts : CMSAbstractWebPart
{
    #region "Properties"
    private readonly ICatalogService _catalogApi;
    public MediaAlertForm _alertform;

    #endregion


    #region "Methods"

    public CRC_webparts_Multimedia_MediaAlerts()
    {
        _catalogApi = new CatalogService();
        _alertform = new MediaAlertForm();
    }

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    public string CustomTableName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TableName"), string.Empty);
        }
        set
        {
            this.SetValue("TableName", value);
        }
    }

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            regExEmailAddress.ErrorMessage = reqEmailAddress.ErrorMessage = ResHelper.GetString("CRC.Error.Email");
            regExEmailAddress.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
            reqName.ErrorMessage = ResHelper.GetString("CRC.Media.Error.Name");
        }
        DataBind();
    }

    public void BtnSignUp(object sender, EventArgs e)
    {
        var formName = SignUpNameTextBox.Value;
        var formEmail = SignUpEmailTextBox.Value;
        _alertform.Name = formName;
        _alertform.Email = formEmail;
        var result = _catalogApi.SaveAlertForm(CustomTableName, _alertform);
        if (result != null && result.ItemID > 0)
        {
            alertDefault.Visible = false;
            alertSuccess.Visible = true;
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



