using System;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.DocumentEngine;
using CMS.Helpers;
using MultiMediaApi.Services.Repository;
using MultiMediaApi.Services;
using MultiMediaApi.Models;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using CMS.Localization;

public partial class CRC_webparts_Multimedia_VideoDetails : CMSAbstractWebPart
{
    #region "Properties"
    private readonly ICatalogService _catalogApi;
    private readonly IVideoService _videoApi;
    public IEnumerable<Category> categories;
    public IEnumerable<Region> regions;
    public Literal litCategories;
    public Literal litRegions;
    public List<Video> relatedItems;
    public string tempDuration;
    private string listCategoriesUrl;
    private string categoryhotlinktext;
    private string regionhotlinktext;
    public string parentPath;

    public CRC_webparts_Multimedia_VideoDetails()
    {
        _catalogApi = new CatalogService();
        _videoApi = new VideoService();
        this.categories = new List<Category>();
        this.regions = new List<Region>();
        this.litCategories = new Literal();
        this.litRegions = new Literal();
        this.relatedItems = new List<Video>();
        listCategoriesUrl = string.Empty;
        categoryhotlinktext = "?categories=";
        regionhotlinktext = "?regions=";
        tempDuration = "{0} {1} {2} {3} {4} {5}";
        parentPath = DocumentContext.CurrentDocument.Parent.NodeAliasPath;
    }

    public string VideoEmbedID
    {
        get
        {
            return ValidationHelper.GetString(DocumentContext.CurrentDocument.GetValue("VideoEmbedID"), string.Empty);
        }
        set
        {
            this.SetValue("VideoEmbedID", value);
        }
    }

    public string ListingURL
    {
        get
        {
            return _catalogApi.CleanPath(ValidationHelper.GetString(GetValue("ListingURL"), string.Empty));
        }
        set
        {
            this.SetValue("ListingURL", value);
        }
    }

    public string PublicListingUrl
    {
        get
        {
            return _catalogApi.CleanPath(ValidationHelper.GetString(GetValue("PublicListingUrl"), string.Empty));
        }
        set
        {
            this.SetValue("PublicListingUrl", value);
        }
    }

    public string DownloadLink
    {
        get
        {
            return ValidationHelper.GetString(DocumentContext.CurrentDocument.GetValue("DownloadLink"), string.Empty);
        }
        set
        {
            this.SetValue("DownloadLink", value);
        }
    }
    #endregion


    #region "Methods"   
    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    
    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            litVideoEmbedID.Text = string.Format("<script src=\"//fast.wistia.com/embed/medias/{0}.jsonp\" async></script>", VideoEmbedID);
            litVideoSummary.Text = DocumentContext.CurrentDocument.GetValue("VideoSummary", "");
            litCredits.Text = DocumentContext.CurrentDocument.GetValue("Credits", "");
            litTranscript.Text = DocumentContext.CurrentDocument.GetValue("Transcript", "");
            litCategories.Text = DocumentContext.CurrentDocument.GetValue("Categories", "");
            litRegions.Text = DocumentContext.CurrentDocument.GetValue("Regions", "");
            litLength.Text = DocumentContext.CurrentDocument.GetValue("Length", "");
            //placeholder.Visible = DocumentContext.CurrentDocument.GetBooleanValue("DownloadAvailable", false);
            var datecreated = DocumentContext.CurrentDocument.GetDateTimeValue("DocumentCreatedWhen", DateTime.MinValue);

            //Call API service to get categories and regions for video details
            var videoCategoryIds = _catalogApi.GetCategoryIds(litCategories.Text);
            var videoRegionIds = _catalogApi.GetRegionIds(litRegions.Text);
            this.categories = _catalogApi.CategoriesById(videoCategoryIds, DocumentContext.CurrentDocumentCulture.CultureCode);
            this.regions = _catalogApi.RegionsById(videoRegionIds, DocumentContext.CurrentDocumentCulture.CultureCode);

            //Check if video is public. Pick listing url depending on public or media
            var linkUrl = "";
            var videotype = CurrentDocument.GetIntegerValue("VideoType", 0);
            if (videotype != 0 && videotype == 1)
            {
                linkUrl = PublicListingUrl;
            }
            else if (videotype != 0 && videotype == 2)
            {
                linkUrl = ListingURL;
            }
            //Build links to search page
            if (categories != null && categories.Count() > 0)
            {
                foreach (var item in categories)
                {
                    listCategoriesUrl = string.Concat(linkUrl, categoryhotlinktext + item.CategoryID + '_' + item.CategoryName);
                    item.SearchLink = listCategoriesUrl;
                }
            }
            if (regions != null && regions.Count() > 0)
            {
                foreach (var item in regions)
                {
                    listCategoriesUrl = string.Concat(linkUrl, regionhotlinktext + item.CategoryID + '_' + item.CategoryName);
                    item.SearchLink = listCategoriesUrl;
                }
            }

            //Bind categories and regions to view repeater
            categorytags.DataSource = categories;
            categorytags.DataBind();
            regiontags.DataSource = regions;
            regiontags.DataBind();

            //Build video duration string     
            var date = new DateTime().AddSeconds(ValidationHelper.GetDouble(DocumentContext.CurrentDocument.GetValue("VideoLength", ""), 0));
            if (date != null)
            {
                var duration = _catalogApi.VideoDetailsDuration(date, tempDuration);
                litLength.Text = duration;
            }

            //gather related videos guids if they exist and get video details
            string headingText = string.Empty;
            var videoGuid = ValidationHelper.GetGuid(DocumentContext.CurrentDocument.GetValue("DocumentGuid"), Guid.Empty);
            this.relatedItems = _videoApi.GetDetailsRelatedVideos(videoGuid, videoCategoryIds, videoRegionIds, datecreated, CurrentDocument, parentPath, LocalizationContext.CurrentCulture.CultureCode.ToLower());
            var tempitems = JsonConvert.SerializeObject(relatedItems);
            //Assign related videos heading text if list of related videos is not empty
            headingText = relatedItems.Count > 0 ? ResHelper.GetString("CRC.RelatedVideos", LocalizationContext.CurrentCulture.CultureCode) : string.Empty;
            relatedVideos.Attributes.Add("data-relatedVideos", tempitems);
            relatedVideos.Attributes.Add("data-listing-url", linkUrl);
            relatedVideos.Attributes.Add("data-heading", headingText);
            relatedVideos.Attributes.Add("data-tags-title", ResHelper.GetString("CRC.Blog.Tags", LocalizationContext.CurrentCulture.CultureCode));
            relatedVideos.Attributes.Add("data-show-more-text", ResHelper.GetString("CRC.SeeMore", LocalizationContext.CurrentCulture.CultureCode));
            relatedVideos.Attributes.Add("data-show-less-text", ResHelper.GetString("CRC.SeeLess", LocalizationContext.CurrentCulture.CultureCode));
        }
        DataBind();
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



