
using CMS.Helpers;
using CMS.PortalControls;
using System.Web.UI.WebControls;

public partial class crc_webparts_Multimedia_VideoListing : CMSAbstractWebPart
{
    #region "Properties"
    
    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            var pageSize = ValidationHelper.GetInteger(GetValue("PageSize"), -1);
            var pageNumber = URLHelper.GetQueryValue(URLHelper.GetAbsoluteUrl(RequestContext.CurrentURL), "page");
            var currentPage = pageNumber == "" ? 1 : ValidationHelper.GetInteger(pageNumber, 1);

            var nextUrl =  URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (currentPage + 1).ToString()));
            var prevUrl = currentPage == 2 ? URLHelper.GetAbsoluteUrl(URLHelper.RemoveParameterFromUrl(RequestContext.CurrentURL, "page")) : URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (currentPage - 1).ToString()));

            if (currentPage > 1)
                Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='prev' href='{0}' />\n", prevUrl) });

            if (currentPage < pageSize)
                Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='next' href='{0}' />\n", nextUrl) });
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



