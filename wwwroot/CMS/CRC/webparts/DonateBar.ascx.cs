using System;
using System.Globalization;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Helpers;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class DonateBar : CMSAbstractWebPart
    {
        public string DonationFormUrl
        {
            get { return ValidationHelper.GetString(GetValue("DonationFormUrl"), String.Empty); }
            set { SetValue("DonationFormUrl", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitTranslation();
            DataBind();

            //btnDonate.Click += BtnDonateServerClick;
            TransformationHelper.RegisterStartUpScript("document.addEventListener('DOMContentLoaded', function() { initDonationBarTracking(); });", Page);
        }

        void BtnDonateServerClick(object sender, EventArgs e)
        {
            Page.Validate("DonationBar");
            var amount = TransformationHelper.ConvertDonationValueToDouble(DonationAmount.Text);//string.IsNullOrWhiteSpace(DonationAmount.Text) ? string.Empty : DonationAmount.Text.Replace(",", ".");
            var donationType = rbSingleDonation.Checked
                                   ? rbSingleDonation.Value
                                   : (rbMonthlyDonation.Checked ? rbMonthlyDonation.Value : string.Empty);

            var url = URLHelper.AddParameterToUrl(DonationFormUrl, "type", donationType);
            url = URLHelper.AddParameterToUrl(url, "amount", amount.ToString().Replace(',', '.'));
            url = URLHelper.AddParameterToUrl(url, "langpref", LocalizationContext.PreferredCultureCode);
            url = URLHelper.AddParameterToUrl(url, "_ga", (hidDonateBarGAHash.Value ?? string.Empty).ToLower().Replace("_ga=", string.Empty));

            Response.Redirect(url, true);
        }


        private void InitTranslation()
        {
            DonationAmount.Attributes.Add("placeholder", ResHelper.GetString("CRC.DonateBar.Placeholder"));
            btnDonate.Text = String.Format("{0}", ResHelper.GetString("CRC.DonateBar.GiveNow"));
        }
    }
}