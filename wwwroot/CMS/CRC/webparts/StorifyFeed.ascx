<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.webparts.StorifyFeed" CodeBehind="StorifyFeed.ascx.cs" EnableViewState="false" %>

<asp:PlaceHolder runat="server">
<div id="storify-tweets"></div>

<div style="display: none">
    <div class="tweet">
        <a href="#" target="_blank" class="avatar">
            <img src="http://pbs.twimg.com/profile_images/378800000341366416/caab15b2638f4280d19d155829a02924_normal.jpeg" alt="" />
        </a>
        <div class="quote-author">
            <a href="#" target="_blank" class="user">UserName</a><br />
            <a href="#" target="_blank" class="username">Display Name</a>
        </div>
        <p class="text">text goes here</p>
        <a href="https://twitter.com/#!/redcrosscanada/status/428235569345605632" target="_blank" class="statusLink">
           sss
        </a>
         
        <div class="video"></div>
            <p>
                <a class="retweet" target="_blank" href="#"> <%= ResHelper.GetString("CRC.Retweet") %> </a>&nbsp;|&nbsp;
                <a class="fav" target="_blank" href="#"><%= ResHelper.GetString("CRC.Favourite") %></a>&nbsp;|&nbsp;
                <a class="reply" target="_blank" href="#"><%= ResHelper.GetString("CRC.Reply") %></a>&nbsp;|&nbsp;
                <span class="timespan"></span>
            </p>

    </div>
</div>
</asp:PlaceHolder>