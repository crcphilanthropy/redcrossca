using System;
using System.Web.UI;

using CMS.Helpers;
using CMS.PortalControls;
using System.Web.UI.WebControls;

public partial class CMSWebParts_crc_webparts_PhotoListing : CMSAbstractWebPart
{
    /// <summary>
    /// Enables or disables resolving of inline controls.
    /// </summary>
    public bool ResolveDynamicControls
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ResolveDynamicControls"), true);
        }
        set
        {
            SetValue("ResolveDynamicControls", value);
        }
    }


    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            var pageSize = ValidationHelper.GetInteger(GetValue("PageSize"), -1);
            var pageNumber = URLHelper.GetQueryValue(URLHelper.GetAbsoluteUrl(RequestContext.CurrentURL), "page");
            var currentPage = pageNumber == "" ? 1 : ValidationHelper.GetInteger(pageNumber, 1);

            var nextUrl = URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (currentPage + 1).ToString()));
            var prevUrl = currentPage == 2 ? URLHelper.GetAbsoluteUrl(URLHelper.RemoveParameterFromUrl(RequestContext.CurrentURL, "page")) : URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (currentPage - 1).ToString()));

            if (currentPage > 1)
                Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='prev' href='{0}' />\n", prevUrl) });

            if (currentPage < pageSize)
                Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='next' href='{0}' />\n", nextUrl) });

        }
    }


    /// <summary>
    /// OnLoad handler
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        // Update update panel always 
        if (UpdatePanel != null)
        {
            UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;
        }

        base.OnLoad(e);
    }

}