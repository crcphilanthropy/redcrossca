using System;
using System.Collections.Generic;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.DocumentEngine;
using CMSApp.CRC.Entities;
using Newtonsoft.Json;
using CMS.DataEngine;
using CMS.Localization;

namespace CMSApp.CRC.webparts
{
    public partial class QuizBuilder : CMSAbstractWebPart
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //TransformationHelper.RegisterStartUpScript("initQuiz(quizQuestions);", Page, "Quiz");


            int totalResponsesPerQuestion = SettingsKeyInfoProvider.GetIntValue(SiteContext.CurrentSiteName + ".CRCQuizTotalResponses");

            // output Quiz docType properties, and then child QuizQuestion properties
            // ltrThankYou.Text = HttpUtility.HtmlEncode( CMSContext.CurrentDocument.GetValue("ThankYou").ToString());
            ltrBegin.Text = DocumentContext.CurrentDocument.GetStringValue("Begin", string.Empty);
            //  ltrIntroduction.Text = HttpUtility.HtmlEncode(CMSContext.CurrentDocument.GetValue("Introduction").ToString());
            ltrQuizId.Text = DocumentContext.CurrentDocument.DocumentID.ToString();

            TreeNodeDataSet nodeList = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, string.Format("{0}/%", DocumentContext.CurrentDocument.NodeAliasPath),
                                      LocalizationContext.PreferredCultureCode, false, "CRC.QuizQuestion",
                                                    "",
                                                    "NodeOrder",
                                                    TreeProvider.ALL_LEVELS,
                                                    true,
                                                    0);
            if (nodeList != null)
            {
                List<CrcQuizQuestion> quizQuestions = new List<CrcQuizQuestion>();
                foreach (CMS.DocumentEngine.TreeNode node in nodeList)
                {
                    // only display enabled questions
                    bool isEnabled = bool.Parse(node.GetValue("Enabled").ToString());
                    if (isEnabled)
                    {
                        CrcQuizQuestion question = new CrcQuizQuestion();
                        question.QuestionText = node.GetValue("Question").ToString();
                        question.ResponseId = int.Parse(node.GetValue("ResponseValue").ToString());
                        question.ResponseRationale = node.GetValue("ResponseRationale").ToString();

                        // build the list of responses
                        for (int x = 0; x < totalResponsesPerQuestion; x++)
                        {
                            var response = node.GetValue("Response" + (x + 1));
                            if (response != null)
                            {
                                question.ResponseList.Add(response.ToString());
                            }
                        }
                        quizQuestions.Add(question);
                    }
                }
                ltrQuizQuestions.Text = JsonConvert.SerializeObject(quizQuestions);
            }
            else
            {
                ltrQuizQuestions.Text = "\"\";";
            }
        }
    }
}