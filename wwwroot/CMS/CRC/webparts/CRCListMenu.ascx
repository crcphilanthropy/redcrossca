﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRCListMenu.ascx.cs" Inherits="CMSApp.CRC.webparts.CRCListMenu" EnableViewState="false" %>
<%@ Import Namespace="CMSApp.CRC" %>
<asp:Repeater runat="server" ID="rptListMenu" EnableViewState="False">
    <HeaderTemplate>
        <div class="main-nav">
            <div class="row">
                <div class="large-12 column">
                    <nav role="navigation">
                        <ul id="<%# ID %>" class="nav-menu clearfix inline-list right">
    </HeaderTemplate>
    <ItemTemplate>
        <li class="nav-item accessible-megamenu-top-nav-item <%# GetActive(Eval("NodeAliasPath").ToString()) %>">
            <div class="nav-wrap">
                <a href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath"), EvalText("DocumentMenuClass"), EvalText("DocumentMenuRedirectUrl")) %>" class="<%# Eval("DocumentMenuClass") %> cp-track" data-event="generic-event" data-label="<%# TransformationHelper.GetFormattedNavPath(Eval("DocumentUrlPath")) %>" data-category="navigation"  data-action="main nav">
                    <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                </a>
            </div>
            <asp:Repeater runat="server" ID="rptLevel2" EnableViewState="False">
                <HeaderTemplate>
                    <div class="sub-nav">
                        <ul class="sub-nav-group clearfix">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="<%# GetActive(Eval("NodeAliasPath").ToString()) %>"  >
                        <a id="sub-nav_<%# Eval("DocumentName").ToString().Replace(" ", "_") %>_Lv2" href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath"), EvalText("DocumentMenuClass"), EvalText("DocumentMenuRedirectUrl")) %>" class="<%# Eval("DocumentMenuClass") %> cp-track" data-event="generic-event" data-label="<%# TransformationHelper.GetFormattedNavPath(Eval("DocumentUrlPath")) %>" data-category="navigation"  data-action="main nav" >
                            <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                        </a>
                    </li>
                    <asp:Repeater runat="server" ID="rptLevel3" EnableViewState="False">
                        <ItemTemplate>
                            <li class="<%# string.Format("l3 {0}", GetActive(Eval("NodeAliasPath").ToString())) %> ">
                                <a id="sub-nav_<%# Eval("DocumentName").ToString().Replace(" ", "_")%>_Lv3" href="<%# TransformationHelper.GetDocumentUrl(Eval("DocumentUrlPath"), Eval("NodeAliasPath"), EvalText("DocumentMenuClass"), EvalText("DocumentMenuRedirectUrl")) %>" class="<%# Eval("DocumentMenuClass") %>cp-track"  data-event="generic-event" data-label="<%# TransformationHelper.GetFormattedNavPath(Eval("DocumentUrlPath")) %>" data-category="navigation" data-action="main nav" >
                                    <%# CMS.Controls.TransformationHelper.HelperObject.IfEmpty(Eval("DocumentMenuCaption"),Eval("DocumentName"),Eval("DocumentMenuCaption")) %>
                                </a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
                <SeparatorTemplate>
                    <asp:PlaceHolder runat="server" ID="phSeparator" EnableViewState="False">
                        </ul>
                        <ul class="sub-nav-group clearfix">
                    </asp:PlaceHolder>
                </SeparatorTemplate>
                <FooterTemplate>
                    </ul>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
        </nav>
                        </div>
                    </div>
                </div>
    </FooterTemplate>
</asp:Repeater>
