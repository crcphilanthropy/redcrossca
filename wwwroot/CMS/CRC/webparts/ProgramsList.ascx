<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramsList.ascx.cs" Inherits="CMSApp.CRC.WebParts.ProgramsList" EnableViewState="false" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Import Namespace="CMS.Localization" %>
<h2>
    <asp:Literal runat="server" ID="litTitle" />
</h2>
<asp:Repeater runat="server" ID="rptP" EnableViewState="False">
    <ItemTemplate>
        <div class="program">
            <a href="#"><%# Eval("Title") %></a>
            <div class="program-details">
                <p>
                    <%# Eval("Description") %>
                </p>
                <h5>
                    <%# ResHelper.GetString("CRC.SeeServicesIn") %>
                </h5>
                <asp:Repeater runat="server" ID="rptC" EnableViewState="False">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                           <a href="<%# GetCityPath( TransformationHelper.FormatCategoryDisplayName(TransformationHelper.GetProperty<string>(Container.DataItem as DataRow, "CategoryName"),"en-CA"),DataBinder.Eval(((RepeaterItem)Container.Parent.Parent).DataItem,"DocumentUrlPath") as string) %>"><%# TransformationHelper.FormatCategoryDisplayName(TransformationHelper.GetProperty<string>(Container.DataItem as DataRow, "CategoryDisplayName"),LocalizationContext.PreferredCultureCode) %></a>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
