using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.Controls;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.Helpers;
using CMS.Taxonomy;
using CMS.Localization;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC.webparts
{
    public partial class JobList : CMSAbstractWebPart
    {
        private string _sessionKeyJobsOrderByField = "Jobs_OrderByField";
        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("TransformationName"), string.Empty); }
            set { SetValue("TransformationName", value); }
        }

        public bool HideProvinceFilter
        {
            get { return ValidationHelper.GetBoolean(GetValue("HideProvinceFilter"), false); }
            set { SetValue("HideProvinceFilter", value); }
        }

        public bool SearchAllJobs
        {
            get { return ValidationHelper.GetBoolean(GetValue("SearchAllJobs"), false); }
            set { SetValue("SearchAllJobs", value); }
        }

        public bool SearchInProvince
        {
            get { return ValidationHelper.GetBoolean(GetValue("SearchInProvince"), false); }
            set { SetValue("SearchInProvince", value); }
        }

        public string Path
        {
            get { return ValidationHelper.GetString(GetValue("Path"), string.Empty); }
            set { SetValue("Path", value); }
        }

        /// <summary>
        /// Gets or sets the number of records to display on a page.
        /// </summary>
        public int PageSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PageSize"), 20);
            }
            set
            {
                SetValue("PageSize", value);
            }
        }


        /// <summary>
        /// Gets or sets the number of pages displayed for current page range.
        /// </summary>
        public int GroupSize
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("GroupSize"), 10);
            }
            set
            {
                SetValue("GroupSize", value);
            }
        }




        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitSortBy();
            InitProvinces();

            CRCPager.PageSize = PageSize;
            CRCPager.GroupSize = GroupSize;

            if (!IsPostBack)
                Session.Remove(_sessionKeyJobsOrderByField);

            DataBind();
        }

        private void InitSortBy()
        {
            ddlSortBy.Items.Clear();
            ddlSortBy.Items.Add(new ListItem(ResourceStringHelper.GetString("CRC.Sort.SortBy", "Sort By..."), "DocumentCreatedWhen DESC"));
            ddlSortBy.Items.Add(new ListItem(ResourceStringHelper.GetString("CRC.Sort.JobTitleAsc", "Job Title"), "Title ASC"));
            ddlSortBy.Items.Add(new ListItem(ResourceStringHelper.GetString("CRC.Sort.LocationAsc", "Location"), "Location ASC"));
            ddlSortBy.Items.Add(new ListItem(ResourceStringHelper.GetString("CRC.Sort.DeadlineAsc", "Application Deadline"), "ApplicationDeadline ASC"));
            ddlSortBy.Items.Add(new ListItem(ResourceStringHelper.GetString("CRC.Sort.DatePostedDesc", "Date Posted"), "DocumentCreatedWhen Desc"));

            ddlSortBy.SelectedIndexChanged += DdlSortBySelectedIndexChanged;
        }

        void DdlSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            Session[_sessionKeyJobsOrderByField] = ddlSortBy.SelectedValue;
        }


        public void HeaderClick(object sender, EventArgs e)
        {
            ClearSortHeaders();
            var lnk = ((TableHeaderLink)sender);
            lnk.CssClass = lnk.Direction = string.Equals("ASC", lnk.Direction, StringComparison.OrdinalIgnoreCase) ? "DESC" : "ASC";
            lnk.Attributes.Add("data-direction", lnk.Direction);
            Session[_sessionKeyJobsOrderByField] = string.Format("{0} {1}", lnk.OrderByField, lnk.Direction);
        }

        private void ClearSortHeaders()
        {
            lnkAppDeadline.CssClass = string.Empty;
            lnkDatePosted.CssClass = string.Empty;
            lnkLocation.CssClass = string.Empty;
            lnkTitle.CssClass = string.Empty;
            lnkType.CssClass = string.Empty;
        }

        private void InitProvinces()
        {
            phFilters.Visible = !HideProvinceFilter;
            if (HideProvinceFilter)
                return;

            var provinces = CacheHelper.Cache(() => CategoryInfoProvider.GetCategories(
                "CategoryParentID = (SELECT top 1 CategoryID FROM CMS_Category WHERE CategoryDisplayName LIKE '%National%' AND CategoryLEVEL = 1)",
                "CategoryDisplayName ASC"), new CacheSettings(Variables.CacheTimeInMinutes, "selectallprovinces"));

            var list = new List<ListItem> { new ListItem(" " + ResHelper.GetString("CRC.FiltherByProvince"), string.Empty) };
            foreach (var categoryInfo in provinces)
            {
                list.Add(new ListItem(ResHelper.LocalizeString(categoryInfo.CategoryDisplayName), categoryInfo.CategoryName));
            }

            list = list.OrderBy(item => item.Text).ToList();
            ProvinceDropDown.DataSource = list;
            ProvinceDropDown.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitJobsRepeater();
        }

        private void InitJobsRepeater()
        {
            var selectedProvince = ProvinceDropDown.SelectedValue;
            if (SearchInProvince && HideProvinceFilter)
                selectedProvince = GetProvinceCategoryCode();

            var orderBy = Session[_sessionKeyJobsOrderByField] as string ?? string.Empty;
            var whereClause = string.IsNullOrEmpty(selectedProvince)
                                  ? string.Empty
                                  : string.Format("Province = '{0}'", selectedProvince);

            rptJobs.ItemTemplate = CMSAbstractDataProperties.LoadTransformation(rptJobs, TransformationName);
            var ds = CacheHelper.Cache(() => TreeHelper.GetDocuments(
                SiteContext.CurrentSiteName,
                Path,
                LocalizationContext.PreferredCultureCode,
                false,
                "CRC.Job",
                whereClause,
                "DocumentModifiedWhen DESC",
                TreeProvider.ALL_LEVELS,
                true,
                -1), new CacheSettings(Variables.CacheTimeInMinutes, string.Format("InitJobsList_{0}_{1}_{2}", LocalizationContext.PreferredCultureCode, Path, whereClause)));

            DataView dv = null;
            if (!DataHelper.IsEmpty(ds))
            {
                var temp = ds.Copy();
                temp.Tables[0].Columns.Add("CareerTypeName");
                foreach (DataRow row in temp.Tables[0].Rows)
                {
                    row["CareerTypeName"] = TransformationHelper.GetCustomTableItemDisplayName(row["CareerType"],
                                                                                               "RedCross.CareerType",
                                                                                               "Name");
                }
                dv = temp.Tables[0].DefaultView;
                dv.Sort = orderBy;
                phNoResults.Visible = false;
            }
            else
            {
                phNoResults.Visible = true;
            }

            if (dv == null) return;

            CRCPager.DataSource = dv;
            CRCPager.Repeater = rptJobs;
            CRCPager.DataBind();
        }

        private string GetProvinceCategoryCode()
        {
            var parameters = new QueryDataParameters
                {
                    new DataParameter("DocumentCulture", LocalizationContext.PreferredCultureCode),
                    new DataParameter("NodeAliasPath", CurrentDocument.NodeAliasPath)
                };
            var items = CacheHelper.Cache(
                    () => new DataQuery("CRC.Province", "SelectCurrentProvinceCategory").Where(null, parameters)
                                                                                        .Columns("CategoryName")
                                                                                        .TopN(1)
                                                                                        .Execute(),
                    new CacheSettings(Variables.CacheTimeInMinutes, string.Format("GetProvinceCategoryCode_{0}_{1}", LocalizationContext.PreferredCultureCode, CurrentDocument.NodeAliasPath)));


            return DataHelper.DataSourceIsEmpty(items) ? string.Empty : ValidationHelper.GetString(items.Tables[0].Rows[0]["CategoryName"], string.Empty);
        }
    }
}