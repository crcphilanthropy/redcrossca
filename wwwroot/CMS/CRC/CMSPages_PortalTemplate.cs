﻿using System.Collections.Generic;
using CMS.DocumentEngine;
using CMS.Localization;
using CMS.PortalEngine;
using CMS.UIControls;
using System;
using CMSApp.CRC;

public partial class CMSPages_PortalTemplate
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        var page = Page as ContentPage;
        if (page == null) return;

        var culture = DocumentContext.CurrentDocument == null ? LocalizationContext.PreferredCultureCode : DocumentContext.CurrentDocument.DocumentCulture;

        if (string.IsNullOrEmpty(culture)) return;

        var list = culture.Split(new[] { '-' });

        if (list.Length > 0)
            page.XmlNamespace = string.Format("xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"{0}\" xml:lang=\"{0}\" ", list[0].ToLowerInvariant());

        if (DocumentContext.CurrentDocument != null)
            page.BodyClass = string.Format("{0} {1}", page.BodyClass, DocumentContext.CurrentDocument.NodeAlias.ToLower());

        if (PortalContext.ViewMode == ViewModeEnum.LiveSite)
            page.BodyClass = string.Format("{0} live-site", page.BodyClass);

        PreRender += CMSPages_PortalTemplate_PreRender;
    }

    void CMSPages_PortalTemplate_PreRender(object sender, EventArgs e)
    {        
        var document = DocumentContext.CurrentDocument;
        TransformationHelper.RegisterStartUpScript(string.Format("var navPath = '{0}';", document == null ? "/" : document.NodeAliasPath), Page, "loadMenu");
    }
}