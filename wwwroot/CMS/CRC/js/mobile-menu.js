"use strict";

if (typeof ECA === 'undefined' || !ECA) {
    var ECA = {};
}

ECA = {
    namespace: 'ECA',
    addModule: function (namespace, module) {
        var parts = namespace.split('.'),
            parent = ECA,
            i;

        var namespaceTarget = parts[parts.length - 1];

        if (parts[0] === ECA.namespace) {
            parts = parts.slice(1);
        }

        for (i = 0; i < parts.length; i += 1) {
            if (typeof parent[parts[i]] === "undefined") {
                parent[parts[i]] = {};
            }

            if (parts[i] === namespaceTarget) {
                parent[parts[i]] = module;
            }

            parent = parent[parts[i]];
        }

        return parent;
    }
}
"use strict";

ECA.addModule('util', (function () {

    var extend = function () {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    };

    var getBaseUrl = function () {
        var url = location.href;
        var baseURL = url.substring(0, url.indexOf('/', 14));

        if (baseURL.indexOf('http://localhost') != -1) {
            var url = location.href;
            var pathname = location.pathname;
            var index1 = url.indexOf(pathname);
            var index2 = url.indexOf("/", index1 + 1);
            var baseLocalUrl = url.substr(0, index2);
            return baseLocalUrl + "/";
        }
        else {
            return baseURL + "/";
        }
    };

    return {
        extend: extend,
        getBaseUrl: getBaseUrl
    };

})());
"use strict";

ECA.http = (function () {

    var globalBasicAuth = null;

    var getXHR = function () {
        if (typeof XMLHttpRequest !== 'undefined') {
            return new XMLHttpRequest();
        }
        var versions = [
            "MSXML2.XmlHttp.5.0",
            "MSXML2.XmlHttp.4.0",
            "MSXML2.XmlHttp.3.0",
            "MSXML2.XmlHttp.2.0",
            "Microsoft.XmlHttp"
        ];

        var xhr;
        for (var i = 0; i < versions.length; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            } catch (e) {
            }
        }
        return xhr;
    };

    var send = function (options) {
        var defaults = {
            url: undefined,
            method: 'GET',
            data: null,
            onSuccess: function () { },
            onError: function () { },
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var x = getXHR();

        x.open(params.method, params.url, true);


        for (var key in params.headers) {
            if (params.headers.hasOwnProperty(key)) {
                x.setRequestHeader(key, params.headers[key]);
            }
        }

        if (ECA.http.globalBasicAuth !== null) {
            x.setRequestHeader("Authorization", 'Basic ' + ECA.http.globalBasicAuth);
        }

        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                if (isResponseSuccessful(x)) {
                    params.onSuccess(x.responseText);
                }
                else {
                    params.onError(x.statusText);
                }
            }
        };
        if (params.method == 'POST') {
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        x.send(params.data);
    };

    var isResponseSuccessful = function (httpRequest) {
        var success = (httpRequest.status == 0 ||
            (httpRequest.status >= 200 && httpRequest.status < 300) ||
            httpRequest.status == 304 || httpRequest.status == 1223);
        return success;
    };

    var get = function (options) {
        var defaults = {
            url: undefined,
            data: null,
            onSuccess: undefined,
            onError: undefined,
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var query = [];
        for (var key in params.data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(params.data[key]));
        }

        send(params);
    };

    var post = function (options) {
        var defaults = {
            url: undefined,
            data: null,
            onSuccess: undefined,
            onError: undefined,
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var formattedData = [];
        for (var key in params.data) {
            formattedData.push(encodeURIComponent(key) + '=' + encodeURIComponent(params.data[key]));
        }

        params.data = formattedData;

        send(params);
    };

    return {
        globalBasicAuth: globalBasicAuth,
        getXHR: getXHR,
        send: send,
        get: get,
        post: post
    };

})();



"use strict";

ECA.addModule('kentico.mobilemenu', (function () {

    var serviceUrl = '';
    var menuStore = [];

    var getMenuAtPath = function (path, options, callback) {

        var response = findMenuAtPath(path);
        
        if (response === null) {
            callServiceUrl(path, options, callback);
        } else {
            callback(response);
        }
    };
    

    var findMenuAtPath = function (path) {
        if (menuStore.length > 0) {
            for (var i = 0; i < menuStore.length; i++) {
                if (path === menuStore[i].path) {
                    console.log("Load from cache");
                    return menuStore[i];
                }
            }
        }
        return null;
    };

    var callServiceUrl = function(path, options, callback) {
        var requestUrl = ECA.kentico.mobilemenu.serviceUrl + '?options=' + JSON.stringify(options);

        ECA.http.get({
            url: requestUrl,
            onSuccess: function (data) {
                var responseObject = JSON.parse(data);
                responseObject.path = path;
                menuStore.push(responseObject);
                callback(responseObject);
            }
        });
    };

    return {
        getMenuAtPath : getMenuAtPath,
        serviceUrl: serviceUrl
    };

})());
"use strict";

ECA.addModule('template', (function() {
    var render = function(html, options) {
        var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0, match;
        var add = function(line, js) {
            js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
            return add;
        }
        while (match = re.exec(html)) {
            add(html.slice(cursor, match.index))(match[1], true);
            cursor = match.index + match[0].length;
        }
        add(html.substr(cursor, html.length - cursor));
        code += 'return r.join("");';
        return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
    };
    
    return {
        render: render
    };

})());
window.onload = initE2eTest;

function initE2eTest() {

    ECA.kentico.mobilemenu.serviceUrl = '/API/Menu';

    var backButtonTemplate = "<a class='get-menu nav-back nav-title' data-path='<%this.path%>'><%this.title%></a>";
    var currentNodeTemplate = "<li class=\"parent\"><a data-path='<%this.path%>' href='<%this.url%>'> <%this.title%></a></li>";
    var template = "<%for(var index in this.navigationItemList) {%>" +
        "<li>" +
        "<% if(this.navigationItemList[index].hasChildren) {%>" +
        "<a class='get-menu' href='<%this.navigationItemList[index].url%>' data-path='<%this.navigationItemList[index].path%>'><%this.navigationItemList[index].title%></a>" +
        "<%}else{%>" +
        "<a data-path='<%this.path%>' href='<%this.navigationItemList[index].url%>'><%this.navigationItemList[index].title%></a>" +
        "<%}%>" +
        "</li>" +
        "<%}%>";
    var lang = $('body').hasClass('FRCA') ? 'fr-CA' : 'en-CA';
    var isIE8 = $('body').hasClass('IE8');

    function loadMenu() {
        if (isIE8) return;

        var path = navPath;
        if (typeof this.getAttribute === "function") {
            path = this.getAttribute('data-path');
        }

         var options = {
            path: path,
            orderByCondition: "NodeOrder ASC",
            //andWhereCondition: ["ExcludeInMobileNav <> 1"],
            orWhereCondition: [],
            classNames: ["CRC.Page", "CRC.Appeal", "CRC.Province", "CRC.IYCBranchFolder", "CMS.Blog", "CRC.Quiz"],
            backButtonResourceString: 'crc.nav.back',
            checkPermissions: false,
            useCustomFields: false,
            lang: lang
         };

         ECA.kentico.mobilemenu.getMenuAtPath(path, options, function (data) {
            var html = '';
            if (data.parentNode != undefined) {

                // Slide Animation
                document.getElementById('menu').classList.remove("toggle");

                setTimeout(function () {
                    document.getElementById('menu').classList.add("toggle");
                }, 50);

                // Back Button JS
                document.getElementById('Back').innerHTML = ECA.template.render(backButtonTemplate, data.parentNode);

            } else {

                // Slide Animation
                document.getElementById('menu').classList.remove("toggle");

                setTimeout(function () {
                    document.getElementById('menu').classList.add("toggle");
                }, 50);

                // Back Button JS
                document.getElementById('Back').innerHTML = "";

            }
            if (data.currentNode != undefined && data.parentNode != undefined)
                html += ECA.template.render(currentNodeTemplate, data.currentNode);

            html += ECA.template.render(template, data); 
            document.getElementById('menu').innerHTML = html;
            
            
            // Loop navigation items to current item 
            var currentStateNode = data.currentStateNode.url,
                navItems = document.getElementById('menu');
             
            var currentItem = $('li', navItems).map(function(){ // loop all nav items
               var path = $('a', this).attr('href').replace(/\.[^/.]+$/, ""); // Replace .aspx extesions
               if (path === currentStateNode){
                  return this; // Set currentItem to this
               }
            });
            
            if(currentItem) // Add class to current item
               currentItem.addClass('current');
            

            // Add click events to items with lower level menus
            var elements = document.getElementsByClassName("get-menu");
            for (var i = 0; i < elements.length; i++) {               
                
                elements[i].addEventListener('click', function (e) {
                    e.preventDefault();
                    this.classList.add('loading');
                });

                elements[i].addEventListener("click", loadMenu);

            }
            
           // Dispatch a menu onLoaded event 
           if ($.isFunction($.fn.ecaMenuLoaded)) {
               $.fn.ecaMenuLoaded();
           }          
         
            
        });
    }

    try {
        loadMenu();
    } catch(e) {
        //console.log(e);
    }

}