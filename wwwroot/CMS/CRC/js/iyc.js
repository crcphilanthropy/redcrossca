﻿
var iyc = {
    geocoder: new google.maps.Geocoder(),

    init: function () {
        this.setButtonState();
        setTimeout(function () {
            iyc.setButtonState();
        }, 2000);

        $('.iyc-filter input[type="text"], .iyc-filter select').change(function () {
            iyc.setButtonState();
        });

        $('.iyc-filter input[type="text"], .iyc-filter select').blur(function () {
            iyc.setButtonState();
        });

        $('.iyc-filter input[type="text"]').keypress(function (evt) {
            if (iyc.setButtonState()) {
                var keyId = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
                if (keyId == 13) {
                    iyc.submit();
                    return false;
                }
            }
            return true;
        });

        $('.iyc-filter input[type="text"]').keyup(function(evt) {
            iyc.setButtonState();
        });

        $('.iyc-filter-button').click(function () {
            iyc.submit();
            return false;
        });

        if ($('.subcategory-select option').length > 0) {
            $('.search-wrap').removeClass('optional-field');
        }

        $('.category-select').change(function () {
            $('.subcategory-select').empty();
            var categoryGuid = $(this).val();
            if (categoryGuid == '' || categoryGuid == undefined) {
                $('.search-wrap').addClass('optional-field');
                return;
            }

            $.get('/api/iyc/getsubcategories?categoryGuid=' + categoryGuid, function (data) {
                if (data.length == 0)
                    $('.search-wrap').addClass('optional-field');
                else
                    $('.search-wrap').removeClass('optional-field');

                $(data).each(function () {
                    $('.subcategory-select').append($('<option>', { value: this.DocumentGuid }).text(this.Name));
                });
            });
        });
    },

    submit: function () {
        $('.location-error').hide();
        var address = $('.iyc-location').pVal();
        if (address.length < 3) {
            $('.iyc-filter-button-submit').click();
            return false;
        }

        iyc.getLongLat(address).then(function (location) {
            $('#hidLatitude').val(location.lat()); //lat
            $('#hidLongitude').val(location.lng()); //long
            $('.iyc-filter-button-submit').click();
        }).fail(function (error) {
            $('.location-error').show();
            $('.iyc-result').hide();
        });
    },

    setButtonState: function () {
        var isValid = false;
        $('.iyc-filter-button').prop('disabled', true);
        $('.iyc-filter-button-submit').prop('disabled', true);
        $('.search-wrap').addClass('disabled');
        $('.iyc-filter input[type="text"], .iyc-filter select').each(function () {
            if ($(this).val() != '00000000-0000-0000-0000-000000000000' && $(this).val() != undefined && $(this).val().length >= 3) {
                $('.iyc-filter-button').prop('disabled', false);
                $('.iyc-filter-button-submit').prop('disabled', false);
                $('.search-wrap').removeClass('disabled');
                isValid = true;
            }
        });

        return isValid;
    },

    getCurrentLocation: function () {
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.successFunction, this.errorFunction, options);
        }
    },

    successFunction: function (position) {
        try {
            geocoder = new google.maps.Geocoder();
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            iyc.getAddress(lat, lng).then(function (city) {
                $('.result-pivot-location').val(city.formatted_address);
            });
        } catch (e) {
        }
    },

    errorFunction: function (err) {
        try {
            console.warn('ERROR(' + err.code + '): ' + err.message);
        } catch (e) { }
    },

    getLongLat: function (address) {
        var deferred = $.Deferred();
        this.geocoder.geocode({ 'address': address, 'region': 'CA' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                deferred.resolve(results[0].geometry.location);
            } else {
                deferred.reject(status);
            }
        });
        return deferred.promise();
    },

    getAddress: function (lat, lng) {

        var deferred = $.Deferred();
        var latlng = new google.maps.LatLng(lat, lng);
        this.geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var city = results[0];
                for (var x = 0; x < results.length; x++) {
                    for (var a = 0; a < results[x].types.length && a < 1; a++) {
                        if (results[x].types[a] == "locality") { //street_address , locality, sublocality, neighborhood, political, postal_code,postal_code_prefix
                            city = results[x];
                            break;
                        }
                    }
                }
                deferred.resolve(city);
            } else {
                deferred.reject(status);
            }
        });
        return deferred.promise();
    },

    setPivotLocation: function (lat, lng) {
        iyc.getAddress(lat, lng).then(function (city) {
            $('.result-pivot-location').html(city.formatted_address);
            $('.result-pivot').show();
        });
    },

    setNoResult: function (lat, lng, maxDistance) {
        iyc.getAddress(lat, lng).then(function (city) {
            $('.result-pivot-location').html(city.formatted_address);
            $('.result-pivot-max-distance').html(maxDistance + 'km');
            $('.no-result-pivot').show();
        });
    },

    getStaticMapUrl: function (lat, lng) {
        return "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=14&size=400x400&maptype=roadmap&markers=color:red%7Clabel:%7C" + lat + "," + lng;
    }
};

$.fn.pVal = function () {
    var $this = $(this),
        val = $this.eq(0).val();
    if (val == $this.attr('placeholder'))
        return '';
    else
        return val;
}