/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals.
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function raw(s) {
        return s;
    }

    function decoded(s) {
        return decodeURIComponent(s.replace(pluses, ' '));
    }

    function converted(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }
        try {
            return config.json ? JSON.parse(s) : s;
        } catch(er) {}
    }

    var config = $.cookie = function (key, value, options) {

        // write
        if (value !== undefined) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = config.json ? JSON.stringify(value) : String(value);

            return (document.cookie = [
                config.raw ? key : encodeURIComponent(key),
                '=',
                config.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // read
        var decode = config.raw ? raw : decoded;
        var cookies = document.cookie.split('; ');
        var result = key ? undefined : {};
        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = decode(parts.join('='));

            if (key && key === name) {
                result = converted(cookie);
                break;
            }

            if (!key) {
                result[name] = converted(cookie);
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) !== undefined) {
            // Must not alter options, thus extending a fresh object...
            $.cookie(key, '', $.extend({}, options, { expires: -1 }));
            return true;
        }
        return false;
    };

}));

/*
 * jQuery jFontSize Plugin
 * Examples and documentation: http://jfontsize.com
 * Author: Frederico Soares Vanelli
 *         fredsvanelli@gmail.com
 *         http://twitter.com/fredvanelli
 *         http://facebook.com/fred.vanelli
 *
 * Copyright (c) 2011
 * Version: 1.0 (2011-07-13)
 * Dual licensed under the MIT and GPL licenses.
 * http://jfontsize.com/license
 * Requires: jQuery v1.2.6 or later
 */
(function ($) {
    $.fn.jfontsize = function (opcoes) {
        var $this = $(this);
        var defaults = { btnMinusClasseId: '#jfontsize-minus', btnDefaultClasseId: '#jfontsize-default', btnPlusClasseId: '#jfontsize-plus', btnMinusMaxHits: 10, btnPlusMaxHits: 10, sizeChange: 1 };
        if (opcoes) {
            opcoes = $.extend(defaults, opcoes)
        }
        var limite = new Array();
        var fontsize;
        var fontsize_padrao = new Array();
        var fontSizeLimit = 0;
        
        if ($.cookie('fontsize_padrao') != undefined && $.cookie('fontsize_limite') != undefined &&  $.cookie('fontsize') != undefined) {
            fontsize_padrao = $.cookie('fontsize_padrao').split(/,/);
            limite = $.cookie('fontsize_limite').split(/,/);
            fontsize = parseInt($.cookie('fontsize'));
            $this.each(function (i) {
                
                $(this).css('font-size', fontsize+ 'px');
            })
        }
        else {
            $(this).each(function (i) {
                limite[i] = fontSizeLimit;
                fontsize_padrao[i];
            });
        }
        $(opcoes.btnMinusClasseId + ', ' + opcoes.btnDefaultClasseId + ', ' + opcoes.btnPlusClasseId).removeAttr('href');
        $(opcoes.btnMinusClasseId + ', ' + opcoes.btnDefaultClasseId + ', ' + opcoes.btnPlusClasseId).css('cursor', 'pointer');
        $(opcoes.btnMinusClasseId).click(function () {
            $(opcoes.btnPlusClasseId).removeClass('jfontsize-disabled');
            $this.each(function (i) {
                if (limite[i] > (-(opcoes.btnMinusMaxHits))) {
                    fontsize_padrao[i] = $(this).css('font-size');
                    fontsize_padrao[i] = fontsize_padrao[i].replace('px', '');
                    fontsize = $(this).css('font-size');
                    fontsize = parseInt(fontsize.replace('px', ''));
                    fontsize = fontsize - (opcoes.sizeChange);
                    fontsize_padrao[i] = fontsize_padrao[i] - (limite[i] * opcoes.sizeChange);
                    limite[i]--;
                    $(this).css('font-size', fontsize + 'px');
                    if (limite[i] == (-(opcoes.btnMinusMaxHits))) {
                        $(opcoes.btnMinusClasseId).addClass('jfontsize-disabled');
                    }
                   
                    
                }
            })
            $.cookie('fontsize', fontsize, { expires: 7, path: '/' });
            $.cookie('fontsize_padrao', fontsize_padrao, { expires: 7, path: '/' });
            $.cookie('fontsize_limite', limite, { expires: 7, path: '/' });
        });
        $(opcoes.btnDefaultClasseId).click(function () {
            $(opcoes.btnMinusClasseId).removeClass('jfontsize-disabled');
            $(opcoes.btnPlusClasseId).removeClass('jfontsize-disabled');
            $this.each(function (i) {
                limite[i] = 0;
                $(this).css('font-size', fontsize_padrao[i] + 'px');
            })
        });
        $(opcoes.btnPlusClasseId).click(function () {
            $(opcoes.btnMinusClasseId).removeClass('jfontsize-disabled');
            $this.each(function (i) {
                if (limite[i] < opcoes.btnPlusMaxHits) {
                    fontsize_padrao[i] = $(this).css('font-size');
                    fontsize_padrao[i] = fontsize_padrao[i].replace('px', '');
                    fontsize = $(this).css('font-size');
                    fontsize = parseInt(fontsize.replace('px', ''));
                    fontsize = fontsize + opcoes.sizeChange;
                    fontsize_padrao[i] = fontsize_padrao[i] - (limite[i] * opcoes.sizeChange);
                    limite[i]++;
                    $(this).css('font-size', fontsize + 'px');
                    if (limite[i] == opcoes.btnPlusMaxHits) {
                        $(opcoes.btnPlusClasseId).addClass('jfontsize-disabled');
                    }
                    $.cookie('font_size', fontsize);
                }
            })
            $.cookie('fontsize', fontsize, { expires: 7, path: '/' });
            $.cookie('fontsize_padrao', fontsize_padrao, { expires: 7, path: '/' });
            $.cookie('fontsize_limite', limite, { expires: 7, path: '/' });
        });
    };
})(jQuery);

/*
 * jQuery Easing v1.3
 */

;
eval(function (p, a, c, k, e, r) {
    e = function (c) {
        return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
    };

    if (!''.replace(/^/, String)) {
        while (c--) r[e(c)] = k[c] || e(c);
        k = [function (e) {
            return r[e]
        }];
        e = function () {
            return '\\w+'
        };

        c = 1
    }
    ;
    while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
    return p
}('h.i[\'1a\']=h.i[\'z\'];h.O(h.i,{y:\'D\',z:9(x,t,b,c,d){6 h.i[h.i.y](x,t,b,c,d)},17:9(x,t,b,c,d){6 c*(t/=d)*t+b},D:9(x,t,b,c,d){6-c*(t/=d)*(t-2)+b},13:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t+b;6-c/2*((--t)*(t-2)-1)+b},X:9(x,t,b,c,d){6 c*(t/=d)*t*t+b},U:9(x,t,b,c,d){6 c*((t=t/d-1)*t*t+1)+b},R:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t+b;6 c/2*((t-=2)*t*t+2)+b},N:9(x,t,b,c,d){6 c*(t/=d)*t*t*t+b},M:9(x,t,b,c,d){6-c*((t=t/d-1)*t*t*t-1)+b},L:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t*t+b;6-c/2*((t-=2)*t*t*t-2)+b},K:9(x,t,b,c,d){6 c*(t/=d)*t*t*t*t+b},J:9(x,t,b,c,d){6 c*((t=t/d-1)*t*t*t*t+1)+b},I:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t*t*t+b;6 c/2*((t-=2)*t*t*t*t+2)+b},G:9(x,t,b,c,d){6-c*8.C(t/d*(8.g/2))+c+b},15:9(x,t,b,c,d){6 c*8.n(t/d*(8.g/2))+b},12:9(x,t,b,c,d){6-c/2*(8.C(8.g*t/d)-1)+b},Z:9(x,t,b,c,d){6(t==0)?b:c*8.j(2,10*(t/d-1))+b},Y:9(x,t,b,c,d){6(t==d)?b+c:c*(-8.j(2,-10*t/d)+1)+b},W:9(x,t,b,c,d){e(t==0)6 b;e(t==d)6 b+c;e((t/=d/2)<1)6 c/2*8.j(2,10*(t-1))+b;6 c/2*(-8.j(2,-10*--t)+2)+b},V:9(x,t,b,c,d){6-c*(8.o(1-(t/=d)*t)-1)+b},S:9(x,t,b,c,d){6 c*8.o(1-(t=t/d-1)*t)+b},Q:9(x,t,b,c,d){e((t/=d/2)<1)6-c/2*(8.o(1-t*t)-1)+b;6 c/2*(8.o(1-(t-=2)*t)+1)+b},P:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d)==1)6 b+c;e(!p)p=d*.3;e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);6-(a*8.j(2,10*(t-=1))*8.n((t*d-s)*(2*8.g)/p))+b},H:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d)==1)6 b+c;e(!p)p=d*.3;e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);6 a*8.j(2,-10*t)*8.n((t*d-s)*(2*8.g)/p)+c+b},T:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d/2)==2)6 b+c;e(!p)p=d*(.3*1.5);e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);e(t<1)6-.5*(a*8.j(2,10*(t-=1))*8.n((t*d-s)*(2*8.g)/p))+b;6 a*8.j(2,-10*(t-=1))*8.n((t*d-s)*(2*8.g)/p)*.5+c+b},F:9(x,t,b,c,d,s){e(s==u)s=1.l;6 c*(t/=d)*t*((s+1)*t-s)+b},E:9(x,t,b,c,d,s){e(s==u)s=1.l;6 c*((t=t/d-1)*t*((s+1)*t+s)+1)+b},16:9(x,t,b,c,d,s){e(s==u)s=1.l;e((t/=d/2)<1)6 c/2*(t*t*(((s*=(1.B))+1)*t-s))+b;6 c/2*((t-=2)*t*(((s*=(1.B))+1)*t+s)+2)+b},A:9(x,t,b,c,d){6 c-h.i.v(x,d-t,0,c,d)+b},v:9(x,t,b,c,d){e((t/=d)<(1/2.k)){6 c*(7.q*t*t)+b}m e(t<(2/2.k)){6 c*(7.q*(t-=(1.5/2.k))*t+.k)+b}m e(t<(2.5/2.k)){6 c*(7.q*(t-=(2.14/2.k))*t+.11)+b}m{6 c*(7.q*(t-=(2.18/2.k))*t+.19)+b}},1b:9(x,t,b,c,d){e(t<d/2)6 h.i.A(x,t*2,0,c,d)*.5+b;6 h.i.v(x,t*2-d,0,c,d)*.5+c*.5+b}});', 62, 74, '||||||return||Math|function|||||if|var|PI|jQuery|easing|pow|75|70158|else|sin|sqrt||5625|asin|||undefined|easeOutBounce|abs||def|swing|easeInBounce|525|cos|easeOutQuad|easeOutBack|easeInBack|easeInSine|easeOutElastic|easeInOutQuint|easeOutQuint|easeInQuint|easeInOutQuart|easeOutQuart|easeInQuart|extend|easeInElastic|easeInOutCirc|easeInOutCubic|easeOutCirc|easeInOutElastic|easeOutCubic|easeInCirc|easeInOutExpo|easeInCubic|easeOutExpo|easeInExpo||9375|easeInOutSine|easeInOutQuad|25|easeOutSine|easeInOutBack|easeInQuad|625|984375|jswing|easeInOutBounce'.split('|'), 0, {}))

/* ===================================================
 * bootstrap-transition.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 */

!function ($) {

    "use strict"; // jshint ;_;


    /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
     * ======================================================= */

    $(function () {

        $.support.transition = (function () {

            var transitionEnd = (function () {

                var el = document.createElement('bootstrap')
                    , transEndEventNames = {
                        'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd otransitionend', 'transition': 'transitionend'
                    }
                    , name

                for (name in transEndEventNames) {
                    if (el.style[name] !== undefined) {
                        return transEndEventNames[name]
                    }
                }

            }())

            return transitionEnd && {
                end: transitionEnd
            }

        })()

    })

}(window.jQuery);

/* =========================================================
 * bootstrap-modal.js v2.3.0
 * http://twitter.github.com/bootstrap/javascript.html#modals
 * =========================================================
 */

!function ($) {

    "use strict"; // jshint ;_;

    var Modal = function (element, options) {
        this.options = options
        this.$element = $(element)
            .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
        this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
    }

    Modal.prototype = {

        constructor: Modal, toggle: function () {
            return this[!this.isShown ? 'show' : 'hide']()
        }, show: function () {
            var that = this
                , e = $.Event('show')

            this.$element.trigger(e)

            if (this.isShown || e.isDefaultPrevented()) return

            this.isShown = true

            this.escape()

            this.backdrop(function () {
                var transition = $.support.transition && that.$element.hasClass('fade')

                if (!that.$element.parent().length) {
                    that.$element.appendTo(document.body) //don't move modals dom position
                }

                that.$element.show()

                if (transition) {
                    that.$element[0].offsetWidth // force reflow
                }

                that.$element
                    .addClass('in')
                    .attr('aria-hidden', false)

                that.enforceFocus()

                transition ?
                    that.$element.one($.support.transition.end, function () {
                        that.$element.focus().trigger('shown')
                    }) :
                    that.$element.focus().trigger('shown')

            })
        }, hide: function (e) {
            e && e.preventDefault()

            var that = this

            e = $.Event('hide')

            this.$element.trigger(e)

            if (!this.isShown || e.isDefaultPrevented()) return

            this.isShown = false

            this.escape()

            $(document).off('focusin.modal')

            this.$element
                .removeClass('in')
                .attr('aria-hidden', true)

            $.support.transition && this.$element.hasClass('fade') ?
                this.hideWithTransition() :
                this.hideModal()
        }, enforceFocus: function () {
            var that = this
            $(document).on('focusin.modal', function (e) {
                if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
                    that.$element.focus()
                }
            })
        }, escape: function () {
            var that = this
            if (this.isShown && this.options.keyboard) {
                this.$element.on('keyup.dismiss.modal', function (e) {
                    e.which == 27 && that.hide()
                })
            } else if (!this.isShown) {
                this.$element.off('keyup.dismiss.modal')
            }
        }, hideWithTransition: function () {
            var that = this
                , timeout = setTimeout(function () {
                    that.$element.off($.support.transition.end)
                    that.hideModal()
                }, 500)

            this.$element.one($.support.transition.end, function () {
                clearTimeout(timeout)
                that.hideModal()
            })
        }, hideModal: function () {
            var that = this
            this.$element.hide()
            this.backdrop(function () {
                that.removeBackdrop()
                that.$element.trigger('hidden')
            })
        }, removeBackdrop: function () {
            this.$backdrop.remove()
            this.$backdrop = null
        }, backdrop: function (callback) {
            var that = this
                , animate = this.$element.hasClass('fade') ? 'fade' : ''

            if (this.isShown && this.options.backdrop) {
                var doAnimate = $.support.transition && animate

                this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
                    .appendTo(document.body)

                this.$backdrop.click(
                    this.options.backdrop == 'static' ?
                        $.proxy(this.$element[0].focus, this.$element[0])
                        : $.proxy(this.hide, this)
                )

                if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

                this.$backdrop.addClass('in')

                if (!callback) return

                doAnimate ?
                    this.$backdrop.one($.support.transition.end, callback) :
                    callback()

            } else if (!this.isShown && this.$backdrop) {
                this.$backdrop.removeClass('in')

                $.support.transition && this.$element.hasClass('fade') ?
                    this.$backdrop.one($.support.transition.end, callback) :
                    callback()

            } else if (callback) {
                callback()
            }
        }
    }

    var old = $.fn.modal

    $.fn.modal = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('modal')
                , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('modal', (data = new Modal(this, options)))
            if (typeof option == 'string') data[option]()
            else if (options.show) data.show()
        })
    }

    $.fn.modal.defaults = {
        backdrop: true, keyboard: true, show: true
    }

    $.fn.modal.Constructor = Modal

    $.fn.modal.noConflict = function () {
        $.fn.modal = old
        return this
    }

    $(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
        var $this = $(this)
            , href = $this.attr('href')
            , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
            , option = $target.data('modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

        e.preventDefault()

        $target
            .modal(option)
            .one('hide', function () {
                $this.focus()
            })
    })

}(window.jQuery);

/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function (a) {
    function d(b) {
        var c = b || window.event, d = [].slice.call(arguments, 1), e = 0, f = !0, g = 0, h = 0;
        return b = a.event.fix(c), b.type = "mousewheel", c.wheelDelta && (e = c.wheelDelta / 120), c.detail && (e = -c.detail / 3), h = e, c.axis !== undefined && c.axis === c.HORIZONTAL_AXIS && (h = 0, g = -1 * e), c.wheelDeltaY !== undefined && (h = c.wheelDeltaY / 120), c.wheelDeltaX !== undefined && (g = -1 * c.wheelDeltaX / 120), d.unshift(b, e, g, h), (a.event.dispatch || a.event.handle).apply(this, d)
    }

    var b = ["DOMMouseScroll", "mousewheel"];
    if (a.event.fixHooks) for (var c = b.length; c;) a.event.fixHooks[b[--c]] = a.event.mouseHooks;
    a.event.special.mousewheel = { setup: function () {
        if (this.addEventListener) for (var a = b.length; a;) this.addEventListener(b[--a], d, !1); else this.onmousewheel = d
    }, teardown: function () {
        if (this.removeEventListener) for (var a = b.length; a;) this.removeEventListener(b[--a], d, !1); else this.onmousewheel = null
    } }, a.fn.extend({ mousewheel: function (a) {
        return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
    }, unmousewheel: function (a) {
        return this.unbind("mousewheel", a)
    } })
})(jQuery);

/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.3.0
 *
 */
(function(f){jQuery.fn.extend({slimScroll:function(h){var a=f.extend({width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:0.4,alwaysVisible:!1,disableFadeOut:!1,railVisible:!1,railColor:"#333",railOpacity:0.2,railDraggable:!0,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:!1,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px"},h);this.each(function(){function r(d){if(s){d=d||
    window.event;var c=0;d.wheelDelta&&(c=-d.wheelDelta/120);d.detail&&(c=d.detail/3);f(d.target||d.srcTarget||d.srcElement).closest("."+a.wrapperClass).is(b.parent())&&m(c,!0);d.preventDefault&&!k&&d.preventDefault();k||(d.returnValue=!1)}}function m(d,f,h){k=!1;var e=d,g=b.outerHeight()-c.outerHeight();f&&(e=parseInt(c.css("top"))+d*parseInt(a.wheelStep)/100*c.outerHeight(),e=Math.min(Math.max(e,0),g),e=0<d?Math.ceil(e):Math.floor(e),c.css({top:e+"px"}));l=parseInt(c.css("top"))/(b.outerHeight()-c.outerHeight());
    e=l*(b[0].scrollHeight-b.outerHeight());h&&(e=d,d=e/b[0].scrollHeight*b.outerHeight(),d=Math.min(Math.max(d,0),g),c.css({top:d+"px"}));b.scrollTop(e);b.trigger("slimscrolling",~~e);v();p()}function C(){window.addEventListener?(this.addEventListener("DOMMouseScroll",r,!1),this.addEventListener("mousewheel",r,!1),this.addEventListener("MozMousePixelScroll",r,!1)):document.attachEvent("onmousewheel",r)}function w(){u=Math.max(b.outerHeight()/b[0].scrollHeight*b.outerHeight(),D);c.css({height:u+"px"});
    var a=u==b.outerHeight()?"none":"block";c.css({display:a})}function v(){w();clearTimeout(A);l==~~l?(k=a.allowPageScroll,B!=l&&b.trigger("slimscroll",0==~~l?"top":"bottom")):k=!1;B=l;u>=b.outerHeight()?k=!0:(c.stop(!0,!0).fadeIn("fast"),a.railVisible&&g.stop(!0,!0).fadeIn("fast"))}function p(){a.alwaysVisible||(A=setTimeout(function(){a.disableFadeOut&&s||(x||y)||(c.fadeOut("slow"),g.fadeOut("slow"))},1E3))}var s,x,y,A,z,u,l,B,D=30,k=!1,b=f(this);if(b.parent().hasClass(a.wrapperClass)){var n=b.scrollTop(),
    c=b.parent().find("."+a.barClass),g=b.parent().find("."+a.railClass);w();if(f.isPlainObject(h)){if("height"in h&&"auto"==h.height){b.parent().css("height","auto");b.css("height","auto");var q=b.parent().parent().height();b.parent().css("height",q);b.css("height",q)}if("scrollTo"in h)n=parseInt(a.scrollTo);else if("scrollBy"in h)n+=parseInt(a.scrollBy);else if("destroy"in h){c.remove();g.remove();b.unwrap();return}m(n,!1,!0)}}else{a.height="auto"==a.height?b.parent().height():a.height;n=f("<div></div>").addClass(a.wrapperClass).css({position:"relative",
    overflow:"hidden",width:a.width,height:a.height});b.css({overflow:"hidden",width:a.width,height:a.height});var g=f("<div></div>").addClass(a.railClass).css({width:a.size,height:"100%",position:"absolute",top:0,display:a.alwaysVisible&&a.railVisible?"block":"none","border-radius":a.railBorderRadius,background:a.railColor,opacity:a.railOpacity,zIndex:90}),c=f("<div></div>").addClass(a.barClass).css({background:a.color,width:a.size,position:"absolute",top:0,opacity:a.opacity,display:a.alwaysVisible?
    "block":"none","border-radius":a.borderRadius,BorderRadius:a.borderRadius,MozBorderRadius:a.borderRadius,WebkitBorderRadius:a.borderRadius,zIndex:99}),q="right"==a.position?{right:a.distance}:{left:a.distance};g.css(q);c.css(q);b.wrap(n);b.parent().append(c);b.parent().append(g);a.railDraggable&&c.bind("mousedown",function(a){var b=f(document);y=!0;t=parseFloat(c.css("top"));pageY=a.pageY;b.bind("mousemove.slimscroll",function(a){currTop=t+a.pageY-pageY;c.css("top",currTop);m(0,c.position().top,!1)});
    b.bind("mouseup.slimscroll",function(a){y=!1;p();b.unbind(".slimscroll")});return!1}).bind("selectstart.slimscroll",function(a){a.stopPropagation();a.preventDefault();return!1});g.hover(function(){v()},function(){p()});c.hover(function(){x=!0},function(){x=!1});b.hover(function(){s=!0;v();p()},function(){s=!1;p()});b.bind("touchstart",function(a,b){a.originalEvent.touches.length&&(z=a.originalEvent.touches[0].pageY)});b.bind("touchmove",function(b){k||b.originalEvent.preventDefault();b.originalEvent.touches.length&&
(m((z-b.originalEvent.touches[0].pageY)/a.touchScrollStep,!0),z=b.originalEvent.touches[0].pageY)});w();"bottom"===a.start?(c.css({top:b.outerHeight()-c.outerHeight()}),m(0,!0)):"top"!==a.start&&(m(f(a.start).position().top,null,!0),a.alwaysVisible||c.hide());C()}});return this}});jQuery.fn.extend({slimscroll:jQuery.fn.slimScroll})})(jQuery);

/*! perfect-scrollbar - v0.5.5
 * http://noraesae.github.com/perfect-scrollbar/
 * Copyright (c) 2014 Hyunje Alex Jun; Licensed MIT */
(function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof exports?e(require("jquery")):e(jQuery)})(function(e){"use strict";function o(e){return"string"==typeof e?parseInt(e,10):~~e}var t={wheelSpeed:1,wheelPropagation:!1,minScrollbarLength:null,maxScrollbarLength:null,useBothWheelAxes:!1,useKeyboard:!0,suppressScrollX:!1,suppressScrollY:!1,scrollXMarginOffset:0,scrollYMarginOffset:0,includePadding:!1},n=function(){var e=0;return function(){var o=e;return e+=1,".perfect-scrollbar-"+o}}();e.fn.perfectScrollbar=function(r,l){return this.each(function(){function i(e,t){var n=e+t,r=P-k;C=0>n?0:n>r?r:n;var l=o(C*(x-P)/(P-k));L.scrollTop(l)}function a(e,t){var n=e+t,r=S-E;X=0>n?0:n>r?r:n;var l=o(X*(D-S)/(S-E));L.scrollLeft(l)}function s(e){return y.minScrollbarLength&&(e=Math.max(e,y.minScrollbarLength)),y.maxScrollbarLength&&(e=Math.min(e,y.maxScrollbarLength)),e}function c(){var e={width:S,display:M?"inherit":"none"};e.left=W?L.scrollLeft()+S-D:L.scrollLeft(),B?e.bottom=q-L.scrollTop():e.top=R+L.scrollTop(),K.css(e);var o={top:L.scrollTop(),height:P,display:Y?"inherit":"none"};z?o.right=W?D-L.scrollLeft()-Q-U.outerWidth():Q-L.scrollLeft():o.left=W?L.scrollLeft()+2*S-D-F-U.outerWidth():F+L.scrollLeft(),H.css(o),O.css({left:X,width:E-A}),U.css({top:C,height:k-G})}function d(){K.hide(),H.hide(),S=y.includePadding?L.innerWidth():L.width(),P=y.includePadding?L.innerHeight():L.height(),D=L.prop("scrollWidth"),x=L.prop("scrollHeight"),!y.suppressScrollX&&D>S+y.scrollXMarginOffset?(M=!0,E=s(o(S*S/D)),X=o(L.scrollLeft()*(S-E)/(D-S))):(M=!1,E=0,X=0,L.scrollLeft(0)),!y.suppressScrollY&&x>P+y.scrollYMarginOffset?(Y=!0,k=s(o(P*P/x)),C=o(L.scrollTop()*(P-k)/(x-P))):(Y=!1,k=0,C=0,L.scrollTop(0)),X>=S-E&&(X=S-E),C>=P-k&&(C=P-k),c(),M?L.addClass("ps-active-x"):L.removeClass("ps-active-x"),Y?L.addClass("ps-active-y"):L.removeClass("ps-active-y"),y.suppressScrollX||K.show(),y.suppressScrollY||H.show()}function p(){var o,t;O.bind("mousedown"+j,function(e){t=e.pageX,o=O.position().left,K.addClass("in-scrolling"),e.stopPropagation(),e.preventDefault()}),e(I).bind("mousemove"+j,function(e){K.hasClass("in-scrolling")&&(a(o,e.pageX-t),d(),e.stopPropagation(),e.preventDefault())}),e(I).bind("mouseup"+j,function(){K.hasClass("in-scrolling")&&K.removeClass("in-scrolling")}),o=t=null}function u(){var o,t;U.bind("mousedown"+j,function(e){t=e.pageY,o=U.position().top,H.addClass("in-scrolling"),e.stopPropagation(),e.preventDefault()}),e(I).bind("mousemove"+j,function(e){H.hasClass("in-scrolling")&&(i(o,e.pageY-t),d(),e.stopPropagation(),e.preventDefault())}),e(I).bind("mouseup"+j,function(){H.hasClass("in-scrolling")&&H.removeClass("in-scrolling")}),o=t=null}function f(e,o){var t=L.scrollTop();if(0===e){if(!Y)return!1;if(0===t&&o>0||t>=x-P&&0>o)return!y.wheelPropagation}var n=L.scrollLeft();if(0===o){if(!M)return!1;if(0===n&&0>e||n>=D-S&&e>0)return!y.wheelPropagation}return!0}function v(){function e(e){var o=e.originalEvent.deltaX,t=-1*e.originalEvent.deltaY;return(o===void 0||t===void 0)&&(o=-1*e.originalEvent.wheelDeltaX/6,t=e.originalEvent.wheelDeltaY/6),e.originalEvent.deltaMode&&1===e.originalEvent.deltaMode&&(o*=10,t*=10),o!==o&&t!==t&&(o=0,t=e.originalEvent.wheelDelta),[o,t]}function o(o){var n=e(o),r=n[0],l=n[1];t=!1,y.useBothWheelAxes?Y&&!M?(l?L.scrollTop(L.scrollTop()-l*y.wheelSpeed):L.scrollTop(L.scrollTop()+r*y.wheelSpeed),t=!0):M&&!Y&&(r?L.scrollLeft(L.scrollLeft()+r*y.wheelSpeed):L.scrollLeft(L.scrollLeft()-l*y.wheelSpeed),t=!0):(L.scrollTop(L.scrollTop()-l*y.wheelSpeed),L.scrollLeft(L.scrollLeft()+r*y.wheelSpeed)),d(),t=t||f(r,l),t&&(o.stopPropagation(),o.preventDefault())}var t=!1;window.onwheel!==void 0?L.bind("wheel"+j,o):window.onmousewheel!==void 0&&L.bind("mousewheel"+j,o)}function h(){var o=!1;L.bind("mouseenter"+j,function(){o=!0}),L.bind("mouseleave"+j,function(){o=!1});var t=!1;e(I).bind("keydown"+j,function(n){if((!n.isDefaultPrevented||!n.isDefaultPrevented())&&o){for(var r=document.activeElement?document.activeElement:I.activeElement;r.shadowRoot;)r=r.shadowRoot.activeElement;if(!e(r).is(":input,[contenteditable]")){var l=0,i=0;switch(n.which){case 37:l=-30;break;case 38:i=30;break;case 39:l=30;break;case 40:i=-30;break;case 33:i=90;break;case 32:case 34:i=-90;break;case 35:i=n.ctrlKey?-x:-P;break;case 36:i=n.ctrlKey?L.scrollTop():P;break;default:return}L.scrollTop(L.scrollTop()-i),L.scrollLeft(L.scrollLeft()+l),t=f(l,i),t&&n.preventDefault()}}})}function b(){function e(e){e.stopPropagation()}U.bind("click"+j,e),H.bind("click"+j,function(e){var t=o(k/2),n=e.pageY-H.offset().top-t,r=P-k,l=n/r;0>l?l=0:l>1&&(l=1),L.scrollTop((x-P)*l)}),O.bind("click"+j,e),K.bind("click"+j,function(e){var t=o(E/2),n=e.pageX-K.offset().left-t,r=S-E,l=n/r;0>l?l=0:l>1&&(l=1),L.scrollLeft((D-S)*l)})}function g(o,t){function n(e,o){L.scrollTop(L.scrollTop()-o),L.scrollLeft(L.scrollLeft()-e),d()}function r(){b=!0}function l(){b=!1}function i(e){return e.originalEvent.targetTouches?e.originalEvent.targetTouches[0]:e.originalEvent}function a(e){var o=e.originalEvent;return o.targetTouches&&1===o.targetTouches.length?!0:o.pointerType&&"mouse"!==o.pointerType?!0:!1}function s(e){if(a(e)){g=!0;var o=i(e);u.pageX=o.pageX,u.pageY=o.pageY,f=(new Date).getTime(),null!==h&&clearInterval(h),e.stopPropagation(),e.preventDefault()}}function c(e){if(!b&&g&&a(e)){var o=i(e),t={pageX:o.pageX,pageY:o.pageY},r=t.pageX-u.pageX,l=t.pageY-u.pageY;n(r,l),u=t;var s=(new Date).getTime(),c=s-f;c>0&&(v.x=r/c,v.y=l/c,f=s),e.stopPropagation(),e.preventDefault()}}function p(){!b&&g&&(g=!1,clearInterval(h),h=setInterval(function(){return.01>Math.abs(v.x)&&.01>Math.abs(v.y)?(clearInterval(h),void 0):(n(30*v.x,30*v.y),v.x*=.8,v.y*=.8,void 0)},10))}var u={},f=0,v={},h=null,b=!1,g=!1;o&&(e(window).bind("touchstart"+j,r),e(window).bind("touchend"+j,l),L.bind("touchstart"+j,s),L.bind("touchmove"+j,c),L.bind("touchend"+j,p)),t&&(window.PointerEvent?(e(window).bind("pointerdown"+j,r),e(window).bind("pointerup"+j,l),L.bind("pointerdown"+j,s),L.bind("pointermove"+j,c),L.bind("pointerup"+j,p)):window.MSPointerEvent&&(e(window).bind("MSPointerDown"+j,r),e(window).bind("MSPointerUp"+j,l),L.bind("MSPointerDown"+j,s),L.bind("MSPointerMove"+j,c),L.bind("MSPointerUp"+j,p)))}function w(){L.bind("scroll"+j,function(){d()})}function m(){L.unbind(j),e(window).unbind(j),e(I).unbind(j),L.data("perfect-scrollbar",null),L.data("perfect-scrollbar-update",null),L.data("perfect-scrollbar-destroy",null),O.remove(),U.remove(),K.remove(),H.remove(),K=H=O=U=M=Y=S=P=D=x=E=X=q=B=R=k=C=Q=z=F=W=j=null}function T(){d(),w(),p(),u(),b(),v(),(J||N)&&g(J,N),y.useKeyboard&&h(),L.data("perfect-scrollbar",L),L.data("perfect-scrollbar-update",d),L.data("perfect-scrollbar-destroy",m)}var y=e.extend(!0,{},t),L=e(this);if("object"==typeof r?e.extend(!0,y,r):l=r,"update"===l)return L.data("perfect-scrollbar-update")&&L.data("perfect-scrollbar-update")(),L;if("destroy"===l)return L.data("perfect-scrollbar-destroy")&&L.data("perfect-scrollbar-destroy")(),L;if(L.data("perfect-scrollbar"))return L.data("perfect-scrollbar");L.addClass("ps-container");var S,P,D,x,M,E,X,Y,k,C,W="rtl"===L.css("direction"),j=n(),I=this.ownerDocument||document,K=e("<div class='ps-scrollbar-x-rail'>").appendTo(L),O=e("<div class='ps-scrollbar-x'>").appendTo(K),q=o(K.css("bottom")),B=q===q,R=B?null:o(K.css("top")),A=o(K.css("borderLeftWidth"))+o(K.css("borderRightWidth")),H=e("<div class='ps-scrollbar-y-rail'>").appendTo(L),U=e("<div class='ps-scrollbar-y'>").appendTo(H),Q=o(H.css("right")),z=Q===Q,F=z?null:o(H.css("left")),G=o(H.css("borderTopWidth"))+o(H.css("borderBottomWidth")),J="ontouchstart"in window||window.DocumentTouch&&document instanceof window.DocumentTouch,N=null!==window.navigator.msMaxTouchPoints;return T(),L})}});


/* Tooltipster v3.2.6 */;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,debug:true,delay:200,minWidth:0,maxWidth:null,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){if(t.options.content!==null){t._content_set(t.options.content)}else{var r=t.$el.attr("title");if(typeof r==="undefined")r=null;t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.removeAttr("title").addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.minWidth?"min-width:"+Math.round(r.options.minWidth)+"px;":"",c=r.options.maxWidth?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition()}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0])t.$elProxy.remove();t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html();t.$el.removeClass("tooltipstered").attr("title",r).removeData(t.namespace).removeData("tooltipster-ns").off("."+t.namespace)}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e,t){if(typeof t=="undefined")return this.options[e];else{this.options[e]=t;return this}},status:function(){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1],t[2])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple,f=t[0]&&typeof t[0].debug!=="undefined",l=f&&t[0].debug||!f&&i.debug;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else if(a){n=true}else if(l){console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.')}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);

/* Select2 */
(function(e,t){"use strict";function o(e,t){var n=0,r=t.length,i;if(typeof e==="undefined"){return-1}if(e.constructor===String){for(;n<r;n=n+1)if(e.localeCompare(t[n])===0)return n}else{for(;n<r;n=n+1){i=t[n];if(i.constructor===String){if(i.localeCompare(e)===0)return n}else{if(i===e)return n}}}return-1}function u(e,n){if(e===n)return true;if(e===t||n===t)return false;if(e===null||n===null)return false;if(e.constructor===String)return e.localeCompare(n)===0;if(n.constructor===String)return n.localeCompare(e)===0;return false}function a(t,n){var r,i,s;if(t===null||t.length<1)return[];r=t.split(n);for(i=0,s=r.length;i<s;i=i+1)r[i]=e.trim(r[i]);return r}function f(e){return e.outerWidth()-e.width()}function l(e){e.bind("keydown",function(){e.data("keyup-change-value",e.val())});e.bind("keyup",function(){if(e.val()!==e.data("keyup-change-value")){e.trigger("keyup-change")}})}function c(n){n.bind("mousemove",function(n){var r=e(document).data("select2-lastpos");if(r===t||r.x!==n.pageX||r.y!==n.pageY){e(n.target).trigger("mousemove-filtered",n)}})}function h(e,t){var n;return function(){window.clearTimeout(n);n=window.setTimeout(t,e)}}function p(e,t){var n=h(e,function(e){t.trigger("scroll-debounced",e)});t.bind("scroll",function(e){if(o(e.target,t.get())>=0)n(e)})}function d(e){e.preventDefault();e.stopPropagation()}function v(t){var n,r;n=e("<div></div>").css({position:"absolute",left:"-1000px",top:"-1000px",display:"none",fontSize:t.css("fontSize"),fontFamily:t.css("fontFamily"),fontStyle:t.css("fontStyle"),fontWeight:t.css("fontWeight"),letterSpacing:t.css("letterSpacing"),textTransform:t.css("textTransform"),whiteSpace:"nowrap"});n.text(t.val());e("body").append(n);r=n.width();n.remove();return r}function m(t){var n,r=0,i=null,s=t.quietMillis||100;return function(o){window.clearTimeout(n);n=window.setTimeout(function(){r+=1;var n=r,s=t.data,u=t.transport||e.ajax;s=s.call(this,o.term,o.page,o.context);if(null!==i){i.abort()}i=u.call(null,{url:t.url,dataType:t.dataType,data:s,success:function(e){if(n<r){return}var i=t.results(e,o.page);o.callback(i)}})},s)}}function g(t){var n=t,r=function(e){return""+e.text};if(!e.isArray(n)){r=n.text;if(!e.isFunction(r))r=function(e){return e[n.text]};n=n.results}return function(t){var i=t.term,s={};if(i===""){t.callback({results:n});return}s.results=e(n).filter(function(){return t.matcher(i,r(this))}).get();t.callback(s)}}function y(n){if(e.isFunction(n)){return n}return function(r){var i=r.term,s={results:[]};e(n).each(function(){var e=this.text!==t,n=e?this.text:this;if(i===""||r.matcher(i,n)){s.results.push(e?this:{id:this,text:this})}});r.callback(s)}}function b(t,n){var r=function(){};r.prototype=new t;r.prototype.constructor=r;r.prototype.parent=t.prototype;r.prototype=e.extend(r.prototype,n);return r}if(window.Select2!==t){return}var n,r,i,s;n={TAB:9,ENTER:13,ESC:27,SPACE:32,LEFT:37,UP:38,RIGHT:39,DOWN:40,SHIFT:16,CTRL:17,ALT:18,PAGE_UP:33,PAGE_DOWN:34,HOME:36,END:35,BACKSPACE:8,DELETE:46,isArrow:function(e){e=e.which?e.which:e;switch(e){case n.LEFT:case n.RIGHT:case n.UP:case n.DOWN:return true}return false},isControl:function(e){e=e.which?e.which:e;switch(e){case n.SHIFT:case n.CTRL:case n.ALT:return true}return false},isFunctionKey:function(e){e=e.which?e.which:e;return e>=112&&e<=123}};e(document).delegate("*","mousemove",function(t){e(document).data("select2-lastpos",{x:t.pageX,y:t.pageY})});e(document).ready(function(){e(document).delegate("*","mousedown focusin touchend",function(t){var n=e(t.target).closest("div.select2-container").get(0);e(document).find("div.select2-container-active").each(function(){if(this!==n)e(this).data("select2").blur()})})});r=b(Object,{bind:function(e){var t=this;return function(){e.apply(t,arguments)}},init:function(n){var r,i,s=".select2-results";this.opts=n=this.prepareOpts(n);this.id=n.id;if(n.element.data("select2")!==t&&n.element.data("select2")!==null){this.destroy()}this.enabled=true;this.container=this.createContainer();if(n.element.attr("class")!==t){this.container.addClass(n.element.attr("class"))}this.opts.element.data("select2",this).hide().after(this.container);this.container.data("select2",this);this.dropdown=this.container.find(".select2-drop");this.results=r=this.container.find(s);this.search=i=this.container.find("input[type=text]");this.resultsPage=0;this.context=null;this.initContainer();c(this.results);this.container.delegate(s,"mousemove-filtered",this.bind(this.highlightUnderEvent));p(80,this.results);this.container.delegate(s,"scroll-debounced",this.bind(this.loadMoreIfNeeded));if(e.fn.mousewheel){r.mousewheel(function(e,t,n,i){var s=r.scrollTop(),o;if(i>0&&s-i<=0){r.scrollTop(0);d(e)}else if(i<0&&r.get(0).scrollHeight-r.scrollTop()+i<=r.height()){r.scrollTop(r.get(0).scrollHeight-r.height());d(e)}})}l(i);i.bind("keyup-change",this.bind(this.updateResults));i.bind("focus",function(){i.addClass("select2-focused")});i.bind("blur",function(){i.removeClass("select2-focused")});this.container.delegate(s,"click",this.bind(function(t){if(e(t.target).closest(".select2-result:not(.select2-disabled)").length>0){this.highlightUnderEvent(t);this.selectHighlighted(t)}else{d(t);this.focusSearch()}}));if(e.isFunction(this.opts.initSelection)){this.initSelection();this.monitorSource()}if(n.element.is(":disabled"))this.disable()},destroy:function(){var e=this.opts.element.data("select2");if(e!==t){e.container.remove();e.opts.element.removeData("select2").unbind(".select2").show()}},prepareOpts:function(n){var r,i,s;r=n.element;if(r.get(0).tagName.toLowerCase()==="select"){this.select=i=n.element}if(i){e.each(["id","multiple","ajax","query","createSearchChoice","initSelection","data","tags"],function(){if(this in n){throw new Error("Option '"+this+"' is not allowed for Select2 when attached to a <select> element.")}})}n=e.extend({},{formatResult:function(e){return e.text},formatSelection:function(e){return e.text},formatNoMatches:function(){return"No matches found"},formatInputTooShort:function(e,t){return"Please enter "+(t-e.length)+" more characters"},minimumResultsForSearch:0,minimumInputLength:0,id:function(e){return e.id},matcher:function(e,t){return t.toUpperCase().indexOf(e.toUpperCase())>=0}},n);if(typeof n.id!=="function"){s=n.id;n.id=function(e){return e[s]}}if(i){n.query=this.bind(function(n){var i={results:[],more:false},s=n.term,o=this.getPlaceholder();r.find("option").each(function(r){var u=e(this),a=u.text();if(r===0&&o!==t&&a==="")return true;if(n.matcher(s,a)){i.results.push({id:u.attr("value"),text:a})}});n.callback(i)});n.id=function(e){return e.id}}else{if(!("query"in n)){if("ajax"in n){n.query=m(n.ajax)}else if("data"in n){n.query=g(n.data)}else if("tags"in n){n.query=y(n.tags);n.createSearchChoice=function(e){return{id:e,text:e}};n.initSelection=function(t){var n=[];e(a(t.val(),",")).each(function(){n.push({id:this,text:this})});return n}}}}if(typeof n.query!=="function"){throw"query function not defined for Select2 "+n.element.attr("id")}return n},monitorSource:function(){this.opts.element.bind("change.select2",this.bind(function(e){if(this.opts.element.data("select2-change-triggered")!==true){this.initSelection()}}))},triggerChange:function(){this.opts.element.data("select2-change-triggered",true);this.opts.element.trigger("change");this.opts.element.data("select2-change-triggered",false)},enable:function(){if(this.enabled)return;this.enabled=true;this.container.removeClass("select2-container-disabled")},disable:function(){if(!this.enabled)return;this.close();this.enabled=false;this.container.addClass("select2-container-disabled")},opened:function(){return this.container.hasClass("select2-dropdown-open")},open:function(){if(this.opened())return;this.container.addClass("select2-dropdown-open").addClass("select2-container-active");this.updateResults(true);this.dropdown.show();this.ensureHighlightVisible();this.focusSearch()},close:function(){if(!this.opened())return;this.dropdown.hide();this.container.removeClass("select2-dropdown-open");this.results.empty();this.clearSearch()},clearSearch:function(){},ensureHighlightVisible:function(){var t=this.results,n,r,i,s,o,u,a;n=t.children(".select2-result");r=this.highlight();if(r<0)return;i=e(n[r]);s=i.offset().top+i.outerHeight();if(r===n.length-1){a=t.find("li.select2-more-results");if(a.length>0){s=a.offset().top+a.outerHeight()}}o=t.offset().top+t.outerHeight();if(s>o){t.scrollTop(t.scrollTop()+(s-o))}u=i.offset().top-t.offset().top;if(u<0){t.scrollTop(t.scrollTop()+u)}},moveHighlight:function(t){var n=this.results.children(".select2-result"),r=this.highlight();while(r>-1&&r<n.length){r+=t;if(!e(n[r]).hasClass("select2-disabled")){this.highlight(r);break}}},highlight:function(t){var n=this.results.children(".select2-result");if(arguments.length===0){return o(n.filter(".select2-highlighted")[0],n.get())}n.removeClass("select2-highlighted");if(t>=n.length)t=n.length-1;if(t<0)t=0;e(n[t]).addClass("select2-highlighted");this.ensureHighlightVisible();if(this.opened())this.focusSearch()},highlightUnderEvent:function(t){var n=e(t.target).closest(".select2-result");if(n.length>0){this.highlight(n.index())}},loadMoreIfNeeded:function(){var n=this.results,r=n.find("li.select2-more-results"),i,s=-1,o=this.resultsPage+1;if(r.length===0)return;i=r.offset().top-n.offset().top-n.height();if(i<=0){r.addClass("select2-active");this.opts.query({term:this.search.val(),page:o,context:this.context,matcher:this.opts.matcher,callback:this.bind(function(i){var u=[],a=this;e(i.results).each(function(){u.push("<li class='select2-result'>");u.push(a.opts.formatResult(this));u.push("</li>")});r.before(u.join(""));n.find(".select2-result").each(function(n){var r=e(this);if(r.data("select2-data")!==t){s=n}else{r.data("select2-data",i.results[n-s-1])}});if(i.more){r.removeClass("select2-active")}else{r.remove()}this.resultsPage=o})})}},updateResults:function(n){function a(e){i.html(e);i.scrollTop(0);r.removeClass("select2-active")}var r=this.search,i=this.results,s=this.opts,o=this;if(n!==true&&this.showSearchInput===false){return}r.addClass("select2-active");if(r.val().length<s.minimumInputLength){a("<li class='select2-no-results'>"+s.formatInputTooShort(r.val(),s.minimumInputLength)+"</li>");return}this.resultsPage=1;s.query({term:r.val(),page:this.resultsPage,context:null,matcher:s.matcher,callback:this.bind(function(f){var l=[],c;this.context=f.context===t?null:f.context;if(this.opts.createSearchChoice&&r.val()!==""){c=this.opts.createSearchChoice.call(null,r.val(),f.results);if(c!==t&&c!==null&&o.id(c)!==t&&o.id(c)!==null){if(e(f.results).filter(function(){return u(o.id(this),o.id(c))}).length===0){f.results.unshift(c)}}}if(f.results.length===0){a("<li class='select2-no-results'>"+s.formatNoMatches(r.val())+"</li>");return}e(f.results).each(function(){l.push("<li class='select2-result'>");l.push(s.formatResult(this));l.push("</li>")});if(f.more===true){l.push("<li class='select2-more-results'>Loading more results...</li>")}a(l.join(""));i.children(".select2-result").each(function(t){var n=f.results[t];e(this).data("select2-data",n)});this.postprocessResults(f,n)})})},cancel:function(){this.close()},blur:function(){window.setTimeout(this.bind(function(){this.close();this.container.removeClass("select2-container-active");this.clearSearch();this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");this.search.blur()}),10)},focusSearch:function(){window.setTimeout(this.bind(function(){this.search.focus()}),10)},selectHighlighted:function(){var e=this.results.find(".select2-highlighted:not(.select2-disabled)").data("select2-data");if(e){this.onSelect(e)}},getPlaceholder:function(){return this.opts.element.attr("placeholder")||this.opts.element.data("placeholder")||this.opts.placeholder},getContainerWidth:function(){var e,n,r,i,s;if(this.opts.width!==t)return this.opts.width;e=this.opts.element.attr("style");if(e!==t){n=e.split(";");for(i=0,s=n.length;i<s;i=i+1){r=n[i].replace(/\s/g,"").match(/width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/);if(r!==null&&r.length>=1)return r[1]}}return this.opts.element.width()+"px"}});i=b(r,{createContainer:function(){return e("<div></div>",{"class":"select2-container",style:"width: "+this.getContainerWidth()}).html(["    <a href='javascript:void(0)' class='select2-choice'>","   <span></span><abbr class='select2-search-choice-close' style='display:none;'></abbr>","   <div><b></b></div>","</a>","    <div class='select2-drop' style='display:none;'>","   <div class='select2-search'>","       <input type='text' autocomplete='off'/>","   </div>","   <ul class='select2-results'>","   </ul>","</div>"].join(""))},open:function(){if(this.opened())return;this.parent.open.apply(this,arguments)},close:function(){if(!this.opened())return;this.parent.close.apply(this,arguments)},focus:function(){this.close();this.selection.focus()},isFocused:function(){return this.selection.is(":focus")},cancel:function(){this.parent.cancel.apply(this,arguments);this.selection.focus()},initContainer:function(){var e,t=this.container,r=false,i=".select2-choice";this.selection=e=t.find(i);this.search.bind("keydown",this.bind(function(e){switch(e.which){case n.UP:case n.DOWN:this.moveHighlight(e.which===n.UP?-1:1);d(e);return;case n.TAB:case n.ENTER:this.selectHighlighted();d(e);return;case n.ESC:this.cancel(e);e.preventDefault();return}}));t.delegate(i,"click",this.bind(function(t){r=true;if(this.opened()){this.close();e.focus()}else if(this.enabled){this.open()}t.preventDefault();r=false}));t.delegate(i,"keydown",this.bind(function(e){if(!this.enabled||e.which===n.TAB||n.isControl(e)||n.isFunctionKey(e)||e.which===n.ESC){return}this.open();if(e.which===n.PAGE_UP||e.which===n.PAGE_DOWN||e.which===n.SPACE){d(e)}if(e.which===n.ENTER){d(e)}}));t.delegate(i,"focus",function(){if(this.enabled)t.addClass("select2-container-active")});t.delegate(i,"blur",this.bind(function(){if(r)return;if(!this.opened())this.blur()}));e.delegate("abbr","click",this.bind(function(e){if(!this.enabled)return;this.val("");d(e);this.close();this.triggerChange()}));this.setPlaceholder()},initSelection:function(){var e;if(this.opts.element.val()===""){this.updateSelection({id:"",text:""})}else{e=this.opts.initSelection.call(null,this.opts.element);if(e!==t&&e!==null){this.updateSelection(e)}}this.close();this.setPlaceholder()},prepareOpts:function(){var e=this.parent.prepareOpts.apply(this,arguments);if(e.element.get(0).tagName.toLowerCase()==="select"){e.initSelection=function(e){var t=e.find(":selected");return{id:t.attr("value"),text:t.text()}}}return e},setPlaceholder:function(){var e=this.getPlaceholder();if(this.opts.element.val()===""&&e!==t){if(this.select&&this.select.find("option:first").text()!=="")return;if(typeof e==="object"){this.updateSelection(e)}else{this.selection.find("span").html(e)}this.selection.addClass("select2-default");this.selection.find("abbr").hide()}},postprocessResults:function(t,n){var r=0,i=this,s=true;this.results.find(".select2-result").each(function(t){if(u(i.id(e(this).data("select2-data")),i.opts.element.val())){r=t;return false}});this.highlight(r);if(n===true){s=this.showSearchInput=t.results.length>=this.opts.minimumResultsForSearch;this.container.find(".select2-search")[s?"removeClass":"addClass"]("select2-search-hidden");this.container[s?"addClass":"removeClass"]("select2-with-searchbox")}},onSelect:function(e){var t=this.opts.element.val();this.opts.element.val(this.id(e));this.updateSelection(e);this.close();this.selection.focus();if(!u(t,this.id(e))){this.triggerChange()}},updateSelection:function(e){this.selection.find("span").html(this.opts.formatSelection(e));this.selection.removeClass("select2-default");if(this.opts.allowClear&&this.getPlaceholder()!==t){this.selection.find("abbr").show()}},val:function(){var t,n=null;if(arguments.length===0){return this.opts.element.val()}t=arguments[0];if(this.select){this.select.val(t).find(":selected").each(function(){n={id:e(this).attr("value"),text:e(this).text()};return false});this.updateSelection(n)}else{this.opts.element.val(!t?"":this.id(t));this.updateSelection(t)}this.setPlaceholder()},clearSearch:function(){this.search.val("")}});s=b(r,{createContainer:function(){return e("<div></div>",{"class":"select2-container select2-container-multi",style:"width: "+this.getContainerWidth()}).html(["    <ul class='select2-choices'>","  <li class='select2-search-field'>","    <input type='text' autocomplete='off' style='width: 25px;'>","  </li>","</ul>","<div class='select2-drop' style='display:none;'>","   <ul class='select2-results'>","   </ul>","</div>"].join(""))},prepareOpts:function(){var t=this.parent.prepareOpts.apply(this,arguments);t=e.extend({},{closeOnSelect:true},t);if(t.element.get(0).tagName.toLowerCase()==="select"){t.initSelection=function(t){var n=[];t.find(":selected").each(function(){n.push({id:e(this).attr("value"),text:e(this).text()})});return n}}return t},initContainer:function(){var e=".select2-choices",t;this.searchContainer=this.container.find(".select2-search-field");this.selection=t=this.container.find(e);this.search.bind("keydown",this.bind(function(e){if(!this.enabled)return;if(e.which===n.BACKSPACE&&this.search.val()===""){this.close();var r,i=t.find(".select2-search-choice-focus");if(i.length>0){this.unselect(i.first());this.search.width(10);d(e);return}r=t.find(".select2-search-choice");if(r.length>0){r.last().addClass("select2-search-choice-focus")}}else{t.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus")}if(this.opened()){switch(e.which){case n.UP:case n.DOWN:this.moveHighlight(e.which===n.UP?-1:1);d(e);return;case n.ENTER:case n.TAB:this.selectHighlighted();d(e);return;case n.ESC:this.cancel(e);e.preventDefault();return}}if(e.which===n.TAB||n.isControl(e)||n.isFunctionKey(e)||e.which===n.BACKSPACE||e.which===n.ESC){return}this.open();if(e.which===n.PAGE_UP||e.which===n.PAGE_DOWN){d(e)}}));this.search.bind("keyup",this.bind(this.resizeSearch));this.container.delegate(e,"click",this.bind(function(e){if(!this.enabled)return;this.open();this.focusSearch();e.preventDefault()}));this.container.delegate(e,"focus",this.bind(function(){if(!this.enabled)return;this.container.addClass("select2-container-active");this.clearPlaceholder()}));this.clearSearch()},enable:function(){if(this.enabled)return;this.parent.enable.apply(this,arguments);this.search.show()},disable:function(){if(!this.enabled)return;this.parent.disable.apply(this,arguments);this.search.hide()},initSelection:function(){var e;if(this.opts.element.val()===""){this.updateSelection([])}if(this.select||this.opts.element.val()!==""){e=this.opts.initSelection.call(null,this.opts.element);if(e!==t&&e!==null){this.updateSelection(e)}}this.close();this.clearSearch()},clearSearch:function(){var e=this.getPlaceholder();if(e!==t&&this.getVal().length===0&&this.search.hasClass("select2-focused")===false){this.search.val(e).addClass("select2-default");this.search.width(this.getContainerWidth())}else{this.search.val("").width(10)}},clearPlaceholder:function(){if(this.search.hasClass("select2-default")){this.search.val("").removeClass("select2-default")}},open:function(){if(this.opened())return;this.parent.open.apply(this,arguments);this.resizeSearch();this.focusSearch()},close:function(){if(!this.opened())return;this.parent.close.apply(this,arguments)},focus:function(){this.close();this.search.focus()},isFocused:function(){return this.search.hasClass("select2-focused")},updateSelection:function(t){var n=[],r=[],i=this;e(t).each(function(){if(o(i.id(this),n)<0){n.push(i.id(this));r.push(this)}});t=r;this.selection.find(".select2-search-choice").remove();e(t).each(function(){i.addSelectedChoice(this)});i.postprocessResults()},onSelect:function(e){this.addSelectedChoice(e);if(this.select){this.postprocessResults()}if(this.opts.closeOnSelect){this.close();this.search.width(10)}else{this.search.width(10);this.resizeSearch()}this.triggerChange();this.focusSearch()},cancel:function(){this.close();this.focusSearch()},addSelectedChoice:function(t){var n,r=this.id(t),i,s=this.getVal();i=["<li class='select2-search-choice'>",this.opts.formatSelection(t),"<a href='javascript:void(0)' class='select2-search-choice-close' tabindex='-1'></a>","</li>"];n=e(i.join(""));n.find("a").bind("click dblclick",this.bind(function(t){if(!this.enabled)return;this.unselect(e(t.target));this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");d(t);this.close();this.focusSearch()})).bind("focus",this.bind(function(){if(!this.enabled)return;this.container.addClass("select2-container-active")}));n.data("select2-data",t);n.insertBefore(this.searchContainer);s.push(r);this.setVal(s)},unselect:function(e){var t=this.getVal(),n;e=e.closest(".select2-search-choice");if(e.length===0){throw"Invalid argument: "+e+". Must be .select2-search-choice"}n=o(this.id(e.data("select2-data")),t);if(n>=0){t.splice(n,1);this.setVal(t);if(this.select)this.postprocessResults()}e.remove();this.triggerChange()},postprocessResults:function(){var t=this.getVal(),n=this.results.find(".select2-result"),r=this;n.each(function(){var n=e(this),i=r.id(n.data("select2-data"));if(o(i,t)>=0){n.addClass("select2-disabled")}else{n.removeClass("select2-disabled")}});n.each(function(t){if(!e(this).hasClass("select2-disabled")){r.highlight(t);return false}})},resizeSearch:function(){var e,t,n,r,i;e=v(this.search)+10;t=this.search.offset().left;n=this.selection.width();r=this.selection.offset().left;i=n-(t-r)-f(this.search);if(i<e){i=n-f(this.search)}if(i<40){i=n-f(this.search)}this.search.width(i)},getVal:function(){var e;if(this.select){e=this.select.val();return e===null?[]:e}else{e=this.opts.element.val();return a(e,",")}},setVal:function(t){var n;if(this.select){this.select.val(t)}else{n=[];e(t).each(function(){if(o(this,n)<0)n.push(this)});this.opts.element.val(n.length===0?"":n.join(","))}},val:function(){var t,n=[],r=this;if(arguments.length===0){return this.getVal()}t=arguments[0];if(this.select){this.setVal(t);this.select.find(":selected").each(function(){n.push({id:e(this).attr("value"),text:e(this).text()})});this.updateSelection(n)}else{t=t===null?[]:t;this.setVal(t);e(t).each(function(){n.push(r.id(this))});this.setVal(n);this.updateSelection(t)}this.clearSearch()},onSortStart:function(){if(this.select){throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.")}this.search.width(0);this.searchContainer.hide()},onSortEnd:function(){var t=[],n=this;this.searchContainer.show();this.searchContainer.appendTo(this.searchContainer.parent());this.resizeSearch();this.selection.find(".select2-search-choice").each(function(){t.push(n.opts.id(e(this).data("select2-data")))});this.setVal(t);this.triggerChange()}});e.fn.select2=function(){var n=Array.prototype.slice.call(arguments,0),r,u,a,f,l=["val","destroy","open","close","focus","isFocused","container","onSortStart","onSortEnd","enable","disable"];this.each(function(){if(n.length===0||typeof n[0]==="object"){r=n.length===0?{}:e.extend({},n[0]);r.element=e(this);if(r.element.get(0).tagName.toLowerCase()==="select"){f=r.element.attr("multiple")}else{f=r.multiple||false;if("tags"in r){r.multiple=f=true}}u=f?new s:new i;u.init(r)}else if(typeof n[0]==="string"){if(o(n[0],l)<0){throw"Unknown method: "+n[0]}a=t;u=e(this).data("select2");if(u===t)return;if(n[0]==="container"){a=u.container}else{a=u[n[0]].apply(u,n.slice(1))}if(a!==t){return false}}else{throw"Invalid arguments to select2 plugin: "+n}});return a===t?this:a};window.Select2={query:{ajax:m,local:g,tags:y},util:{debounce:h},"class":{"abstract":r,single:i,multi:s}}})(jQuery)