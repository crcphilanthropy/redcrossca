
var redcrossDonate = {},
    timeOut = null,
    learnMoreWidth = $(".learn-more-container").width();

redcrossDonate.setEqualHeight = function () {
    var donateRows = $('.ctas .panel');
    if ($('.ctas .panel').length) {
        equalheight(donateRows);
    }
}
redcrossDonate.setEqualHeight();

// Resize handler
window.onresize = function () {
    if (timeOut !== null)
        clearTimeout(timeOut);

    timeOut = setTimeout(function () {

        if ($('.ctas .panel').length) {
            redcrossDonate.setEqualHeight(); // set height of tablet columns
        }

        learnMoreWidth = $(".learn-more-container").width();
        if (!$('.learn-more-container').hasClass('open')) {
            $('.learn-more-container').css('left', '-' + learnMoreWidth + 'px');
        }

        // Set height of .form-contents on Alternate template
        if ($('.site-container').hasClass('alternate')) {
            $('.donate-now .form-contents').css({ 'height': $('.donate-form .fieldset-fix').height() + 'px' });
        }
    },
    200);
};
$(window).trigger('resize');



// Perfect scrollbar
// - Create custom scroller on learn more panels
if ($('.learn-more-panel').length) {
    $('.learn-more-panel').perfectScrollbar({
        suppressScrollX: true
    });
}

// Tooltips
if ($('.tooltip').length) {
    $('.tooltip').tooltipster();
}


var donateForm = {
    appeals: null,
    appealBaseUrl: null,
    selectedAppeal: null,
    hashPreText: 'selected-appeal-',
    changeAppeal: function () {
        var appealGuid = $('.ddl-appeals').val();
        $('.show-hide-appeal').hide();
        $('.show-hide-appeal.' + appealGuid).fadeIn('slow');
        var currentListItem = $(".learn-more-panel ul li." + appealGuid);
        $(currentListItem).parent().prepend(currentListItem);
        $(".learn-more-panel").scrollTop(0);
        if (donateForm.appeals != undefined) {
            var selectedAppeal = null;
            for (var i = 0; i < donateForm.appeals.length; i++) {
                if (donateForm.appeals[i].DocumentGuid == appealGuid) {
                    selectedAppeal = donateForm.appeals[i];
                }
            }

            $('.price-points input[type="radio"]').prop('checked', false);
            $('.checkbox input').prop('checked', false);
            $('.checkbox input').prop('disabled', false);
            $('.txt-other').val('$');

            if (selectedAppeal == null) return;
            window.location.hash = donateForm.hashPreText + appealGuid;

            $('input.price-point-1').val(selectedAppeal.PricePoint1);
            $('input.price-point-2').val(selectedAppeal.PricePoint2);
            $('input.price-point-3').val(selectedAppeal.PricePoint3);
            $('label.price-point-1 span.value').text(selectedAppeal.PricePoint1Label);
            $('label.price-point-2 span.value').text(selectedAppeal.PricePoint2Label);
            $('label.price-point-3 span.value').text(selectedAppeal.PricePoint3Label);
            $(".chk-monthly input").prop("disabled", selectedAppeal.DisableMonthlyOption);
            $(".chk-monthly input").prop("checked", selectedAppeal.IsMonthlyDonationOnly);
            $('li.price-point-1 a span').text(selectedAppeal.PricePoint1Label);
            $('li.price-point-2 a span').text(selectedAppeal.PricePoint2Label);
            $('li.price-point-3 a span').text(selectedAppeal.PricePoint3Label);
            $('li.price-point-1').attr("data-value", selectedAppeal.PricePoint1Label);
            $('li.price-point-2').attr("data-value", selectedAppeal.PricePoint2Label);
            $('li.price-point-3').attr("data-value", selectedAppeal.PricePoint3Label);

            if (selectedAppeal.CodeName != undefined && selectedAppeal.CodeName != null && selectedAppeal.CodeName != '')
                try {
                    history.pushState(selectedAppeal, selectedAppeal.CodeName, donateForm.appealBaseUrl + selectedAppeal.CodeName + "#" + appealGuid);
                    document.title = selectedAppeal.PageTitle;
                } catch (ex) {

                }
            
            donateForm.selectedAppeal = selectedAppeal;
        }
    },

    pageAppealLoadAt: new Date(),
    initDonateFormTracking: function () {
        //executes with JQuery mousedown on button in form 
        $('.btn-donate').click(function (event) {
            var appealTelNum1 = $('#appealTelNum1').val();
            if ((Math.abs(new Date() - donateForm.pageAppealLoadAt) < 1000)) {
                event.preventDefault();
                return false;
            }

            if (appealTelNum1 != undefined) {
                if (appealTelNum1.length > 0) {
                    event.preventDefault();
                    return false;
                }
            }

            event.preventDefault();
            var donationWidgetValue = "";
            var donationWidgetLabel;
            var donationWidgetCycle;

            //gathers information on the Donation Cycle. 
            if ($('.chk-monthly input').checked) {
                donationWidgetCycle = "Monthly";
            } else {
                donationWidgetCycle = "One Time";
            }

            //gathers information on the label
            var getInputAmountText = $(".txt-other, input.donate-price-point-value").val();
            getInputAmountText = getInputAmountText.replace('$', '');
            if (getInputAmountText.length > 0) {
                // something was entered in OTher text area.  Ensure it's valid
                var getAmount = GetValidatedAmount(getInputAmountText);
                if (getAmount > 0) {
                    donationWidgetLabel = "Entered";
                    donationWidgetValue = Math.round(getAmount);
                    //$(".txt-other").val(getAmount);      // set the textbox value to the "possibly" updated new amount
                }
                else {
                    return false;
                }
            } else {
                donationWidgetLabel = "Selected";

                var selectedValue = $("input[type='radio'][name='pricepoint']:checked");
                if (selectedValue.length > 0) {
                    donationWidgetValue = selectedValue.val();
                }
            }

            //sends event into GA 
            dataLayer.push({ "event": "generic-event", "eventCategory": " Donate Main Form Widget (Drop Down)", "eventAction": donationWidgetCycle, "eventLabel": donationWidgetLabel, "eventValue": donationWidgetValue });
            $('.donate-bar-hash').val(ga.getAll()[0].get('linkerParam'));
            $(".btn-donate").off("click");
            setTimeout(function () { // after 1 second, submit the form
                $(".btn-donate").trigger("click");
            }, 1000);
            return true;
        });
    },



    init: function () {

        donateForm.initDonateFormTracking();
        var selectedAppeal = window.location.hash;

        if (selectedAppeal.length > 1) {
            selectedAppeal = selectedAppeal.substring(1); //remove #
            selectedAppeal = selectedAppeal.replace(donateForm.hashPreText, '');
        }


        if (selectedAppeal == undefined || selectedAppeal == 'null' || selectedAppeal == '') {
            selectedAppeal = $('.ddl-appeals').val();
        } else {
            $('.ddl-appeals').val(selectedAppeal);
        }

        // Add .active class to donate navigation
        $('.donate-select-nav li').each(function () {
            var value = $(this).data('value');
            if (value === selectedAppeal) {
                $(this).addClass('active');
            }
        });



        // Alternate Donate selection nav			  
        $('.donate-price-points-nav li.textinput input').val('$');
        $('.donate-select-nav li a').click(function (e) {
            e.preventDefault();

            var el = $(this).parent(),
               index = el.index(),
               val = el.data('value');

            if (!el.hasClass('active')) {
                // Set value and trigger change event
                $('.ddl-appeals').select2('val', val).trigger('change');
                el.addClass('active').siblings('li').removeClass('active');
            }

            return false;
        });


        // Alternate Donate Price Points Nav
        $('.donate-price-points-nav li a').click(function (e) {
            e.preventDefault();
            var el = $(this).parent(),
               index = el.index(),
               val = el.data('value');
            if (!el.hasClass('active')) {
                // Set value and trigger change event
                $('.price-points li input[type=radio]').eq(index).prop('checked', true);
                el.siblings('li').removeClass('active');
                el.addClass('active');

                if (val === 'Other') {
                    $('.donate-price-points-nav li.textinput input').focus().val('');
                } else {
                    $('.donate-price-points-nav li.textinput input').val('$');
                }
            }
        });


        // Set focus state of input 
        $('.donate-price-points-nav li.textinput input').on('click', function () {
            var val = $(this).val();
            if (!$('.donate-price-points-nav li[data-value="Other"]').hasClass('active')) {
                $('.donate-price-points-nav li[data-value="Other"] a').trigger('click');
            }
            $('.price-points input.txt-other').val(val);
        });

        // Set value of other fom input
        $('.donate-price-points-nav li.textinput input').on('input', function () {
            var val = $(this).val();
            $('.price-points input.txt-other').val(val);
        });


        $('.ddl-appeals').change(function () { donateForm.changeAppeal(); });

        $('.learn-more-tab').click(function (e) {

            $(".learn-more-container").toggleClass("open");
            if ($(".learn-more-container").hasClass("open")) {
                $(".learn-more-container").animate({ "left": 0 }, "slow");
            } else {
                $(".learn-more-container").animate({ "left": -learnMoreWidth }, "slow");
            }
            e.stopPropagation();
        });
        $('.txt-other').val("$");
        $(document).click(function () {
            $(".learn-more-container").removeClass("open");
            $(".learn-more-container").animate({ "left": -learnMoreWidth }, "slow");
        });
        $(".learn-more-container").click(function (e) {
            var target = $(e.target);
            if (!target.is('span.close')) {
                e.stopPropagation();
            }

        });

        $(".donate-form select").css("width", "100%");
        $(".donate-form select").select2();
        $('.price-points input').focus(function () {
            $('.txt-other').val("$");
        });
        $('.txt-other').focus(function () {
            $(this).val('');
            $('input.price-point-4').prop('checked', true);
        });

        var disabledColor = '#ABABAB';
        $('.chk-monthly input').change(function () {
            if (donateForm.selectedAppeal.AllowMonthlyInHonour) return;
            $('.chk-in-honour input').prop('disabled', $(this).is(':checked'));
            $('.chk-in-honour input').prop('checked', false);
            if ($(this).is(':checked')) {
                $('.chk-in-honour label').css('color', disabledColor);
            } else {
                $('.chk-in-honour label').css('color', '');
            }
        });

        $('.chk-in-honour input').change(function () {
            if (donateForm.selectedAppeal.AllowMonthlyInHonour) return;
            $('.chk-monthly input').prop('disabled', $(this).is(':checked'));
            $('.chk-monthly input').prop('checked', false);
            if ($(this).is(':checked')) {
                $('.chk-monthly label').css('color', disabledColor);
            } else {
                $('.chk-monthly label').css('color', '');
            }
        });

        donateForm.changeAppeal();

        if ($('.ros-banner').length) {
            $('footer').css('margin', '0 0 100px');
        }

        // ROS Banner override
        $('.ros-banner .Banner').removeAttr("onclick");

        $('.ros-banner .Banner').removeAttr("onmouseup");


    }
};

// Allow numeric and $ only!	
$("input#p_lt_ctl03_CRC_Widget_DonateForm_txtOther, .donate-price-points-nav li.textinput input, input#p_lt_ctl06_CRC_Widget_DonateForm_txtOther").keydown(function (e) {
    if ($.inArray(e.keyCode, [188, 46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A
       (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: home, end, left, right, down, up
       (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


$('.donate-track-tablet').click(function (event) {
    event.preventDefault();
    var selectedAppeal = $(this).data('link');
    var language = $(this).data('language');
    var gaID = ga.getAll()[0].get('linkerParam');
    var finalUrl = selectedAppeal + "&langpref=" + language + "&" + gaID;
    window.location.href = finalUrl;

});


/*
Alternate Form 
//donateForm.init();
$('.site-container').removeClass('left-nav').addClass('full-width alternate');
$('.columns.donate-now').removeClass('small-9');
$('.form-container').height($('.fieldset-fix').height());
$('.form-contents').height($('.fieldset-fix').height());
*/
