// Utility Belt
// ============
/**
 * a collection of lightweight/generic convenience functions used by multiple components
 * might not be the surefire way to organize this...
 * @module utility
 * @namespace
 * @version 0.1
 */

define([], function () {

  return {

    /**
     * cross browser test for transitionEnd event support
     * @returns {String}    transitionend event for current browser, empty string if not supported
     */
    getTransitionEvent: function () {
      var t, el = document.createElement('fakeelement'),
        transitions = {
          'WebkitTransition' : 'webkitTransitionEnd',
          'MozTransition'    : 'transitionend',
          'OTransition'      : 'oTransitionEnd',
          'msTransition'     : 'msTransitionEnd',
          'transition'       : 'transitionEnd'
        };

      for (t in transitions) {
        if (transitions.hasOwnProperty(t) && el.style[t] !== undefined) {
          return transitions[t];
        }
      }
      return '';
    }
  }
});