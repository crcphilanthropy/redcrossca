//Peralta    
requirejs.config({
	paths: {
		underscore: 'vendor/underscore/underscore',
		templates: 'templates',
		'requirejs-text': 'vendor/requirejs-text/text',
		requirejs: 'vendor/requirejs/require',
		utility: './libs/utils'
	},
	shim: {
		jquery: {
			exports: '$'
		},
		underscore: {
			exports: '_'
		}
	},
	packages: [

	]
});

define([
  'navigation',
  'requirejs-text!/crc/js/navigation/templates/NavigationGroup.html'

], function(Navigation, template) { "use strict";

  var nav,
    options,
    navWrapper = $('.flexpanel .navigation'),
    isIE8 = $('body').hasClass('IE8'),
    lang = $('body').hasClass('FRCA') ? 'fr-CA' : 'en-CA';

    $.ajax({
      //url: "/build/js/navigation/nav.json",
      url: "/api/menu?lang=" + lang,
      method: "GET",
      datatype: "json",
      context: document.body

    }).done(function(res) {

      options = {
        template: template,
        data: res,
        showHomeLink: true,
        homeUrl: res.url,
        homeTitle: res.title
      };
      nav = new Navigation(navWrapper, options, isIE8);

      currentState();

      //if (!isIE8)
        //tac.triggerFlexpanel();

    }).fail(function(res) { // TODO : Works since its not running off of a server 

        var test = JSON.parse(res.responseText);   

        options = {
          template: template,
          data: test,
          showHomeLink: true,
          homeUrl: test.url,
          homeTitle: test.title
        };
        nav = new Navigation(navWrapper, options, isIE8);
    });

});