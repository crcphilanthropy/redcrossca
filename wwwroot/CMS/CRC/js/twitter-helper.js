﻿Date.now = Date.now || function () { return +new Date; };
var days;
var hrs;
var mins;
var hasBeen = '';
var replyText = 'Reply';
var retweetText = 'Retweet';
var favouriteText = 'Favourite';

if (jQuery('body').hasClass('FRCA')) {
    days = 'jours';
    hrs = 'heures';
    mins = 'minutes';
    hasBeen = 'Il y a';
    replyText = 'Répondre';
    retweetText = 'Retweeter';
    favouriteText = 'Favori';
} else {
    days = 'days ago';
    hrs = 'hrs ago';
    mins = 'mins ago';
}



//Twitter Parsers
String.prototype.parseURL = function () {
    return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function (url) {
        return url.link(url);
    });
};
String.prototype.parseUsername = function () {
    return this.replace(/[@]+[A-Za-z0-9-_]+/g, function (u) {
        var username = u.replace("@", "");
        return u.link("http://twitter.com/" + username);
    });
};
String.prototype.parseHashtag = function () {
    return this.replace(/[#]+[A-Za-z0-9-_]+/g, function (t) {
        var tag = t.replace("#", "%23");
        return t.link("http://twitter.com/search?q=" + tag + "&src=hash");
    });
};
function parseTwitterDate(str) {

    var toReturn = new Date(str);
    if (!isNaN(toReturn)) return toReturn;

    var v = str.split(' ');
    toReturn = new Date(Date.parse(v[1] + " " + v[2] + ", " + v[5] + " " + v[3] + " UTC"));
    if (!isNaN(toReturn))
        return toReturn;

    v = str.split('T');
    var v2 = v[1].split('.');
    toReturn = new Date(Date.parse(v[0].replace(/-/g, "/") + " " + v2[0]));
    if (!isNaN(toReturn))
        return toReturn;

    return Date.now;
}

function MillisecondsToDuration(n) {
    var dtm = new Date();
    dtm.setTime(n);
    var d = 0;
    var h = Math.floor(n / 3600000);
    var m = dtm.getMinutes();
    if (h > 24)
        d = parseInt(h / 24);

    if (d > 0)
        return hasBeen + ' ' + d + ' ' + days;

    if (h > 0)
        return hasBeen + ' ' + h + ' ' + hrs;

    return hasBeen + ' ' + m + ' ' + mins;
}

function selectSingleTweet(tweetId) {
    var url = '/api/twitter/GetTweet?id=' + tweetId;
    $.getJSON(url, function (data) {
        if (data == undefined) return;
        
        var tweet = data.text;
        var createdDate = data.time_span;
        tweet = tweet.parseURL().parseUsername().parseHashtag();
        $('.tweet-text').html(tweet);
        $('.tweet-time').html(createdDate);
    });
}

function loadLatestTweet(userName, numTweets) {

    var url = '/api/twitter/get?id=' + userName + '&tweetCount=' + numTweets;
    $.getJSON(url, function (data) {
        if (data == undefined) return;
        for (var i = 0; i < data.length; i++) {
            var tweet = data[i];
            var tweetText = tweet.text.parseURL().parseUsername().parseHashtag();
            //var userInfo = '<span><img src="' + tweet.userPic + '" alt="' + tweet.displayName + '" />' + '<strong>' + tweet.displayName + '</strong>' + '<br />' + '<p>' + '@' + tweet.username + '</p>' + '</span>';
            var userInfo = '';
            var metaInfo = '<ul class="meta">';
            metaInfo += '<li><a target="_blank" href="https://twitter.com/#!/' + userName + '/status/' + tweet.id_str + '" target="_blank">' + tweet.time_span + '</a></li>';
            metaInfo += '<li><a target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=' + tweet.id_str + '" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="reply" data-label="https://twitter.com/intent/tweet?in_reply_to=' + tweet.id_str + '" target="_blank">' + replyText + '</a></li>';
            metaInfo += '<li><a target="_blank" href="https://twitter.com/intent/retweet?tweet_id=' + tweet.id_str + '" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="retweet" data-label="https://twitter.com/intent/retweet?tweet_id=' + tweet.id_str + '" target="_blank">' + retweetText + '</a></li>';
            metaInfo += '<li><a target="_blank" href="https://twitter.com/intent/favorite?tweet_id=' + tweet.id_str + '" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="favorite" data-label="https://twitter.com/intent/favorite?tweet_id=' + tweet.id_str + '" target="_blank">' + favouriteText + '</a></li>';
            metaInfo += '</ul>';
            $(".twitter-widget").append('<article class="twtr-tweet-text"><p class="tweet">' + userInfo + tweetText + metaInfo + '</p></article>');
        }
    });
}