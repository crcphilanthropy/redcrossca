/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var $ = jQuery.noConflict();
$(document).ready(function () {

    // Remove Stray borders
    $('.main-content > p:first-child:empty').hide();

    // Slider
    if ($('#slider').length) {
        $('#slider').royalSlider({
            autoPlay: {
                enabled: true,
                delay: 7500
            },
            arrowsNav: false,
            fadeinLoadedSlide: true,
            controlNavigationSpacing: 0,
            controlNavigation: 'thumbnails',
            thumbs: {
                autoCenter: false,
                fitInViewport: true,
                orientation: 'vertical',
                spacing: 0
            },
            loop: true,
            keyboardNavEnabled: true,
            imageScaleMode: 'none',
            imageScalePadding: 0,
            imageAlignCenter: false,
            numImagesToPreload: 3,
            autoScaleSlider: false
        });

    }


    // Newsroom Tabs
    $(".tabs li").click(function (e) {
        $(".tabs li").removeClass('active');
        $(this).addClass("active");
        var selected = $(this).find("a").attr("href");
        $('.video-set').fadeOut();
        $(selected).show();
        e.preventDefault();
    });

    // Sidebar Navigation 
    $('li.HighLighted').parents('ul').css("display", "block");

    // Program Page Accordion
    $('.program > a:first-child').on('click', function (e) {
        var $this = $(this);
        var selfClick = $this.next('.program-details').is(':visible');

        if (selfClick) {
            $this.next('.program-details').slideUp();
            $this.removeClass('active');
        } else {
            $('.program-details').slideUp();
            $('.program a').removeClass('active');
            $this.next('.program-details').slideDown();
            $this.addClass('active');
        }
        e.preventDefault();
    });

    // FAQ Item Accordion
    $('.item > a:first-child').on('click', function (e) {
        var $this = $(this);
        var selfClick = $this.next('.item-details').is(':visible');

        if (selfClick) {
            $this.next('.item-details').slideUp();
            $this.removeClass('active');
        } else {
            $('.item-details').slideUp();
            $('.item a').removeClass('active');
            $this.next('.item-details').slideDown();
            $this.addClass('active');
        }
        e.preventDefault();
    });

    // Clean up Brafton feed
    if ($('.TextContent p')) {
        $('.TextContent p').replaceWith(function () {
            return $(this).contents();
        });
    }

    // Main Menu
    $('nav.main-menu ul#menuElem > li > a').each(function (i) {
        if (i < 3) {
            var html = $(this).html();
            html = html.replace("\"", "").trim();
            var word = html.substr(0, html.indexOf(" "));
            var rest = html.substr(html.indexOf(" "));
            rest = rest.replace("\"", "");
            $(this).html(rest).prepend($("<span/>").html(word).addClass("black"));
        }
    });
    $('nav.main-menu ul#menuElem > li:eq(3)').addClass('volunteer');
    $('nav.main-menu ul#menuElem > li:eq(4)').addClass('donate');
    $('nav.main-menu ul#menuElem > li > a').css('display', 'block');

    //    $('nav.main-menu ul ul li a').each(function () {
    //        if ($(this).html().trim().length > 28 || ($(this).parent('li').hasClass('l3') && $(this).html().trim().length > 23) || $(this).html() === 'Make a Monthly Donation' || $(this).html() === 'Canadian Red Cross Fund' || $(this).html() === 'Become a Monthly Donor') {
    //            $(this).parent('li').css('paddingBottom', '15px');
    //        }
    //    });

    // Breadcrumb classes
    if ($('ul.breadcrumbs li').length > 0) {
        $('ul.breadcrumbs li:last-child').addClass('current');
        $('ul.breadcrumbs li:last-child').prev().addClass('parent');
        $('ul.breadcrumbs li:last-child').after('<li class="bc-close-container"><span class="bc-close"></span></li>');
        $('ul.breadcrumbs').css('visibility', 'visible');
    }

    // Add placeholder to search-box
    $('.search-form input.txtSearch').attr('placeholder', $('.search-form label.lblSearch').html());

    // Image Safeguards
    $('.photo-gallery img, .right-rail .cta img').wrap('<div class="image container-157x94" />');
    $('.story-grid img, .story-cta img, .wide-cta img, footer .span5 img').wrap('<div class="image container-146x88" />');
    $('.small-cta img, .small-cta2 img, article.story img').wrap('<div class="image container-69x50" />');
    $('.medium-cta img, .wide-cta2 img').wrap('<div class="image container-245x146" />');
    $('.large-feature-cta img').wrap('<div class="image container-310x135" />');

    // Don't show L3 arrows if no children
    $('ul#menuElem > li > ul > li.HighLighted').each(function () {
        if ($(this).children('a + ul').length > 0) {
            $(this).children('a').addClass('children');
        }
        if ($(this).find('li.HighLighted').length === 0) {
            $(this).children('a').addClass('selected');
        }
    });

    // Apply Placeholder Ployfill if Required
    if ($(".IE8,.IE9").size() == 0) {
        if (document.all && !document.addEventListener) {
            $('input[type="text"]').each(function () {
                var input = $(this);
                $(input).val(input.attr('placeholder'));
                $(input).focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });
                $(input).blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // First-of-type fallback
    if (document.all && !document.addEventListener) {
        $(".main-content p:first-of-type").css('marginTop', '10px');
    }

    // Modal Window in iFrame
    $('a.modal-btn').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $("#ModalLabel").html($(".modal-title.hidden", $(this).parents(".modal-cta")).html());
        if ($("#ModalLabel").html() == "") {
            $("#ModalLabel").html($(".modal-external-title", $(this).parents(".modal-cta")).html());
        }
        $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
    });

    $('.donate-track').click(function () {
        SetGAHash(this);
    });

    $('.addthis_button_expanded').remove();
    //Appeal donation form
    $(".donationCTAContainer ul.donationAmountOptions li.otherAmount span input").focus(function(){
        $(".donationCTAContainer ul.donationAmountOptions li.otherAmount input:radio").click();
    });

    $(".donationCTAContainer.mainContent ul.donationAmountOptions li input:radio").click(function(){
        if($(this).val() != "RadioAmount4"){
            $(".donationCTAContainer ul.donationAmountOptions li.otherAmount span input:text").val("");
        }
    });
   
   /* $('.RadioMonthlyDonation').click(function () {
        if (($('.monthlyRadio1').html()).length > 0) {
            $('.radioAmount1 label').html($('.monthlyRadio1').html())
        }
        if (($('.monthlyRadio2').html()).length > 0) {
            $('.radioAmount2 label').html($('.monthlyRadio2').html())
        }
        if (($('.monthlyRadio3').html()).length > 0) {
            $('.radioAmount3 label').html($('.monthlyRadio3').html())
        }
    });
    $('.RadioSingleDonation').click(function () {
        if (($('.singleRadio1').html()).length > 0) {
            $('.radioAmount1 label').html($('.singleRadio1').html())
        }
        if (($('.singleRadio2').html()).length > 0) {
            $('.radioAmount2 label').html($('.singleRadio2').html())
        }
        if (($('.singleRadio3').html()).length > 0) {
            $('.radioAmount3 label').html($('.singleRadio3').html())
        }
    });*/
    //init jwplayer

    $('body.FRCA a.CalendarAction').html('Maintenant');
    $(".full2 .large-feature-cta .standard-column:nth-child(3n+1)").css("clear","left");
    $(".right-rail a.submit-btn").append(" >");
});

function DonationTypeValidate(source, arguments) {
    arguments.IsValid = $('#rbSingleDonation').is(':checked') || $('#rbMonthlyDonation').is(':checked');
}

function SetGAHash(src) {
    try {
        var tracker = _gat._getTrackers()[0];
        if (tracker) {
            var hash = tracker._getLinkerUrl(window.location.href + '?type=single&amount=15', true);
            $('.donate-bar-hash').val(hash);
        }
    } catch (e) {
        console.log('error');
    }
}



// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function noop() {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());