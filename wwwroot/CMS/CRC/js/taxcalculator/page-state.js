define(['backbone', 'data', 'libs/utils'], function (Backbone, data, utils) {
	return Backbone.Model.extend({

		initialize: function(){
			this.on('change', this.stateChanged);
		},

		buildUrl: function(){
			var url = '';

			if (this.get('donated')) url += "/donated" + this.get('donated');
			if (this.get('firstTimeDonor')) url += "/first" + this.get('firstTimeDonor');
			if (this.get('province')) url += "/province" + this.get('province');

			return url ? 'data' + url : '';
		},

		stateChanged: function(){
			var provincialCredit = this.getProvincialCredit();
			var federalCredit = this.getFederalCredit();
			var bonusCredit = this.getBonusCredit();
			var tempTotalCredit = utils.roundNumber(federalCredit + provincialCredit + bonusCredit, 2);

			this.set('provincial', this.getFormattedValue(utils.roundNumber(provincialCredit,2)));
			this.set('federal', this.getFormattedValue(utils.roundNumber(federalCredit, 2)));
			this.set('combined', this.getFormattedValue(utils.roundNumber(federalCredit + provincialCredit, 2)));
			this.set('bonusCredit', this.getFormattedValue(utils.roundNumber(bonusCredit, 2)));
			this.set('totalCredit', this.getFormattedValue(tempTotalCredit));
			this.set('spent', this.getFormattedValue(utils.roundNumber(this.get('donated') - tempTotalCredit, 2)));
			this.set('donatedF', this.getFormattedValue(utils.roundNumber(this.get('donated'), 2)));
		},

		getProvincialCredit: function(){
			var donated = this.get('donated');
			var province = this.get('province');

			var rate = data.rates[province];
			var res = 0;

			if (donated <= 200){
				res = donated * rate.upto200;
			} else {
				res = 200 * rate.upto200 + (donated - 200) * rate.over200;
			}
			return res;
		},

		getFederalCredit: function(){
			var donated = this.get('donated');
			var rate = data.rates["CA"]; // federal

			var res = 0;

			if (donated <= 200){
				res = donated * rate.upto200;
			} else {
				res = 200 * rate.upto200 + (donated - 200) * rate.over200;
			}
			return res;
		},

		getBonusCredit: function(){
			if (this.get('firstTimeDonor') == 1)
				return Math.min(this.get('donated'),1000) * 0.25;
			else
				return 0;
		},

		getFormattedValue: function(result) {
		    if (rcTaxCalc.cultureStrings.cultureCode == 'en')
		        return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    else
		        return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replace(".",",");
		}
      
	});
});
