define(['backbone'], function (Backbone) {

	return Backbone.Router.extend({

		initialize: function (options) {
			this.pageState = options.pageState;
			this.listenTo(this.pageState, "change", this.onPageStateChanged);
			Backbone.history.start();
		},

		routes: {
			"": "loadDefault",
			"data/donated:donated/first:first/province:province": "updateCreditInfo"
		},

		loadDefault: function () {
			this.pageState.set({ province: '', donated: 0, firstTimeDonor: 0 });
		},

		updateCreditInfo: function (donated, first, province) {
			this.pageState.set({ province: province, donated: donated, firstTimeDonor: first });
		},

		onPageStateChanged: function () {
			this.navigate(this.pageState.buildUrl());
		},

	});

});
