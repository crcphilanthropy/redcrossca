define([], function(){
	return {
		roundNumber: function(num, places)
		{
			if (!places)
				places = 2; 	// default is 2 places

			// The idea is to shift the decimal right by the number of decimal places required.  Round the value.  Then
			// shift the decimal left by the same number of decimal places.  However floating point arithmetic is problematic.
			// For instance 162.295 * 100 has a result of 16229.499999999998.  If you're really curious as to why this happens there's
			// a good article here: http://docs.sun.com/source/806-3568/ncg_goldberg.html.
			// As a work-around the algorithm below converts the number to a string and moves the decimal point in the
			// string by the required number of places, rounds off the result and then moves the decimal point back by the
			// required number of places.

			var sNum = num.toString();
			var decPos = sNum.indexOf(".");
			if (decPos == -1)
			{
				sNum = sNum + ".";
				decPos = sNum.length - 1;
			}

			// Pad with trailing zeros if needed
			var sub1 = sNum.substring(decPos + 1);
			var nPad = places - sub1.length;
			for (;nPad > 0; nPad--)
			{
				sNum = sNum + "0";
			}

			// Shift the decimal point right
			sNum = sNum.substr(0, decPos) + sNum.substr(decPos + 1, places) + "." + sNum.substr(decPos + 1 + places);

			// account for leading zeros
			var nZeroCount = 0;
			var i = 0;
			for (i = 0; i < sNum.length; i++)
			{
				if (sNum[i] == "0")
				{
					nZeroCount++;
				}
				else
				{
					break;
				}
			}

			// Round off
			var nTmp = Math.round(sNum);

			// replace leading zeros
			sNum = nTmp.toString();
			for (i = 0; i < nZeroCount; i++)
			{
				sNum = "0" + sNum;
			}
			var len = sNum.length;

			// Shift the decimal point left
			sNum = sNum.substr(0, len - places) + "." + sNum.substr(len - places);

			return sNum;
		}
	}
})

