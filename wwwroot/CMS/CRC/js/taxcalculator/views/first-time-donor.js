define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['first-time-donor-info'],

		events: {
			'click input[type=radio]' : 'updateState'
		},

		initialize: function(options){
			this.pageState = options.pageState;
			this.listenTo(this.pageState, 'change', this.render);
			this.render();
		},

		render: function(){
			this.$el.html(this.template({
				firstTimeDonor: this.pageState.get('firstTimeDonor')
			})
			);
		},

		updateState: function(e){
			e.preventDefault();
			e.stopPropagation();

			var firstTime = $(this.el).find(".radio-buttons-container input[type=radio]:checked").val()
			this.pageState.set({ firstTimeDonor: firstTime });
		}
	});
});
