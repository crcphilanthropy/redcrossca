define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['donate-now'],

		initialize: function(options){
			this.pageState = options.pageState;
			this.listenTo(this.pageState, 'change', this.render);
			this.render();
		},

		render: function(){
			this.$el.html(this.template({
				amount: this.pageState.get('donated'),
				donated: this.pageState.get('donatedF'),
				spent: this.pageState.get('spent')
			}));
		}
	});
});