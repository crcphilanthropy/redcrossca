define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['donation-info'],

		events: {
			'change select#province' : 'updateState',
			'click input[type=button]' : 'updateState',
			'keypress input[type=text]': 'maybeUpdateState'
		},

		initialize: function(options){
			if(rcTaxCalc.cultureStrings.cultureCode == 'en'){
				this.provinces = options.provinces;
			}
			else {
				this.provinces = options.provincesFr;
			}

			this.pageState = options.pageState;
			this.listenTo(this.pageState, 'change', this.render);

			this.render();
		},

		render: function(){
			this.$el.html(this.template({
				provinces: this.provinces,
				selectedProvince: this.pageState.get('province'),
				donated: this.pageState.get('donated')
			}));
		},

		updateState: function(e){
			e.preventDefault();
			e.stopPropagation();

			var province = $(this.el).find("#province").val()
			var donated = $(this.el).find("#donation-amount-2013").val();
			this.pageState.set({ province: province, donated: donated})
		},

		maybeUpdateState: function(e){
			if (e.which === 13){
				e.preventDefault();
				e.stopPropagation();
				this.updateState(e);
			}
		}
	});
});