define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['tax-credit-info'],

		initialize: function(options){
			this.pageState = options.pageState;
			this.listenTo(this.pageState, 'change', this.render);

			this.render();
		},

		render: function(){
			this.$el.html(this.template({
				provincial: this.pageState.get('provincial'),
				federal: this.pageState.get('federal'),
				combined: this.pageState.get('combined')
			}));
		}
	});
});