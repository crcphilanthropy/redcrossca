define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['first-time-donor-intro'],

		initialize: function(){
			this.render();
		},

		render: function(){
			this.$el.html(this.template());
		}
	});
});
