define(['backbone', 'templates'], function(Backbone, temlates){
	return Backbone.View.extend({

		template: temlates['bonus-credit-info'],

		initialize: function(options){
			this.pageState = options.pageState;
			this.listenTo(this.pageState, 'change', this.render);
			this.render();
		},

		render: function(){
			this.$el.html(this.template({
				bonusCredit: this.pageState.get('bonusCredit'),
				totalCredit: this.pageState.get('totalCredit')
			}));
		}

	});
});
