define([
	'backbone',
	'views/donation-info',
	'views/tax-credit',
	'views/first-time-donor',
	'views/bonus-credit',
	'views/donate-now',
	'views/calculate-tax-intro',
	'views/first-time-donor-intro',
	'page-state',
	'router',	
	'data'
],
	function(Backbone,
					 DonationInfoView, TaxCreditInfoView, FirstTimeDonorView, BonusCreditView, DonateNowView, CalculateTaxView, FirstTimeIntroView,
					 PageState, Router, data) {

		return function() {

			var pageState =  new PageState();

			new DonationInfoView({ el: '.donation-info', provinces: data.provinces, provincesFr:data.provincesFr, pageState: pageState });
			new TaxCreditInfoView({ el: '.tax-credit-info', pageState: pageState });
			new FirstTimeDonorView({ el: '.first-time-donor', pageState: pageState });
			new BonusCreditView({ el: '.bonus-credit-info', pageState: pageState });
			new DonateNowView({ el: '.summary', pageState: pageState });
			new CalculateTaxView({ el: '.calculate-tax-intro' });
			new FirstTimeIntroView({ el: '.first-time-donor-intro' });

			new Router({ pageState: pageState});

		};
	}
);

