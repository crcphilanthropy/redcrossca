require.config({	
	baseUrl : '/CRC/js/taxcalculator',

	paths : {
		jquery : 'vendor/jquery-1.10.2.min',
		underscore : 'vendor/underscore-min',
		backbone : 'vendor/backbone-min',
		text : 'vendor/require/text'

	},

	shim : {
		'underscore' : {
			exports : '_'
		},

		'backbone' : {
			deps : ['underscore', 'jquery'],
			exports : 'Backbone'
		}
	}
});

require(['app'], function(app){
	app();
});
