define([],function () {	
	return {
		provinces: [
			{ value: "AB", title: rcTaxCalc.cultureStrings.provinces.AB },
			{ value: "BC", title: rcTaxCalc.cultureStrings.provinces.BC },
			{ value: "MB", title: rcTaxCalc.cultureStrings.provinces.MB },
			{ value: "NB", title: rcTaxCalc.cultureStrings.provinces.NB },
			{ value: "NL", title: rcTaxCalc.cultureStrings.provinces.NL },
			{ value: "NT", title: rcTaxCalc.cultureStrings.provinces.NT },
            { value: "NS", title: rcTaxCalc.cultureStrings.provinces.NS },
			{ value: "NU", title: rcTaxCalc.cultureStrings.provinces.NU },
			{ value: "ON", title: rcTaxCalc.cultureStrings.provinces.ON },
			{ value: "PE", title: rcTaxCalc.cultureStrings.provinces.PE },
			{ value: "QC", title: rcTaxCalc.cultureStrings.provinces.QC },
			{ value: "SK", title: rcTaxCalc.cultureStrings.provinces.SK },
			{ value: "YT", title: rcTaxCalc.cultureStrings.provinces.YT }
		],

		provincesFr: [
			{ value: "AB", title: rcTaxCalc.cultureStrings.provinces.AB },
			{ value: "BC", title: rcTaxCalc.cultureStrings.provinces.BC },
            { value: "PE", title: rcTaxCalc.cultureStrings.provinces.PE },
            { value: "MB", title: rcTaxCalc.cultureStrings.provinces.MB },
            { value: "NB", title: rcTaxCalc.cultureStrings.provinces.NB },
            { value: "NS", title: rcTaxCalc.cultureStrings.provinces.NS },
			{ value: "NU", title: rcTaxCalc.cultureStrings.provinces.NU },
            { value: "ON", title: rcTaxCalc.cultureStrings.provinces.ON },
			{ value: "QC", title: rcTaxCalc.cultureStrings.provinces.QC },
            { value: "SK", title: rcTaxCalc.cultureStrings.provinces.SK },
            { value: "NL", title: rcTaxCalc.cultureStrings.provinces.NL },
			{ value: "NT", title: rcTaxCalc.cultureStrings.provinces.NT },
			{ value: "YT", title: rcTaxCalc.cultureStrings.provinces.YT }
		],

		rates: {
			"AB": { upto200: 0.100, over200: 0.210 },    // Alberta
			"BC": { upto200: 0.0506, over200: 0.147 },   // British Columbia
			"MB": { upto200: 0.108, over200: 0.174 },    // Manitoba
			"NB": { upto200: 0.0968, over200: 0.1795 },  // New Brunswick
			"NL": { upto200: 0.087, over200: 0.183 },    // Newfoundland and Labrador
			"NT": { upto200: 0.059, over200: 0.1405 },   // Northwest Territories
			"NS": { upto200: 0.0879, over200: 0.210 },   // Nova Scotia
			"NU": { upto200: 0.04, over200: 0.115 },     // Nunavut
			"ON": { upto200: 0.0505, over200: 0.1116 },  // Ontario
			"PE": { upto200: 0.098, over200: 0.167 },    // Prince Edward Island
			"QC": { upto200: 0.200, over200: 0.2400 },   // Quebec
			"SK": { upto200: 0.110, over200: 0.150 },    // Saskatchewan
			"YT": { upto200: 0.064, over200: 0.1280 },  // Yukon
			"CA": { upto200: 0.15, over200: 0.29 },			 // FEDERAl
			"OUT": { upto200: 0.0, over200: 0.0 },    	 // Outside Canada
			"": { upto200: 0.0, over200: 0.0 }
		}
	};
});