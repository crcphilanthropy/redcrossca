﻿var donateForm = {
    appeals: null,
    hashPreText: 'selected-appeal-',
    changeAppeal: function () {
        var appealGuid = $('.ddl-appeals').val();
        $('.show-hide-appeal').hide();
        $('.show-hide-appeal.' + appealGuid).fadeIn('slow');
        var currentListItem = $(".learn-more-panel ul li." + appealGuid);
        $(currentListItem).parent().prepend(currentListItem);
        $(".learn-more-panel").scrollTop(0);
        if (donateForm.appeals != undefined) {
            var selectedAppeal = null;
            for (var i = 0; i < donateForm.appeals.length; i++) {
                if (donateForm.appeals[i].DocumentGuid == appealGuid) {
                    selectedAppeal = donateForm.appeals[i];
                }
            }

            $('.price-points input[type="radio"]').prop('checked', false);
            $('.checkbox input').prop('checked', false);
            $('.txt-other').val('$');

            if (selectedAppeal == null) return;
            window.location.hash = donateForm.hashPreText + appealGuid;

            $('input.price-point-1').val(selectedAppeal.PricePoint1);
            $('input.price-point-2').val(selectedAppeal.PricePoint2);
            $('input.price-point-3').val(selectedAppeal.PricePoint3);
            $('label.price-point-1 span.value').text(selectedAppeal.PricePoint1Label);
            $('label.price-point-2 span.value').text(selectedAppeal.PricePoint2Label);
            $('label.price-point-3 span.value').text(selectedAppeal.PricePoint3Label);
            $(".chk-monthly input").prop("disabled", selectedAppeal.DisableMonthlyOption);
            $(".chk-monthly input").prop("checked", selectedAppeal.IsMonthlyDonationOnly);
            $('li.price-point-1 a span').text(selectedAppeal.PricePoint1Label);
            $('li.price-point-2 a span').text(selectedAppeal.PricePoint2Label);
            $('li.price-point-3 a span').text(selectedAppeal.PricePoint3Label);
            $('li.price-point-1').attr("data-value", selectedAppeal.PricePoint1Label);
            $('li.price-point-2').attr("data-value", selectedAppeal.PricePoint2Label);
            $('li.price-point-3').attr("data-value", selectedAppeal.PricePoint3Label);
        }
    },

    pageAppealLoadAt: new Date(),
    initDonateFormTracking: function () {

        //executes with JQuery mousedown on button in form 
        $('.btn-donate').click(function (event) {
            var appealTelNum1 = $('#appealTelNum1').val();
            if ((Math.abs(new Date() - donateForm.pageAppealLoadAt) < 1000)) {
                event.preventDefault();
                return false;
            }

            if (appealTelNum1 != undefined) {
                if (appealTelNum1.length > 0) {
                    event.preventDefault();
                    return false;
                }
            }

            event.preventDefault();
            var donationWidgetValue = "";
            var donationWidgetLabel;
            var donationWidgetCycle;

            //gathers information on the Donation Cycle. 
            if ($('.chk-monthly input').checked) {
                donationWidgetCycle = "Monthly";
            } else {
                donationWidgetCycle = "One Time";
            }

            //gathers information on the label
            var getInputAmountText = $(".txt-other").val();
            getInputAmountText = getInputAmountText.replace('$', '');
            if (getInputAmountText.length > 0) {
                // something was entered in OTher text area.  Ensure it's valid
                var getAmount = GetValidatedAmount(getInputAmountText);
                if (getAmount > 0) {
                    donationWidgetLabel = "Entered";
                    donationWidgetValue = Math.round(getAmount);
                    //$(".txt-other").val(getAmount);      // set the textbox value to the "possibly" updated new amount
                }
                else {
                    return false;
                }
            } else {
                donationWidgetLabel = "Selected";

                var selectedValue = $("input[type='radio'][name='pricepoint']:checked");
                if (selectedValue.length > 0) {
                    donationWidgetValue = selectedValue.val();
                }
            }

            //sends event into GA 
            dataLayer.push({ "event": "generic-event", "eventCategory": " Donate Main Form Widget (Drop Down)", "eventAction": donationWidgetCycle, "eventLabel": donationWidgetValue, "eventValue": donationWidgetValue });
            $(".btn-donate").off("click");
            setTimeout(function () { // after 1 second, submit the form
                $(".btn-donate").trigger("click");
            }, 1000);
            return true;
        });
    },



    init: function () {

        donateForm.initDonateFormTracking();

        var selectedAppeal = window.location.hash;
        if (selectedAppeal.length > 1) {
            selectedAppeal = selectedAppeal.substring(1); //remove #
            selectedAppeal = selectedAppeal.replace(donateForm.hashPreText, '');
        }

        if (selectedAppeal != undefined && selectedAppeal != 'null' && selectedAppeal != '')
            $('.ddl-appeals').val(selectedAppeal);

        $('.ddl-appeals').change(function () { donateForm.changeAppeal(); });
        var learnMoreWidth = ($(".learn-more-container").width());

        $('.learn-more-tab').click(function (e) {

            $(".learn-more-container").toggleClass("open");
            if ($(".learn-more-container").hasClass("open")) {
                $(".learn-more-container").animate({ "left": 0 }, "slow");
            } else {
                $(".learn-more-container").animate({ "left": -learnMoreWidth }, "slow");
            }
            e.stopPropagation();
        });
        $('.txt-other').val("$");
        $(document).click(function () {
            $(".learn-more-container").removeClass("open");
            $(".learn-more-container").animate({ "left": -learnMoreWidth }, "slow");
        });
        $(".learn-more-container").click(function(e){
            var target = $(e.target);
            if(! target.is('span.close')){

               e.stopPropagation();
            }

        });

        $(".donate-form select").select2();
        $('.price-points input').focus(function(){
            $('.txt-other').val("$");
        });
        $('.txt-other').focus(function () {
            $(this).val("");
            $('input.price-point-4').prop('checked', true);
        });

        donateForm.changeAppeal();

        if($('.ros-banner').length){
            $('footer').css('margin', '0 0 100px');
        }

        // ROS Banner override
        $('.ros-banner .Banner').removeAttr("onclick");

        $('.ros-banner .Banner').removeAttr("onmouseup");


        // Font Size Changer
        $('.home, footer, .left-column, .main-content, .right-rail, #sitemap, .large-feature-cta, .main-menu, header, .banner').jfontsize({
            btnMinusClasseId: '.smallFont',
            btnDefaultClasseId: '.defaultFont',
            btnPlusClasseId: '.largeFont',
            btnMinusMaxHits: 2,
            btnPlusMaxHits: 4,
            sizeChange: 1
        });

    }
};

