.search-wrap {
  background: $panel-bg;
  padding: 20px;
  margin-bottom: 20px;
  position: relative;

  h3, h2 {
    border-bottom: 1px solid #bdbdbd;
    padding-bottom: 15px;
    margin: 0 0 15px 0;
  }

  fieldset {
    width: 100%;
    padding: 0;
    border: 0;
    margin: 0 0 20px 0;

    label {
      font-weight: 600;
      margin-bottom: 5px;
      text-align: left;
    }
    input {
      width: 100%;
      height: 40px;
      padding: 10px;
      margin-bottom: 0px;
    }
    select {
      height: 40px;
      padding: rem-calc(0 10);
      margin-bottom: 0px;
      border-radius: 6px;
    }
  }

  .search-cat, .submit {
    fieldset {
      margin-bottom: 0;
    }
  }

  .submit {
    padding-top: 24px;

    a {
      padding: 12px 15px 12px 4px;
      width: 100%;
      margin-bottom: 0;
    }
  }

  .and-or {
    text-align: center;
    padding-top: rem-calc(35);
    p {
      margin: 0;
    }
  }

  /*--- If the select box has NOT been triggered ---*/

  &.optional-field {

    &:before {
      content: '';
      width: 93%;
      height: 1px;
      background: #bdbdbd;
      position: absolute;
      top: 209px;
      left: 4%;
    }
    fieldset {
      margin: 0 0 40px 0;
    }

    .search-cat {
      display: none;
    }
    .submit {
      padding-top: 0;

      fieldset {
        margin: 0;
      }
    }

  }

  &.disabled {
    .submit {
      a {
        background: $disabled;
        cursor: default;
      }
    }
  }

  /*--- Local Branch ---*/

  &.local-branch {

    &:before {
      content: "";
      display: none;
    }

    .submit {

      padding-left: 20px;

      a {
        width: auto;
        padding: 12px 37px 12px 29px;
      }
    }
    fieldset {
      margin-bottom: 0;
    }
  }
}

@media #{$small-only} {
  .search-wrap {
    background: $panel-bg;
    padding: 20px;
    margin-bottom: 20px;
    position: relative;

    &:before {
      content: '';
      display: none;
    }

    h3 {
      border: none;
      margin-bottom: 15px;
      padding-bottom: 0;
    }

    fieldset {
      margin-bottom: 20px;
    }

    .and-or {
      text-align: left;
      padding: 0px 0px 20px 0px
    }

    .search-cat {
      fieldset {
        margin-bottom: 20px;
      }
    }

    .submit {
      padding-top: 0px;
    }

    /*--- Local Branch ---*/

    &.local-branch {

      .submit {

        padding-left: 0px;

        a {
          width: 100%;
          padding: 12px 37px 12px 29px;
        }

        fieldset {
          margin-bottom: 0px;
        }
      }

      fieldset {
        margin-bottom: 20px;
      }
    }
  }
}


