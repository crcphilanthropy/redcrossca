/*var crc = {};

 (function ($) {
 "use strict";



 })(jQuery);

 */

$(document).foundation('equalizer', 'reflow');


$(document).ready(function(){

    // Accessible Mega Menu
    //$('.search-wrap').accessibleMegaMenu();
    if ($('.main-nav nav').length > 0) {
        $('.main-nav nav').accessibleMegaMenu();
        $('.province-dd-wrap').accessibleMegaMenu();
    }
    //$('.nav-secondary .utility ul').accessibleMegaMenu();
    //$('.log-wrap').accessibleMegaMenu();

  $(".menu-btn, span.close").on("click",function(){
    $(".flexpanel").toggleClass("open");
  });


	// Mobile Nav Custom JS

  $(".navigation").on("click",".nav-back",function(){
    //$(this).closest(".nav-group").removeClass("nav-active");
    //
    setTimeout(function(){
    //
    },1000);

    $(this).closest(".nav-group").remove();

  });

  $(".navigation").on("click","a.has-child", function(){

    setTimeout(function(){
      $(".nav-active li:first-child a").focus();
      //$(".nav-active .nav-back").focus();
    },100);

  });

 // Accordion JS

  var question = $(".accordion div .question");
  var answer = $(".accordion div .answer");

  question.on("click",function(){
   if ($(this).parent("div").hasClass("active")) {
       $(this).parent("div").removeClass("active");
   }
   else{
       if($(window).width() > 950) {
           question.parent("div").removeClass("active");
       }
    $(this).parent("div").toggleClass("active");
   }
  });

  $(".accordion > div:first-child").addClass("active");

  if ($('.slider').length > 0) {

    $(".slider").slick({
    dots:true,
    arrows:true,
    mobileFirst:true,
    infinite:true,
    lazyLoad: 'progressive',
    customPaging: function(slider, i) {
     var buttonMarkup = '<button type="button" data-role="none" >';
     if(btns[i][0] !== ""){
      buttonMarkup += '<img class="icon" src=\''+ (btns[i][0]) +'\'/>';
     }
     buttonMarkup += (btns[i][1]);
     buttonMarkup += '</button>';
     return buttonMarkup;
    }
    });

    var btnWrapperClass = 'slick-dots-count-' + btns.length;

    $(".slick-dots").wrap("<div class='slick-dots-wrapper' id=" + btnWrapperClass + "></div>");

    if(btns.length == 1){
    var singleSlideButton = '<div class="slick-dots-wrapper" id="slick-dots-count-1">';
    singleSlideButton += '<ul class="slick-dots"><li class="slick-active">';
    singleSlideButton += '<button type="button" data-role="none">';
    singleSlideButton += '<img class="icon" src=\''+ (btns[0][0]) +'\'/>';
    singleSlideButton += (btns[0][1]);
    singleSlideButton += '</button>';
    singleSlideButton += '</ul></li></div>';

    $(".slider .slide:last-child").append(singleSlideButton);
    }

  }

 // Make SVGs Embedded from IMG Tags
 $('.slick-dots-wrapper img').each(function(){
  var $img = $(this);
  var imgID = $img.attr('id');
  var imgClass = $img.attr('class');
  var imgURL = $img.attr('src');

  jQuery.get(imgURL, function(data) {
   // Get the SVG tag, ignore the rest
   var $svg = jQuery(data).find('svg');

   // Add replaced image's ID to the new SVG
   if(typeof imgID !== 'undefined') {
    $svg = $svg.attr('id', imgID);
   }
   // Add replaced image's classes to the new SVG
   if(typeof imgClass !== 'undefined') {
    $svg = $svg.attr('class', imgClass+' replaced-svg');
   }

   // Remove any invalid XML tags as per http://validator.w3.org
   $svg = $svg.removeAttr('xmlns:a');

   // Replace image with new SVG
   $img.replaceWith($svg);

  }, 'xml');

 });



    if ($('#table').length > 0) {
        // Table Sorting
        $("#table").tablesorter();
    }

    // Don't show L3 arrows if no children
    $('ul.side-nav > li > ul > li.active').each(function () {
        if ($(this).children('a + ul').length > 0) {
            $(this).children('a').addClass('children');
        }
        if ($(this).find('li.active').length === 0) {
            $(this).children('a').addClass('selected');
        }
    });



});

function currentState(){

  if( $(".nav-group").length > 1) {

    var href = window.location.pathname;
    var relativePath = href.substr(href.lastIndexOf('/') + 1);

    $(".nav-active li a").each(function(){

        var urlPath = $(this).attr("href");

        if (urlPath.indexOf(relativePath) != -1) {

            $(this).parent("li").addClass("current");

            return false;
        }
    });

  }
}  