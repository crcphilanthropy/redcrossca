/**
 * Generates and manages navigation systems
 * @module navigation
 * @version 0.2
 * @licence (c) 2014 ecentricarts inc. http://ecentricarts.com
 */

define([
    'underscore',
    'utility'
  ],
  function ( _, utils ) {
    "use strict";

    /**
     * Instantiates a navigation controller
     * @alias module:navigation
     * @param element {HTMLElement}          top level wrapper element for navigation markup
     * @param [options] {Namespace}     optional settings hash (overrides defaults defined in this.settings)
     * @constructor
     */
    var Navigation = function (element, options, isIE8) {

      this.settings = {                               // the default settings hash
        template: null,                               // underscore template for rendering dynamic nav
        data: null,                                   // JSON data structure representing the nav tree
        classNames: {                                 // css classes for nav tree traversal
          group: 'nav-group',
          homeLink: 'nav-home',
          parentLink: 'nav-back',
          childLink: 'nav-child',
          prev: 'nav-prev',
          next: 'nav-next',
          active: 'nav-active',
          remove: 'nav-remove'
        },
        functions: {                                           // instance overrides for Navigation.prototype methods
          transition: null
        }
      };

      _.extend(this.settings, options);                        // merging default settings with passed in options
      this.element = this.getHTMLElement(element);             // wrapper/container DOM node

      var currentState = this.findCurrentState(location.pathname, this.settings.data); // sets the current menu level based on url
      this.currentNode = this.getCurrentNode(currentState);   // reference to current node in the nav tree
      this.prevStack = currentState.stack;                    // stack of references to ancestor nodes
      this.settings.data.root = true;                         // add indicator/flag to root node

      this.createPrevLayers(this.prevStack);

      this.setupListeners(this.element, isIE8);                // setting up tap/click listeners on nav wrapper

      if (this.element.nodeType !== 1)
        throw new Error('Navigation(): first argument is required (DOM element or jQuery selector).');

      if (!this.settings.data || typeof this.settings.data !== 'object')
        throw new Error('Navigation(): data must be JSON.');

      if (!this.settings.template || typeof this.settings.template !== 'string')
        throw new Error('Navigation(): template must be a string.');

      this.renderNavigation();
    };

    // Methods
    // -------
    Navigation.prototype = {

      /**
       * handles tap events that occur on <a> tags within this.element - checks for pre-defined class names and calls tree
       * traversal methods accordingly
       * @method
       * @param event
       */
      tapHandler: function (event) {
        var target = event.target,
          index, classes;

        event.stopPropagation();
        event.preventDefault();

        if (target.tagName.toLowerCase() === 'a') {
          classes = this.settings.classNames;

          if (this.hasClass(target, classes.parentLink))
            this.moveUp();

          else if (this.hasClass(target, classes.homeLink))
            this.moveUp(this.prevStack.length);

          else if (this.hasClass(target, classes.childLink)) {
            index = target.getAttribute('data-index');

            if (this.hasChildren(target, index))
              this.moveDown(index);
            else
              document.location.href = target.getAttribute('href');
          }

          else document.location.href = target.getAttribute('href');
        }
      },

      /**
       * sets a child node from the current children array this.currentNode
       * @method
       * @param i index of selected element in children array
       */
      moveDown: function (i) {
        var classes = this.settings.classNames,
          newDiv = document.createElement('div'),
          $oldDiv = $("." + classes.group + "." + classes.active, this.element).first(),
          oldDiv = this.getHTMLElement($oldDiv);

        this.prevStack.push(this.currentNode);
        this.currentNode = this.currentNode.children[i];
        this.transition(newDiv, oldDiv, classes.next);
      },

      /**
       * sets an ancestor node from the menu tree to this.currentNode
       * @method
       * @param {Number} [n]  optional number of levels "up" to traverse
       */
      moveUp: function (n) {
        var classes = this.settings.classNames,
          $newDiv = $("." + classes.group + "." + classes.prev, this.element).last(),
          newDiv = this.getHTMLElement($newDiv),
          oldDiv = $("." + classes.active, this.element).first();


        if (typeof n === 'undefined')  n = 1;
        if (n > this.prevStack.length)  n = this.prevStack.length;

        for (n; n > 0; n--) {
          this.currentNode = this.prevStack.pop();
        }
        this.transition(newDiv, oldDiv, classes.prev);
      },

      /**
       * handles transitions between rendered views
       * overridable via this.settings.functions.transition (arguments must remain the same)
       * @method
       * @param newDiv        the requested node's view
       * @param oldDiv        the previous node's view
       * @param classname     class name indicating direction (prev [parent, ancestor], next [child]);
       */
      transition: function (newDiv, oldDiv, classname) {
        var classes = this.settings.classNames,
          $oldDiv = $(oldDiv),
          $newDiv = $(newDiv);

        newDiv.innerHTML = this.renderView();
        $oldDiv.removeClass(classes.active);
        $newDiv.addClass(classes.group).addClass(classname);

        if (classname === classes.prev) {
          $oldDiv.addClass(classes.remove);
        } else {
          $oldDiv.addClass(classes.prev);
          this.element.appendChild(newDiv);
        }

        window.setTimeout(function () {
          $newDiv.removeClass(classname).addClass(classes.active);
        }, 10);
      },

      /**
       * parses HTML template, optionally using data if data has been set
       * @method
       * @returns {String}    a string of rendered html
       */
      renderView: function (node) {
        var compile = _.template(this.settings.template);
        return node ? compile(node) : compile(this.currentNode);
      },

      /**
       * removes previous nodes once the new element has been drawn & transitions onto the screen
       * bound to transitionEnd event by constructor
       * @method
       * @param event
       */
      removeNodes: function (event) {
        var classes = this.settings.classNames,
          match;

        event.stopPropagation();

        match = $("." + classes.remove, this.element);
        match.remove();
      },

      hasChildren: function (target, index) {
        var selected = index ? this.currentNode.children[index] : null;
        return selected && selected.children && selected.children.length !== 0;
      },

      /**
       * searches for a node that matches to the current url path
       * and returns the node and stack of his ancestors
       * @path - current url path
       * @root - root of the json data
       */
      findCurrentState: function (path, root) {
        var res = this.searchNode(path, root, []);
        return res || {stack: [], node: root};
      },

      getCurrentNode: function (currentState) {
        return (currentState.stack.length > 0 && !this.isLandingPage(currentState.node))
          ? currentState.stack.pop()
          : currentState.node;
      },

      isLandingPage: function (node) {
        return !node.hideLink && node.children.length > 0;
      },

      /**
       * recursively iterates through child nodes to find a node that matches passed in path
       * @path - url path
       * @node - root node to iterate over
       * @stack - stack of navigation views, stores prev views
       */
      searchNode: function (path, node, stack) {
        if (this.normilizePath(node.url) === path)
          return {stack: stack, node: node};

        var currentStack = stack.slice();
        currentStack.push(node);

        //for (var i = 0; i < node.children.length; i++) {
       //   var res = this.searchNode(path, node.children[i], currentStack);
       //   if (res) return res;
       // }
      },

      createPrevLayers: function(stack) {
        var i, classes = this.settings.classNames;

        for (i = 0; i < stack.length; i++) {
          var newDiv = document.createElement('div');
          $(newDiv).addClass(classes.group).addClass(classes.prev);
          newDiv.innerHTML = this.renderView(stack[i]);
          this.element.appendChild(newDiv);
        }
      },

      /**
       * removes leading hash from the url
       * utility method to normalize url format for dev and production environments
       * @path - url path
       */
      normilizePath: function (path) {
        if (path.indexOf("#") === 0) return path.substr(1);
        else return path;
      },

      hasClass: function (el, cls) {
        return $(el).hasClass(cls);
      },

      getHTMLElement: function (element) {
        return typeof element[0] === 'undefined' ? element : element[0];
      },

      setupListeners: function (wrapper, isIE8) {


       
          wrapper.addEventListener('click', this.tapHandler.bind(this));
          wrapper.addEventListener(utils.getTransitionEvent(), this.removeNodes.bind(this), false);


        //$(wrapper).on('click', this.tapHandler.bind(this));
        //wrapper.addEventListener('click', this.tapHandler.bind(this));d
        //wrapper.addEventListener(utils.getTransitionEvent(), this.removeNodes.bind(this), false);
      },

      renderNavigation: function () {
        var newDiv = document.createElement('div'),
          classes = this.settings.classNames;
        $(newDiv).addClass(classes.group).addClass(classes.active);
        newDiv.innerHTML = this.renderView();
        this.element.appendChild(newDiv);
      }
    };

    return Navigation;
  });