module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'nested',
          sourceMap: true
        },
        files: {
          'build/css/app.css': 'src/scss/app.scss'
        }
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      dev: {
        files: ['src/*.html', 'src/js/**', 'src/scss/**', 'src/css/**'],
        tasks: ['sass', 'sync'],
        options: {
          livereload: true
        }
      },
      psd: {
        files: ['images-assets/**'],
        tasks: ['sync:psd'],
        options: {
          livereload: true
        }
      }
    },

    sync: {
      build: {
        files: [
          {
            cwd: 'src',
            src: ['**/*.html', 'js/**', 'images/**'],
            dest: 'build'
          },
          {
            cwd: 'build',
            src: ['css/*'],
            dest: '../'
          },
          {
            cwd: 'build',
            src: ['js/*'],
            dest: '../'
          }
        ],
        verbose: true
      },
      psd: {
        files: [
          {
            cwd: 'images-assets',
            src: '**',
            dest: 'build/images'
          }
        ],
        verbose: true
      }
    },

    connect: {
      options: {
       hostname: 'localhost',
       //hostname: '192.168.100.165',
        port: 8580,
        livereload: 35729
      },
      dev: {
        options: {
          open: 'http://localhost:8580/build',
          //open: 'http://192.168.100.165:8580/build',
          base: ['']
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-sync');

  grunt.registerTask('build', ['sass', 'sync']);
  grunt.registerTask('default', ['build','connect:dev', 'watch']);
}
