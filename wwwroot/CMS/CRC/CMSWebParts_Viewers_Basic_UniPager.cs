using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.Helpers;

public partial class CMSWebParts_Viewers_Basic_UniPager
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        base.PreRender += CMSWebPartsViewersBasicUniPagerPreRender;
    }

    void CMSWebPartsViewersBasicUniPagerPreRender(object sender, EventArgs e)
    {
        if (pagerElem == null || pagerElem.PageCount <= 1) return;

        var nextUrl = URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (pagerElem.CurrentPage + 1).ToString()));
        var prevUrl = pagerElem.CurrentPage == 2 ? URLHelper.GetAbsoluteUrl(URLHelper.RemoveParameterFromUrl(RequestContext.CurrentURL, "page")) : URLHelper.GetAbsoluteUrl(URLHelper.AddParameterToUrl(RequestContext.CurrentURL, "page", (pagerElem.CurrentPage - 1).ToString()));

        if (pagerElem.CurrentPage > 1)
            Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='prev' href='{0}' />\n", prevUrl) });

        if (pagerElem.CurrentPage < pagerElem.PageCount)
            Page.Header.Controls.Add(new Literal() { Text = string.Format("\n<link rel='next' href='{0}' />\n", nextUrl) });


        
    }
}