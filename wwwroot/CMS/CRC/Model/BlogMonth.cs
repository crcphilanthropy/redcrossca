﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSApp.CRC.Model
{
    public class BlogMonth
    {
        public string MonthText { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Url { get; set; }
        public int Count { get; set; }
    }
}