﻿using System;
using System.Collections.Generic;

namespace CMSApp.CRC.Model
{
    public class Tweet
    {
        public string id_str { get; set; }
        public string created_at { get; set; }
        public string time_span { get; set; }
        public string text { get; set; }
        public List<TweetImage> images { get; set; }
        public Tweet retweeted_status { get; set; }
        public string username { get; set; }
        public string displayName { get; set; }
        public string userPic { get; set; }
        public int favourite_count { get; set; }
        public int retweet_count { get; set; }
        internal DateTime time { get; set; }
        public object everything { get; set; }
    }

    public class TweetImage
    {
        public string image_url { get; set; }
    }
}
