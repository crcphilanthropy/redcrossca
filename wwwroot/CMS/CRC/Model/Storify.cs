﻿using System.Collections.Generic;

namespace CMSApp.CRC.Model
{
    public class Date
    {
        public string created { get; set; }
        public string modified { get; set; }
        public string published { get; set; }
    }

    public class Quoted
    {
        public string username { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public object notified_at { get; set; }
        public string avatar { get; set; }
        public string userid { get; set; }
    }

    public class CreatedWith
    {
        public string href { get; set; }
        public string appname { get; set; }
        public string name { get; set; }
    }

    public class Meta
    {
        public List<Quoted> quoted { get; set; }
        public List<string> hashtags { get; set; }
        public CreatedWith created_with { get; set; }
    }

    public class Embed
    {
        public int clicks { get; set; }
        public int views { get; set; }
        public string href { get; set; }
        public string domain { get; set; }
    }

    public class Elements
    {
        public int text { get; set; }
        public int quote { get; set; }
        public int image { get; set; }
        public int video { get; set; }
        public int link { get; set; }
        public int other { get; set; }
    }

    public class Stats
    {
        public int popularity { get; set; }
        public int views { get; set; }
        public int likes { get; set; }
        public int comments { get; set; }
        public int elementComments { get; set; }
        public List<Embed> embeds { get; set; }
        public Elements elements { get; set; }
        public int clicks { get; set; }
    }

    public class Options
    {
        public bool infinite_scroll { get; set; }
        public bool hide_stats { get; set; }
        public bool allow_embedding { get; set; }
        public bool comments { get; set; }
        public bool related_stories { get; set; }
        public bool ga { get; set; }
    }

    public class Notifications
    {
        public bool newsletter { get; set; }
        public bool digest { get; set; }
        public bool likes { get; set; }
        public bool comments { get; set; }
        public bool follower { get; set; }
        public bool quoted { get; set; }
        public bool friend_quoted { get; set; }
        public bool autoshare { get; set; }
        public bool element_comment { get; set; }
        public bool element_like { get; set; }
        public bool story_comment { get; set; }
        public bool story_like { get; set; }
    }

    public class Fonts
    {
        public string title { get; set; }
        public string body { get; set; }
    }

    public class Colors
    {
        public string text { get; set; }
        public string link { get; set; }
        public string background { get; set; }
    }

    public class Typekit
    {
        public List<object> fonts { get; set; }
    }

    public class Style
    {
        public Fonts fonts { get; set; }
        public Colors colors { get; set; }
        public Typekit typekit { get; set; }
    }

    public class Settings
    {
        public string comments { get; set; }
        public bool facebook_autoshare { get; set; }
        public bool facebook_quoteimg { get; set; }
        public bool facebook_post { get; set; }
        public bool twitter_post { get; set; }
        public bool ban_notify { get; set; }
        public Options options { get; set; }
        public Notifications notifications { get; set; }
        public Style style { get; set; }
    }

    public class Stats2
    {
        public int views { get; set; }
        public int subscriptions { get; set; }
        public int subscribers { get; set; }
        public int stories { get; set; }
        public int embeds { get; set; }
    }

    public class Date2
    {
        public string updated { get; set; }
        public string last_story { get; set; }
        public string last_seen { get; set; }
        public object last_email { get; set; }
        public object featured { get; set; }
        public string created { get; set; }
    }

    public class CoverPhoto
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class FeaturesEnabled
    {
        public bool custom_embed_style { get; set; }
        public bool private_stories { get; set; }
        public bool html_for_seo { get; set; }
        public bool no_advertising { get; set; }
        public bool business_options { get; set; }
        public bool headerless_embed { get; set; }
        public bool pdf { get; set; }
        public bool realtime_updates { get; set; }
        public bool storylock { get; set; }
        public int maxEditors { get; set; }
    }

    public class AgencyInfo
    {
        public int maxCustomersNum { get; set; }
        public List<object> customers { get; set; }
    }

    public class Keys
    {
        public object approve { get; set; }
        public object remove { get; set; }
    }

    public class CustomerInfo
    {
        public bool approved { get; set; }
        public bool removalApproved { get; set; }
        public object agencyId { get; set; }
        public object agencyUsername { get; set; }
        public object approved_at { get; set; }
        public object paid_plan { get; set; }
        public Keys keys { get; set; }
    }

    public class Agency
    {
        public bool isAgency { get; set; }
        public AgencyInfo agencyInfo { get; set; }
        public bool isCustomer { get; set; }
        public CustomerInfo customerInfo { get; set; }
    }

    public class Author
    {
        public string _id { get; set; }
        public string username { get; set; }
        public int _access { get; set; }
        public string name { get; set; }
        public object bio { get; set; }
        public object location { get; set; }
        public string website { get; set; }
        public string avatar { get; set; }
        public string permalink { get; set; }
        public Settings settings { get; set; }
        public string lang { get; set; }
        public Stats2 stats { get; set; }
        public Date2 date { get; set; }
        public CoverPhoto coverPhoto { get; set; }
        public bool canFeatureStories { get; set; }
        public List<object> featuredStories { get; set; }
        public string paid_plan { get; set; }
        public bool email_verified { get; set; }
        public FeaturesEnabled features_enabled { get; set; }
        public Agency agency { get; set; }
        public bool is_spam { get; set; }
        public bool canEdit { get; set; }
    }

    public class Link
    {
        public string thumbnail { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public string rest_api_url { get; set; }
        public string oembed_url { get; set; }
    }

    public class Image
    {
        public string src { get; set; }
        public string caption { get; set; }
    }

    public class Oembed
    {
        public string type { get; set; }
        public string html { get; set; }
        public string thumbnail_url { get; set; }
        public string title { get; set; }
        public string provider_url { get; set; }
        public string provider_name { get; set; }
        public string url { get; set; }
        public string author_url { get; set; }
        public string author_name { get; set; }
    }

    public class Video
    {
        public string thumbnail { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public string src { get; set; }
    }

    public class Data
    {
        public Link link { get; set; }
        public Image image { get; set; }
        public Oembed oembed { get; set; }
        public Video video { get; set; }
        public string text { get; set; }
    }

    public class Source
    {
        public string href { get; set; }
        public string name { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
    }

    public class Attribution
    {
        public string title { get; set; }
        public string href { get; set; }
        public string name { get; set; }
        public string thumbnail { get; set; }
        public string username { get; set; }
    }

    public class Stats3
    {
        public int likes { get; set; }
        public int comments { get; set; }
    }

    public class Element
    {
        public string id { get; set; }
        public string eid { get; set; }
        public string type { get; set; }
        public string permalink { get; set; }
        public string posted_at { get; set; }
        public string added_at { get; set; }
        public Data data { get; set; }
        public Source source { get; set; }
        public Attribution attribution { get; set; }
        public List<object> comments { get; set; }
        public Stats3 stats { get; set; }
        public string via { get; set; }
        public object meta { get; set; }
    }

    public class Content
    {
        public string sid { get; set; }
        public string title { get; set; }
        public string slug { get; set; }
        public string status { get; set; }
        public int version { get; set; }
        public string permalink { get; set; }
        public string shortlink { get; set; }
        public string description { get; set; }
        public string thumbnail { get; set; }
        public Date date { get; set; }
        public bool @private { get; set; }
        public bool not_indexed { get; set; }
        public bool is_spam { get; set; }
        public List<object> topics { get; set; }
        public List<object> siteposts { get; set; }
        public Meta meta { get; set; }
        public Stats stats { get; set; }
        public bool modified { get; set; }
        public bool deleted { get; set; }
        public bool canEdit { get; set; }
        public Author author { get; set; }
        public List<object> comments { get; set; }
        public int page { get; set; }
        public int per_page { get; set; }
        public List<Element> elements { get; set; }
    }

    public class RootObject
    {
        public Content content { get; set; }
        public int code { get; set; }
    }
}