﻿//using System;
//using System.Linq;
//using System.Net;
//using CMS.DataEngine;
//using CMS.EventLog;
//using CMS.Helpers;
//using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;
//using LyrisProxy;

//namespace CMSApp.CRC
//{
//    public class LyrisProxyWrapper
//    {
//        public enum Result
//        {
//            Subscribed,
//            Duplicate,
//            NotCreated,
//            Error
//        }

//        public Result SubmitToLyris(SubscriptionItem item)
//        {
//            var webServiceUrl = SettingsKeyInfoProvider.GetValue("LyrisServer");
//            var username = SettingsKeyInfoProvider.GetValue("LyrisUser");
//            var password = SettingsKeyInfoProvider.GetValue("LyrisPassword");

//            // Call the web service passing the name, email and list
//            var client = new lmapi
//            {
//                Url = webServiceUrl,
//                Credentials = new NetworkCredential(username, password)
//            };

//            if (IsDuplicateLyrisSubscription(client, item.Email, item.List))
//                return Result.Duplicate;

//            var memberId = client.CreateSingleMember(item.Email, string.IsNullOrWhiteSpace(item.FullName) ? item.Email : item.FullName, item.List);

//            if (memberId != 0)
//            {
//                try
//                {
//                    client.UpdateMemberStatus(new SimpleMemberStruct { EmailAddress = item.Email, ListName = item.List, MemberID = memberId }, MemberStatusEnum.needshello);
//                    return Result.Subscribed;
//                }
//                catch (Exception ex)
//                {
//                    EventLogProvider.LogException("Newsletter", "SubmitToLyris.UpdateMemberStatus", ex);
//                    return Result.Error;
//                }
//            }

//            return Result.NotCreated;
//        }

//        private bool IsDuplicateLyrisSubscription(lmapi client, string email, string list)
//        {
//            try
//            {
//                var userLists = client.EmailOnWhatLists(email) ?? new string[0];
//                return userLists.ToList().Contains(list, new StringEqualityComparer());
//            }
//            catch (Exception ex)
//            {
//                EventLogProvider.LogException("Newsletter", "IsDuplicateLyrisSubscription", ex);
//                return false;
//            }
//        }

//        public void SaveToCustomTable(SubscriptionItem item)
//        {
//            try
//            {
//                var itemToDelete = ValidationHelper.GetInteger(CustomTableHelper.GetItemValue("CRC.LyrisSubscriptions", string.Format("Email = '{0}' and List = '{1}'", item.Email, item.List), "ItemID"), 0);
//                if (itemToDelete > 0)
//                {
//                    CustomTableHelper.DeleteCustomTableItem("CRC.LyrisSubscriptions", itemToDelete);
//                }

//                CustomTableHelper.InsertCustomTableItem("CRC.LyrisSubscriptions", item);
//            }
//            catch (Exception exception)
//            {
//                EventLogProvider.LogException("LyrisController", "SaveToCustomTable", exception);
//            }
//        }
//    }
//}