module.exports = function (grunt) {

   grunt.initConfig({
      
      pkg: grunt.file.readJSON('package.json'),
      
      sass: {
         dist: {
            options: {
               style: 'expanded',
               lineNumbers: true
            },
            files: {
               'css/print.css': 'sass/print.scss',
               'css/web.css': 'sass/web.scss'
            }
         }
      },
      
      watch: {
         css: {
            files: [
            'sass/*.scss',
            'sass/base/*.scss',
            'sass/core/*.scss',
            'sass/foundation/*.scss',
            ],
            tasks: ['sass'],
            options: {
               spawn: false,
            }
         },
      }
   });
   
   grunt.loadNpmTasks('grunt-sass');
   grunt.loadNpmTasks('grunt-contrib-watch');
   
   grunt.registerTask('default', ['sass', 'watch']);    

};