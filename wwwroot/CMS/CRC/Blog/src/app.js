import $ from 'jQuery';
import Modernizr from 'Modernizr';
// red cross blog namespace
const RedCrossBlog = {};

if ($('body').hasClass('live-site')) {
  const lang = $('.language-toggle a').clone();
  $('.copyright .columns h6').prepend(lang);
}

if ($('#NewsletterFirstnameTextBox').length) {
  $('#NewsletterFirstnameTextBox').focus();
}

// ROS Banner Doesnt Overlap Script
RedCrossBlog.resizeCRCFooter = function () {
  $('#page-footer .row').css('padding-bottom', $('.ros-banner').height() + 12);
};

$(document).ready(() => {
  if ($('.ros-banner').length) {
    RedCrossBlog.resizeCRCFooter();
  }
});

/*
   *  Window.Resize
   */
let resizeTimeOut = null;
window.onresize = function () {
  if (resizeTimeOut !== null) clearTimeout(resizeTimeOut);
  resizeTimeOut = setTimeout(() => {
    if ($('.ros-banner').length) {
      RedCrossBlog.resizeCRCFooter();
    }
  }, 300);
};

/*
   *  Duplicate social media
   *  @since 2015
   */
if ($('body').hasClass('live-site')) {
  const socialMedia = $('#page-header .social-media-links').clone();
  $('#page-footer .row .copyright .columns').prepend(socialMedia);
}

/*
   *  Close promo banner
   *  @since 2015

   */
$('.closeBanner').on('click', function () {
  const path = $(this).data('path');
  const hidePromotionNDays = $(this).data('hidepromotionndays');
  $('.promo-banner').slideUp();
  $.post(`/api/promotionalbanner/post?path=${path}&hidePromotionNDays=${hidePromotionNDays}`);
});

/*
	 *  Create Mobile Menu
	 *  @since 2015

   */

RedCrossBlog.mobileNav = function () {
  let nav = $('#mobile-categories .mobile-category-target'),
    title = nav.data('title'),
    is_found = false,
    pathname = window.location.pathname;

  nav.html('');
  // Create the dropdown base
  $("<div class='select-wrap'><select class='hide-for-desktop'></select></div>").appendTo(nav);

  // Create default option "Go to..."
  // $("<option />", {"value" : 'default', "text" : '-- '+ title +' --'}).appendTo("select", nav);

  // Populate dropdown with menu items
  $('#right-rail .category-listing ul.links').each(function () {
    $('li', this).each(function () {
      let el = $('a', this),
        selected = '';
      if (el.attr('href') == pathname) {
        is_found = true;
        selected = 'selected="selected"';
      }
      const option = `<option value="${el.attr('href')}" ${selected}>${el.text()}</option>`;
      $('select', nav).append(option);
    });
  });

  // If selected value is null
  if (!is_found) {
    $('select option:nth-child(0)', nav).prop('selected', 'selected');
  }

  // On change event
  $('select', nav).change(function () {
    if ($(this).val() != 'default') {
      window.location = $(this).find('option:selected').val();
    }
  });
};

if ($('#right-rail .category-listing').length) RedCrossBlog.mobileNav();

/* Show / Mobile navigation */

if ($('.main-navigation .horizontal-list ul').length) {
  if ($('body').hasClass('live-site')) {
    const mainNav = $('.main-navigation .horizontal-list ul').clone();
    $('.flexpanel .navigation').append(mainNav);
    $('.mobile-blog-navigation ul li.menu a').on('click', () => {
      $('.flexpanel').addClass('open');
    });
    $('.flexpanel .close').on('click', () => {
      $('.flexpanel').removeClass('open');
    });
  }
}

/*
	 *  Placeholder fix for older browsers
   */
if (!Modernizr.input.placeholder) {
  $('[placeholder]')
    .focus(function () {
      const input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    })
    .blur(function () {
      const input = $(this);
      if (input.val() === '' || input.val() === input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    })
    .blur();
  $('[placeholder]').parents('form').submit(function () {
    $(this).find('[placeholder]').each(function () {
      const input = $(this);
      if (input.val() === input.attr('placeholder')) {
        input.val('');
      }
    });
  });
}

// SLIDER module
RedCrossBlog.SliderPagination = function ($sliderParent) {
  let paginationContainer,
    max,
    that = this;

  this.$sliderParent = $sliderParent;

  paginationContainer = $(
    `<div class="slider-pagination"><span class="previous"><div></div></span><span class="current">1</span> ${RedCrossBlog
      .main.uiCulture === 'FR'
      ? 'de'
      : 'of'} <span class="total-slides"></span><span class="next"><div></div></span></div>`,
  );
  $sliderParent.append(paginationContainer);
  $sliderParent.find('.previous').click(() => {
    that.onPrevious();
  });
  $sliderParent.find('.next').click(() => {
    that.onNext();
  });

  max = this.$sliderParent.find('ul.ui-tabs-nav > li').size();
  this.$sliderParent.find('.total-slides').html(max);
};

RedCrossBlog.SliderPagination.prototype.onNext = function (ev) {
  const currentIndex = this.$sliderParent.tabs('option', 'active');
  if (ev) ev.gesture.preventDefault();

  if (this.isLastSlide(currentIndex)) {
    this.setAsCurrent(0);
  } else {
    this.setAsCurrent(currentIndex + 1);
  }
  if (ev) ev.gesture.stopDetect();
};

RedCrossBlog.SliderPagination.prototype.onPrevious = function (ev) {
  const currentIndex = this.$sliderParent.tabs('option', 'active');
  if (ev) ev.gesture.preventDefault();

  if (currentIndex === 0) {
    this.setAsCurrent(this.$sliderParent.find('ul.ui-tabs-nav > li').size() - 1);
  } else {
    this.setAsCurrent(currentIndex - 1);
  }
  if (ev) ev.gesture.stopDetect();
};

RedCrossBlog.SliderPagination.prototype.isLastSlide = function (index) {
  return index == this.$sliderParent.find('ul.ui-tabs-nav > li').size() - 1;
};

RedCrossBlog.SliderPagination.prototype.setAsCurrent = function (index) {
  this.$sliderParent.tabs('option', 'active', index);
  this.updatePagination(index);
};

RedCrossBlog.SliderPagination.prototype.updatePagination = function (index) {
  this.$sliderParent.find('.current').html(index + 1);
};

RedCrossBlog.SliderPagination.prototype.onTabsactivate = function (event, ui) {
  this.updatePagination($(ui.newTab[0]).index());
};

// MAIN

RedCrossBlog.main = (function () {
  const self = this;
  const uiCulture = $('body').hasClass('FRCA') ? 'FR' : 'EN';

  const Utils = {
    getUrlVars(url) {
      let vars = [],
        hash;
      const hashes = url.slice(url.indexOf('?') + 1).split('&');
      for (let i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }

      return vars;
    },
  };

  const initializeDropDownList = function () {
    $('.drop-down-list').each(function () {
      const $elem = $(this);

      if ($elem.length > 0) {
        const $header = $elem.find('.header');
        const $body = $elem.find('.body');

        $body.hide();
        $header.hover(
          () => {
            $body.show();
          },
          () => {
            $body.hide();
          },
        );

        $body.hover(
          () => {
            $body.show();
          },
          () => {
            $body.hide();
          },
        );
      }
    });
  };

  const initializeCtaToggle = function (openByDefault) {
    $('.cta.toggle').each(function () {
      const $elem = $(this);
      if ($elem.length > 0) {
        const $header = $elem.find('.header');
        const $body = $elem.find('.body');
        let isOpen = openByDefault || false;

        const toggleState = function (isOpen) {
          $body.toggle(isOpen);

          if (isOpen) $elem.addClass('open');
          else $elem.removeClass('open');
        };

        toggleState(isOpen);

        $header.click(() => {
          isOpen = !isOpen;
          toggleState(isOpen);
        });
      }
    });
  };

  const initializeBlogArchives = function (itemCount) {
    $('.blog-archives').each(function () {
      let defaultItemsCount = itemCount || 6,
        itemCountFilter = `$top=${defaultItemsCount}`,
        showAll = false;

      let $this = $(this),
        $content = $this.find('.body > ul');

      function parseItems(data) {
        if (data.length > 0) {
          $content.empty();

          $.each(data, (i, elem) => {
            const li = createListItem(elem.Url, elem.MonthText);
            $content.append(li);
          });

          if (!showAll) {
            // create link trigger
            const triggerText = getDataTriggerText();
            const $trigger = $(`<li><a href="#">${triggerText}</a></li>`);

            $trigger.find('a').on('click', (event) => {
              event.preventDefault();

              showAll = !showAll;

              if (showAll) {
                Init();
              } else {
                Init(itemCountFilter);
              }
            });

            $content.append($trigger);
          }
        }
      }

      function createListItem(url, text, cssClass) {
        return cssClass == undefined
          ? `<li><a href="${url}">${text}</a></li>`
          : `<li class="${cssClass}"><a href="${url}">${text}</a></li>`;
      }

      function getDataTriggerText() {
        if (!showAll) return uiCulture === 'FR' ? 'Mois pr&eacute;c&eacute;dents' : 'OLDER';
      }

      function Init(filter) {
        let dataUrl = !filter
          ? '/api/blogmonth?$orderby=Year%20desc,%20Month%20desc'
          : `/api/blogmonth?$orderby=Year%20desc,%20Month%20desc&${filter}`;
        dataUrl = `${dataUrl}&lang=${uiCulture === 'FR' ? 'fr-CA' : 'en-CA'}`;

        $.ajax({ url: dataUrl }).done((data) => {
          parseItems(data);
        });
      }

      Init(itemCountFilter);
    });
  };

  const initializeAddThisWidget = function () {
    const addthis_share = {
      templates: {
        twitter:
          `{{title}} {{url}} (via ${uiCulture}` == 'FR'
            ? '@croixrouge_qc'
            : '@redcrosscanada' + ')',
      },
    };

    var addthis_config = addthis_config || {};
    addthis_config.ui_language = uiCulture;

    setTimeout(() => {
      if (addthis !== undefined) addthis.addEventListener('addthis.menu.share', shareEventHandler);
    }, 1000);
  };

  function shareEventHandler(evt) {
    if (evt.type == 'addthis.menu.share') {
      dataLayer.push({
        event: 'social-event',
        socialNetork: evt.data.service,
        socialAction: 'Share',
        socialTarget: evt.data.url,
      });
    }
  }

  const sliderManager = (function () {
    let currentSlider = null,
      initialization = function () {
        $('.slides').each(function () {
          $(this).tabs();
          const pagination = new RedCrossBlog.SliderPagination($(this));

          $(this).on('tabsactivate', (event, ui) => {
            pagination.onTabsactivate(event, ui);
          });
        });
      };

    return {
      init: initialization,
      current: currentSlider,
    };
  }());

  const initializeSliders = function () {};

  const initializeSVG = function () {
    $('img[src*=".svg"]').each(function () {
      const $img = $(this);
      const imgID = $img.attr('id');
      const imgClass = $img.attr('class');
      const imgURL = $img.attr('src');

      jQuery.get(
        imgURL,
        (data) => {
          // Get the SVG tag, ignore the rest
          let $svg = jQuery(data).find('svg');

          // Add replaced image's ID to the new SVG
          if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', `${imgClass} replaced-svg`);
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Replace image with new SVG
          $img.replaceWith($svg);
        },
        'xml',
      );
    });
  };

  const initializeGlobalComponents = function () {
    initializeDropDownList();
    initializeCtaToggle();
    initializeBlogArchives();
    initializeAddThisWidget();
    initializeSVG();
    sliderManager.init();
  };

  return {
    InitializeGlobalComponents: initializeGlobalComponents,
  };
}());

// script init
jQuery(($) => {
  RedCrossBlog.main.InitializeGlobalComponents();
});

function initQuiz(quizQuestions) {
  // variables
  const data = quizQuestions;
  let quiz = '';
  const total = data.length;
  let correctnumber = 0;

  // construct quiz ui content
  $.each(data, (index, element) => {
    const QuestionText = element.QuestionText;
    const ResponseRationale = element.ResponseRationale;
    const ResponseId = element.ResponseId;
    const ResponseList = element.ResponseList;
    quiz += `<div id='page-${index}' class='pages'>`;
    quiz += `<div class='question' id='q-${index}'>`;
    quiz += `<p>${index + 1}) ${QuestionText}</p>`;
    quiz += '</div>';
    quiz += `<div class='option' id='o-${index}'>`;
    jQuery.each(ResponseList, (index, value) => {
      if (value !== '') {
        quiz +=
          `<div><input type='radio' class='select' name='option' value='${value}' /><label for='` +
          `o-${index}'>${value}</label></div>`;
      }
    });
    quiz += '</div>';
    quiz += `<div class='explaination hide' id='e-${index}'>`;
    quiz += `<input type='hidden' name='a-${index}' value='${ResponseId}' class='answer' />`;
    quiz += `<hr />${ResponseRationale}`;
    quiz += '</div>';
    quiz +=
      `${"<br /><br /><a class='next hide button large red' href='#'>" +
        '<span>'}${quizNextText}</span>` + '</a>';
    quiz += '</div>';
  });
  $('#quiz #quiz-content').html(quiz);
  $('#quiz #intro .next').addClass('large red');
  $('#quiz #intro .next span').text(quizBeginButtonText);

  // Begin button action
  $('#quiz #intro .next').click(() => {
    // log: start quiz
    logStart(quizId);

    $('#quiz .pages:first').show();
    $('#intro').hide();
    $('#quiz-content').show();

    dataLayer.push({
      event: 'Quiz',
      eventCategory: '',
      eventAction: 'Next Intro Next',
      eventLabel: quizId,
    });
    return false;
  });
  // Quiz question Next button action
  $('#quiz .pages .next').each(function (index) {
    $(this).click(function () {
      $(this).parent().next().show();
      $(this).parent().remove();
      if ($('#quiz-content').html() === '') {
        $('#quiz-content').hide();
        // calculate result
        const percentage = `<strong>${Math.floor(correctnumber / total * 100)}%` + '</strong>';
        quizCompletedText = quizCompletedText
          .replace('%PERCENT%', percentage)
          .replace('%CORRECT%', correctnumber)
          .replace('%TOTAL%', total);
        $('#quiz #thankyou .result').html(quizCompletedText);

        // show last page
        $('#thankyou').show();

        // log: complete quiz
        logComplete(quizId);
      }

      dataLayer.push({
        event: 'Quiz',
        eventCategory: '',
        eventAction: 'Next Question Next',
        eventLabel: quizId,
      });
      return false;
    });
  });
  // Select action
  $('#quiz .pages .option .select').click(function () {
    const answer = $(this).parent().parent().parent().find('.explaination').find('.answer').val();
    const selected = $(this); // current selected object

    $(this).parents('.pages').find('.hide').removeClass('hide');
    $(this).parents('.pages').find('.select').prop('disabled', true);

    // highlight the answer is correct or not
    $(this).parents('.pages').find('.select').each(function (index) {
      if (answer == index + 1) {
        $(this).parent().addClass('correct');
      } else {
        selected.parent().addClass('incorrect');
      }
      if (answer == index + 1 && $(this).is(':checked')) {
        correctnumber += 1;
      }
    });
  });
  $('#quiz .option div').click(function (event) {
    if ($(this).find('input:radio').is(':enabled')) {
      $(this).find('input:radio').prop('checked', true).trigger('click');
    }
  });

  function logStart(id) {
    const url = `/api/QuizData/post?id=${id}`;
    $.getJSON(url, (data) => {});
  }

  function logComplete(id) {
    const url = `/api/QuizData/quizcomplete?id=${id}`;
    $.getJSON(url, (data) => {});
  }

  // $(".next").on("click", function () {
  //    // Scroll to the top when clicking next
  //    $('html, body').animate({
  //        scrollTop: $(".donate-bar").offset().top
  //    }, 100);

  // });
}
