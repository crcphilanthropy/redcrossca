﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSApp.CRC.modules.TagRemoval
{
    public class TagMapping
    {
        public string OriginalTagName { get; set; }
        public string NewTagName { get; set; }
    }
}