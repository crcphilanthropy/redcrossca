using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using CMS.IO;
using CMS.Taxonomy;

namespace CMSApp.CRC.modules.TagRemoval
{
    public class TagRemover
    {
        private List<string> _removedTagsList;
        public List<string> RemovedTagsList
        {
            get
            {
                return _removedTagsList ?? (_removedTagsList = new List<string>());
            }
        }


        private List<TagMapping> _mappedTagsList;
        public List<TagMapping> MappedTagsList
        {
            get
            {
                return _mappedTagsList ?? (_mappedTagsList = new List<TagMapping>());
            }
        }

        private List<string> _processedTags;
        public List<string> ProcessedTags
        {
            get
            {
                return _processedTags ?? (_processedTags = new List<string>());
            }
        }


        private GeneralConnection _dbConnection;
        private GeneralConnection DbConnection
        {
            get
            {
                return _dbConnection ?? (_dbConnection = ConnectionHelper.GetConnection());
            }
        }

        private int _tagGroupId = -1;

        public int TagGroupId
        {
            get
            {
                if (_tagGroupId != -1)
                {
                    return _tagGroupId;
                }

                return (_tagGroupId = TagGroupInfoProvider.GetTagGroupInfo(_tagGroupName, SiteContext.CurrentSiteID).TagGroupID);
            }
        }

        private readonly string _tagGroupName = "CRC.TagGroup.Blog.en-CA.content";
        private const string TagTable = "CMS_Tag";
        private const string DocumentTable = "CMS_Document";
        private const string DocumentTagTable = "CMS_DocumentTag";

        public TagRemover(string tagGroupName)
        {
            _tagGroupName = tagGroupName;
        }


        public void ProcessData(System.IO.StreamReader streamReader)
        {
            string lineBuffer;
            while ((lineBuffer = streamReader.ReadLine()) != null)
            {
                var columns = lineBuffer.Split(new[] { ',' });
                ProcessLine(columns[2], columns[3]);
            }
        }

        private void ProcessLine(string oldTagName, string newTagName)
        {

            if (IsTagNameEmpty(oldTagName) || IsTagNameEmpty(newTagName) || oldTagName.ToLower() == newTagName.ToLower())
            {
                return;
            }

            var oldTags = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\' AND TagGroupId = {1}", oldTagName.Replace("\'", "\'\'"), TagGroupId), "TagName");
            if (oldTags == null || !oldTags.Any())
            {
                return;
            }

            if (newTagName.ToLower().StartsWith("remove"))
            {
                ProcessTagRemoval(oldTagName, oldTags.ToList());
            }

            else
            {
                RemapTag(oldTagName, newTagName, oldTags);
            }

            foreach (var tag in oldTags)
            {
                TagInfoProvider.DeleteTagInfo(tag);
            }
        }

        private void RemoveTagFromDocumentTagsField(IList<TagInfo> oldTags)
        {
            var linkedDocuments = DbConnection.ExecuteQuery(String.Format("SELECT d.DocumentId, d.DocumentTags FROM {0} dt INNER JOIN {1} d ON dt.DocumentID = d.DocumentID WHERE dt.TagID IN ( {2} )"
                    , DocumentTagTable
                    , DocumentTable,
                    String.Join(",", oldTags.Select<TagInfo, string>(x => x.TagID.ToString()).ToArray())), new QueryDataParameters(), QueryTypeEnum.SQLQuery, false);


            //remove tag from document tags
            foreach (var row in linkedDocuments.Tables[0].AsEnumerable())
            {
                var namesOfOldTags = oldTags.Select(x => x.TagName.ToLower());
                var tagList = SplitTags(row["DocumentTags"].ToString());
                var filteredList = tagList.Where(x => !namesOfOldTags.Contains(String.Format("\"{0}\"", x.ToLower())) && !namesOfOldTags.Contains(x.ToLower()) && !namesOfOldTags.Contains(x.Trim(new[] { '\"'}).ToLower()));
                row["DocumentTags"] = String.Join(",", filteredList);
            }

            //update linked documents
            foreach (var document in linkedDocuments.Tables[0].AsEnumerable())
            {
                DbConnection.ExecuteNonQuery(String.Format("UPDATE {0} SET DocumentTags = @DocumentTags WHERE DocumentID = @DocumentId", DocumentTable), new QueryDataParameters()
                                                                                                                     {
                                                                                                                         new DataParameter("@DocumentTags",document["DocumentTags"].ToString()),
                                                                                                                         new DataParameter("@DocumentId",document["DocumentID"].ToString())
                                                                                                                     }, QueryTypeEnum.SQLQuery, false);
            }
        }

        private static List<string> SplitTags(string tagString, bool removeEmptyEntries = true)
        {
            var tagList = new List<string>();
            bool isQuoted = false;
            int previousWordStart = 0;
            for (var i = 0; i < tagString.Length; )
            {
                if (tagString[i] == '\"')
                {
                    isQuoted = !isQuoted;
                }

                else if (!isQuoted && (tagString[i] == ' ' || tagString[i] == ','))
                {
                    tagList.Add(tagString.Substring(previousWordStart, i - previousWordStart));
                    previousWordStart = i + 1;
                }

                i++;
            }

            if (previousWordStart < tagString.Length - 1)
            {
                tagList.Add(tagString.Substring(previousWordStart));
            }

            if (removeEmptyEntries)
            {
                tagList = tagList.Where(x => !String.IsNullOrWhiteSpace(x)).ToList();
            }

            return tagList;
        }

        //private void RemapTag(string oldTagName, string newTagName, InfoDataSet<TagInfo> oldTags)
        //{
        //    var newTag = GetTagToMap(newTagName);

        //    if (!oldTags.Any<TagInfo>(x => TagMappingAlreadyExists(x.TagID, newTag.TagID)))
        //    {
        //        //get linked documents
        //        var linkedDocuments = DbConnection.ExecuteQuery(String.Format("SELECT d.DocumentId, d.DocumentTags FROM {0} dt INNER JOIN {1} d ON dt.DocumentID = d.DocumentID WHERE dt.TagID IN ( {2} )"
        //            , DocumentTagTable
        //            , DocumentTable,
        //            String.Join(",", oldTags.Select<TagInfo, string>(x => x.TagID.ToString()).ToArray())), new QueryDataParameters(), QueryTypeEnum.SQLQuery, false);

        //        //string replace DocumentTags                
        //        foreach (var row in linkedDocuments.Tables[0].AsEnumerable())
        //        {
        //            var updatedTagList = new List<string>();

        //            foreach (var tag in SplitTags(row["DocumentTags"].ToString()))
        //            {
        //                foreach (var nameOfOldTag in oldTags.Select<TagInfo, string>(x => x.TagName))
        //                {
        //                    if (tag == nameOfOldTag || tag == String.Format("\"{0}\"", nameOfOldTag))
        //                    {
        //                        if(!TagMappingAlreadyExists(newTag.TagID,int.Parse(row["DocumentID"].ToString())))
        //                        {
        //                            updatedTagList.Add(String.Format("\"{0}\"", newTagName));

        //                        }
        //                    }

        //                    else
        //                    {
        //                        updatedTagList.Add(tag);
        //                    }
        //                }
        //            }
        //            row["DocumentTags"] = String.Join(",", updatedTagList);

        //            DbConnection.ExecuteNonQuery(String.Format("UPDATE {0} SET DocumentTags = @DocumentTags WHERE DocumentID = @DocumentId", DocumentTable), new QueryDataParameters()
        //                                                                                                             {
        //                                                                                                                 new DataParameter("@DocumentTags",row["DocumentTags"].ToString()),
        //                                                                                                                 new DataParameter("@DocumentId",row["DocumentID"].ToString())
        //                                                                                                             }, QueryTypeEnum.SQLQuery, false);

        //        }

        //        foreach (var row in linkedDocuments.Tables[0].AsEnumerable())
        //        {

        //            foreach(var oldTag in oldTags.AsEnumerable<TagInfo>())
        //            {

        //                if (!TagMappingAlreadyExists(newTag.TagID, int.Parse(row["DocumentID"].ToString())))
        //                {


        //                    newTag.TagCount += DbConnection.ExecuteNonQuery(String.Format("UPDATE {0} SET TagID = @NewTagId WHERE TagID = @OldTagId AND DocumentID = @DocumentId",
        //                    DocumentTagTable),
        //                    new QueryDataParameters()
        //                    {
        //                        new DataParameter("@NewTagId",newTag.TagID),
        //                        new DataParameter("@OldTagId",oldTag.TagID),
        //                        new DataParameter("@DocumentId",int.Parse(row["DocumentID"].ToString()))
        //                    }
        //                    , QueryTypeEnum.SQLQuery, false);
        //                }

        //                else
        //                {
        //                    DbConnection.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE TagID = @TagId AND DocumentID = @DocumentId", DocumentTagTable),
        //                        new QueryDataParameters()
        //                        {
        //                            new DataParameter("@TagId",oldTag.TagID),
        //                            new DataParameter("@DocumentId", row["DocumentID"].ToString())
        //                        }, QueryTypeEnum.SQLQuery,false);

        //                }
                    
        //            }
        //        }

        //        TagInfoProvider.SetTagInfo(newTag);
        //    }

        //    else
        //    {               
        //        DbConnection.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE TagID in ({1})", DocumentTagTable, String.Join(",", oldTags.Select<TagInfo, string>(x => x.TagID.ToString()))),
        //            new QueryDataParameters(), QueryTypeEnum.SQLQuery, false);
        //    }
        //    MappedTagsList.Add(new TagMapping()
        //    {
        //        OriginalTagName = oldTagName,
        //        NewTagName = newTagName,

        //    });
        //}



        private void RemapTag(string oldTagName, string newTagName, IEnumerable<TagInfo> oldTags)
        {
            var newTag = GetTagToMap(newTagName);

            foreach (var tag in oldTags)
            {

                var linkedDocuments = DbConnection.ExecuteQuery(String.Format("SELECT d.DocumentId, d.DocumentTags FROM {0} dt INNER JOIN {1} d ON dt.DocumentID = d.DocumentID WHERE dt.TagID = @TagId"
                                                                            , DocumentTagTable
                                                                            , DocumentTable)
                    , new QueryDataParameters() { new DataParameter("@TagId",tag.TagID)}, QueryTypeEnum.SQLQuery, false);


                foreach (var document in linkedDocuments.Tables[0].AsEnumerable())
                {
                    var updatedTagList = new List<string>();
                    foreach (var tagName in SplitTags(document["DocumentTags"].ToString()))
                    {
                        if (tagName.ToLower() == oldTagName.ToLower() || oldTagName.ToLower() == String.Format("\"{0}\"", oldTagName.ToLower()) || tagName.Trim(new[] { '\"' }).ToLower() == oldTagName.ToLower())
                        {
                            if (!TagMappingAlreadyExists(newTag.TagID, int.Parse(document["DocumentID"].ToString())))
                            {
                                updatedTagList.Add(String.Format("\"{0}\"", newTagName));
                            }
                        }

                        else
                        {
                            updatedTagList.Add(tagName);
                        }
                    }

                    DbConnection.ExecuteNonQuery(String.Format("UPDATE {0} SET DocumentTags = @DocumentTags WHERE DocumentID = @DocumentId", DocumentTable), new QueryDataParameters()
                                                                                                                     {
                                                                                                                         new DataParameter("@DocumentTags",String.Join(",", updatedTagList)),
                                                                                                                         new DataParameter("@DocumentId",document["DocumentID"].ToString())
                                                                                                                     }, QueryTypeEnum.SQLQuery, false);

                    if(!TagMappingAlreadyExists(newTag.TagID,int.Parse(document["DocumentID"].ToString())))
                    {                  
                        newTag.TagCount += DbConnection.ExecuteNonQuery(String.Format("UPDATE {0} SET TagID = @NewTagId WHERE TagID = @OldTagId AND DocumentID = @DocumentId",
                            DocumentTagTable),
                            new QueryDataParameters()
                            {
                                new DataParameter("@NewTagId",newTag.TagID),
                                new DataParameter("@OldTagId",tag.TagID),
                                new DataParameter("@DocumentId",int.Parse(document["DocumentID"].ToString()))
                            }
                            , QueryTypeEnum.SQLQuery, false);
                    }

                    else
                    {                   
                        DbConnection.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE TagID = @TagId AND DocumentID = @DocumentId", DocumentTagTable),
                                new QueryDataParameters()
                                {
                                    new DataParameter("@TagId",tag.TagID),
                                    new DataParameter("@DocumentId", document["DocumentID"].ToString())
                                }, QueryTypeEnum.SQLQuery, false);
                    }
                }

            }

            TagInfoProvider.SetTagInfo(newTag);

            MappedTagsList.Add(new TagMapping()
            {
                OriginalTagName = oldTagName,
                NewTagName = newTagName,

            });
        }



        private void ProcessTagRemoval(string oldTagName, IList<TagInfo> oldTags)
        {
            RemoveTagFromDocumentTagsField(oldTags);

            //delete tag outright, just remove mappings
            DbConnection.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE TagID IN ({1})", DocumentTagTable, String.Join(",", oldTags.Select(x => x.TagID.ToString()))), new QueryDataParameters(), QueryTypeEnum.SQLQuery, false);

            RemovedTagsList.Add(oldTagName);
        }

        private bool IsTagNameEmpty(string tagName)
        {
            return String.IsNullOrWhiteSpace(tagName);
        }

        private TagInfo GetTagToMap(string tagName)
        {
            //grab tag that will replace the old tag
            var newTag = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\' AND TagGroupID = {1}", tagName.Replace("\'", "\'\'"),TagGroupId), null).FirstOrDefault<TagInfo>();

            //if the tag doesn't exist, create one
            if (newTag == null)
            {
                TagInfoProvider.SetTagInfo(new TagInfo()
                {
                    TagName = tagName.Trim(),
                    TagCount = 0,
                    TagGroupID = TagGroupId
                });

                newTag = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\'", tagName.Replace("\'", "\'\'")), null).FirstOrDefault<TagInfo>();
            }
            return newTag;
        }

        private bool TagMappingAlreadyExists(int tagId, int documentId)
        {
            var query = "SELECT COUNT(*) FROM CMS_DocumentTag dt WHERE dt.TagID = @TagId AND dt.DocumentID = @DocumentId";

            var result = DbConnection.ExecuteScalar(query, new QueryDataParameters() 
                                                                { 
                                                                    new DataParameter("@TagId",tagId),
                                                                    new DataParameter("@DocumentId",documentId)
                                                                }, QueryTypeEnum.SQLQuery, false);
            return int.Parse(result.ToString()) > 0;
        }
    }  
}