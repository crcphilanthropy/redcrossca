using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CMSApp.CRC.modules.TagRemoval;
using CMS.Membership;




public partial class CMSModules_TagRemoval_Default : CMSToolsPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MembershipContext.AuthenticatedUser.IsAuthorizedPerResource("cms.TagRemoval", "read"))
        {
            RedirectToAccessDenied("cms.TagRemoval", "Read");
        }

        ResultLabel.Text = string.Empty;
        litError.Text = string.Empty;
        phError.Visible = false;
        phSuccess.Visible = false;
    }

    protected void UploadButton_Click(object sender, EventArgs e)
    {
        var tagGroupName = Request.Form[ddlTagGroupName.UniqueID];
        if (string.IsNullOrWhiteSpace(tagGroupName))
        {
            litError.Text = "Please select a Language";
            phError.Visible = true;
        }
        else
        {
            if (UploadFile(tagGroupName))
                phSuccess.Visible = true;
        }
    }

    private bool UploadFile(string tagGroupName)
    {

        try
        {
            if (FileUploadControl.HasFile)
            {
                var filename = String.Format("{0}.csv", System.IO.Path.GetTempFileName());
                FileUploadControl.SaveAs(filename);

                ProcessFile(filename, tagGroupName);
                File.Delete(filename);
            }
            else
            {
                throw new Exception("Please upload a csv file");
            }
        }

        catch (Exception ex)
        {
            litError.Text = ex.Message;
            phError.Visible = true;
            return false;
        }


        return true;
    }

    private void ProcessFile(string filename, string tagGroupName)
    {

        using (var csvFile = new StreamReader(filename, Encoding.GetEncoding("Windows-1252")))
        {
            var tagRemover = new TagRemover(tagGroupName);
            tagRemover.ProcessData(csvFile);
            ResultLabel.Text = ProcessResults(tagRemover.RemovedTagsList, tagRemover.MappedTagsList);
        }


    }

    private string ProcessResults(List<string> removedTagsList, List<TagMapping> mappedTagsList)
    {
        removedTagsList.Sort();
        mappedTagsList.Sort((x, y) => x.OriginalTagName.CompareTo(y.OriginalTagName));

        var stringBuilder = new StringBuilder();

        if (removedTagsList.Count > 0)
        {
            stringBuilder.Append("<h2>Removed Tags</h2>");

            foreach (var removedTag in removedTagsList)
            {
                stringBuilder.AppendFormat("<p>{0}</p>", removedTag);
            }
        }

        if (mappedTagsList.Count > 0)
        {
            stringBuilder.Append("<h2>Mapped Tags</h2>");

            foreach (var mappedTag in mappedTagsList)
            {
                stringBuilder.AppendFormat("<p>{0} -> {1}</p>", mappedTag.OriginalTagName, mappedTag.NewTagName);
            }
        }

        return stringBuilder.ToString();
    }

}
