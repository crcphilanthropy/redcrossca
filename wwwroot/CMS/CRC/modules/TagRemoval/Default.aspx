﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CMSModules_TagRemoval_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tag Removal</title>

    <link href="~/CRC/css/TagRemoval.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />
    <style>
        .bs-callout
        {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

        .bs-callout-info
        {
            border-left-color: #1b809e;
        }

        .border-left
        {
            border-left: 1px solid #eee;
        }
    </style>
</head>


<body>
    <div class="container">
        <div class="page-header">
            <h1>Tag Removal</h1>
        </div>
        <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
            <h4>Instructions</h4>
            <p>This module allows you to remove or replace tags.  Upload an Comma-separated values (.csv) file with a list of tags and the action to take for each tag, then click the "Remove Tags" button. </p>
        </div>
        <asp:PlaceHolder runat="server" ID="phError" Visible="True">
            <div class="row">
                <div class="col-mid-12">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong>
                        <asp:Literal runat="server" ID="litError"></asp:Literal>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phSuccess" Visible="True">
            <div class="row success-msg">
                <div class="col-mid-12">
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong>
                    </div>
                    <asp:Label ID="ResultLabel" runat="server"></asp:Label>
                </div>
            </div>
        </asp:PlaceHolder>
        <form runat="server">
            <div class="row row-filter">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="<%= ddlTagGroupName.ClientID %>">Language</label>
                        <asp:DropDownList runat="server" ID="ddlTagGroupName" CssClass="form-control">
                            <asp:ListItem Text="Select Language..." Value=""></asp:ListItem>
                            <asp:ListItem Text="English" Value="CRC.TagGroup.Blog.en-CA.content"></asp:ListItem>
                            <asp:ListItem Text="French" Value="CRC.TagGroup.Blog.fr-CA.content"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="<%= FileUploadControl.ClientID %>">File</label>
                        <asp:FileUpload ID="FileUploadControl" runat="server" CssClass="form-control" />
                    </div>
                    <div>
                        <asp:Button ID="UploadButton" runat="server" Text="Remove Tags" OnClick="UploadButton_Click" CssClass="btn btn-primary" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
