﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Membership;
using CMS.UIControls;

namespace CMSApp.CRC.modules.AssignOwner
{
    public partial class Default : CMSToolsPage
    {
        private readonly DocumentQuery _blogQuery = DocumentHelper
            .GetDocuments("CMS.BlogPost")
            .OnCurrentSite()
            .WhereNull("NodeOwner");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;

            var blogPostCount = _blogQuery.Count;

            //Set current count text
            lblCurrentCount.Text = ValidationHelper.GetInteger(blogPostCount, 0).ToString();

            //set default dropdown selection
            ddlUsers.Items.Insert(0, new ListItem("-- Select One --", string.Empty));

            //Disable population of dropdown as well as disable update button if blog post count less than or equal to 0
            if (blogPostCount <= 0)
            {
                ddlUsers.Enabled = false;
                btnUpdate.Enabled = false;

                return;
            }

            //Get List of user
            var userList = UserInfoProvider.GetUsers()
                .Columns("FullName", "UserId")
                .OrderBy(OrderDirection.Ascending, "FullName")
                .ToList();


            //Populate list of user
            foreach (var userInfo in userList)
                ddlUsers.Items.Add(new ListItem(userInfo.FullName + " (" + userInfo.UserID + ")",
                    ValidationHelper.GetString(userInfo.UserID, string.Empty)));
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            var selectedUser = ddlUsers.SelectedValue;

            if (string.IsNullOrEmpty(selectedUser))
                return;

            foreach (var node in _blogQuery)
            {
                try
                {
                    var isPublished = node.IsPublished;
                    //Update Node Owner
                    node.SetValue("NodeOwner", selectedUser);
                    node.Update();
                    if (isPublished)
                        node.Publish("Update Node Owner to " + selectedUser);
                }
                catch (Exception)
                {
                    lblMessage.Text = "An error occured.";
                    lblMessage.ForeColor = Color.Red;

                    return;
                }
            }

            lblMessage.Text = "All blog post(s) have been updated.";
            lblMessage.ForeColor = Color.Green;

            ddlUsers.SelectedIndex = 0;
            ddlUsers.Enabled = false;

            btnUpdate.Enabled = false;

            lblCurrentCount.Text = "0";
        }
    }
}