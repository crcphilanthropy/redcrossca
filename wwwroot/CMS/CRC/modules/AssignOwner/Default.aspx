﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CMSApp.CRC.modules.AssignOwner.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign Owner</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container theme-showcase" role="main" data-ng-cloak="">

            <div class="page-header">
                <h1>CRC - Assign Owner Tool</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        There are currently
                        <asp:Label runat="server" ID="lblCurrentCount" Style="font-weight: 700;" />
                        blog post(s) that do not have an owner assignment.
                    </p>
                    <p>
                        Please select a user below to update the blog post.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="<%= ddlUsers.ClientID %>">Select User</label>
                        <asp:DropDownList runat="server" ID="ddlUsers" CssClass="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Button runat="server" ID="btnUpdate" CssClass="btn btn-default" Text="Update" OnClientClick="return confirm('Are you sure you want to make updates to the above blog posts? Changes are not reversible')" OnClick="btnUpdate_OnClick" />
                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
