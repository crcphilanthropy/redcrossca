﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IYCImportModule.aspx.cs" Inherits="CMSApp.CRC.modules.IYCImportTool.IYCImportModule" %>

<%@ Import Namespace="CMSAppAppCode.Old_App_Code.CRC.Modules" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <style>
        .bs-callout
        {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

        .bs-callout-info
        {
            border-left-color: #1b809e;
        }

        .border-left
        {
            border-left: 1px solid #eee;
        }
    </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript">

        var interval;
        var isComplete = false;
        
        function showIycLogs() {
            
            $('.row-filter').hide();
            $('.row-progress').removeClass('hide');
            $('.row-progress').slideDown("fast");
            interval = setInterval(function () {

                $.get('/api/iyc/getiyclogitems?t=' + new Date().valueOf(), function (data) {
                    var logs = $.parseJSON(data);
                    if (logs == undefined)
                        return;
                    
                    if (logs.length > 0) $('.tbody-logs').html('');
                    var warningCounter = 0;
                    
                    $.each(logs, function (i, item) {
                        if (item.SaveAction == -2) {
                            clearInterval(interval);
                            $('.danger-msg').removeClass('hide');
                            $('.danger-msg').show();
                        } else if (item.SaveAction == 3) {
                            clearInterval(interval);
                            showIycStats();
                        } else {

                            if (item.SaveAction == -1) {
                                warningCounter++;
                                $('.warning-msg').removeClass('hide');
                                $('.warning-counter').html(warningCounter);
                                $('.warning-msg').show();
                            }

                            var $tr = $(item.SaveAction == -1 ? '<tr class="warning">' : '<tr>').append(
                                $('<td>').html('<a href="' + item.Path + '" target="_blank">' + item.Name + "</a>"),
                                $('<td>').html(item.SaveAction == 1 ? "Inserted" : item.SaveAction == 2 ? "Updated" : item.SaveAction == -1 ? "Error" : ""),
                                $('<td>').html(item.Note)
                            );
                            $('.tbody-logs').prepend($tr);
                        }
                    });
                }).fail(function () {
                    isComplete = true;
                    clearInterval(interval);
                    $('.danger-msg').removeClass('hide');
                    $('.danger-msg').show();
                });
            }, 2000);
        }

        function showIycStats() {
            $('.row-stats').removeClass('hide');
            $('.row-stats').slideDown("fast");
            $.get('/api/iyc/GetIYCImportStats?t=' + new Date().valueOf(), function (data) {
                var stats = $.parseJSON(data);
                $('.tbody-stats').html('');

                $.each(stats, function (i, item) {
                    var $tr = $('<tr>').append(
                        $('<td>').html(item.Province),
                        $('<td class="border-left">').html(item.Branches.Total),
                        $('<td>').html(item.Branches.Inserted),
                        $('<td>').html(item.Branches.Updated),
                        $('<td class="border-left">').html(item.Categories.Total),
                        $('<td>').html(item.Categories.Inserted),
                        $('<td>').html(item.Categories.Updated),
                        $('<td class="border-left">').html(item.SubCategories.Total),
                        $('<td>').html(item.SubCategories.Inserted),
                        $('<td>').html(item.SubCategories.Updated),
                        $('<td class="border-left">').html(item.Services.Total),
                        $('<td>').html(item.Services.Inserted),
                        $('<td>').html(item.Services.Updated)
                    );
                    $('.tbody-stats').append($tr);
                });
                $('.success-msg').removeClass('hide');
                $('.success-msg').show();
                isComplete = true;
            });
        }

        $(document).ready(function() {
            $('.show-report').click(function() {
                showIycLogs();
                return false;
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container theme-showcase" role="main" data-ng-cloak="">
            <div class="page-header">
                <h1>CRC - In Your Community Import Tool</h1>
            </div>
            <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
                <h4>Instructions</h4>
                <ol>
                    <li>Select excel file with import data. <a href="/CRC/modules/IYCImportTool/Branch Details - Compilation Final.xlsx">Example file format</a>
                        <ul class="text-danger">
                            <li>For services without Sub-Categories on the services Sheet, the <strong>SubCategoryRowNumber</strong> should  be set to 0 or they will be ignore in the import.</li>
                        </ul>
                    </li>
                    <li>Click Upload and Run. This will take a few minutes</li>
                    <li>Stats will be displayed at the end of how many documents was inserted or updated per province</li>
                    <li>The import will continue if you navigate away from the page before it is finished.</li>
                </ol>
            </div>
            <asp:PlaceHolder runat="server" ID="phError" Visible="False">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            <strong>Error!</strong>
                            <asp:Literal runat="server" ID="litError"></asp:Literal>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="row hide success-msg">
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <strong>Completed!</strong> <a href="/CRC/modules/IYCImportTool/IYCImportModule.aspx">Refresh</a>
                    </div>
                </div>
            </div>
            <div class="row hide danger-msg">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> processing of the file was aborted due to an error. <a href="/CRC/modules/IYCImportTool/IYCImportModule.aspx">Refresh</a>
                    </div>
                </div>
            </div>
            <div class="row hide warning-msg">
                <div class="col-md-12">
                    <div class="alert alert-warning" role="alert">
                        <strong>Warning!</strong> <span class="warning-counter">0</span> Error(s) occurred during file processing
                    </div>
                </div>
            </div>
            <div class="row row-filter">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="<%= ddlLanguage.ClientID %>">Language</label>
                        <asp:DropDownList runat="server" ID="ddlLanguage" CssClass="form-control">
                            <asp:ListItem Text="Please select a Language..." Value=""></asp:ListItem>
                            <asp:ListItem Text="English" Value="en-CA"></asp:ListItem>
                            <asp:ListItem Text="French" Value="fr-CA"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="<%= fuIYCImportFile.ClientID %>">File</label>
                        <asp:FileUpload runat="server" ID="fuIYCImportFile" CssClass="form-control" />
                    </div>
                    <asp:Button runat="server" ID="btnImport" Text="Upload and Run" CssClass="btn btn-primary" />&nbsp;
                    <asp:Button runat="server" ID="btnShowReport" Text="Show last/current report" CssClass="btn btn-default show-report" />
                </div>
            </div>
            <div class="row row-stats hide">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Province</th>
                                    <th colspan="3" class="border-left">Branches</th>
                                    <th colspan="3" class="border-left">Categories</th>
                                    <th colspan="3" class="border-left">Sub-Categories</th>
                                    <th colspan="3" class="border-left">Services</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th class="border-left">Total</th>
                                    <th>Inserted</th>
                                    <th>Updated</th>

                                    <th class="border-left">T</th>
                                    <th>I</th>
                                    <th>U</th>

                                    <th class="border-left">T</th>
                                    <th>I</th>
                                    <th>U</th>

                                    <th class="border-left">T</th>
                                    <th>I</th>
                                    <th>U</th>
                                </tr>
                            </thead>
                            <tbody class="tbody-stats">
                                <asp:Repeater runat="server" ID="rptStats">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("Province") %></td>
                                            <td class="border-left"><%# Eval("Branches.Total") %></td>
                                            <td><%# Eval("Branches.Inserted") %></td>
                                            <td><%# Eval("Branches.Updated") %></td>

                                            <td class="border-left"><%# Eval("Categories.Total") %></td>
                                            <td><%# Eval("Categories.Inserted") %></td>
                                            <td><%# Eval("Categories.Updated") %></td>

                                            <td class="border-left"><%# Eval("SubCategories.Total") %></td>
                                            <td><%# Eval("SubCategories.Inserted") %></td>
                                            <td><%# Eval("SubCategories.Updated") %></td>

                                            <td class="border-left"><%# Eval("Services.Total") %></td>
                                            <td><%# Eval("Services.Inserted") %></td>
                                            <td><%# Eval("Services.Updated") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row row-progress hide">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Document
                                    </th>
                                    <th>Action
                                    </th>
                                    <th>Info
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="tbody-logs">
                                <asp:Repeater runat="server" ID="rptLogItems">
                                    <ItemTemplate>
                                        <tr class="<%# ((IYCImportModule.SaveAction)Eval("SaveAction")) == IYCImportModule.SaveAction.Error ?  "danger" : "" %>">
                                            <td><a href="<%# Eval("Path") %>" target="_blank"><%# Eval("Name") %></a></td>
                                            <td><%# Eval("SaveAction") %></td>
                                            <td><%# Eval("Note") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>
