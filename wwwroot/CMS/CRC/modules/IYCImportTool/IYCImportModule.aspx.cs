﻿using System;
using System.Collections.Generic;
using System.Threading;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.UIControls;

namespace CMSApp.CRC.modules.IYCImportTool
{
    public partial class IYCImportModule : CMSToolsPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnImport.Click += BtnImportClick;
        }

        void BtnImportClick(object sender, EventArgs e)
        {
            try
            {
                phError.Visible = false;

                CustomTableHelper.DeleteCustomTableItems("CRC.SerializedObjects", new List<string> { "IYCLogItems" }, "Name");

                if (!fuIYCImportFile.HasFile || string.IsNullOrWhiteSpace(ddlLanguage.SelectedValue))
                {
                    litError.Text = "Please select a language and upload a file.";
                    phError.Visible = true;
                    return;
                }

                const string folder = "/CRC/files/IYCImportFile";
                if (!System.IO.Directory.Exists(Server.MapPath(folder)))
                    System.IO.Directory.CreateDirectory(Server.MapPath(folder));

                var filePath = Server.MapPath(string.Format("{0}/{1}", folder, fuIYCImportFile.FileName));
                fuIYCImportFile.SaveAs(filePath);

                var processor = new CMSAppAppCode.Old_App_Code.CRC.Modules.IYCImportModule(filePath, ddlLanguage.SelectedValue, SiteContext.CurrentSiteName);

                var isValid = processor.IsValidFile();
                if (!isValid)
                {
                    phError.Visible = true;
                    litError.Text = "Invalid File (Missing worksheet and/or columns). See example file in step 1";
                }

                if (isValid)
                {
                    var t = new Thread(Process);
                    t.Start(processor);
                    TransformationHelper.RegisterStartUpScript("showIycLogs();", Page);
                }
               
            }
            catch (Exception ex)
            {
                phError.Visible = true;
                litError.Text = ex.Message;
                EventLogProvider.LogException("IYCImportModule", "BtnImportClick", ex);
            }
        }


        private static void Process(object p)
        {
            var processor = p as CMSAppAppCode.Old_App_Code.CRC.Modules.IYCImportModule;
            if (processor == null) return;

            processor.Run();
        }
    }
}