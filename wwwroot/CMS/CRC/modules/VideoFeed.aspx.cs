﻿using System;
using System.Web;

namespace CMSApp.CRC.modules
{
    public partial class VideoFeed : CMS.UIControls.CMSPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "text/xml";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
            HttpContext.Current.Response.AddHeader("Content-Disposition:", "attachment;filename=" + HttpUtility.UrlEncode(string.Format("video-{0}.xml", Request.QueryString["id"])));
            HttpContext.Current.Response.Write(CustomTableHelper.GetItemValue("CRC.NewsroomVideo", string.Format("ItemGUID = '{0}'", Request.QueryString["id"]), "Rss") as string ?? "File not found");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.Close();
            HttpContext.Current.Response.End();
        }
    }
}