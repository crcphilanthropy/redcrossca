/****** Object:  StoredProcedure [dbo].[crc_GetDocumentMappings]    Script Date: 05/30/2013 14:31:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crc_GetDocumentMappings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[crc_GetDocumentMappings]
GO


/****** Object:  StoredProcedure [dbo].[crc_GetDocumentMappings]    Script Date: 05/30/2013 14:31:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[crc_GetDocumentMappings]
(
	@documentId INT = NULL
)

AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	
	-- Get the records into a temp table so that they are ordered correctly
	SELECT * INTO #TempTable
	FROM 
		[dbo].[CRC_UrlMapping] AS u
	WHERE
		[u].[TargetId] = @documentId
	ORDER BY
		[u].[ItemID] ASC
	
	-- Create a comma separated list from the records
	DECLARE @listStr VARCHAR(MAX)

	SET @listStr = ''

	SELECT 
		@listStr = CAST([t].[SourceId] AS VARCHAR(20)) + ',' + @listStr
	FROM 
		#TempTable AS t
		
	SELECT @listStr AS returnVal
GO




/****** Object:  StoredProcedure [dbo].[crc_GetDocumentUsingAlias]    Script Date: 05/30/2013 14:31:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crc_GetDocumentUsingAlias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[crc_GetDocumentUsingAlias]
GO


/****** Object:  StoredProcedure [dbo].[crc_GetDocumentUsingAlias]    Script Date: 05/30/2013 14:31:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[crc_GetDocumentUsingAlias]
(
	@aliasUrl NVARCHAR(MAX) = NULL,
	@culture NVARCHAR(5) = NULL
)

AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	
	SELECT TOP 1 
		[d].[DocumentID]
	FROM 
		[dbo].[CMS_DocumentAlias] AS a
		INNER JOIN [dbo].[CMS_Tree] AS t
			ON [a].[AliasNodeID] = [t].[NodeID]
		INNER JOIN [dbo].[CMS_Document] AS d
			ON [t].[NodeID] = [d].[DocumentNodeID]
	WHERE 
		[a].[AliasURLPath] = @aliasUrl AND [d].[DocumentCulture] = @culture
GO



/****** Object:  StoredProcedure [dbo].[crc_GetUrlMapping]    Script Date: 05/30/2013 14:31:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crc_GetUrlMapping]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[crc_GetUrlMapping]
GO



/****** Object:  StoredProcedure [dbo].[crc_GetUrlMapping]    Script Date: 05/30/2013 14:31:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[crc_GetUrlMapping]
(
	@sourceId INT,
	@culture NVARCHAR(5)
)

AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	
	SELECT
		[u].[SourceId],
		[t].[NodeAliasPath],
		[d].[DocumentUrlPath],
		[u].[TargetUrl],
		[u].[LangCode]
	FROM
		[dbo].[CRC_UrlMapping] AS u
		LEFT JOIN [dbo].[CMS_Document] AS d
			ON [u].[TargetId] = [d].[DocumentID]
		LEFT JOIN  [dbo].[CMS_Tree] AS t
			ON [d].[DocumentNodeID] = [t].[NodeID]
	WHERE
		[u].[SourceId] = @sourceId AND [u].[LangCode] = @culture
GO




/****** Object:  StoredProcedure [dbo].[crc_UpdateDocumentMappings]    Script Date: 05/30/2013 14:32:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crc_UpdateDocumentMappings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[crc_UpdateDocumentMappings]
GO


/****** Object:  StoredProcedure [dbo].[crc_UpdateDocumentMappings]    Script Date: 05/30/2013 14:32:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[crc_UpdateDocumentMappings]
(
	@sourceIds NVARCHAR(MAX),
	@targetId INT = NULL,
	@targetUrl NVARCHAR(MAX),
	@language NVARCHAR(5)
)

AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	
	-- First delete all of the records associated with the target id
	DELETE FROM [dbo].[CRC_UrlMapping] WHERE TargetId = @targetId
	
	-- Now add the new source id record(s)
	INSERT INTO [dbo].[CRC_UrlMapping] ([ItemCreatedWhen], [ItemGUID], [SourceId], [TargetId], [TargetUrl], [LangCode])
	SELECT GETDATE(), NEWID(), [f].[item], @targetId, @targetUrl, @language FROM [dbo].[crc_fnSplit](@sourceIds, ',') AS f
GO




/****** Object:  UserDefinedFunction [dbo].[crc_fnSplit]    Script Date: 05/30/2013 14:32:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crc_fnSplit]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[crc_fnSplit]
GO



/****** Object:  UserDefinedFunction [dbo].[crc_fnSplit]    Script Date: 05/30/2013 14:32:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[crc_fnSplit](
    @sInputList VARCHAR(8000) -- List of delimited items
  , @sDelimiter VARCHAR(8000) = ',' -- delimiter that separates items
) RETURNS @List TABLE (item VARCHAR(8000))

BEGIN
DECLARE @sItem VARCHAR(8000)
WHILE CHARINDEX(@sDelimiter,@sInputList,0) <> 0
 BEGIN
 SELECT
  @sItem=RTRIM(LTRIM(SUBSTRING(@sInputList,1,CHARINDEX(@sDelimiter,@sInputList,0)-1))),
  @sInputList=RTRIM(LTRIM(SUBSTRING(@sInputList,CHARINDEX(@sDelimiter,@sInputList,0)+LEN(@sDelimiter),LEN(@sInputList))))

 IF LEN(@sItem) > 0
  INSERT INTO @List SELECT @sItem
 END

IF LEN(@sInputList) > 0
 INSERT INTO @List SELECT @sInputList -- Put the last item in
RETURN
END


GO