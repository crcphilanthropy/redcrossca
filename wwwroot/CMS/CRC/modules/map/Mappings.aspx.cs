using System;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.Helpers;

namespace CMSApp.CRC.modules.map
{
    public partial class Mappings : CMSPropertiesPage
    {
        #region "Page events"

        /// <summary>
        /// Raises the <see cref="E:Init" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set edited document
            editMappings.TreeNode = Node;

            var edited = QueryHelper.GetBoolean("edited", false);
            if (edited)
            {
                ShowConfirmation("Source Id mappings saved");
            }
        }

        #endregion
    }
}