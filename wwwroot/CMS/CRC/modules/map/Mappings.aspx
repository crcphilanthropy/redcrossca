﻿<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true" Theme="Default" CodeBehind="Mappings.aspx.cs" Inherits="CMSApp.CRC.modules.map.Mappings" %>

<%@ Register Src="controls/editMappings.ascx" TagName="EditMappings" TagPrefix="eca" %>

<asp:Content runat="server" ID="pnlContent" ContentPlaceHolderID="plcContent">
    <eca:EditMappings ID="editMappings" runat="server" IsLiveSite="false" />
</asp:Content>