﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editMappings.ascx.cs" Inherits="CMSApp.CRC.modules.map.controls.editMappings" %>

<asp:Panel runat="server" ID="pnlContainer">
    <cms:MessagesPlaceholder ID="plcMess" runat="server" />
    
    <b>Source Ids linked to this document:</b><br/><br/>
    <asp:TextBox ID="txtIds" runat="server" Width="300px" ValidationGroup="Mappings"></asp:TextBox><br/>
    <asp:RegularExpressionValidator 
        ID="txtIdstxtIds" 
        runat="server"
        ControlToValidate="txtIds" 
        ValidationExpression="^[\d,]*$"
        CssClass="red" 
        Display="Dynamic"
        EnableClientScript="true"
        ValidationGroup="Mappings"
        ErrorMessage="You may only enter integers and commas e.g. 1234,4321,4567. No spaces.">
    </asp:RegularExpressionValidator>

    <table width="100%">
        <tr>
            <td align="right">
                <cms:FormSubmitButton ID="btnOk" runat="server" OnClick="btnOk_Click" ValidationGroup="Mappings" />
            </td>
        </tr>
    </table>

</asp:Panel>