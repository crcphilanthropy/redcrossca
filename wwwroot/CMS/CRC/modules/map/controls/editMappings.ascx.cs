using System;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.Helpers;
using CMS.Localization;

namespace CMSApp.CRC.modules.map.controls
{
    public partial class editMappings : CMSUserControl
    {
        #region "Protected variables"

        protected int currentNodeId = 0;
        protected TreeNode node = null;
        protected string mRedirectAfterSaveUrl = "~/CRC/modules/map/Mappings.aspx";

        /// <summary>
        /// Gets or sets the old ids.
        /// </summary>
        /// <value>
        /// The old ids.
        /// </value>
        protected string OldIds
        {
            get
            {
                return ViewState["OLD_IDS"] != null ? ViewState["OLD_IDS"].ToString() : string.Empty;
            }
            set
            {
                ViewState["OLD_IDS"] = value;
            }
        }

        #endregion

        #region "Private properties"

        /// <summary>
        /// Messages placeholder
        /// </summary>
        public override MessagesPlaceHolder MessagesPlaceHolder
        {
            get
            {
                return plcMess;
            }
        }

        #endregion

        #region "Public properties"

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        public TreeNode TreeNode
        {
            get;
            set;
        }

        /// <summary>
        /// Url to redirect after succesful save.
        /// </summary>
        public string RedirectAfterSaveUrl
        {
            get
            {
                return mRedirectAfterSaveUrl;
            }
            set
            {
                mRedirectAfterSaveUrl = value;
            }
        }

        #endregion

        #region "Methods"

        /// <summary>
        /// Init event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Disable document manager events
            DocumentManager.RegisterEvents = false;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (TreeNode != null)
            {
                currentNodeId = TreeNode.NodeID;

                if (!IsPostBack)
                {
                    // Get the list of ids for this document
                    var parameters = new QueryDataParameters
                    {
                        new DataParameter("documentId", TreeNode.DocumentID)
                    };

                    using (var gn = ConnectionHelper.GetConnection())
                    {
                        var dataSet = gn.ExecuteQuery("crc_GetDocumentMappings", parameters, QueryTypeEnum.StoredProcedure, false);

                        if (!DataHelper.DataSourceIsEmpty(dataSet))
                        {
                            var ids = (string) dataSet.Tables[0].Rows[0]["returnVal"];

                            if (!string.IsNullOrEmpty(ids))
                                ids = ids.Trim(new[] { ',' });

                            txtIds.Text = OldIds = ids;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            // Make sure the list of ids have changed
            if (!string.Equals(txtIds.Text.Trim(), OldIds))
            {
                SaveMappings();
            }

            if (!string.IsNullOrEmpty(RedirectAfterSaveUrl))
            {
                var url = URLHelper.AddParameterToUrl(RedirectAfterSaveUrl, "nodeid", currentNodeId.ToString());
                url = URLHelper.AddParameterToUrl(url, "edited", "1");

                // Redirect
                URLHelper.Redirect(url);
            }
        }

        /// <summary>
        /// Saves the mappings.
        /// </summary>
        /// <returns></returns>
        public void SaveMappings()
        {
            using (var gn = ConnectionHelper.GetConnection())
            {
                var parameters = new QueryDataParameters
                    {
                        new DataParameter("sourceIds", txtIds.Text.Trim(new[] {','})),
                        new DataParameter("targetId", TreeNode.DocumentID),
                        new DataParameter("targetUrl", TreeNode.IsLink
                                              ? DocumentURLProvider.GetUrl(TreeNode.NodeAliasPath, null).Trim(new[] {'~'})
                                              : DocumentURLProvider.GetUrl(TreeNode.NodeAliasPath, TreeNode.DocumentUrlPath).Trim(new[] {'~'})),
                        new DataParameter("language", LocalizationContext.PreferredCultureCode)
                    };

                gn.ExecuteNonQuery("crc_UpdateDocumentMappings", parameters, QueryTypeEnum.StoredProcedure, false);
            }
        }

        #endregion
    }
}