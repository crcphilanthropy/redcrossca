﻿using System.Threading;
using System.Web.Script.Serialization;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using CMSAppAppCode.Old_App_Code.CRC.Modules;

namespace CMSApp.CRC.UrlUpdate
{
    public partial class UrlUpdate : Page
    {
        public const int InYourCommunityNodeId = 85;
        public const int HowWeHelpNodeId = 156;
        public const int AboutUsNodeId = 86;
        public const int TrainingAndCertificationNodeId = 7862;
        public const int ProgramsAndServicesNodeId = 184;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnRun.Click += BtnRunClick;
        }

        void BtnRunClick(object sender, EventArgs e)
        {
           
            var nodeID = ValidationHelper.GetInteger(txtNodeId.Value, 0);
            if (nodeID <= 1) return;

            var processor = new URLUpdateProcessor(nodeID, MembershipContext.AuthenticatedUser);
            var t = new Thread(processor.Run);
            t.Start();
            TransformationHelper.RegisterStartUpScript("showLogs();", Page);
        }
    }

    public class URLUpdateProcessor
    {
        private readonly int _nodeId = -1;
        private readonly UserInfo _user;
        private readonly List<IYCImportModule.IYCLogItem> _logItems = new List<IYCImportModule.IYCLogItem>();

        public URLUpdateProcessor(int nodeId, UserInfo user)
        {
            _nodeId = nodeId;
            _user = user;
        }

        public void Run()
        {
            if (_nodeId <= 0) return;

            var treeProvider = new TreeProvider(_user);
            var selectedNode = treeProvider.SelectSingleNode(_nodeId);

            if (selectedNode == null) return;
            foreach (var lang in new List<string> { "en-CA", "fr-CA" })
            {
                foreach (var document in DocumentHelper.GetDocuments(SiteContext.CurrentSiteName, String.Format("{0}/%", selectedNode.NodeAliasPath), lang, false, null, null, null, 1, true, -1, treeProvider))
                {
                    UpdateDocumentUrlPath(document, treeProvider);
                }
            }
            LogItem(new IYCImportModule.IYCLogItem { SaveAction = IYCImportModule.SaveAction.Finish });
        }

        private void UpdateDocumentUrlPath(TreeNode document, TreeProvider tree, bool recursive = true)
        {
            if (document == null)
                return;

            try
            {
                var typedDocument = tree.SelectSingleNode(document.NodeID, document.DocumentCulture, document.ClassName);
                DocumentHelper.UpdateDocument(typedDocument, tree);

                typedDocument = tree.SelectSingleNode(document.NodeID, document.DocumentCulture, document.ClassName);
                LogItem(new IYCImportModule.IYCLogItem
                    {
                        Name = typedDocument.DocumentUrlPath,
                        Note = "",
                        Path = typedDocument.DocumentUrlPath,
                        SaveAction = IYCImportModule.SaveAction.Update
                    });

                if (!recursive) return;
                var nodes = document.Children.Where(d => d.DocumentCulture == document.DocumentCulture);
                foreach (var node in nodes)
                {
                    UpdateDocumentUrlPath(node, tree);
                }
            }

            catch (Exception ex)
            {
                EventLogProvider.LogException("UrlUpdate", "BtnInlineRunClick", ex);
                LogItem(new IYCImportModule.IYCLogItem
                {
                    Name = document.DocumentName,
                    Note = ex.Message,
                    Path = document.DocumentUrlPath,
                    SaveAction = IYCImportModule.SaveAction.Error
                });
            }
        }

        private void LogItem(IYCImportModule.IYCLogItem item)
        {
            _logItems.Add(item);
            CustomTableHelper.DeleteCustomTableItems("CRC.SerializedObjects", new List<string> { "UrlUpdaterLogItems" }, "Name");
            CustomTableHelper.InsertCustomTableItem("CRC.SerializedObjects", new Dictionary<string, object>
                                                        {
                                                            {"Name", "UrlUpdaterLogItems"},
                                                            {"Value", new JavaScriptSerializer().Serialize(_logItems)}
                                                        });
        }
    }


}