﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="UrlUpdate.aspx.cs" Inherits="CMSApp.CRC.UrlUpdate.UrlUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" />


    <style>
        .bs-callout
        {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

        .bs-callout-info
        {
            border-left-color: #1b809e;
        }

        .border-left
        {
            border-left: 1px solid #eee;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtNodeId').prop("type", "number");
            $('#txtNodeId').prop("min", "1");

            $('#btnTempRun').click(function () {
                if (!$('#txtNodeId')[0].checkValidity())
                    return true;

                var nodeId = parseInt($('#txtNodeId').val());
                $.get('/rest/cms.document?format=json&topn=1&orderby=documentCulture&columns=DocumentNamePath,DocumentUrlPath&where=nodeid=' + nodeId, function (data) {
                    $('#documentNamePath').text(data.cms_documents[0].CMS_Document[0].DocumentNamePath);
                    $("#dialog-confirm").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                $('#btnTempRun').prop('disabled', 'disabled');
                                $('#btnTempRun').text('Running...');
                                $('#btnRun').click();
                                $(this).dialog("close");
                            },
                            Cancel: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });


                return false;
            });
        });
        
        var interval;
        var isComplete = false;
        function showLogs() {

            $('.row-filter').hide();
            $('.row-progress').removeClass('hide');
            $('.row-progress').slideDown("fast");
            interval = setInterval(function () {

                $.get('/api/UrlUpdater/getlogitems?t=' + new Date().valueOf(), function (data) {
                    var logs = $.parseJSON(data);
                    if (logs == undefined)
                        return;

                    if (logs.length > 0) $('.tbody-logs').html('');
                    var warningCounter = 0;

                    $.each(logs, function (i, item) {
                        if (item.SaveAction == -2) {
                            clearInterval(interval);
                            $('.danger-msg').removeClass('hide');
                            $('.danger-msg').show();
                        } else if (item.SaveAction == 3) {
                            clearInterval(interval);

                            var $trSuccess = $('<tr class="success">').append(
                                $('<td>').html("Finished"),
                                $('<td>').html(''),
                                $('<td>').html(item.Note)
                            );
                            $('.tbody-logs').prepend($trSuccess);
                        } else {

                            if (item.SaveAction == -1) {
                                warningCounter++;
                                $('.warning-msg').removeClass('hide');
                                $('.warning-counter').html(warningCounter);
                                $('.warning-msg').show();
                            }

                            var $tr = $(item.SaveAction == -1 ? '<tr class="warning">' : '<tr>').append(
                                $('<td>').html('<a href="' + item.Path + '" target="_blank">' + item.Name + "</a>"),
                                $('<td>').html(item.SaveAction == 1 ? "Inserted" : item.SaveAction == 2 ? "Updated" : item.SaveAction == -1 ? "Error" : ""),
                                $('<td>').html(item.Note)
                            );
                            $('.tbody-logs').prepend($tr);
                        }
                    });
                }).fail(function () {
                    isComplete = true;
                    clearInterval(interval);
                    $('.danger-msg').removeClass('hide');
                    $('.danger-msg').show();
                });
            }, 2000);
        }
    </script>
</head>
<body>
    <div id="dialog-confirm" title="Confirm Path" style="display: none;">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 12px 12px 20px 0;"></span>Are you sure you want to update the URLs for child pages below <strong id="documentNamePath"></strong>?</p>
    </div>
    <form id="form1" runat="server">
        <div class="container theme-showcase" role="main" data-ng-cloak="">
            <div class="page-header">
                <h1>CRC - URL Updater Tool</h1>
            </div>
            <div class="row bs-callout bs-callout-info" id="callout-alerts-no-default">
                <div class="col-md-6">
                    <h4>Instructions</h4>
                    <ol>
                        <li>Enter Node ID in text box.
                        <ul>
                            <li>How to get Node ID
                            <ol>
                                <li>Go to the parent page where you would like all the sub pages url to be updated.</li>
                                <li>Click on <strong>Properties -> General</strong></li>
                                <li>The <strong>Node ID</strong> will be in the <strong>Other properties</strong> section</li>
                            </ol>
                            </li>
                        </ul>
                        </li>
                        <li>Click <strong>Run</strong> to start the process.</li>
                        <li>A confirmation popup will ask you to confirm the path, click yes if the path is correct.</li>
                    </ol>
                </div>
                <div class="col-md-6">
                    <a href="/CRC/images/how-to-get-node-id.png" target="_blank">
                        <img src="/CRC/images/how-to-get-node-id.png" alt="How to get a Node ID" style="width: 200px" class="img-thumbnail" />
                    </a>
                </div>
            </div>
            <div class="row row-filter">
                <div class="col-md-12">
                    <div class="well">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="<%= txtNodeId.UniqueID %>">Node ID</label>
                                    <input type="text" runat="server" class="form-control" id="txtNodeId" clientidmode="Static" required="required" />
                                </div>
                                <button type="submit" class="btn btn-primary" id="btnTempRun">Run</button>
                                <asp:Button runat="server" Text="Run" CssClass="btn btn-primary hide" ID="btnRun" ClientIDMode="Static" />
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:PlaceHolder runat="server" ID="phSuccess" Visible="False">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <strong>Completed!</strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <asp:Repeater runat="server" ID="rptUpdatedDocuments">
                                <HeaderTemplate>
                                    <table class="table table-condensed table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Document Language</th>
                                                <th>Document Name</th>
                                                <th>Document URL</th>
                                                <th>Document Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="<%# Eval("Item5") %>">
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Item1") %></td>
                                        <td><%# Eval("Item2") %></td>
                                        <td><%# Eval("Item3") %></td>
                                        <td><%# Eval("Item4") %></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            
            <div class="row row-progress hide">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Document
                                    </th>
                                    <th>Action
                                    </th>
                                    <th>Info
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="tbody-logs">
                               <%-- <asp:Repeater runat="server" ID="rptLogItems">
                                    <ItemTemplate>
                                        <tr class="<%# ((IYCImportModule.SaveAction)Eval("SaveAction")) == IYCImportModule.SaveAction.Error ?  "danger" : "" %>">
                                            <td><a href="<%# Eval("Path") %>" target="_blank"><%# Eval("Name") %></a></td>
                                            <td><%# Eval("SaveAction") %></td>
                                            <td><%# Eval("Note") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>--%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
