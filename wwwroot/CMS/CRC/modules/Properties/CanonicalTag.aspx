﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CanonicalTag.aspx.cs" Inherits="CMSApp.CRC.modules.Properties.CanonicalTag" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSModules/Content/Controls/editmenu.ascx" TagName="editmenu"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcBeforeContent" runat="server">
    <link href="/CMSPages/GetResource.ashx?stylesheetfile=/App_Themes/Default/bootstrap.css" type="text/css" rel="stylesheet" />
    <link href="/CMSPages/GetResource.ashx?stylesheetfile=/App_Themes/Default/bootstrap-additional.css" type="text/css" rel="stylesheet" />
    <link href="/CMSPages/GetResource.ashx?stylesheetfile=/App_Themes/Default/CMSDesk.css" type="text/css" rel="stylesheet" />
    <link href="/CMSPages/GetResource.ashx?stylesheetfile=/App_Themes/Default/DesignMode.css" type="text/css" rel="stylesheet" />
   

    <cms:editmenu ID="menuElem" runat="server" ShowReject="true" ShowSubmitToApproval="true" ShowProperties="false" IsLiveSite="false" />
</asp:Content>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel ID="pnlContent" runat="server">
        <asp:Label ID="lblWorkflow" runat="server" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Panel ID="pnlForm" runat="server">
            <div>
                <h4>SEO Properties
                </h4>
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="editing-form-label-cell">
                            <label for="<%# txtCanonicalTag.UniqueID %>" class="control-label">Canonical Tag:</label>
                        </div>
                        <div class="editing-form-value-cell">
                            <input type="text" id="txtCanonicalTag" class="form-control" runat="server" />
                        </div>
                    </div>
                </div>

            </div>
            <br />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
