﻿<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true" CodeBehind="BlogCategories.aspx.cs" Inherits="CMSApp.CRC.modules.Properties.BlogCategories" Theme="default" %>

<%@ Register Src="~/CRC/Modules/Properties/Controls/MultipleCategoriesSelector.ascx"
    TagName="MultipleCategoriesSelector" TagPrefix="eca" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel ID="pnlContent" runat="server">
        <cms:LocalizedHeading runat="server" Level="4" ResourceString="categories.documentassignedto"
            CssClass="listing-title"></cms:LocalizedHeading>
        <eca:MultipleCategoriesSelector ID="categoriesElem" runat="server" IsLiveSite="false" CategoryType="Blog" />
    </asp:Panel>
    <div class="Clear">
    </div>
</asp:Content>

