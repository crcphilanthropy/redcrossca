﻿<%@ Control Language="C#" AutoEventWireup="true" Codebehind="Versions.ascx.cs" Inherits="CMSApp.CRC.modules.Properties.controls.VersionsControl" %>
<%@ Register Src="~/CMSModules/Content/Controls/VersionList.ascx" TagName="VersionList"
    TagPrefix="cms" %>
<asp:Panel runat="server" ID="pnlVersions">
        <cms:LocalizedHeading runat="server" ID="headCheckOut" ResourceString="properties.scopenotset" Level="4" EnableViewState="false" />
        <div class="form-horizontal">
            <div class="form-group">
                <div class="editing-form-label-cell">
                    <cms:LocalizedLabel CssClass="control-label" ID="lblVersion" runat="server" ResourceString="VersionsProperties.Version" />
                </div>
                <div class="editing-form-value-cell">
                    <asp:DropDownList runat="server" ID="ddlVersions" CssClass="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <div class="editing-form-label-cell">
                    <cms:LocalizedLabel CssClass="control-label" ID="lblComment" runat="server" ResourceString="VersionsProperties.Comment"
                        EnableViewState="false" AssociatedControlID="txtComment" />
                </div>
                <div class="editing-form-value-cell">
                    <cms:CMSTextArea ID="txtComment" runat="server" />
                </div>
            </div>
            
            <div class="form-group">
                <div class="editing-form-value-cell editing-form-value-cell-offset">
                    
                    <cms:LocalizedButton ID="btnSave" runat="server" ButtonStyle="Primary" Visible="True"
                        Text="Save" EnableViewState="false" />
                </div>
            </div>
            <div class="alert alert-success" role="alert" id="divNotice" runat="server" Visible="False">Comment Added</div>
        </div>
    <cms:VersionList ID="versionsElem" runat="server" IsLiveSite="false" />
</asp:Panel>
