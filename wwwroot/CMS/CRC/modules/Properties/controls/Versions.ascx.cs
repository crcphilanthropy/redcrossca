﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.WorkflowEngine;
using TreeNode = CMS.DocumentEngine.TreeNode;

namespace CMSApp.CRC.modules.Properties.controls
{
    public partial class VersionsControl : CMSUserControl
    {
        #region "Variables"

        private WorkflowInfo mWorkflowInfo;

        #endregion


        #region "Properties"

        /// <summary>
        /// Identifier of edited node.
        /// </summary>
        public int NodeID
        {
            get
            {
                return versionsElem.NodeID;
            }
        }


        /// <summary>
        /// Currently edited node.
        /// </summary>
        public TreeNode Node
        {
            get
            {
                return versionsElem.Node;
            }
            set
            {
                versionsElem.Node = value;
            }
        }


        /// <summary>
        /// Tree provider.
        /// </summary>
        public TreeProvider Tree
        {
            get
            {
                return versionsElem.TreeProvider;
            }
        }


        /// <summary>
        /// Version manager.
        /// </summary>
        public VersionManager VersionManager
        {
            get
            {
                return versionsElem.VersionManager;
            }
        }


        /// <summary>
        /// Workflow manager.
        /// </summary>
        public WorkflowManager WorkflowManager
        {
            get
            {
                return versionsElem.WorkflowManager;
            }
        }


        /// <summary>
        /// Returns workflow step information of current node.
        /// </summary>
        public WorkflowInfo WorkflowInfo
        {
            get
            {
                return mWorkflowInfo ?? (mWorkflowInfo = WorkflowManager.GetNodeWorkflow(Node));
            }
            set
            {
                mWorkflowInfo = value;
            }
        }


        /// <summary>
        /// Returns workflow step information of current node.
        /// </summary>
        public WorkflowStepInfo WorkflowStepInfo
        {
            get
            {
                return versionsElem.WorkflowStepInfo;
            }
        }


        /// <summary>
        /// Indicates if control is enabled
        /// </summary>
        public bool Enabled 
        {
            get
            {
                return pnlVersions.Enabled;
            }
            set
            {
                pnlVersions.Enabled = value;
            }
        }

        #endregion


        #region "Page events"

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnSave.Click += BtnSave_Click;
            BindDropDownList();
        }

        private void BindDropDownList()
        {
            var data = VersionHistoryInfoProvider.GetVersionHistories("DocumentID = " + Node.DocumentID, "VersionNumber Desc", -1, "VersionHistoryID, VersionNumber").Execute();

            IEnumerable<KeyValuePair<int, decimal>> list = new List<KeyValuePair<int, decimal>>();
            if(!DataHelper.DataSourceIsEmpty(data))
                list = data.Tables[0].Rows.Cast<DataRow>()
                    .Select(row => new KeyValuePair<int, decimal>(ValidationHelper.GetInteger(row["VersionHistoryID"], 0),ValidationHelper.GetDecimal(ValidationHelper.GetString(row["VersionNumber"],string.Empty), 0))).OrderByDescending(item => item.Value);

            ddlVersions.DataSource = list;
            ddlVersions.DataTextField = "Value";
            ddlVersions.DataValueField = "Key";
            ddlVersions.DataBind();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            var versionID = ValidationHelper.GetInteger(ddlVersions.SelectedValue, 0);
            if (versionID == 0) return;
            var vh = VersionHistoryInfoProvider.GetVersionHistoryInfo(versionID);
            vh.VersionComment = txtComment.Text;
            vh.Update();

            versionsElem.ReloadData();

            txtComment.Text = string.Empty;
            divNotice.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divNotice.Visible = false;
            // Register the scripts
            ScriptHelper.RegisterLoader(Page);

            // Register the dialog script
            ScriptHelper.RegisterDialogScript(Page);
            CMSPage page = Page as CMSPage;
            if (page != null)
            {
                versionsElem.InfoLabel = page.CurrentMaster.InfoLabel;
                versionsElem.ErrorLabel = page.CurrentMaster.ErrorLabel;
            }
            versionsElem.AfterDestroyHistory += versionsElem_AfterDestroyHistory;
            versionsElem.CombineWithDefaultCulture = false;

            if (Node != null)
            {
                // Check read permissions
                if (MembershipContext.AuthenticatedUser.IsAuthorizedPerDocument(Node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(GetString("cmsdesk.notauthorizedtoreaddocument"), Node.NodeAliasPath));
                }
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DocumentManager.DocumentInfo = DocumentManager.GetDocumentInfo(true);

            ScriptHelper.RegisterEditScript(Page, false);
        }

        #endregion


        #region "Methods"
    
        private void ShowInfo(string message, bool persistent)
        {
            if (IsLiveSite)
            {
                ShowInformation(message, persistent: persistent);
            }
            else
            {
                DocumentManager.DocumentInfo = message;
            }
        }

        /// <summary>
        /// Add java script for refresh tree view.
        /// </summary>
        private void AddAfterActionScript()
        {
            if (!IsLiveSite & (Node!= null))
            {
                ScriptHelper.RefreshTree(Page, Node.NodeID, Node.NodeParentID);
            }
        }

        #endregion


        #region "Button handling"

        protected void versionsElem_AfterDestroyHistory(object sender, EventArgs e)
        {
            AddAfterActionScript();
        }

        #endregion
    }
}
