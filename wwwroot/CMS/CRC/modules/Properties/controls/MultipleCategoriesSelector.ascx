<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.modules.Properties.Controls.CMSModules_Categories_Controls_MultipleCategoriesSelector"
    Codebehind="MultipleCategoriesSelector.ascx.cs" %>
<%@ Register Src="~/CRC/Modules/properties/controls/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="eca" %>
<cms:MessagesPlaceHolder ID="plcMess" runat="server" />
<eca:UniSelector ID="selectCategory" runat="server" ReturnColumnName="CategoryID"
    ObjectType="cms.categorylist" ResourcePrefix="categoryselector" OrderBy="CategoryNamePath"
    AdditionalColumns="CategoryNamePath,CategoryEnabled" SelectionMode="Multiple"
    AllowEmpty="false" IsLiveSite="false" />
