﻿using System;
using System.Web.UI.WebControls;

using CMS.Core;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Membership;
using CMS.UIControls;
using CMS.ExtendedControls;

namespace CMSApp.CRC.modules.Properties
{
    [UIElement(ModuleName.CONTENT, "CRC.CanonicalTag")]
    public partial class CanonicalTag : CMSPropertiesPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            OnSaveData();
            OnAfterSave();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            txtCanonicalTag.Value = Node.GetStringValue("CanonicalTag", string.Empty);
        }

        private void OnAfterSave()
        {
            DocumentManager.OnAfterAction += (sender, args) =>
            {
                ScriptHelper.RefreshTree(this, Node.NodeID, Node.NodeParentID);
            };
        }

        private void OnSaveData()
        {
            DocumentManager.OnSaveData += (sender, args) =>
            {
                Node.SetValue("CanonicalTag", txtCanonicalTag.Value);
            };
        }
    }
}