<%@ Page Language="C#" AutoEventWireup="true" Inherits="CMSApp.CRC.modules.Properties.VersionComments"
    Theme="Default" Codebehind="Versions.aspx.cs" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CRC/modules/properties/Controls/Versions.ascx" TagName="Versions"
    TagPrefix="crc" %>
<asp:Content ContentPlaceHolderID="plcBeforeContent" runat="server">
    <cms:CMSDocumentPanel ID="pnlDocInfo" runat="server" />
</asp:Content>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <crc:Versions runat="server" ID="versionsElem" IsLiveSite="false" />
</asp:Content>
