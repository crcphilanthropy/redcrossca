using System;
using CMS.UIControls;

namespace CMSApp.CRC.modules.Properties
{
    [Security(Resource = "CMS.Content", UIElements = "Properties.Versions")]
    public partial class VersionComments : CMSPropertiesPage
    {
        #region "Page events"

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            versionsElem.Node = Node;

            EnableSplitMode = true;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // Set current tab
            SetPropertyTab(TAB_VERSIONS);

            if (Node == null)
            {
                // Hide all if no node is specified
                versionsElem.Visible = false;
            }

            versionsElem.Enabled = !DocumentManager.ProcessingAction;

            DocumentManager.LocalDocumentPanel = pnlDocInfo;
        }

        #endregion
    }
}