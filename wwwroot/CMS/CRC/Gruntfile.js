module.exports = function(grunt) {
   grunt.initConfig({
   pkg: grunt.file.readJSON('package.json'),

   sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          style: 'expanded', //compressed
          lineNumbers: true
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
   }, 
           
   jshint: {
      all: ['js/eca.js']
   },
   
   watch: {         
      css: {
         files: 'scss/**',
         tasks: ['sass', 'cssmin']
      },
      js: {
        files: ['js/eca.js', '../Ecentricarts/**', 'js/donate.js'],
        tasks: ['concat', 'jshint', 'uglify']
      },      
      psd: {
        files: ['images-assets/**'],
        tasks: ['sync:psd'],
        options: {
          livereload: true
        }
      }
   },

   sync: {
      build: {
        files: [
          {
            //cwd: 'CRC',
            src: ['js/app.js'],
            dest:'static/build'
          },
          {
            cwd: 'css',
            src: ['app.css'],
            dest:'static/build/css'
          },
          {
            cwd: 'js/navigtion',
            src: ['main.js'],
            dest:'static/build/'
          }
        ],
        verbose: true
      },
      psd: {
        files: [
          {
            cwd: 'images-assets',
            src: '**',
            dest: 'build/images'
          }
        ],
        verbose: true
      }
   },

   concat: {
      functions: {
         src: [
            'bower_components/foundation/js/foundation.min.js',
            'js/libs/jquery.fitvids.js',
            'js/libs/fastclick.js',
            'js/libs/jquery-accessibleMegaMenu.js',          
            'js/libs/slick/slick.js',   
            'js/eca.js',
			'../Ecentricarts/Scripts/src/eca/eca.js',
            '../Ecentricarts/Scripts/src/eca/eca.util.js',
            '../Ecentricarts/Scripts/src/eca/eca.http.js',
            '../Ecentricarts/Scripts/src/eca/kentico/eca.kentico.mobilemenu.js',          
            '../Ecentricarts/Scripts/src/eca/eca.template.js',   
            '../Ecentricarts/Scripts/e2e/mobilemenu.js'
         ],
         dest: 'js/app.js'
      }
   },

   uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          'js/app.min.js': ['js/app.js'],
          '../src/legacy.min.js': ['js/app.js']
        }
      }
   },
    
   cssmin: {
      css:{
         src: 'css/app.css',
         dest: 'css/app.min.css'
      }
   }

  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');


  grunt.registerTask('build', ['sass', 'concat']);
  grunt.registerTask('deploy', ['uglify', 'cssmin']);
  grunt.registerTask('default', ['sass', 'concat', 'jshint', 'uglify', 'cssmin', 'watch']);
}
