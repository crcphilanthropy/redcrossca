using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.Helpers;

namespace CMSApp.CRC
{
    public class TableHeaderLink : LinkButton
    {
        private new string Text { get; set; }

        public string ResString { get; set; }
        public string OrderByField { get; set; }

        public string Direction
        {
            get { return ViewState["Direction"] as string; }
            set { ViewState["Direction"] = value; }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            Attributes.Remove("data-direction");
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Text = ResHelper.GetString(ResString);
            base.Render(writer);
        }
    }
}