var RCHolidayConfig = (function() {

    'use strict';

    return {
        CurrentCulture: $('html').attr('lang'),
        GTMTrackingCode: 'GTM-KTJ6QC',
        Languages: {
            en: 'en-CA',
            fr: 'fr-CA'
        },
        CurrencyFormat: {
            en: '$0,0',
            fr: '0,0 $'
        },
        ScreenIds: {
            Loading: 'loading',
            Problem: 'problem',
            Donate: 'donate'
        },
        GiftBoxTypes: {
            Undefined: 0,
            PreSetAmount: 1,
            UserDefinedAmount: 2
        },
        SectionKeyFrames: [
            {
                Section: 'problem',
                Desktop: 0,
                Mobile: 0
            },
            {
                Section: 'donate',
                Desktop: 110,
                Mobile: 100
            }
        ],
        NavigationSections: {
            en: [
                {
                    id: 'problem',
                    path: 'help'
                },
                {
                    id: 'donate',
                    path: 'how'
                }
            ],
            fr: [
                {
                    id: 'problem',
                    path: 'aide'
                },
                {
                    id: 'donate',
                    path: 'comment'
                }
            ]
        },
        DefaultCampaign: {
            problemHeader: {
                en: 'Unique Gifts that can save lives',
                fr: 'Des cadeaux uniques pouvant sauver des vies'
            },
            problemSubheader: {
                en: 'Donate now and BMO will double your impact with a matching gift!*',
                fr: 'Faites un don maintenant et BMO en doublera l’impact avec un don équivalent*'
            },
            problemBody: {
                en: 'Choose which Gifts of Survival you\'ll send to those in need this holiday season',
                fr: 'Choisissez les présents de survie que vous voulez offrir aux personnes vulnérables pour les fêtes'
            },
            donateHeader: {
                en: 'Deliver gifts of survival that help others this holiday season',
                fr: 'Pour les fêtes, offrez des articles de survie à ceux qui ont besoin d\'aide'
            },
            donatePSTestHeader: {
                en: 'Deliver a Red Cross Survival Kit that helps others this holiday season',
                fr: 'Pour les fêtes, offrez une trousse de survie de la croix-rouge à ceux qui ont besoin d’aide'
            }
        },
        GiftBoxInactiveImage: {
            en: '/crc/holiday-2016/img/items/box-with-supplies-en.jpg',
            fr: '/crc/holiday-2016/img/items/box-with-supplies-fr.jpg'
        },
        MaxCartItemCount: 99,
        MaxGiftBoxAmount: 999999,
        DonationTypes: {
            self: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58508' },
                    fr: { clientId: '1951', campaignId: '58509' }
                }
            },
            selfPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58649' },
                    fr: { clientId: '1951', campaignId: '58650' }
                }
            },
            selfNonPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58652' },
                    fr: { clientId: '1951', campaignId: '58653' }
                }
            },
            giftEcard: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58577' },
                    fr: { clientId: '1951', campaignId: '58578' }
                }
            },
            giftEcardPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58654' },
                    fr: { clientId: '1951', campaignId: '58655' }
                }
            },
            giftEcardNonPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58656' },
                    fr: { clientId: '1951', campaignId: '58657' }
                }
            },
            giftPrinted: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58594' },
                    fr: { clientId: '1951', campaignId: '58595' }
                }
            },
            giftPrintedPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58932' },
                    fr: { clientId: '1951', campaignId: '58933' }
                }
            },
            giftPrintedNonPS: {
                key: 'self',
                urlTemplate: 'https://donate.redcross.ca/ea-action/action?ea.client.id={{clientId}}&ea.campaign.id={{campaignId}}&en_txn4={{donationSplit}}&amount={{amount}}',
                values: {
                    en: { clientId: '1951', campaignId: '58934' },
                    fr: { clientId: '1951', campaignId: '58935' }
                }
            }
        },
        RedCrossLogoLink: {
            en: 'http://www.redcross.ca/',
            fr: 'http://www.croixrouge.ca/'
        },
        SkipAheadDonationUrl: {
            en: 'https://donate.redcross.ca/ea-action/action?ea.campaign.id=44676&ea.client.id=1951',
            fr: 'https://donate.redcross.ca/ea-action/action?ea.campaign.id=44677&ea.client.id=1951'
        },
        LoadManifest: [
            { id: 'campaigns', src: '/crc/holiday-2016/data/campaigns.json' },
            { id: 'donationItems', src: '/crc/holiday-2016/data/donation-items.json' }
        ]
    }

}());