'use strict';

var dataLayer = window.dataLayer || [];

var RCHolidayGlobal = (function() {

    return new function() {

        this.ua = detect.parse(navigator.userAgent);

        this.getMobileDeviceHeight = function() {

            var deviceHeight = screen.height;

            if(this.ua.os.family === 'Android') {

                if (window.orientation == 0 || window.orientation == 180) {

                    // Portrait Mode
                    deviceHeight = screen.height;
                }
                else if (window.orientation == 90 || window.orientation == -90) {

                    // Landscape Mode
                    deviceHeight = screen.height;
                }
            }
            else {

                if (window.orientation == 90 || window.orientation == -90) {

                    // Landscape Mode
                    deviceHeight = screen.width;
                }
                else if (window.orientation == 0 || window.orientation == 180) {

                    //Portrait Mode
                    deviceHeight = screen.height;
                }

            }

            return deviceHeight;

        };

        this.mobileDeviceHeight = this.getMobileDeviceHeight();

    }

}());

(function ($) {

    $(function() {

        if( (RCHolidayGlobal.ua.device.type === "Mobile" || RCHolidayGlobal.ua.device.type === "Tablet") || RCHolidayGlobal.ua.os.family === "Android" === true) {
            $('html').addClass('isMobile');
        }

        RCHolidayApp.Init();

        $(document).foundation();
     });

 
})(jQuery);