var RCHolidayDataContext = (function() {

    'use strict';

    return new function() {

        var self = this,
        loadingError = false,
        dataLoaded = false,
        loadQueue = new createjs.LoadQueue(),
        Events = {
            Loaded: new signals.Signal(),
            Error: new signals.Signal()
        };

        function loadData (callback) {

            loadQueue.loadManifest(RCHolidayConfig.LoadManifest);

            loadQueue.on('complete', function () {

                if(!loadingError) {

                    dataLoaded = true;
                    loadQueue.removeAllEventListeners();
                }

                var success = !loadingError && dataLoaded;

                if (callback && _.isFunction(callback))
                    callback({ success: success });

            });

            loadQueue.on('error', function() {
                loadingError = true;
            });

        }

        function getCampaignsCollectionData() {

            var data = loadQueue.getResult('campaigns');

            if(data) {
                return _.where(data, {culture: RCHolidayConfig.CurrentCulture});
            }

            return [];
        }

        function getDonationItems() {

            var data = loadQueue.getResult('donationItems');

            if(data) {
                return data;
            }

            return [];

        }

        //
        // public members
        //

        return {
            GetCampaignsCollectionData: getCampaignsCollectionData,
            GetDonationItems: getDonationItems,
            Events: Events,
            LoadData: loadData
        };
    };

}());