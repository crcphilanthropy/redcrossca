var RCHolidayApp = (function (StringJS) {

    'use strict';

    return new function () {

        var $window = $(window),
            $screens = $('section.screen');


        var self = this,
            lastGiftBoxAmount = 0,
            mouseScrollDirection = null,
            keyDirection = null,
            manualScrollDirection = null,
            scrollTop = 0,
            lastScrollTop = 0,
            lastScrollDirection = 0,
            lastKeyDirection = 0,
            nextScreen = null,

            lastWasAnimatedScroll = false,
            lastWasManualScroll = false,
            listenForScroll = true;

        function isElementInViewport(el, opts) {

            var settings = _.extend({ check: 'top' }, opts);

            //special bonus for those using jQuery
            if (typeof jQuery === "function" && el instanceof jQuery) {
                el = el[0];
            }

            var rect = el.getBoundingClientRect();

            if (settings.check === 'bottom') {

                return (
                    rect.top + rect.height < (window.innerHeight || document.documentElement.clientHeight)
                );

            }
            else {

                return (
                    rect.top < (window.innerHeight || document.documentElement.clientHeight)
                );

            }


        }

        function registerEventListeners() {

            self.GiftBoxUserAmount.subscribe(function (value) {

                if (value !== null) {

                    var giftBoxAmount = _.parseInt(value);

                    if (_.isNumber(giftBoxAmount) && !_.isNaN(giftBoxAmount) && giftBoxAmount > 0) {

                        if (giftBoxAmount <= RCHolidayConfig.MaxGiftBoxAmount) {

                            self.GiftBoxUserAmount(giftBoxAmount.toString());
                            setUserDefinedGiftBoxAmount(giftBoxAmount);    

                            if (self.getVersion() === 2){
                                self.UpdateChart();
                            }
                              
                            if (self.getVersion() !== 1)  { 
                                self.GiftBoxUserAmountTooltip(giftBoxAmount); 
                                self.showAllPins();
                            }

                            dataLayer.push({
                                "event": "holiday-event",
                                "eventAction": "additem",
                                "eventLabel": "Gift Box Other - " + giftBoxAmount,
                                "eventValue": giftBoxAmount
                            });
                        }
                        else {

                            giftBoxAmount = RCHolidayConfig.MaxGiftBoxAmount;
                            self.GiftBoxUserAmount(giftBoxAmount.toString());
                            setUserDefinedGiftBoxAmount(giftBoxAmount);
                        }

                    }
                    else {

                        self.GiftBoxUserAmount(null);
                        setUserDefinedGiftBoxAmount(0);
                        self.UpdateChart();

                    }
                }

            });

            // $window.on('resize', _.debounce(function () {

            //     if(!self.IsMobile) {
            //         // RCHolidayPresenter.UpdateSlide($slides);
            //     }

            // }, 250, {leading: true, trailing: true}));

            $window.bind('orientationchange', function (e) {

                RCHolidayGlobal.mobileDeviceHeight = RCHolidayGlobal.getMobileDeviceHeight();
                $screens.css({ minHeight: RCHolidayGlobal.mobileDeviceHeight });

            });

            // $('#txtUserDefineGiftBoxAmount').bind('keyup', function(evt) {

            //     evt.stopPropagation();

            //     var $this = $(this),
            //         keyCode = evt.keyCode;

            //     if(keyCode === 13) {
            //         $this.blur();
            //     }


            // });

        }

        function registerNavigationEventListeners() {

            $window.bind('mousewheel DOMMouseScroll', function (evt) {
                //disable scroll lock
                return;

                // evt.preventDefault();
                mouseScrollDirection = getMouseScrollDirection(evt.originalEvent);
                nextScreen = getNextPage(mouseScrollDirection, self.CurrentScreen());

                if (mouseScrollDirection !== lastScrollDirection) {
                    $window.trigger('scroll', { isAnimatedScroll: true, animationType: 'screen', animationDirection: mouseScrollDirection });
                }

            });

            $window.on('keyup', _.debounce(function (evt) {
                //disable scroll lock
                return;

                var activate = false;

                if (evt.keyCode === 38 || evt.keyCode === 40) {

                    // arrow key up
                    keyDirection = (evt.keyCode === 38) ? -1 : 1;
                    activate = true;
                }

                if (keyDirection !== lastKeyDirection && activate) {

                    lastKeyDirection = keyDirection;
                    $window.trigger('scroll', { isAnimatedScroll: true, animationType: 'screen', animationDirection: keyDirection });

                }

            }, 150, { leading: false, trailing: true }));

            $window.on('scroll', _.debounce(function (evt, data) {

                if ($('body').hasClass('is-reveal-open')) {
                    return;
                }

                var opts = _.extend({ isAnimatedScroll: false, animationType: null, animationDirection: 0 }, data),
                    isAnimatedScroll = opts.isAnimatedScroll || false;

                if (isAnimatedScroll) {

                    lastWasAnimatedScroll = true;

                    if (opts.animationType === 'screen') {

                        nextScreen = getNextPage(opts.animationDirection, self.CurrentScreen());
                        self.AnimateToScreen(nextScreen, self.IsMobile, function () {
                            lastScrollTop = $window.scrollTop();
                        });
                    }

                    if (opts.animationType === 'section') {

                        self.AnimateToScreen(opts.screen, self.IsMobile, function () {
                            lastScrollTop = $window.scrollTop();
                        });
                    }

                }
                else {

                    if (!self.IsMobile) {

                        if (!lastWasAnimatedScroll) {

                            if (!lastWasManualScroll) {

                                lastWasManualScroll = true;
                                manualScrollDirection = getManualScrollDirection();
                                nextScreen = getNextPage(manualScrollDirection, self.CurrentScreen());

                                self.AnimateToScreen(nextScreen, self.IsMobile, function () {
                                    lastScrollTop = $window.scrollTop();
                                });

                            }
                            else {
                                lastWasManualScroll = false;
                            }


                        }
                        else {

                            lastWasAnimatedScroll = false;

                        }
                    }

                }

                if (self.IsMobile) {
                    var scrollTop = $window.scrollTop(),
                        section = RCHolidayRouter.GetSectionFromScrollPosition(scrollTop);

                    if (section) {
                        RCHolidayRouter.SetSilentRoute(section.id);
                    }
                }

            }, 300));
        }

        function getMouseScrollDirection(scrollEvent) {

            var eventType = scrollEvent.type,
                scrollDelta = (eventType === 'DOMMouseScroll') ? scrollEvent.detail : scrollEvent.wheelDelta,
                normalizer = (eventType === 'DOMMouseScroll') ? -1 : 1,
                scrollDirection = ((scrollDelta * normalizer) / Math.abs(scrollDelta)) * -1;

            return scrollDirection;
        }

        function getManualScrollDirection(evt) {

            var direction = 0;

            scrollTop = $window.scrollTop();

            if (scrollTop > lastScrollTop) {

                direction = 1;

            }
            else {

                direction = -1;

            }

            return direction;

        }

        function setInitialScrollPosition(route, isMobile, callback) {

            var section = { id: route.Section.id },
                scrollPos = RCHolidayPresenter.GetScrollTopFromSection(section, isMobile);

            lastWasAnimatedScroll = true;
            $window.scrollTop(scrollPos);
            lastScrollTop = scrollPos;

            if (callback && _.isFunction(callback)) {
                callback(section);
            }
        }

        function getNextPage(scrollingDirection, currentPage) {

            var nextPage = (currentPage + scrollingDirection);

            if (nextPage < 1) nextPage = 1;

            else if (nextPage > 3) nextPage = 3;

            return nextPage;

        }

        function setUserDefinedGiftBoxAmount(amount) {

            self.DonationCart.GiftBoxTotal(amount);
            self.DonationCart.GiftBoxType(RCHolidayConfig.GiftBoxTypes.UserDefinedAmount)
            RCHolidayPresenter.ResetGiftBoxPresetAmounts();

        }

        
         function initializeUIComponents() {
             RCHolidayPresenter.UpdateSlide($slides);
         }

        function initialize(callback) {

            // assign data to variables
            var campaignCollectionData = RCHolidayDataContext.GetCampaignsCollectionData(),
                donationItemsData = RCHolidayDataContext.GetDonationItems();

            self.CurrentCulture = RCHolidayConfig.CurrentCulture;
            self.Campaigns = null;
            self.CurrentCampaign = null;
            self.CurrentScreen = ko.observable(1);
            self.DonationItemCollection = [];
            self.GiftBoxAmounts = [75, 150, 250, 500];
            self.GiftBoxUserAmount = ko.observable();
            self.DonationCart = new RCHolidayModel.DonationCart({
                donationItemsData: donationItemsData
            });
            self.UIStrings = RCHolidayUIStrings;
            self.DonationType = ko.observable('self');
            self.IsMobile = RCHolidayUtils.IsMobile();
            self.LogoLink = RCHolidayConfig.RedCrossLogoLink[RCHolidayConfig.CurrentCulture];
            self.GiftBoxInactiveImage = RCHolidayConfig.GiftBoxInactiveImage[RCHolidayConfig.CurrentCulture];

            // initialize router
            RCHolidayRouter.Init({
                campaignsCollection: campaignCollectionData,
                onComplete: function () {

                    if (RCHolidayRouter.Route.Campaign !== null) {

                        // initialize campaign copy
                        var campaignData = _.findWhere(campaignCollectionData, { id: RCHolidayRouter.Route.Campaign });
                        self.CurrentCampaign = new RCHolidayModel.Campaign(campaignData);
                    }
                    else {

                        // initialize default copy
                        self.CurrentCampaign = new RCHolidayModel.Campaign(RCHolidayConfig.DefaultCampaign);

                    }

                    self.CurrentScreen(RCHolidayRouter.SectionIndex);


                    // GTM Tracking
                    dataLayer.push({
                        'language': RCHolidayConfig.CurrentCulture,
                        'variation': self.CurrentCampaign.id || 'control'
                    });

                    (function (w, d, s, l, i) {
                        w[l] = w[l] || [];
                        w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });

                        var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s),
                            dl = (l != 'dataLayer') ? '&l=' + l : '';

                        j.async = true;
                        j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                        f.parentNode.insertBefore(j, f);

                    })(window, document, 'script', 'dataLayer', RCHolidayConfig.GTMTrackingCode);

                }
            });

            switch (self.getVersion('version')) {
                case 2:
                    var itemsInCart = 0;

                    $('#donate:not(.hide) #txtUserDefineGiftBoxAmount').bind('keypress', function (e) {
                        if (e.keyCode !== 13) {
                            self.DonationCart.Reset();
                        }

                        if (e.keyCode === 13) {
                            var usergivenamount = self.GiftBoxUserAmount();

                            self.GiftBoxUserAmountTooltip(usergivenamount);
                        }

                        $('.item, .select-all, .ps-test-add').removeClass('active');
                        itemsInCart = 0;

                    });



                    self.AddDonationItem = function (donationItem) {

                        if (donationItem.count() < 1) {
                            itemsInCart++;
                        }
                        else {
                            itemsInCart--;
                        }

                        if (self.GiftBoxUserAmount() > 0 || self.DonationCart.GiftBoxTotal() > 0) {
                            self.ResetDonationCart();
                        }

                        self.DonationCart.AddPseudoItem(donationItem);
                        console.log(itemsInCart);

                        self.ToolTipMessage(donationItem, itemsInCart);

                        self.calculateWidth();


                        var chartTotal = self.DonationCart.Total();

                        self.ProgressBar(chartTotal);
                        return true;

                        dataLayer.push({
                            "event": "holiday-event",
                            "eventAction": "additem",
                            "eventLabel": donationItem.codeName,
                            "eventValue": donationItem.price
                        });

                    };


                    self.defaultResetDonationCart = self.ResetDonationCart;

                    self.ResetDonationCart = function(){

                        itemsInCart = 0;

                        myChart.data.datasets[0].data = [0, 100]; 
                        myChart.update();

                        $('.selectAll, .ps-test-add').removeClass('active');
                        $(".fill-number, .is-full").empty();
                        
                        self.closeTooltip(); 
                        self.hideAllPins();

                        var result =  self.defaultResetDonationCart.apply(this, arguments);

                        return result;
                    };

                    var update = function () {
                        myChart.data.datasets[0].data = [10, 190];

                        myChart.update();
                    };
                    var secondUpdate = function () {
                        myChart.data.datasets[0].data = [0, 200];

                        myChart.update();
                    };

                    $('.international, .domestic').on('mouseenter', '.item-container', function () {
                        if (self.DonationCart.Total() === 0) {
                            update();
                            setTimeout(secondUpdate, 300);
                        }
                    });

                    break;

                case 3:
                    var itemsInCart = 0;

                    $('#donate:not(.hide) #txtUserDefineGiftBoxAmount').bind('keypress', function (e) {
                        if (e.keyCode !== 13) {
                            self.DonationCart.Reset();
                        }

                        if (e.keyCode === 13) {
                            var usergivenamount = self.GiftBoxUserAmount();

                            self.GiftBoxUserAmountTooltip(usergivenamount);
                        }

                        if (e.keyCode === 46) {
                            self.UpdateChart();
                            console.log("updated");
                        }

                        $('.item, .select-all').removeClass('active');
                        itemsInCart = 0;

                    });

                    self.AddDonationItem = function (donationItem) {

                        if (donationItem.count() < 1) {
                            itemsInCart++;
                            self.ToolTipMessage(donationItem, itemsInCart);
                        }


                        if (self.GiftBoxUserAmount() > 0 || self.DonationCart.GiftBoxTotal() > 0) {
                            self.ResetDonationCart();
                        }


                        if (self.GiftBoxUserAmount() > 0 || self.DonationCart.GiftBoxTotal() > 0) {
                            self.ResetDonationCart();
                        }

                        self.DonationCart.AddItem(donationItem);

                        self.showPin(donationItem);

                        dataLayer.push({
                            "event": "holiday-event",
                            "eventAction": "additem",
                            "eventLabel": donationItem.codeName,
                            "eventValue": donationItem.price
                        });

                        return true;
                    };

                    self.defaultRemoveDonationItem = self.RemoveDonationItem;

                    self.RemoveDonationItem = function (donationItem) {
                        if (donationItem.count() === 1) {
                            itemsInCart--;
                            self.ToolTipMessage(donationItem, itemsInCart);
                            self.hidePin(donationItem);
                        };

                        var result = self.defaultRemoveDonationItem.apply(this, arguments);

                        return result;
                    };
                    
                    self.defaultResetDonationCart = self.ResetDonationCart;

                    self.ResetDonationCart = function(){

                        itemsInCart = 0;
                    
                        var result =  self.defaultResetDonationCart.apply(this, arguments);


                        return result;
                    };


                    break;
                default:
                    console.log("default");
            }

            // register event listeners
            registerEventListeners();

            // initialize knockout
            ko.applyBindings(self);

            // initialize jQuery
            //initializeUIComponents();

            if (callback && _.isFunction(callback))
                callback({ route: RCHolidayRouter.Route });


        }

        // ---------------- //

        self.GotoScreen = function (screen, isMobile, callback) {

            self.CurrentScreen(screen);

            RCHolidayPresenter.GotoScreen(screen, isMobile, function (section) {

                RCHolidayRouter.SetSilentRoute(section.id);

                if (callback && _.isFunction(callback)) {
                    callback();
                }

            });

        };

        self.AnimateToScreen = function (screen, isMobile, callback) {

            self.CurrentScreen(screen);

            RCHolidayPresenter.AnimateToScreen(screen, isMobile, function (section) {

                RCHolidayRouter.SetSilentRoute(section.id);

                if (callback && _.isFunction(callback)) {
                    callback();
                }

            });

        };

        self.TriggerUIScroll = function (screen) {

            $window.trigger('scroll', { isAnimatedScroll: true, animationType: 'section', screen: screen });

        };

        self.FormatCurrencyString = function (value) {
            return RCHolidayUtils.FormatCurrency(value);
        };

        self.ResolveCultureString = function (cultureString, uiCulture) {
            return RCHolidayUtils.ResolveCultureString(cultureString, uiCulture);
        };

        self.AddDonationItem = function (donationItem) {

            self.DonationCart.AddItem(donationItem);


            dataLayer.push({
                "event": "holiday-event",
                "eventAction": "additem",
                "eventLabel": donationItem.codeName,
                "eventValue": donationItem.price
            });

        };

        self.RemoveDonationItem = function (donationItem) {

            self.DonationCart.RemoveItem(donationItem);

            dataLayer.push({
                "event": "holiday-event",
                "eventAction": "removeitem",
                "eventLabel": donationItem.codeName,
                "eventValue": donationItem.price
            });

        };

        self.SetPresetGiftBoxAmount = function (amount, evt) {

            self.DonationCart.GiftBoxTotal(amount);
            self.DonationCart.GiftBoxType(RCHolidayConfig.GiftBoxTypes.PreSetAmount);
            self.GiftBoxUserAmount(null);

            var $el = $(evt.currentTarget);
            RCHolidayPresenter.ResetGiftBoxPresetAmounts();
            $el.addClass('active');

            if (amount !== lastGiftBoxAmount) {

                dataLayer.push({
                    "event": "holiday-event",
                    "eventAction": "additem",
                    "eventLabel": "Gift Box - " + amount,
                    "eventValue": amount
                });

                lastGiftBoxAmount = amount;
            }

        };

        self.ResetDonationCart = function () {

            if (self.DonationCart.IsActive()) {
                self.DonationCart.Reset();
                self.GiftBoxUserAmount(null);
                self.DonationCart.GiftBoxType(RCHolidayConfig.GiftBoxTypes.Undefined)
            }

        };

        self.GetDonationItemImageUrl = function (image) {

            return StringJS('url("{{img}}")').template({ img: image });
        };

        self.CompleteDonation = function (d, e) {

            if (!self.DonationCart.IsActive())
                return;

            var selectedItems = [],
                gtmItemGroup = [],
                giftBoxType = self.DonationCart.GiftBoxType(),
                giftBoxAmount = self.DonationCart.GiftBoxTotal(),
                giftBoxDonatedItem = '',
                gtmDonatedItemTemplate = '{{ name }}',
                gtmDonatedGiftBoxTemplate = '{{ name }} - {{ price }}',
                gtmDonatedItemGroupTemplate = '{{count}} {{name}}';

            // get items that have been donated
            _.forEach(self.DonationCart.DonationItemsCollection.Items(), function (item) {

                if (item.count() > 0) {
                    selectedItems.push({ count: item.count(), codeName: item.codeName, price: item.price });
                }

            });

            // parse single item
            _.forEach(selectedItems, function (item) {

                var tmpItem = S(gtmDonatedItemTemplate).template({ name: item.codeName, price: item.price }).s,
                    tmpItemGroup = S(gtmDonatedItemGroupTemplate).template({ count: item.count, name: item.codeName, price: item.price }).s;

                dataLayer.push({
                    "event": "holiday-event",
                    "eventAction": "donateditem",
                    "eventLabel": tmpItem,
                    "eventValue": item.price
                });

                gtmItemGroup.push(tmpItemGroup);
            });

            if (self.DonationCart.GiftBoxIsActive()) {

                switch (giftBoxType) {
                    case RCHolidayConfig.GiftBoxTypes.PreSetAmount:
                        giftBoxDonatedItem = S(gtmDonatedGiftBoxTemplate).template({ name: 'Gift Box', price: giftBoxAmount }).s;
                        break;
                    case RCHolidayConfig.GiftBoxTypes.UserDefinedAmount:
                        giftBoxDonatedItem = S(gtmDonatedGiftBoxTemplate).template({ name: 'Gift Box Other', price: giftBoxAmount }).s;
                        break;
                }

                dataLayer.push({
                    "event": "holiday-event",
                    "eventAction": "donateditem",
                    "eventLabel": giftBoxDonatedItem,
                    "eventValue": giftBoxAmount
                });

                gtmItemGroup.push(giftBoxDonatedItem);
            }

            // parse item group
            dataLayer.push({
                "event": "holiday-event",
                "eventAction": "donateditem grouping",
                "eventLabel": gtmItemGroup.join(','),
                "eventValue": self.DonationCart.Total()
            });

            // Navigation to Donation Form
            setTimeout(function () {

                var opts = {
                    donationType: e.currentTarget.value, //self.DonationType(),
                    donationTotal: self.DonationCart.Total(),
                    domesticTotalSplit: Math.round(self.DonationCart.DomesticTotal() / self.DonationCart.Total() * 100),
                    internationalTotalSplit: Math.round(self.DonationCart.InternationalTotal() / self.DonationCart.Total() * 100)
                },
                    gaParams = (typeof (ga) != 'undefined' && ga.hasOwnProperty("getAll")) ? '&' + ga.getAll()[0].get('linkerParam') : '',
                    url = RCHolidayRouter.GetDonateUrl(opts) + gaParams;

                console.log(opts.donationType)
                //Donation Type
                dataLayer.push({
                    "event": "donation-type",
                    "donationType": opts.donationType
                });


                window.location.href = url


            }, 300);

        };

        self.SkipAheadDonationUrl = function () {

            return RCHolidayConfig.SkipAheadDonationUrl[RCHolidayConfig.CurrentCulture];

        };

        self.getVersion = function (theParameter) {
            return ab_test_version;
        };

        self.Init = function () {


            // RCHolidayPresenter.AnimateLoadingScreenIn(function() {

            RCHolidayDataContext.LoadData(function (results) {

                if (results.success) {

                    // data has loaded, ready to initialize!

                    initialize(function (params) {

                        if (!RCHolidayUtils.IsMobile()) {

                            // RCHolidayPresenter.InitializeSkrollr();


                            $window.on('scroll', _.debounce(function () {

                                if (self.CurrentScreen() !== 1) {

                                    // // Video Reset
                                    // video1.currentTime = 0;
                                    // video2.currentTime = 0;
                                }
                                else {
                                    // // Video Play
                                    // video1.play();
                                    // video2.play();
                                }

                            }, 80, { leading: false, trailing: true }));
                        }
                        else {

                            var lastScrollTop = 0,
                                st,
                                direction,
                                $donateContainer = $('#donate:not(.hide) .donate-container'),
                                $giftBoxContainer = $('#gift-box-container'),
                                $masterElements = $('.master-elem'),
                                // $logo = $('#logo'),
                                $tooltip = $('#donate:not(.hide)  #mobileTooltip'),
                                $donate = $('#donate:not(.hide)'),
                                $buttons = $('.buttons'),
                                $footer = $('#donate:not(.hide) .footer-element');


                            $screens.css({ minHeight: RCHolidayGlobal.mobileDeviceHeight });

                            $window.on('scroll', _.debounce(function () {

                                st = window.pageYOffset;
                                direction = (st > lastScrollTop) ? 'down' : 'up';

                                if (isElementInViewport($donateContainer)) {

                                    $buttons.addClass('initialized');
                                    $tooltip.removeClass('hide');

                                    // $logo.velocity({ opacity: 0 }, {
                                    //     complete: function () {
                                    //         $logo.hide();
                                    //     }
                                    // });

                                    // $masterElements.velocity({ opacity: 0 }, {
                                    //     complete: function () {
                                    //         $masterElements.hide();
                                    //     }
                                    // });
                                }
                                else {

                                    $buttons.removeClass('initialized');
                                    $tooltip.addClass('hide');
                                    // $logo.velocity({ opacity: 1 }, {
                                    //     begin: function () {
                                    //         $logo.css({ display: 'block' });
                                    //     }
                                    // });

                                    // $masterElements.velocity({ opacity: 1 }, {
                                    //     begin: function () {
                                    //         $masterElements.show();
                                    //     }
                                    // });
                                }

                                if (isElementInViewport($footer)) {
                                    $buttons.addClass('initialized');
                                    $donate.addClass('extra-padding');

                                    if (direction === "down") {

                                        $buttons.addClass('open');
                                        $donate.addClass('extra-padding');
                                    }
                                }
                                else {

                                    if (direction === 'up') {
                                        $buttons.removeClass('open');
                                    }
                                }


                                lastScrollTop = st;

                            }, 80, { leading: false, trailing: true }));
                        }

                        setInitialScrollPosition(params.route, self.IsMobile, function () {

                            // video1.play();
                            // video2.play();
                            registerNavigationEventListeners();

                        });

                        // RCHolidayPresenter.AnimateLoadingScreenOut();

                    });

                }
                else {
                    // error loading data, cannot initialize :(
                }

            });

            // });

        }

        Chart.defaults.global.legend.display = false;
        Chart.defaults.global.tooltips.enabled = false;

        var ctx = $("#myChart");
        var data = {
            datasets: [{
                data: [0, 100],
                backgroundColor: [
                    "#e00",
                    "#E7E9ED",

                ],
                hoverBackgroundColor: [
                     "#e00",
                     "#E7E9ED",
                ],
                borderWidth: 0,
            }],
            labels: [
                "Red",
                "Grey",
            ]
        };

        var options = {
            cutoutPercentage: 95,
        }
        if (self.getVersion() !== 1) {
            if (self.getVersion() === 2) {
                var myChart = new Chart(ctx,
                {
                    type: 'doughnut',
                    data: data,
                    options: options
                });


                self.ProgressBar = function (amount) {
                    if (amount > 200) {
                        amount = 200;
                    }
                    var amountMax = 200 - amount;
                    myChart.data.datasets[0].data = [amount, amountMax];

                    var num = Math.ceil(amount / 200 * 100);
                    myChart.update();
                    $(".fill-number").html(num + '%');

                    var full = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.GlobalSurvivalKitFull);
                    $(".is-full").html(full);
                };
            }
            self.UpdateChart = function (userAmount) {
                self.ProgressBar(self.DonationCart.Total());
            };

            self.UpdateInputChart = function (userAmount) {
                self.ProgressBar(self.DonationCart.Total());
                self.GiftBoxUserAmountTooltip();
            };

            self.SelectAll = function () {

                if (self.DonationCart.IsActive()) {
                    self.DonationCart.Reset();
                    self.GiftBoxUserAmount(null);
                    self.DonationCart.GiftBoxType(RCHolidayConfig.GiftBoxTypes.Undefined);
                };

                $('input[type=checkbox]').prop("checked", true);
                setUserDefinedGiftBoxAmount(200);
                $('.selectAll, .ps-test-add').addClass('active');
                self.UpdateChart(200);

                if (self.IsMobile) {
                    $('#mobileTooltip').fadeIn('slow');
                };

                var tooltipPre = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePre);
                var tooltipPost = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePost);

                var donationItemMessage = tooltipPre[6] + ' <span class="tooltip-normal">' + tooltipPost[6] + '</span>';

                $('.donationMsg').html(donationItemMessage);


                self.DonationCart.DonationItemsCollection.Items().forEach(function (donationItem) {
                    dataLayer.push({
                        "event": "holiday-event-add-all",
                        "eventAction": "additem-all",
                        "eventLabel": donationItem.codeName,
                        "eventValue": donationItem.price
                    });
                });


                dataLayer.push({
                    "event": "holiday-event-select-all"
                });

            };

            self.ToolTipMessage = function (donationItem, itemsInCart) {

                if (self.IsMobile) {
                    $('#mobileTooltip').fadeIn('slow');
                };

                $('.user-defined-amount').empty();

                var itemTooltip = donationItem.tooltip;
                var tooltipPre = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePre);
                var tooltipPost = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePost);

                var donationItemMessage = tooltipPre[itemsInCart] + ' <span class="tooltip-normal">' + itemTooltip + '</span> <span class="tooltip-red">' + tooltipPost[itemsInCart] + '</span>';

                $('.donationMsg').html(donationItemMessage);

                if (itemsInCart === 0) {
                    $('.donationMsg').empty();
                    $('#mobileTooltip').css('display', 'none');
                };

            };

            self.GiftBoxUserAmountTooltip = function(usergivenamount){
                $('.donationMsg').empty();

                if(self.IsMobile) {
                    self.openTooltip();
                 };

                var tooltipUserAmountPre = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipUserDefinedAmountPre);
                var tooltipUserAmountPost = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipUserDefinedAmountPost);

                var tooltipPre = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePre);
                var tooltipPost = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.TooltipDonationMessagePost);

                if ( usergivenamount  <=  10 ){
                    $('.user-defined-amount').html(tooltipUserAmountPre['micro'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['micro']);
                }

                if (usergivenamount  >= 11 && usergivenamount  <= 25 ){
                    $('.user-defined-amount').html(tooltipUserAmountPre['small'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['small']);
                }

                if (usergivenamount  > 25 && usergivenamount  <= 100 ){
                    $('.user-defined-amount').html(tooltipUserAmountPre['medium'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['medium']);
                }

                if (usergivenamount  > 100 && usergivenamount  <= 200 ){
                    $('.user-defined-amount').html(tooltipUserAmountPre['large'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['large']);
                }

                if ( self.getVersion('version') === 2 && usergivenamount  === 200){

                    var  donationItemMessage =  tooltipPre[6] + ' <span class="tooltip-normal">' +   tooltipPost[6] + '</span>';
                    $('.user-defined-amount').html(donationItemMessage);
                }

                if ( self.getVersion('version') === 2 && usergivenamount  > 200){
                    $('.user-defined-amount').html(tooltipUserAmountPre['grand'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['grand']);
                }

                if (self.getVersion('version') === 3 && usergivenamount  > 200){
                    console.log("hi")
                    $('.user-defined-amount').html(tooltipUserAmountPre['nonpsGrand'] + '<span class=tooltip-normal> ' +  tooltipUserAmountPost['nonpsGrand']);
                }
                
            };

            self.calculateWidth = function (userAmount) {
                var progressbarFill = Math.ceil(self.DonationCart.Total() / 200 * 100);

                if (progressbarFill !== 0) {
                    $('.mobile-progress-instruction').addClass('hide');
                    self.mobileProgress(progressbarFill);
                }
                else {
                    $('.mobileProgressBar').empty();
                    $('.mobile-progress-instruction').removeClass('hide');
                }
                return { width: progressbarFill + '%' };

            };

            self.mobileProgress = function (progressbarFill) {


                var mobileProgressTooltip = RCHolidayUtils.ResolveCultureString(RCHolidayUIStrings.MobileTooltips);

                if (progressbarFill > 0 && progressbarFill < 100) {
                    $('.mobileProgressBar').html(mobileProgressTooltip['small'] + ' ' + progressbarFill + '% full');
                }

                if (progressbarFill === 100) {
                    $('.mobileProgressBar').html(mobileProgressTooltip['medium']);
                }

                if (progressbarFill > 100) {
                    $('.mobileProgressBar').html(mobileProgressTooltip['large']);
                }
            };
        }
        self.OpenLightbox = function () {
            dataLayer.push({
                "event": "donation-type-modal"
            });

        };
        self.closeTooltip = function () {
            $('.donationMsg, .user-defined-amount').empty();
            $('.mobile-tooltip').fadeOut('slow');
        }
        self.closeMobileTooltip = function() {
           $('.mobile-tooltip').fadeOut('slow');
        };
        self.showPin = function (donationItem) {
            var pin = ".pin-" + donationItem.id;
            $(pin).addClass('show');
        }
        self.hidePin = function (donationItem) {
            var pin = ".pin-" + donationItem.id;
            $(pin).removeClass('show');
        }

    };
}(S));

