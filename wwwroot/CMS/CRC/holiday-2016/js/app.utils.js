var RCHolidayUtils = (function(detect) {

    'use strict';

    return {
        IsMobile: function() {

            var ua = detect.parse(navigator.userAgent);
            return (RCHolidayGlobal.ua.device.type === "Mobile" || RCHolidayGlobal.ua.device.type === "Tablet" || RCHolidayGlobal.ua.os.family === "Android") ? true : false;
        },
        GetScrollingDirection: function(event) {},
        ResolveCultureString: function(cultureStringObj, cultureCode) {

            var value = '',
                culture = cultureCode || RCHolidayConfig.CurrentCulture;

            if(_.isObject(cultureStringObj) && cultureStringObj.hasOwnProperty(culture)) {
                value = cultureStringObj[culture] || 'Culture String Not Found';
            }
            else {
                value = cultureStringObj;
            }

            return value;

        },
        FormatCurrency: function (value) {

            var pattern = RCHolidayConfig.CurrencyFormat[RCHolidayConfig.CurrentCulture];
            return numeral(value).format(pattern);

        }

    }

})(detect);
