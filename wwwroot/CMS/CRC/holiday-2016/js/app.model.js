var RCHolidayModel = (function() {

    'use strict';

    return new function() {

        function Campaign(dataObj) {

            var self = this,
                campaignData = _.extend({}, dataObj);

            //
            // public members
            //


            self.id = null;
            self.path = null;
            self.problemHeader = null;
            self.problemSubheader = null;
            self.problemBody = null;
            self.solutionHeader = null;
            self.solutionSubHeader = null;
            self.donateHeader = null;
            self.donateSubHeader = null;

            //
            // private methods
            //

            function init() {

                self.id = campaignData['id'];
                self.path = campaignData['path'];
                self.problemHeader = campaignData['problemHeader'];
                self.problemSubheader = campaignData['problemSubheader'];
                self.problemBody = campaignData['problemBody'];
                self.solutionHeader = campaignData['solutionHeader'];
                self.solutionSubHeader = campaignData['solutionSubHeader'];
                self.donateHeader = campaignData['donateHeader'];
                self.donatePSTestHeader = campaignData['donatePSTestHeader'];
            }

            // initialize self
            init();

        }

        function CampaignCollection(data) {

            var self = this;

            //
            // public members
            //

            self.Items = [];

            self.GetById = function(id) {
                var campaign = _.findWhere(self.Items, { id: id });
                return (campaign) ? campaign : null;
            };

            //
            // private methods
            //

            function init() {

                var campaignData = data;
                _.forEach(campaignData, function(obj) {

                    var campaign = new Campaign(obj);
                    self.Items.push(campaign);

                });

            }

            // initialize self
            init();
        }

        function DonationItem(dataObj) {

            var self = this,
                itemData = _.extend({}, dataObj);

            //
            // public members
            //

            self.id = null;
            self.codeName = null;
            self.name = null;
            self.price = null;
            self.type = null;
            self.donationMessage = null;
            self.defaultImage = null;
            self.activeImage = null;

            self.price = 0;
            self.count = ko.observable(0);

            self.tooltip = null;



            //
            // private methods
            //

            function init() {

                self.id = itemData['id'];
                self.codeName = itemData['codeName'];
                self.name = RCHolidayUtils.ResolveCultureString(itemData['name']);
                self.price = itemData['price'];
                self.type = itemData['type'];
                self.donationMessage = RCHolidayUtils.ResolveCultureString(itemData['donationMessage']);
                self.defaultImage = itemData['defaultImage'];
                self.activeImage = itemData['activeImage'];
                self.tooltip = RCHolidayUtils.ResolveCultureString(itemData['tooltip']);


            }

            // init self
            init();

        }

        function DonationItemCollection(data) {

            var self = this;

            //
            // public members
            //

            self.Items = ko.observableArray([]);

            self.GetById = function(id) {

                var campaign = _.findWhere(self.Items, { id: id });
                return (campaign) ? campaign : null;

            };

            self.Reset = function() {

                _.forEach(self.Items(), function(item) {

                    item.count(0);

                });

            }

            //
            // private methods
            //

            function init() {

                var donationItemsData = data;
                _.forEach(donationItemsData, function(obj) {

                    var donationItem = new DonationItem(obj);
                    self.Items.push(donationItem);

                });

            }

            // initialize self
            init();

        }

        function DonationCart(opts) {

            var self = this,
                settings = _.extend({
                    donationItemsData: []
                }, opts);

            //
            // public members
            //

            self.DonationItemsCollection = new DonationItemCollection(settings.donationItemsData);

            self.GiftBoxTotal = ko.observable(0);

            self.GiftBoxType = ko.observable(RCHolidayConfig.GiftBoxTypes.Undefined);

            self.DomesticTotal = ko.observable(0);

            self.InternationalTotal = ko.observable(0);

            self.GiftBoxDomesticTotal = ko.computed(function() {

                var total = self.GiftBoxTotal() / 2;
                return total;

            });

            self.GiftBoxInternationalTotal = ko.computed(function() {

                var total = self.GiftBoxTotal() / 2;
                return total;

            });

            self.Total = ko.computed(function() {

                var itemTotal = 0,
                    donationTotal = 0,
                    domesticTotal = 0,
                    internationalTotal = 0;

                _.forEach(self.DonationItemsCollection.Items(), function(item) {

                    itemTotal = item.count() * item.price;

                    if(item.type === 'domestic') {

                        domesticTotal += itemTotal;

                    }
                    else if(item.type === 'international') {

                        internationalTotal += itemTotal;

                    }

                    donationTotal += itemTotal;

                });

                donationTotal += self.GiftBoxTotal();

                self.DomesticTotal(domesticTotal + self.GiftBoxDomesticTotal());
                self.InternationalTotal(internationalTotal + self.GiftBoxInternationalTotal());

                return (!_.isNaN(donationTotal)) ? donationTotal : 0;

            });


            self.GiftBoxIsActive = ko.computed(function() {
                return (self.GiftBoxTotal() > 0);
            });

            self.IsActive = ko.computed(function() {

                var isActive = ( self.Total() > 0 );
                return (isActive);

            });

            self.AddItem = function(donationItem, callback) {

                if(!donationItem) return;

                if(donationItem && donationItem.count() < RCHolidayConfig.MaxCartItemCount) {
                    increaseItemCount(donationItem);
                }
            };

            self.RemoveItem = function(donationItem, callback) {

                if(!donationItem) return;

                if(donationItem && donationItem.count() > 0) {
                    decreaseItemCount(donationItem);
                }

            };

            self.AddPseudoItem = function(donationItem, callback) {
                if(!donationItem) return;

                if(donationItem && donationItem.count() < 1 ) {
                    increaseItemCount(donationItem);
                }
                else if (donationItem && donationItem.count() === 1) {
                    decreaseItemCount(donationItem);
                }

            };


            self.ItemInCart = function(codeName) {
                return false;
            };

            self.Reset = function() {

                self.DonationItemsCollection.Reset();
                self.GiftBoxTotal(0);
                RCHolidayPresenter.ResetDonationCartState();
            };

            //
            // private methods
            //

            function getItemById(id) {
                return _.findWhere(self.DonationItemsCollection.Items(), { id: id });
            }

            function increaseItemCount(item) {

                if(item) {
                    var newCount = item.count() + 1;
                    item.count(newCount);
                }

            }

            function decreaseItemCount(item) {

                if(item) {
                    var newCount = item.count() - 1;
                    item.count(newCount);
                }

            }

        }

        


        return {
            Campaign: Campaign,
            CampaignCollection: CampaignCollection,
            DonationItem: DonationItem,
            DonationItemCollection: DonationItemCollection,
            DonationCart: DonationCart
        }
    };

}());