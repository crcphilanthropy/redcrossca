/**
 * Routes Samples
 * --------------
 * #/{campaign_id}/problem
 * #/{campaign_id}/solution
 * #/{campaign_id}/donate
 *
 * campaing_id = unique identifier that will control the copy on selected UI elements
 *
 * Sample Routes
 * -------------
 * #/giving-tuesday/problem
 * #/giving-tuesday/solution
 * #/giving-tuesday/donate
 */

var RCHolidayRouter = (function(StringJS) {

    'use strict';

    return new function() {

        var self = this,
            routeSections = RCHolidayConfig.NavigationSections[RCHolidayConfig.CurrentCulture],
            defaultSection = routeSections[0],
            campaignRoutes = [],
            routeTypes = { Section: 'Section', Campaign: 'Campaign', Root: 'Root', Invalid: 'Invalid' };


        function registerCampaignRoutes(campaignsCollection) {

            _.forEach(campaignsCollection, function(campaign) {
                campaignRoutes.push(campaign['id']);
            });

        }

        // Get and Validate Route Type
        function getRoute(hashSegments) {

            var segmentsCount = hashSegments.length,
                route = {
                    Type: null,
                    Section: null,
                    Campaign: null
                };

            if(segmentsCount === 1) {

                var segment = hashSegments[0];

                if(isRootRoute(segment)) {

                    route.Type = routeTypes.Root;
                    route.Section = defaultSection;
                }
                else if(isSectionRoute(segment)) {

                    route.Type = routeTypes.Section;
                    route.Section = _.findWhere(routeSections, { path: segment });

                }
                else if(isCampaignRoute(segment)) {

                    route.Type = routeTypes.Campaign;
                    route.Section = defaultSection;
                    route.Campaign = segment;

                }
                else {

                    // routeType is invalid
                    route.Type = routeTypes.Root;
                    route.Section = defaultSection;

                }

            }
            else if(segmentsCount === 2) {

                var campaign = hashSegments[0],
                    section = hashSegments[1],
                    isCampaign = isCampaignRoute(campaign),
                    isSection = isSectionRoute(section);

                if(isCampaign && isSection) {

                    route.Type = routeTypes.Campaign;
                    route.Campaign = campaign;
                    route.Section = _.findWhere(routeSections, { path: section });

                }
                else if(isCampaign && !(isSection)) {

                    route.Type = routeTypes.Campaign;
                    route.Campaign = campaign;
                    route.Section = defaultSection;
                }
                else {

                    route.Type = routeTypes.Root;
                    route.Section = defaultSection;
                }

            }
            else {

                // routeType is invalid
                route.Type = routeTypes.Root;
                route.Section = defaultSection;

            }

            return route;

        }

        function handleInitRoute() {

            // get hash
            var hashSegments = hasher.getHashAsArray();

            // identify route type - section or campaign
            var route = getRoute(hashSegments);
            updateCurrentRoute(route);

            self.SectionIndex = getSectionIndex(route.Section.id);

            // validate route type
            switch (route.Type) {
                case routeTypes.Root:
                    setDefaultRoute(route.Section);
                    break;
                case routeTypes.Section:
                    setSectionRoute(route.Section);
                    break;
                case routeTypes.Campaign:
                    setCampaignRoute(route.Campaign, route.Section);
                    break;
                default:
                    setDefaultRoute();
                    break;
            }
        }

        function handleRouteChange(hash) {

            var hashSegments = hasher.getHashAsArray(),
                route = getRoute(hashSegments),
                lastRoute = self.Route,
                routeChangeType = getRouteChangeType(lastRoute.Type, route.Type);

            updateCurrentRoute(route);

            console.log('Route Changed');
            console.log('routeChangeType: ', routeChangeType);

            if(routeChangeType !== null) {
                window.location.reload();
            }
        }

        function isRootRoute(hashSegment) {
            return !!(hashSegment === '' || hashSegment === '/');
        }

        function isSectionRoute(section) {

            var results = _.findWhere(routeSections, { path: section });
            return (results) ? true : false;
        }

        function isCampaignRoute(campaign) {

            return _.includes(campaignRoutes, campaign);

        }

        function setDefaultRoute(section) {
            setSilentRouteByPath(section.path);
        }

        function setSectionRoute(section) {
            setSilentRouteByPath(section.path);
        }

        function setCampaignRoute(campaign, section) {
            var routeTemplate = campaign + '/' + section.path;
            setSilentRouteByPath(routeTemplate);
        }

        function getSectionIndex(sectionId) {
            return _.findIndex(routeSections, { id: sectionId }) + 1;
        }

        function getRouteChangeType(lastRouteType, newRouteType) {

            var routeChangeType = null;

            if(lastRouteType !== routeTypes.Campaign && newRouteType === routeTypes.Campaign) {
                routeChangeType = routeTypes.Campaign;
            }
            else if(lastRouteType !== routeTypes.Section && newRouteType === routeTypes.Section) {
                routeChangeType = routeTypes.Section;
            }
            else if(lastRouteType !== routeTypes.Root && newRouteType === routeTypes.Root) {
                routeChangeType = routeTypes.Invalid;
            }

            return routeChangeType;

        }

        function updateCurrentRoute(route) {

            self.Route.Type = route.Type;
            self.Route.Campaign = route.Campaign;
            self.Route.Section = route.Section;

        }

        function setSilentRouteByPath(path) {
            hasher.changed.active = false;
            hasher.setHash(path);
            hasher.changed.active = true;
        }

        // ---------------- //

        self.Route = {
            Type: null,
            Campaign: null,
            Section: null
        };

        self.SectionIndex = 1;

        self.Events = {
            HandleRoute: new signals.Signal()
        };

        self.GetRouteById = function(routeId) {

            var section = _.findWhere(routeSections, { id: routeId });
            return (section) ? section : defaultSection;

        };

        self.SetRoute = function(route) {

            hasher.setHash(route);

        };

        self.SetSilentRoute = function(sectionId) {

            var hashSegments = hasher.getHashAsArray(),
                section = _.findWhere(routeSections, { id: sectionId }) || defaultSection,
                route = null;

            if(hashSegments.length === 1) {

                hashSegments[0] = section.path;

            }
            else if(hashSegments.length === 2) {

                hashSegments[1] = section.path;

            }

            route = hashSegments.join('/');

            hasher.changed.active = false;
            hasher.setHash(route);
            hasher.changed.active = true;

        };

        self.GetDonateUrl = function(opts) {

            var params = _.extend(
                    {
                        donationType: 'self',
                        donationTotal: 0,
                        domesticTotalSplit: 0,
                        internationalTotalSplit: 0
                    }, opts),

                donationType = RCHolidayConfig.DonationTypes[params.donationType] || RCHolidayConfig.DonationTypes.self,

                // string concatenation
                donationSplit = S('{{dom}},{{int}}').template({ dom: params.domesticTotalSplit , int: params.internationalTotalSplit }).s,
                urlValues = _.extend(
                    {
                        'amount': params.donationTotal,
                        'donationSplit': donationSplit
                    }, donationType.values[RCHolidayConfig.CurrentCulture]);

            return S(donationType.urlTemplate).template(urlValues).s;
        };

        self.GetSectionFromScrollPosition = function(scrollPosition) {

            var sectionIndex = Math.floor(scrollPosition / RCHolidayGlobal.mobileDeviceHeight),
                sectionCount = routeSections.length,
                section = null;

            if(sectionIndex >= sectionCount) {
                sectionIndex = sectionCount - 1;
            }

            section = routeSections[sectionIndex];

            return (section) ? section : null;

        };

        self.Init = function(opts) {

            var params = _.extend({
                    campaignsCollection: [],
                    onComplete: null
                }, opts);

            registerCampaignRoutes(params.campaignsCollection);

            hasher.changed.add(handleRouteChange);
            hasher.initialized.add(handleInitRoute);
            hasher.prependHash = '';
            hasher.init();

            if(_.isFunction(params.onComplete)) {
                params.onComplete();
            }

        };
    };

}(S));