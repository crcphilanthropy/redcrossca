var RCHolidayPresenter = (function($, skrollr){

    'use strict';

    return new function() {

        var self = this,
            velocity = $.Velocity,
            // _skrollr = null,
            $appView = $('body'),
            $logo = $appView.find('#logo'),
            $preloader = $appView.find('#preloader');

        // function animateLoadingScreenIn(callback) {

        //     $('body').removeClass('loading');

        //     velocity.RunSequence([
        //         {
        //             elements: $logo,
        //             properties: {
        //                 top: 200
        //             },
        //             options: {
        //                 duration: 500
        //             }
        //         },
        //         {
        //             elements: $preloader,
        //             properties: {
        //                 top: 400
        //             },
        //             sequenceQueue: false,
        //             duration: 500,
        //             delay: 500,
        //             options: {
        //                 complete: function() {

        //                     if(callback && _.isFunction(callback))
        //                         callback();

        //                 }
        //             }
        //         }
        //     ]);

        // }

        // function animateLoadingScreenOut(callback) {

        //     var $header = $('#global-header');

        //     velocity.RunSequence([
        //         {
        //             elements: $preloader,
        //             properties: {
        //                 opacity: 0
        //             },
        //             options: {
        //                 duration: 500
        //             }
        //         },
        //         {
        //             elements: $logo,
        //             properties: {
        //                 top: 5,
        //                 left: 15,
        //                 width: 100,
        //                 height: 100,
        //                 marginLeft: 0
        //             }
        //         },
        //         {
        //             elements: $header,
        //             properties: {
        //                 height: 0
        //             },
        //             options: {
        //                 sequenceQueue: false,
        //                 complete: function() {

        //                     $preloader.remove();
        //                     $('.master-elem').addClass('show');

        //                     if(callback && _.isFunction(callback))
        //                         callback();

        //                 }
        //             }
        //         }
        //     ]);

        // }

        // function initializeSkrollr() {
        //     _skrollr = skrollr.init();
        // }

        function getScrollTopFromSection(section, isMobile) {

            var sectionKeyFrame = _.find(RCHolidayConfig.SectionKeyFrames, { Section: section.id }),
                scrollTop = 0,
                keyFrameType = (isMobile) ? 'Mobile' : 'Desktop';

            if(sectionKeyFrame) {

                var keyFrame = sectionKeyFrame[keyFrameType],
                    screenHeight = (!isMobile) ? window.innerHeight : RCHolidayGlobal.mobileDeviceHeight;

                if(!isMobile) {
                    scrollTop = keyFrame * screenHeight / 100;
                }
                else {
                    scrollTop = keyFrame * screenHeight / 100;
                }
            }

            return scrollTop;
        }

        function setScrollTop(route) {

           var section = route['Section'],
               scrollTop = getScrollTopFromSection(section);

           // _skrollr.setScrollTop(scrollTop);
        }

        // function initializeParallax() {

        //     $('.parallax-viewport').parallax({
        //         relativeInput: true,
        //         invertX: true,
        //         invertY: false
        //     });

        // }

        function resetDonationCartState() {

            resetGiftBoxPresetAmounts();

        }

        function resetGiftBoxPresetAmounts() {

            var $presetGiftBoxAmounts = $appView.find('.gift-box-amounts li.active');
            $presetGiftBoxAmounts.removeClass('active');

        }

        function animateToScreen(screenIndex, isMobile, callback) {

            var section = { id: RCHolidayConfig.SectionKeyFrames[screenIndex - 1]['Section'] },
                scrollPos = getScrollTopFromSection(section, isMobile);

            if(!isMobile) {

                $('html').velocity('stop').velocity('scroll', {
                    offset: scrollPos, duration: 1500, complete: function () {

                        if (callback && _.isFunction(callback))
                            callback(section);

                    }
                });
            }
            else {

                $('body').animate({
                        scrollTop: scrollPos,
                        duration: 1500
                    },
                    {
                        complete: function() {

                            if (callback && _.isFunction(callback))
                                callback(section);

                        }
                    });

            }

        }

        function gotoScreen(screenIndex, isMobile, callback) {

            var section = { id: RCHolidayConfig.SectionKeyFrames[screenIndex - 1]['Section'] },
                scrollPos = getScrollTopFromSection(section, isMobile);

            $(window).scrollTop(scrollPos);

            if(callback && _.isFunction(callback))
                callback(section);

        }

        // function updateSlideSize(clientWidth, clientHeight) {

        //     var originalWidth = 698,
        //         originalHeight = 798,
        //         panelWidth = clientWidth / 2,
        //         imageOversizeScale = 1.05,
        //         landscape = { width: 0, height: 0 },
        //         portrait = { width: 0, height: 0 },
        //         newWidth = 0,
        //         newHeight = 0;

        //     landscape.width = Math.ceil((panelWidth) * imageOversizeScale);
        //     landscape.height = Math.ceil((originalHeight / originalWidth) * landscape.width);

        //     portrait.height = Math.ceil(clientHeight * imageOversizeScale);
        //     portrait.width = Math.ceil((originalWidth / originalHeight) * portrait.height);


        //     if(landscape.width >= panelWidth && landscape.height >= clientHeight) {

        //         // landscape
        //         newWidth = landscape.width;
        //         newHeight = landscape.height;

        //     }
        //     else {

        //         // portrait
        //         newWidth = portrait.width;
        //         newHeight = portrait.height;


        //     }

        //     return {
        //         width: newWidth,
        //         height: newHeight
        //     };

        // }

        // function updateSlide($elements) {

        //     var width = window.innerWidth || document.body.clientHeight,
        //         height = window.innerHeight || document.body.clientWidth;

        //     $elements.each(function() {

        //         var $elem = $(this),
        //             slideSize = updateSlideSize(width, height),
        //             elemCss = {};

        //         if($elem.hasClass('slide-left')) {
        //             elemCss.width = slideSize.width;
        //             elemCss.height = slideSize.height;
        //         }

        //         if($elem.hasClass('slide-right')) {
        //             elemCss.width = slideSize.width;
        //             elemCss.height = slideSize.height;
        //         }

        //         $elem.css(elemCss);

        //     });

        // }

        return {
            // AnimateLoadingScreenIn: animateLoadingScreenIn,
            // AnimateLoadingScreenOut: animateLoadingScreenOut,
            // InitializeSkrollr: initializeSkrollr,
            // InitializeParallax: initializeParallax,
            SetScrollTop: setScrollTop,
            ResetDonationCartState: resetDonationCartState,
            ResetGiftBoxPresetAmounts: resetGiftBoxPresetAmounts,
            AnimateToScreen: animateToScreen,
            GotoScreen: gotoScreen,
            GetScrollTopFromSection: getScrollTopFromSection,
            // UpdateSlide: updateSlide
        }
    };

}(jQuery));