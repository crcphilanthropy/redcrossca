var RCHolidayUIStrings = (function() {

    'use strict';

    return {
        CurrencySymbol: {
            en: '$',
            fr: '$'
        },
        ShareText: {
            en: 'Share',
            fr: 'Partager'
        },
        DonationItemsInstructionText: {
            en: 'Simply select the items you want to send those in need:',
            fr: 'Choisissez les articles que vous souhaitez offrir aux personnes vulnérables :'
        },
        DonationItemsPSTestInstructionsText:{
            en: 'Simply select the gifts you’d like to send. Choose all six to deliver the most impact with a complete Red Cross Holiday Survival Kit.',
            fr: 'Sélectionnez les articles que vous souhaitez offrir. Choisissez les six pour faire cadeau d’une trousse de survie complète de la Croix-Rouge.'
        },
        DonationItemsPSTestInstructionsMobiileText:{
            en: 'Simply select the gifts you’d like to send. Choose all six to deliver the most impact with a complete Red Cross Holiday Survival Kit. Or enter an amount based on your own budget.',
            fr: 'Sélectionnez les articles que vous souhaitez offrir. Choisissez les six pour faire cadeau d’une trousse de survie complète de la Croix-Rouge.'
        },
        DonationItemsPSNonTestInstructionsText:{
            en: 'Simply select the items you want to send those in need:',
            fr: 'Choisissez les articles que vous souhaitez offrir aux personnes vulnérables:'
        },
        DonationItemsPSNonTestInstructionsMobileText:{
            en: 'Simply select the items you want to send those in need. The more gifts you choose, the more impact you can make!',
            fr: 'Simply select the items you want to send those in need. The more gifts you choose, the more impact you can make! Or enter an amount based on your own budget'
        },
        DonationItemsMobileItemsInstructionText:{
            en: 'Select Items:',
            fr: 'Sélectionnez les articles'
        },
        GiftBoxInstructionText: {
            en: 'Or choose your budget and we’ll put together a gift box for the most urgent needs both at home and abroad.',
            fr: 'Ou indiquez-nous votre budget et nous assemblerons un colis pour combler les besoins les plus urgents, ici et ailleurs dans le monde.'
        },
        GiftBoxSelectAmountText: {
            en: 'Select Amount:',
            fr: 'Choisissez le montant:'
        },
        GiftBoxSelectAmountTextPS: {
            en: 'Choose your own amount',
            fr: ''
        },
        GiftBoxSelectAllText: {
            en: 'Select All',
            fr: 'Sélectionner tout'
        },
        GiftBoxPlaceholderText: {
            en: 'Other',
            fr: 'Autre'
        },
        GiftBoxInactiveImageAltText: {
            en: 'Box with Supplies',
            fr:  'Colis de provisions'
        },
        GiftBoxActiveText: {
            en: 'Your generous gift will ease suffering in Canada and abroad, wherever the need is greatest.',
            fr: 'Votre don allègera les souffrances des personnes au Canada et à l’étranger, là où les besoins sont les plus pressants.'
        },
        GlobalSurvivalKit: {
            en: 'Global Survival Kit',
            fr: 'Trousse de survie universelle'
        },
        GlobalSurvivalKitFull: {
            en: 'Full',
            fr: 'Full'
        },
        ProgressBarInstructionText: {
            en: 'Thanks to our December match from BMO each gift you give will be matched! ',
            fr: 'Grâce à l’offre de don jumelé de BMO, tout ce que vous donnez en décembre sera doublé!',
        },
        ProgressBarAltText: {
            en: 'World Globe',
            fr:  'Globe du monde'
        },
        DonationCartYourDonationText: {
            en: 'Your Donation',
            fr: 'Votre don'
        },
        DonationCartResetText: {
            en: 'Reset',
            fr: 'Réinitialiser'
        },
        DonationCartMatchText:{
            en: 'December gifts are matched by BMO.',
            fr: 'En décembre, BMO égale les dons.' 
        },
        DonationCartMatchSubbHead:{
            en: 'That means your impact is DOUBLED!',
            fr: 'Ainsi, votre don vaut le DOUBLE!'
        },
        DonationItemTypeDomesticText: {
            en: 'In Canada',
            fr:  'Au Canada'
        },
        DonationItemTypeInternationalText: {
            en: 'International',
            fr: 'International'
        },
        DonationTypeHeader:{
            en: 'Choose how you\'ll give your gift today:',
            fr: 'Choisissez la façon de verser votre don:'
        },
        DonationTypeSelfText: {
            en: 'Donate in your own name or as a family or organization',
            fr: 'Don en votre propre nom ou au nom de votre famille ou d’une entreprise.'
        },
        DonationTypeOnBehalfText: {
            en: 'Donate on behalf of someone special and send them a printed card.',
            fr: 'Don au nom d’un être cher et lui envoyer une carte imprimée.'
        },
        DonationTypeOnBehalfEcard: {
            en: 'Donate on behalf of someone special and send them an electronic card.',
            fr: 'Don au nom d’un être cher et lui envoyer une carte électronique.'
        },
        DonationTypeTaxReturnText: {
            en: '* You’ll receive a tax receipt in your name with each option',
            fr: '* Vous recevrez un reçu fiscal peu importe l’option choisie.'
        },
        CompleteDonationText: {
            en: 'Complete Donation',
            fr: 'Effectuer Votre Don'
        },
        SkipAheadToDonateNowText: {
            en: 'Skip Ahead To Donate Now',
            fr: 'Aller directement au formulaire de don'
        },
        FooterCopyText: {
            en: '<a href="http://www.redcross.ca/donate/terms-and-conditions-holiday2015" target="_blank">These items symbolize products and services</a> that the Red Cross provides in Canada; services vary by location. While your inspired gift may not purchase the actual items and services selected during this holiday season, it will help where the need is greatest.<br />Charity Registration Number 119219814RR0001',
            fr: 'BMO Banque de Montréal versera un montant équivalant aux dons faits d’ici le 25 décembre 2016 jusqu’à une contribution maximale de 50 000 $.<br /> <a href="http://www.croixrouge.ca/faites-un-don/modalites-fete" target="_blank">Ces articles représentent des produits et services</a> offerts par la Croix-Rouge au Canada; les services varient selon les régions. Votre précieux don ne sera peut-être pas utilisé pour fournir les articles et services exacts que vous avez sélectionnés, mais il sera assurément mis à contribution pour répondre aux besoins les plus urgents.<br />Numéro d’enregistrement 119219814RR0001'
        },
        TooltipDonationMessagePre: {
            en: {
                1: 'Great choice!',
                2: 'These gifts go a long way!',
                3: 'Every gift improves lives!',
                4: 'You\'re making a real impact!',
                5: 'You\'re saving lives!',
                6: 'We couldn\'t do it without you!'  
            },
            fr: {
                1: 'Excellent choix!',
                2: 'Des cadeaux qui font du bien!',
                3: 'Chaque cadeau touche des vies!',
                4: 'Vos choix feront une réelle différence!',
                5: 'Vous sauvez des vies!',
                6: 'Nous ne pouvons rien sans vous!'

            }
        },
        TooltipDonationMessagePost: {
            en: {
                1: 'Would you like to add another gift to build a global Holiday Survival Kit?',
                2: 'Your Survival Kit is off to a great start! Want to keep going with another gift?',
                3: 'Want to add another gift to grow your kit even more?',
                4: 'Your Survival Kit is growing fast, keep it up!',
                5: 'You\'re one item away from completing a full Survival Kit that sends help all around the world!',
                6: 'You\'ve put together a full Red Cross Holiday Survival Kit that will make the maximum impact! '  
            },
            fr: {
                1: 'Souhaitez-vous ajouter un autre article pour créer une trousse de survie universelle?',
                2: 'C\'est bien parti pour votre trousse de survie. Envie d\'aller plus loin?',
                3: 'Pourquoi ne pas en toucher encore plus en ajoutant un autre article à votre trousse?',
                4: 'Votre trousse de survie se remplie. Poursuivez sur cette lancée!',
                5: 'Vous n\'êtes qu\'à un article de créer une trousse complète pour aider des gens du monde entier.',
                6: 'Vous avez assemblé une trousse de survie complète qui aura un profond impact dans la vie des gens.'
            }
        },
        TooltipUserDefinedAmountPre: {
            en: {
                'micro': 'Thank you!',
                'small': 'Your gift will go a long way!',
                'medium': 'You\'re making a real impact!',
                'large': 'You\'re saving lives!',
                'grand': 'Wow you\'ve filled more than one complete Holiday Survival Kit!'
            },
            fr: {
                'micro': 'Merci!',
                'small': 'Votre cadeau fera du bien!',
                'medium': 'Vos choix feront une réelle différence!',
                'large': 'Vous sauvez des vies!',
                'grand': 'Vous avez rempli une trousse de survie complète et plus encore!'
            }
        },
        TooltipUserDefinedAmountPost: {
            en: {
                'micro': 'Your gift helps meet the most urgent needs both at home and abroad.',
                'small': 'The Red Cross Holiday Survival Kit you\'re supporting meets urgent needs both at home and abroad.',
                'medium': 'With your support, we\'ll put together a Survival Kit for the most urgent needs both at home and abroad.',
                'large': 'We’ll put together a Survival Kit for the most urgent needs both at home and abroad.',
                'grand': 'That support will assist vulnerable people in Canada and abroad.'
            },
            fr: {
                'micro': 'Votre cadeau aidera à répondre aux besoins les plus urgents, ici et à l\’étranger.',
                'small': 'La trousse de survie de la Croix-Rouge que vous offrez pour les fêtes répondra aux besoins les plus urgents, ici et à l\'étranger.',
                'medium': 'Grâce à votre don, nous assemblerons une trousse de survie pour répondre aux besoins les plus urgents, ici et à l\'étranger.',
                'large': 'Nous assemblerons une trousse de survie pour répondre aux besoins les plus urgents, ici et à l\'étranger. ',
                'grand': 'Votre soutien permettra d\'aider les personnes vulnérables, ici et à l\'étranger.'
            }
        },
        MobileProgressInstruction:{
            en: 'Tap a gift to add it to your Holiday Survival Kit',
            fr: 'Tappex sur un article pour l\'ajouter à votre trousse de survie'
        },
        MobileTooltips:{
            en: {
                'small': 'Your Global Holiday Survival Kit is now',
                'medium': 'Your full Global Holiday Survival Kit is ready to send',
                'large': 'Fantastic! Your Global Holiday Survival Kit is beyond full!'
            },
            fr: {
                'small': 'Votre trousse de survie universelle des fêtes est remplie à',
                'medium': 'Votre trousse de survie universelle des fêtes est pleine et prête à être envoyée!',
                'large': 'Super! Votre trousse de survie universelle des fêtes est plus que pleine!'
            }
        }

    };

}());