import axios from 'axios';

const call = axios.create({
  // baseURL: `${BASE_URL}/api`,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

const handleErrors = (response) => {
  if (response.status !== 200) {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }

  return response.data;
};

export const all = arr => call.all(arr).then(axios.spread);

export const get = (path, data) => call.get(`${path}`, data).then(handleErrors);

export const post = (path, data) => call.post(`${path}`, data).then(handleErrors);
