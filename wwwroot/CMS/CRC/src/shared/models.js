// @flow

export const categoryModel = (key: string, category: Object) => ({
  key: key.toLowerCase(),
  label: category.CategoryDisplayName,
  value: `${category.CategoryID}_${category.CategoryName}`,
});

export const videoModel = (video: Object) => ({
  id: video.VideoDetailID,
  title: video.VideoTitle,
  description: video.VideoSummary,
  thumbnail: video.Thumbnail,
  alt: video.AltText,
  downloadLink: video.DownloadLink,
  pageLink: video.DetailsPageLink,
  showDownload: video.DownloadAvailable,
  videoLength: video.VideoLength,
  tagList: [
    ...video.Categories.map(category => ({
      key: 'categories',
      label: category.CategoryDisplayName,
      value: `${category.CategoryID}_${category.CategoryName}`,
    })),
    ...video.Regions.map(category => ({
      key: 'regions',
      label: category.CategoryDisplayName,
      value: `${category.CategoryID}_${category.CategoryName}`,
    })),
  ],
});

export const photoModel = (photo: Object) => ({
  id: photo.PhotoID,
  title: photo.Title,
  description: photo.Description,
  thumbnail: photo.Thumbnail,
  lowResImage: photo.Image,
  highResImage: photo.HighResImage,
  altText: photo.AltText,
  showDownload: photo.Download,
  credits: photo.PhotoCredits,
  tagList: [
    ...photo.Categories.map(category => ({
      key: 'categories',
      label: category.CategoryDisplayName,
      value: `${category.CategoryID}_${category.CategoryName}`,
    })),
    ...photo.Regions.map(category => ({
      key: 'regions',
      label: category.CategoryDisplayName,
      value: `${category.CategoryID}_${category.CategoryName}`,
    })),
  ],
});
