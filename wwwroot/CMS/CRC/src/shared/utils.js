export const secToMinutesAndSeconds = (sec) => {
  const minutes = Math.floor(sec / 60);
  const seconds = (sec % 60).toFixed(0);
  return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};

export const decodeHTML = (html) => {
  const element = document.createElement('textarea');
  element.innerHTML = html;
  return element.value;
};
