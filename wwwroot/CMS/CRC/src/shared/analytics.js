// @flow
export const gtmPush = (event: string, data: { [key: string]: string }) => {
  const dataLayer = window.dataLayer;
  if (Array.isArray(dataLayer) && !Array.isArray(data) && typeof data === 'object') {
    dataLayer.push({ event, ...data });
  }
};
