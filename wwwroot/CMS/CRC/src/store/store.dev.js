// @flow
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import rootReducer from 'reducers';

const getMiddlewares = () => {
  const middlewares = [thunk, createLogger];

  return middlewares;
};

const getEnhancers = () => {
  const enhancers = [];

  const devToolsExtension =
    typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function'
      ? window.__REDUX_DEVTOOLS_EXTENSION__
      : undefined;

  if (devToolsExtension !== undefined) {
    enhancers.push(devToolsExtension());
  }

  return enhancers;
};

export default (history: {}, preloadedState: any = {}) => {
  const enhancers = getEnhancers();
  const middlewares = getMiddlewares(history);
  const store: any = createStore(
    rootReducer,
    preloadedState,
    compose(applyMiddleware(...middlewares), ...enhancers),
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const reducers = require('../reducers').default;
      store.replaceReducer(reducers(store.asyncReducers));
    });
  }

  return store;
};
