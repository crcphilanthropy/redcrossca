// @flow
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from 'reducers';

const getMiddlewares = () => {
  const middlewares = [thunk];

  return middlewares;
};

export default (history: {}, preloadedState: any = {}) => {
  const middlewares = getMiddlewares(history);
  const store: any = createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(...middlewares)
  );

  return store;
};
