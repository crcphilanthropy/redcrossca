// @flow
const moduleExport: any = __PROD__
  ? require('./store.prod').default
  : require('./store.dev').default;

export default moduleExport;
