// @flow
import { connect } from 'react-redux';
import { actions } from 'reducers/video';

import VideosForm from './videos-for-media-form';

const mapDispatchtoProps = {
  postData: actions.postData,
  downloadVideo: actions.downloadVideo,
};

const mapStateToProps = ({ video }) => ({
  ready: video.ready,
});

export default connect(mapStateToProps, mapDispatchtoProps)(VideosForm);
