import $ from 'jquery';
import React from 'react';
import cookie from 'js-cookie';
import PropTypes from 'prop-types';

import DownloadForm from 'containers/download-form';
import FormInput from 'components/form-input';

import { gtmPush } from 'shared/analytics';

const COOKIE_NAME = 'CRC_MediaVideos';
const COOKIE_AGE = 60 * 60 * 24 * 7; // 1 week

class VideosForm extends React.Component {
  state = {
    formInput: {
      formName: null,
      formEmail: null,
      formPublication: null,
    },
  };

  componentWillMount() {
    if (this.checkhasSubmitted()) {
      this.setState({
        hasCookies: true,
      });
    }
  }

  componentDidMount() {
    const { postData, cultureCode, videoId } = this.props;
    const { hasCookies } = this.state;

    if (hasCookies) {
      const formData = cookie.get(COOKIE_NAME);
      if (formData !== undefined) {
        postData({ ...JSON.parse(formData), DetailID: videoId, Culture: cultureCode });
      }
    }
  }

  _onFormDownloadClick = (e) => {
    e.preventDefault();

    const { formInput } = this.state;
    const { videoId, postData, cultureCode } = this.props;

    const formData = {
      Name: formInput.formName,
      Email: formInput.formEmail,
      Publication: formInput.formPublication,
    };

    postData({ ...formData, DetailID: videoId, Culture: cultureCode }).then(() => {
      // write to cookie/localStorage
      cookie.set(COOKIE_NAME, JSON.stringify(formData), {
        expires: COOKIE_AGE,
      });

      gtmPush('e_downloadFormSubmit', { mediaType: 'videos' });

      // force re-render
      this.setState({
        hasCookies: true,
      });

      const container = this.container;

      if (container !== null) {
        const top = $(container).offset().top;
        $('html, body').animate({ scrollTop: top }, 2000);
      }
    });
  };
  checkhasSubmitted() {
    return cookie.get(COOKIE_NAME) !== undefined;
  }

  fieldsHasValue() {
    const { formInput } = this.state;
    return Object.keys(formInput).every(key => formInput[key] !== null);
  }

  _onFormInputChange = (name, value) => {
    const formObject = {
      [name]: value === '' ? null : value,
    };

    this.setState({
      formInput: {
        ...this.state.formInput,
        ...formObject,
      },
    });
  };

  _onVideoDownload = (e) => {
    e.preventDefault();
    const { downloadVideo, pageTitle } = this.props;

    gtmPush('e_videosDownload', {
      videoName: pageTitle,
    });

    downloadVideo(pageTitle);
  };

  renderDownloadForm() {
    const {
      title,
      description,
      footerDescription,
      submitText,
      successMessage,
      successButtonText,
      nameLabel,
      namePlaceholder,
      nameError,
      emailLabel,
      emailPlaceholder,
      emailError,
      publicationLabel,
      publicationPlaceholder,
      publicationError,
      ready,
    } = this.props;

    const { hasCookies } = this.state;
    let props = {
      title,
      description,
      footerDescription,
      submitContainerClassName: 'columns medium-3',
      submitText,
      onSubmitClick: this._onFormDownloadClick,
      inputContainerClassName: 'row',
      successMessage,
      successClickText: successButtonText,
      showSuccessClick: ready,
      onSuccessClick: this._onVideoDownload,
      disableButton: !this.fieldsHasValue(),
    };

    // Add cookie property
    if (hasCookies) {
      props = {
        ...props,
        showSuccessView: true,
      };
    }

    return (
      <DownloadForm {...props}>
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formName'}
          type={'text'}
          required
          title={nameLabel}
          placeholder={namePlaceholder}
          onChange={this._onFormInputChange}
          error={nameError}
          pattern={"^[a-zA-Z''-'\\s]{1,40}$"}
        />
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formEmail'}
          type={'email'}
          required
          title={emailLabel}
          placeholder={emailPlaceholder}
          onChange={this._onFormInputChange}
          error={emailError}
          pattern={'^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'}
        />
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formPublication'}
          type={'text'}
          required
          title={publicationLabel}
          placeholder={publicationPlaceholder}
          onChange={this._onFormInputChange}
          error={publicationError}
          pattern={"^[a-zA-Z0-9''-'\\s]{1,40}$"}
        />
      </DownloadForm>
    );
  }

  render() {
    return (
      <div
        ref={(element) => {
          // $FlowFixMe
          this.container = element;
        }}
      >
        {this.renderDownloadForm()}
      </div>
    );
  }
}

DownloadForm.propTypes = {
  pageTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  footerDescription: PropTypes.string.isRequired,
  inputContainerClassName: PropTypes.string.isRequired,
  submitContainerClassName: PropTypes.string.isRequired,
  submitText: PropTypes.string.isRequired,
  successMessage: PropTypes.string.isRequired,
  successClickText: PropTypes.string.isRequired,
  showSuccessView: PropTypes.bool,
  onSubmitClick: PropTypes.func.isRequired,
  onSuccessClick: PropTypes.func.isRequired,
};

FormInput.propTypes = {
  containerClassName: PropTypes.string.isRequired,
  fieldName: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default VideosForm;
