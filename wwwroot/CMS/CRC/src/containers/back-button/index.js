// @flow
import React from 'react';
import classnames from 'classnames';
import type { Props } from './type';

import style from './style.scss';
const getPreviousUrl = () => localStorage.getItem('previousUrl');

const BackButton = ({ text }: Props) => {
  const previousUrl = getPreviousUrl(history);

  if (previousUrl === null || previousUrl === '') {
    return null;
  }

  return (
    <div className={style.container}>
      <a className={classnames('button', style.button)} href={previousUrl}>
        {text}
      </a>
    </div>
  );
};

export default BackButton;
