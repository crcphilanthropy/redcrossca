// @flow

export type BrowserHistory = {
  push: (obj: Object) => void,
  location: {
    search: string,
    pathname: string,
    state: {
      [key: string]: any
    }
  },
  listen: () => void
};

export type Props = {
  text: string,
  history: BrowserHistory
};
