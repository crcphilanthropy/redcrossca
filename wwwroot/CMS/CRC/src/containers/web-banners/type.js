// @flow
import type { ThunkAction } from 'reducers/banners/type';

export type Props = {
  ready: boolean,
  cultureCode: string,
  title: string,
  description: string,
  footerDescription: string,
  submitText: string,
  successMessage: string,
  companyNameLabel: string,
  companyNamePlaceholder: string,
  companyNameError: string,
  getCodesDescription: string,
  scrollToTopText: string,
  embedLabel: string,
  trackingLabel: string,
  bannerSets: Array<Object>,
  fetchData: () => ThunkAction,
  postData: () => ThunkAction,
};

export type State = {
  formInput: {
    [key: string]: any,
  },
  hasCookies?: boolean,
};
