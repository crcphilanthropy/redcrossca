// @flow
import { connect } from 'react-redux';
import { actions } from 'reducers/banners';

import WebBanners from './web-banners';

const mapDispatchtoProps = {
  fetchData: actions.fetchData,
  postData: actions.postData,
};

const mapStateToProps = ({ banners }) => ({
  ready: banners.ready,
  bannerSets: banners.list,
});

export default connect(mapStateToProps, mapDispatchtoProps)(WebBanners);
