// @flow
import $ from 'jquery';
import React, { Component } from 'react';
import cookie from 'js-cookie';

import BannerSet from 'components/banner-set';
import DownloadForm from 'containers/download-form';
import FormInput from 'components/form-input';
import Loading from 'components/loading';

import { gtmPush } from 'shared/analytics';
import type { Props, State } from './type';

const COOKIE_NAME = 'CRC_WebBannersSet';
const COOKIE_AGE = 60 * 60 * 24 * 7; // 1 week

class WebBanners extends Component<any, Props, State> {
  state = {
    formInput: {},
    hasCookies: undefined,
  };

  componentWillMount() {
    if (this.checkhasSubmitted()) {
      this.setState({
        hasCookies: true,
      });
    }
  }

  componentDidMount() {
    const { fetchData, postData, cultureCode } = this.props;
    const { hasCookies } = this.state;

    if (!hasCookies) {
      fetchData(cultureCode);
      return;
    }

    // retrieve cookie name and do a post to get value
    const companyName = cookie.get(COOKIE_NAME);
    if (companyName !== undefined || companyName !== '') {
      postData(companyName, cultureCode);
    }
  }

  checkhasSubmitted() {
    return cookie.get(COOKIE_NAME) !== undefined;
  }

  fieldsHasValue() {
    const { formInput } = this.state;
    return Object.keys(formInput).some(key => formInput[key] !== null);
  }

  _onScrollToTop = (e: SyntheticInputEvent) => {
    e.preventDefault();

    // $FlowFixMe
    const container = this.container;

    if (container !== null) {
      const top = $(container).offset().top;
      $('html, body').animate({ scrollTop: top }, 1000);
    }
  };

  _onFormDownloadClick = (e: SyntheticInputEvent) => {
    e.preventDefault();

    const { formInput } = this.state;
    const { postData, cultureCode } = this.props;

    postData(`"${formInput.formName}"`, cultureCode).then(() => {
      // write to cookie/localStorage
      cookie.set(COOKIE_NAME, JSON.stringify(formInput.formName), {
        expires: COOKIE_AGE,
      });

      gtmPush('e_webBannersFormSubmit', { companyName: formInput.formName });

      // force re-render
      this.setState({
        hasCookies: true,
      });
    });
  };

  _onFormInputChange = (name: string, value: string) => {
    const formObject = {
      [name]: value === '' ? null : value,
    };

    this.setState({
      formInput: {
        ...this.state.formInput,
        ...formObject,
      },
    });
  };

  renderDownloadForm() {
    const {
      title,
      description,
      footerDescription,
      submitText,
      successMessage,
      companyNameLabel,
      companyNamePlaceholder,
      companyNameError,
    } = this.props;

    const { hasCookies } = this.state;
    let props = {
      title,
      description,
      footerDescription,
      submitContainerClassName: 'columns medium-6',
      submitText,
      onSubmitClick: this._onFormDownloadClick,
      inputContainerClassName: 'row',
      successMessage,
      disableButton: !this.fieldsHasValue(),
    };

    // Add cookie property
    if (hasCookies) {
      props = {
        ...props,
        showSuccessView: true,
      };
    }

    return (
      <DownloadForm {...props}>
        <FormInput
          containerClassName={'columns medium-6'}
          fieldName={'formName'}
          type={'text'}
          required
          title={companyNameLabel}
          placeholder={companyNamePlaceholder}
          onChange={this._onFormInputChange}
          error={companyNameError}
          pattern={"^[a-zA-Z0-9''-'\\s]{1,40}$"}
        />
      </DownloadForm>
    );
  }

  renderListing() {
    const {
      bannerSets,
      getCodesDescription,
      scrollToTopText,
      embedLabel,
      trackingLabel,
    } = this.props;
    const { hasCookies } = this.state;

    return (
      <div className="row">
        <div className="columns small-12">
          {bannerSets.map((bannerSet) => {
            let props = {
              key: bannerSet.BannerSetID,
              details: bannerSet,
              scrollToTop: this._onScrollToTop,
              getCodesDescription,
              scrollToTopText,
              embedLabel,
              trackingLabel,
            };

            if (hasCookies) {
              props = {
                ...props,
                cookie: true,
              };
            }
            return <BannerSet {...props} />;
          })}
        </div>
      </div>
    );
  }

  render() {
    const { ready } = this.props;

    if (!ready) {
      return <Loading />;
    }

    return (
      <div
        ref={(element) => {
          // $FlowFixMe
          this.container = element;
        }}
      >
        {this.renderDownloadForm()}
        {this.renderListing()}
      </div>
    );
  }
}

export default WebBanners;
