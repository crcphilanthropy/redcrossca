import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { videoModel } from 'shared/models';

import VideoItem from 'components/video-item';
import style from './style.scss';

const selectSingleTag = (key, { value }, listingUrl) => {
  window.location = `${listingUrl}?${key}=${value}`;
};

const RelatedItemsContainer = (props) => {
  const { list: relatedList, listingUrl, heading, showMoreText, showLessText, tagsTitle } = props;
  const formattedList = relatedList.map(videoModel);

  return (
    <div className={style['related-videos-container']}>
      <h2>
        {heading}
      </h2>
      <div className={style['related-list']}>
        {formattedList.map(item =>
          <VideoItem
            {...item}
            key={item.id}
            showMoreText={showMoreText}
            showLessText={showLessText}
            tagsTitle={tagsTitle}
            containerClassName={style[classnames('media-item')]}
            onClickTag={(key, selected) => selectSingleTag(key, selected, listingUrl)}
          />,
        )}
      </div>
    </div>
  );
};

RelatedItemsContainer.propTypes = {
  list: PropTypes.arrayOf(
    React.PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
      thumbnail: PropTypes.string,
      showDownload: PropTypes.string,
      videoLength: PropTypes.string,
      tagList: PropTypes.string,
    }),
  ).isRequired,
  listingUrl: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  showMoreText: PropTypes.string.isRequired,
  showLessText: PropTypes.string.isRequired,
  tagsTitle: PropTypes.string.isRequired,
};

export default RelatedItemsContainer;
