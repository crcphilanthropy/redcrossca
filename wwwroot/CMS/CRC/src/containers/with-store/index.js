import React from 'react';
import { Provider } from 'react-redux';
import history from 'store/history';
import configureStore from 'store/configureStore';
import hoistStatics from 'hoist-non-react-statics';

const initialState = window.__INITIAL_STATE__ || {};
const store = configureStore(history, initialState);

const withStore = (WrappedComponent) => {
  const C = props =>
    <Provider store={store}>
      <WrappedComponent {...props} />
    </Provider>;

  C.displayName = `withStore(${WrappedComponent.displayName || WrappedComponent.name})`;

  return hoistStatics(C, WrappedComponent);
};

export default withStore;
