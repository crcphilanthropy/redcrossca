// @flow
import { connect } from 'react-redux';
import { actions as videoActions } from 'reducers/video';
import { actions as categoryActions } from 'reducers/category';

import type { Props } from './type';

import { regionsFilterSelector, categoriesFilterSelector } from './selector';

import VideosContainer from './videos-container';

const mapDispatchtoProps = {
  videoFetch: videoActions.fetchData,
  categoryFetch: categoryActions.fetchData,
};

const mapStateToProps = (state, props: Props) => ({
  ready: state.video.ready,
  videoList: state.video.list,
  filtersReady: state.category.ready,
  filters: {
    categories: categoriesFilterSelector(state, props),
    regions: regionsFilterSelector(state, props),
  },
});

export default connect(mapStateToProps, mapDispatchtoProps)(VideosContainer);
