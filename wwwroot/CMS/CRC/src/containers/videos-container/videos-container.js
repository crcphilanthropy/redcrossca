// @flow
import React, { Component } from 'react';

import MediaContainer from 'containers/media-container';
import VideoItem from 'components/video-item';
import Loading from 'components/loading';

import { gtmPush } from 'shared/analytics';
import type { Props } from './type';

class VideosContainer extends Component<void, Props, void> {
  componentDidMount() {
    const { categoryFetch, videoFetch, path, hideSummary, cultureCode } = this.props;
    if (path !== undefined) {
      Promise.all([videoFetch(path, cultureCode, { hideSummary }), categoryFetch(cultureCode)]);
    }
  }

  _onFilterChangeAnalytics = (key: string, value: string) => {
    const { filterChangeAnalyticsEvent } = this.props;
    gtmPush(filterChangeAnalyticsEvent, {
      filterType: key,
      filterValue: value,
    });
  };

  _onTagClickAnaltyics = (videoName: string, tagName: string) => {
    const { tagClickAnalyticsEvent } = this.props;
    gtmPush(tagClickAnalyticsEvent, {
      videoName,
      tagName,
    });
  };

  renderListing() {
    const {
      history,
      pageSize,
      videoList,
      filters,
      filtersReady,
      filterButtonText,
      showMoreText,
      showLessText,
      tagsTitle,
    } = this.props;

    const props = {
      history,
      pageSize,
      list: videoList,
      filters,
      filtersReady,
      filterButtonText,
      showMoreText,
      showLessText,
      tagsTitle,
      MediaItemComponent: VideoItem,
      filterChangeAnalytics: this._onFilterChangeAnalytics,
      tagClickAnalytics: this._onTagClickAnaltyics,
    };

    return <MediaContainer {...props} />;
  }

  render() {
    const { ready, filtersReady } = this.props;

    if (!ready || !filtersReady) {
      return <Loading />;
    }

    return this.renderListing();
  }
}

export default VideosContainer;
