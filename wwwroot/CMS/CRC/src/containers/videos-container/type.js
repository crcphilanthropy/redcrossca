// @flow
import type { ThunkAction } from 'reducers/video/type';

export type BrowserHistory = {
  push: (obj: Object) => void,
  location: {
    search: string,
    pathname: string,
  },
  listen: () => void,
};

export type Props = {
  +path: string,
  +cultureCode: string,
  ready: boolean,
  filtersReady: boolean,
  filterButtonText: string,
  history: BrowserHistory,
  pageSize: number,
  hideSummary: boolean,
  showMoreText: string,
  showLessText: string,
  tagsTitle: string,
  videoList: Array<Object>,
  filters: {
    [key: string]: Object,
  },
  videoFetch: (path: string, cultureCode: string) => ThunkAction,
  categoryFetch: (cultureCode: string) => ThunkAction,
  filterChangeAnalyticsEvent: string,
  tagClickAnalyticsEvent: string,
};
