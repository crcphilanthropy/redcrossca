// @flow
import { connect } from 'react-redux';
import { actions as photoActions } from 'reducers/photo';
import { actions as categoryActions } from 'reducers/category';

import type { Props } from './type';

import { regionsFilterSelector, categoriesFilterSelector } from './selector';

import PhotosContainer from './photos-container';

const mapDispatchtoProps = {
  photoFetch: photoActions.fetchData,
  photoPost: photoActions.postData,
  categoryFetch: categoryActions.fetchData,
};

const mapStateToProps = (state, props: Props) => ({
  ready: state.photo.ready,
  photoList: state.photo.list,
  filtersReady: state.category.ready,
  filters: {
    categories: categoriesFilterSelector(state, props),
    regions: regionsFilterSelector(state, props),
  },
});

export default connect(mapStateToProps, mapDispatchtoProps)(PhotosContainer);
