// @flow
import $ from 'jquery';
import React, { Component } from 'react';
import cookie from 'js-cookie';
import classnames from 'classnames';

import FormInput from 'components/form-input';
import DownloadForm from 'containers/download-form';
import MediaContainer from 'containers/media-container';
import PhotoItem from 'components/photo-item';
import Loading from 'components/loading';

import { gtmPush } from 'shared/analytics';
import type { Props, State, PostData } from './type';

const COOKIE_NAME = 'CRC_Photos';
const COOKIE_AGE = 60 * 60 * 24 * 7; // 1 week

class PhotosContainer extends Component<void, Props, State> {
  state = {
    formInput: {
      formName: null,
      formEmail: null,
      formPublication: null,
    },
    hasCookies: undefined,
  };

  componentWillMount() {
    if (this.checkhasSubmitted()) {
      this.setState({
        hasCookies: true,
      });
    }
  }
  componentDidMount() {
    const { categoryFetch, photoFetch, photoPost, cultureCode } = this.props;
    const { hasCookies } = this.state;

    if (!hasCookies) {
      Promise.all([photoFetch(cultureCode), categoryFetch(cultureCode)]);
    }

    // retrieve cookie name and do a post to get a value
    const formData = cookie.get(COOKIE_NAME);
    if (formData !== undefined) {
      Promise.all([photoPost(formData), categoryFetch(cultureCode)]);
    }
  }

  checkhasSubmitted() {
    return cookie.get(COOKIE_NAME) !== undefined;
  }

  fieldsHasValue() {
    const { formInput } = this.state;

    return Object.keys(formInput).every(key => formInput[key] !== null);
  }

  _onFormInputChange = (name: string, value: string) => {
    const formObject = {
      [name]: value === '' ? null : value,
    };

    this.setState({
      formInput: {
        ...this.state.formInput,
        ...formObject,
      },
    });
  };

  _onScrollToTop = (e: SyntheticInputEvent) => {
    e.preventDefault();

    // $FlowFixMe
    const container = this.container;

    if (container !== null) {
      const top = $(container).offset().top;
      $('html, body').animate({ scrollTop: top }, 1000);
    }
  };

  _onFilterChangeAnalytics = (key: string, value: string) => {
    gtmPush('e_imagesFilter', {
      filterType: key,
      filterValue: value,
    });
  };

  _onTagClickAnaltyics = (imageName: string, tagName: string) => {
    gtmPush('e_imagesTagClick', {
      imageName,
      tagName,
    });
  };

  _onFormDownloadClick = (e: SyntheticInputEvent) => {
    e.preventDefault();

    const { photoPost } = this.props;
    const { formInput } = this.state;

    const formData: PostData = {
      Name: formInput.formName,
      Email: formInput.formEmail,
      Publication: formInput.formPublication,
      Culture: this.props.cultureCode,
    };

    photoPost(formData).then(() => {
      // write to cookie/localStorage
      cookie.set(COOKIE_NAME, JSON.stringify(formData), {
        expires: COOKIE_AGE,
      });

      gtmPush('e_downloadFormSubmit', { mediaType: 'images' });

      // force re-render
      this.setState({
        hasCookies: true,
      });
    });

    const container = this.container;

    if (container !== null) {
      const top = $(container).offset().top;
      $('html, body').animate({ scrollTop: top }, 2000);
    }
  };

  renderDownloadForm() {
    const {
      title,
      description,
      footerDescription,
      submitText,
      successMessage,
      nameLabel,
      namePlaceholder,
      nameError,
      emailLabel,
      emailPlaceholder,
      emailError,
      publicationLabel,
      publicationPlaceholder,
      publicationError,
    } = this.props;

    const { hasCookies } = this.state;
    let props = {
      title,
      description,
      footerDescription,
      submitContainerClassName: classnames('columns medium-3'),
      submitText,
      onSubmitClick: this._onFormDownloadClick,
      inputContainerClassName: 'row',
      successMessage,
      disableButton: !this.fieldsHasValue(),
    };

    // Add cookie property
    if (hasCookies) {
      props = {
        ...props,
        showSuccessView: true,
      };
    }

    return (
      <DownloadForm {...props}>
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formName'}
          type={'text'}
          required
          title={nameLabel}
          placeholder={namePlaceholder}
          onChange={this._onFormInputChange}
          error={nameError}
          pattern={"^[a-zA-Z''-'\\s]{1,40}$"}
        />
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formEmail'}
          type={'email'}
          required
          title={emailLabel}
          placeholder={emailPlaceholder}
          onChange={this._onFormInputChange}
          error={emailError}
          pattern={'^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'}
        />
        <FormInput
          containerClassName={'columns medium-3'}
          fieldName={'formPublication'}
          type={'text'}
          required
          title={publicationLabel}
          placeholder={publicationPlaceholder}
          onChange={this._onFormInputChange}
          error={publicationError}
          pattern={"^[a-zA-Z0-9''-'\\s]{1,40}$"}
        />
      </DownloadForm>
    );
  }

  renderListing() {
    const {
      history,
      pageSize,
      photoList,
      filters,
      filtersReady,
      filterButtonText,
      showMoreText,
      showLessText,
      photoCredits,
      downloadPhotoTitle,
      tagsTitle,
      fillForm,
      highRes,
      lowRes,
    } = this.props;

    const { hasCookies } = this.state;

    let props = {
      history,
      pageSize,
      list: photoList,
      filters,
      filtersReady,
      filterButtonText,
      MediaItemComponent: PhotoItem,
      showMoreText,
      showLessText,
      photoCredits,
      downloadPhotoTitle,
      tagsTitle,
      fillForm,
      highRes,
      lowRes,
      scrollToTop: this._onScrollToTop,
      filterChangeAnalytics: this._onFilterChangeAnalytics,
      tagClickAnalytics: this._onTagClickAnaltyics,
    };

    if (hasCookies) {
      props = {
        ...props,
        hasCookies: true,
      };
    }

    return <MediaContainer {...props} />;
  }

  render() {
    const { ready, filtersReady } = this.props;

    if (!ready || !filtersReady) {
      return <Loading />;
    }

    return (
      <div
        ref={(element) => {
          // $FlowFixMe
          this.container = element;
        }}
      >
        {this.renderDownloadForm()}
        {this.renderListing()}
      </div>
    );
  }
}

export default PhotosContainer;
