// @flow
import { createSelector } from 'reselect';

const categoriesSelector = ({ category }) => category.data;
const categoriesPropsSelector = (state, props) => props;

/**
 * Get Regions Filter
 */
export const regionsFilterSelector = createSelector(
  categoriesSelector,
  categoriesPropsSelector,
  (categories, { regionLabel, regionPlaceholder }) => ({
    label: regionLabel,
    placeholder: regionPlaceholder,
    data: categories.reduce((source, category) => {
      if (category.key === 'regions') {
        source.push({
          value: category.value,
          label: category.label,
        });
      }

      return source;
    }, []),
  }),
);

/**
 * Get Categories Filter
 */
export const categoriesFilterSelector = createSelector(
  categoriesSelector,
  categoriesPropsSelector,
  (categories, { categoryLabel, categoryPlaceholder }) => ({
    label: categoryLabel,
    placeholder: categoryPlaceholder,
    data: categories.reduce((source, category) => {
      if (category.key === 'categories') {
        source.push({
          value: category.value,
          label: category.label,
        });
      }

      return source;
    }, []),
  }),
);
