// @flow
import type { ThunkAction } from 'reducers/photo/type';
export type { PostData } from 'reducers/photo/type';

export type BrowserHistory = {
  push: (obj: Object) => void,
  location: {
    search: string,
    pathname: string,
  },
  listen: () => void,
};

export type Props = {
  ready: boolean,
  filtersReady: boolean,
  history: BrowserHistory,
  pageSize: number,
  photoList: Array<Object>,
  filters: {
    [key: string]: Object,
  },
  +cultureCode: string,
  title: string,
  description: string,
  footerDescription: string,
  submitText: string,
  successMessage: string,
  nameLabel: string,
  namePlaceholder: string,
  nameError: string,
  emailLabel: string,
  emailPlaceholder: string,
  emailError: string,
  publicationLabel: string,
  publicationPlaceholder: string,
  publicationError: string,
  showMoreText: string,
  showLessText: string,
  filterButtonText: string,
  photoCredits: string,
  downloadPhotoTitle: string,
  tagsTitle: string,
  fillForm: string,
  highRes: string,
  lowRes: string,
  photoFetch: (cultureCode: string) => ThunkAction,
  photoPost: () => ThunkAction,
  categoryFetch: (cultureCode: string) => ThunkAction,
};

export type State = {
  hasCookies?: boolean | null,
  formInput: {
    formName: string | null,
    formEmail: string | null,
    formPublication: string | null,
  },
};
