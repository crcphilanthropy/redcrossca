// @flow
import type { Children } from 'react';

export type Props = {
  children?: Children,
  title: string,
  description: string,
  footerDescription?: string,
  inputContainerClassName?: string,
  submitContainerClassName?: string,
  containerClassName?: string,
  submitText: string,
  successMessage: string,
  showSuccessView?: boolean,
  disableButton?: boolean,
  onSubmitClick: (e: SyntheticInputEvent, formData: Object) => void,
  showSuccessClick?: boolean,
  successClickText?: string,
  onSuccessClick?: (e: Event) => void,
};
