// @flow
import React, { PureComponent } from 'react';
import classnames from 'classnames';
import style from './style.scss';
import type { Props } from './type';

class DownloadForm extends PureComponent<void, Props, void> {
  defaultProps = {
    showSuccessView: false,
    submitText: 'Submit',
    disableButton: false,
    showSuccessClick: false,
    successClickText: 'Success',
  };

  componentDidMount() {
    this.removeValidationTooltip();
  }

  removeValidationTooltip() {
    const inputs = document.querySelectorAll('.download-form input');

    for (let i = 0; i < inputs.length; i += 1) {
      inputs[i].addEventListener('invalid', (e) => {
        e.preventDefault();
      });
    }
  }

  renderSuccess() {
    const { successMessage, onSuccessClick, successClickText, showSuccessClick } = this.props;
    return (
      <div className={style['form-success']}>
        <p>
          {successMessage}
        </p>
        {showSuccessClick
          ? <a onClick={onSuccessClick}>
            <input className={style.btn} type="submit" value={successClickText} />
          </a>
          : null}
      </div>
    );
  }

  renderForm() {
    const {
      description,
      footerDescription,
      containerClassName,
      inputContainerClassName,
      submitContainerClassName,
      submitText,
      onSubmitClick,
      children,
      disableButton,
    } = this.props;

    return (
      <div className={containerClassName}>
        <div className={style.description}>
          {description}
        </div>
        <form
          className={classnames(style['photos-form'], 'download-form')}
          onSubmit={onSubmitClick}
        >
          <div className={inputContainerClassName}>
            <div className={style.fields}>
              {children}
              <div className={submitContainerClassName}>
                <button
                  className={classnames(style.btn, 'button')}
                  type="submit"
                  disabled={disableButton}
                >
                  {submitText}
                </button>
              </div>
            </div>
          </div>
        </form>
        <div className={style['footer-description']}>
          {footerDescription}
        </div>
      </div>
    );
  }

  render() {
    const { title, showSuccessView } = this.props;
    return (
      <div className={style['photos-form-container']}>
        <h3>
          {title}
        </h3>
        <hr />
        {(showSuccessView === undefined ? false : showSuccessView)
          ? this.renderSuccess()
          : this.renderForm()}
      </div>
    );
  }
}

export default DownloadForm;
