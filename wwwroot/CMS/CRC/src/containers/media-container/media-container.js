// @flow
import $ from 'jquery';
import React, { Component } from 'react';
import classnames from 'classnames';
import queryString from 'query-string';

import Pagination from 'components/pagination';
import Filters from 'components/filters';

import type { Media, Props, QueryStringObject, SelectOption } from './type';

import style from './style.scss';

class MediaContainer extends Component<any, Props, void> {
  componentDidMount() {
    // Initial update of tag list
    this.updateSelectedFilters();
  }

  getSearchObj(): QueryStringObject {
    const { history } = this.props;
    const search: string = history.location.search;
    const queryObj = queryString.parse(search);

    return Object.keys(queryObj).reduce(
      (source, key) => ({
        ...source,
        [key.toLowerCase()]: queryObj[key],
      }),
      {},
    );
  }

  getCurrentPage(): number {
    const searchObj = this.getSearchObj();
    return searchObj.page === undefined ? 1 : parseInt(searchObj.page, 0);
  }

  getParamsByKey(key: string) {
    const searchObj = this.getSearchObj();

    let params = searchObj[key];

    if (params === null || params === undefined || params === '') {
      return [];
    }

    // If there are multiple arrays, pick only the first array
    if (Array.isArray(params)) {
      params = params[0];
    }

    return params.split(',');
  }

  updateHistoryParams(key: string, values: Array<string>) {
    const searchObj = this.getSearchObj();

    const updatedSearchObj = {
      ...searchObj,
      [key.toLowerCase()]: values.length <= 0 ? [] : values.join(','),
    };

    this.push(updatedSearchObj);
  }

  updateSelectedFilters() {
    const { updateFilter } = this.props;
    const searchObj = this.getSearchObj();

    // loop through the search object and update the state of the selected keys
    const keys = Object.keys(searchObj);

    for (let x = 0; x < keys.length; x += 1) {
      const params = this.getParamsByKey(keys[x]);
      updateFilter(keys[x], params);
    }
  }

  updateValues(key: string, values: Array<string>): void {
    const { updateFilter } = this.props;

    this.updateHistoryParams(key, values);
    updateFilter(key, values);
  }

  push(searchObj: QueryStringObject = {}) {
    const { history } = this.props;
    history.push({
      search: queryString.stringify(searchObj, { encode: false }),
    });
  }

  _selectSingleTag = (key: string, { value, label }: SelectOption, mediaName: string): void => {
    // move page focus to the tag filter section so users can see which tag has been selected
    window.location.href = '#media-filter';
    const { setSingleFilter, tagClickAnalytics } = this.props;

    if (tagClickAnalytics !== undefined) {
      tagClickAnalytics(mediaName, label);
    }

    this.push({
      [key.toLowerCase()]: [value],
    });
    setSingleFilter(key, value);
  };

  _selectTag = (key: string, { value, label }: SelectOption): void => {
    const { filterChangeAnalytics } = this.props;
    const params = this.getParamsByKey(key);

    // if values not found
    if (params.indexOf(value) === -1) {
      // fire analytics
      if (filterChangeAnalytics !== undefined) {
        filterChangeAnalytics(key, label);
      }

      const values = [...params, value];

      this.updateValues(key, values);
      this.updateValues('page', ['1']); // reset page to 1
    }
  };

  _removeTag = (key: string, { value }: SelectOption): void => {
    const params = this.getParamsByKey(key);

    const values = params.filter(v => v !== value);

    this.updateValues(key, values);
    this.updateValues('page', ['1']); // reset page to 1
  };

  _pageChange = (page: Object): void => {
    $('html, body').animate({ scrollTop: 0 }, 0);
    const { updatePage } = this.props;
    const newPage = page.selected + 1;

    this.updateHistoryParams('Page', [newPage]);
    updatePage(newPage);
  };

  _clearButtonClick = (e: Event) => {
    e.preventDefault();
    e.stopPropagation();

    const { clearTags } = this.props;
    this.push();
    clearTags();
  };

  _pageLinkClick = () => {
    // e.preventDefault();

    const { history } = this.props;
    const currentUrl = history.createHref(history.location);

    localStorage.setItem('previousUrl', currentUrl);
  };

  renderFilters() {
    const { filters, filtersReady, tagList, filterButtonText } = this.props;

    if (!filtersReady) {
      return null;
    }

    return (
      <Filters
        filters={filters}
        tagList={tagList}
        clearFilterText={filterButtonText}
        onSelectTag={this._selectTag}
        onRemoveTag={this._removeTag}
        onClearButtonClick={this._clearButtonClick}
      />
    );
  }

  renderPagination() {
    const { pageCount } = this.props;

    if (pageCount <= 0) {
      return null;
    }

    let currentPage = this.getCurrentPage();

    // Check if current page is more than page count, select the first page
    if (currentPage > pageCount) {
      currentPage = 1;
    }

    return (
      <div className="row">
        <div className="columns small-12">
          <Pagination
            disableInitialCallback
            initialPage={currentPage - 1}
            pageRangeDisplayed={4}
            marginPagesDisplayed={2}
            pageCount={pageCount}
            onPageChange={this._pageChange}
          />
        </div>
      </div>
    );
  }

  renderList() {
    const {
      mediaList,
      MediaItemComponent,
      hasCookies,
      showMoreText,
      showLessText,
      scrollToTop,
      photoCredits,
      downloadPhotoTitle,
      tagsTitle,
      fillForm,
      highRes,
      lowRes,
    } = this.props;

    if (mediaList.length <= 0) {
      return null;
    }

    return (
      <div className="row">
        <div className="columns small-12">
          <div className={style['media-list']}>
            {mediaList.map((item: Media, idx) =>
              <MediaItemComponent
                {...item}
                key={item.id}
                containerClassName={classnames('media-item', {
                  end: mediaList.length === idx + 1,
                })}
                onClickTag={(key: string, selected: SelectOption) =>
                  this._selectSingleTag(key, selected, item.title)}
                onPageLinkClick={this._pageLinkClick}
                hasCookies={hasCookies}
                showLightbox
                showMoreText={showMoreText}
                showLessText={showLessText}
                onFillInFormClick={scrollToTop}
                photoCredits={photoCredits}
                downloadPhotoTitle={downloadPhotoTitle}
                tagsTitle={tagsTitle}
                fillForm={fillForm}
                highRes={highRes}
                lowRes={lowRes}
              />,
            )}
          </div>
        </div>
      </div>
    );
  }

  render() {
    // const { ready } = this.props;

    // if (!ready) {
    //   return <div>Loading</div>;
    // }

    return (
      <div>
        {this.renderFilters()}
        <div className="row">
          <div className="columns small-12">
            <hr />
          </div>
        </div>
        {this.renderList()}
        {this.renderPagination()}
      </div>
    );
  }
}

export default MediaContainer;
