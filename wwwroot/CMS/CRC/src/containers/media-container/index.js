// @flow
import { connect } from 'react-redux';
import { actions as filterActions } from 'reducers/filter';

import type { Props } from './type';

import {
  pagedViewSelector,
  tagListSelector,
  totalPageCountSelector
} from './selector';

import Media from './media-container';

const mapDispatchtoProps = {
  setSingleFilter: filterActions.setSingleFilter,
  updateFilter: filterActions.updateFilter,
  clearTags: filterActions.clearFilter,
  updatePage: filterActions.updatePage
};

const mapStateToProps = (state, props: Props) => ({
  // ready: props.list.length > 0,
  pageCount: totalPageCountSelector(state, props),
  mediaList: pagedViewSelector(state, props),
  tagList: tagListSelector(state)
});

export default connect(mapStateToProps, mapDispatchtoProps)(Media);
