// @flow
import type { ThunkAction } from 'reducers/filter/type';

export type QueryStringObject = { [key: string]: string | string[] };

export type SelectOption = {
  value: string,
  label: string,
};

export type BrowserHistory = {
  push: (obj: Object) => void,
  location: {
    search: string,
    pathname: string,
  },
  listen: () => void,
  createHref: (location: $PropertyType<BrowserHistory, 'location'>) => string,
};

export type Media = {
  id: string,
  showDownload?: boolean,
  containerClassName: string,
  thumbnail: string,
  title: string,
  description: string,
  subDescription?: string,
  tagList: Array<{
    key: string,
    label: string,
    value: string,
  }>,
};

export type Props = {
  history: BrowserHistory,
  mediaList: Array<Media>,
  filters: Object,
  filtersReady: boolean,
  filterButtonText: string,
  pageSize: number,
  pageCount: number,
  showMoreText: string,
  showLessText: string,
  hasCookies?: boolean,
  photoCredits: string,
  downloadPhotoTitle: string,
  tagsTitle: string,
  fillForm: string,
  highRes: string,
  lowRes: string,
  tagList: Array<{
    key: string,
    label: string,
    value: string,
  }>,
  scrollToTop: () => void,
  filterChangeAnalytics?: (key: string, value: string) => void,
  tagClickAnalytics?: (mediaName: string, tagName: string) => void,
  setSingleFilter: (key: string, id: string) => ThunkAction,
  updateFilter: (key: string, arrIds: Array<string>) => ThunkAction,
  clearTags: () => ThunkAction,
  updatePage: (pageNumber: string) => ThunkAction,
  +MediaItemComponent: typeof React$Component,
};
