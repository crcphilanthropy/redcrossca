// @flow
import { createSelector } from 'reselect';

const pageNumberSelector = ({ filter }) => {
  const pageFilter = filter.selectedFilters.find(f => f !== null && f.key === 'page');
  if (pageFilter === undefined) {
    return 1;
  }

  return pageFilter.value;
};

const selectedFiltersSelector = ({ filter }) => filter.selectedFilters;
const categoryDataSelector = ({ category }) => category.data;

const photosSelector = (state, { list }) => list;
const pageSizeSelector = (state, { pageSize }) => pageSize;

const getCategoryID = (value: string): number => {
  const splitValues = value.split('_');
  if (splitValues.length !== 2 && isNaN(splitValues[0])) {
    return -1;
  }

  return parseInt(splitValues[0], 10);
};

/**
 * Get Filtered list of photos and videos based on the categories and regions
 */
export const photoFilteredViewSelector = createSelector(
  photosSelector,
  selectedFiltersSelector,
  categoryDataSelector,
  (photos, selectedFilters, categories) => {
    const categoryKeys = [...new Set(categories.map(category => category.key))];

    selectedFilters = selectedFilters.filter(
      item => item !== null && categoryKeys.indexOf(item.key) !== -1,
    );
    // Splits the selected filter into categories and regions to make it easier to check & compare the tags
    const categoryFilters = selectedFilters.filter(item => item.key === 'categories');
    const regionFilters = selectedFilters.filter(item => item.key === 'regions');

    if (selectedFilters.length === 0) {
      return photos;
    }

    return photos.reduce((source, photo) => {
        /* Check to see if the tag list matches the selected filters.
        If filter contains categories and regions, then only photos/videos that matches at least 1 category and 1 region will be displayed. */
      if (categoryFilters.length > 0 && regionFilters.length > 0) {
        const hasCategoryMatch = photo.tagList.some(
               tag =>
                   categoryFilters.findIndex(
                     filter => filter.key === tag.key && filter.value === getCategoryID(tag.value),
                   ) !== -1,
            );

        const hasRegionMatch = photo.tagList.some(
               tag =>
                   regionFilters.findIndex(
                     filter => filter.key === tag.key && filter.value === getCategoryID(tag.value),
                   ) !== -1,
            );

        if (hasCategoryMatch && hasRegionMatch) {
          source.push(photo);
        }
      } else {
        const hasMatch = photo.tagList.some(
               tag =>
                   selectedFilters.findIndex(
                     filter => filter.key === tag.key && filter.value === getCategoryID(tag.value),
                   ) !== -1,
             );

        if (hasMatch) {
          source.push(photo);
        }
      }

      return source;
    }, []);
  },
);

/**
 * Get total page count
 */
export const totalPageCountSelector = createSelector(
  photoFilteredViewSelector,
  pageSizeSelector,
  (list, pageSize) => {
    const total = list.length;

    if (total <= pageSize) {
      return 0;
    }

    return (total - total % pageSize) / pageSize + (total % pageSize === 0 ? 0 : 1);
  },
);

/**
 * Filter by page
 */
export const pagedViewSelector = createSelector(
  photoFilteredViewSelector,
  pageNumberSelector,
  totalPageCountSelector,
  pageSizeSelector,
  (list, pageNumber, pageCount, pageSize) => {
    if (pageNumber > pageCount) {
      pageNumber = 1;
    }

    const index = pageNumber - 1;
    const startIndex = index * pageSize;
    const endIndex = startIndex + pageSize;

    return list.slice(startIndex, endIndex);
  },
);

/**
 * Get a list of selected filter
 */
export const tagListSelector = createSelector(
  selectedFiltersSelector,
  categoryDataSelector,
  (selectedFilters, categoryData) =>
    selectedFilters.reduce((source, filter) => {
      if (filter !== null) {
        const foundFilter = categoryData.find(
          category => category.key === filter.key && getCategoryID(category.value) === filter.value,
        );

        if (foundFilter !== undefined) {
          source.push(foundFilter);
        }
      }
      return source;
    }, []),
);
