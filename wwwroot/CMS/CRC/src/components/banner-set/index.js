// @flow
import React, { Component } from 'react';
import classnames from 'classnames';
import Masonry from 'react-masonry-component';
import { decodeHTML } from 'shared/utils';

import style from './style.scss';
import type { Props } from './type';

class BannerSet extends Component<any, Props, any> {
  handleFocus = (e: SyntheticInputEvent) => {
    e.target.select();
  };

  renderBanner() {
    const banners = this.props.details.Banners;
    const cookie = this.props.cookie;

    return banners.map((banner) => {
      const width = banner.BannerType;
      const bannerWidth = width.substr(0, width.lastIndexOf('*'));

      return (
        <div
          className={classnames(style.banner, `banner-${bannerWidth}`, 'banner-item')}
          key={banner.BannerID}
        >
          <p className={style['banner-title']}>
            {banner.Title}
          </p>
          <img src={banner.Image} alt="" />
          {cookie
            ? <div className={style.codes}>
              <label htmlFor={'crc-form-embed-code'} className={style.label}>
                {this.props.embedLabel} ({banner.Title})
              </label>
              <input
                id={'crc-form-embed-code'}
                type="text"
                className="embed-code"
                defaultValue={decodeHTML(banner.EmbedLink)}
              />
              <label htmlFor={'crc-form-tracking-code'} className={style.label}>
                {this.props.trackingLabel} ({banner.Title})
              </label>
              <input
                id={'crc-form-tracking-code'}
                type="text"
                className="tracking-code"
                defaultValue={decodeHTML(banner.TrackingCode)}
              />
            </div>
            : ''}
        </div>
      );
    });
  }

  renderGetEmbedLinksPrompt() {
    const { cookie, scrollToTop, getCodesDescription, scrollToTopText } = this.props;

    if (!cookie) {
      return (
        <p>
          {getCodesDescription} <a onClick={scrollToTop}>{scrollToTopText}</a>
        </p>
      );
    }

    return null;
  }

  render() {
    const { details } = this.props;
    // const banners = details.Banners;

    return (
      <div className={style['banner-set']}>
        <h2>
          {details.Title}
        </h2>
        <hr />
        <p>
          {details.Description}
        </p>
        {this.renderGetEmbedLinksPrompt()}
        <div className={classnames(style['banner-list'])}>
          <Masonry
            className={'banner-grid'}
            disableImagesLoaded={false}
            updateOnEachImageLoad={false}
          >
            {this.renderBanner()}
          </Masonry>
        </div>
      </div>
    );
  }
}

export default BannerSet;
