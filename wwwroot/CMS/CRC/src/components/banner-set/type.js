// @flow
export type Props = {
  details: {
    Title: string,
    Description: string,
    +Banners: Array<{
      BannerID: number,
      Title: string,
      Image: string,
      TrackingCode: string,
      EmbedLink: string,
      BannerType: string,
    }>,
  },
  embedLabel: string,
  trackingLabel: string,
  cookie: boolean,
  scrollToTop: (e: SyntheticInputEvent) => void,
  getCodesDescription: string,
  scrollToTopText: string,
};
