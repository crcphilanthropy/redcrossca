// @flow
import React, { PureComponent } from 'react';
import classnames from 'classnames';
import type { Props } from './type';
import style from './style.scss';

// const sortFilterList = (list: Array<Object>): Array<Object> => {
//  list.sort((a: Object, b: Object) => {
//    // Sort by Categories or Region First
//    if (a.type < b.type) {
//      return -1;
//    }

//    if (b.type > b.type) {
//      return 1;
//    }

//    // sort by label
//    if (a.value < b.value) {
//      return -1;
//    }

//    if (a.value > b.value) {
//      return 1;
//    }

//    return 0;
//  });

//  return list;
// };

class Tags extends PureComponent<any, Props, void> {
  static defaultProps = {
    text: '',
    className: '',
    tagClassName: style.tag,
    showCross: false,
    list: [],
  };

  renderText() {
    const { text, textClassName } = this.props;

    if (text === '') {
      return null;
    }

    return (
      <div className={textClassName}>
        {text}:
      </div>
    );
  }

  render() {
    const { list, showCross, className, tagClassName, onClickTag }: Props = this.props;

    if (list.length === 0) {
      return null;
    }

    const sortedList = list;

    return (
      <div className={classnames(className)}>
        {this.renderText()}
        <ul className={style.list}>
          {sortedList.map(({ key, value, label }) =>
            <li key={value}>
              <a
                className={classnames(tagClassName)}
                onClick={(e: Event) => {
                  e.stopPropagation();
                  e.preventDefault();
                  onClickTag(key, { value, label });
                }}
              >
                <span className={style.text}>
                  {label}
                </span>
                {showCross ? <span className={classnames(style.cross, 'fa', 'fa-times')} /> : null}
              </a>
            </li>,
          )}
        </ul>
      </div>
    );
  }
}

export default Tags;
