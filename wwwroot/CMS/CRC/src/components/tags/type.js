// @flow

export type Props = {
  text?: string,
  textClassName?: string,
  className?: string,
  tagClassName?: string,
  showCross: boolean,
  +list: Array<{
    key: string,
    label: string,
    value: string
  }>,
  +onClickTag: () => void
};
