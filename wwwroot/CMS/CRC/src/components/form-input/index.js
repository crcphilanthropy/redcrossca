// @flow
import React from 'react';
import type { Props } from './type';
import style from './style.scss';

class FormInput extends React.Component<any, Props, any> {
  static defaultProps = {
    required: false,
    type: 'text',
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      showErrors: false,
    };
  }

  formFieldChange = (e: Event) => {
    const { onChange } = this.props;
    const input = e.target;
    let valid = true;

    // $FlowFixMe
    if (!input.validity.valid || input.validity.valueMissing) {
      valid = false;
    }

    if (e.target instanceof HTMLInputElement) {
      const { name, value } = e.target;

      onChange(name, valid ? value : '');

      this.setState({
        showErrors: !valid,
      });
    }
  };

  render() {
    return (
      <div className={this.props.containerClassName}>
        <fieldset className={style['form-group']}>
          <label htmlFor={`crc-form-${this.props.fieldName}`}>
            <span>* </span>
            {this.props.title}
          </label>
          <input
            id={`crc-form-${this.props.fieldName}`}
            type={this.props.type}
            name={this.props.fieldName}
            required={this.props.required}
            value={this.props.initialValue}
            placeholder={this.props.placeholder}
            onChange={(e: Event) => this.formFieldChange(e, this.props)}
            pattern={this.props.pattern}
          />
          {this.state.showErrors
            ? <span className="error" aria-live="polite">
              {this.props.error}
            </span>
            : null}
        </fieldset>
      </div>
    );
  }
}

export default FormInput;
