// @flow

export type Props = {
  containerClassName?: string,
  +fieldName: string,
  +type: string,
  title?: string,
  placeholder?: string,
  required: boolean,
  initialValue?: string,
  error: string,
  +onChange: (name: string, value: string) => void,
};
