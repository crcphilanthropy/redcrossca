import React from 'react';
import style from './style.scss';

const Loading = () =>
  <div>
    <i className="fa fa-spinner fa-spin fa-3x fa-fw" />
    <span className={style['screen-reader']}>Loading...</span>
  </div>;

export default Loading;
