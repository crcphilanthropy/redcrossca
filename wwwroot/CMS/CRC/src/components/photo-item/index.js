// @flow
import React from 'react';
import classnames from 'classnames';
import download from 'downloadjs';

import Tags from 'components/tags';
import ShowMore from 'components/show-more';
import Lightbox from 'components/lightbox';
import globalStyle from 'scss/components/multimedia/media-item';

import { gtmPush } from 'shared/analytics';
import type { Props } from './type';
import style from './style.scss';

class PhotoItem extends React.Component<void, Props, any> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  analyticsPush(resolution: string) {
    const { title } = this.props;

    gtmPush('e_imagesDownload', {
      imageName: title,
      imageResolution: resolution,
    });
  }

  openLightbox = (e: Event) => {
    e.preventDefault();

    this.setState({
      isOpen: true,
    });
  };

  closeLightbox = (e: Event) => {
    e.preventDefault();

    this.setState({
      isOpen: false,
    });
  };

  handleKeyDown = (e: Event) => {
    if (e.keyCode === 27) {
      this.setState({
        isOpen: false,
      });
    }
  };

  _downloadFile = (e: Event, url: string) => {
    e.preventDefault();

    download(url);
  };

  renderLightbox() {
    if (this.props.showLightbox) {
      return (
        <div>
          <button onClick={this.openLightbox} className={style['lightbox-item']}>
            <img
              className={globalStyle.image}
              src={this.props.thumbnail}
              alt={this.props.altText}
            />
            <div className={style['zoom-icon']}>
              <i className={classnames('fa fa-search-plus')} aria-hidden="true" />
            </div>
          </button>
          {this.state.isOpen
            ? <Lightbox
              onClose={this.closeLightbox}
              ariaLabel={this.props.title}
              ariaDesc={this.props.description}
              onKeyDown={this.handleKeyDown}
            >
              <img src={this.props.lowResImage} alt={this.props.altText} />
            </Lightbox>
            : null}
        </div>
      );
    }

    return <img className={globalStyle.image} src={this.props.thumbnail} />;
  }

  renderCredits() {
    const { credits, photoCredits } = this.props;

    if (credits === undefined) {
      return null;
    }

    return (
      <div className={style.credits}>
        {photoCredits}: {credits}
      </div>
    );
  }

  renderDownloadLink() {
    const { lowResImage, highResImage, lowRes, highRes } = this.props;

    return (
      <div>
        {lowResImage !== null && lowResImage !== ''
          ? <a
            className={style['download-link']}
            onClick={() => {
                // Push to Analytics
              this.analyticsPush('Low Res');
              download(lowResImage);
            }}
          >
            {lowRes}
          </a>
          : null}
        {highResImage !== null && highResImage !== ''
          ? <a
            className={style['download-link']}
            onClick={() => {
                // Push to Analytics
              this.analyticsPush('High Res');
              download(highResImage);
            }}
          >
            {highRes}
          </a>
          : null}
      </div>
    );
  }

  renderDownload() {
    const { hasCookies, downloadPhotoTitle, fillForm, onFillInFormClick } = this.props;

    return (
      <div>
        <div className={style['download-text']}>
          {downloadPhotoTitle}:
        </div>
        {hasCookies !== undefined && hasCookies
          ? this.renderDownloadLink()
          : <a onClick={onFillInFormClick}>
            {fillForm} <i className="fa fa-angle-right" aria-hidden="true" />
          </a>}
      </div>
    );
  }

  renderDescription() {
    const { description, showMoreText, showLessText } = this.props;

    if (description === null || description === '') {
      return null;
    }

    /* eslint-disable react/no-danger */
    return (
      <div className={globalStyle.description}>
        <ShowMore showMoreText={showMoreText} showLessText={showLessText}>
          <div dangerouslySetInnerHTML={{ __html: description }} />
        </ShowMore>
      </div>
    );
    /* eslint-enable */
  }

  render() {
    return (
      <div className={classnames(globalStyle.wrapper, this.props.containerClassName)}>
        <div className={globalStyle.container}>
          <div>
            {this.renderLightbox()}
            <div className={globalStyle.information}>
              <div className={globalStyle.title}>
                {this.props.title}
              </div>
              {this.renderDescription()}
              {this.renderCredits()}
              {this.renderDownload()}
            </div>
          </div>
          <div className={globalStyle.tags}>
            <Tags
              text={this.props.tagsTitle}
              textClassName={globalStyle['tag-title']}
              tagClassName={globalStyle.tag}
              list={this.props.tagList}
              onClickTag={this.props.onClickTag}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PhotoItem;
