// @flow
import React, { Component } from 'react';

import type { Props, State } from './type';
import style from './style.scss';

class ShowMore extends Component<any, Props, State> {
  static defaultProps = {
    showMoreText: 'Show More',
    showLessText: 'Show Less'
  };

  state = {
    expand: false,
    hideShowMore: true
  };

  componentDidMount() {
    // $FlowFixMe
    if (this.contextElement.clientHeight >= 66) {
      //eslint-disable-next-line
      this.setState({
        hideShowMore: false
      });
    }
  }

  _onShowMore = (e: Event) => {
    e.preventDefault();

    this.setState({
      expand: !this.state.expand
    });
  };

  render() {
    const { children, showMoreText, showLessText } = this.props;
    const { expand, hideShowMore } = this.state;

    return (
      <div className={style.container}>
        <div className={style.context} style={{ height: expand ? 'auto' : 0 }}>
          <div
            ref={(elem) => {
              // $FlowFixMe
              this.contextElement = elem;
            }}
          >
            {children}
          </div>
        </div>
        {hideShowMore
          ? null
          : <a className={style.button} onClick={this._onShowMore}>
            {expand ? showLessText : showMoreText}
          </a>}
      </div>
    );
  }
}

export default ShowMore;
