// @flow
import type { Children } from 'react';

export type Props = {
  children?: Children,
  showMoreText?: string,
  showLessText?: string
};

export type State = {
  expand: boolean,
  hideShowMore: boolean
};
