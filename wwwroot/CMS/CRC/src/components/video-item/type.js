// @flow
export type Props = {
  showDownload?: boolean,
  containerClassName: string,
  +thumbnail: string,
  alt?: string,
  +title: string,
  +description: string,
  +pageLink: string,
  videoLength: number,
  credits?: string,
  tagList: Array<{
    key: string,
    label: string,
    value: string,
  }>,
  showMoreText: string,
  showLessText: string,
  tagsTitle: string,
  onClickTag?: (key: string, id: string) => void,
  onPageLinkClick?: (e: Event) => void,
};
