// @flow
import React from 'react';
import classnames from 'classnames';

import Tags from 'components/tags';
import ShowMore from 'components/show-more';
import { secToMinutesAndSeconds } from 'shared/utils';
import globalStyle from 'scss/components/multimedia/media-item';

import type { Props } from './type';
import style from './style.scss';

const renderDescription = (description: string, showMoreText: string, showLessText: string) => {
  if (description === null || description === '') {
    return null;
  }

  /* eslint-disable react/no-danger */
  return (
    <div className={globalStyle.description}>
      <ShowMore showMoreText={showMoreText} showLessText={showLessText}>
        <div dangerouslySetInnerHTML={{ __html: description }} />
      </ShowMore>
    </div>
  );
  /* eslint-enable */
};
const renderVideoLength = (videoLength) => {
  if (videoLength === undefined || videoLength === '') {
    return null;
  }

  return (
    <div className={style['video-length']}>
      {secToMinutesAndSeconds(videoLength)}
    </div>
  );
};

const renderImage = (thumbnail, alt, videoLength) => {
  if (thumbnail === undefined || thumbnail === '') {
    return null;
  }

  return (
    <div className={style['image-container']}>
      <img className={globalStyle.image} src={thumbnail} alt={alt} />
      {renderVideoLength(videoLength)}
    </div>
  );
};

const VideoItem = (props: Props) =>
  <div className={classnames(globalStyle.wrapper, props.containerClassName)}>
    <div className={globalStyle.container}>
      <div>
        <a href={props.pageLink} onClick={props.onPageLinkClick}>
          {renderImage(props.thumbnail, props.alt, props.videoLength)}
        </a>
        <div className={globalStyle.information}>
          <div className={globalStyle.title}>
            <a href={props.pageLink} onClick={props.onPageLinkClick}>
              {props.title}
            </a>
          </div>
          {renderDescription(props.description, props.showMoreText, props.showLessText)}
        </div>
      </div>
      <div className={globalStyle.tags}>
        <Tags
          text={props.tagsTitle}
          textClassName={globalStyle['tag-title']}
          tagClassName={globalStyle.tag}
          list={props.tagList}
          onClickTag={props.onClickTag}
        />
      </div>
    </div>
  </div>;

export default VideoItem;
