import React from 'react';
import PropTypes from 'prop-types';
import FocusTrap from 'react-focus-trap';
import style from './style.scss';

class Lightbox extends React.Component {

  componentDidMount() {
    window.addEventListener('keydown', this.props.onKeyDown);
    document.body.classList.add('no-scroll');
    document.body.addEventListener('touchmove', this.freezeVp, false);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.props.onKeyDown);
    document.body.classList.remove('no-scroll');
    document.body.removeEventListener('touchmove', this.freezeVp, false);
  }

  freezeVp(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div className={style.dialog} aria-hidden="false">
        <div className={style.mask} onClick={this.props.onClose} />
        <div className={style.window} role="dialog" tabIndex="0" aria-labelledby={this.props.ariaLabel} aria-describedby={this.props.ariaDesc}>
          {this.props.children}
          <FocusTrap>
            <button className={style.close} onClick={this.props.onClose}>
              <i className='fa fa-times' aria-hidden="true" />
              <span className={style['sr-only']}>Close lightbox</span>
            </button>
          </FocusTrap>
        </div>
      </div>
    );
  }
}

Lightbox.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired
};

export default Lightbox;
