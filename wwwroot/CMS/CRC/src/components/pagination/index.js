// @flow
import React from 'react';
import Paginate from 'react-paginate';
import classnames from 'classnames';

import type { Props } from './type';
import style from './style.scss';

const Pagination = (props: Props) => <Paginate {...props} />;

Pagination.defaultProps = {
  previousLabel: '«',
  nextLabel: '»',
  containerClassName: classnames(style.container, 'pagination'),
  activeClassName: 'current',
  previousClassName: 'arrow',
  nextClassName: 'arrow',
  disabledClassName: 'unavailable'
};

export default Pagination;
