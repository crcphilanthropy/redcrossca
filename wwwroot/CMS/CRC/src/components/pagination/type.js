// @flow
export type Props = {
  +pageCount: number,
  +pageRangeDisplayed: number,
  +marginPagesDisplayed: number,
  [key: string]: any
};
