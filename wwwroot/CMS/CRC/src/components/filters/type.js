// @flow

export type SelectOption = {
  value: string,
  label: string,
};

export type Props = {
  +filters: {
    +[key: string]: {
      placeholder?: string,
      label: ?string,
      data: Array<Object>,
    },
  },
  +tagList: Array<Object>,
  clearFilterText: string,
  onSelectTag: (key: string, value: SelectOption) => void,
  onRemoveTag: (key: string, value: SelectOption) => void,
  onClearButtonClick: (e: Event) => void,
};
