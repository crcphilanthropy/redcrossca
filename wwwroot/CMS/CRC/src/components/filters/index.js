// @flow
import React from 'react';
import Select from 'react-select';

import Tags from 'components/tags';

import type { Props, SelectOption } from './type';
import style from './style.scss';

/* list of selected tag that is binded to the Redux state store
(value assigned in the renderTagList component defined below).
CheckboxOption component is only called by the 3rd party React-Select component so we
cannot manually inject props to it, hence the use of this variable */
let listOfSelectedTags = [];

/* This component renders the checkbox for the dropdown options on the filter
below as per the example on the Github page of the 3rd party component used. */
class CheckboxOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = { checked: '' };
    this.handleMouseDown = this.handleMouseDown.bind(this);
  }

  componentDidMount() {
    const isOptionSelected = listOfSelectedTags.findIndex(
          tag => tag.value === this.props.option.value) !== -1;
    if (isOptionSelected) {
      this.setState({ checked: 'checked' });
    } else { this.setState({ checked: '' }); }
  }

  handleMouseDown(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.checked === '') {
      this.setState({ checked: 'checked' });
    } else { this.setState({ checked: '' }); }
    this.props.onSelect(this.props.option, event);
  }

  render() {
    return (<div className={style['dropdown-option']} onMouseDown={this.handleMouseDown}><input type="checkbox" checked={this.state.checked} /> <p>{this.props.children}</p></div>);
  }
}

/* Filter component for both Category and Region.
Note: this component is used both for photos and videos */
const renderFilters = (
  { tagList, filters, onSelectTag, onRemoveTag, onClearButtonClick, clearFilterText }: Props) => {
  listOfSelectedTags = tagList;

  return (
    <div className="row" id="media-filter">
      <div className="columns medium-8">
        <div className="row">
          {Object.keys(filters).map(key =>
            <div key={key} className="columns medium-6">
              <div className={style['category-label']}>
                {filters[key].label}
              </div>
              <Select
                multi
                closeOnSelect={false}
                searchable={false}
                optionComponent={CheckboxOption}
                name={`${key}-field`}
                className={style.select}
                placeholder={filters[key].placeholder}
                options={filters[key].data}
                onChange={
                      (selectedValue: SelectOption) => {
                        /* Checks to see if the option is already in the list of selected tags.
                        If yes,remove that tag, otherwise add the tags to the selected list. */
                        if (listOfSelectedTags.findIndex(tag => tag.value === selectedValue[0].value) === -1) {
                          onSelectTag(key, selectedValue[0]);
                        } else { onRemoveTag(key, selectedValue[0]); }
                      }
                }
              />
            </div>,
            )}
        </div>
      </div>
      <div className="columns small-12 medium-4">
        <button className={style['clear-filter']} onClick={onClearButtonClick}>
          {clearFilterText}
        </button>
      </div>
    </div>
  );
};

// Component that renders the selected tag list below the filters
const renderTagList = ({ tagList, onRemoveTag }: Props) => {
  if (tagList.length <= 0) {
    return null;
  }

  return (
    <div className="row">
      <div className="columns small-12">
        <Tags showCross list={tagList} onClickTag={onRemoveTag} />
      </div>
    </div>
  );
};

const Filters = (props: Props) =>
  <div>
    {renderFilters(props)}
    {renderTagList(props)}
  </div>;

export default Filters;
