import 'babel-polyfill';
import 'legacy';
import 'scss/app';
import 'scss/components/_videos';
import 'scss/components/_multimedia-styles';
import history from 'store/history';

/**
 * Return rendeder component with store attached
 * @param {*} node - to be attached to
 * @param {*} Component - React component
 * @param {*} props - additional props
 */
const renderWithStore = async (node, Component, props) => {
  const React = await import('react');
  const { render } = await import('react-dom');
  const {
    default: withStore,
  } = await import(/* webpackChunkName: "with-store" */ 'containers/with-store');

  const WrappedComponent = withStore(Component);

  render(<WrappedComponent {...props} />, node);
};

/**
 * Initialize Photo Container
 */
const initializePhotosContainer = async () => {
  const photosContainer = document.querySelectorAll('[data-mm-photo-listing]');
  if (photosContainer.length > 0) {
    const {
      default: PhotosContainer,
    } = await import(/* webpackChunkName: "photos-container" */ './containers/photos-container');

    for (let x = 0; x < photosContainer.length; x += 1) {
      const element = photosContainer[x];
      const pgSize = element.getAttribute('data-page-size');
      const options = {
        cultureCode: element.getAttribute('data-culture'),
        title: element.getAttribute('data-form-title'),
        description: element.getAttribute('data-form-description'),
        footerDescription: element.getAttribute('data-form-footer-description'),
        submitText: element.getAttribute('data-form-submit-text'),
        successMessage: element.getAttribute('data-form-success-message'),
        nameLabel: element.getAttribute('data-name-label'),
        namePlaceholder: element.getAttribute('data-name-placeholder'),
        nameError: element.getAttribute('data-name-error'),
        emailLabel: element.getAttribute('data-email-label'),
        emailPlaceholder: element.getAttribute('data-email-placeholder'),
        emailError: element.getAttribute('data-email-error'),
        publicationLabel: element.getAttribute('data-publication-label'),
        publicationPlaceholder: element.getAttribute('data-publication-placeholder'),
        publicationError: element.getAttribute('data-publication-error'),
        filterButtonText: element.getAttribute('data-filter-button-text'),
        pageSize: parseInt(pgSize, 10),
        categoryLabel: element.getAttribute('data-category-label'),
        categoryPlaceholder: element.getAttribute('data-category-placeholder'),
        regionLabel: element.getAttribute('data-region-label'),
        regionPlaceholder: element.getAttribute('data-region-placeholder'),
        showMoreText: element.getAttribute('data-show-more-text'),
        showLessText: element.getAttribute('data-show-less-text'),
        photoCredits: element.getAttribute('data-photo-credits-title'),
        downloadPhotoTitle: element.getAttribute('data-download-photo-title'),
        tagsTitle: element.getAttribute('data-tags-title'),
        fillForm: element.getAttribute('data-fill-form-title'),
        highRes: element.getAttribute('data-high-res'),
        lowRes: element.getAttribute('data-low-res'),
      };

      renderWithStore(element, PhotosContainer, { ...options, history });
    }
  }
};
/**
 * initialize video container
 * @param {*} node
 * @param {*} options
 */
const initializeVideosContainer = async () => {
  const videosContainer = document.querySelectorAll('[data-mm-video-listing]');
  if (videosContainer.length > 0) {
    const {
      default: VideosContainer,
    } = await import(/* webpackChunkName: "videos-container" */ './containers/videos-container');
    for (let x = 0; x < videosContainer.length; x += 1) {
      const element = videosContainer[x];
      const pgSize = element.getAttribute('data-page-size');
      const options = {
        path: element.getAttribute('data-path'),
        cultureCode: element.getAttribute('data-culture'),
        pageSize: parseInt(pgSize, 10),
        hideSummary: element.getAttribute('data-hide-summary') === 'True',
        categoryLabel: element.getAttribute('data-category-label'),
        categoryPlaceholder: element.getAttribute('data-category-placeholder'),
        regionLabel: element.getAttribute('data-region-label'),
        regionPlaceholder: element.getAttribute('data-region-placeholder'),
        showMoreText: element.getAttribute('data-show-more-text'),
        showLessText: element.getAttribute('data-show-less-text'),
        tagsTitle: element.getAttribute('data-tags-title'),
        filterButtonText: element.getAttribute('data-filter-button-text'),
        filterChangeAnalyticsEvent: element.getAttribute('data-filter-change-analytics'),
        tagClickAnalyticsEvent: element.getAttribute('data-tag-click-analytics'),
      };

      renderWithStore(element, VideosContainer, { ...options, history });
    }
  }
};

/**
 * Initialize web banners
 * @param {*} node
 */
const initializeWebBanners = async () => {
  const bannersContainer = document.querySelectorAll('[data-mm-web-banners]');
  if (bannersContainer.length > 0) {
    const {
      default: WebBanners,
    } = await import(/* webpackChunkName: "web-banners" */ './containers/web-banners');

    for (let x = 0; x < bannersContainer.length; x += 1) {
      const element = bannersContainer[x];
      const options = {
        cultureCode: element.getAttribute('data-culture'),
        title: element.getAttribute('data-form-title'),
        description: element.getAttribute('data-form-description'),
        footerDescription: element.getAttribute('data-form-footer-description'),
        submitText: element.getAttribute('data-form-submit-text'),
        successMessage: element.getAttribute('data-form-success-message'),
        companyNameLabel: element.getAttribute('data-company-name-label'),
        companyNamePlaceholder: element.getAttribute('data-company-name-placeholder'),
        companyNameError: element.getAttribute('data-company-name-error'),
        getCodesDescription: element.getAttribute('data-get-codes-description'),
        scrollToTopText: element.getAttribute('data-get-scroll-link-text'),
        trackingLabel: element.getAttribute('data-tracking-label'),
        embedLabel: element.getAttribute('data-embed-label'),
      };

      renderWithStore(element, WebBanners, options);
    }
  }
};

/**
 * Initialize videos download form component
 * @param {*} node
 * @param {*} url
 */
const initializeVideosFormContainer = async () => {
  const element = document.getElementById('video-download-form');
  if (element) {
    const {
      default: VideosForm,
    } = await import(/* webpackChunkName: "videos-for-media-form" */ './containers/videos-for-media-form');
    const options = {
      pageTitle: element.getAttribute('data-page-title'),
      cultureCode: element.getAttribute('data-culture'),
      videoId: element.getAttribute('data-id'),
      title: element.getAttribute('data-form-title'),
      description: element.getAttribute('data-form-description'),
      footerDescription: element.getAttribute('data-form-footer-description'),
      submitText: element.getAttribute('data-form-submit-text'),
      successMessage: element.getAttribute('data-form-success-message'),
      successButtonText: element.getAttribute('data-form-success-button-text'),
      nameLabel: element.getAttribute('data-name-label'),
      namePlaceholder: element.getAttribute('data-name-placeholder'),
      nameError: element.getAttribute('data-name-error'),
      emailLabel: element.getAttribute('data-email-label'),
      emailPlaceholder: element.getAttribute('data-email-placeholder'),
      emailError: element.getAttribute('data-email-error'),
      publicationLabel: element.getAttribute('data-publication-label'),
      publicationPlaceholder: element.getAttribute('data-publication-placeholder'),
      publicationError: element.getAttribute('data-publication-error'),
    };
    renderWithStore(element, VideosForm, options);
  }
};

/**
 * Initialize related items component
 * @param {*} node
 * @param {*} options
 */
const initializeRelatedItemsContainer = async () => {
  const elements = document.querySelectorAll('[data-relatedVideos]');

  if (elements.length > 0) {
    const React = await import('react');
    const { render } = await import('react-dom');
    const {
      default: RelatedItemsContainer,
    } = await import(/* webpackChunkName: "related-items-container" */ './containers/related-items-container');
    for (let x = 0; x < elements.length; x += 1) {
      const element = elements[x];
      const options = {
        list: JSON.parse(element.getAttribute('data-relatedvideos')),
        listingUrl: element.getAttribute('data-listing-url'),
        heading: element.getAttribute('data-heading'),
        showMoreText: element.getAttribute('data-show-more-text'),
        showLessText: element.getAttribute('data-show-less-text'),
        tagsTitle: element.getAttribute('data-tags-title'),
      };
      render(<RelatedItemsContainer {...options} />, element);
    }
  }
};

/**
 * Initialize back button component
 * @param {*} node
 * @param {*} options
 */
const initializedBackButton = async () => {
  const elements = document.querySelectorAll('[data-mm-back-button]');
  if (elements.length > 0) {
    const React = await import('react');
    const { render } = await import('react-dom');
    const {
      default: BackButton,
    } = await import(/* webpackChunkName: "back-button" */ './containers/back-button');
    for (let x = 0; x < elements.length; x += 1) {
      const element = elements[x];
      const options = {
        text: element.getAttribute('data-button-text'),
        history,
      };
      render(<BackButton {...options} />, element);
    }
  }
};

/**
 * Application Start -- Place everything in this function
 */
(() => {
  initializePhotosContainer();

  initializeVideosContainer();

  initializeWebBanners();

  initializeVideosFormContainer();

  initializeRelatedItemsContainer();

  initializedBackButton();
})();
