// @flow
import type { Filter, GetState, State, Action, ThunkAction, Dispatch } from './type';

export const types = {
  FILTER_UPDATE: 'FILTER_UPDATE',
  FILTER_CLEAR: 'FILTER_CLEAR',
  FILTER_PAGE_UPDATE: 'FILTER_PAGE_UPDATE',
  FILTER_PAGE_CLEAR: 'FILTER_PAGE_CLEAR',
};

const initialState = {
  selectedFilters: [],
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case types.FILTER_UPDATE:
    case types.FILTER_PAGE_UPDATE:
    case types.FILTER_PAGE_CLEAR:
      return {
        ...state,
        selectedFilters: action.payload,
      };
    case types.FILTER_CLEAR:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const updatePage = (pageNumber: string): ThunkAction => (
  dispatch: Dispatch,
  getState: GetState,
) => {
  // $FlowFixMe
  const { filter: { selectedFilters } } = getState();

  const payload: Array<Filter> = [
    ...selectedFilters.filter((filter: Filter) => filter !== null && filter.key !== 'page'),
    {
      key: 'page',
      value: pageNumber,
    },
  ];

  dispatch({ type: types.FILTER_PAGE_UPDATE, payload });
};

const getFilterId = (value: string): number | null => {
  const split = value.split('_');

  if (split.length !== 2 || isNaN(split[0])) {
    return null;
  }

  return parseInt(split[0], 10);
};

const formatFilter = (key: string, value: string): Filter => {
  let returnValue = value;

  if (key !== 'page') {
    returnValue = getFilterId(value);
  }

  if (returnValue == null) {
    return null;
  }

  return {
    key: key.toLowerCase(),
    value: returnValue,
  };
};

const updateFilter = (key: string, arrIds: Array<string>): ThunkAction => (
  dispatch: Dispatch,
  getState: GetState,
) => {
  // $FlowFixMe
  const { filter: { selectedFilters } } = getState();

  if (key !== undefined) {
    const payload: Array<Filter> = [
      ...selectedFilters.filter(filter => filter !== null && filter.key !== key),
      ...arrIds.map((id): Filter => formatFilter(key, id)),
    ];

    dispatch({ type: types.FILTER_UPDATE, payload });
  }
};

const setSingleFilter = (key: string, id: string): ThunkAction => (dispatch: Dispatch) => {
  if (key !== undefined) {
    const payload: Array<Filter> = [formatFilter(key, id)];

    dispatch({ type: types.FILTER_UPDATE, payload });
  }
};

const clearFilter = (): ThunkAction => (dispatch: Dispatch) => {
  dispatch({ type: types.FILTER_CLEAR });
};

export const actions = {
  setSingleFilter,
  updateFilter,
  clearFilter,
  updatePage,
};
