// @flow
export type Filter = {
  key: string,
  value: any,
} | null;

export type State = {
  selectedFilters: Array<Filter>,
};

export type Action = {
  +type: string,
  payload?: any,
};

export type GetState = () => State;

// eslint-disable-next-line no-use-before-define
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;

export type Dispatch = (action: Action | ThunkAction | Promise<Action>) => any;
