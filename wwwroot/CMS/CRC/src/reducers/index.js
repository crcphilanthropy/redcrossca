import { combineReducers } from 'redux';

import photo from 'reducers/photo';
import banners from 'reducers/banners';
import video from 'reducers/video';
import category from 'reducers/category';
import filter from 'reducers/filter';

export const rootReducer = combineReducers({
  banners,
  video,
  photo,
  category,
  filter
});

export default rootReducer;
