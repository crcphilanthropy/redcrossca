// @flow
import { get } from 'api/fetch';
import { categoryModel } from 'shared/models';
import type { Category, State, Action, ThunkAction, Dispatch } from './type';

export const types = {
  CATEGORY_FETCH_DATA: 'CATEGORY_FETCH_DATA',
};

const initialState = {
  ready: false,
  data: [],
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case types.CATEGORY_FETCH_DATA:
      return {
        ...state,
        ready: true,
        data: action.payload,
      };
    default:
      return state;
  }
};

const formatCategory = data =>
  Object.keys(data).reduce((source: Array<Category>, key) => {
    const categories = data[key];

    source = [...source, ...categories.map(category => categoryModel(key, category))];
    return source;
  }, []);

const fetchData = (cultureCode: string): ThunkAction => (dispatch: Dispatch) =>
  get(`/api/tags?culture=${cultureCode}`).then(data =>
    dispatch({
      type: types.CATEGORY_FETCH_DATA,
      payload: formatCategory(data),
    }),
  );

export const actions = {
  fetchData,
};
