// @flow
export type Category = {
  key: string,
  label: string,
  value: string,
};

export type State = {
  ready: boolean,
  data: Array<Category>,
};

export type Action = {
  +type: string,
  payload?: any,
};

export type GetState = () => State;

// eslint-disable-next-line no-use-before-define
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;

export type Dispatch = (action: Action | ThunkAction | Promise<Action>) => any;
