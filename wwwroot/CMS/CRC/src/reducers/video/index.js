// @flow
import { get, post } from 'api/fetch';
import { videoModel } from 'shared/models';

import type { State, Action, ThunkAction, GetState, Dispatch } from './type';

const API = '/api/videos';

export const types = {
  VIDEO_FETCH_DATA: 'VIDEO_FETCH_DATA',
  VIDEO_POST_DATA: 'VIDEO_POST_DATA',
  VIDEO_TAG_UPDATE: 'VIDEO_TAG_UPDATE',
  VIDEO_TAG_REMOVE: 'VIDEO_TAG_REMOVE',
  VIDEO_TAG_CLEAR: 'VIDEO_TAG_CLEAR',
};

const initialState = {
  ready: false,
  list: [],
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case types.VIDEO_FETCH_DATA:
      return {
        ...state,
        ready: true,
        list: action.payload,
      };
    case types.VIDEO_POST_DATA:
      return {
        ...state,
        ready: action.payload !== undefined && action.payload.downloadLink !== undefined,
        selectedVideo: action.payload,
      };
    default:
      return state;
  }
};

const fetchData = (
  path: string,
  cultureCode: string,
  options: { [key: string]: any },
): ThunkAction => (dispatch: Dispatch) =>
  get(`${API}?path=${path}&culture=${cultureCode}`).then(data =>
    dispatch({
      type: types.VIDEO_FETCH_DATA,
      payload: data.reduce((source, video) => {
        const { hideSummary } = options;
        if (hideSummary !== undefined && hideSummary) {
          // remove video summary
          video.VideoSummary = null;
        }

        source.push(videoModel(video));

        return source;
      }, []),
    }),
  );

const postData = (formData: { [key: string]: any }): ThunkAction => (dispatch: Dispatch) =>
  post(API, formData).then(data =>
    dispatch({ type: types.VIDEO_POST_DATA, payload: videoModel(data) }),
  );

const downloadVideo = (fileName: string): ThunkAction => (
  dispatch: Dispatch,
  getState: GetState,
) => {
  // $FlowFixMe
  const { video: { selectedVideo } } = getState();

  if (selectedVideo) {
    const { downloadLink } = selectedVideo;
    window.location = `/crc/handlers/videohandler.ashx?name=${fileName}&downloadlink=${downloadLink}`;
  }
};

export const actions = {
  fetchData,
  postData,
  downloadVideo,
};
