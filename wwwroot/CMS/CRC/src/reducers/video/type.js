// @flow
import type { Category } from '../category/type';
export type { Category } from '../category/type';

export type Video = {
  id: number,
  title: string,
  description?: string,
  thumbnail: string,
  showDownload: boolean,
  videoLength: number,
  downloadLink?: string,
  tagList: Array<Category>,
};

export type State = {
  ready: boolean,
  list: Array<Video>,
  selectedVideo?: Video,
};

export type Action = {
  +type: string,
  payload?: any,
};

export type GetState = () => State;

// eslint-disable-next-line no-use-before-define
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;

export type Dispatch = (action: Action | ThunkAction | Promise<Action>) => any;
