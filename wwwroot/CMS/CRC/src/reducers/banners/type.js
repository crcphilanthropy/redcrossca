// @flow
export type Banner = {
  BannerID: number,
  Title: string,
  TrackingCode: string,
  EmbedLink: string,
  Image: string,
  AltText: string,
  BannerSetID: number,
  BannerType: string
}

export type BannerSet = {
  BannerSetID: number,
  Title: string,
  Description: string,
  Banners: Array<Banner>
};


export type State = {
  ready: boolean,
  list: Array<BannerSet>
};

export type Action = {
  +type: string,
  payload ?: any
};

export type GetState = () => State;

// eslint-disable-next-line no-use-before-define
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;

export type Dispatch = (action: Action | ThunkAction | Promise<Action>) => any;
