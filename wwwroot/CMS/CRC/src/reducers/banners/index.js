// @flow
import { get, post } from 'api/fetch';
import type { State, Action, ThunkAction, Dispatch } from './type';

export const types = {
  BANNER_FETCH_DATA: 'BANNER_FETCH_DATA',
  BANNER_POST_DATA: 'BANNER_POST_DATA',
};

const initialState = {
  ready: false,
  list: [],
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case types.BANNER_FETCH_DATA:
    case types.BANNER_POST_DATA:
      return {
        ...state,
        ready: true,
        list: action.payload,
      };
    default:
      return state;
  }
};

const fetchData = (cultureCode: string): ThunkAction => (dispatch: Dispatch) =>
  get(`/api/bannerSets?culture=${cultureCode}`).then(data =>
    dispatch({ type: types.BANNER_FETCH_DATA, payload: data }),
  );

const postData = (name: string, cultureCode: string): ThunkAction => (dispatch: Dispatch) =>
  post(`/api/bannerSets?culture=${cultureCode}`, name).then(data =>
    dispatch({ type: types.BANNER_POST_DATA, payload: data }),
  );

export const actions = {
  fetchData,
  postData,
};
