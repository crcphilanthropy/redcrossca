// @flow
import { get, post } from 'api/fetch';
import { photoModel } from 'shared/models';

import type { PostData, State, Action, ThunkAction, Dispatch } from './type';

export const types = {
  PHOTO_POST_DATA: 'PHOTO_POST_DATA',
  PHOTO_FETCH_DATA: 'PHOTO_FETCH_DATA',
  PHOTO_TAG_UPDATE: 'PHOTO_TAG_UPDATE',
  PHOTO_TAG_REMOVE: 'PHOTO_TAG_REMOVE',
  PHOTO_TAG_CLEAR: 'PHOTO_TAG_CLEAR',
};

const initialState = {
  ready: false,
  list: [],
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case types.PHOTO_FETCH_DATA:
    case types.PHOTO_POST_DATA:
      return {
        ...state,
        ready: true,
        list: action.payload,
      };
    default:
      return state;
  }
};

const fetchData = (cultureCode: string): ThunkAction => (dispatch: Dispatch) =>
  get(`/api/photos?culture=${cultureCode}`).then(data =>
    dispatch({ type: types.PHOTO_FETCH_DATA, payload: data.map(photoModel) }),
  );

const postData = (formData: PostData): ThunkAction => (dispatch: Dispatch) =>
  post('/api/photos', formData).then(data =>
    dispatch({ type: types.PHOTO_POST_DATA, payload: data.map(photoModel) }),
  );

export const actions = {
  fetchData,
  postData,
};
