// @flow
import type { Category } from '../category/type';
export type { Category } from '../category/type';

export type PostData = {
  Name: string | null,
  Email: string | null,
  Publication: string | null,
};

export type Photo = {
  id: number,
  title: string,
  description: string,
  altText: string,
  thumbnail: string,
  lowResImage: string,
  highResImage: string,
  showDownload: boolean,
  credits: string,
  tagList: Array<Category>,
};

export type State = {
  ready: boolean,
  list: Array<Photo>,
};

export type Action = {
  +type: string,
  payload?: any,
};

export type GetState = () => State;

// eslint-disable-next-line no-use-before-define
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;

export type Dispatch = (action: Action | ThunkAction | Promise<Action>) => any;
