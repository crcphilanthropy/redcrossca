import { gtmPush } from 'shared/analytics';

const crc = {};

$(document).ready(() => {
  $(document).foundation();

  const quizContainer = document.getElementById('quiz');
  if (quizContainer) {
    initQuiz(quizQuestions);
  }
  
  // Disptched from /CMS/Ecentricarts/Scripts/e2e/mobilemenu.js
  $.fn.ecaMenuLoaded = function () {
    $('.menu-btn').addClass('loaded');
  };

  // Set focus on newletter name
  if ($('#NewsletterFirstnameTextBox').length) {
    $('#NewsletterFirstnameTextBox').focus();
  }

  $.get(`/api/iyc/getprovince?id=${dataLayer[0].province}`, (data) => {
    if (data !== undefined && data !== null) $('.iyc-geo-url').attr('href', data.DocumentUrlPath);
  });

  // FitVids
  $('.site-main').fitVids();

  // Fastclick for device taps
  // FastClick.attach(document.body, 'a');

  $('.side-nav li:has(ul)').addClass('children');

  // Add class if element is active

  $('.active:last').children('a').addClass('selected');

  // Find Local Branch - Force users to scroll to the top

  if ($('body').hasClass('find-local-services ') || $('body').hasClass('search-results')) {
    setTimeout(() => {
      // $(window).scrollTop(0);
      $('html, body').animate(
        {
          scrollTop: $('.site-header').offset().top,
        },
        100,
      );
    }, 350);
  }

  // Find Local Branch Preloader

  $('.search-wrap a').on('click', function () {
    $(this).addClass('clicked');
  });

  // Accessible Mega Menu
  $('.search-wrap').accessibleMegaMenu();
  if ($('.main-nav nav').length > 0) {
    $('.main-nav nav').accessibleMegaMenu();
    $('.province-dd-wrap').accessibleMegaMenu();
    $('.main-nav ul > li.nav-item:last').addClass('last'); // Add .last to last menu item.
  }

  $('.nav-secondary .utility ul').accessibleMegaMenu();
  $('.log-wrap').accessibleMegaMenu();

  $('.menu-btn, span.close').on('click', () => {
    $('.flexpanel').toggleClass('open');
    $('body').toggleClass('stationary');
  });

  // - On hover
  $('.main-nav ul > li, li.province-dd').hover(
    function () {
      $('.sub-nav', this).addClass('open');
    },
    function () {
      $('.sub-nav', this).removeClass('open');
    },
  );

  // Mobile Nav Custom JS

  $('.navigation').on('click', '.nav-back', function () {
    $(this).closest('.nav-group').remove();
  });

  $('.navigation').on('click', 'a.has-child', () => {
    setTimeout(() => {
      $('.nav-active li:first-child a').focus();
      // $(".nav-active .nav-back").focus();
    }, 100);
  });

  // Mobile Menu - Province Dropdowns

  $('.province-dd').on('click', function () {
    $(this).toggleClass('open');
  });

  // Add Target="_blank" to twitter anchor tags

  setTimeout(() => {
    $('.twtr-tweets a').attr('target', '_blank');
  }, 3000);

  // Contact Modals Stack Table
  const contactModalsTable = $('.contact-modals').find('table');
  if (contactModalsTable.length > 0) {
    // wrap table with 'no-more-tables' class
    contactModalsTable.wrap($('<div>', { class: 'no-more-tables' }));

    // get header
    $.each(contactModalsTable, (idx, obj) => {
      const header = $(obj).find('thead th');
      const bodyRow = $(obj).find('tbody tr');

      $.each(bodyRow, (rowIdx, row) => {
        const body = $(row).find('td');

        for (let i = 0; i < header.length; i++) {
          $(body[i]).attr('data-content', $(header[i]).text());
        }
      });
    });
  }

  // Accordion JS

  const question = $('.accordion div .question');
  const answer = $('.accordion div .answer');

  question.on('click', function () {
    if ($(this).parent('div').hasClass('active')) {
      $(this).parent('div').removeClass('active');
    } else {
      if ($(window).width() > 950) {
        question.parent('div').removeClass('active');
      }
      $(this).parent('div').toggleClass('active');
    }
  });

  $('.accordion > div:first-child').addClass('active');

  if ($('.slider').length > 0) {
    $('.slider').slick({
      dots: true,
      arrows: true,
      mobileFirst: true,
      infinite: true,
      lazyLoad: 'progressive',
      customPaging(slider, i) {
        let buttonMarkup = `<button type="button" class="cp-track" data-role="none" data-event="generic-event" data-category="carousel" data-action="thumbnail click" data-label="${btns[
          i
        ][1]}">`;
        if (btns[i][0] !== '') {
          buttonMarkup += `<img class="icon vertical-align" src='${btns[i][0]}'/>`;
        }
        buttonMarkup += `<div class="vertical-align">${btns[i][1]}</div>`;
        buttonMarkup += '</button>';
        return buttonMarkup;
      },
    });

    const btnWrapperClass = `slick-dots-count-${btns.length}`;

    $('.slick-dots').wrap(`<div class='slick-dots-wrapper' id=${btnWrapperClass}></div>`);

    if (btns.length == 1) {
      let singleSlideButton = '<div class="slick-dots-wrapper" id="slick-dots-count-1">';
      singleSlideButton += '<ul class="slick-dots"><li class="slick-active">';
      singleSlideButton += `<button type="button" class="cp-track" type="button" data-role="none" data-event="generic-event" data-category="carousel" data-action="thumbnail click" data-label="${btns[0][1]}">`;
      singleSlideButton += `<img class="icon vertical-align" src='${btns[0][0]}'/>`;
      singleSlideButton += `<div class="vertical-align">${btns[0][1]}</div>`;
      singleSlideButton += '</button>';
      singleSlideButton += '</ul></li></div>';

      $('.slider .slide:last-child').append(singleSlideButton);
    }
  }

  /* Placeholder fix for older browsers */

  if (!Modernizr.input.placeholder) {
    $('[placeholder]')
      .focus(function () {
        const input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      })
      .blur(function () {
        const input = $(this);
        if (input.val() === '' || input.val() === input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      })
      .blur();
    $('[placeholder]').parents('form').submit(function () {
      $(this).find('[placeholder]').each(function () {
        const input = $(this);
        if (input.val() === input.attr('placeholder')) {
          input.val('');
        }
      });
    });
  }

  // Make SVGs Embedded from IMG Tags
  $('img[src*=".svg"]').each(function () {
    const $img = $(this);
    const imgID = $img.attr('id');
    const imgClass = $img.attr('class');
    const imgURL = $img.attr('src');

    jQuery.get(
      imgURL,
      (data) => {
        // Get the SVG tag, ignore the rest
        let $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', `${imgClass} replaced-svg`);
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);
      },
      'xml',
    );
  });

  // Job list preloader on click

  $('body').on('click', '.DESC', function () {
    $(this).addClass('loading');
  });

  $('body').on('click', '.ASC', function () {
    $(this).addClass('loading');
  });

  // Add .ASC class to data column on job search
  // /about-us/jobs/opportunities-in-canada
  if ($('#tblJobList').length) {
    $('#tblJobList th').eq(1).find('a').addClass('DESC');
  }

  if ($('#table').length > 0) {
    // Table Sorting
    $('#table').tablesorter();
  }

  // ROS Banner Doesnt Overlap Script
  crc.resizeCRCFooter = function () {
    $('footer').css('padding-bottom', $('.ros-banner').height());
  };

  if ($('.ros-banner').length) {
    crc.resizeCRCFooter();
  }

  $('.ros-banner .Banner').removeAttr('onclick');
  $('.ros-banner .Banner').removeAttr('onmouseup');

  /*
  *  Window.Resize
  */
  let resizeTimeOut = null;
  window.onresize = function () {
    if (resizeTimeOut !== null) clearTimeout(resizeTimeOut);
    resizeTimeOut = setTimeout(() => {
      if ($('.ros-banner').length) {
        crc.resizeCRCFooter();
      }
    }, 300);
  };

  // Close Promo Banner

  $('.closeBanner').on('click', function () {
    const path = $(this).data('path');
    const hidePromotionNDays = $(this).data('hidepromotionndays');
    $('.promo-banner').slideUp();
    $.post(`/api/promotionalbanner/post?path=${path}&hidePromotionNDays=${hidePromotionNDays}`);
  });

  $('.nav-wrap .double').parent().siblings().addClass('double');

  // Tigger submit of Donation form on "Enter   Click"

  $('.donation-CTA input').keypress((e) => {
    if (e.which == 13) {
      // console.log("clicked enter");
      $(' .donation-CTA input[type="submit"]').trigger('click');
      return false; // <---- Add this line
    }
  });

  // Cardinal Path Tracking
  $('.cp-track.socialshares').data('label', window.location.href);
  $('.eca-form-track').click(function () {
    let searchRefinement;
    if ($(this).data('filter') == 'on') {
      searchRefinement = $('.cp-refinement').data('value');
    }
    let refinementValue = '';
    const prevCategory = getParameterByName('category');
    const prevSubCategory = getParameterByName('subcategory');
    const prevLocation = getParameterByName('location');

    if ($('.category-select option:selected').index() !== 0) {
      if (
        $('.category-select option:selected').val() !== prevCategory ||
        searchRefinement === undefined
      ) {
        refinementValue += $('.category-select option:selected').text();
      }

      if (
        $('.subcategory-select option:selected').index() !== 0 &&
        $('.subcategory-select option:selected').index() != -1 &&
        (prevSubCategory != $('.subcategory-select option:selected').val() ||
          searchRefinement === undefined)
      ) {
        refinementValue += ` - ${$('.subcategory-select option:selected').text()}`;
      }
    }
    if (
      $('input.iyc-location').val() !== '' &&
      ($('input.iyc-location').val() != prevLocation || searchRefinement === undefined)
    ) {
      if (refinementValue !== '') {
        refinementValue += '/';
      }
      refinementValue += $('input.iyc-location').val();
    }

    try {
      gtmPush($(this).data('event'), {
        eventCategory: $(this).data('category'),
        eventAction: searchRefinement || $(this).data('action'),
        eventLabel: (evLabel = $(this).data('label') ? $(this).data('label') : refinementValue),
      });
    } catch (e) {
      console.log(e);
    }
  });

  $('.donate-track').click(function () {
    SetGAHash(this);
  });

  String.prototype.toHtmlEntities = function () {
    return this.replace(/[^a-z0-9\.\-\_\s\t]/gi, c => `&#${c.charCodeAt(0)};`);
  };

  function htmlEntities(str) {
    return str.replace(/[^a-z0-9\.\-\_\s\t]/gi, c => `&#${c.charCodeAt(0)};`);
  }

  function logStart(id) {
    const url = `/api/QuizData/post?id=${id}`;
    $.getJSON(url, (data) => {});
  }

  function logComplete(id) {
    const url = `/api/QuizData/quizcomplete?id=${id}`;
    $.getJSON(url, (data) => {});
  }
  function scrollUp() {
    setTimeout(() => {
      $('html, body').animate(
        {
          scrollTop: $('.no-more-tables').first().offset().top,
        },
        800,
      );
    }, 5000);
  }
  // QUIZ JS ENDS
  initEmailTrim();

  // Listen for the share event

  setTimeout(() => {
    try {
      if (addthis !== undefined) {
        addthis.init();
        addthis.addEventListener('addthis.menu.share', shareEventHandler);
      }
    } catch (ex) {
      console.log(ex);
    }
  }, 1000);

  $('.languageSwitchLink').click(() => {
    $('.language-toggle-hash').val(ga.getAll()[0].get('linkerParam'));
  });

  $('.lyrisForm').click(function () {
    const validated = Page_ClientValidate($(this).data('validation-group'));
    if (validated) {
      gtmPush('generic-event', {
        eventCategory: 'newsletter submission',
        eventAction: $(this).data('event-action'),
        eventLabel: $(this).data('event-label'),
      });
    }
  });

  $('a.fancybox\\.iframe').click((e) => {
    e.preventDefault();
  });

  openExternalLinksInNewWindow();

  $('.transcript-button').click((e) => {
    const button = $(e.currentTarget);
    const title = button.data('title');
    const analytics = button.data('transcriptAnalyticsEvent');

    if ((analytics !== undefined) & (analytics !== '')) {
      gtmPush(analytics, {
        videoName: title,
      });
    }
  });

  // Transcript toggle
  // $('.transcript-title').click((e) => {
  //   if ($('.transcript-item').hasClass('is-active')) {
  //     $('.show-text').css('display', 'none');
  //     $('.hide-text').css('display', 'inline');
  //   } else {
  //     $('.show-text').css('display', 'inline');
  //     $('.hide-text').css('display', 'none');
  //   }
  // });

  // Disable HTML% input validation tooltips

  $('input').on('invalid', (e) => {
    e.preventDefault();
  });
});

function openExternalLinksInNewWindow() {
  try {
    const a = new RegExp(`/${window.location.host}/`);
    $("a[href^='http://'], a[href^='https://']").each(function () {
      if (!a.test(this.href)) {
        if ($(this).attr('target') === undefined || $(this).attr('target') === '') {
          $(this).attr('target', '_blank');
        }
      }
    });
  } catch (e) {}
}

function initQuiz(quizQuestions) {
  // variables
  const data = quizQuestions;
  let quiz = '';
  const total = data.length;
  let correctnumber = 0;

  // construct quiz ui content
  $.each(data, (index, element) => {
    const QuestionText = element.QuestionText;
    const ResponseRationale = element.ResponseRationale;
    const ResponseId = element.ResponseId;
    const ResponseList = element.ResponseList;
    quiz += `<div id='page-${index}' class='pages'>`;
    quiz += `<div class='question' id='q-${index}'>`;
    quiz += `<p>${index + 1}) ${QuestionText}</p>`;
    quiz += '</div>';
    quiz += `<div class='option' id='o-${index}'>`;
    jQuery.each(ResponseList, (index, value) => {
      if (value !== '') {
        quiz +=
          `<div><input type='radio' class='select' name='option' value='${value}' /><label for='` +
          `o-${index}'>${value}</label></div>`;
      }
    });
    quiz += '</div>';
    quiz += `<div class='explaination hide' id='e-${index}'>`;
    quiz += `<input type='hidden' name='a-${index}' value='${ResponseId}' class='answer' />`;
    quiz += `<hr />${ResponseRationale}`;
    quiz += '</div>';
    quiz +=
      `${"<br /><br /><a class='next hide button' href='#'>" + '<span>'}${quizNextText}</span>` +
      '</a>';
    quiz += '</div>';
  });
  $('#quiz #quiz-content').html(quiz);
  $('#quiz #intro .next').text(quizBeginButtonText);

  // Begin button action
  $('#quiz #intro .next').click(() => {
    // log: start quiz
    logStart(quizId);

    $('#quiz .pages:first').show();
    $('#intro').hide();

    gtmPush('Quiz', {
      eventCategory: '',
      eventAction: 'Next Intro Next',
      eventLabel: quizId,
    });
    return false;
  });
  // Quiz question Next button action
  $('#quiz .pages .next').each(function (index) {
    $(this).click(function () {
      $(this).parent().next().show();
      $(this).parent().remove();
      if ($('#quiz-content').html() === '') {
        // calculate result
        const percentage =
          `<span class='h2'>${Math.floor(correctnumber / total * 100)}%` + '</span>';
        quizCompletedText = quizCompletedText
          .replace('%PERCENT%', percentage)
          .replace('%CORRECT%', correctnumber)
          .replace('%TOTAL%', total);
        $('#quiz #thankyou .result').html(quizCompletedText);

        // show last page
        $('#thankyou').show();

        // log: complete quiz
        logComplete(quizId);
      }

      gtmPush('Quiz', {
        eventCategory: '',
        eventAction: 'Next Question Next',
        eventLabel: quizId,
      });

      return false;
    });
  });
  // Select action
  $('#quiz .pages .option .select').click(function () {
    const answer = $(this).parent().parent().parent().find('.explaination').find('.answer').val();
    const selected = $(this); // current selected object

    $(this).parent().parent().parent().find('.hide').show();
    $(this).parent().parent().find('.select').prop('disabled', true);

    // highlight the answer is correct or not
    $(this).parent().parent().find('.select').each(function (index) {
      if (answer == index + 1) {
        $(this).parent().addClass('correct');
      } else {
        selected.parent().addClass('incorrect');
      }
      if (answer == index + 1 && $(this).is(':checked')) {
        correctnumber += 1;
      }
    });
  });
  $('#quiz .option div').click(function (event) {
    if ($(this).find('input:radio').is(':enabled')) {
      $(this).find('input:radio').prop('checked', true).trigger('click');
    }
  });

  function logStart(id) {
    const url = `/api/QuizData/post?id=${id}`;
    $.getJSON(url, (data) => {});
  }

  function logComplete(id) {
    const url = `/api/QuizData/quizcomplete?id=${id}`;
    $.getJSON(url, (data) => {});
  }

  $('.next').on('click', () => {
    // Scroll to the top when clicking next
    $('html, body').animate(
      {
        scrollTop: $('.donate-bar').offset().top,
      },
      100,
    );
  });
}

// Addthis appears when clicking on the mobile/table social button

$('#sTrigger').on('click', () => {
  $('#at4m-sb i').click();
  $('#at4m-mobile-container').addClass('show-important');
  $('#at4m-mobile-container').addClass('at4-show');
  $('#at4m-mobile-container').addClass('at4-show');
});

$('#at4m-mcc').click(() => {
  $('#at4m-mobile-container').removeClass('show-important');
});

function clickAddThis() {
  $('#sTrigger').on('click', () => {
    $('#at4m-sb i').click();
    $('#at4m-mobile-container').addClass('show-important');
    $('#at4m-mobile-container').addClass('at4-show');
    $('#at4m-mobile-container').addClass('at4-show');
  });

  $('#at4m-mcc').click(() => {
    $('#at4m-mobile-container').removeClass('show-important');
  });
}

function scrollUp() {
  $('html, body').animate(
    {
      scrollTop: $('.no-more-tables').offset().top - 75,
    },
    700,
  );
}

function SetGAHash(src) {
  try {
    const tracker = _gat._getTrackers()[0];
    if (tracker) {
      const hash = tracker._getLinkerUrl(`${window.location.href}?type=single&amount=15`, true);
      $('.donate-bar-hash').val(hash);
    }
  } catch (e) {}
}

function initEmailTrim() {
  $('input.trim[type="text"]').keydown(function (e) {
    if (e.which == 32 || e.keyCode == 32) {
      e.preventDefault();
      return;
    }

    const preText = $(this).val();
    const text = $.trim(preText);
    if (preText != text) {
      this.value = '';
      this.value = text;
    }
  });
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  let regex = new RegExp(`[\\?&]${name}=([^&#]*)`),
    results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function shareEventHandler(evt) {
  if (evt.type == 'addthis.menu.share') {
    gtmPush('social-event', {
      socialNetwork: evt.data.service,
      socialAction: 'Share',
      socialTarget: evt.data.url,
    });
  }
}

setTimeout(() => {
  try {
    if (FB && FB.Event && FB.Event.subscribe) {
      FB.Event.subscribe('edge.create', (opt_target) => {
        gtmPush('social-event', {
          socialNetwork: 'Facebook',
          socialAction: 'Like',
          socialTarget: opt_target,
        });
      });
      FB.Event.subscribe('edge.remove', (opt_target) => {
        gtmPush('social-event', {
          socialNetwork: 'Facebook',
          socialAction: 'Unlike',
          socialTarget: opt_target,
        });
      });
    }
  } catch (e) {}
}, 1000);

Date.now =
  Date.now ||
  function () {
    return +new Date();
  };
let days;
let hrs;
let mins;
let hasBeen = '';
let replyText = 'Reply';
let retweetText = 'Retweet';
let favouriteText = 'Favourite';

if (jQuery('body').hasClass('FRCA')) {
  days = 'jours';
  hrs = 'heures';
  mins = 'minutes';
  hasBeen = 'Il y a';
  replyText = 'R&eacute;pondre';
  retweetText = 'Retweeter';
  favouriteText = 'Favori';
} else {
  days = 'days ago';
  hrs = 'hrs ago';
  mins = 'mins ago';
}

// Twitter Parsers
String.prototype.parseURL = function () {
  return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, url =>
    url.link(url),
  );
};
String.prototype.parseUsername = function () {
  return this.replace(/[@]+[A-Za-z0-9-_]+/g, (u) => {
    const username = u.replace('@', '');
    return u.link(`http://twitter.com/${username}`);
  });
};
String.prototype.parseHashtag = function () {
  return this.replace(/[#]+[A-Za-z0-9-_]+/g, (t) => {
    const tag = t.replace('#', '%23');
    return t.link(`http://twitter.com/search?q=${tag}&src=hash`);
  });
};
function parseTwitterDate(str) {
  let toReturn = new Date(str);
  if (!isNaN(toReturn)) return toReturn;

  let v = str.split(' ');
  toReturn = new Date(Date.parse(`${v[1]} ${v[2]}, ${v[5]} ${v[3]} UTC`));
  if (!isNaN(toReturn)) return toReturn;

  v = str.split('T');
  const v2 = v[1].split('.');
  toReturn = new Date(Date.parse(`${v[0].replace(/-/g, '/')} ${v2[0]}`));
  if (!isNaN(toReturn)) return toReturn;

  return Date.now;
}

function MillisecondsToDuration(n) {
  const dtm = new Date();
  dtm.setTime(n);
  let d = 0;
  const h = Math.floor(n / 3600000);
  const m = dtm.getMinutes();
  if (h > 24) d = parseInt(h / 24);

  if (d > 0) return `${hasBeen} ${d} ${days}`;

  if (h > 0) return `${hasBeen} ${h} ${hrs}`;

  return `${hasBeen} ${m} ${mins}`;
}

// Twitter Widget

const replySVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 72"><path d="M41 31h-9V19c0-1.14-.647-2.183-1.668-2.688-1.022-.507-2.243-.39-3.15.302l-21 16C5.438 33.18 5 34.064 5 35s.437 1.82 1.182 2.387l21 16c.533.405 1.174.613 1.82.613.453 0 .908-.103 1.33-.312C31.354 53.183 32 52.14 32 51V39h9c5.514 0 10 4.486 10 10 0 2.21 1.79 4 4 4s4-1.79 4-4c0-9.925-8.075-18-18-18z"/></svg>';
const retweetSVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72"><path d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/></svg>';
const favSVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 72"><path d="M38.723,12c-7.187,0-11.16,7.306-11.723,8.131C26.437,19.306,22.504,12,15.277,12C8.791,12,3.533,18.163,3.533,24.647 C3.533,39.964,21.891,55.907,27,56c5.109-0.093,23.467-16.036,23.467-31.353C50.467,18.163,45.209,12,38.723,12z"/></svg>';
const starSVG = '';

function selectSingleTweet(tweetId) {
  const url = `/api/twitter/GetTweet?id=${tweetId}`;
  twttr.ready((twitter) => {
    $.getJSON(url).done((data) => {
      if (data === undefined || data === null) {
        return;
      }

      const twitterWidget = $('[data-twitter-ready=false]');

      if (twitterWidget.length > 0) {
        $.each(twitterWidget, (idx, obj) => {
          const container = $(obj);
          const header = container.children('.tweet-header');

          // Profile picture
          $('<img>', {
            src: data.userPic,
          }).appendTo(header.children('.profile-icon'));

          // username
          header.children('.profile').children('.username').text(`@${data.username}`);

          // display name
          header.children('.profile').children('.display-name').text(data.displayName);

          const followButton = header.children('.follow-button')[0];
          $(followButton).on('click', (e) => {
            e.preventDefault();
          });
          // follow button
          twitter.widgets.createFollowButton(data.username, followButton, {
            size: 'large',
            showScreenName: false,
            showCount: false,
          });

          // $('<a>', {
          //   href: 'https://twitter.com/intent/follow?user_id=' + data.id_str,
          //   'data-size': 'large',
          //   'data-show-screen-name': false,
          //   'data-show-count': false
          // })
          //   .on('click', function (e) { e.preventDefault(); })
          //   .text('Follow')
          //   .appendTo($(header.children('.follow-button')[0]));

          const tweet = container.children('.tweet');
          tweet.html(data.text.parseURL().parseUsername().parseHashtag());

          const footer = container.children('.tweet-footer');
          const action = footer.children('.tweet-action');

          // reply
          $('<a>', {
            href: `https://twitter.com/intent/tweet?in_reply_to=${data.id_str}`,
          })
            .on('click', (e) => {
              e.preventDefault();
            })
            .html(
              `${replySVG}`,
            )
            .appendTo($(action.children('.reply')));

          // retweet
          $('<a>', {
            href: `https://twitter.com/intent/retweet?tweet_id=${data.id_str}`,
          })
            .on('click', (e) => {
              e.preventDefault();
            })
            .html(
              `${retweetSVG}${data.retweet_count}`,
            )
            .appendTo($(action.children('.retweet')));

          // favourite
          $('<a>', {
            href: `https://twitter.com/intent/like?tweet_id=${data.id_str}`,
          })
            .on('click', (e) => {
              e.preventDefault();
            })
            .html(
              `${favSVG}${data.favourite_count}`,
            )
            .appendTo($(action.children('.favourite')));

          // date
          footer.children('.date').text(data.created_at);

          // Set ready to true
          container.attr('data-twitter-ready', true);
        });
      }
    });
  });
}

window.selectSingleTweet = selectSingleTweet;

function loadLatestTweet(userName, numTweets) {
  const url = `/api/twitter/get?id=${userName}&tweetCount=${numTweets}`;
  $.getJSON(url, (data) => {
    if (data === undefined || data === null) return;
    for (let i = 0; i < data.length; i++) {
      const tweet = data[i];
      const tweetText = tweet.text.parseURL().parseUsername().parseHashtag();
      var userInfo = `<div class="user-info"><img src="${tweet.userPic}" alt="${tweet.displayName}" /> <div><p class="strong">${tweet.displayName}</p> <p>@${tweet.username}</p></div> <p class="date">${tweet.created_at}</p></div>`;
      // const userInfo = '';
      let metaInfo = '<ul class="meta">';
      // metaInfo += `<li><a target="_blank" href="https://twitter.com/#!/${userName}/status/${tweet.id_str}" target="_blank">${tweet.created_at}</a></li>`;
      metaInfo += `<li class="reply"><a target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=${tweet.id_str}" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="reply" data-label="https://twitter.com/intent/tweet?in_reply_to=${tweet.id_str}" target="_blank">${replySVG} ${replyText}</a></li>`;
      metaInfo += `<li class="retweet"><a target="_blank" href="https://twitter.com/intent/retweet?tweet_id=${tweet.id_str}" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="retweet" data-label="https://twitter.com/intent/retweet?tweet_id=${tweet.id_str}" target="_blank">${retweetSVG} ${retweetText}</a></li>`;
      metaInfo += `<li class="favourite"><a target="_blank" href="https://twitter.com/intent/favorite?tweet_id=${tweet.id_str}" class="cp-track" data-event="generic-event" data-category="twitter feed" data-action="favorite" data-label="https://twitter.com/intent/favorite?tweet_id=${tweet.id_str}" target="_blank">${favSVG} ${favouriteText}</a></li>`;
      metaInfo += '</ul>';
      $('.twitter-widget').append(
        `<article class="twtr-tweet-text"><div class="tweet">${userInfo}${tweetText}${metaInfo}</div></article>`,
      );
    }
  });
}

window.loadLatestTweet = loadLatestTweet;

// Transcript toggle
const transcript = $('.transcript-text');
const transcriptButton = $('.transcript-button');
$('.transcript-button').on('click', (e) => {
  e.preventDefault();

  transcript.toggleClass('show-transcript');

  if (transcript.hasClass('show-transcript')) {
    transcriptButton.attr('aria-expanded', true);
    transcript.attr('aria-hidden', false);
    $('.show-text').css('display', 'none');
    $('.hide-text').css('display', 'block');
  } else {
    transcriptButton.attr('aria-expanded', false);
    transcript.attr('aria-hidden', true);
    $('.show-text').css('display', 'block');
    $('.hide-text').css('display', 'none');
  }
});
