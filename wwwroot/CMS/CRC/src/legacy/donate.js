import $ from 'jquery';
import './libs/equalHeight.js';
import './libs/select2.min.js';
import './libs/perfectscrollbar.min.js';
import './libs/tipster.min.js';

const redcrossDonate = {};
let timeOut = null,
  learnMoreWidth = $('.learn-more-container').width();

redcrossDonate.setEqualHeight = function () {
  const donateRows = $('.ctas .panel');
  if ($('.ctas .panel').length) {
    equalheight(donateRows);
  }
};
redcrossDonate.setEqualHeight();

// Resize handler
window.onresize = function () {
  if (timeOut !== null) clearTimeout(timeOut);

  timeOut = setTimeout(() => {
    if ($('.ctas .panel').length) {
      redcrossDonate.setEqualHeight(); // set height of tablet columns
    }

    learnMoreWidth = $('.learn-more-container').width();
    if (!$('.learn-more-container').hasClass('open')) {
      $('.learn-more-container').css('left', `-${learnMoreWidth}px`);
    }

    // Set height of .form-contents on Alternate template
    if ($('.site-container').hasClass('alternate')) {
      $('.donate-now .form-contents').css({
        height: `${$('.donate-form .fieldset-fix').height()}px`,
      });
    }
  }, 200);
};
$(window).trigger('resize');

// Perfect scrollbar
// - Create custom scroller on learn more panels
if ($('.learn-more-panel').length) {
  $('.learn-more-panel').perfectScrollbar({
    suppressScrollX: true,
  });
}

// Tooltips
if ($('.tooltip').length) {
  $('.tooltip').tooltipster();
}

const disabledColor = '#ABABAB';

var donateForm = {
  appeals: null,
  appealBaseUrl: null,
  selectedAppeal: null,
  hashPreText: 'selected-appeal-',
  changeAppeal() {
    const appealGuid = $('.ddl-appeals').val();
    $('.show-hide-appeal').hide();
    $(`.show-hide-appeal.${appealGuid}`).fadeIn('slow');
    const currentListItem = $(`.learn-more-panel ul li.${appealGuid}`);
    $(currentListItem).parent().prepend(currentListItem);
    $('.learn-more-panel').scrollTop(0);
    if (donateForm.appeals != undefined) {
      let selectedAppeal = null;
      for (let i = 0; i < donateForm.appeals.length; i++) {
        if (donateForm.appeals[i].DocumentGuid == appealGuid) {
          selectedAppeal = donateForm.appeals[i];
        }
      }

      $('.price-points input[type="radio"]').prop('checked', false);
      $('.checkbox input').prop('checked', false);
      $('.checkbox input').prop('disabled', false);
      $('.txt-other').val('$');

      if (selectedAppeal == null) return;
      window.location.hash = donateForm.hashPreText + appealGuid;

      /* set background image. Instead of changing the background-image property, we're loading
      all images in the same location on the DOM with a z-index of -1 and setting whichever one is active to 1.
      This is because the client does not want to see any flickering because of CSS transitions. */
      if ($('.form-container').hasClass('interactive')) {
        $('.bg-donate').css('z-index', '-1');
        $('.bg-donate').each(function (index) {
          if ($(this).attr('data-guid') == (selectedAppeal.DocumentGuid)) {
            $(this).css('z-index', '1');
          }
        });
      } else { 
        let nonInteractiveImage = 'url("/CRC/img/donate/donate-bg-with-left.jpg") repeat scroll 0 0#ffffff';
        $('.form-container').css('background', nonInteractiveImage); 
      }

      $('input.price-point-1').val(selectedAppeal.PricePoint1);
      $('input.price-point-2').val(selectedAppeal.PricePoint2);
      $('input.price-point-3').val(selectedAppeal.PricePoint3);
      $('label.price-point-1 span.value').text(selectedAppeal.PricePoint1Label);
      $('label.price-point-2 span.value').text(selectedAppeal.PricePoint2Label);
      $('label.price-point-3 span.value').text(selectedAppeal.PricePoint3Label);
      $('.chk-monthly input').prop('disabled', selectedAppeal.DisableMonthlyOption);
      $('.chk-monthly input').prop('checked', selectedAppeal.IsMonthlyDonationOnly);
      $('.chk-in-honour input').prop('disabled', !selectedAppeal.EnableInHonour);
      $('.chk-in-honour label').css('color', !selectedAppeal.EnableInHonour ? disabledColor : '');
      $('li.price-point-1 a span').text(selectedAppeal.PricePoint1Label);
      $('li.price-point-2 a span').text(selectedAppeal.PricePoint2Label);
      $('li.price-point-3 a span').text(selectedAppeal.PricePoint3Label);
      $('li.price-point-1').attr('data-value', selectedAppeal.PricePoint1Label);
      $('li.price-point-2').attr('data-value', selectedAppeal.PricePoint2Label);
      $('li.price-point-3').attr('data-value', selectedAppeal.PricePoint3Label);

      if (
        selectedAppeal.CodeName != undefined &&
        selectedAppeal.CodeName != null &&
        selectedAppeal.CodeName != ''
      ) {
        try {
          history.pushState(
            selectedAppeal,
            selectedAppeal.CodeName,
            `${donateForm.appealBaseUrl + selectedAppeal.CodeName}#${appealGuid}`,
          );
          document.title = selectedAppeal.PageTitle;
        } catch (ex) {}
      }

      donateForm.selectedAppeal = selectedAppeal;
    }
  },

  pageAppealLoadAt: new Date(),
  initDonateFormTracking() {
    // executes with JQuery mousedown on button in form
    $('.btn-donate').click((event) => {
      const appealTelNum1 = $('#appealTelNum1').val();
      if (Math.abs(new Date() - donateForm.pageAppealLoadAt) < 1000) {
        event.preventDefault();
        return false;
      }

      if (appealTelNum1 != undefined) {
        if (appealTelNum1.length > 0) {
          event.preventDefault();
          return false;
        }
      }

      event.preventDefault();
      let donationWidgetValue = '';
      let donationWidgetLabel;
      let donationWidgetCycle;

      // gathers information on the Donation Cycle.
      if ($('.chk-monthly input').checked) {
        donationWidgetCycle = 'Monthly';
      } else {
        donationWidgetCycle = 'One Time';
      }

      // gathers information on the label
      let getInputAmountText = $('.txt-other, input.donate-price-point-value').val();
      getInputAmountText = getInputAmountText.replace('$', '');
      if (getInputAmountText.length > 0) {
        // something was entered in OTher text area.  Ensure it's valid
        const getAmount = GetValidatedAmount(getInputAmountText);
        if (getAmount > 0) {
          donationWidgetLabel = 'Entered';
          donationWidgetValue = Math.round(getAmount);
          // $(".txt-other").val(getAmount);      // set the textbox value to the "possibly" updated new amount
        } else {
          return false;
        }
      } else {
        donationWidgetLabel = 'Selected';

        const selectedValue = $("input[type='radio'][name='pricepoint']:checked");
        if (selectedValue.length > 0) {
          donationWidgetValue = selectedValue.val();
        }
      }

      // sends event into GA
      dataLayer.push({
        event: 'generic-event',
        eventCategory: ' Donate Main Form Widget (Drop Down)',
        eventAction: donationWidgetCycle,
        eventLabel: donationWidgetLabel,
        eventValue: donationWidgetValue,
      });
      $('.donate-bar-hash').val(ga.getAll()[0].get('linkerParam'));
      $('.btn-donate').off('click');
      setTimeout(() => {
        // after 1 second, submit the form
        $('.btn-donate').trigger('click');
      }, 1000);
      return true;
    });
  },

  preloadDonateImages() {
    // Preloading background images to prevent flickering due to CSS rendering.
    for (var i = 0; i < donateForm.appeals.length; i++) {
      $('.interactive').prepend('<div data-guid="' + donateForm.appeals[i].DocumentGuid + '" ' + 'class="bg-donate" style="background-image: url(' + donateForm.appeals[i].BackgroundImage + ')"></div>');
      $('.bg-donate').css('z-index', '-1');
      $('.bg-donate').css('position', 'absolute');
      $('.bg-donate').css('height', '100%');
      $('.bg-donate').css('width', '100%');
      $('.bg-donate').css('margin', '-30px');
    }
  },

  init() {
    donateForm.initDonateFormTracking();
    let selectedAppeal = window.location.hash;

    if (selectedAppeal.length > 1) {
      selectedAppeal = selectedAppeal.substring(1); // remove #
      selectedAppeal = selectedAppeal.replace(donateForm.hashPreText, '');
    }

    if (selectedAppeal == undefined || selectedAppeal == 'null' || selectedAppeal == '') {
      selectedAppeal = $('.ddl-appeals').val();
    } else {
      $('.ddl-appeals').val(selectedAppeal);
    }

    // Add .active class to donate navigation
    $('.donate-select-nav li').each(function () {
      const value = $(this).data('value');
      if (value === selectedAppeal) {
        $(this).addClass('active');
      }
    });

    // Alternate Donate selection nav
    $('.donate-price-points-nav li.textinput input').val('$');
    $('.donate-select-nav li a').click(function (e) {
      e.preventDefault();

      let el = $(this).parent(),
        index = el.index(),
        val = el.data('value');

      if (!el.hasClass('active')) {
        // Set value and trigger change event
        $('.ddl-appeals').select2('val', val).trigger('change');
        el.addClass('active').siblings('li').removeClass('active');
      }

      return false;
    });

    // Alternate Donate Price Points Nav
    $('.donate-price-points-nav li a').click(function (e) {
      e.preventDefault();
      let el = $(this).parent(),
        index = el.index(),
        val = el.data('value');
      if (!el.hasClass('active')) {
        // Set value and trigger change event
        $('.price-points li input[type=radio]').eq(index).prop('checked', true);
        el.siblings('li').removeClass('active');
        el.addClass('active');

        if (val === 'Other') {
          $('.donate-price-points-nav li.textinput input').focus().val('');
        } else {
          $('.donate-price-points-nav li.textinput input').val('$');
        }
      }
    });

    // Set focus state of input
    $('.donate-price-points-nav li.textinput input').on('click', function () {
      const val = $(this).val();
      if (!$('.donate-price-points-nav li[data-value="Other"]').hasClass('active')) {
        $('.donate-price-points-nav li[data-value="Other"] a').trigger('click');
      }
      $('.price-points input.txt-other').val(val);
    });

    // Set value of other fom input
    $('.donate-price-points-nav li.textinput input').on('input', function () {
      const val = $(this).val();
      $('.price-points input.txt-other').val(val);
    });

    $('.ddl-appeals').change(() => {
      donateForm.changeAppeal();
    });

    $('.learn-more-tab').click((e) => {
      $('.learn-more-container').toggleClass('open');
      if ($('.learn-more-container').hasClass('open')) {
        $('.learn-more-container').animate({ left: 0 }, 'slow');
      } else {
        $('.learn-more-container').animate({ left: -learnMoreWidth }, 'slow');
      }
      e.stopPropagation();
    });
    $('.txt-other').val('$');
    $(document).click(() => {
      $('.learn-more-container').removeClass('open');
      $('.learn-more-container').animate({ left: -learnMoreWidth }, 'slow');
    });
    $('.learn-more-container').click((e) => {
      const target = $(e.target);
      if (!target.is('span.close')) {
        e.stopPropagation();
      }
    });

    $('.donate-form select').css('width', '100%');
    $('.donate-form select').select2();
    $('.price-points input').focus(() => {
      $('.txt-other').val('$');
    });
    $('.txt-other').focus(function () {
      $(this).val('');
      $('input.price-point-4').prop('checked', true);
    });

    $('.chk-monthly input').change(function () {
      if (donateForm.selectedAppeal.AllowMonthlyInHonour) return;

      const EnableInHonour = donateForm.selectedAppeal.EnableInHonour;

      $('.chk-in-honour input').prop('disabled', $(this).is(':checked') || !EnableInHonour);
      $('.chk-in-honour input').prop('checked', false);
      if ($(this).is(':checked') || !EnableInHonour) {
        $('.chk-in-honour label').css('color', disabledColor);
      } else {
        $('.chk-in-honour label').css('color', '');
      }
    });

    $('.chk-in-honour input').change(function () {
      if (donateForm.selectedAppeal.AllowMonthlyInHonour) return;
      $('.chk-monthly input').prop('disabled', $(this).is(':checked'));
      $('.chk-monthly input').prop('checked', false);
      if ($(this).is(':checked')) {
        $('.chk-monthly label').css('color', disabledColor);
      } else {
        $('.chk-monthly label').css('color', '');
      }
    });
    
    // uncomment the following variable to test interactive backgrounds for the donate form
    const isInteractive = true;

    //check for Optimizely setting. Optimizely is set by CRC themselves and they pass in a variable to us as 'isInteractive'
    if (typeof isInteractive !== "undefined" && isInteractive){
      $('.form-container').addClass('interactive'); 
      donateForm.preloadDonateImages();
    }

    donateForm.changeAppeal();

    if ($('.ros-banner').length) {
      $('footer').css('margin', '0 0 100px');
    }

    // ROS Banner override
    $('.ros-banner .Banner').removeAttr('onclick');

    $('.ros-banner .Banner').removeAttr('onmouseup');
  },
};

window.donateForm = donateForm;

// Allow numeric and $ only!
$(
  'input#p_lt_ctl03_CRC_Widget_DonateForm_txtOther, .donate-price-points-nav li.textinput input, input#p_lt_ctl06_CRC_Widget_DonateForm_txtOther',
).keydown((e) => {
  if (
    $.inArray(e.keyCode, [188, 46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) ||
    // Allow: home, end, left, right, down, up
    (e.keyCode >= 35 && e.keyCode <= 40)
  ) {
    // let it happen, don't do anything
    return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
});

$('.donate-track-tablet').click(function (event) {
  event.preventDefault();
  const selectedAppeal = $(this).data('link');
  const language = $(this).data('language');
  const gaID = ga.getAll()[0].get('linkerParam');
  const finalUrl = `${selectedAppeal}&langpref=${language}&${gaID}`;
  window.location.href = finalUrl;
});

/*
Alternate Form
//donateForm.init();
$('.site-container').removeClass('left-nav').addClass('full-width alternate');
$('.columns.donate-now').removeClass('small-9');
$('.form-container').height($('.fieldset-fix').height());
$('.form-contents').height($('.fieldset-fix').height());
*/
