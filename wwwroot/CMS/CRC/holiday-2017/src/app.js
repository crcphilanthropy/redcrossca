import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { reducer } from 'reducers';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'scss/app';
import BannerImage from 'containers/banner-image-container';
import Donation from 'containers/donate-page-container';
import DonationTest from 'containers/donate-page-container-2';


const holidayStore = createStore(reducer);

const Parent = () => {
  return (
    <Router>
      <main>
        <Route path="/perfectgifts" render={ (props) => <BannerImage version="1" {...props} /> } />
        <Route path="/survivalkits" render={ (props) => <BannerImage version="2" {...props} /> } />
        <Route path="/cadeauideal" render={ (props) => <BannerImage version="1" {...props} /> } />
        <Route path="/troussedesurvie" render={ (props) => <BannerImage version="2" {...props} /> } />
        <Route path="/perfectgifts" component={Donation} />
        <Route path="/survivalkits" component={DonationTest} />
        <Route path="/cadeauideal" component={Donation} />
        <Route path="/troussedesurvie" component={DonationTest} />
      </main>
    </Router>
  );
};

render(<Provider store = { holidayStore }><Parent /></Provider>, document.getElementById('holiday2017'));
