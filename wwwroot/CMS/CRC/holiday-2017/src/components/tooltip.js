import React from 'react';
import { Provider, connect } from 'react-redux';
import { donateAction, tooltipAction, resolveString } from '../reducers';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  itemsSelected: state.itemsSelected,
  isCustomDonate: state.isCustomDonate,
  lastItemSelectedMessage: state.lastItemSelectedMessage,
  isItemHover: state.isItemHover
});

class Tooltip extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { donateAmount, itemsSelected, textData, isCustomDonate, lastItemSelectedMessage, isItemHover, version } = this.props;
    let toolTipMessage1 = '';
    let toolTipMessage2 = '';
    let toolTipMessage3 = '';
    if (!isCustomDonate && !isItemHover) {
      const toolTipPreMessages = resolveString(textData.TooltipDonationMessagePre);
      const toolTipPostMessages = resolveString(textData.TooltipDonationMessagePost);
      toolTipMessage1 = itemsSelected > 0 ? toolTipPreMessages[itemsSelected] : '';
      toolTipMessage3 = itemsSelected > 0 ? toolTipPostMessages[itemsSelected] : '';
      toolTipMessage2 = lastItemSelectedMessage[lastItemSelectedMessage.length - 1];
    } else if (isCustomDonate && !isItemHover) {
      // preset custom donation types based ono amount entered on input box or clicked on gift arrays.
      const customDonationTypes = {
        MICRO: donateAmount > 0 && donateAmount <= 10,
        SMALL: donateAmount > 10 && donateAmount <= 25,
        MEDIUM: donateAmount > 25 && donateAmount <= 100,
        LARGE: donateAmount > 100 && donateAmount <= 199.99,
        FULL: donateAmount === 200,
        OVER: donateAmount > 200
      };
      const toolTipCustomPreMessages = resolveString(textData.TooltipUserDefinedAmountPre);
      const toolTipCustomPostMessages = resolveString(textData.TooltipUserDefinedAmountPost);
      // Switch statement to set the tooltip message based on the custom donation types/level.
      switch (true) {
        case (donateAmount === 0): {
          toolTipMessage1 = '';
          toolTipMessage3 = '';
          break;
        }
        case (customDonationTypes.MICRO): {
          toolTipMessage1 = toolTipCustomPreMessages['micro'];
          toolTipMessage3 = toolTipCustomPostMessages['micro'];
          break;
        }
        case (customDonationTypes.SMALL): {
          toolTipMessage1 = toolTipCustomPreMessages['small'];
          toolTipMessage3 = toolTipCustomPostMessages['small'];
          break;
        }
        case (customDonationTypes.MEDIUM): {
          toolTipMessage1 = toolTipCustomPreMessages['medium'];
          toolTipMessage3 = toolTipCustomPostMessages['medium'];
          break;
        }
        case (customDonationTypes.LARGE): {
          toolTipMessage1 = toolTipCustomPreMessages['large'];
          toolTipMessage3 = toolTipCustomPostMessages['large'];
          break;
        }
        case (customDonationTypes.FULL): {
          toolTipMessage1 = toolTipCustomPreMessages['full'];
          toolTipMessage3 = toolTipCustomPostMessages['full'];
          break;
        }
        case (customDonationTypes.OVER): {
          toolTipMessage1 = toolTipCustomPreMessages['grand'];
          toolTipMessage3 = toolTipCustomPostMessages['grand'];
          break;
        }
        default: {
          toolTipMessage1 = '';
          toolTipMessage3 = '';
        }
      }
    }

    return (
      <div className="item-tooltip">
        {
          itemsSelected > 0 ?
            <p>{ toolTipMessage1 } <span className="normal">{ toolTipMessage2 }</span> <span className="red">{ toolTipMessage3 }</span></p> :
          version === 'v2' && itemsSelected === 0 ?
            <p><span className="red">{ toolTipMessage1 }</span><span className="normal"> { toolTipMessage3 }</span></p> :
          version === 'v1' && itemsSelected === 0 ?
            <p><span>{ toolTipMessage1 }</span><span className="normal"> { toolTipMessage3 }</span></p> : ''
        }
      </div>
    );
  }
}


export default connect(mapStateToProps)(Tooltip);
