import React from 'react';
import { Provider, connect } from 'react-redux';
import { resolveString } from '../reducers';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  itemsSelected: state.itemsSelected,
  isCustomDonate: state.isCustomDonate,
  lastItemSelectedMessage: state.lastItemSelectedMessage,
  currentDomesticAmount: state.currentDomesticAmount,
  currentInternationalAmount: state.currentInternationalAmount

});

class DonateOptions extends React.Component {
  constructor(props) {
    super(props);
  }

  // Gets the value of a specific query string parameter from the URL. Returns null if the query string does not exist.
  getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec((window.location.search).toLowerCase());
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }

  // calculates and pushes the analytics code to the datalayer array variable for cardinalPath
  sendAnalyticsCode = (type) => {
    const {itemsSelected, lastItemSelectedMessage, donationItems, isCustomDonate, donateAmount} = this.props;
    let donatedItems = [];
    if (itemsSelected > 0) {
      donatedItems = donationItems.filter((item) => {
        return lastItemSelectedMessage.indexOf(resolveString(item.tooltip)) > -1;
      });
    } else if (isCustomDonate && document.getElementById('user-amount').value !== '') {
      donatedItems[0] = {
        codeName: 'Giftbox Other - ' + donateAmount,
        price: donateAmount
      };
    } else {
      donatedItems[0] = {
        codeName: 'Giftbox - ' + donateAmount,
        price: donateAmount
      };
    }
    // Individual items
    let i;
    for (i = 0; i < donatedItems.length; i++){
      window.dataLayer.push({
        'event': 'holiday-event',
        'eventAction': 'donateditem',
        'eventLabel': donatedItems[i].codeName,
        'eventValue': donatedItems[i].price
      });
    }
    // Group of items
    const donatedItemsNames = [];
    let x;
    for (x = 0; x < donatedItems.length; x++){
      donatedItemsNames.push('1' + donatedItems[x].codeName);
    }
    window.dataLayer.push({
      'event': 'holiday-event',
      'eventAction': 'donateditem grouping',
      'eventLabel': donatedItemsNames.join(', '),
      'eventValue': donateAmount
    });
    // Donation Type
    window.dataLayer.push({
      'event': 'donation-type',
      'donationType': type
    });
  }

  //Calculates the percentage split between domestic and international donation amount from the total donation and returns the formatted string.
  getDonationPercentageSplit = () => {
    const { donateAmount, textData, currentDomesticAmount, currentInternationalAmount, isCustomDonate } = this.props;
    let domesticPercentage = 0;
    let internationalPercentage = 0;
    if (!isCustomDonate) {
      domesticPercentage = donateAmount === 0 ? 0 : Math.round((currentDomesticAmount / donateAmount) * 100);
      internationalPercentage = donateAmount === 0 ? 0 : 100 - domesticPercentage;
    } else {
      domesticPercentage = 50;
      internationalPercentage = 50;
    }

    return domesticPercentage + ',' + internationalPercentage;
  }

  /* As per Cardinal Path's instruction, a 300 milisecond delay is needed before pushing the data to Google Analytics.
  Hence the purpose for this function.
  The event will need to be processed here as passing event objects to another function is not allowed.
  This function also builds the URL by combining percentages, utm variables, google analytics, donation amount etc. to the final URl */
  processDonation = (e) => {
    e.preventDefault();
    const type = e.currentTarget.getAttribute('data-type');
    setTimeout(() => {
      const { textData, donateAmount } = this.props;
      this.sendAnalyticsCode(type);
      const gaParams = (typeof (window.ga) !== 'undefined' && window.ga.hasOwnProperty('getAll')) ? '&' + window.ga.getAll()[0].get('linkerParam') : '';
      const percentageSplit = this.getDonationPercentageSplit();
      const analyticsParam = [];
      analyticsParam.push({'utm_source': this.getParameterByName('utm_source')});
      analyticsParam.push({'utm_medium': this.getParameterByName('utm_medium')});
      analyticsParam.push({'utm_content': this.getParameterByName('utm_content')});
      analyticsParam.push({'utm_campaign': this.getParameterByName('utm_campaign')});
      analyticsParam.push({'name': this.getParameterByName('name')});
      // Builds the final analytics query string. We don't do this directly by calling getParameterByName() because we need to do null checks.
      const finalAnalyticsParams = analyticsParam.reduce((acc, currentParam) => {
        const key = Object.keys(currentParam);
        acc = currentParam[key] !== null && currentParam[key] !== '' ? acc + '&' + key + '=' + currentParam[key] : acc;
        return acc;
      }, '');
      const urlVersion = this.props.version === 'v1' ? textData.DonationTypes.control : textData.DonationTypes.test;
      const baseDonationURL = type === 'own donation' ? resolveString(urlVersion.self) : resolveString(urlVersion.honor);
      const finalDonationURL = baseDonationURL + finalAnalyticsParams + '&en_txn4=' + percentageSplit + '&amount=' + donateAmount + gaParams;
      window.location.href = finalDonationURL;
    }, 300);
  }

  render() {
    const { textData } = this.props;
    return (
      <div>
        <h2 className="donate-head">{ resolveString(textData.DonationTypeHeader) }</h2>
        <div className="donate-box">
          <button className="donate-option" data-type="own donation" onClick={ (e) => this.processDonation(e) }>
            <div className="donate-icon">
              <img src="/crc/holiday-2017/assets/ui/hand.svg" alt="" />
            </div>
            <div className="donate-tagline">
              <p>{ resolveString(textData.DonationTypeSelfText) }</p>
            </div>
          </button>
          <button className="donate-option" data-type="on behalf" onClick={ (e) => this.processDonation(e) }>
            <div className="donate-icon">
              <img src="/crc/holiday-2017/assets/ui/heart.svg" alt="" />
            </div>
            <div className="donate-tagline">
              <p>{ resolveString(textData.DonationTypeOnBehalfEcard) }</p>
            </div>
          </button>
        </div>
        <p className="lightbox-footer">{ resolveString(textData.DonationTypeTaxReturnText) }</p>
      </div>
    );
  }
}

export default connect(mapStateToProps)(DonateOptions);
