import React from 'react';
import FocusTrap from 'react-focus-trap';
import { Provider, connect } from 'react-redux';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  itemsSelected: state.itemsSelected,
  isCustomDonate: state.isCustomDonate,
  lastItemSelectedMessage: state.lastItemSelectedMessage,
  currentDomesticAmount: state.currentDomesticAmount,
  currentInternationalAmount: state.currentInternationalAmount

});

class Lightbox extends React.Component {

  componentDidMount() {
    window.addEventListener('keydown', this.props.onKeyDown);
    document.body.classList.add('no-scroll');
    document.body.addEventListener('touchmove', this.freezeVp, false);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.props.onKeyDown);
    document.body.classList.remove('no-scroll');
    document.body.removeEventListener('touchmove', this.freezeVp, false);
  }

  freezeVp(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div className='dialog' aria-hidden='false'>
        <div className='mask' onClick={this.props.onClose} />
        <div className='window' role='dialog' tabIndex='0' id='modal'>
          {this.props.children}
          <FocusTrap>
            <button className='btn-close' onClick={this.props.onClose}>
              x
              <span className='show-for-sr'>Close lightbox</span>
            </button>
          </FocusTrap>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Lightbox);
