import React from 'react';
import { resolveString, resolveCurrency } from '../reducers';


class Item extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {itemClick, itemHover, activeImage, defaultImage, donationMessage, name, codeName, price, tooltip, type } = this.props;

    return (
      <div className="item-container" data-key={price}>
        <div className="item" onClick={ (e) => itemClick(name, codeName, price, tooltip, type, e) } onMouseEnter={ (e) => itemHover(e) }>
          <div className="front" style={{backgroundImage: "url('" + defaultImage + "')"}}>
            <div className="check"></div>
            <p className="gift-amount">{resolveCurrency(price)}</p>
            <p className="gift-name">{name}</p>
          </div>
          <div className="back" style={{backgroundImage: "url('" + activeImage + "')"}}>
            <div className="check"><img src="/crc/holiday-2017/assets/ui/checkmark.svg" alt="" /></div>
            <p className="gift-detail">{donationMessage}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Item;
