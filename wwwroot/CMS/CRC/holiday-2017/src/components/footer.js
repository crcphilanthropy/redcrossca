import React from 'react';
import { Provider, connect } from 'react-redux';
import { resolveString } from '../reducers';

const mapStateToProps = state => ({
  textData: state.textData
});


class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  getFullYear() {
    const date = new Date();
    return date.getFullYear();
  }

  sendAnalyticsCode() {
    window.dataLayer.push({
      'event': 'donation-privacy-policy'
    });
  }

  /* Function to build the url to toggle to between English and French.
  Grabs the current culture and set the culture to toggle to based on the current one.
  Grabs the version of the site (control or test) for the pathname
  builds the string by replacing the redcross keyword according to the destination culture in the url and the old version culture with the new one. */
  getToggleURL() {
    const currentCulture = document.querySelector('html').getAttribute('lang');
    const newCulture = currentCulture === 'en' ? 'fr' : 'en';
    const hostName = this.props.textData.FooterToggleHostname;
    const urlVersion = this.props.version === 'v1' ? resolveString(this.props.textData.FooterLink.control) : resolveString(this.props.textData.FooterLink.test);
    const newURL = window.location.href.split('#')[0].replace(hostName[currentCulture], hostName[newCulture]).replace(location.pathname, urlVersion);
    return newURL;
  }

  render() {
    return (
      <footer>
        <p>{ resolveString(this.props.textData.MatchTextFinePrint) } <a href={ resolveString(this.props.textData.FooterTermsLink) } target="_blank" rel="noopener noreferrer">{ resolveString(this.props.textData.FooterTermsText) }</a>
          { resolveString(this.props.textData.FooterCopyText) }
          <br /> { resolveString(this.props.textData.FooterCharityNo) }
          <a href={ this.getToggleURL() }>{ resolveString(this.props.textData.FooterLinkText) }</a>
          | <a target="_blank" href={ resolveString(this.props.textData.FooterPrivacyLink) } rel="noopener noreferrer" onClick={ this.sendAnalyticsCode } >{ resolveString(this.props.textData.FooterPrivacyText) }</a> © { resolveString(this.props.textData.RedCrossLogoAlt) } <span>{ this.getFullYear() }</span>
        </p>
      </footer>
    );
  }
}

export default connect(mapStateToProps)(Footer);
