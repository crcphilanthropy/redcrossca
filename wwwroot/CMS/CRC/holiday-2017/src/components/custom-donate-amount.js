import React from 'react';
import { Provider, connect } from 'react-redux';
import { donateAction, resolveString } from '../reducers';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  isCustomDonate: state.isCustomDonate
});

class CustomDonateInput extends React.Component {
  constructor(props) {
    super(props);
  }

  /* submitting the custom donation amount to the redux store after doing validation (max amt defaults to 10K, only numbers allowed in the input)
  This function also removes any non-numeric characters from the input box when the user submits the amount as per previous year's behavior */
  handleSubmit = (e) => {
    if (e.target.value.length === 0 && document.querySelectorAll('.added, .array-selected').length === 0) {
      this.props.removeAllDonation(e);
    }

    if (e.target.value.length > 0) {
      const currentDonateTotal = this.props.donateAmount;
      e.target.value = e.target.value.replace(/,+/g, '.');
      e.target.value = e.target.value.replace(/[^\d.-]/g, '');
      let customDonateAmount = parseInt(e.target.value, 10);
      e.target.value = customDonateAmount;
      const isInputValid = !isNaN(customDonateAmount) && customDonateAmount > 0;
      if (isInputValid && customDonateAmount > 10000) {
        customDonateAmount = 10000;
        this.props.removeAllDonation(e);
        e.target.value = 10000;
        this.props.addDonation(customDonateAmount, true, 'custom');
      } else if (isInputValid) {
        const currentInputAmount = e.target.value;
        this.props.removeAllDonation(e);
        e.target.value = currentInputAmount;
        this.props.addDonation(customDonateAmount, true, 'custom');
      } else {
        this.props.removeAllDonation(e);
      }
      //For analytics purposes - Cardinal Path
      if (isInputValid) {
        window.dataLayer.push({
          'event': 'holiday-event',
          'eventAction': 'additem',
          'eventLabel': 'Giftbox Other - ' + customDonateAmount,
          'eventValue': customDonateAmount
        });
      }
    }
  }

 //Whenever the user types on the input box, if it's not the Enter key, remove all donation, otherwise submit the donation amount to the store.
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.target.blur();
    } else {
      //this.props.removeAllDonation(e);
    }
  }

  render() {
    return (
      <span className="currency-symbol">
        <label htmlFor="user-amount" className="show-for-sr">Enter your own amount</label>
        <input type="tel" id="user-amount" autoComplete="off" onKeyDown={ this.handleKeyPress } onBlur={ this.handleSubmit } placeholder={this.props.placeholder} />
      </span>
    );
  }
}

export default connect(mapStateToProps)(CustomDonateInput);
