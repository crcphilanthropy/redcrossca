import $ from 'jquery';
import React from 'react';
import { Provider, connect } from 'react-redux';
import ProgressBar from 'react-progressbar.js';
import { resolveString } from '../reducers';
const Circle = ProgressBar.Circle;

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  progress: state.donation > 0 ? state.donation / 200 : state.fakeHoverDonation,
  progressPercentage: Math.floor(state.donation / 200 * 100),
  itemsSelected: state.itemsSelected,
  isItemHover: state.isItemHover
});


class Globe extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    //this.addRev();
  }

  componentDidUpdate() {
    if (this.props.donateAmount > 0) {
      //this.removeRev();
    }

    if (this.props.donateAmount === 0) {
      //this.addRev();
    }
  }

   // Updates Globe inner text based on the calculated total percentage of selected or custom amount.
  updateText() {
    if (this.props.progress > 1 && !this.props.isItemHover) {
      return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p> <h2 class="red">100%</h2><p> ' + resolveString(this.props.textData.Full) + '</p>');
    }

    if (this.props.progressPercentage > 0 && !this.props.isItemHover) {
      return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p><h2 class="red">' + this.props.progressPercentage + '%</h2> <p>' + resolveString(this.props.textData.Full) + '</p>');
    }

    return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p>');
  }

  addRev() {
    const items = document.querySelectorAll('.item-container');

    Array.prototype.forEach.call(items, item => item.addEventListener('mouseenter', this.progressRev));
  }

  removeRev() {
    const items = document.querySelectorAll('.item-container');

    Array.prototype.forEach.call(items, item => item.removeEventListener('mouseenter', this.progressRev));
  }

  progressRev() {
    const bar = document.getElementsByTagName('path');
    bar[1].setAttribute('class', 'globe-animate');

    window.setTimeout(() => {
      bar[1].removeAttribute('class', 'globe-animate');
    }, 400);
  }

  render() {

    const options = {
      color: '#EE0000',
      strokeWidth: 2,
      trailWidth: 2,
      trailColor: '#e7e9ed',
      text: {
        style: {
          color: '#404040',
          position: 'absolute',
          left: '50%',
          top: '50%',
          margin: 0,
          padding: 0,
          textAlign: 'center',
          transform: {
            prefix: true,
            value: 'translate(-50%, -50%)'
          }
        }
      }
    };

    const containerStyle = {
      width: '360px',
      height: '360px'
    };

    return (
      <div className="circle-box" id="circle">
        <Circle
          progress={this.props.progress > 1 ? 1 : this.props.progress}
          text={this.updateText()}
          options={options}
          initialAnimate
          containerStyle={containerStyle}
          containerClassName={'circle'}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps)(Globe);
