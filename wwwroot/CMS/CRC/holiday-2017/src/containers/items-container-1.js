import React from 'react';
import { Provider, connect } from 'react-redux';
import { donateAction, tooltipAction, resolveString } from '../reducers';
import Item from '../components/item';

const mapStateToProps = state => ({
  textData: state.textData,
  donationItems: state.donationItems,
  isCustomDonate: state.isCustomDonate,
  donateAmount: state.donation
});

class Items extends React.Component {
  constructor(props) {
    super(props);
  }

  //Checks to see if the user is on a touch-enabled device. If not, then perform a quick animation
  onItemHover = (e) => {
    if (!('ontouchstart' in window || navigator.maxTouchPoints)) {
      const { donateAmount, dispatch } = this.props;
      if (donateAmount === 0) {
        dispatch(donateAction.addFakeHoverDonation());
        setTimeout(function f(event) { dispatch(donateAction.removeFakeHoverDonation()); }, 90);
      }
    }
  }

  /* Toggles the item tile to make it click. Checks to see if the current gift item
  that was clicked was 'ticked' or 'unticked' and calls the add/subtract function accordingly. */
  onItemClick = (name, codeName, amount, message, type, e) => {
    const { dispatch, isCustomDonate } = this.props;
    if (isCustomDonate) {
      this.props.removeAllDonation(e);
    }
    const clickedItem = e.currentTarget;
    clickedItem.classList.toggle('added');
    if (clickedItem.className.search('added') >= 0) {
      this.props.addDonation(amount, false, type);
      dispatch(tooltipAction.addTooltipMessage(message));
      //For analytics purposes - Cardinal Path
      window.dataLayer.push({
        'event': 'holiday-event',
        'eventAction': 'additem',
        'eventLabel': codeName,
        'eventValue': amount
      });
    } else {
      this.props.removeDonation(amount, false, type);
      dispatch(tooltipAction.removeTooltipMessage(message));
    }
  }

  render() {
    const { donationItems } = this.props;
    const { textData } = this.props;
    return (
      <div className="row">
        <div className="small-6 medium-12 large-12 columns vertical-padding">
          <p className="label region">{ resolveString(textData.DonationItemTypeInternational) }</p>
          <div className="item-array">
            { donationItems.filter((i, index) => (index < 3)).map((item) =>
              <Item
                key={item.id}
                itemClick={this.onItemClick}
                itemHover={this.onItemHover}
                activeImage={item.activeImage}
                defaultImage={item.defaultImage}
                donationMessage={resolveString(item.donationMessage)}
                name={resolveString(item.name)}
                codeName={item.codeName}
                price={item.price}
                type={item.type}
                tooltip={resolveString(item.tooltip)}
              />
            )}
          </div>
        </div>

        <div className="small-6 medium-12 large-12 columns vertical-padding">
          <p className="label region">{ resolveString(textData.DonationItemTypeDomestic) }</p>
          <div className="item-array">
            { donationItems.filter((i, index) => (index > 2)).map((item) =>
              <Item
                key={item.price}
                itemClick={this.onItemClick}
                itemHover={this.onItemHover}
                activeImage={item.activeImage}
                defaultImage={item.defaultImage}
                donationMessage={resolveString(item.donationMessage)}
                name={resolveString(item.name)}
                codeName={item.codeName}
                price={item.price}
                type={item.type}
                tooltip={resolveString(item.tooltip)}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Items);
