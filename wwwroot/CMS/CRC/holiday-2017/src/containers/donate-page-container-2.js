import React from 'react';
import { Provider, connect } from 'react-redux';
import scrollMonitor from 'scrollMonitor';
import Break from 'react-break';

import Items from './items-container-1';
import GiftArray from './gift-array-container';
import ProgressBarLine from './progress-bar-container';
import MobileProgressBar from './mobile-progress-bar';
import Tooltip from '../components/tooltip';
import CustomDonateInput from '../components/custom-donate-amount';
import Lightbox from '../components/lightbox';
import DonateOptions from '../components/donate-options';
import Footer from '../components/footer';
import { donateAction, tooltipAction, resolveString, resolveCurrency } from '../reducers';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  itemsSelected: state.itemsSelected,
  isCustomDonate: state.isCustomDonate,
  lastItemSelectedMessage: state.lastItemSelectedMessage,
  isItemHover: state.isItemHover
});

const UIBreakpoints = {
  mobile: 0,
  desktop: 1025
};

class DonationTest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  componentDidMount() {
    const currentCultureCode = document.querySelector('html').getAttribute('lang') || 'en';
    window.dataLayer.unshift({
      'language': currentCultureCode,
      'variation': 'test'
    });
    this.watchScroll();
    this.changeHash();
    this.bodyClass();
  }

  // apply class to body
  bodyClass() {
    const v2 = document.querySelectorAll('.donate-v2');
    if (v2 !== null) {
      document.body.classList.add('body-v2');
    }
  }

   //Dispatches the selectAll action to the reducer to add max donation amount only from all tiled items to the store. Clears custom amount.
  onSelectAllDonation = (e) => {
    e.preventDefault();
    if (this.props.itemsSelected < 6) {
      const { dispatch, donationItems } = this.props;
      dispatch(donateAction.selectAll());
      dispatch(tooltipAction.addAllTooltipMessage());
      const tiledItemsList = document.querySelectorAll(' .item-container > .item');
      Array.prototype.forEach.call(tiledItemsList, (element, index) => element.classList.add('added'));
      const giftArrayList = document.querySelectorAll('.btn-array');
      Array.prototype.forEach.call(giftArrayList, (element, index) => element.classList.remove('array-selected'));
      document.getElementById('user-amount').value = '';
      // For analytics purposes - Cardinal Path
      let i;
      for (i = 0; i < donationItems.length; i++) {
        window.dataLayer.push({
          'event': 'holiday-event-add-all',
          'eventAction': 'additem-all',
          'eventLabel': donationItems[i].codeName,
          'eventValue': donationItems[i].price
        });
      }
      window.dataLayer.push({
        'event': 'holiday-event-select-all'
      });
    }
  }

  // Dispatches the removeAll action to the reducer to reset total donation amount to zero in the store. Clears custom amount.
  onRemoveAllDonation = (e) => {
    const { dispatch } = this.props;
    dispatch(donateAction.removeAll());
    dispatch(tooltipAction.removeAllTooltipMessage());
    const elementList = document.querySelectorAll('.added, .array-selected');
    Array.prototype.forEach.call(elementList, (element, index) => {
      element.classList.remove('added');
      element.classList.remove('array-selected');
    });
    if (document.activeElement.id !== 'user-amount') {
      e.preventDefault();
      document.getElementById('user-amount').value = '';
    }
    /* Google analytics code (cardinal path). Execute only when this handler is called from the 'reset' button.
    Since this handler function is called from other click events too. */
    if (e.currentTarget.classList.contains('btn-reset')) {
      window.dataLayer.push({
        'event': 'donation-reset'
      });
    }
  }

  // Helper function that calls the store to add a donation amount
  onAddDonation = (amount, isCustomDonate, type) => {
    const { dispatch } = this.props;
    dispatch(donateAction.add(amount, isCustomDonate, type));
  }

  // Helper function that calls the store to subtract a donation amount
  onRemoveDonation = (amount, isCustomDonate, type) => {
    const { dispatch } = this.props;
    dispatch(donateAction.remove(amount, isCustomDonate, type));
  }

  // lightbox functions section
  openLightbox = (e) => {
    e.preventDefault();

    const truck = document.getElementById('truck');
    const box = document.getElementById('box');

    if (truck !== null) {
      box.classList.add('box-hide');
      truck.classList.add('move');

      window.setTimeout(() => {
        truck.classList.remove('truck-visible');
      }, 400);

      window.setTimeout(() => {
        truck.classList.remove('move');
        this.setState({
          isOpen: true,
        });
      }, 500);
    } else {
      this.setState({ isOpen: true });
    }

    const modal = document.getElementById('modal');
    const urlModal = '#modal';
    history.pushState(undefined, undefined, urlModal);

    window.dataLayer.push({
      'event': 'donation-type-modal'
    });
  };

  closeLightbox = (e) => {
    e.preventDefault();

    this.setState({
      isOpen: false,
    });

    const truck = document.getElementById('truck');
    const box = document.getElementById('box');

    if (truck !== null) {
      box.classList.remove('box-hide');
      truck.classList.add('truck-visible');
    }

  };

  handleKeyDown = (e) => {
    if (e.keyCode === 27) {
      this.setState({
        isOpen: false
      });
    }
  };

  hideTooltip = (e) => {
    e.preventDefault();
    const tooltip = document.getElementById('mobile-tooltips');
    if (tooltip) {
      tooltip.classList.remove('visible');
    }
  };

  showTooltip = () => {
    const tooltip = document.getElementById('mobile-tooltips');
    if (tooltip) {
      tooltip.classList.add('visible');
    }
  };

  //scroll detection
  watchScroll() {
    const donateItems = document.getElementById('items-container');
    const checkout = document.getElementById('checkout');
    const elementWatcher = scrollMonitor.create(donateItems);

    elementWatcher.enterViewport(() => {
      checkout.classList.add('initialize');
    });
    elementWatcher.exitViewport(() => {
      checkout.classList.remove('initialize');
    });
  }

  // Per request from CRC, this function adds hash descriptions to the URL to guide the user.
  changeHash() {
    const p1 = document.getElementById(resolveString(this.props.textData.NavigationSections.p1));

    const p1elementWatcher = scrollMonitor.create(p1, -250);
    const urlHelp = '#' + resolveString(this.props.textData.NavigationSections.p1);
    const urlHow = '#' + resolveString(this.props.textData.NavigationSections.p2);

    p1elementWatcher.visibilityChange(() => {
      if (p1elementWatcher.isInViewport){
        history.replaceState(undefined, undefined, urlHelp);
      }
    });

    p1elementWatcher.exitViewport(() => {
      history.replaceState(undefined, undefined, urlHow);
    });

  }

  //Function to print out the Donation page header dynamically based on the 'name' query string in the URL
  printDynamicDonationHeader = () => {
    const baseDonateHeader = resolveString(this.props.textData.DefaultCampaign.donateHeader);
    const userName = this.getParameterByName('name');
    const dynamicDonateHeader = userName === null || userName === '' ? baseDonateHeader : userName + ', ' + baseDonateHeader.charAt(0).toLowerCase() + baseDonateHeader.slice(1);
    return dynamicDonateHeader;
  }

  // Gets the value of a specific query string parameter from the URL. Returns null if the query string does not exist.
  getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }

  render() {

    const { donateAmount, textData, itemsSelected, isItemHover, isCustomDonate } = this.props;

    return (
      <section className="donate slide-in donate-v2" id={ resolveString(this.props.textData.NavigationSections.p2) }>
        <div className="container">
          <h2 className="text-center" id="p2H2">{ this.printDynamicDonationHeader() }</h2>
          <Break
            breakpoints={UIBreakpoints}
            query={{ method: 'is', breakpoint: 'mobile' }}
          >
            <div className="match-mobile">
              <div className="match-icon">
                <img src="/crc/holiday-2017/assets/Aviva-Logo-Stacked.png" alt="" />
              </div>
              <p>{ resolveString(textData.MatchTextP2) } { resolveString(textData.MatchTextP2Doubled) }</p>
            </div>
          </Break>
          <div className="content">
            <Break
              breakpoints={UIBreakpoints}
              query={{ method: 'isAtLeast', breakpoint: 'desktop' }}
            >
              <div className="v2-progress">
                <ProgressBarLine />
                <div className="v2-tooltips">
                  <Tooltip version="v2" />
                </div>
              </div>
            </Break>
            <div className="flex-container">
              <div className="panel-left">
                <Break
                  breakpoints={UIBreakpoints}
                  query={{ method: 'isAtLeast', breakpoint: 'desktop' }}
                >
                  <p>{ resolveString(textData.DonationItemsInstruction) }</p>
                </Break>
                <Break
                  breakpoints={UIBreakpoints}
                  query={{ method: 'is', breakpoint: 'mobile' }}
                >
                  <p className="mobile-label condensed">{ resolveString(textData.MobileItemsInstruction) }</p>
                </Break>
                <div className="row">
                  <Break
                    breakpoints={UIBreakpoints}
                    query={{ method: 'is', breakpoint: 'mobile' }}
                  >
                    <p className="mobile-label condensed">{ resolveString(textData.MobileSelectAmount) }</p>
                    <div className="gift-array-mobile">
                      <GiftArray addDonation={this.onAddDonation} removeDonation={this.onRemoveDonation} removeAllDonation={this.onRemoveAllDonation} />
                      <div className="select-buttons">
                        <div className="user-defined-amount-v2">
                          <CustomDonateInput addDonation={this.onAddDonation} removeAllDonation={this.onRemoveAllDonation} placeholder={ resolveString(textData.GiftBoxPlaceholderText) } />
                        </div>
                      </div>
                    </div>
                    <p className="mobile-label condensed">{ resolveString(textData.MobileSelectItems) }</p>
                  </Break>
                  <div className="small-12 medium-12 large-12 columns" id="items-container">
                    <Items addDonation={this.onAddDonation} removeDonation={this.onRemoveDonation} removeAllDonation={this.onRemoveAllDonation} />
                  </div>
                </div>

                <Break
                  breakpoints={UIBreakpoints}
                  query={{ method: 'is', breakpoint: 'mobile' }}
                >
                  <div className="row select-buttons">
                    <div className="select-all">
                      <button className="btn-select" onClick={ this.onSelectAllDonation }>{ resolveString(textData.GiftBoxSelectAllText) }</button>
                    </div>
                  </div>
                </Break>
              </div>
              <Break
                breakpoints={UIBreakpoints}
                query={{ method: 'is', breakpoint: 'desktop' }}
              >
                <div className="panel-right gift-array">
                  <p>{ resolveString(textData.GiftArrayInstruction) }</p>
                  <GiftArray addDonation={this.onAddDonation} removeDonation={this.onRemoveDonation} removeAllDonation={this.onRemoveAllDonation} />
                  <div className="user-defined-amount-v2">
                    <CustomDonateInput addDonation={this.onAddDonation} removeAllDonation={this.onRemoveAllDonation} placeholder={ resolveString(textData.GiftBoxPlaceholderText) } />
                  </div>
                </div>
              </Break>
            </div>

            <div className="checkout-row" id="checkout">
              <Break
                breakpoints={UIBreakpoints}
                query={{ method: 'is', breakpoint: 'mobile' }}
              >
                <div
                  className={'mobile-tooltips ' +
                   (this.props.donateAmount > 0 && !this.props.isItemHover ? 'visible ' + donateAmount : null)
                  }
                  id="mobile-tooltips"
                >
                  <button className="btn-close btn-close-tooltip" onClick={this.hideTooltip}>x</button>
                  <Tooltip version="v2" />
                </div>
                <div>
                  <MobileProgressBar />
                </div>
              </Break>
              <div className="flex-container dark-bg-mobile">
                <div className="panel-left white-bg-desktop padding-around checkout-mobile match-flex">
                  <Break
                    breakpoints={UIBreakpoints}
                    query={{ method: 'is', breakpoint: 'desktop' }}
                  >
                    <div className="select-all">
                      <button className="btn-select" onClick={ this.onSelectAllDonation }>{ resolveString(textData.GiftBoxSelectAllText) }</button>
                    </div>
                  </Break>

                  <div className="donation-total">
                    <div>
                      <p>{ resolveString(textData.YourDonation) }</p>
                      <button
                        onClick={this.onRemoveAllDonation}
                        className={'btn-reset ' + (this.props.donateAmount > 0 && !isItemHover ? 'red' : null)}
                      >
                        { resolveString(textData.Reset) }
                      </button>
                    </div>
                    <p
                      className={'total ' + (this.props.donateAmount > 0 && !isItemHover ? 'active' : null)}
                    >
                      { this.props.donateAmount > 0 && !isItemHover ? resolveCurrency(donateAmount) : resolveCurrency(0) }
                    </p>
                  </div>
                </div>
                <div className="panel-right padding-left complete-mobile">
                  <button className="btn-complete" onClick={this.openLightbox} disabled={donateAmount === 0 || isItemHover}>{ resolveString(textData.CompleteDonationText) }</button>
                </div>
              </div>
            </div>
          </div>

          <div className="match-v2 margin-bottom">
            <div className="match-v2-icon">
              <img src="/crc/holiday-2017/assets/Aviva-Logo-Horizontal.png" alt="" />
            </div>
            <div className="match-v2-copy">
              <p> <span className="condensed">{ resolveString(textData.MatchTextP2) }</span> <span className="thin">{ resolveString(textData.MatchTextP2Doubled) }</span></p>
            </div>
          </div>
        </div>

        <Footer version="v2" />

        { this.state.isOpen
          ? <Lightbox
            onClose={this.closeLightbox}
            onKeyDown={this.handleKeyDown}
            version="v2"
          >
            <DonateOptions version="v2" />
          </Lightbox>
        : null }

      </section>

    );
  }
}

export default connect(mapStateToProps)(DonationTest);
