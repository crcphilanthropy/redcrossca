import React from 'react';
import { Provider, connect } from 'react-redux';
import ProgressBar from 'react-progressbar.js';
import { resolveString } from '../reducers';
const Line = ProgressBar.Line;

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  progress: state.donation > 0 ? state.donation / 200 : state.fakeHoverDonation,
  progressPercentage: Math.floor(state.donation / 200 * 100),
  isItemHover: state.isItemHover
});


class ProgressBarLine extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // this.addRev();
  }

  componentDidUpdate(prevProps, prevState) {
    // if (this.props.donateAmount > 0) {
    //   this.removeRev();
    // }

    // if (this.props.donateAmount === 0) {
    //   this.addRev();
    // }
  }

  // Updates progress bar text based on the calculated total percentage of selected or custom amount.
  updateText() {
    if (this.props.progress > 1 && !this.props.isItemHover) {
      return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p> <p>100% ' + resolveString(this.props.textData.Full) + '</p>');
    }

    if (this.props.progressPercentage > 0 && !this.props.isItemHover) {
      return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p><p>' + this.props.progressPercentage + '% ' + resolveString(this.props.textData.Full) + '</p>');
    }

    return ('<p>' + resolveString(this.props.textData.GlobalSurvivalKit) + '</p>');
  }

  // addRev() {
  //   const items = document.querySelectorAll('.item-container');

  //   Array.prototype.forEach.call(items, item => item.addEventListener('mouseenter', this.progressRev));
  // }

  // removeRev() {
  //   const items = document.querySelectorAll('.item-container');

  //   Array.prototype.forEach.call(items, item => item.removeEventListener('mouseenter', this.progressRev));
  // }

  // progressRev() {
  //   const bar = document.querySelectorAll('#line svg');
  //   bar[0].children[1].classList.add('path-animate');

  //   window.setTimeout(() => {
  //     bar[0].children[1].classList.remove('path-animate');
  //   }, 400);
  // }

  updateIndicators() {
    const indicatorPosition = {
      left: this.props.progress * 100 + '%',
    };

    const indicatorPositionFull = {
      left: '97%'
    };

    return (
      <div>
        <img src="/crc/holiday-2017/assets/ui/box.png" alt="" style={this.props.progress >= 1 ? indicatorPositionFull : indicatorPosition} className="box" id="box" />
        <img src="/crc/holiday-2017/assets/ui/triangle.png" alt="" style={this.props.progress >= 1 ? indicatorPositionFull : indicatorPosition} className="indicator" />
        <img src="/crc/holiday-2017/assets/ui/Truck.png" alt="" className={ this.props.donateAmount > 0 ? 'truck truck-visible' : 'truck'} id="truck" />
      </div>
    );
  }

  staticIndicators() {
    const indicatorStatic = {
      left: 0
    };

    return (
      <div>
        <img src="/crc/holiday-2017/assets/ui/box.png" alt="" style={indicatorStatic} className="box" id="box" />
        <img src="/crc/holiday-2017/assets/ui/triangle.png" alt="" style={indicatorStatic} className="indicator" />
      </div>
    );
  }

  render() {

    const options = {
      color: '#EE0000',
      strokeWidth: 1.2,
      trailWidth: 1.2,
      trailColor: '#E7E9ED',
      duration: 500,
      svgStyle: {
        borderRadius: '1rem',
      },
      text: {
        style: {
          color: '#FFF',
          background: '#EE0000',
          fontSize: '1.4rem',
          position: 'absolute',
          height: '6rem',
          width: '17%',
          left: '0',
          bottom: '-8.8rem',
          margin: 0,
          padding: '0.6rem',
          transform: {
            prefix: true,
          }
        }
      }
    };

    const containerStyle = {
      width: '100%',
      height: '1.2rem'
    };

    // const indicatorPosition = {
    //   left: 'calc(' + this.props.progress * 100 + '%)'
    // };

    // const indicatorPositionFull = {
    //   left: 'calc(100% - 3rem)'
    // };

    return (
      <div id="line" className="progress-line">
        <Line
          progress={this.props.progress > 1 ? 1 : this.props.progress}
          text={this.updateText()}
          options={options}
          initialAnimate
          containerStyle={containerStyle}
          containerClassName={'line'}
        />
        { !this.props.isItemHover && this.props.donateAmount > 0 ? this.updateIndicators() : this.staticIndicators() }
      </div>
    );
  }
}

export default connect(mapStateToProps)(ProgressBarLine);
