import React from 'react';
import { Provider, connect } from 'react-redux';
import { donateAction, tooltipAction, resolveString, resolveCurrency } from '../reducers';
import Item from '../components/item';

const mapStateToProps = state => ({
  textData: state.textData,
  donationItems: state.donationItems,
  donateAmount: state.donation
});

class GiftArray extends React.Component {
  constructor(props) {
    super(props);
  }

  /* Toggles the gift array buttons to make it click. Checks to see if the current gift item
  that was clicked was 'ticked' or 'unticked' and calls the add/subtract function accordingly. */
  onArrayClick = (e, amount) => {
    e.preventDefault();
    const clickedItem = e.currentTarget;
    const { dispatch } = this.props;
    if (document.getElementById('user-amount').value !== '' || clickedItem.className.search('array-selected') < 0) {
      this.props.removeAllDonation(e);
    }
    clickedItem.classList.toggle('array-selected');
    if (clickedItem.className.search('array-selected') >= 0) {
      this.props.addDonation(amount, true, 'custom');
      //For analytics purposes - Cardinal Path
      window.dataLayer.push({
        'event': 'holiday-event',
        'eventAction': 'additem',
        'eventLabel': 'Giftbox - ' + amount,
        'eventValue': amount
      });
    } else {
      this.props.removeDonation(amount, false, 'custom');
    }
  }

  render() {
    const { donationItems } = this.props;
    const { textData } = this.props;
    return (
      <div className="array-btns-group">
        <button className="btn-array" onClick={ e => this.onArrayClick(e, 75) }>{ resolveCurrency(75) }</button>
        <button className="btn-array" onClick={ e => this.onArrayClick(e, 150) }>{ resolveCurrency(150) }</button>
        <button className="btn-array" onClick={ e => this.onArrayClick(e, 250) }>{ resolveCurrency(250) }</button>
        <button className="btn-array" onClick={ e => this.onArrayClick(e, 500) }>{ resolveCurrency(500) }</button>
      </div>
    );
  }
}

export default connect(mapStateToProps)(GiftArray);
