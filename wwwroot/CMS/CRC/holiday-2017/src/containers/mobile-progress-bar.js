import React from 'react';
import { Provider, connect } from 'react-redux';
import ProgressBar from 'react-progressbar.js';
import { resolveString } from '../reducers';
const Line = ProgressBar.Line;

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems,
  progress: state.donation > 0 ? state.donation / 200 : state.fakeHoverDonation,
  progressPercentage: Math.floor(state.donation / 200 * 100),
  isItemHover: state.isItemHover
});


class MobileProgressBar extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // this.addRev();
  }

  componentDidUpdate(prevProps, prevState) {
    // if (this.props.donateAmount > 0) {
    //   this.removeRev();
    // }

    // if (this.props.donateAmount === 0) {
    //   this.addRev();
    // }
  }

   // Updates progress bar text based on the calculated total percentage of selected or custom amount.
  updateText() {
    if (this.props.progress > 1 && !this.props.isItemHover) {
      return (<p> {resolveString(this.props.textData.GlobalSurvivalKitMobileGrand)} </p>);
    }

    if (this.props.progress === 1 && !this.props.isItemHover) {
      return (<p>{resolveString(this.props.textData.GlobalSurvivalKitMobileFull)}</p>);
    }

    if (this.props.progressPercentage > 0 && !this.props.isItemHover) {
      return (<p>{ resolveString(this.props.textData.GlobalSurvivalKitMobile) } { this.props.progressPercentage}% {resolveString(this.props.textData.FullMobile) } </p>);
    }

    return (<p>{ resolveString(this.props.textData.GlobalSurvivalKitMobileEmpty) }</p>);
  }

  // addRev() {
  //   const items = document.querySelectorAll('.item-container');

  //   Array.prototype.forEach.call(items, item => item.addEventListener('mouseenter', this.progressRev));
  // }

  // removeRev() {
  //   const items = document.querySelectorAll('.item-container');

  //   Array.prototype.forEach.call(items, item => item.removeEventListener('mouseenter', this.progressRev));
  // }

  // progressRev() {
  //   const bar = document.querySelectorAll('#mobile-bar svg');
  //   bar[0].children[1].classList.add('path-animate');

  //   window.setTimeout(() => {
  //     bar[0].children[1].classList.remove('path-animate');
  //   }, 400);
  // }

  render() {

    const options = {
      color: '#EE0000',
      strokeWidth: 2,
      trailWidth: 2,
      trailColor: '#404040',
      duration: 1200,
      svgStyle: {
      },
      text: {
        style: {
          color: '#404040',
          fontSize: '1.2rem',
          fontWeight: '700',
          transform: {
            prefix: true,
          }
        }
      }
    };

    const containerStyle = {
      width: '100%',
      height: '1.5rem'
    };

    return (
      <div id="mobile-bar" className="mobile-progress-container">
        {this.updateText()}
        <Line
          progress={this.props.progress > 1 ? 1 : this.props.progress}
          options={options}
          initialAnimate
          containerStyle={containerStyle}
          containerClassName={'mobile-progress'}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps)(MobileProgressBar);
