import $ from 'jquery';
import React from 'react';
import Break from 'react-break';
import { Provider, connect } from 'react-redux';
import { donateAction, tooltipAction, resolveString } from '../reducers';

const mapStateToProps = state => ({
  donateAmount: state.donation,
  textData: state.textData,
  donationItems: state.donationItems
});

const UIBreakpoints = {
  mobile: 0,
  medium: 680
};

class BannerImage extends React.Component {
  constructor(props) {
    super(props);
  }

  // This function is triggered everytime the user clicks on the FB/Twitter share links. And it is for analytics purposes for Cardinal Path.
  pushSocialAnalyticsCode = (e, socialType) => {
    window.dataLayer.push({
      'event': 'social-event',
      'socialNetork': socialType,
      'socialAction': 'share',
      'socialTarget': e.currentTarget.href
    });

  }

  // Gets the appropriate twitter share link based on control/test version of the site
  resolveTwitterShareLink = () => {
    return this.props.version === '1' ? this.props.textData.TwitterShareLinkControl : this.props.textData.TwitterShareLinkTest;
  }

  // Gets the appropriate Facebook share link based on control/test version of the site
  resolveFacebookShareLink = () => {
    return this.props.version === '1' ? this.props.textData.FBShareLinkControl : this.props.textData.FBShareLinkTest;
  }

  // On page1 item click: Reset all donation. Add the corresponding donation amount based on p1 item clicked. Flip corresponding p2 tile.
  onItemClick = (name, codeName, amount, message, type, e) => {
    // this._onScrollToItem('div[data-key="' + amount + '"]');
    this._onScrollToDonate();
    if (this.props.donateAmount > 0) {
      this.removeAllDonation();
    }
    const { dispatch } = this.props;
    const clickedItem = document.querySelector('div[data-key="' + amount + '"] > .item');
    // Added a delay of 1 sec as per CRC's instruction (against recommendation) to force the user to see the tile flip.
    setTimeout(() => {
      clickedItem.classList.add('added');
      dispatch(tooltipAction.addTooltipMessage(message));
      dispatch(donateAction.add(amount, false, type));
    }, 1000);
    //For analytics purposes - Cardinal Path
    window.dataLayer.push({
      'event': 'holiday-event-itemclick-icon',
      'eventAction': 'itemclick-icon',
      'eventLabel': codeName,
    });
    window.dataLayer.push({
      'event': 'holiday-event-additem-icon',
      'eventAction': 'additem-icon',
      'eventLabel': codeName,
      'eventValue': amount
    });
  }

  // Reset all donation and unflip all tiles and unselect all gift array.
  removeAllDonation = () => {
    const { dispatch } = this.props;
    dispatch(donateAction.removeAll());
    dispatch(tooltipAction.removeAllTooltipMessage());
    const elementList = document.querySelectorAll('.added, .array-selected');
    Array.prototype.forEach.call(elementList, (element, index) => {
      element.classList.remove('added');
      element.classList.remove('array-selected');
    });
    if (document.activeElement.id !== 'user-amount') {
      document.getElementById('user-amount').value = '';
    }
  }

  _onScrollToItem = (item) => {
    $('html, body').animate({ scrollTop: $(item).position().top += -5 }, 1000);
  };

  _onScrollToDonate = () => {
    $('html, body').animate({ scrollTop: $('#p2H2').position().top += -5 }, 1000);
  };

  render() {
    const { donationItems, textData } = this.props;

    return (
      <section id={ resolveString(this.props.textData.NavigationSections.p1) } className="header">
        <div className="logo-social">
          <div className="logo">
            <a href={ resolveString(textData.RedCrossLogoLink) } target="_blank" rel="noopener noreferrer">
              <img src={ resolveString(textData.RedCrossLogo) } alt={ resolveString(textData.RedCrossLogoAlt) } />
            </a>
          </div>
          <div className="social-icons">
            <p>{ resolveString(textData.ShareText) }</p>
            <div>
              <a href={ resolveString(this.resolveFacebookShareLink()) } target="_blank" rel="noopener noreferrer" onClick={e => this.pushSocialAnalyticsCode(e, 'facebook') }>
                <img src={ textData.SocialLogo.fb } alt={ textData.SocialLogoAlt.fb } />
              </a>
              <a href={ resolveString(this.resolveTwitterShareLink()) } target="_blank" rel="noopener noreferrer" onClick={e => this.pushSocialAnalyticsCode(e, 'twitter') }>
                <img src={ textData.SocialLogo.twitter } alt={ textData.SocialLogoAlt.twitter } />
              </a>
            </div>
          </div>
        </div>
        <div className="header-content">
          <div className="text-center">
            <h1>{ resolveString(textData.DefaultCampaign.p1Header) }</h1>
            <h2 className="p1">{ resolveString(textData.DefaultCampaign.p1Subheader) }</h2>
            <h2 className="p1">{ resolveString(textData.MatchTextP1) }</h2>
          </div>
          <div className="wrapper">
            <div className="item-click-containers">
              <Break
                breakpoints={UIBreakpoints}
                query={{ method: 'is', breakpoint: 'mobile' }}
              >
                <img src={ resolveString(textData.BannerImage.mobile) } alt="" />
              </Break>
              <Break
                breakpoints={UIBreakpoints}
                query={{ method: 'is', breakpoint: 'medium' }}
              >
                <img src={ resolveString(textData.BannerImage.desktop) } alt="" />
              </Break>
              {
                donationItems.map((i, index) => {
                  const name = 'item-click item-click-' + (index + 1);

                  return (
                    <div className={ name } key={i.id} onClick={(e) => this.onItemClick(i.name.en, i.codeName, i.price, resolveString(i.tooltip), i.type, e) }>
                      <span className="add">
                        <img src="/crc/holiday-2017/assets/ui/add.svg" alt="" />
                      </span>
                    </div>
                  );
                })
              }
            </div>
          </div>
          <div className="arrow-down" onClick={this._onScrollToDonate}>
            <img src="/crc/holiday-2017/assets/ui/arrow.svg" alt="Click here to scroll down" />
          </div>
        </div>
      </section>
    );
  }
}

export default connect(mapStateToProps)(BannerImage);
