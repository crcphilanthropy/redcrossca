import contentText from '../data/content.json';
import items from '../data/donation-items.json';

// The types of actions that you can dispatch to modify the state of the store
export const types = {
  ADD_DONATION: 'ADD_DONATION',
  REMOVE_DONATION: 'REMOVE_DONATION',
  REMOVE_ALL_DONATION: 'REMOVE_ALL_DONATION',
  SELECT_ALL_DONATION: 'SELECT_ALL_DONATION',
  ADDALL_TOOLTIP_MESSAGE: 'ADDALL_TOOLTIP_MESSAGE',
  REMOVEALL_TOOLTIP_MESSAGE: 'REMOVEALL_TOOLTIP_MESSAGE',
  ADD_TOOLTIP_MESSAGE: 'ADD_TOOLTIP_MESSAGE',
  REMOVE_TOOLTIP_MESSAGE: 'REMOVE_TOOLTIP_MESSAGE',
  ADD_FAKEHOVERDONATION: 'ADD_FAKEHOVERDONATION',
  REMOVE_FAKEHOVERDONATION: 'REMOVE_FAKEHOVERDONATION'
};


// Helper functions to dispatch actions, optionally with payloads
export const donateAction = {
  add: (amount, isCustomDonate, type) => {
    return { type: types.ADD_DONATION, payload: [amount, isCustomDonate, type] };
  },
  remove: (amount, isCustomDonate, type) => {
    return { type: types.REMOVE_DONATION, payload: [amount, isCustomDonate, type] };
  },
  removeAll: () => {
    return { type: types.REMOVE_ALL_DONATION };
  },
  selectAll: () => {
    return { type: types.SELECT_ALL_DONATION };
  },
  addFakeHoverDonation: () => {
    return { type: types.ADD_FAKEHOVERDONATION };
  },
  removeFakeHoverDonation: () => {
    return { type: types.REMOVE_FAKEHOVERDONATION };
  }
};

//Helper function to dispatch tooltip actions
export const tooltipAction = {
  addAllTooltipMessage: () => {
    return { type: types.ADDALL_TOOLTIP_MESSAGE };
  },
  removeAllTooltipMessage: () => {
    return { type: types.REMOVEALL_TOOLTIP_MESSAGE };
  },
  addTooltipMessage: (message) => {
    return { type: types.ADD_TOOLTIP_MESSAGE, payload: message };
  },
  removeTooltipMessage: (message) => {
    return { type: types.REMOVE_TOOLTIP_MESSAGE, payload: message };
  }
};

/* Resource string resolver. Detects whether the site is in French or English mode and returns
the correct string */
export const resolveString = (stringToResolve) => {
  const currentCultureCode = document.querySelector('html').getAttribute('lang') || 'en';
  return (stringToResolve[currentCultureCode]);
};

/* Currency symbol placement resolver. Detects whether the site is in French or English mode and returns
the correct placement for each language. */
export const resolveCurrency = (amount) => {
  const currentCultureCode = document.querySelector('html').getAttribute('lang') || 'en';
  switch (currentCultureCode) {
    case 'en': {
      return '$' + amount;
    }
    case 'fr': {
      return amount + ' $';
    }
    default: {
      return '$' + amount;
    }
  }
};


// Initial state of the store
const initialState = {
  donation: 0,
  currentDomesticAmount: 0,
  currentInternationalAmount: 0,
  textData: contentText,
  donationItems: items,
  lastItemSelectedMessage: [],
  itemsSelected: 0,
  isCustomDonate: false,
  isItemHover: false,
  fakeHoverDonation: 0

};

// The reducer function that modifies the store state. The state must not be modified directly as it's read-only as per Redux's rules.
export const reducer = (state = initialState, action) => {
  const { donation, itemsSelected, donationItems, lastItemSelectedMessage, domesticPercentage, internationalPercentage, currentDomesticAmount, currentInternationalAmount } = state;
  const { type, payload } = action;

  switch (type) {
    case types.ADD_FAKEHOVERDONATION: {
      const newFakeDonation = 0.25;
      const itemHoverState = true;

      return {
        ...state,
        fakeHoverDonation: newFakeDonation
      };
    }
    case types.REMOVE_FAKEHOVERDONATION: {
      const newFakeDonation = 0;
      const itemHoverState = false;
      return {
        ...state,
        fakeHoverDonation: newFakeDonation
      };
    }
    case types.ADD_DONATION: {
      const newDonation = donation + payload[0];
      const totalSelected = payload[1] ? 0 : itemsSelected + 1;
      const customDonation = payload[1];
      const donationType = payload[2];
      const newDomesticAmount = donationType === 'domestic' ? currentDomesticAmount + payload[0] : currentDomesticAmount;
      const newInternationalAmount = donationType === 'international' ? currentInternationalAmount + payload[0] : currentInternationalAmount;
      const itemHoverState = donationType === 'hover';
      return {
        ...state,
        itemsSelected: totalSelected,
        donation: newDonation,
        isCustomDonate: customDonation,
        currentDomesticAmount: newDomesticAmount,
        currentInternationalAmount: newInternationalAmount,
        isItemHover: itemHoverState
      };
    }
    case types.REMOVE_DONATION: {
      const newDonation = donation - payload[0];
      const totalSelected = payload[1] ? 0 : itemsSelected - 1;
      const customDonation = payload[1];
      const donationType = payload[2];
      const newDomesticAmount = donationType === 'domestic' ? currentDomesticAmount - payload[0] : currentDomesticAmount;
      const newInternationalAmount = donationType === 'international' ? currentInternationalAmount - payload[0] : currentInternationalAmount;
      return {
        ...state,
        itemsSelected: totalSelected,
        donation: newDonation,
        currentDomesticAmount: newDomesticAmount,
        currentInternationalAmount: newInternationalAmount
      };
    }
    case types.REMOVE_ALL_DONATION: {
      const newDonation = 0;
      const totalSelected = 0;
      const newDomesticAmount = 0;
      const newInternationalAmount = 0;
      const itemHoverState = false;
      return {
        ...state,
        itemsSelected: totalSelected,
        donation: newDonation,
        currentDomesticAmount: newDomesticAmount,
        currentInternationalAmount: newInternationalAmount,
        isItemHover: itemHoverState
      };
    }
    case types.SELECT_ALL_DONATION: {
      const newDonation = 200;
      const totalSelected = 6;
      const customDonation = false;
      const newDomesticAmount = 100;
      const newInternationalAmount = 100;
      return {
        ...state,
        itemsSelected: totalSelected,
        donation: newDonation,
        isCustomDonate: customDonation,
        currentDomesticAmount: newDomesticAmount,
        currentInternationalAmount: newInternationalAmount
      };
    }
    case types.ADD_TOOLTIP_MESSAGE: {
      const message = payload;
      const messageArray = lastItemSelectedMessage;
      messageArray.push(message);
      return {
        ...state,
        lastItemSelectedMessage: messageArray
      };
    }
    case types.REMOVE_TOOLTIP_MESSAGE: {
      const message = payload;
      const messageArray = lastItemSelectedMessage;
      const index = messageArray.indexOf(message);
      messageArray.splice(index, 1);
      return {
        ...state,
        lastItemSelectedMessage: messageArray
      };
    }
    case types.ADDALL_TOOLTIP_MESSAGE: {
      const messageArray = donationItems.reduce((arr, x) => {
        arr.push(resolveString(x.tooltip));
        return arr;
      }, []);
      return {
        ...state,
        lastItemSelectedMessage: messageArray
      };
    }
    case types.REMOVEALL_TOOLTIP_MESSAGE: {
      const messageArray = [];
      return {
        ...state,
        lastItemSelectedMessage: messageArray
      };
    }
    default: {
      return state;
    }
  }
};
