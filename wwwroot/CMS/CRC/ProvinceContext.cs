using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.Taxonomy;
using CMS.Helpers;
using CMS.Localization;
using CMSAppAppCode.Old_App_Code.CRC;

namespace CMSApp.CRC
{
    public class ProvinceContext
    {


        public TreeNodeDataSet Services = new TreeNodeDataSet();
        public DataSet Cities = new DataSet();
        public TreeNodeDataSet Branches = new TreeNodeDataSet();
        public TreeNodeDataSet Programs = new TreeNodeDataSet();

        public ProvinceContext() { }

        public ProvinceContext(int provinceId)
        {
            SetValues(provinceId);
        }

        private void SetValues(int provinceId)
        {
            Cities = CategoryInfoProvider.GetChildCategories(provinceId, string.Format("CategoryParentID = {0}", provinceId), string.Empty, -1, null, SiteContext.CurrentSiteID);
            if (DataHelper.DataSourceIsEmpty(Cities))
                return;

            var cityIDs = Cities.Tables[0].Rows.Cast<DataRow>().Select(item => GenerateSeparatorValueWhereClause(',', "Cities", item["CategoryID"]));

            var cIds = Cities.Tables[0].Rows.Cast<DataRow>().Select(item => item["CategoryID"].ToString());
            var whereClause = string.Format("(DocumentID IN (SELECT DocumentID FROM CMS_DocumentCategory WHERE CategoryID in ({0})))", cIds.Join(","));

            Branches = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, true,
                                               "CRC.Branch", whereClause, string.Empty, TreeProvider.ALL_LEVELS, true, -1);

            if (DataHelper.DataSourceIsEmpty(Branches))
                return;

            var branchGuids = Branches.Select(item => string.Format("fkBranches Like '%{0}%'", item.DocumentGUID));

            Services = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, true,
                                               "CRC.Service", string.Join(" OR ", branchGuids), string.Empty, TreeProvider.ALL_LEVELS, true, -1);

            if (DataHelper.DataSourceIsEmpty(Services))
                return;

            var programGuids = Services.Select(item => string.Format("DocumentGUID = '{0}'", item.Parent.DocumentGUID)).Distinct();

            Programs = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, false,
                                               "CRC.Program", string.Join(" OR ", programGuids), "Title ASC", TreeProvider.ALL_LEVELS, true, -1);

        }

        public static object CacheManagerOnRetriving(object sender, EventArgs args)
        {
            var genericEventArgs = args as CacheManager.GenericEventArgs<int>;
            if (genericEventArgs == null)
                return null;

            var provinceId = genericEventArgs.Dependency;
            return new ProvinceContext(provinceId);
        }

        public static string GenerateSeparatorValueWhereClause(char separator, string column, object value)
        {
            return string.Format("{0} LIKE '{1}{2}%' OR {0} LIKE '%{2}{1}' OR {0} LIKE '%{2}{1}{2}%' OR (CHARINDEX('{2}', {0}) = 0 AND {0} = '{1}')", column, value, separator);
        }
    }
}