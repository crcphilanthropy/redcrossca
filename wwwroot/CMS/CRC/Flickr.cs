using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlickrNet;

namespace CMSApp.CRC
{
    public static class Flickr
    {
        public static string FlickrAPIKey
        {
            get { return CMS.DataEngine.SettingsKeyInfoProvider.GetStringValue("Flickr_APIKey"); }
        }

        public static string FlickrSharedSecret
        {
            get { return CMS.DataEngine.SettingsKeyInfoProvider.GetStringValue("Flickr_SharedSecret"); }
        }

        public static string FlickrUserID
        {
            get { return CMS.DataEngine.SettingsKeyInfoProvider.GetStringValue("Flickr_UserID"); }
        }

        private static FlickrNet.Flickr _instance;
        public static FlickrNet.Flickr Instance
        {
            get
            {
                return _instance ??
                       (_instance =
                        new FlickrNet.Flickr(FlickrAPIKey, FlickrSharedSecret) { InstanceCacheDisabled = true });
            }
        }
    }
}
