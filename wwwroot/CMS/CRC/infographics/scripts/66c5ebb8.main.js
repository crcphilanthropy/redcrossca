$(function(){
	//if (Modernizr.svg) { jQuery('img.svg').attr('src', function() { return $(this).attr('src').replace('.png', '.svg'); }); }
	$('.various').fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	//all paragraphs having the class font-size will be handled by the plugin already in use.
	$('.font-size').jfontsize({
		btnMinusClasseId: '.smallFont',
		btnDefaultClasseId: '.defaultFont',
		btnPlusClasseId: '.largeFont',
		btnMinusMaxHits: 2,
		btnPlusMaxHits: 4,
		sizeChange: 1
	}); 
	
});

