﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;

namespace CMSApp.CRC.Handlers
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class VideoHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.

            //Get request download link and video name
            var downloadLink = context.Request.QueryString["downloadlink"];
            var name = context.Request.QueryString["name"];

            //Set response content header info
            var response = context.Response;
            var cd = new ContentDisposition
            {
                FileName = name+".mp4",
                Inline = false
            };
            response.ContentType = "video/mp4";
            response.Headers["Content-Disposition"] = cd.ToString();
            //Request to video download data with downloadLink
            using (var client = new WebClient())
            using (var stream = client.OpenRead(downloadLink))
            {
                response.BufferOutput = false;   // to prevent buffering
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    response.OutputStream.Write(buffer, 0, bytesRead);
                    stream.CopyTo(response.OutputStream);
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}