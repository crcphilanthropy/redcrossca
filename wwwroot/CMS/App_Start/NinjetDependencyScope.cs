﻿//using Ninject;
//using Ninject.Syntax;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics.Contracts;
//using System.Linq;
//using System.Web;
//using System.Web.Http.Dependencies;

//namespace CMS.Mvc
//{
//    public class NinjcetDependencyScope : IDependencyScope
//    {
//        private IResolutionRoot resolver;

//        public NinjcetDependencyScope(IResolutionRoot resolver)
//        {
//            Contract.Assert(resolver != null);
//            this.resolver = resolver;
//        }

//        public object GetService(Type serviceType)
//        {
//            if (resolver == null)
//                throw new ObjectDisposedException("this", "This scope has been disposed");

//            return resolver.TryGet(serviceType);
//        }

//        public IEnumerable<object> GetServices(Type serviceType)
//        {
//            if (resolver == null)
//                throw new ObjectDisposedException("this", "This scope has been disposed");

//            return resolver.GetAll(serviceType);
//        }

//        public void Dispose()
//        {
//            IDisposable disposable = resolver as IDisposable;
//            if (disposable != null)
//                disposable.Dispose();

//            resolver = null;
//        }
//    }

//    // This class is the resolver, but it is also the global scope
//    // so we derive from NinjectScope.
//    public class NinjectDependencyResolver : NinjcetDependencyScope, IDependencyResolver
//    {
//        IKernel kernel;

//        public NinjectDependencyResolver(IKernel kernel) : base(kernel)
//        {
//            this.kernel = kernel;
//        }

//        public IDependencyScope BeginScope()
//        {
//            return new NinjcetDependencyScope(kernel.BeginBlock());
//        }
//    }
//}
