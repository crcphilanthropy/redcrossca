﻿using AttributeRouting.Web.Http.WebHost;
using System.Web.Http;

namespace CMS.Mvc
{
    /// <summary>
    /// Class providing manipulation with the application API routes.
    /// </summary>
    public static class WebApiConfig
    {

        /// <summary>
        /// Registers the specified config.
        /// </summary>
        /// <param name="config">The http configuration</param>
        public static void Register(HttpConfiguration config)
        {
            /*
             * Add your WebApi routes registration here
             *
             * Add a reference for the System.Net.Http and System.Net.Http.WebRequest libraries from the Lib/MVC/ if needed.
             */
             //Attribute routing
            GlobalConfiguration.Configuration.Routes.MapHttpAttributeRoutes();

          
            config.Routes.MapHttpRoute(
                name: "MultimediaApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Convention based routing
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            

            //Always return results in json format
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(
                    new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));

        }
    }
}
