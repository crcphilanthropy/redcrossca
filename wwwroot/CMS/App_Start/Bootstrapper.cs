﻿using CMSApp.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Bootstrapper), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(Bootstrapper), "Stop")]
namespace CMSApp.App_Start
{

    public static class Bootstrapper
    {
        public static void Start()
        {
            
        }

        public static void Stop()
        {

        }
    }
}