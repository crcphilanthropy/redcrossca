//[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CMS.Mvc.NinjectWebCommon), "Start")]
//[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CMS.Mvc.NinjectWebCommon), "Stop")]

//namespace CMS.Mvc
//{
//    using System;
//    using System.Web;

//    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

//    using Ninject;
//    using Ninject.Web.Common;
//    using System.Web.Http;

//    using AutoMapper;
//    using MultiMediaApi.Repository;
//    using MultiMediaApi.Services.Repository;
//    using MultiMediaApi.Services;
//    using WebApiContrib.IoC.Ninject;

//    public static class NinjectWebCommon
//    {
//        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

//        /// <summary>
//        /// Starts the application
//        /// </summary>
//        public static void Start()
//        {
//            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
//            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
//            bootstrapper.Initialize(CreateKernel);
//        }

//        /// <summary>
//        /// Stops the application.
//        /// </summary>
//        public static void Stop()
//        {
//            bootstrapper.ShutDown();
//        }

//        /// <summary>
//        /// Creates the kernel that will manage your application.
//        /// </summary>
//        /// <returns>The created kernel.</returns>
//        private static IKernel CreateKernel()
//        {
//            var kernel = new StandardKernel();
//            try
//            {
//                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
//                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

//                RegisterServices(kernel);

//                // Install our Ninject-based IDependencyResolver into the Web API config
//                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);

//                return kernel;

//            }
//            catch
//            {
//                kernel.Dispose();
//                throw;
//            }
//        }

//        /// <summary>
//        /// Load your modules or register your services here!
//        /// </summary>
//        /// <param name="kernel">The kernel.</param>
//        private static void RegisterServices(IKernel kernel)
//        {
//            kernel.Bind<IBanner>().To<BannerRepository>();
//            kernel.Bind<IBannerSet>().To<BannerSetRepository>();
//            kernel.Bind<ICompany>().To<CompanyRepository>();
//            kernel.Bind<IPhoto>().To<PhotoRepository>();
//            kernel.Bind<IFileResult>().To<FileRepository>();
//            kernel.Bind<ICatalog>().To<CatalogRepository>();
//            kernel.Bind<IVideo>().To<VideoRepository>();
//            //services
//            kernel.Bind<IBannerService>().To<BannerService>();
//            kernel.Bind<ICatalogService>().To<CatalogService>();
//            kernel.Bind<IEmbedLinkService>().To<EmbedLinkService>();
//            kernel.Bind<ITrackingCodeService>().To<TrackingCodeService>();
//            kernel.Bind<IPhotoService>().To<PhotoService>();
//            kernel.Bind<IVideoService>().To<VideoService>();
//            kernel.Bind<IMediaLibraryService>().To<MediaLibraryService>();
//            kernel.Bind<IApiCacheService>().To<ApiCacheService>();
//            //automapper
//            kernel.Bind<IMappingEngine>().To<MappingEngine>();

//        }

//    }

//}
