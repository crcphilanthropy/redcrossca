﻿using AutoMapper;
using MultiMediaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Mvc
{
    /// <summary>
    /// Class automapper configuration setup
    /// </summary>
    public static class MapperConfig
    {
        /// <summary>
        /// adds mapping profiles in automapper config.
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<BannerProfile>();
                cfg.AddProfile<MediaProfile>();
                cfg.AddProfile<CompanyProfile>();
                cfg.AddProfile<FileProfile>();
                cfg.AddProfile<PhotoProfile>();
                cfg.AddProfile<VideoProfile>();
            });
        }
    }
}