﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace CMS.Mvc
{
    /// <summary>
    /// Class providing manipulation with the application routes.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registers the application routes.
        /// </summary>
        /// <param name="routes">The routes collection</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            /*
             * Add your custom routes registration here
             */

            //if (routes["DefaultApiWithId"] == null)
            //    using (routes.GetWriteLock())
            //        routes.MapHttpRoute("DefaultApiWithId", "Api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });

            //if (routes["DefaultApiWithAction"] == null)
            //    using (routes.GetWriteLock())
            //        routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");

            //if (routes["DefaultApiGet"] == null)
            //    using (routes.GetWriteLock())
            //        routes.MapHttpRoute("DefaultApiGet", "Api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) });

            //if (routes["DefaultApiPost"] == null)
            //    using (routes.GetWriteLock())
            //        routes.MapHttpRoute("DefaultApiPost", "Api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post.ToString()) });
        }
    }
}