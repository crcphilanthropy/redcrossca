﻿/**
 * Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add('default',
[
    { name: 'paragraph', element: 'p' },
    { name: 'Corp - h1 Heading', element: 'h1' },
    { name: 'Corp - h2 Heading', element: 'h2' },
    { name: 'Corp - h3 Heading', element: 'h3' },
    { name: 'Corp - h4 Heading', element: 'h4' },
    { name: 'Corp - h5 Heading', element: 'h5' },
    { name: 'Corp - Intro Paragraph', element: 'span', attributes: { 'class': 'intro' } },
    { name: 'Blog - h1 Heading', element: 'h1' },
    { name: 'Blog - h2 Heading', element: 'h2' },
    { name: 'Blog - h3 Heading', element: 'h3' },
    { name: 'Blog - h4 Heading', element: 'h4' },
    { name: 'Blog - h5 Heading', element: 'h5' },
    { name: 'Caption', element: 'p', attributes: { 'class': 'media-item-caption' } },
    { name: 'Align Img Left', element: 'span', attributes: { 'class': 'imgLeft' } },
    { name: 'Align Img Right', element: 'span', attributes: { 'class': 'imgRight' } },
    { name: 'Align Img Center', element: 'span', attributes: { 'class': 'imgCenter' } },
    { name: 'Hide on Mobile', element: 'span', attributes: { 'class': 'hide-on-mobile' } },

    /* Lists */
    { name: 'List - Two Col', element: 'ul', attributes: { 'class': 'two-col clearfix' } },
    { name: 'List - Two Col No Bullets', element: 'ul', attributes: { 'class': 'two-col no-bullets clearfix' } },
    { name: 'List - Three Col', element: 'ul', attributes: { 'class': 'three-col clearfix' } },
    { name: 'List - Three Col No Bullets', element: 'ul', attributes: { 'class': 'three-col no-bullets clearfix' } }
]);

