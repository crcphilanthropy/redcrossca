﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/


CKEDITOR.stylesSet.add('default',
[
    { name: 'paragraph', element: 'p' },
    { name: 'Corp - h1 Heading', element: 'h1' },
    { name: 'Corp - h2 Heading', element: 'h2' },
    { name: 'Corp - h3 Heading', element: 'h3' },
    { name: 'Corp - h4 Heading', element: 'h4' },
    { name: 'Corp - h5 Heading', element: 'h5' },
    { name: 'Corp - Intro Paragraph', element: 'span', attributes: { 'class': 'intro' } },
    { name: 'Blog - h1 Heading', element: 'h1' },
    { name: 'Blog - h2 Heading', element: 'h2' },
    { name: 'Blog - h3 Heading', element: 'h3' },
    { name: 'Blog - h4 Heading', element: 'h4' },
    { name: 'Blog - h5 Heading', element: 'h5' },
    { name: 'Caption', element: 'p', attributes: { 'class': 'media-item-caption' }},
    { name: 'Align Img Left', element: 'span', attributes: { 'class': 'imgLeft' } },
    { name: 'Align Img Right', element: 'span', attributes: { 'class': 'imgRight' } },
    { name: 'Align Img Center', element: 'span', attributes: { 'class': 'imgCenter' } },
    { name: 'Hide on Mobile', element: 'span', attributes: { 'class': 'hide-on-mobile' } }
]);