﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ECA_FormControls_MultiDocumentSelector_Dialog_MultiDocumentDialog" Theme="Default" Codebehind="MultiDocumentDialog.aspx.cs" %>

<%@ Register Src="~/CMSModules/Content/FormControls/Documents/SelectDocument.ascx" TagName="docselector" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Multiple Document Selector</title>
    <style type="text/css">
        body
        {
            padding: 0px;
            margin: 0px;
        }

        #findDoc, #orderDoc
        {
            background-color: #fbfbfb;
            border: solid 1px #8d8d8d;
            padding: 5px 10px 5px 10px;
            margin: 10px;
        }

        #orderDoc
        {
            height: 220px;
            overflow: scroll;
        }

        #dialogOptions
        {
            margin: 10px;
        }

        ul, li
        {
            list-style-type: none;
            padding: 0px;
        }

        .itemSpace
        {
            width: 385px;
            height: 25px;
            margin: 2px 0px 2px 0px;
            border: 2px dashed #458f41;
            background: #d2fdce;
        }

        .dragHandle
        {
            width: 50px;
            height: 25px;
            background: #2f80c1 url('Icons/drag.png') no-repeat 5px 5px;
            cursor: move;
        }

        .itemArea
        {
            width: 335px;
            height: 25px;
            margin: 2px 0px 2px 0px;
            background-color: #2f80c1;
            color: #ffffff;
            float: left;
        }

            .itemArea span
            {
                display: block;
                padding: 5px 0 0 0;
                width: 300px;
                float: left;
            }

        .btnDelete
        {
            border: 0px;
            height: 25px;
            width: 25px;
            background: #2f80c1 url('Icons/delete.png') no-repeat 5px 5px !important;
            float: right;
            cursor: pointer;
        }

        .lblRed
        {
            color: #FF0000;
        }
    </style>

    <script type="text/javascript">

        /**
        * Update the multi document selector from the calling page.
        * @method SetCallerValue
        * @param {string} controlID.  Id to set Guid values to.
        * @param {string} guidValue. Guid values of selected pages.
        * @param {string} callerPreviewObj.  Display list to update.
        * @param {string} previewObj. Display list value on this page.
        * @param {string} callerUpdatePanelObj. Update panel.
        */
        function SetCallerValue(controlID, guidValue, callerPreviewObj, previewObj, callerUpdatePanelObj) {

            console.log(document.getElementById(guidValue).value);
            var ref = null;

            if (window.opener == null) {
                if (window.dialogArguments != null) {
                    ref = window.dialogArguments;
                }
            }
            else if (window.opener != null) {
                ref = window.opener;
            }
            if (ref == null)
                ref = wopener;

            ref.document.getElementById(controlID).value = document.getElementById(guidValue).value;
            ref.document.getElementById(callerPreviewObj).innerHTML = document.getElementById(previewObj).innerHTML;
            ref.__doPostBack(callerUpdatePanelObj, '');
            CloseDialog();
        }

    </script>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server" />
        <div class="WidgetTabsPageHeader">
            <div class="PageTitleHeader">
                <table id="m_pt_titleTable" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">

                    <tr>

                        <td id="m_pt_titleRow" style="width: 100%; white-space: nowrap;">

                            <img id="m_pt_imgTitle" class="PageTitleImage" alt="" src="Icons/titlemultidocselect.png" />

                            <span id="m_pt_lblTitle" class="PageTitle">Multiple Document Selector</span>

                        </td>

                        <td class="TextRight"></td>

                        <td style="vertical-align: top;"></td>

                    </tr>

                </table>
            </div>
        </div>

        <div>
            <asp:UpdatePanel ID="upnlMuliDocSelector" runat="server">
                <ContentTemplate>
                    <div id="findDoc">
                        <h3>1. Select Document</h3>
                        <cms:docselector ID="docSelector" runat="server" IsLiveSite="false" />
                        <asp:Button ID="btnAddToDocumentList" runat="server" Text="Add To List" OnClick="btnAddToDocumentList_Click" CssClass="SubmitButton" />
                        <asp:Label ID="lblMessage" runat="server" CssClass="lblRed" />
                    </div>
                    <div id="orderDoc">
                        <h3>2. Order Document List</h3>
                        <ajaxToolkit:ReorderList
                            ID="rlSelectedDocuments"
                            DragHandleAlignment="Left"
                            PostBackOnReorder="true"
                            CallbackCssStyle="callbackStyle"
                            ItemInsertLocation="End"
                            ShowInsertItem="true"
                            CssClass="ReorderList"
                            runat="server"
                            SortOrderField="Order"
                            DataKeyField="NodeGUID"
                            OnItemReorder="rlSelectedDocuments_ItemReorder"
                            OnDeleteCommand="rlSelectedDocuments_DeleteCommand">
                            <DragHandleTemplate>
                                <div class="dragHandle">
                                </div>
                            </DragHandleTemplate>
                            <ItemTemplate>
                                <div class="itemArea">
                                    <asp:Label ID="ID" Visible="false" runat="server" Text='<%# Eval("NodeGUID") %>'></asp:Label>
                                    <asp:Label ID="Order" Visible="false" runat="server" Text='<%# Eval("Order") %>'></asp:Label>
                                    <asp:Label ID="Name" runat="server" Text='<%#ShortenName(Eval("DocumentName")) %>' ToolTip='<%# Eval("DocumentNamePath") %>'></asp:Label>
                                
                                    <asp:Button ID="btnDelete" CssClass="btnDelete" runat="server" Text="" CommandName="Delete" ToolTip="Remove" UseSubmitBehavior="false" />
                                </div>
                            </ItemTemplate>
                            <ReorderTemplate>
                                <div class="itemSpace"></div>
                            </ReorderTemplate>
                        </ajaxToolkit:ReorderList>

                        <br />
                        <br />
                        <asp:HiddenField ID="hdGuidList" runat="server" />
                        <div id="resultContainer" style="display: none">
                            <asp:Repeater ID="rptPages" runat="server">
                                <HeaderTemplate>
                                    <ul id="pagesSummary">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><%# Eval("DocumentName")%></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="dialogOptions">
                <asp:Button ID="btnInsert" runat="server" Text="Update" CssClass="ContentButton" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="CloseDialog();" CssClass="ContentButton" />
            </div>
        </div>
    </form>
</body>
</html>
