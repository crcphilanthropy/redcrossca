using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using CMS.UIControls;
using ECA.FormControls.MultiDocumentSelector;
using CMS.Helpers;

public partial class ECA_FormControls_MultiDocumentSelector_Dialog_MultiDocumentDialog : CMSLiveModalPage
{
    private ViewStateList<SortableDocument> _sortableDocumentList;
    public ViewStateList<SortableDocument> SortableDocumentList
    {
        get { return _sortableDocumentList ?? (_sortableDocumentList = new ViewStateList<SortableDocument>(ViewState, "SortableDocumentList")); }
    }

    //{
    //    get { return ViewState["SortableDocumentList"] as List<SortableDocument> ?? new List<SortableDocument>(); }
    //    set { ViewState["SortableDocumentList"] = value; }
    //}

    /// <summary>
    /// Allowed class names.
    /// </summary>
    public string ClassNameList
    {
        get { return QueryHelper.GetString("ClassNameList", ""); }
    }

    /// <summary>
    /// Maximum number of items for the list.
    /// </summary>
    public int MaxItems
    {
        get { return QueryHelper.GetInteger("MaxItems", 0); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        const string previewObj = "pagesSummary";
        var selectorId = QueryHelper.GetString("selector", "");
        var callerPreviewObj = QueryHelper.GetString("preview", "");
        var callerUpdatePanel = QueryHelper.GetString("updatePanel", "");

        btnInsert.OnClientClick = "SetCallerValue('" + selectorId + "', '" + hdGuidList.ClientID + "', '" + callerPreviewObj + "','" + previewObj + "','" + callerUpdatePanel + "'); return false;";

        if (!Page.IsPostBack)
        {
            LoadSortableDocumentsFromTree();
        }
        else
        {
            rlSelectedDocuments.DataSource = SortableDocumentList;
            rlSelectedDocuments.DataBind();
        }
    }

    /// <summary>
    /// Populate the reordered list with 
    /// </summary>
    protected void LoadSortableDocumentsFromTree()
    {
        var list = QueryHelper.GetString("list", string.Empty);
        if (!string.IsNullOrEmpty(list))
        {
            try
            {
                SortableDocumentList.Clear();
                SortableDocumentList.AddRange(SortableDocumentProvider.GetSortableDocuments(list, ClassNameList));
                rlSelectedDocuments.DataSource = SortableDocumentList;
                rlSelectedDocuments.DataBind();
            }
            catch (Exception)
            {
            }
        }

        SetGuidList();
    }

    /// <summary>
    /// Reorder list items in the database when a reorder event occurs.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rlSelectedDocuments_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
    {
        var newOrder = 0;
        foreach (var doc in SortableDocumentList)
            doc.Order = newOrder++;

        lblMessage.Text = string.Empty;
        rlSelectedDocuments.DataSource = SortableDocumentList;
        rlSelectedDocuments.DataBind();

        SetGuidList();
    }

    /// <summary>
    /// Event for deleting an item from the reorder list.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rlSelectedDocuments_DeleteCommand(object sender, AjaxControlToolkit.ReorderListCommandEventArgs e)
    {
        //Manage delete
        if (e.CommandName == "Delete")
        {
            var removeByGuid = ((Label)e.Item.FindControl("ID")).Text;
            SortableDocumentList.RemoveAll(x => x.NodeGUID == removeByGuid);

            var newOrder = 0;
            foreach (var doc in SortableDocumentList)
                doc.Order = newOrder++;

            lblMessage.Text = string.Empty;
            rlSelectedDocuments.DataSource = SortableDocumentList;
            rlSelectedDocuments.DataBind();

            SetGuidList();
        }
    }

    /// <summary>
    /// Add a new item to the reorderlist.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddToDocumentList_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(docSelector.Value as string))
        {
            var newDocumentGuid = docSelector.Value.ToString();

            var docAlreadyExists = SortableDocumentList.Any(s => s.NodeGUID == newDocumentGuid);

            if (docAlreadyExists)
            {
                lblMessage.Text = "* Document already exists in the list.";
            }
            else if (!SortableDocumentProvider.DocumentIsValidClass(new Guid(newDocumentGuid), ClassNameList))
            {
                lblMessage.Text = "* Document is not of valid type.";
            }
            else if (MaxItems > 0 && SortableDocumentList.Count >= MaxItems)
            {
                lblMessage.Text = "* Maximum items added (" + MaxItems.ToString(CultureInfo.InvariantCulture) + ").";
            }
            else
            {
                lblMessage.Text = "";
                var doc = new SortableDocument
                {
                    Order = SortableDocumentList.Count(),
                    NodeGUID = newDocumentGuid,
                    DocumentName = SortableDocumentProvider.GetDocumentNodeName(newDocumentGuid)
                };
                SortableDocumentList.Add(doc);
            }

            rlSelectedDocuments.DataSource = SortableDocumentList;
            rlSelectedDocuments.DataBind();

            SetGuidList();
        }
        else
        {
            lblMessage.Text = "* Please select a document.";
        }
    }

    /// <summary>
    /// Save the new order to a hidden value and save the
    /// new order in a ul list that we will return to the calling
    /// page for display.
    /// </summary>
    protected void SetGuidList()
    {
        hdGuidList.Value = string.Empty;

        foreach (var doc in SortableDocumentList)
            hdGuidList.Value += doc.NodeGUID + ";";

        if (hdGuidList.Value.Length > 1)
            hdGuidList.Value = hdGuidList.Value.Substring(0, hdGuidList.Value.Length - 1);

        rptPages.DataSource = SortableDocumentList;
        rptPages.DataBind();
    }

    /// <summary>
    /// Shorten the name for display.
    /// </summary>
    /// <param name="nameObj">Object to shorten.</param>
    /// <returns>Short name</returns>
    public string ShortenName(object nameObj)
    {
        const int max = 50;

        var name = string.Empty;

        if (nameObj != null)
        {
            try
            {
                name = nameObj.ToString();

                if (name.Length > max)
                    name = name.Substring(0, max - 3) + "...";
            }
            catch (SystemException)
            {
            }
        }

        return name;
    }
}