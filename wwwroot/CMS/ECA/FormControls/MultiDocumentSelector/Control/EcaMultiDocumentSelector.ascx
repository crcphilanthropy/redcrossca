﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ECA_FormControls_MultiDocumentSelector_Control_EcaMultiDocumentSelector" Codebehind="EcaMultiDocumentSelector.ascx.cs" %>
<hr/>
<asp:Literal ID="ltlUserMessage" runat="server" Text="The following pages are selected.  Click 'Configure' to edit this list." />
<asp:Literal ID="ltlPageList" runat="server" />
<ul id="<%=GetListName()%>">
    <asp:Repeater ID="rptPages" runat="server">
        <ItemTemplate>
            <li>
                <strong><%# Eval("DocumentName") %></strong>
                <br />
                Path: <%# Eval("DocumentNamePath") %>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<asp:UpdatePanel runat="server" ID="MultiDocPanel" OnLoad="MultiDocPanel_Load">
    <ContentTemplate>
        <asp:Button ID="btnCallMultiModal" runat="server" Text="Configure" CssClass="ContentButton"/>
        <asp:HiddenField runat="server" ID="hdGuids" />
    </ContentTemplate>
</asp:UpdatePanel>
<hr/>