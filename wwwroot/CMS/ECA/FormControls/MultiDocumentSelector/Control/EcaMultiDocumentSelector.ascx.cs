using System;
using System.Linq;
using CMS.EventLog;
using ECA.FormControls.MultiDocumentSelector;
using CMS.Helpers;
using CMS.SiteProvider;

public partial class ECA_FormControls_MultiDocumentSelector_Control_EcaMultiDocumentSelector : CMS.FormControls.FormEngineUserControl
{
    /// <summary>
    /// Form control value.
    /// </summary>
    private string value;

    /// <summary>
    /// Gets or sets the class names that can be added to the list.
    /// </summary>
    public string ClassNameList
    {
        get { return ValidationHelper.GetString(GetValue("ClassNameList"), ""); }
        set { SetValue("ClassNameList", value); }
    }

    /// <summary>
    /// Gets or sets maximum number of items that can be added to the list.
    /// </summary>
    public int MaxItems
    {
        get { return ValidationHelper.GetInteger(GetValue("MaxItems"), 0); }
        set { SetValue("MaxItems", value); }
    }

    /// <summary>
    /// Getter/Setter for form control value.
    /// </summary>
    public override object Value
    {
        get
        {
            return hdGuids.Value ?? value;
        }
        set
        {
            hdGuids.Value = this.value = (string)value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterDialogScript(Page);

        if (!StopProcessing)
        {
            ReloadData();
        }
    }

    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        if (!Page.IsPostBack)
        {
            hdGuids.Value = value;
        }

        btnCallMultiModal.OnClientClick = "modalDialog('" + GetDialogUrl() + "','MultiPathSelection', '455px', '450px'); return false;";
        CreateDisplayList();
    }

    /// <summary>
    /// Register a button action to open the Multi-Document Dialog.
    /// </summary>
    /// <returns>URL to the dialog window.</returns>
    protected string GetDialogUrl()
    {
        return URLHelper.GetAbsoluteUrl(URLHelper.ResolveUrl("~/ECA/FormControls/MultiDocumentSelector/Dialog/MultiDocumentDialog.aspx?selector=" + hdGuids.ClientID + "&list=" + hdGuids.Value + "&preview=" + GetListName() + "&updatePanel=" + MultiDocPanel.ClientID + "&ClassNameList=" + ClassNameList + "&MaxItems=" + MaxItems.ToString()));
    }

    /// <summary>
    /// Create display of pages
    /// </summary>
    protected void CreateDisplayList()
    {
        if (!string.IsNullOrWhiteSpace(hdGuids.Value))
        {
            try
            {
                var sortableDocumentList = SortableDocumentProvider.GetSortableDocuments(hdGuids.Value);

                if (sortableDocumentList.Any())
                {
                    rptPages.DataSource = sortableDocumentList;
                    rptPages.DataBind();
                }
            }
            catch (Exception ex)
            {
                EventLogProvider.LogEvent(new EventLogInfo
                {
                    EventType = "E",
                    EventDescription = ex.Message,
                    EventCode = "ECA.MultiDocumentSelector",
                    EventTime = DateTime.Now,
                    Source = SiteContext.CurrentSiteName,
                    SiteID = SiteContext.CurrentSiteID
                });
            }
        }
        else
        {
            rptPages.DataSource = null;
            rptPages.DataBind();
        }
    }

    /// <summary>
    /// Used to provide client script access to fire the update panel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void MultiDocPanel_Load(object sender, EventArgs e)
    {
        btnCallMultiModal.OnClientClick = "modalDialog('" + GetDialogUrl() + "','MultiPathSelection', '455px', '450px'); return false;";
    }

    /// <summary>
    /// Get a unique name for the list.
    /// </summary>
    /// <returns>Unique name.</returns>
    public string GetListName()
    {
        return rptPages.ClientID;
    }
}