﻿using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMSApp.CRC.Entities;
using OfficeOpenXml;
using CMS.DataEngine;
using CMS.SettingsProvider;
using System.IO;




public partial class CMSModules_TagRemoval_Default : CMSToolsPage
{

    private class TagMapping
    {
        public string TagName { get; set; }
        public string TagAction { get; set; }
    }


    private StringBuilder _sb;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.TagRemoval", "read"))
        {
            RedirectToAccessDenied("cms.TagRemoval", "Read");
        }

    }

    protected void UploadButton_Click(object sender, EventArgs e)
    {
        UploadFile();
    }

    private bool UploadFile()
    {
        _sb = new StringBuilder();

        try
        {
            if (FileUploadControl.HasFile)
            {
                var filename = String.Format("{0}.csv", System.IO.Path.GetTempFileName());
                FileUploadControl.SaveAs(filename);

                ProcessFile(filename);
                File.Delete(filename);
            }
        }

        catch(Exception ex)
        {
            ResultLabel.Text = ex.Message;
        }

        ResultLabel.Text += _sb.ToString();
        return true;
    }

    private void ProcessFile(string filename)
    {      
        var removedTagsList = new List<string>();
        var mappedTagsList = new List<Tuple<string, string>>();
        var processedTags = new List<string>();

        using (var dbConnection = ConnectionHelper.GetConnection())
        {
            
            using (var csvFile = new StreamReader(filename,Encoding.GetEncoding("Windows-1252")))
            {                 
                string lineBuffer;
                while( (lineBuffer = csvFile.ReadLine() ) != null)
                {
                    ProcessTag(lineBuffer,removedTagsList,processedTags,mappedTagsList,dbConnection);
                }
            
            }
        }
        ProcessResults(removedTagsList,mappedTagsList);
    }

    private void ProcessTag(string csvLine,List<string> removedTagsList,List<string> processedTags, List<Tuple<string,string>> mappedTagsList, GeneralConnection dbConnection)
    {
        var nameOfOldTag = csvLine.Split(',')[2];
        var nameOfNewTag = csvLine.Split(',')[3];

        if (RowIsDuplicate(nameOfOldTag))
        {
            return;
        }

        if (String.IsNullOrWhiteSpace(nameOfNewTag))
        {
            return;
        }

        /* This seems redundant, but it will filter out duplicates in cases of input errors */

        //skip duplicates, the old tags no longer exist 
        //and the mapping has been done
        if (processedTags.Contains(nameOfOldTag))
        {
            return;
        }

        //get tag, the 'oldTag' string adds SQL escape for single quotes
        var tag = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\'", nameOfOldTag.Replace("\'", "\'\'")), null).FirstOrDefault<TagInfo>();
        if (tag == null)
        {
            return;
        }

        if (TagShouldBeRemoved(nameOfNewTag))
        {
            RemoveTag(tag, dbConnection);
            removedTagsList.Add(tag.TagName);
        }

        else
        {
            var newTag = GetTagToMap(nameOfNewTag, tag.TagGroupID);

            try { RemapTag(tag, newTag, dbConnection); }
            catch (Exception ex)
            {
                return;
            }
            mappedTagsList.Add(new Tuple<string, string>(nameOfOldTag, nameOfNewTag));
        }

        //get rid of the old tag
        try
        {
            if (!TagHasRemainingReferences(tag.TagID, dbConnection))
            {
                TagInfoProvider.DeleteTagInfo(tag.TagID);
            }
        }
        catch (Exception ex)
        {
            return;
        }
        //keep a record of the tag in case of duplicates
        processedTags.Add(nameOfOldTag);
        
    }

    private bool TagHasRemainingReferences(int tagId, GeneralConnection dbConnection)
    {
        return int.Parse(
            dbConnection.ExecuteScalar("SELECT COUNT(*) FROM CMS_DocumentTag WHERE TagID = @TagId", new QueryDataParameters() 
                                                                                                    { 
                                                                                                        new DataParameter("@TagId", tagId) 
                                                                                                    }
                                                                                                    , QueryTypeEnum.SQLQuery, false)
                                                                                                    .ToString()) > 0;
    }

    private bool TagShouldBeRemoved(string nameOfNewTag)
    {
        return nameOfNewTag == null || nameOfNewTag.ToLower().StartsWith("remove");
    }

    private bool RowIsDuplicate(string nameOfOldTag)
    {
        // pages within a tag-search are differentiated using a query string.  
        //We want to ignore additional pages because they are suplicates for the purpose of this process.
        //The first page within a tag-search has no query string, so those are the ones we want.
        return nameOfOldTag.Contains('?');  
    }

    private void RemoveTag(TagInfo tag, GeneralConnection dbConnection)
    {
        int removedTagLinks = dbConnection.ExecuteNonQuery("DELETE FROM CMS_DocumentTag WHERE TagID = @TagId", new QueryDataParameters()
                                                                                                {
                                                                                                    new DataParameter("@TagId",tag.TagID)
                                                                                                }, QueryTypeEnum.SQLQuery, false
                                                                                    );

        tag.TagCount -= removedTagLinks;
        TagInfoProvider.SetTagInfo(tag);
    }

    private TagInfo GetTagToMap(string tagName, int tagGroupId)
    {
        //grab tag that will replace the old tag
        var newTag = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\'", tagName.Replace("\'","\'\'")), null).FirstOrDefault<TagInfo>();

        //if the tag doesn't exist, create one
        if (newTag == null)
        {
            TagInfoProvider.SetTagInfo(new TagInfo()
            {
                TagName = tagName,
                TagCount = 0,
                TagGroupID = tagGroupId
            });

            newTag = TagInfoProvider.GetTags(String.Format("TagName = \'{0}\'", tagName.Replace("\'","\'\'")), null).FirstOrDefault<TagInfo>();
        }

        return newTag;
    }

    private void RemapTag(TagInfo oldTag, TagInfo newTag, GeneralConnection dbConnection)
    {
        if (!TagMappingAlreadyExists(oldTag.TagID, newTag.TagID, dbConnection))
        {

            //map new tags to the documents with the old tag
            var numberOfUpdates = dbConnection.ExecuteNonQuery("UPDATE CMS_DocumentTag SET TagID = @NewTagId WHERE TagID = @OldTagId", new QueryDataParameters()
                                                                                                                                                {
                                                                                                                                                    new DataParameter("@OldTagId",oldTag.TagID),
                                                                                                                                                    new DataParameter("@NewTagId",newTag.TagID)
                                                                                                                                                },
                                                                                                                                                QueryTypeEnum.SQLQuery, false
            );

            newTag.TagCount += numberOfUpdates;
            TagInfoProvider.SetTagInfo(newTag);        
        }
    }


    private bool TagMappingAlreadyExists(int oldTagId, int newTagId, GeneralConnection dbConnection)
    {

        var query = "SELECT COUNT(*) FROM CMS_DocumentTag dt1 INNER JOIN  CMS_DocumentTag dt2 ON dt1.DocumentID = dt2.DocumentID WHERE dt1.TagID = @OldTagId AND dt2.TagID = @NewTagId";


        var result = dbConnection.ExecuteScalar(query, new QueryDataParameters() 
                                                            { 
                                                                new DataParameter("@OldTagId",oldTagId),
                                                                new DataParameter("@NewTagId", newTagId)
                                                            }, QueryTypeEnum.SQLQuery, false);
        return int.Parse(result.ToString()) > 0;
    }


    private void ProcessResults(List<string> removedTagsList, List<Tuple<string, string>> mappedTagsList)
    {
        removedTagsList.Sort();
        mappedTagsList.Sort((x, y) => x.Item1.CompareTo(y.Item1));

        if (removedTagsList.Count > 0)
        {
            _sb.Append("<h2>Removed Tags</h2>");

            foreach (var removedTag in removedTagsList)
            {
                _sb.AppendFormat("<p>{0}</p>", removedTag);
            }
        }

        if (mappedTagsList.Count > 0)
        {
            _sb.Append("<h2>Mapped Tags</h2>");

            foreach (var mappedTag in mappedTagsList)
            {
                _sb.AppendFormat("<p>{0} -> {1}</p>", mappedTag.Item1, mappedTag.Item2);
            }
        }
    }
                                                                                                                                                                                                                      
}
