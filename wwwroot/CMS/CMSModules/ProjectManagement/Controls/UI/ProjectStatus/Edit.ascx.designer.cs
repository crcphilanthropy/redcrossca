﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSModules_ProjectManagement_Controls_UI_Projectstatus_Edit {
    
    /// <summary>
    /// plcMess control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.MessagesPlaceHolder plcMess;
    
    /// <summary>
    /// lblStatusDisplayName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusDisplayName;
    
    /// <summary>
    /// txtStatusDisplayName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_System_LocalizableTextBox txtStatusDisplayName;
    
    /// <summary>
    /// rfvStatusDisplayName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSRequiredFieldValidator rfvStatusDisplayName;
    
    /// <summary>
    /// lblStatusName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusName;
    
    /// <summary>
    /// txtStatusName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_System_CodeName txtStatusName;
    
    /// <summary>
    /// rfvStatusName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSRequiredFieldValidator rfvStatusName;
    
    /// <summary>
    /// lblStatusColor control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusColor;
    
    /// <summary>
    /// colorPicker control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.Controls.ColorPicker colorPicker;
    
    /// <summary>
    /// lblStatusIcon control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusIcon;
    
    /// <summary>
    /// txtStatusIcon control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSTextBox txtStatusIcon;
    
    /// <summary>
    /// lblStatusIsNotStarted control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusIsNotStarted;
    
    /// <summary>
    /// chkStatusIsNotStarted control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSCheckBox chkStatusIsNotStarted;
    
    /// <summary>
    /// lblStatusIsFinished control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusIsFinished;
    
    /// <summary>
    /// chkStatusIsFinished control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSCheckBox chkStatusIsFinished;
    
    /// <summary>
    /// lblStatusEnabled control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblStatusEnabled;
    
    /// <summary>
    /// chkStatusEnabled control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSCheckBox chkStatusEnabled;
    
    /// <summary>
    /// btnOk control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormSubmitButton btnOk;
}
