﻿cmsdefine(['CMS/WebFormCaller'], function (webFormCaller) {
    'use strict';

    var Module = function(data) {
        var received = 0,
            callBackParam,
            asyncProcessFinished = false,
            timeout = null,
            asyncBusy = false,
            config = data,

            setNextTimeout = function() {
                timeout = setTimeout(getAsyncStatus, 200);
            },

            getAsyncStatus = function() {
                if (!asyncBusy) {
                    try {
                        asyncBusy = true;
                        setTimeout('asyncBusy = false;', 2000);
                        callBackParam = received + '|' + config.machineName;
                        var cbOptions = {
                            targetControlUniqueId: config.uniqueId,
                            args: callBackParam,
                            successCallback: receiveAsyncStatus,
                            errorCallback: receiveError
                        };
                        webFormCaller.doCallback(cbOptions);
                    } catch (ex) {
                        setNextTimeout();
                        throw ex;
                    }
                }

                if (asyncProcessFinished) {
                    cancel(false);
                    return;
                } else {
                    setNextTimeout();
                }
            },

            setClose = function() {
                var cancelElem = document.getElementById(config.id + '_btnCancel');
                if (cancelElem != null) {
                    cancelElem.value = config.closeText;
                }
            },

            receiveError = function(rvalue) {
                setNextTimeout();
            },

            receiveAsyncStatus = function(rvalue) {
                var totalReceived, i, resultValue, values, code;

                asyncBusy = false;
                if (asyncProcessFinished) {
                    return;
                }

                values = rvalue.split('|');

                code = values[0];
                totalReceived = parseInt(values[1]);

                resultValue = '';

                for (i = 2; i < values.length; i++) {
                    resultValue += values[i];
                }

                if (resultValue != '') {
                    setLog(resultValue, totalReceived);
                }

                if (code == 'running') {
                } else if (code == 'finished') {
                    asyncProcessFinished = true;

                    webFormCaller.doPostback({
                        targetControlUniqueId: config.uniqueId,
                        args: 'finished|' + config.machineName
                    });
                } else if ((code == 'threadlost') || (code == 'stopped')) {
                    asyncProcessFinished = true;
                    setClose();
                } else if (code == 'error') {
                    asyncProcessFinished = true;

                    if (config.postbackOnError) {
                        webFormCaller.doPostback({
                            targetControlUniqueId: config.uniqueId,
                            args: 'error|' + config.machineName
                        });
                    } else {
                        setClose();
                    }
                }
            },

            setLog = function(text, totalReceived) {
                var elem = document.getElementById(config.logId),
                    lines,
                    log;

                received = totalReceived;

                lines = text.split('\n');

                log = '';
                if (config.reversed) {
                    for (var i = lines.length - 1; i >= 0; i--) {
                        if (i != 0) {
                            log += lines[i] + '<br />';
                        } else {
                            log += lines[i];
                        }
                    }
                } else {
                    for (var i = 0; i < lines.length; i++) {
                        if (i != lines.length) {
                            log += lines[i] + '<br />';
                        } else {
                            log += lines[i];
                        }
                    }
                }

                var node = document.createElement("span");
                node.innerHTML = log;

                if (elem.childNodes.count < 1) {
                    elem.appendChild(node);
                } else {
                    elem.insertBefore(node, elem.childNodes[0]);
                }
            },

            cancel = function(withPostback) {
                var t;

                asyncProcessFinished = true;
                if (withPostback) {
                    webFormCaller.doPostback({
                        targetControlUniqueId: config.cancelButtonUniqueId
                    });
                } else {
                    t = timeout;
                    if ((t != 'undefined') && (t != null)) {
                        clearTimeout(timeout);
                    }
                }
            };

        setNextTimeout();

        window.CMS = window.CMS || {};

        return window.CMS['AC_' + config.id] = {
            cancel: cancel
        };
    };

    return Module;
});