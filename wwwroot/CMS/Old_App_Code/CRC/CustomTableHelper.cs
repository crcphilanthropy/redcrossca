using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CustomTables;
using CMS.Helpers;
using CMS.Membership;

namespace CMSApp.CRC
{
    public class CustomTableHelper
    {
        /// <summary>
        /// Gets custom table item by id
        /// </summary>
        /// <param name="className"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static CustomTableItem GetItem(string className, int itemId)
        {
            return CustomTableItemProvider.GetItem(itemId, className);
        }

        /// <summary>
        /// Get custom table item by guid
        /// </summary>
        /// <param name="className"></param>
        /// <param name="itemGuid"></param>
        /// <returns></returns>
        public static CustomTableItem GetItem(string className, Guid itemGuid)
        {
            return CustomTableItemProvider.GetItem(itemGuid, className);
        }

        /// <summary>
        /// Get Custom table items
        /// </summary>
        /// <param name="className"></param>
        /// <param name="whereClause"></param>
        /// <param name="orderBy"></param>
        /// <param name="topN"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static DataSet GetItems(string className, string whereClause = null, string orderBy = null, int topN = -1, string columns = "")
        {
            return CustomTableItemProvider.GetItems(className, whereClause, orderBy, topN, columns);
        }

        /// <summary>
        /// Get custom table items as list of specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className"></param>
        /// <param name="whereClause"></param>
        /// <param name="orderBy"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetItems<T>(string className, string whereClause = null, string orderBy = null, string columns = null) where T : class , new()
        {
            var toReturn = new List<T>();
            var data = CustomTableItemProvider.GetItems(className, whereClause, orderBy, columns: columns);

            var properties = typeof(T).GetProperties();
            if (DataHelper.DataSourceIsEmpty(data)) return toReturn;

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var item = new T();
                foreach (var propertyInfo in properties.Where(propertyInfo => data.Tables[0].Columns.Contains(propertyInfo.Name) && row[propertyInfo.Name] != DBNull.Value))
                {
                    propertyInfo.SetValue(item, row[propertyInfo.Name], null);
                }
                toReturn.Add(item);
            }

            return toReturn;
        }


        /// <summary>
        /// Get key and value items for binding
        /// </summary>
        /// <param name="className"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="textColumnName"></param>
        /// <param name="whereClause"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetBindingItems(string className, string keyColumnName,
                                                               string textColumnName, string whereClause = null,
                                                               string orderBy = null)
        {
            var items = GetItems(className, whereClause, orderBy, -1, string.Format("{0}, {1}", keyColumnName, textColumnName));
            return DataHelper.DataSourceIsEmpty(items)
                ? new Dictionary<string, string>()
                : items.Tables[0].Rows.Cast<DataRow>().ToDictionary(row => ValidationHelper.GetString(row[keyColumnName], string.Empty), row => ResHelper.LocalizeString(ValidationHelper.GetString(row[textColumnName], string.Empty)));
        }

        /// <summary>
        /// Get custom table item value for display
        /// </summary>
        /// <param name="className"></param>
        /// <param name="whereClause"></param>
        /// <param name="columnToReturn"></param>
        /// <returns></returns>
        public static object GetItemValue(string className, string whereClause, string columnToReturn, string orderBy = null)
        {
            var ds = CustomTableItemProvider.GetItems(className, whereClause, orderBy);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains(columnToReturn))
                return ds.Tables[0].Rows[0][columnToReturn];

            return null;
        }


        /// <summary>
        /// Get Custom table list of values for display
        /// </summary>
        /// <param name="className"></param>
        /// <param name="whereClause"></param>
        /// <param name="columnToReturn"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string GetItemsValue(string className, string whereClause, string columnToReturn, string separator = "|")
        {
            var ds = CustomTableItemProvider.GetItems(className, whereClause, string.Empty);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains(columnToReturn))
            {
                var toReturn = (from DataRow row in ds.Tables[0].Rows
                                where row[columnToReturn] != null && row[columnToReturn] != DBNull.Value && !string.IsNullOrWhiteSpace(row[columnToReturn].ToString())
                                select row[columnToReturn].ToString()).ToList();

                toReturn = toReturn.Select(item => ResHelper.LocalizeString(item)).ToList();
                return string.Join(separator, toReturn).Trim();
            }

            return null;
        }

        /// <summary>
        /// Insert item into custom table
        /// </summary>
        /// <param name="className"></param>
        /// <param name="columnAndValues"></param>
        /// <returns></returns>
        public static int InsertCustomTableItem(string className, Dictionary<string, object> columnAndValues)
        {
            var item = CustomTableItem.New(className);

            foreach (var columnAndValue in columnAndValues)
            {
                item.SetValue(columnAndValue.Key, columnAndValue.Value);
            }

            item.Insert();

            return item.ItemID;
        }

        /// <summary>
        /// Insert item into custom table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className"></param>
        /// <param name="itemToAdd"></param>
        /// <returns></returns>
        public static int InsertCustomTableItem<T>(string className, T itemToAdd)
        {
            var properties = typeof(T).GetProperties();
            return InsertCustomTableItem(className, properties.ToDictionary(propertyInfo => propertyInfo.Name, propertyInfo => propertyInfo.GetValue(itemToAdd, null)));
        }

        /// <summary>
        /// Insert multiple items into custom table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className"></param>
        /// <param name="itemsToAdd"></param>
        public static void InsertCustomTableItems<T>(string className, List<T> itemsToAdd)
        {
            var properties = typeof(T).GetProperties();
            itemsToAdd.ForEach(itemToAdd => InsertCustomTableItem(className, properties.ToDictionary(propertyInfo => propertyInfo.Name, propertyInfo => propertyInfo.GetValue(itemToAdd, null))));
        }


        /// <summary>
        /// Update custom table item
        /// </summary>
        /// <param name="className"></param>
        /// <param name="columnAndValues"></param>
        /// <param name="itemToUpdate"></param>
        /// <returns></returns>
        public static int UpdateCustomTableItem(string className, Dictionary<string, object> columnAndValues, DataRow itemToUpdate)
        {

            var item = CustomTableItem.New(className, itemToUpdate);

            foreach (var columnAndValue in columnAndValues)
            {
                item.SetValue(columnAndValue.Key, columnAndValue.Value);
            }

            item.Update();

            return item.ItemID;
        }


        public static int UpdateCustomTableItem<T>(string className, T newValues, DataRow itemToUpdate)
        {
            var properties = typeof(T).GetProperties();
            return UpdateCustomTableItem(className, properties.ToDictionary(propertyInfo => propertyInfo.Name, propertyInfo => propertyInfo.GetValue(newValues, null)), itemToUpdate);
        }


        /// <summary>
        /// Delete item from custom table
        /// </summary>
        /// <param name="className"></param>
        /// <param name="id"></param>
        public static void DeleteCustomTableItem(string className, int id)
        {
            var item = CustomTableItemProvider.GetItem(id, className);
            item.Delete();
        }

        /// <summary>
        /// Delete multiple items from custom table
        /// </summary>
        /// <param name="className"></param>
        /// <param name="values"></param>
        /// <param name="columnName"></param>
        public static void DeleteCustomTableItems(string className, List<string> values, string columnName)
        {
            var items = CustomTableItemProvider.GetItems(className, String.Format("{0} in ('{1}')", columnName, values.Join("', '")), string.Empty);

            if (items == null || items.Tables.Count <= 0 || items.Tables[0].Rows.Count <= 0) return;

            foreach (DataRow row in items.Tables[0].Rows)
            {
                DeleteCustomTableItem(className, ValidationHelper.GetInteger(row["ItemID"], 0));
            }
        }

        /// <summary>
        /// Delete all items from custom table
        /// </summary>
        /// <param name="className"></param>
        public static void DeleteAllCustomTableItems(string className)
        {
            CustomTableItemProvider.DeleteItems(className);
        }
    }
}
