﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSAppAppCode.Old_App_Code.CRC.WebControls
{
    public class BaseTransformation : CMS.Controls.CMSTransformation
    {
        protected void BtnSignUpServerClick(object sender, EventArgs e)
        {
            var txtEmail = FindControl("SignUpEmailCTATextBox") as System.Web.UI.HtmlControls.HtmlInputText;
            var hidSubscriptionEmailURL = FindControl("hidSubscriptionEmailURL") as HiddenField;
            if (txtEmail == null || hidSubscriptionEmailURL == null) return;

            var email = Request.Form[txtEmail.UniqueID];
            if (string.IsNullOrWhiteSpace(email))
                email = txtEmail.Value;

            var url = hidSubscriptionEmailURL.Value;

            Response.Redirect(URLHelper.AddParameterToUrl(url, "email", email));
        }

        protected string GetNewsThumbnailImage(Guid thumbnailGuid, string braftonPath, string nodeAliasPath, bool resize = false)
        {
            if (thumbnailGuid == Guid.Empty && string.IsNullOrWhiteSpace(braftonPath))
                return string.Empty;

            if (!string.IsNullOrWhiteSpace(braftonPath))
                return braftonPath.StartsWith("http", StringComparison.OrdinalIgnoreCase)
                           ? braftonPath
                           : string.Format("/CRC/Brafton/{0}", braftonPath);

            return thumbnailGuid != Guid.Empty ?
                resize ? URLHelper.AddParameterToUrl(AttachmentInfoProvider.GetAttachmentUrl(thumbnailGuid, nodeAliasPath), "width", "146") : AttachmentInfoProvider.GetAttachmentUrl(thumbnailGuid, nodeAliasPath)
                : string.Empty;
        }

        private IService _currentService;
        public IService CurrentService
        {
            get
            {
                return _currentService ??
                       (_currentService = new ServiceProvider().GetByDocumentGuid(CurrentDocument.DocumentGUID));
            }
        }

        private IBranch _currentBranch;
        public IBranch CurrentBranch
        {
            get
            {
                return _currentBranch ??
                       (_currentBranch = new BranchProvider().GetByGuid(CurrentDocument.DocumentGUID));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            BindBranchServices();
        }

        private void BindBranchServices()
        {
            var rptItems = FindControl("rptBranchServices") as Repeater;
            if (rptItems == null)
                return;

            IEnumerable<IPageMenu> services = new List<IPageMenu>();
            if (CurrentService != null && CurrentService.Branch != null)
                services = CurrentService.Branch.Services;

            if (CurrentBranch != null)
                services = CurrentBranch.UseCategories ? CurrentBranch.GetCategories(className: "CRC.IYCSimpleCategory") : CurrentBranch.Services;

            rptItems.DataSource = services;
            rptItems.DataBind();
        }
    }
}