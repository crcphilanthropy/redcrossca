using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMS.WorkflowEngine;

namespace CMSApp.CRC
{
    public class CRCDocumentHelper
    {
        public static TreeNode SaveFolder(TreeProvider treeProvider, string folderName, string folderClassName, TreeNode parentNode, string cultureCode, string defaultCulture = "en-CA", Dictionary<string, object> values = null)
        {
            var folderPath = string.Format("{0}/{1}", parentNode.NodeAliasPath, folderName);
            folderPath = folderPath.Replace(" ", "-");
            folderPath = Regex.Replace(folderPath, "-{2,6}", "-");
            var nodeFolder = treeProvider.SelectSingleNode(SiteContext.CurrentSiteName, folderPath, cultureCode);
            if (nodeFolder != null)
                return nodeFolder;

            var isNewCulture = false;
            if (!String.Equals(cultureCode, defaultCulture, StringComparison.OrdinalIgnoreCase))
            {
                nodeFolder = treeProvider.SelectSingleNode(SiteContext.CurrentSiteName, folderPath, defaultCulture);
                isNewCulture = nodeFolder != null;
            }


            if (nodeFolder == null)
                nodeFolder = TreeNode.New(folderClassName, treeProvider);

            nodeFolder.NodeName = folderName;
            nodeFolder.NodeAlias = folderName;
            nodeFolder.DocumentName = folderName;
            nodeFolder.DocumentCulture = cultureCode;
            nodeFolder.SetValue("MenuItemName", folderName);
            nodeFolder.SetValue("DocumentName", nodeFolder.DocumentName);

            if (values != null)
            {
                foreach (var val in values)
                {
                    nodeFolder.SetValue(val.Key, val.Value);
                }
            }

            if (isNewCulture)
                nodeFolder.InsertAsNewCultureVersion(cultureCode);
            else
                nodeFolder.Insert(parentNode);

            PublishDocument(nodeFolder, treeProvider);

            return nodeFolder;
        }

        public static void PublishDocument(TreeNode node, TreeProvider treeProvider)
        {
            var workFlowManager = WorkflowManager.GetInstance(treeProvider);

            var workflow = workFlowManager.GetNodeWorkflow(node);
            if (workflow == null) return;

            var workFlowStep = workFlowManager.GetPublishedWorkflowStep(node);

            if (workFlowStep != null && node.Parent != null)
                workFlowManager.MoveToSpecificStep(node, workFlowStep, "Auto Document Published", true, WorkflowTransitionTypeEnum.Automatic);
        }

        public static void SetEditWorkFlow(TreeNode node, TreeProvider treeProvider)
        {
            var workFlowManager = WorkflowManager.GetInstance(treeProvider);

            var workflow = workFlowManager.GetNodeWorkflow(node);
            if (workflow == null) return;

            var workFlowStep = workFlowManager.GetFirstWorkflowStep(node);
            if (workFlowStep != null && node.Parent != null)
                workFlowManager.MoveToSpecificStep(node, workFlowStep, "Auto Document Edit", true, WorkflowTransitionTypeEnum.Automatic);
        }

        public static void SetWorkFlow(TreeNode node, TreeProvider treeProvider, string stepName)
        {
            var workFlowManager = WorkflowManager.GetInstance(treeProvider);

            var workflow = workFlowManager.GetNodeWorkflow(node);
            if (workflow == null) return;

            if (node.IsCheckedOut)
            {
                node.CheckIn();
            }

            var workFlowStep = workFlowManager.GetNextStepInfo(node).FirstOrDefault(item => item.StepName.ToLower().Contains((stepName ?? String.Empty).ToLower()));

            if (workFlowStep != null)
                workFlowManager.MoveToSpecificStep(node, workFlowStep, "Auto Workflow Update", true, WorkflowTransitionTypeEnum.Automatic);
        }
    }
}