﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.PortalEngine;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using LinqToExcel;

namespace CMSAppAppCode.Old_App_Code.CRC.Modules
{
  public class IYCImportModule
  {

    public class IYCLogItem
    {
      public string Name { get; set; }
      public string Path { get; set; }
      public string Note { get; set; }
      public SaveAction SaveAction { get; set; }
    }

    public class IYCImportStat
    {
      public string Province { get; set; }
      public IYCStatCounter Branches { get; set; }
      public IYCStatCounter Categories { get; set; }
      public IYCStatCounter SubCategories { get; set; }
      public IYCStatCounter Services { get; set; }
    }

    public class IYCStatCounter
    {
      public int Total { get; set; }
      public int Inserted { get; set; }
      public int Updated { get; set; }
    }

    public enum SaveAction
    {
      Error = -2,
      Warning = -1,
      None = 0,
      Insert = 1,
      Update = 2,
      Finish = 3
    }

    private readonly ExcelQueryFactory _excelQueryFactory;

    private IEnumerable<Province> _provinces;
    private IEnumerable<Branch> _branches;
    private readonly IList<Branch> _updatedBranches = new List<Branch>();
    private readonly string _culture;
    private readonly string _siteName;
    private readonly string _alternateCulture;
    private readonly TreeProvider _treeProvider;

    private readonly int _provinceTemplateId;
    private readonly int _branchTemplateId;
    private readonly int _categoryTemplateId;
    private readonly int _serviceTemplateId;
    private readonly int _findABranchTemplateId;

    private readonly List<IYCImportStat> _stats = new List<IYCImportStat>();
    public List<IYCImportStat> Stats { get { return _stats; } }

    private readonly List<IYCLogItem> _logItems = new List<IYCLogItem>();
    public List<IYCLogItem> LogItems { get { return _logItems; } }

    public IYCImportModule(string filePath, string culture, string siteName)
    {
      _excelQueryFactory = new ExcelQueryFactory(filePath);
      _siteName = siteName;
      _culture = culture;
      _alternateCulture = _culture.Equals("en-CA", StringComparison.OrdinalIgnoreCase)
                              ? "fr-CA"
                              : "en-CA";
      _treeProvider = new TreeProvider(MembershipContext.AuthenticatedUser);

      SetTemplateIDs("CRC.PageTemplate.IYCProvincePage", ref _provinceTemplateId);
      SetTemplateIDs("CRC.PageTemplate.IYCBranchDetailPage", ref _branchTemplateId);
      SetTemplateIDs("CRC.PageTemplate.IYCCategoryPage", ref _categoryTemplateId);
      SetTemplateIDs("CRC.PageTemplate.IYCServiceDetailPage", ref _serviceTemplateId);
      SetTemplateIDs("CRC.PageTemplate.IYCFindABranchFilter", ref _findABranchTemplateId);
    }

    public bool IsValidFile()
    {
      var worksheets = new Dictionary<string, List<string>>
                {
                    {"Provinces",new List<string>{"Name", "ProvinceCode", "Latitude", "Longitude", "MetaTitle", "MetaDescription","MetaKeywords"}},
                    {"Branches",new List<string>{"BranchRowNumber", "Name", "Summary", "BranchAddress", "BranchHours", "BranchContact", "Latitude", "Longitude", "ProvinceCode", "MetaTitle", "MetaDescription", "MetaKeywords"}},
                    {"Categories",new List<string>{"CategoryRowNumber", "Name", "Description", "ProvinceCode", "MetaTitle","MetaDescription", "MetaKeywords"}},
                    {"Sub-Categories",new List<string>{"SubCategoryRowNumber", "CategoryRowNumber", "Name", "Description", "MetaTitle","MetaDescription", "MetaKeywords"}},
                    {"Services",new List<string>{"ServiceRowNumber", "Name", "Description", "HoursOfOperation", "Contact","Address", "Latitude", "Longitude", "BranchRowNumber", "CategoryRowNumber", "SubCategoryRowNumber", "MetaTitle", "MetaDescription","MetaKeywords"}}
                };

      foreach (var worksheet in worksheets)
      {
        if (!_excelQueryFactory.GetWorksheetNames().Contains(worksheet.Key, new StringEqualityComparer()))
          return false;

        foreach (var column in worksheet.Value)
        {
          if (!_excelQueryFactory.GetColumnNames(worksheet.Key).Contains(column, new StringEqualityComparer()))
            return false;
        }
      }

      return true;
    }

    private void SetTemplateIDs(string templateName, ref int templateId)
    {
      var pageTemplate = PageTemplateInfoProvider.GetPageTemplateInfo(templateName);
      if (pageTemplate != null)
        templateId = pageTemplate.PageTemplateId;
    }

    public void Run()
    {
      try
      {
        _provinces = (from item in _excelQueryFactory.Worksheet<Province>("Provinces")
                      select item).Skip(1);

        _branches = (from item in _excelQueryFactory.Worksheet<Branch>("Branches")
                     select item).Skip(1);

        var parentNode = _treeProvider.SelectSingleNode(_siteName, "/in-your-community", _culture);

        _provinces.ToList().ForEach(province =>
            {
              if (province.ProvinceCode.Equals("N/A", StringComparison.OrdinalIgnoreCase))
                return;

              var provinceStat = new IYCImportStat
              {
                Province = province.Name,
                Branches = new IYCStatCounter(),
                Categories = new IYCStatCounter(),
                SubCategories = new IYCStatCounter(),
                Services = new IYCStatCounter()
              };
              SaveAction status;
              var provinceNode = Save(province, "CRC.Province", parentNode,
                                              string.Format("ProvinceCode = '{0}'", province.ProvinceCode), 0,
                                              _provinceTemplateId, out status);
              if (provinceNode == null) return;

              ImportBranches(
                          _branches.Where(
                              item =>
                              item.ProvinceCode.Equals(province.ProvinceCode, StringComparison.OrdinalIgnoreCase))
                                   .ToList(), provinceNode, ref provinceStat);

              var categories = from item in _excelQueryFactory.Worksheet<PageMenu>("Categories")
                               where
                                           item.ProvinceCode.Equals(province.ProvinceCode,
                                                                    StringComparison.OrdinalIgnoreCase)
                               select item;

              ImportCategories(categories.ToList(), provinceNode, ref provinceStat);

              _stats.Add(provinceStat);
            });

        System.IO.File.Delete(_excelQueryFactory.FileName);

        CustomTableHelper.DeleteCustomTableItems("CRC.SerializedObjects", new List<string> { "IYCImportStats" },
                                                 "Name");
        CustomTableHelper.InsertCustomTableItem("CRC.SerializedObjects", new Dictionary<string, object>
                    {
                        {"Name", "IYCImportStats"},
                        {"Value", new JavaScriptSerializer().Serialize(_stats)}
                    });

        LogItem(new IYCLogItem { SaveAction = SaveAction.Finish });
      }
      catch (Exception ex)
      {
        LogItem(new IYCLogItem
        {
          Name = ex.Message,
          Note = ex.StackTrace,
          SaveAction = SaveAction.Error
        });
        EventLogProvider.LogException("IYCImportModule", "Run", ex);
      }
    }

    private void ImportCategories(ICollection<PageMenu> categories, TreeNode parentNode, ref IYCImportStat provinceStats)
    {
      provinceStats.Categories.Total = categories.Count();
      foreach (var category in categories)
      {
        SaveAction status;
        var categoryNode = Save(category, "CRC.IYCCategory", parentNode, string.Format("RowNumber = {0}", category.CategoryRowNumber), category.CategoryRowNumber, _categoryTemplateId, out status);
        if (categoryNode == null) continue;

        if (status == SaveAction.Update)
          provinceStats.Categories.Updated++;
        else
          provinceStats.Categories.Inserted++;

        var tempCategory = category;
        var subCategories = (from item in _excelQueryFactory.Worksheet<PageMenu>("Sub-Categories")
                             where item.CategoryRowNumber.Equals(tempCategory.CategoryRowNumber)
                             select item).ToList();

        ImportSubCategories(subCategories, categoryNode, ref provinceStats);

        var services = from item in _excelQueryFactory.Worksheet<Service>("Services")
                       where item.CategoryRowNumber.Equals(tempCategory.CategoryRowNumber) && item.SubCategoryRowNumber < 1
                       select item;

        ImportServices(services.ToList(), categoryNode, ref provinceStats);
      }
    }

    private void ImportServices(ICollection<Service> services, TreeNode parentNode, ref IYCImportStat provinceStats)
    {
      provinceStats.Services.Total += services.Count;
      foreach (var service in services)
      {
        var branch = _updatedBranches.FirstOrDefault(item => item.BranchRowNumber == service.BranchRowNumber);
        if (branch == null) continue;

        var branchNode = SaveLinkedDocument(parentNode, string.Format("RowNumber = {0}", service.BranchRowNumber), "CRC.IYCBranch", branch.NodeAliasPath);
        if (branchNode == null) continue;

        SaveAction status;
        var serviceNode = Save(service, "CRC.IYCService", branchNode, string.Format("RowNumber = {0}", service.ServiceRowNumber), service.ServiceRowNumber, _serviceTemplateId, out status);
        if (serviceNode == null) continue;

        switch (status)
        {
          case SaveAction.Update:
            provinceStats.Services.Updated++;
            break;
          case SaveAction.Insert:
            provinceStats.Services.Inserted++;
            break;
        }
      }
    }

    private void ImportSubCategories(ICollection<PageMenu> subCategories, TreeNode parentNode, ref IYCImportStat provinceStats)
    {
      provinceStats.SubCategories.Total += subCategories.Count;
      foreach (var subCategory in subCategories)
      {
        SaveAction status;
        var subCategoryNode = Save(subCategory, "CRC.IYCSubCategory", parentNode, string.Format("RowNumber = {0}", subCategory.SubCategoryRowNumber), subCategory.SubCategoryRowNumber, 0, out status);
        if (subCategoryNode == null) continue;

        if (status == SaveAction.Update)
          provinceStats.SubCategories.Updated++;
        else
          provinceStats.SubCategories.Inserted++;

        var tempSubCategory = subCategory;
        var services = from item in _excelQueryFactory.Worksheet<Service>("Services")
                       where item.SubCategoryRowNumber.Equals(tempSubCategory.SubCategoryRowNumber)
                       select item;

        ImportServices(services.ToList(), subCategoryNode, ref provinceStats);
      }
    }

    private void ImportBranches(List<Branch> branches, TreeNode parentNode, ref IYCImportStat provinceStats)
    {
      var findABranchNode = _treeProvider.SelectSingleNode(_siteName, string.Format("{0}/find-a-branch", parentNode.NodeAliasPath), _culture);
      provinceStats.Branches.Total = branches.Count();

      if (findABranchNode == null)
      {
        SaveAction status;
        findABranchNode = Save(new PageMenu { Name = "Find A Branch" }, "CRC.IYCBranchFolder", parentNode, null, 0, _findABranchTemplateId, out status);
      }

      foreach (var branch in branches)
      {
        SaveAction status;
        var branchNode = Save(branch, "CRC.IYCBranch", findABranchNode, string.Format("RowNumber = {0}", branch.BranchRowNumber), branch.BranchRowNumber, _branchTemplateId, out status);
        if (branchNode == null) continue;

        if (status == SaveAction.Update)
          provinceStats.Branches.Updated++;
        else
          provinceStats.Branches.Inserted++;

        branch.NodeID = branchNode.NodeID;
        branch.DocumentGuid = branchNode.DocumentGUID;
        branch.NodeAliasPath = branchNode.NodeAliasPath;
        _updatedBranches.Add(branch);
      }

    }

    private TreeNode SaveLinkedDocument(TreeNode parentNode, string where, string className, string nodeAliasPath)
    {
      var ds = _treeProvider.SelectNodes(_siteName,
                                            string.Format("{0}/%", parentNode.NodeAliasPath), _culture, false,
                                            className, where, null, -1, true, 1);

      if (!DataHelper.DataSourceIsEmpty(ds))
        return ds.First();

      ds = _treeProvider.SelectNodes(_siteName,
                                            string.Format("{0}/%", parentNode.NodeAliasPath), _alternateCulture, false,
                                            className, where, null, -1, true, 1);

      TreeNode node;
      if (!DataHelper.DataSourceIsEmpty(ds))
      {
        node = ds.First();
        node.InsertAsNewCultureVersion(_culture);
        return node;
      }

      node = _treeProvider.SelectSingleNode(_siteName, nodeAliasPath, _culture);
      if (node == null)
        return null;

      node.InsertAsLink(parentNode);

      return node;
    }

    private TreeNode Save<T>(T item, string className, TreeNode parentNode, string where, int rowNumber, int templateId, out SaveAction status) where T : PageMenu
    {
      try
      {
        if (rowNumber < 0)
        {
          status = SaveAction.None;
          return null;
        }

        var toSave = DocumentHelper.GetDocuments(className)
                .OnSite(_siteName)
                .Path(string.Format("{0}/%", parentNode.NodeAliasPath))
                .Culture(_culture)
                .Where(where)
                .TopN(1)
                .CombineWithDefaultCulture(false)
                .FirstOrDefault();

        var isNew = false;
        var isNewCulture = false;
        if (toSave == null)
        {
          toSave = DocumentHelper.GetDocuments(className)
              .OnSite(_siteName)
              .Path(string.Format("{0}/%", parentNode.NodeAliasPath))
              .Culture(_alternateCulture)
              .Where(where)
              .CombineWithDefaultCulture(false)
              .TopN(1)
              .FirstOrDefault();


          if (toSave != null)
          {
            isNewCulture = true;
          }
        }

        if (toSave == null)
        {
          toSave = TreeNode.New(className);
          isNew = true;
        }

        if (!isNew && !isNewCulture)
        {
          toSave.CheckOut();
        }

        var properties = typeof(T).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
        var excludeProperties = new List<string>
                    {
                        "DocumentUrl",
                        "NodeAliasPath",
                        "NodeID",
                        "DocumentGUID",
                        "NodeGuid",
                        "NodeOrder",
                        "NodeParentID",
                        "Thumbnail",
                        "ThumbnailAltText",
                        "DocumentName",
                        "IconPath",
                        "ProvinceDisplayCode",
                        "FindServicesResultPage"
                    };

        foreach (var propertyInfo in properties.Where(prop => !excludeProperties.Contains(prop.Name, new StringEqualityComparer())))
        {
          toSave.SetValue(propertyInfo.Name, propertyInfo.GetValue(item, null));
        }

        toSave.DocumentName = item.Name.Trim();
        if (templateId > 0)
          toSave.SetValue("DocumentPageTemplateID", templateId);

        var pTagRx = new Regex("<p>.*</p>", RegexOptions.IgnoreCase);
        if (!string.IsNullOrWhiteSpace(item.Description) && !pTagRx.IsMatch(item.Description))
          item.Description = string.Format("<p>{0}</p>", item.Description);

        toSave.SetValue("Name", item.Name.Trim());
        toSave.SetValue("Summary", item.Summary);
        toSave.SetValue("Description", item.Description);
        toSave.SetValue("RowNumber", rowNumber);
        toSave.SetValue("DocumentPageTitle", item.MetaTitle);
        toSave.SetValue("DocumentPageKeywords", item.MetaKeywords);
        toSave.SetValue("DocumentPageDescription", item.MetaDescription);

        if (string.IsNullOrWhiteSpace(toSave.GetStringValue("IconPath", string.Empty)))
        {
          toSave.SetValue("IconPath", "/CRC/images/first-aid-cpr.svg");
        }

        if (isNew || isNewCulture)
        {
          toSave.SetValue("ShowRosBanner", -1);
          toSave.SetValue("ShowTwitter", -1);
          toSave.SetValue("ShowPromotionalBanner", -1);
          toSave.SetValue("ShowMobileShare", -1);
          toSave.SetValue("ShowMobileEmailSignUp", -1);
        }

        if (isNew)
        {
          toSave.DocumentCulture = _culture;
          toSave.SetValue("DocumentCulture", _culture);
          status = SaveAction.Insert;
          toSave.Insert(parentNode);
        }
        else if (isNewCulture)
        {
          status = SaveAction.Insert;
          toSave.InsertAsNewCultureVersion(_culture);
        }
        else
        {
          status = SaveAction.Update;
          toSave.Update();
          toSave.CheckIn();
        }

        LogItem(new IYCLogItem
        {
          Name = item.Name,
          Path =
                    string.IsNullOrWhiteSpace(toSave.DocumentUrlPath)
                        ? toSave.NodeAliasPath
                        : toSave.DocumentUrlPath,
          SaveAction = status
        });

        CRCDocumentHelper.PublishDocument(toSave, _treeProvider);
        toSave.Publish();

        return toSave;
      }
      catch (Exception ex)
      {
        status = SaveAction.Warning;
        LogItem(new IYCLogItem
        {
          Name = item.Name,
          Note = ex.Message,
          SaveAction = status
        });
        EventLogProvider.LogException("IYCImportModule", "Save", ex);

        return null;
      }
    }

    private void LogItem(IYCLogItem item)
    {
      _logItems.Add(item);
      CustomTableHelper.DeleteCustomTableItems("CRC.SerializedObjects", new List<string> { "IYCLogItems" }, "Name");
      CustomTableHelper.InsertCustomTableItem("CRC.SerializedObjects", new Dictionary<string, object>
                                                        {
                                                            {"Name", "IYCLogItems"},
                                                            {"Value", new JavaScriptSerializer().Serialize(_logItems)}
                                                        });
    }
  }
}