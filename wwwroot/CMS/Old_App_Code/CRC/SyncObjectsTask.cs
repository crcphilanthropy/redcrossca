﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

using CMS.DataEngine;
using CMS.EventLog;
using CMS.ExtendedControls;
using CMS.Helpers;
using CMS.Base;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls.ActionsConfig;
using CMS.Synchronization;
using CMS.Membership;
using CMS.Scheduler;
using CMS.EventLog;

namespace CMSAppAppCode.Old_App_Code.CRC
{
    public class SyncObjectsTask : ITask
    {



        public string Execute(TaskInfo task)
        {
            if ((task == null) || (task.TaskSiteID <= 0))
            {
                return "Only site-specific task is allowed to run content synchronization.";
            }
            try
            {
                SiteInfo siteInfo = SiteInfoProvider.GetSiteInfo(task.TaskSiteID);
                if (siteInfo != null)
                {
                    DataSet dataSource = StagingTaskInfoProvider.SelectObjectTaskList(siteInfo.SiteID, 0, "", null, null, 0, "TaskID");
                    if (!DataHelper.DataSourceIsEmpty(dataSource))
                    {
                        foreach (DataRow row in dataSource.Tables[0].Rows)
                        {
                            int taskId = ValidationHelper.GetInteger(row["TaskID"], 0, null);
                            if (taskId > 0)
                            {
                                StagingHelper.RunSynchronization(taskId, 0);
                            }
                        }
                    }
                    return null;
                }
                return "Task site not found.";
            }
            catch (Exception exception)
            {
                EventLogProvider.LogException("Content", "EXCEPTION", exception, 0, null, true);
                return exception.Message;
            }
        }


    }
}