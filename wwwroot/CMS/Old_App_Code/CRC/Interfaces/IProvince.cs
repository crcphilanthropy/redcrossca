﻿namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IProvince : IPageMenu, ICoordinates
    {
        string ProvinceCode { get; set; }
        string ProvinceDisplayCode { get; set; }
        string IconPath { get; set; }
        string FindServicesResultPage { get; set; }
    }
}
