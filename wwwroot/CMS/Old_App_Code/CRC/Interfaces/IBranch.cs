﻿using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IBranch : IPageMenu, ICoordinates
    {
        string BranchAddress { get; set; }
        string BranchHours { get; set; }
        string BranchContact { get; set; }
        IEnumerable<IService> Services { get; }
        IEnumerable<IPageMenu> GetCategories(string path = null, string className = null);
        bool UseCategories { get; set; }
    }
}
