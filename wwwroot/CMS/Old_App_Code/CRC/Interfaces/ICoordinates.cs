﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface ICoordinates
    {
        Decimal Longitude { get; set; }
        Decimal Latitude { get; set; }
        double Distance { get; set; }
    }
}
