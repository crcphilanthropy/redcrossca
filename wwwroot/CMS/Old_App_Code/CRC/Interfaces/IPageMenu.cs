﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IPageMenu
    {
        Guid DocumentGuid { get; set; }
        int NodeID { get; set; }
        int NodeParentID { get; set; }
        int NodeOrder { get; set; }
        string DocumentName { get; set; }
        string NodeAliasPath { get; set; }
        string DocumentUrlPath { get; set; }
        string DocumentLiveUrl { get; }
        string Name { get; set; }
        string Summary { get; set; }
        string Description { get; set; }
        Guid Thumbnail { get; set; }
        string ThumbnailAltText { get; set; }
    }
}
