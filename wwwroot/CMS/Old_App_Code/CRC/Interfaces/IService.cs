﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IService : IPageMenu, ICoordinates
    {
        Guid BranchDocumentGuid { get; set; }
        string Address { get; set; }
        string HoursOfOperation { get; set; }
        string Contact { get; set; }
        Guid CategoryDocumentGuid { get; set; }
        Guid SubCategoryDocumentGuid { get; set; }
        string CategoryName { get; set; }
        string CategoryUrl { get; set; }
        string CategoryIconPath { get; set; }
        string CategoryIconAltText { get; set; }
        string SubCategoryName { get; set; }
        string SubCategoryUrl { get; set; }
        string SubCategoryIconPath { get; set; }
        string SubCategoryIconAltText { get; set; }
        IBranch Branch { get; }
    }
}
