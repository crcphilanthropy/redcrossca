﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IServiceSearchConfig
    {
        Guid CategoryDocumentGuid { get; set; }
        Guid SubCategoryDocumentGuid { get; set; }
        Decimal Latitude { get; set; }
        Decimal Longitude { get; set; }
        int MaxDistance { get; set; }
        bool IsEmpty();
    }
}
