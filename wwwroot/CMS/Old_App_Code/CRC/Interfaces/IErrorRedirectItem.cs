﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRC.Interfaces
{
    public interface IErrorRedirectItem
    {
        Guid ItemGuid { get; set; }
        String Path { get; set; }
        string RedirectTo { get; set; }
        int StatusCode { get; set; }
        int Order { get; set; }
        string Language { get; set; }
        bool IsExactMatch { get; }

        bool IsTransfer { get; set; }
    }
}