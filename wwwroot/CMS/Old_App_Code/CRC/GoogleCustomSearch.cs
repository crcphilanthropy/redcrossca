﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using Ecentricarts.GoogleSiteSearch;
using Ecentricarts.GoogleSiteSearch.Model;
using Newtonsoft.Json;

namespace CMSAppAppCode.Old_App_Code.CRC
{
    public class GoogleCustomSearch
    {
        private readonly int _pageSize = 10;

        public GoogleCustomSearch(int pageSize)
        {
            _pageSize = pageSize;
        }

        public Search Search(string keyword, int currentPage, string language, string selectedSite, string fileType)
        {
            var serviceUrl = (string)CacheHelper.GetItem("global-search-api-url");
            if (string.IsNullOrEmpty(serviceUrl))
            {
                serviceUrl = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".GoogleSearchApiUrl");
                CacheHelper.Add("global-search-api-url", serviceUrl, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1440));
            }

            var lookupMethod = (string)CacheHelper.GetItem("global-search-lookup-method");
            if (string.IsNullOrEmpty(lookupMethod))
            {
                lookupMethod = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".GoogleSearchLookupMethod");
                CacheHelper.Add("global-search-lookup-method", lookupMethod, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1440));
            }

            var cx = (string)CacheHelper.GetItem("global-search-cx");
            if (string.IsNullOrEmpty(cx))
            {
                cx = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".GoogleSearchCx");
                CacheHelper.Add("global-search-cx", cx, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1440));
            }

            var apiKey = (string)CacheHelper.GetItem("global-search-api-key");
            if (string.IsNullOrEmpty(apiKey))
            {
                apiKey = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".GoogleSearchApiKey");
                CacheHelper.Add("global-search-api-key", apiKey, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1440));
            }

            var excludeFileTypes = (string)CacheHelper.GetItem("global-search-exclude-filetypes");
            if (string.IsNullOrEmpty(excludeFileTypes))
            {
                excludeFileTypes = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".GoogleSearchExcludeFileTypes");
                CacheHelper.Add("global-search-exclude-filetypes", excludeFileTypes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1440));
            }

            // Create the request
            //var language = string.Format("lang_{0}", LocalizationContext.CurrentCulture.CultureAlias);
            var client = new SearchClient(!string.IsNullOrEmpty(serviceUrl) ? serviceUrl : "https://www.googleapis.com",
                !string.IsNullOrEmpty(lookupMethod) ? lookupMethod : "customsearch/v1");
            var searchRequest = new Request
            {
                SearchTerms = string.Format("{0}{1}", URLHelper.URLDecode(keyword), !string.IsNullOrEmpty(excludeFileTypes) ? string.Format(" {0}", excludeFileTypes) : string.Empty),
                Cx = cx,
                ApiKey = apiKey,
                StartIndex = currentPage == 0 ? 1 : ((currentPage * _pageSize) + 1),
                Hl = LocalizationContext.CurrentCulture.CultureAlias,
                Language = language,
                FileType = fileType,
                SiteSearch = selectedSite,
                Filter = "0"
            };

            // Convert the object into a dictionary of parameters and pass this to the Google Site Search library
            var parameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(searchRequest, Formatting.Indented));
            parameters = parameters.Where(item => !string.IsNullOrEmpty(item.Value) && (item.Value != "0" || item.Key.Equals("filter", StringComparison.OrdinalIgnoreCase))).ToDictionary(i => i.Key, i => i.Value);

            // Get the results from the Google Site Search library
            var googleResults = client.GetResults(parameters);

            // Log the request and response from Google in a custom table
            //if (googleResults != null)
            //{
            //    var queryString = parameters.Aggregate("", (current, param) => current + string.Format("&{0}={1}", param.Key, param.Value)).TrimStart(new[] { '&' });

            //    var log = new GoogleSiteSearchLog
            //    {
            //        Request = string.Format("{0}/{1}?{2}", serviceUrl, lookupMethod, queryString),
            //        Response = JsonConvert.SerializeObject(googleResults)
            //    };

            //    log.WriteLogEntry();
            //}

            return googleResults;
        }
    }
}