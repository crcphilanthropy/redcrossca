﻿using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSAppAppCode.Old_App_Code.CRC.Helpers
{
    public static class SectionHelper
    {
        public enum SiteSection { Blog, History, Main, IYC}

        public static SiteSection GetCurrentSection()
        {
            var paths = RequestContext.CurrentURL.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

            var siteSection = string.Empty;
            switch (paths.FirstOrDefault())
            {
                case "blog":
                case "blogue":
                    return SiteSection.Blog;

                case "history":
                case "histoire":
                    return SiteSection.History;
                case "in-your-community":
                case "dans-votre-collectivite":
                    return SiteSection.IYC;
            }
            return SiteSection.Main;
        }
    }
}