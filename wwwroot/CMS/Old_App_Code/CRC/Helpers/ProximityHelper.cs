﻿using System;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Helpers
{
    public class ProximityHelper
    {
        /// <summary>
        /// Calculate distance between two geo points
        /// </summary>
        /// <param name="item">Item location (eg. branch location)</param>
        /// <param name="lat">Latitude of pivot point</param>
        /// <param name="lng">Longitude of pivot point</param>
        /// <returns></returns>
        public static double CalculateDistince(ICoordinates item, decimal lat, decimal lng)
        {
            return (6378.1 * Math.Acos(Math.Sin(Convert.ToDouble(item.Latitude) / 57.29577951) * Math.Sin(Convert.ToDouble(lat) / 57.29577951)
                       +
                       Math.Cos(Convert.ToDouble(item.Latitude) / 57.29577951) * Math.Cos(Convert.ToDouble(lat) / 57.29577951) *
                       Math.Cos(Convert.ToDouble((item.Longitude - lng)) / 57.29577951)));
        }
    }
}