using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CMS.CMSHelper;
using CMS.DocumentEngine;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.Taxonomy;
using CMS.Helpers;
using CMS.Localization;
using CMSAppAppCode.Old_App_Code.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Helpers;

namespace CMSApp.CRC
{
    public static class TreeNodeExtensions
    {
        public static CategoryInfo CurrentCategory(this TreeNode node)
        {
            var currentCategory = HttpContext.Current.Items["CurrentCategory"] as CategoryInfo;
            if (currentCategory != null) return currentCategory;
            var categoryId = QueryHelper.GetInteger("categoryid", -1);
            HttpContext.Current.Items["CurrentCategory"] = currentCategory = categoryId > 0 ? CategoryInfoProvider.GetCategoryInfo(categoryId) : CategoryInfoProvider.GetCategoryInfo(GetCategoryCode(), SiteContext.CurrentSiteName);
            return currentCategory;
        }

        public static int ProvinceCategory(this TreeNode node)
        {
            return CacheHelper.Cache(() =>
                {
                    var data = CategoryInfoProvider.GetDocumentCategories(
                        node.DocumentID,
                        where:
                            "CMS_DocumentCategory.CategoryID in (select c.CategoryID from CMS_Category c where c.CategoryLevel = 2 AND c.CategoryNamePath like '/{$CRC.Regions$}/%')",
                        topN: 1,
                        columns: "CMS_DocumentCategory.CategoryID");
                    return DataHelper.DataSourceIsEmpty(data) ? 0 : ValidationHelper.GetInteger(data.Tables[0].Rows[0]["CategoryID"], 0);
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("ProvinceCategory_{0}_{1}", LocalizationContext.PreferredCultureCode, node.DocumentID)));
        }

        public static string LocalizedDescription(this CategoryInfo categoryInfo)
        {
            var description = HttpContext.Current.Items["CategoryLocalizedDescription"] as String;

            if (!string.IsNullOrWhiteSpace(description)) return description;

            if (categoryInfo == null) return string.Empty;

            description = ResHelper.LocalizeString(ValidationHelper.GetString(CustomTableHelper.GetItemValue("CRC.CategoryUrls", string.Format("CategoryName = '{0}'", categoryInfo.CategoryName), "Description"), string.Empty));
            if (string.IsNullOrWhiteSpace(description)) description = categoryInfo.CategoryDescription;
            HttpContext.Current.Items["CategoryLocalizedDescription"] = description;

            if (!string.IsNullOrWhiteSpace(description))
                DocumentContext.CurrentDescription = HttpContext.Current.Server.HtmlEncode(HTMLHelper.StripTags(description).Replace("\"", ""));

            return description;
        }

        public static string LocalizedDisplayName(this CategoryInfo categoryInfo)
        {
            return categoryInfo == null ? string.Empty : ResHelper.LocalizeString(categoryInfo.CategoryDisplayName);
        }

        private static string GetCategoryCode()
        {
            if (!string.IsNullOrWhiteSpace(QueryHelper.GetString("category", string.Empty)))
            {
                var category = QueryHelper.GetString("category", string.Empty);
                var code = ValidationHelper.GetString(CustomTableHelper.GetItemValue("CRC.CategoryUrls", string.Format("{0} = '{1}'", LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? "CategoryUrl_Fr" : "CategoryUrl", category), "CategoryName"), string.Empty);
                if (string.IsNullOrEmpty(code) && SectionHelper.GetCurrentSection() == SectionHelper.SiteSection.Main) throw new HttpException(404, "Not Found");
                if (!string.IsNullOrEmpty(code)) return code;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Inherited Custom field value from document type
        /// </summary>
        /// <param name="node">Starting TreeNode</param>
        /// <param name="fieldName">Field Name</param>
        /// <returns>returns -1[inherited] / 0[false] / 1[true]</returns>
        public static int GetInheritedFieldValue(this TreeNode node, string fieldName)
        {
            if (node == null)
                return 1;

            var toReturn = node.GetIntegerValue(fieldName, -1);
            return toReturn == -1 ? node.Parent.GetInheritedFieldValue(fieldName) : toReturn;
        }

        /// <summary>
        /// Create object of specified type (T) from TreeNode
        /// </summary>
        /// <typeparam name="T">Type to convert to, must have empty constructor</typeparam>
        /// <param name="node">TreeNode</param>
        /// <returns>T object</returns>
        public static T ConvertTo<T>(this TreeNode node) where T : class, new()
        {
            if (node == null) return default(T);

            var item = new T();
            var properties = typeof(T).GetProperties();
            foreach (var propertyInfo in properties.Where(propertyInfo => node.GetValue(propertyInfo.Name) != null))
            {
                propertyInfo.SetValue(item,
                                      propertyInfo.PropertyType == typeof(Decimal)
                                          ? ValidationHelper.GetDecimal(node.GetValue(propertyInfo.Name), 0)
                                          : node.GetValue(propertyInfo.Name), null);
            }

            return item;
        }

        /// <summary>
        /// Create object of specified type (T) from DataRow
        /// </summary>
        /// <typeparam name="T">Type to convert to, must have empty constructor</typeparam>
        /// <param name="row">DataRow</param>
        /// <returns>T object</returns>
        public static T ConvertTo<T>(this DataRow row) where T : class, new()
        {
            if (row == null) return default(T);

            var item = new T();
            var properties = typeof(T).GetProperties();
            foreach (var propertyInfo in properties.Where(propertyInfo => row.Table.Columns.Contains(propertyInfo.Name) && row[propertyInfo.Name] != System.DBNull.Value))
            {
                try
                {
                    propertyInfo.SetValue(item, row[propertyInfo.Name], null);
                }
                catch (Exception ex)
                {
                    throw new Exception(propertyInfo.Name);
                }
            }

            return item;
        }

        /// <summary>
        /// Convert TreeNodeDataSet to IEnumerable of T
        /// </summary>
        /// <typeparam name="T">Type to convert each item to, must have empty constructor</typeparam>
        /// <param name="data">TreeNodeDataSet</param>
        /// <returns>IEnumerable of T</returns>
        public static IEnumerable<T> ConvertTo<T>(this TreeNodeDataSet data) where T : class, new()
        {
            return ((DataSet)data).ConvertTo<T>();
        }

        /// <summary>
        /// Convert DataSet to IEnumerable of T
        /// </summary>
        /// <typeparam name="T">Type to convert each item to, must have empty constructor</typeparam>
        /// <param name="data">DataSet</param>
        /// <returns>IEnumerable of T</returns>
        public static IEnumerable<T> ConvertTo<T>(this DataSet data) where T : class, new()
        {
            if (DataHelper.DataSourceIsEmpty(data))
                return new List<T>();

            var toReturn = new ConcurrentBag<T>();
            Parallel.ForEach(data.Tables[0].Rows.Cast<DataRow>(), row =>
            {
                if (row.ConvertTo<T>() != null)
                    toReturn.Add(row.ConvertTo<T>());
            });

            return toReturn;
        }
    }
}