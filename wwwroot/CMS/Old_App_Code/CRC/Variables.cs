﻿using System;
using System.Collections.Generic;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CMS.WebAnalytics;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CultureInfo = System.Globalization.CultureInfo;
using System.Linq;

namespace CMSAppAppCode.Old_App_Code.CRC
{
    public static class Variables
    {
        internal const string ProximityFormula = "(6378.1 * ACOS(SIN(Latitude / 57.29577951) * SIN({0} / 57.29577951) + COS(Latitude / 57.29577951) * COS({0} / 57.29577951) * COS((Longitude - {1}) / 57.29577951)))";

        /// <summary>
        /// {0} Latitude
        /// {1} Longitude
        /// {2} Width
        /// {3} Height
        /// </summary>
        public static string StaticGoogleMap =
            "https://maps.googleapis.com/maps/api/staticmap?center={0},{1}&markers=color:red%7C{0},{1}&zoom=16&size={2}x{3}";

        private static CultureInfo _englishCultureInfo;
        internal static CultureInfo EnglishCultureInfo
        {
            get
            {
                return _englishCultureInfo ?? (_englishCultureInfo = CultureInfo.GetCultureInfo("en-CA"));
            }
        }

        /// <summary>
        /// Maximum distance for proximity search, get value from kentico custom settings
        /// </summary>
        public static int MaxDistance
        {
            get
            {
                var toReturn = SettingsKeyInfoProvider.GetIntValue(string.Format("{0}.MaxDistance", SiteContext.CurrentSiteName));
                return toReturn < 0 ? 0 : toReturn;
            }
        }

        /// <summary>
        /// Number of servies to return in search, get value from kentico custom settings
        /// </summary>
        public static int TopNServices
        {
            get
            {
                var toReturn = SettingsKeyInfoProvider.GetIntValue(string.Format("{0}.TopNServices", SiteContext.CurrentSiteName));
                return toReturn == 0 ? 50 : toReturn;
            }
        }

        public static string GlobalTweetId
        {
            get
            {
                return LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) ? SettingsKeyInfoProvider.GetValue(string.Format("{0}.GlobalTweetId_FR", SiteContext.CurrentSiteName)) : SettingsKeyInfoProvider.GetValue(string.Format("{0}.GlobalTweetId", SiteContext.CurrentSiteName));
            }
        }

        public static string GoogleMapsAPIUrl
        {
            get
            {
                //https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true
                return SettingsKeyInfoProvider.GetValue(string.Format("{0}.GoogleMapsAPIUrl", SiteContext.CurrentSiteName));
            }
        }

        public static string RegionCode
        {
            get
            {
                try
                {
                    return GetRegionCode();
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogException("Variales", "RegionCode", ex);
                    return "NA";
                }
            }
            set { SessionHelper.SetValue("regionCode", value); }
        }

        public static int ShowPromotionNTimes
        {
            get
            {
                return SettingsKeyInfoProvider.GetIntValue(string.Format("{0}.ShowPromotionNTimes", SiteContext.CurrentSiteName));
            }
        }

        public static int HidePromotionNDays
        {
            get
            {
                return SettingsKeyInfoProvider.GetIntValue(string.Format("{0}.HidePromotionNDays", SiteContext.CurrentSiteName));
            }
        }

        private static string GetRegionCode()
        {
            var ipAddress = GetIpAddress();
            var logIp = (System.Web.HttpContext.Current.Request.QueryString["LogIP"] ?? String.Empty).Equals("true", StringComparison.OrdinalIgnoreCase);
           

            var regionCode = SessionHelper.GetValue("regionCode") as string;
            if (!string.IsNullOrWhiteSpace(regionCode))
            {
                if (logIp)
                {
                    EventLogProvider.LogInformation("Variables.GetRegionCode", "Ip Address", ipAddress);
                    EventLogProvider.LogInformation("Variables.GetRegionCode", "Region Code", regionCode);
                }
                return regionCode;
            }

            var location = GeoIPHelper.GetLocationByIp(ipAddress);
            regionCode = location == null ? string.Empty : location.RegionCode;

            var provinces = new ProvinceProvider().GetAllProvinces();
            var geoProvince = provinces.FirstOrDefault(item => item.ProvinceCode.Equals(regionCode, StringComparison.OrdinalIgnoreCase) || (regionCode == "PE" && item.ProvinceCode == "PEI") || (regionCode == "YT" && item.ProvinceCode == "BC"));

            if (logIp)
            {
                EventLogProvider.LogInformation("Variables.GetRegionCode", "Ip Address", ipAddress);
                EventLogProvider.LogInformation("Variables.GetRegionCode", "Region Code", regionCode);
            }

            regionCode = geoProvince == null || string.IsNullOrWhiteSpace(regionCode) ? "NA" : geoProvince.ProvinceCode;

            SessionHelper.SetValue("regionCode", regionCode);

            return regionCode;
        }

        private static string GetIpAddress()
        {
            var context = System.Web.HttpContext.Current;

            if (!string.IsNullOrWhiteSpace(context.Request.Headers["client-ip"]))
                return context.Request.Headers["client-ip"];

            var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                var addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static int CacheTimeInMinutes
        {
            get
            {
                return SettingsKeyInfoProvider.GetIntValue(string.Format("{0}.CacheTimeInMinutes", SiteContext.CurrentSiteName));
            }
        }

        public static bool EnableCustomCaching
        {
            get
            {
                return SettingsKeyInfoProvider.GetBoolValue(string.Format("{0}.EnableCache", SiteContext.CurrentSiteName));
            }
        }

    }
}