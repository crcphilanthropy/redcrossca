﻿using System;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    public class ErrorRedirectItem : IErrorRedirectItem
    {
        public Guid ItemGuid { get; set; }
        public string Path { get; set; }
        public string RedirectTo { get; set; }
        public int StatusCode { get; set; }
        public int Order { get; set; }
        public string Language { get; set; }
        public bool IsExactMatch { get { return false; } }
        public bool IsTransfer { get; set; }
    }
}