﻿using System;
using System.Runtime.Serialization;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using System.Linq;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    [KnownType(typeof(Service))]
    [KnownType(typeof(Branch))]
    [DataContract]
    public class Service : PageMenu, IService
    {
        private IBranch _branch;
        private Guid _branchDocumentGuid;

        [DataMember]
        public Guid BranchDocumentGuid
        {
            get { return _branchDocumentGuid; }
            set
            {
                if (_branchDocumentGuid != value)
                    _branch = null;

                _branchDocumentGuid = value;
            }
        }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string HoursOfOperation { get; set; }

        [DataMember]
        public string Contact { get; set; }

        [DataMember]
        public double Distance { get; set; }

        [DataMember]
        public Guid CategoryDocumentGuid { get; set; }

        [DataMember]
        public Guid SubCategoryDocumentGuid { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public string CategoryUrl { get; set; }

        [DataMember]
        public string CategoryIconPath { get; set; }

        [DataMember]
        public string CategoryIconAltText { get; set; }

        [DataMember]
        public string SubCategoryName { get; set; }

        [DataMember]
        public string SubCategoryUrl { get; set; }

        [DataMember]
        public string SubCategoryIconPath { get; set; }

        [DataMember]
        public string SubCategoryIconAltText { get; set; }

        public IBranch Branch
        {
            get
            {
                return _branch ?? (_branch = new BranchProvider().GetByGuid(_branchDocumentGuid));
            }
        }

        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }

        public int ServiceRowNumber { get; set; }
    }
}