﻿using System;
using System.Runtime.Serialization;
using CMS;
using CMS.DocumentEngine;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    [DataContract]
    [KnownType(typeof(PageMenu))]
    public class PageMenu : IPageMenu
    {
        [DataMember]
        public Guid DocumentGuid { get; set; }

        [DataMember]
        public int NodeID { get; set; }

        [DataMember]
        public int NodeParentID { get; set; }

        [DataMember]
        public int NodeOrder { get; set; }

        [DataMember]
        public string DocumentName { get; set; }

        [DataMember]
        public string NodeAliasPath { get; set; }

        [DataMember]
        public string DocumentUrlPath { get; set; }

        private string _documentLiveUrl;
        public string DocumentLiveUrl
        {
            get
            {
                return _documentLiveUrl ??
                       (_documentLiveUrl =
                        DocumentURLProvider.GetUrl(NodeAliasPath, DocumentUrlPath).Trim(new[] { '~' }).ToLower());
            }
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Summary { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Guid Thumbnail { get; set; }

        [DataMember]
        public string ThumbnailAltText { get; set; }

        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        public string ProvinceCode { get; set; }
        public int BranchRowNumber { get; set; }
        public int CategoryRowNumber { get; set; }
        public int SubCategoryRowNumber { get; set; }
    }
}