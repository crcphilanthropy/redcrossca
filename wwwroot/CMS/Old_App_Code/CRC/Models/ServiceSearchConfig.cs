﻿using System;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    public class ServiceSearchConfig : IServiceSearchConfig
    {
        public Guid CategoryDocumentGuid { get; set; }
        public Guid SubCategoryDocumentGuid { get; set; }
        public Decimal Latitude { get; set; }
        public Decimal Longitude { get; set; }
        public int MaxDistance { get; set; }

        /// <summary>
        /// Check if values are empty/default except for MaxDistance
        /// </summary>
        /// <returns>bool</returns>
        public bool IsEmpty()
        {
            return CategoryDocumentGuid == Guid.Empty
                && SubCategoryDocumentGuid == Guid.Empty
                && Latitude == 0
                && Longitude == 0;
        }
    }
}