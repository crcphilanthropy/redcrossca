﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    [KnownType(typeof(Branch))]
    [KnownType(typeof(Service))]
    [DataContract]
    public class Branch : PageMenu, IBranch
    {
        private IEnumerable<IService> _services;
        private IEnumerable<IPageMenu> _categories;

        [DataMember]
        public string BranchAddress { get; set; }

        [DataMember]
        public string BranchHours { get; set; }

        [DataMember]
        public string BranchContact { get; set; }

        [DataMember]
        public Decimal Longitude { get; set; }

        [DataMember]
        public Decimal Latitude { get; set; }

        [DataMember]
        public double Distance { get; set; }

        [DataMember]
        public IEnumerable<IService> Services
        {
            get
            {
                return _services ?? (_services = UseCategories ? new List<IService>() : new ServiceProvider().GetAllByBranch(DocumentGuid));
            }
        }

        public IEnumerable<IPageMenu> GetCategories(string path = null, string className = null)
        {
            var serviceCategoryProvider = new ServiceCategoryProvider();
            if (String.IsNullOrWhiteSpace(path) && serviceCategoryProvider.ProvinceNode != null) path = string.Format("{0}/%", serviceCategoryProvider.ProvinceNode.NodeAliasPath);
            return _categories ?? (_categories = !UseCategories ? new List<IPageMenu>() : serviceCategoryProvider.GetCategories(path, className));
        }

        public bool UseCategories { get; set; }
    }
}