﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    public class PromotionalCookie
    {
        public string CTAPath { get; set; }
        public DateTime DateTime { get; set; }
        public int CloseCounter { get; set; }
    }
}