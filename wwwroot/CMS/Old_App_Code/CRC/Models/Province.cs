﻿using System.Runtime.Serialization;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    [KnownType(typeof(Province))]
    [DataContract]
    public class Province : PageMenu, IProvince
    {
        [DataMember]
        public new string ProvinceCode { get; set; }

        public string IconPath { get; set; }

        private string _provinceDisplayCode;
        public string ProvinceDisplayCode
        {
            get { return _provinceDisplayCode ?? (_provinceDisplayCode = ProvinceCode); }
            set { _provinceDisplayCode = value; }
        }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        public double Distance { get; set; }

        private string _group;
        public string Group
        {
            get { return _group ?? "drop-down-nav"; }
            set { _group = value; }
        }


        public string FindServicesResultPage { get; set; }
    }
}