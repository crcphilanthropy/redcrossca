﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Models
{
    public class ArchivePageRedirectItem : IErrorRedirectItem
    {
        public Guid ItemGuid { get; set; }
        public string Path { get; set; }
        public string RedirectTo { get; set; }
        public int StatusCode { get; set; }
        public int Order { get; set; }
        public string Language { get; set; }
        public bool IsExactMatch { get { return true; } }
        public bool IsTransfer { get; set; }
    }
}