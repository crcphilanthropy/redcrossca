using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using CMS.DocumentEngine;
using CMS.PortalEngine;
using CMS.SiteProvider;
using CMSAppAppCode.Old_App_Code.CRC;
using CultureInfo = System.Globalization.CultureInfo;
using CMS.Helpers;
using CMS.CustomTables;
using CMS.Localization;
using CMS.Taxonomy;
using CMS.DataEngine;
using CMS.Membership;

namespace CMSApp.CRC
{
    public static class TransformationHelper
    {
        public static string GetDocumentUrl(object documentUrlPath, object nodeAliasPath, string className = null, string documentMenuRedirectUrl = null)
        {
            if (!string.IsNullOrWhiteSpace(documentMenuRedirectUrl)) return documentMenuRedirectUrl;

            var url = DocumentURLProvider.GetUrl(nodeAliasPath as string, documentUrlPath as string);
            if (!string.IsNullOrWhiteSpace(className) &&
                className.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Contains("iyc-geo-url", new StringEqualityComparer()))
                return GetIYCUrl(url);

            return url;
        }

        public static string GetCustomTableItemDisplayNames(object items, string className, string columnName = "DisplayName", string separator = "|")
        {
            var values = (items as string ?? string.Empty).Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries).ToList();

            var toReturn = values.Select(item => GetCustomTableItemDisplayName(item, className, columnName)).Where(item => !string.IsNullOrWhiteSpace(item));
            return toReturn.Join(", ");
        }

        public static string GetCustomTableItemDisplayName(object itemGuid, string className, string columnName = "DisplayName")
        {
            var displayName = string.Empty;
            CustomTableItem item = null;

            var guid = ValidationHelper.GetGuid(itemGuid, Guid.Empty);
            if (guid != Guid.Empty)
                item = CustomTableItemProvider.GetItem(guid, className);

            if (item != null)
                displayName = item.GetStringValue(columnName, string.Empty);

            return displayName;
        }

        public static string DateFormat
        {
            get
            {
                switch (LocalizationContext.PreferredCultureCode.ToUpper())
                {
                    case "FR-CA":
                        return "dd MMMM yyyy";
                }

                return "MMMM dd, yyyy";
            }
        }


        public static string GetRssDateTime(object dt)
        {
            var timeStamp = ValidationHelper.GetDateTime(dt, DateTime.MinValue);
            return timeStamp.ToString("F", new System.Globalization.CultureInfo(LocalizationContext.PreferredCultureCode));
        }

        public static string CurrentProgramName
        {
            get
            {
                var toReturn = new TreeProvider().SelectSingleNode(SiteContext.CurrentSiteName, string.Format("/where-we-work/programs-services/{0}", HttpContext.Current.Request.QueryString["program"]), LocalizationContext.PreferredCultureCode);
                return toReturn == null ? ValidationHelper.GetString(DocumentContext.CurrentDocument.GetProperty("Title"), "Program Name") : ValidationHelper.GetString(toReturn.GetProperty("Title"), string.Empty);
            }
        }

        public static string GetSearchFileType(string itemUrl)
        {
            var regEx = new Regex(@"\.(PDF|DOC|XLS|PPT)", RegexOptions.IgnoreCase);
            var m = regEx.Match(itemUrl);
            var toReturn = string.Empty;
            if (m.Groups.Count > 1 && !string.IsNullOrWhiteSpace(m.Groups[1].Value))
            {
                var ext = m.Groups[1].Value.ToUpper();
                switch (ext.ToUpper())
                {
                    case "DOC":
                    case "XLS":
                        toReturn = ResHelper.GetString("CRC.WordOrExcel");
                        break;
                    case "PPT":
                        toReturn = ResHelper.GetString("CRC.PowerPoint");
                        break;
                    default:
                        toReturn = ResHelper.GetString(string.Format("CRC.{0}", ext));
                        break;
                }
            }

            return string.IsNullOrWhiteSpace(toReturn) ? string.Empty : string.Format("<span class='file-type'>[{0}]</span>", toReturn);
        }

        public static string GetGeoLocationTranslation(string locationName, string locationType)
        {
            var resKey = string.Format("CRC.{0}.{1}", locationType, (locationName ?? string.Empty).Replace(" ", string.Empty));
            var toReturn = ResHelper.GetString(resKey);
            return toReturn.Equals(resKey) ? locationName : toReturn;
        }

        public static string FormatCategoryDisplayName(string categoryDisplayName, string cultureCode, bool useHtmlEncode = true)
        {
            // Localize category display name
            categoryDisplayName = ResHelper.LocalizeString(categoryDisplayName, cultureCode);

            // Encode category display name
            if (useHtmlEncode)
                categoryDisplayName = HTMLHelper.HTMLEncode(categoryDisplayName);

            return categoryDisplayName;
        }

        public static string GetFormattedNavPath(object navPath)
        {
            var newstring = navPath.ToString();
            return newstring.TrimStart('/').Replace("-", " ").Replace("/", " > ");
        }

        public static T GetProperty<T>(DataRow row, string property)
        {
            if (row == null)
                return default(T);

            return ValidationHelper.GetValue<T>(row[property]);
        }

        public static string GetNewsLocation(int documentID)
        {
            var categories = CategoryInfoProvider.GetDocumentCategories(documentID, "CategoryLevel in (2,3) and CategoryNamePath Like '/{$CRC.Regions$}%'", "CategoryLevel Desc");
            if (DataHelper.IsEmpty(categories) || categories == null || categories.Items.Count == 0)
                return string.Empty;

            var category = categories.Cast<CategoryInfo>().First();
            if (category.CategoryLevel == 3)
            {
                return string.Format("{0}, {1} - ", ResHelper.LocalizeString(category.CategoryDisplayName),
                                     ResHelper.LocalizeString(CategoryInfoProvider.GetCategoryInfo(category.CategoryParentID).CategoryDisplayName));
            }

            return string.Format("{0} - ", ResHelper.LocalizeString(category.CategoryDisplayName));
        }

        public static string GetNewsTopics(int documentID)
        {
            const string columns = "*, (select cu.CategoryUrl from CRC_CategoryUrls cu where cu.CategoryName = CMS_Category.CategoryName) as CategoryUrl_EN,(select cu.CategoryUrl_FR from CRC_CategoryUrls cu where cu.CategoryName = CMS_Category.CategoryName) as CategoryUrl_FR";
            var categories = CategoryInfoProvider.GetDocumentCategories(documentID, null, "CategoryNamePath ASC, CategoryDisplayName ASC", columns: columns);
            if (DataHelper.IsEmpty(categories) || categories == null || categories.Items.Count == 0)
                return string.Empty;

            var url = getCultureDocumentUrl(TreePathUtils.GetDocumentUrl(DocumentContext.CurrentDocument.Parent.DocumentID).TrimStart(new char[] { '~' }));
            var topics = categories.Tables[0].Rows.Cast<DataRow>().Select(item =>
                {
                    var customUrl = ValidationHelper.GetString(item[string.Format("CategoryUrl_{0}", LocalizationContext.PreferredCultureCode.Substring(0, 2))], String.Empty);
                    if (!string.IsNullOrWhiteSpace(customUrl))
                    {
                        var topicUrl = string.Format("{0}/{1}/{2}", url, item["CategoryNamePath"].ToString().ToLower().Contains("{$crc.regions$}") ? "region" : "topic", customUrl).ToLower();
                        return string.Format("<a href='{0}'>{1}</a>", topicUrl, ResHelper.LocalizeString(item["CategoryDisplayName"].ToString()));
                    }

                    return string.Format("<a href='{0}?categoryid={1}'>{2}</a>", url, item["CategoryID"], ResHelper.LocalizeString(item["CategoryDisplayName"].ToString()));
                }).ToList();
            return topics.Any() ? String.Format("{0}: {1}<br />", ResHelper.GetString("CRC.Topics"), topics.Join(", ")) : string.Empty;
        }

        public static string GetRegionPath(string cultureCode)
        {
            return SettingsKeyInfoProvider.GetStringValue(string.Format("CRC_Settings_RegionPath_{0}",
                                                                  cultureCode.Replace("-", "_")));
        }

        public static string GetDomainAliasByCultureCode(int siteID, string cultureCode)
        {
            var domainAlias = SiteContext.CurrentSite.DomainName;
            if (!DocumentContext.CurrentDocumentCulture.CultureCode.Equals(cultureCode, StringComparison.OrdinalIgnoreCase))
            {
                var queryParam = new QueryDataParameters
                    {
                        {"@SiteID", siteID},
                        {"@SiteDefaultVisitorCulture", cultureCode}
                    };
                var ds = ConnectionHelper.ExecuteQuery("CRC.CustomTransformations.GetDomainAliasByCultureCode", queryParam, null, null);
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    var returnedDocument = ds.Tables[0].Rows[0];
                    domainAlias = ValidationHelper.GetString(returnedDocument["SiteDomainAliasName"], domainAlias);
                }
            }
            return domainAlias;
        }


        public static DataRow getDocumentByGUID(string guid)
        {
            DataRow returnedDocument = null;

            QueryDataParameters queryParam = new QueryDataParameters();
            queryParam.Add("@NodeGUID", ValidationHelper.GetGuid(guid, Guid.Empty));

            DataSet ds = ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByGUID", queryParam, null, null);
            if (!CMS.Helpers.DataHelper.DataSourceIsEmpty(ds))
            {
                returnedDocument = ds.Tables[0].Rows[0];
            }

            return returnedDocument;
        }
        public static string GetSectionAlias()
        {
            string sectionName = "";
            string currentAlias = CMS.DocumentEngine.DocumentContext.CurrentAliasPath;
            if (currentAlias == "/")
            {
                sectionName = "home";
            }
            else
            {
                string[] aliasPaths = currentAlias.Split('/');
                sectionName = aliasPaths[1].ToLower();
            }
            return sectionName;
        }
        public static string GetParentAlias(string alias)
        {
            string sectionName = "";
            string currentAlias = alias;
            if (currentAlias == "/")
            {
                sectionName = "home";
            }
            else
            {
                string[] aliasPaths = currentAlias.Split('/');
                sectionName = aliasPaths[aliasPaths.Length - 2].ToLower();
            }
            return sectionName;
        }
        public static string GetUserAvatar(object userId)
        {
            var id = ValidationHelper.GetInteger(userId, 0);

            var user = UserInfoProvider.GetUserInfo(id);

            return GetUserAvatar(user);
        }

        public static string GetUserAvatar(UserInfo user)
        {
            string result = "";
            if (user.UserAvatarID != 0)
            {
                var avinf = AvatarInfoProvider.GetAvatarInfo(user.UserAvatarID);
                result = URLHelper.ResolveUrl("~/CMSModules/Avatars/CMSPages/GetAvatar.aspx") + "?avatarguid=" + avinf.AvatarGUID.ToString();
            }

            return result;
        }

        public static string targetBlankHTML(object openNewWin)
        {
            string html = "";
            if (ValidationHelper.GetBoolean(openNewWin, false))
            {
                html = "target=\"_blank\"";
            }
            return html;
        }
        public static string GetSectionName()
        {
            string result = "";
            string path = CMS.DocumentEngine.DocumentContext.CurrentDocument.DocumentNamePath.ToString();
            char[] splitchar = { '/' };
            string[] ary = path.Split(splitchar);
            if (ary.Length > 1)
            {
                var tree = new TreeProvider(CMS.Membership.MembershipContext.AuthenticatedUser);

                // Fill dataset with documents
                DataSet documents = null;
                try
                {
                    documents = tree.SelectNodes(CMS.SiteProvider.SiteContext.CurrentSiteName, "/" + GetSectionAlias(), CMS.DocumentEngine.DocumentContext.CurrentDocumentCulture.CultureCode, false, "CMS.Folder;CRC.Page;CMS.MenuItem;CMS.Blog");
                }
                catch (Exception e)
                {
                }
                if (!DataHelper.DataSourceIsEmpty(documents))
                {
                    string documentCaption = CMS.Helpers.ValidationHelper.GetString(documents.Tables[0].Rows[0]["DocumentMenuCaption"], "");
                    if (String.IsNullOrEmpty(documentCaption))
                    {
                        result = ary[1];
                    }
                    else
                    {
                        result = documentCaption;
                    }
                    //result = result.Replace(" ", "_");
                }

            }
            else
            {
                result = ary[ary.Length - 1];
                //result = result.Replace(" ", "_");
            }
            return result;
        }

        public static void RegisterStartUpScript(string script, Page page, string scriptName = "", bool includeScript = true)
        {
            if (string.IsNullOrWhiteSpace(scriptName))
                scriptName = Guid.NewGuid().ToString();

            if (page.ClientScript.IsStartupScriptRegistered(scriptName)) return;

            var sm = ScriptManager.GetCurrent(page);
            if (sm == null)
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), scriptName, script, includeScript);
            }
            else
            {
                ScriptManager.RegisterStartupScript(page, page.GetType(), scriptName, script, includeScript);
            }
        }

        public static string getDocumentNameByAliasPath(string path)
        {
            string documentName = "";
            TreeNode node = TreeHelper.GetDocument(CMS.SiteProvider.SiteContext.CurrentSiteName,
                                                    path,
                                                    CMS.DocumentEngine.DocumentContext.CurrentDocumentCulture.CultureCode,
                                                    false,
                                                    "CRC.Page", true);
            if (node != null)
            {
                documentName = node.DocumentName;
            }
            return documentName;
        }

        public static string ReplaceAccentCharacters(string text)
        {
            var toReturn = (text ?? string.Empty).ToLower();
            var dictionary = new Dictionary<string, string>
                {
                    { "&#192;", "a" },
                    { "&#194;", "a" },
                    { "&#196;", "a" },
                    { "&#200;", "e" },
                    { "&#201;", "e" },
                    { "&#202;", "e" },
                    { "&#203;", "e" },
                    { "&#206;", "i" },
                    { "&#207;", "i" },
                    { "&#212;", "o" },
                    { "&#140;", "oe" }, 
                    { "&#217;", "u" }, 
                    { "&#219;", "u" }, 
                    { "&#220;", "u" }, 
                    { "&#159;", "y" }, 
                    { "&#224;", "a" },
                    { "&#226;", "a" },
                    { "&#228;", "a" },
                    { "&#232;", "e" },
                    { "&#233;", "e" },
                    { "&#234;", "e" },
                    { "&#238;", "i" },
                    { "&#239;", "i" },
                    { "&#244;", "o" },
                    { "&#156;", "oe" },
                    { "&#250;", "u" },
                    { "&#251;", "u" },
                    { "&#252;", "u" },
                    { "&#255;", "y" }
                };
            char[] replacement = { 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', };
            char[] accents = { 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'é', 'è', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ö', 'õ', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ' };

            for (var i = 0; i < accents.Length; i++)
            {
                toReturn = toReturn.Replace(accents[i], replacement[i]);
            }

            dictionary.ToList().ForEach(item => { toReturn = toReturn.Replace(item.Key, item.Value); });

            return toReturn;
        }
        public static string getCultureDocumentUrl(string path)
        {
            return CacheHelper.Cache(() =>
                 {
                     string cultureUrl = path;
                     var documents = TreeHelper.GetDocuments(SiteContext.CurrentSiteName,
                     "/%",
                     DocumentContext.CurrentDocumentCulture.CultureCode,
                     false,
                     "",
                     string.Format("NodeAliasPath = '{0}' OR DocumentUrlPath = '{0}'", path.Replace("~", string.Empty)),
                     null,
                     -1,
                     true, 1, "NodeAliasPath,DocumentUrlPath");

                     if (!DataHelper.DataSourceIsEmpty(documents))
                         cultureUrl = ValidationHelper.GetString(documents.Tables[0].Rows[0]["DocumentUrlPath"], ValidationHelper.GetString(documents.Tables[0].Rows[0]["NodeAliasPath"], string.Empty));

                     return string.IsNullOrWhiteSpace(cultureUrl) ? path : cultureUrl;
                 }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("cultureDocumentUrl_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, path)));

        }
        public static string getCultureDocumentUrl(object path)
        {

            string convertedString = ValidationHelper.GetString(path, "");
            return getCultureDocumentUrl(convertedString);

        }

        private static readonly Dictionary<string, string> FilterTypes = new Dictionary<string, string> { { "tag", "tag" }, { "author", "auteur" }, { "category", "categorie" } };
        public static string GetBlogFilterPath(string filterType, string param, TreeNode blogNode = null)
        {
            if (blogNode == null)
                blogNode = GetSubSiteNode();


            if (LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase) && FilterTypes.ContainsKey(filterType.ToLower()))
                filterType = FilterTypes[filterType];

            var decodedTag = HttpUtility.HtmlDecode(param);
            var replacedTag = decodedTag.Replace("\"", string.Empty).Replace(" ", "-")
                                                    .Replace('?', '-')
                                                    .Replace('\\', '-')
                                                    .Replace('/', '-')
                                                    .Replace('.', '-')
                                                    .Replace('%', '-')
                                                    .Replace('$', '-')
                                                    .Replace('#', '-')
                                                    .Replace('!', '-')
                                                    .Replace('@', '-')
                                                    .Replace('^', '-')
                                                    .Replace('*', '-').Replace('&', '-');

            var encodedUrl = HttpUtility.HtmlEncode(replacedTag);
            //var urlEncode = HttpContext.Current.Server.UrlEncode(param);
            //if (urlEncode != null)
            //    return blogNode != null ? string.Format("{0}/{1}/{2}", blogNode.DocumentUrlPath, filterType, urlEncode.ToLower()) : "#";

            return blogNode != null ? string.Format("{0}/{1}/{2}", blogNode.DocumentUrlPath, filterType, encodedUrl.ToLower()) : string.Format("{0}/{1}/{2}", LocalizationContext.PreferredCultureCode.Equals("fr-ca", StringComparison.OrdinalIgnoreCase) ? "/blogue" : "/blog", filterType, param.ToLower());
        }

        /// <summary>
        /// Returns the sub site root node.
        /// </summary>
        /// <param name="classNames">Page types of sub site node to search for. If no class is specified it will search for the blog node</param>
        /// <returns></returns>
        public static TreeNode GetSubSiteNode(List<string> classNames = null)
        {
            if(classNames == null)
                classNames = new List<string> { "cms.blog" };

            var currentNode = DocumentContext.CurrentDocument;

            while (currentNode != null && !classNames.Contains(currentNode.ClassName, new StringEqualityComparer()))
            {
                currentNode = currentNode.Parent;
            }
            return currentNode;
        }

        public static string GetBlogOwnerInfo(object dataItem)
        {
            var id = ValidationHelper.GetString(DataBinder.Eval(dataItem, "id"), string.Empty);
            if (ValidationHelper.GetString(DataBinder.Eval(dataItem, "type"), string.Empty).Equals("cms.document", StringComparison.OrdinalIgnoreCase) && id.Contains(";"))
            {

                var documentId = ValidationHelper.GetInteger(id.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(), 0);
                var node = new TreeProvider().SelectSingleDocument(documentId);
                if (node != null)
                {
                    return
                       string.Format(
                           "<p class='post-metadata'>{0} {1}, {2} <a href='{3}'>{4}</a>{5}</p>",
                           ResHelper.GetString("CRC.Blog.Posted"),
                           ValidationHelper.GetDateTime(DataBinder.Eval(dataItem, "Created"), DateTimeHelper.ZERO_TIME).ToString(DateFormat),
                           ResHelper.GetString("CRC.Blog.By"),
                           GetBlogFilterPath("author", node.Owner.UserName),
                           node.Owner.FullName,
                           string.IsNullOrWhiteSpace(node.Owner.UserSignature) ? string.Empty : string.Format(", {0}", node.Owner.UserSignature)
                           );
                }
            }

            return
                string.Format(
                    "<p class='post-metadata'>{0} {1}</p>",
                    ResHelper.GetString("CRC.Blog.Posted"),
                    ValidationHelper.GetDateTime(DataBinder.Eval(dataItem, "Created"), DateTimeHelper.ZERO_TIME).ToString(DateFormat)
                    );
        }

        public static string RegisterSearchText(object dataItem, Page page)
        {
            var dv = dataItem as DataRowView;
            if (dv != null)
                RegisterStartUpScript(string.Format("$('#search-query-text').html('Your search for {0} returned {1} results.')", QueryHelper.GetString("searchtext", string.Empty), dv.DataView.ToTable().Rows.Count), page, "search-query-text");

            var dr = dataItem as DataRow;
            if (dr != null)
                RegisterStartUpScript(string.Format("$('#search-query-text').html('Your search for {0} returned {1} results.')", QueryHelper.GetString("searchtext", string.Empty), dr.Table.Rows.Count), page, "search-query-text");

            return string.Empty;
        }

        public static string GetImageCaption(Guid imageGuid)
        {
            var image = AttachmentInfoProvider.GetAttachmentInfo(imageGuid, SiteContext.CurrentSiteName);
            if (image == null || string.IsNullOrWhiteSpace(image.AttachmentDescription)) return string.Empty;

            return
                string.Format(
                    "<span class='photo-credit'>{0}</span>",
                    image.AttachmentDescription);
        }

        public static string GetAlternateLanguageDomain()
        {
            var currentDomain = HttpContext.Current.Request.Url.Host.ToLower();//.Replace("www.", string.Empty);
            var desiredLanguage = LocalizationContext.PreferredCultureCode.Equals("en-CA", StringComparison.OrdinalIgnoreCase)
                                      ? "fr-CA"
                                      : "en-CA";

            var items = CustomTableItemProvider.GetItems("CRC.DomainAliasGroups",
                string.Format("GroupName = (select top 1 dag.GroupName from CRC_DomainAliasGroups dag where dag.DomainName = '{0}') and Language = '{1}'", currentDomain, desiredLanguage),
                "ItemOrder ASC",
                1);

            return !DataHelper.DataSourceIsEmpty(items) ? ValidationHelper.GetString(items.Tables[0].Rows[0]["DomainName"], currentDomain) : currentDomain;
        }

        public static double ConvertDonationValueToDouble(string amount)
        {
            var toReturn = 0d;

            amount = (amount ?? string.Empty).Replace(",", ".").Replace("$", string.Empty).Trim();

            double.TryParse(amount, NumberStyles.Any, CultureInfo.InvariantCulture, out toReturn);
            return toReturn;
        }

        public static string GetIYCUrl(String defaultUrl)
        {
            if (string.IsNullOrWhiteSpace(Variables.RegionCode) || Variables.RegionCode.Equals("NA"))
                return defaultUrl;

            var cacheManager = new CacheManager();
            cacheManager.OnRetriving += (sender, e) =>
            {
                var province = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%",
                                                      LocalizationContext.PreferredCultureCode, false, "CRC.Province",
                                                      string.Format("ProvinceCode = '{0}'", Variables.RegionCode), null,
                                                      -1, true, 1, "NodeAliasPath,DocumentUrlPath");

                if (DataHelper.DataSourceIsEmpty(province)) return null;

                return DocumentURLProvider.GetUrl(
                        ValidationHelper.GetString(province.Tables[0].Rows[0]["NodeAliasPath"], String.Empty),
                        ValidationHelper.GetString(province.Tables[0].Rows[0]["DocumentUrlPath"], String.Empty));
            };

            return cacheManager.GetObject<string>(string.Format("IYCUrl_{0}", Variables.RegionCode)) ?? defaultUrl;
        }

        public static string AddClickToCall(string content)
        {
            if (!DeviceContext.CurrentDevice.IsMobile && !DeviceContext.CurrentDevice.IsTablet) return content;

            var regex = new Regex(@"((\d-)?\(?\d{3}\)?-? *\d{3}-? *-?\d{4})|((\d )?\(?\d{3}\)?-? *\d{3}-? *-?\d{4})", RegexOptions.IgnoreCase);
            return regex.Replace(content, (m) => string.Format("<a href=\"tel:{0}\">{0}</a>", m.Value));
        }

        public static string ReplaceNewLine(string content)
        {
            return (content ?? string.Empty).Replace(Environment.NewLine, "<br />").Replace("\n", "<br />");
        }

        public static string GetResizedImage(string imagePath, int imageWidth)
        {
            if (imageWidth < 1) return imagePath;

            imagePath = URLHelper.AddParameterToUrl(imagePath, "width", imageWidth.ToString());
            imagePath = URLHelper.RemoveParameterFromUrl(imagePath, "height");
            if (!string.IsNullOrWhiteSpace(imagePath) && imagePath.StartsWith("~/"))
                imagePath = imagePath.Substring(1);

            return imagePath;
        }
    }
}