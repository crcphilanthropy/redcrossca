using System;
using System.Web;
using System.Web.Caching;
using CMS.Localization;
using CMS.PortalEngine;
using CMS.SiteProvider;

namespace CMSAppAppCode.Old_App_Code.CRC
{
    public class CacheManager
    {
        private static readonly Object _obj = new object();

        public delegate object RetrivingHandler(Object sender, EventArgs args);

        public event RetrivingHandler OnRetriving;


        public CacheManager()
        {
            TimeInMinutes = CMS.DataEngine.SettingsKeyInfoProvider.GetIntValue("CacheTimeInMinutes");
            Enabled = CMS.DataEngine.SettingsKeyInfoProvider.GetBoolValue("EnableCache");
        }

        public int TimeInMinutes { get; set; }
        public bool Enabled { get; set; }

        public void AddItemToCache<T>(string key, T value) where T : class
        {
            if (!Enabled) return;

            RemoveItemFromCache(key, false);

            lock (_obj)
            {
                HttpRuntime.Cache.Add(key, value, null, DateTime.Now.Add(TimeSpan.FromMinutes(TimeInMinutes)), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);
            }
        }

        public void RemoveItemFromCache(string key, bool concatKey = true)
        {
            if (concatKey)
                key = GetConcatKey(key);

            lock (_obj)
            {
                HttpRuntime.Cache.Remove(key);
            }
        }

        public virtual T GetObject<T, TV>(string key, TV dependency) where T : class
        {
            key = GetConcatKey(key);
            var item = default(T);
            var useCache = Enabled && (PortalContext.ViewMode == ViewModeEnum.LiveSite);
            if (useCache)
                item = (T)HttpRuntime.Cache[key];

            if (item != null || OnRetriving == null)
                return item;

            item = (T)OnRetriving(this, new GenericEventArgs<TV>() { Dependency = dependency });

            if (null != item)
            {
                AddItemToCache(key, item);
            }

            return item;
        }

        public virtual T GetObject<T>(string key) where T : class
        {
            key = GetConcatKey(key);
            var item = default(T);
            var useCache = Enabled && (PortalContext.ViewMode == ViewModeEnum.LiveSite);
            if (useCache)
                item = (T)HttpRuntime.Cache[key];

            if (item != null || OnRetriving == null)
                return item;

            item = (T)OnRetriving(this, new GenericEventArgs<string>() { Dependency = key });

            if (null != item)
            {
                AddItemToCache(key, item);
            }

            return item;
        }

        private string GetConcatKey(string key)
        {
            return string.Format("{0}_{1}_{2}", key, SiteContext.CurrentSiteName,
                                 LocalizationContext.PreferredCultureCode);
        }

        public class GenericEventArgs<T> : EventArgs
        {
            public T Dependency { get; set; }
        }

    }
}
