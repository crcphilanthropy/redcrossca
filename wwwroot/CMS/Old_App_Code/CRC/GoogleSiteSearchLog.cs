﻿using CMS.CustomTables;
using CMS.DataEngine;

namespace CMSAppAppCode
{
    public class GoogleSiteSearchLog
    {
        public string Request { get; set; }
        public string Response { get; set; }

        /// <summary>
        /// Writes the log entry.
        /// </summary>
        /// <returns></returns>
        public bool WriteLogEntry()
        {
            // Check if the table exists
            var customTable = DataClassInfoProvider.GetDataClassInfo("Osler.GoogleSiteSearchLog");
            if (customTable != null)
            {
                // Create new custom table item 
                var newCustomTableItem = CustomTableItem.New("Osler.GoogleSiteSearchLog");

                newCustomTableItem.SetValue("Request", Request);
                newCustomTableItem.SetValue("Response", Response);

                // Insert the custom table item into database
                newCustomTableItem.Insert();

                return true;
            }

            return false;
        }
    }
}