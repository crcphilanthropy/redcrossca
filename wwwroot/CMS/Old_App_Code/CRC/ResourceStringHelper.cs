using System;
using CMS.Localization;
using CMS.Helpers;

namespace CMSApp.CRC
{
    public static class ResourceStringHelper
    {
        public static void AddResourceString(string stringKey, string defaultText)
        {
            var ri = ResourceStringInfoProvider.GetResourceStringInfo(stringKey);
            if (ri != null) return;

            ri = new ResourceStringInfo
                {
                    StringKey = stringKey,
                    StringIsCustom = true,
                    CultureCode = ResourceStringInfoProvider.DefaultUICulture,
                    TranslationText = ValidationHelper.GetString(defaultText, string.Empty)
                };
            ResourceStringInfoProvider.SetResourceStringInfo(ri);
        }

        public static string GetString(string stringKey, string defaultText = "", string cultureCode = "")
        {
            if (string.IsNullOrWhiteSpace(stringKey)) return string.Empty;

            cultureCode = string.IsNullOrWhiteSpace(cultureCode) ? LocalizationContext.PreferredCultureCode : cultureCode;


            var toReturn = string.IsNullOrWhiteSpace(cultureCode)
                       ? ResHelper.GetString(stringKey)
                       : ResHelper.GetString(stringKey, cultureCode);


            //add default text if no translation is found
            if (stringKey.Equals(toReturn, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(defaultText))
            {
                AddResourceString(stringKey, defaultText);
                return defaultText;
            }

            return toReturn;
        }

        public static string LocalizeString(string stringKey, string cultureCode = "")
        {
            return string.IsNullOrWhiteSpace(cultureCode)
                       ? ResHelper.LocalizeString(stringKey)
                       : ResHelper.LocalizeString(stringKey, cultureCode);
        }
    }
}
