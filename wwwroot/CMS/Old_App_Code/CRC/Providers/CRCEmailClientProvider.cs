﻿using System;
using System.Configuration;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.SiteProvider;
using CMSApp.CRC;
using CRC.EmailClient.Interfaces;
using CRC.EmailClient.Clients;
using CRC.EmailClient.Models;
using CMSAppAppCode.Old_App_Code.CRC.Extensions;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    public class CRCEmailClientProvider
    {
        public enum EmailClientResult
        {
            Subscribed,
            Duplicate,
            NotCreated,
            Error
        }

        public enum EmailClientType
        {
            Lyris,
            Campaigner,
            CampaignMonitor
        }

        public static bool IsInTestMode
        {
            get
            {
                return string.Equals(ConfigurationManager.AppSettings["TestEmailSubscription"], "true", StringComparison.OrdinalIgnoreCase);
            }
        }

        public static EmailClientResult Subscribe<T>(IEmailContact<T> contact)
        {
            IEmailClient<T> client;
            switch (CurrentEmailClient)
            {
                case EmailClientType.CampaignMonitor:
                    client = (IEmailClient<T>)new CampaignMonitorEmailClient(new EmailClientConfig()
                    {
                        ApiKey = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignMonitorApiKey", SiteContext.CurrentSiteName)),
                        Password = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignMonitorApiKey", SiteContext.CurrentSiteName)),
                        UserName = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignMonitoryClientID", SiteContext.CurrentSiteName)),
                        ServiceUrl = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignerServiceURL", SiteContext.CurrentSiteName))
                    });
                    break;
                case EmailClientType.Lyris:
                    var webServiceUrl = SettingsKeyInfoProvider.GetValue("LyrisServer");
                    var username = SettingsKeyInfoProvider.GetValue("LyrisUser");
                    var password = SettingsKeyInfoProvider.GetValue("LyrisPassword");
                    client = (IEmailClient<T>)new LyrisEmailClient(new EmailClientConfig
                    {
                        Password = password,
                        ServiceUrl = webServiceUrl,
                        UserName = username
                    });
                    break;
                default:
                    client = (IEmailClient<T>)new CampaignerEmailClient(new EmailClientConfig()
                    {
                        Password = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignerAPIPassword", SiteContext.CurrentSiteName)),
                        UserName = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignerAPIUsername", SiteContext.CurrentSiteName)),
                        ServiceUrl = SettingsKeyInfoProvider.GetValue(string.Format("{0}.CampaignerServiceURL", SiteContext.CurrentSiteName))
                    });
                    break;
            }

            client.Error += (sender, exception) => EventLogProvider.LogException("CRCEmailClientProvider", "Subscribe", exception.ExceptionObject as Exception);

            if (client.AlreadySubscribe(contact)) return EmailClientResult.Duplicate;

            return client.Subscribe(contact) ? EmailClientResult.Subscribed : EmailClientResult.Error;
        }

        public static void SaveToCustomTable<T>(IEmailContact<T> contact)
        {
            try
            {
                var itemToDelete = ValidationHelper.GetInteger(
                        CustomTableHelper.GetItemValue("CRC.LyrisSubscriptions",
                                                       string.Format("Email = '{0}' and List = '{1}'", contact.Email,
                                                                     contact.ListKey), "ItemID"), 0);
                if (itemToDelete > 0)
                {
                    CustomTableHelper.DeleteCustomTableItem("CRC.LyrisSubscriptions", itemToDelete);
                }

                CustomTableHelper.InsertCustomTableItem("CRC.LyrisSubscriptions", contact);
            }
            catch (Exception exception)
            {
                EventLogProvider.LogException("LyrisController", "SaveToCustomTable", exception);
            }
        }

        public static EmailClientType CurrentEmailClient
        {
            get
            {
                return SettingsKeyInfoProvider.GetValue(string.Format("{0}.EmailClientType", SiteContext.CurrentSiteName)).ToEnum(EmailClientType.Lyris);
            }
        } 
    }
}