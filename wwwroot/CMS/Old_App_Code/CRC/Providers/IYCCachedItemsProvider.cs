﻿using System;
using System.Collections.Generic;
using System.Data;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Localization;
using CMS.SiteProvider;
using CMSApp.CRC;
using System.Linq;
using CMSAppAppCode.Old_App_Code.CRC.Helpers;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    /// <summary>
    /// This provider is used to get and cached data for IYC.
    /// </summary>
    /// <typeparam name="T">Must be class of that implements ICoordinates and has an empty constructor</typeparam>
    internal class IYCCachedItemsProvider<T> where T : class, ICoordinates, new()
    {
        private readonly string _className;
        private readonly string[] _columns;
        private readonly string _queryName;

        internal IYCCachedItemsProvider(string className, string queryName = null, params string[] columns)
        {
            _className = className;
            _columns = columns;
            _queryName = queryName;
        }


        /// <summary>
        /// Get Items with Proximity/Distance
        /// </summary>
        /// <param name="key">Cached Key</param>
        /// <param name="lat">Pivot Latitude</param>
        /// <param name="lng">Pivot Longitude</param>
        /// <param name="maxDistance">Max Radius/Distance</param>
        /// <returns>return IEnumerable of type T</returns>
        internal IEnumerable<T> GetItemsWithProximity(string key, decimal lat, decimal lng, int maxDistance)
        {
            var items = GetData(key);

            items = (from n in items
                     let distance = ProximityHelper.CalculateDistince(n, lat, lng)
                     where (distance <= maxDistance || maxDistance == 0) || (lat == 0 && lng == 0)
                     orderby distance
                     select new { Item = n, Distance = (lat == 0 && lng == 0) ? 0 : distance }).Select(n =>
                         {
                             n.Item.Distance = n.Distance;
                             return n.Item;
                         });

            return items;
        }

        /// <summary>
        /// Get list of items from cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>IEnumerable of T</returns>
        internal IEnumerable<T> GetItems(string key)
        {
            var items = GetData(key);
            return items;
        }

        /// <summary>
        /// Get single item from cached data
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="where">Where</param>
        /// <returns>Single Item of type T</returns>
        internal T FirstOrDefault(string key, Func<T, bool> where)
        {
            var items = GetData(key);
            return items.FirstOrDefault(where);
        }

        /// <summary>
        /// Get data, tried to get from cache first, adds to cache if it add to make a DB call
        /// </summary>
        /// <param name="key">Cache key</param>
        /// <returns></returns>
        private IEnumerable<T> GetData(string key)
        {
            var cacheManager = new CacheManager();
            cacheManager.OnRetriving += cacheManager_OnRetriving;
            return cacheManager.GetObject<IEnumerable<T>>(key) ?? new List<T>();
        }

        /// <summary>
        /// Get data to be added to cached
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        object cacheManager_OnRetriving(object sender, EventArgs args)
        {
            if (!string.IsNullOrWhiteSpace(_queryName))
                return GetByQuery();

            DataSet data = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, "/%", LocalizationContext.PreferredCultureCode, false, _className,
                "NodeLinkedNodeID is null", null, -1, true, -1, string.Join(",", _columns));

            return data.ConvertTo<T>();
        }

        /// <summary>
        /// Get list of Items using Kentico's DataQuery (queries created in Page Types)
        /// </summary>
        /// <returns></returns>
        private IEnumerable<T> GetByQuery()
        {
            var parameters = new QueryDataParameters { { "DocumentCulture", LocalizationContext.PreferredCultureCode } };
            var query = new DataQuery(_className, _queryName)
                .Where(null, parameters)
                .Columns(_columns);

            var data = query.Execute();

            return data.ConvertTo<T>();
        }
    }
}