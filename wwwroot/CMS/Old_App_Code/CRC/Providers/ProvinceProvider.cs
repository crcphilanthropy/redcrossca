﻿using System;
using System.Collections.Generic;
using CMS.DocumentEngine;
using CMS.Localization;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    public class ProvinceProvider
    {
        private readonly string[] _columns = new[]
                {
                    "DocumentGuid",
                    "NodeID",
                    "NodeParentID",
                    "NodeOrder",
                    "DocumentName",
                    "NodeAliasPath",
                    "DocumentUrlPath",
                    "Name",
                    "Summary",
                    "Description",
                    "ProvinceCode",
                    "ProvinceDisplayCode",
                    "IconPath",
                    "ThumbnailAltText",
                    "Longitude",
                    "Latitude",
                    "FindServicesResultPage"
                };

        public IEnumerable<IProvince> GetAllProvinces()
        {
            var cachedProvider = new IYCCachedItemsProvider<Province>("CRC.Province", null, _columns);
            return cachedProvider.GetItems("IYCProvincesAll");
        }

        private TreeNode _province;
        public TreeNode CurrentProvince
        {
            get { return _province ?? (_province = GetCurrentProvince()); }
        }

        private TreeNode GetCurrentProvince()
        {
            var document = DocumentContext.CurrentDocument;
            while (document != null)
            {
                if (document.ClassName.Equals("CRC.Province", StringComparison.OrdinalIgnoreCase))
                    return document;

                document = document.Parent;
            }

            return null;
        }

        public IPageMenu GetInYourCommunityDocument()
        {
            var cacheManager = new CacheManager();
            cacheManager.OnRetriving += (sender, args) =>
            {
                var data = TreeHelper.GetDocument(SiteContext.CurrentSiteName, "/In-Your-Community",
                    LocalizationContext.PreferredUICultureCode, false, "CRC.Page", true);

                return data == null ?  null : data.ConvertTo<PageMenu>();
            };
            return cacheManager.GetObject<PageMenu>("in_your_community_node_" + LocalizationContext.PreferredUICultureCode);
        }
    }
}