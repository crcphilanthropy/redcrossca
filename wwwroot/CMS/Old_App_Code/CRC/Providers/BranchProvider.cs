﻿using System;
using System.Collections.Generic;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using System.Linq;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    /// <summary>
    /// Branch Provider
    /// </summary>
    public class BranchProvider
    {
        private readonly string[] _columns = new []
                { 
                    "DocumentGuid", 
                    "NodeID", 
                    "NodeParentID",
                    "NodeOrder",
                    "DocumentName",
                    "NodeAliasPath",
                    "DocumentUrlPath", 
                    "Name",
                    "Summary",
                    "Description",
                    "Thumbnail",
                    "ThumbnailAltText", 
                    "BranchAddress",
                    "BranchHours",
                    "BranchContact",
                    "Longitude",
                    "Latitude",
                    "UseCategories"
                };

        /// <summary>
        /// Perform search on Branches
        /// </summary>
        /// <param name="latitude">Pivot Latitude</param>
        /// <param name="longitude">Pivot Longitude</param>
        /// <param name="maxDistance">Radius for search</param>
        /// <returns>IEnumerable of IBranch order by distance then by Branch Name</returns>
        public IEnumerable<IBranch> Search(Decimal latitude, Decimal longitude, int maxDistance)
        {
            var cachedProvider = new IYCCachedItemsProvider<Branch>("CRC.IYCBranch", null, _columns);
            return cachedProvider.GetItemsWithProximity("IYCBranchesAll", latitude, longitude, maxDistance)
                .OrderBy(branch=> branch.Distance)
                .ThenBy(branch=> branch.Name);
        }

        /// <summary>
        /// Get branch by documentGuid
        /// </summary>
        /// <param name="branchDocumentGuid">Branch Document Guid</param>
        /// <returns>return Branch</returns>
        public IBranch GetByGuid(Guid branchDocumentGuid)
        {
            var cachedProvider = new IYCCachedItemsProvider<Branch>("CRC.IYCBranch", null, _columns);
            return cachedProvider.FirstOrDefault("IYCBranchesAll", branch => branch.DocumentGuid.Equals(branchDocumentGuid));
        }
    }
}