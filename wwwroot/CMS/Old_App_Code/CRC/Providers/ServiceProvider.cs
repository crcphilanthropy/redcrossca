﻿using System;
using System.Collections.Generic;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using System.Linq;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    /// <summary>
    /// Service Provider
    /// </summary>
    public class ServiceProvider
    {
        private readonly string[] _columns = new[]
                { 
                    "(case when s.Latitude is null then b.Latitude " +
                        "when s.Latitude = 0 then b.Latitude " +
                        "else s.Latitude END) as Latitude", 
                    "(case when s.Longitude is null then b.Longitude " +
                        "when s.Longitude = 0 then b.Longitude " +
                        "else s.Longitude END) as Longitude ", 
                    "(case when s.Contact is null then b.BranchContact when s.Contact = '' then b.BranchContact else s.contact end) as [Contact]",
                    "(case when s.Address is null then b.BranchAddress when s.Address = '' then b.BranchAddress else s.Address end) as [Address]",
                    "(case when s.HoursOfOperation is null then b.BranchHours when s.HoursOfOperation = '' then b.BranchHours else s.HoursOfOperation end) as [HoursOfOperation]",
                    "c.Name as [CategoryName]", 
                    "c.DocumentUrlPath as [CategoryUrl]",
                    "c.IconPath as [CategoryIconPath]",
                    "c.ThumbnailAltText as [CategoryIconAltText]",
                    "sc.Name as [SubCategoryName]", 
                    "sc.DocumentUrlPath as [SubCategoryUrl]",
                    "sc.IconPath as [SubCategoryIconPath]",
                    "sc.ThumbnailAltText as [SubCategoryIconAltText]",
                    "s.DocumentGuid", 
                    "s.NodeID", 
                    "s.NodeParentID",
                    "s.DocumentName",
                    "s.NodeAliasPath",
                    "s.DocumentUrlPath", 
                    "s.Name",
                    "(case " +
                        "when s.Summary is not null and s.Summary <> '' then s.Summary " +
                        "when (s.Summary is null or s.Summary = '') and sc.Summary is not null and sc.Summary <> '' then sc.Summary " +
                        "when sc.Summary is null or sc.Summary = '' then c.Summary " +
                        "else s.Summary End) as Summary",
                    "(case " +
                        "when s.Description is not null and s.Description <> '' then s.Description " +
                        "when (s.Description is null or s.Description = '') and sc.Description is not null and sc.Description <> '' then sc.Description " +
                        "when sc.Description is null or sc.Description = '' then c.Description " +
                        "else s.Description End) as Description",
                    "s.Thumbnail",
                    "s.ThumbnailAltText",
                    "s.BranchDocumentGuid",
                    "s.CategoryDocumentGuid",
                    "s.SubCategoryDocumentGuid"
                };

        private readonly IYCCachedItemsProvider<Service> _cachedProvider;
        public ServiceProvider()
        {
            _cachedProvider = new IYCCachedItemsProvider<Service>("CRC.IYCService", "ProximitySearch", _columns);
        }

        /// <summary>
        /// Search Services base on config(criteria)
        /// </summary>
        /// <param name="config">search config/criteria</param>
        /// <returns>IEnumerable of Service order by Distacne, then by Service name</returns>
        public IEnumerable<IService> Search(IServiceSearchConfig config)
        {
            if (config.IsEmpty()) return new List<IService>();

            //var cachedProvider = new IYCCachedItemsProvider<Service>("CRC.IYCService", "ProximitySearch", _columns);
            var query = _cachedProvider
                .GetItemsWithProximity("IYCServicesAll", config.Latitude, config.Longitude, config.MaxDistance);

            if (config.CategoryDocumentGuid != Guid.Empty)
                query = query.Where(service => service.CategoryDocumentGuid.Equals(config.CategoryDocumentGuid));

            if (config.SubCategoryDocumentGuid != Guid.Empty)
                query = query.Where(service => service.SubCategoryDocumentGuid.Equals(config.SubCategoryDocumentGuid));

            return query.OrderBy(service => service.Distance)
            .ThenBy(service => service.Name)
            .Take(Variables.TopNServices);
        }

        /// <summary>
        /// Get services by branch
        /// </summary>
        /// <param name="branchDocumentGuid">Branch document guid</param>
        /// <returns>IEnumerable of Service order by Service name</returns>
        public IEnumerable<IService> GetAllByBranch(Guid branchDocumentGuid)
        {
            return _cachedProvider.GetItems("IYCServicesAll").Where(service => service.BranchDocumentGuid.Equals(branchDocumentGuid)).OrderBy(service => service.Name);
        }

        /// <summary>
        /// Get single service Item
        /// </summary>
        /// <param name="serviceDocumentGuid">Service Document Guid</param>
        /// <returns>IService object</returns>
        public IService GetByDocumentGuid(Guid serviceDocumentGuid)
        {
            return _cachedProvider.FirstOrDefault("IYCServicesAll", service => service.DocumentGuid.Equals(serviceDocumentGuid));
        }
    }
}