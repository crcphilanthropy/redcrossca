﻿using System;
using System.Web;
using System.Web.Script.Serialization;
using CMSAppAppCode.Old_App_Code.CRC.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    public class DisruptiveCTAProvider
    {
        private readonly int _hideAfterNTimesClosed;
        private readonly int _hideAfterNDays;
        private readonly string _cookieKey;

        public DisruptiveCTAProvider(int hideAfterNTimesClosed, int hideAfterNDays, string cookieKey)
        {
            _hideAfterNTimesClosed = hideAfterNTimesClosed;
            _hideAfterNDays = hideAfterNDays;
            _cookieKey = cookieKey;
        }

        public bool ShowDisruptiveCTA(string path = "")
        {
            var serializer = new JavaScriptSerializer();
            var cookie = HttpContext.Current.Request.Cookies[_cookieKey];
            PromotionalCookie promoCookie;

            if (cookie != null)
            {
                promoCookie = serializer.Deserialize<PromotionalCookie>(cookie.Value);

                if (!promoCookie.CTAPath.Equals(path, StringComparison.OrdinalIgnoreCase))
                {
                    RemovePromoBannerEmailCookie();
                    promoCookie = CreatePromotionCookie(path);
                    cookie.Expires = promoCookie.DateTime.AddDays(_hideAfterNDays);
                }

                if (promoCookie.CloseCounter >= _hideAfterNTimesClosed) return false;
            }
            else
            {
                cookie = new HttpCookie(_cookieKey);

                promoCookie = CreatePromotionCookie(path);
                cookie.Expires = promoCookie.DateTime.AddDays(_hideAfterNDays);
            }

            cookie.Value = serializer.Serialize(promoCookie);

            HttpContext.Current.Response.Cookies.Add(cookie);
            return true;
        }

        private void RemovePromoBannerEmailCookie()
        {
            if (HttpContext.Current.Request.Cookies[_cookieKey] == null) return;

            var myCookie = new HttpCookie(_cookieKey) { Expires = DateTime.Now.AddDays(-1d) };
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        private static PromotionalCookie CreatePromotionCookie(string path)
        {
            return new PromotionalCookie
            {
                CTAPath = path,
                DateTime = DateTime.Now
            };
        }
    }
}