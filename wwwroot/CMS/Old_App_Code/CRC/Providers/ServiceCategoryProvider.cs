﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.Providers
{
    /// <summary>
    /// Service Categories Provider
    /// </summary>
    public class ServiceCategoryProvider
    {
        private IProvince _provinceNode;
        private readonly List<string> _columns = new List<string>
                { 
                    "DocumentGuid", 
                    "NodeID", 
                    "NodeParentID",
                    "NodeOrder",
                    "DocumentName",
                    "NodeAliasPath",
                    "DocumentUrlPath", 
                    "Name",
                    "Summary",
                    "Description",
                    "Thumbnail",
                    "ThumbnailAltText"
                };


        /// <summary>
        /// Get Province base on current document location
        /// </summary>
        /// <returns>Province</returns>
        private static IProvince GetProvince()
        {
            var document = DocumentContext.CurrentDocument;
            while (document != null)
            {
                if (document.ClassName.Equals("CRC.Province", StringComparison.OrdinalIgnoreCase))
                    return document.ConvertTo<Province>();

                document = document.Parent;
            }

            return null;
        }

        /// <summary>
        /// Province base on current document location
        /// </summary>
        public IProvince ProvinceNode
        {
            get { return _provinceNode ?? (_provinceNode = GetProvince()); }
        }


        /// <summary>
        /// Get list of categories base on Path
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>IEnumerable of IPageMenu</returns>
        public IEnumerable<IPageMenu> GetCategories(string path, string className = "CRC.IYCCategory")
        {
            return GetData(className, path);
        }

        /// <summary>
        /// Get list of sub categories base on Path
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>IEnumerable of IPageMenu</returns>
        public IEnumerable<IPageMenu> GetSubCategories(string path)
        {
            return GetData("CRC.IYCSubCategory", path);
        }

        /// <summary>
        /// Get list of sub categories base on category document guid
        /// </summary>
        /// <param name="categoryDocumentGuid">Category Document Guid</param>
        /// <param name="includeSelectAll">Add select all option for drop down list</param>
        /// <returns>IEnumerable of IPageMenu</returns>
        public IEnumerable<IPageMenu> GetSubCategories(Guid categoryDocumentGuid, bool includeSelectAll = true)
        {
            var toReturn = GetData("CRC.IYCSubCategory",
                           "/%",
                           string.Format(
                               "NodeParentID = (SELECT TOP 1 NODEID FROM View_CMS_Tree_Joined_Versions T WHERE T.DocumentGuid = '{0}')",
                               categoryDocumentGuid)).ToList();

            if (toReturn.Any() && includeSelectAll)
                toReturn.Insert(0, new PageMenu() { Name = ResourceStringHelper.GetString("CRC.Select") });

            return toReturn;
        }


        /// <summary>
        /// Get Data
        /// </summary>
        /// <param name="className">Page Type</param>
        /// <param name="path">Path</param>
        /// <param name="where">Where clause</param>
        /// <returns>IEnumerable of IPageMenu</returns>
        private IEnumerable<IPageMenu> GetData(string className, string path, string where = null)
        {
            return CacheHelper.Cache(() =>
                {
                    var data = TreeHelper.GetDocuments(SiteContext.CurrentSiteName, path,
                                              LocalizationContext.PreferredCultureCode, false, className, where,
                                              "Name Asc", -1, true, -1, string.Join(",", _columns));

                    return data.ConvertTo<PageMenu>().OrderBy(item => item.Name);
                }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("service_category_provider_{0}_{1}_{2}_{3}", className, path, where, LocalizationContext.PreferredCultureCode)));
        }
    }
}