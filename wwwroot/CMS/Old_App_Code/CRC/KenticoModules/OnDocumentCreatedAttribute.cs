using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.PortalEngine;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC;
using CultureInfo = System.Globalization.CultureInfo;
using CMS.Base;
using CMS.DataEngine;
using CMS.Membership;
using CMS.Localization;
using CMS.Helpers;

[OnDocumentCreated]
public partial class CMSModuleLoader
{
    public class OnDocumentCreatedAttribute : CMSLoaderAttribute
    {
        /// <summary>
        /// Inits this instance.
        /// </summary>
        public override void Init()
        {
            base.Init();

            DocumentEvents.Insert.Before += InsertOnBefore;
            DocumentEvents.Insert.Before += InheritTagGroup;
            DocumentEvents.InsertNewCulture.Before += InheritTagGroup;
            DocumentEvents.Insert.After += InsertOnAfter;
            DocumentEvents.InsertNewCulture.After += InsertOnAfter;
            DocumentEvents.Update.Before += UpdateBefore;
            ObjectEvents.Insert.Before += InsertObjectBefore;
            ObjectEvents.Update.Before += UpdateObjectBefore;

            //IYC
            DocumentEvents.Insert.Before += SetIYCValues;
            DocumentEvents.Update.Before += SetIYCValues;
            WorkflowEvents.Publish.After += ClearIYCCache;

            //Populate Email Content
            DocumentEvents.Insert.Before += PopulateEmailContent;
            DocumentEvents.Update.Before += PopulateEmailContent;

            //SetInitialValues
            DocumentEvents.Insert.Before += SetInitialValues;
        }

        void SetInitialValues(object sender, DocumentEventArgs e)
        {
            if (e.Node == null) return;
            e.Node.SetValue("ShowRosBanner", -1);
            e.Node.SetValue("ShowTwitter", -1);
            e.Node.SetValue("ShowPromotionalBanner", -1);
            e.Node.SetValue("ShowMobileShare", -1);
            e.Node.SetValue("ShowMobileEmailSignUp", -1);
        }

        private void PopulateEmailContent(object sender, DocumentEventArgs e)
        {
            switch (e.Node.ClassName.ToLower())
            {
                case "crc.archivedemail":
                    var content = new WebClient().DownloadString(URLHelper.GetAbsoluteUrl(e.Node.GetStringValue("HTMLEmail", string.Empty)));
                    e.Node.SetValue("EmailContent", content);
                    break;
            }
        }

        private void InheritTagGroup(object sender, DocumentEventArgs e)
        {
            switch (e.Node.ClassName.ToLower())
            {
                case "crc.blogyear":
                case "cms.blogmonth":
                case "cms.blogpost":
                    e.Node.SetValue("DocumentTagGroupID", null);
                    break;
            }

            if (e.Node.ClassName.StartsWith("crc.email", StringComparison.OrdinalIgnoreCase))
                e.Node.SetValue("DocumentShowInSiteMap", false);
        }

        void ClearIYCCache(object sender, WorkflowEventArgs e)
        {
            switch (e.Document.ClassName.ToLower())
            {
                case "crc.iycservice":
                case "crc.iycbranch":
                case "crc.iycsubcategory":
                case "crc.iyccategory":
                    var cacheManger = new CacheManager();
                    cacheManger.RemoveItemFromCache("IYCServicesAll");
                    cacheManger.RemoveItemFromCache("IYCBranchesAll");
                    break;
            }
        }

        void SetIYCValues(object sender, DocumentEventArgs e)
        {
            switch (e.Node.ClassName.ToLower())
            {
                case "crc.iycservice":
                    var parent = e.Node.Parent;
                    while (parent != null)
                    {
                        switch (parent.ClassName.ToLower())
                        {
                            case "crc.iycbranch":
                                e.Node.SetValue("BranchDocumentGuid", parent.DocumentGUID);
                                break;
                            case "crc.iycsubcategory":
                                e.Node.SetValue("SubCategoryDocumentGuid", parent.DocumentGUID);
                                break;
                            case "crc.iyccategory":
                                e.Node.SetValue("CategoryDocumentGuid", parent.DocumentGUID);
                                return;
                        }
                        parent = parent.Parent;
                    }
                    break;
            }
        }

        /// <summary>
        /// Event that fires before an object is inserted
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ObjectEventArgs"/> instance containing the event data.</param>
        private void InsertObjectBefore(object sender, ObjectEventArgs e)
        {
            ChangeAliasToLower(e);
            if (e.Object != null)
            {
                if (e.Object.GetStringValue("ClassName", string.Empty).StartsWith("crc.email", StringComparison.OrdinalIgnoreCase))
                {
                    e.Object.SetValue("DocumentShowInSiteMap", false);
                }

            }
        }

        /// <summary>
        /// Event that fires before an object is updated
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ObjectEventArgs"/> instance containing the event data.</param>
        private void UpdateObjectBefore(object sender, ObjectEventArgs e)
        {
            ChangeAliasToLower(e);
        }

        /// <summary>
        /// Changes the document alias to lowercase.
        /// </summary>
        /// <param name="e">The <see cref="ObjectEventArgs"/> instance containing the event data.</param>
        private void ChangeAliasToLower(ObjectEventArgs e)
        {
            if (e.Object is DocumentAliasInfo)
            {
                ((DocumentAliasInfo)e.Object).AliasURLPath = ((DocumentAliasInfo)e.Object).AliasURLPath.ToLower();
            }
        }

        /// <summary>
        /// Updates the before.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void UpdateBefore(object sender, DocumentEventArgs e)
        {
            switch (e.Node.NodeClassName.ToLower())
            {
                case "crc.program":
                    UpdateProgram(sender, e, false);
                    break;
                default:
                    CreateAlias(e);
                    UpdateDocumentUrlPath(e, false);
                    break;
            }

            if (!string.IsNullOrWhiteSpace(e.Node.DocumentName))
                e.Node.DocumentName = e.Node.DocumentName.Trim();

            ChangePathsToLower(e);
        }

        private void CreateAlias(DocumentEventArgs e)
        {
            try
            {
                if (Enumerable.Cast<DocumentAliasInfo>(e.Node.Aliases).Any(alias => alias.AliasURLPath.Equals(e.Node.RelativeURL.Replace("~", string.Empty), StringComparison.OrdinalIgnoreCase)))
                    return;


                if (!string.IsNullOrWhiteSpace(e.Node.DocumentUrlPath)) return;

                var aliasinfo = new DocumentAliasInfo
                {
                    AliasURLPath = e.Node.RelativeURL.Replace("~", string.Empty),
                    AliasCulture = e.Node.DocumentCulture,
                    AliasSiteID = SiteContext.CurrentSiteID,
                    AliasNodeID = e.Node.NodeID
                };
                aliasinfo.Insert();
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("OnDocumentCreatedAttribute", "CreateAlias", ex);
            }
        }

        /// <summary>
        /// Inserts the on before.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void InsertOnBefore(object sender, DocumentEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.Node.DocumentName))
                e.Node.DocumentName = e.Node.DocumentName.Trim();

            if (e.Node.ClassName.StartsWith("crc.email", StringComparison.OrdinalIgnoreCase))
                e.Node.SetValue("DocumentShowInSiteMap", false);

            ChangePathsToLower(e);
        }

        /// <summary>
        /// Changes the paths to lower.
        /// </summary>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void ChangePathsToLower(DocumentEventArgs e)
        {
            e.Node.SetValue("NodeAliasPath", e.Node.NodeAliasPath.ToLower());
            e.Node.SetValue("DocumentUrlPath", e.Node.DocumentUrlPath.ToLower());
            e.Node.DocumentUrlPath = e.Node.DocumentUrlPath.ToLower();

            e.Node.SetValue("DocumentNamePath", e.Node.DocumentNamePath.ToLower());
        }

        /// <summary>
        /// Inserts the on after.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void InsertOnAfter(object sender, DocumentEventArgs e)
        {
            switch (e.Node.NodeClassName.ToLower())
            {
                case "crc.navheading":
                    UpdateNavigationHeadings(sender, e);
                    break;
                case "crc.navigationsubheading":
                    UpdateNavigationSubHeadings(sender, e);
                    break;
                case "crc.program":
                    UpdateProgram(sender, e);
                    break;
                case "crc.blogyear":
                    AddBlogMonths(sender, e);
                    UpdateDocumentUrlPath(e);
                    break;
                default:
                    UpdateDocumentUrlPath(e);
                    break;
            }
        }

        private void AddBlogMonths(object sender, DocumentEventArgs e)
        {
            var year = e.Node.GetDateTimeValue("BlogMonthStartingDate", DateTime.Now).Year;
            var pageTemplate = PageTemplateInfoProvider.GetPageTemplateInfo("CRC.Blog.PageTemplates.ListingPage");
            var treeProvider = new TreeProvider(MembershipContext.AuthenticatedUser)
            {
                MergeResults = true,
                UseAutomaticOrdering = false //disable automatic ordering
            };

            for (var i = 1; i <= 12; i++)
            {
                CRCDocumentHelper.SaveFolder(treeProvider, i.ToString(CultureInfo.InvariantCulture), "CMS.BlogMonth", e.Node, LocalizationContext.PreferredCultureCode, "en-CA", new Dictionary<string, object>
                    {
                        { "BlogMonthName", i },
                        { "BlogMonthStartingDate", new DateTime(year, i, 1) },
                        { "NodeOrder", i },
                        { "DocumentPageTemplateID", pageTemplate == null ? 0 : pageTemplate.PageTemplateId },
                        { "ShowRosBanner", -1 },
                        { "ShowTwitter", -1 },
                        { "ShowPromotionalBanner", -1 },
                        { "ShowMobileShare", -1 },
                        { "ShowMobileEmailSignUp", -1 }
                    });
            }

            e.Node.SetValue("NodeChildNodesCount", 12);
            e.Node.Update();
        }

        private readonly List<string> _ignoreUrlPathDocuments = new List<string>
        {
            "crc.holidaycampaign"
        };

        /// <summary>
        /// Updates the document URL path.
        /// </summary>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        /// <param name="saveAfterChange">if set to <c>true</c> [save after change].</param>
        private void UpdateDocumentUrlPath(DocumentEventArgs e, bool saveAfterChange = true)
        {
            var regexQueryPattern = new Regex("({).*(})", RegexOptions.IgnoreCase);
            if (regexQueryPattern.IsMatch(e.Node.DocumentUrlPath) || _ignoreUrlPathDocuments.Contains(e.Node.ClassName, new StringEqualityComparer())) return;

            var ignoreClasses = new List<string> { "CRC.Campaign" };
            if (ignoreClasses.Contains(e.Node.ClassName)) return;

            var path = new List<string> { RemoveForbiddenCharacters(e.Node.DocumentName).Trim(new[] { '-' }) };
            var parentNode = e.Node.Parent;
            while (parentNode != null)
            {
                if (!(e.Node.ClassName.Equals("CRC.IYCService", StringComparison.OrdinalIgnoreCase) && parentNode.ClassName.Equals("CRC.IYCBranch", StringComparison.OrdinalIgnoreCase)))
                    path.Add(RemoveForbiddenCharacters(parentNode.DocumentName).Trim(new[] { '-' }));

                parentNode = parentNode.Parent;
            }
            path.Reverse();
            e.Node.DocumentUrlPath = string.Format("{0}", string.Join("/", path)).ToLower();
            e.Node.SetValue("DocumentUrlPath", string.Format("{0}", string.Join("/", path)).ToLower());
            e.Node.DocumentUseNamePathForUrlPath = false;
            e.Node.SetValue("DocumentUseNamePathForUrlPath", false);
            if (saveAfterChange)
                e.Node.Update();
        }

        private string RemoveForbiddenCharacters(string str)
        {
            var regEx = SettingsKeyInfoProvider.GetStringValue(string.Format("{0}.ForbiddenCharactersRegEx", SiteContext.CurrentSiteName));
            if (string.IsNullOrWhiteSpace(regEx)) return str;

            return Regex.Replace(str.Trim(), regEx, "-", RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Updates the navigation sub headings.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void UpdateNavigationSubHeadings(object sender, DocumentEventArgs e)
        {
            e.Node.DocumentMenuItemInactive = true;
            e.Node.DocumentMenuClass = "subheading";
            e.Node.Update();
        }

        /// <summary>
        /// Updates the navigation headings.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        private void UpdateNavigationHeadings(object sender, DocumentEventArgs e)
        {
            e.Node.DocumentMenuItemInactive = true;
            e.Node.DocumentMenuClass = "heading";
            e.Node.Update();
        }

        /// <summary>
        /// Updates the program.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DocumentEventArgs"/> instance containing the event data.</param>
        /// <param name="saveAfterChange">if set to <c>true</c> [save after change].</param>
        private void UpdateProgram(object sender, DocumentEventArgs e, bool saveAfterChange = true)
        {
            var url = String.Format("{0}/{{province}}/{{city}}/{1}", CMSApp.CRC.TransformationHelper.GetRegionPath(e.Node.DocumentCulture), RemoveForbiddenCharacters(ValidationHelper.GetString(e.Node.GetValue("Title"), e.Node.NodeAlias))).ToLower();
            e.Node.DocumentUrlPath = url;
            e.Node.SetValue("DocumentUrlPath", url);
            if (saveAfterChange)
                e.Node.Update();
        }
    }
}
