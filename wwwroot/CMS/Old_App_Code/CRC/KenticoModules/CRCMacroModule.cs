using System;
using System.Collections.Generic;
using CMS.DocumentEngine;
using System.Web;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMS.Base;
using CMS.Localization;
using CMS.MacroEngine;
using CMS.Helpers;
using CMS.Taxonomy;

/// <summary>
/// Sample custom module class. Partial class ensures correct registration. For adding new methods, modify SampleModule inner class.
/// </summary>
[CRCMacroLoader]
[Obsolete]
public partial class CMSModuleLoader
{
    #region "Macro methods loader attribute"

    /// <summary>
    /// Attribute class ensuring correct initialization of methods in macro resolver. You do not need to modify this class.
    /// </summary>
    private class CRCMacroLoader : CMSLoaderAttribute
    {
        /// <summary>
        /// Registers module methods.
        /// </summary>
        public override void Init()
        {
            // -- Custom macro resolving
            MacroResolver.OnResolveCustomMacro += MacroResolver_OnResolveCRCMacro;
        }

        /// <summary>
        /// Resolves the custom macro
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void MacroResolver_OnResolveCRCMacro(object sender, MacroEventArgs e)
        {
            if (!e.Match)
            {
                // Add your custom macro evaluation
                TreeNode featuredNode;
                int documentId;
                TreeNode blogNode;

                switch (e.Expression.ToLower())
                {
                    case "categoryid":
                        var category = DocumentContext.CurrentDocument.CurrentCategory();
                        if (category != null)
                            e.Result = category.CategoryID;
                        e.Match = true;
                        break;
                    case "categorynewstitle":
                        e.Match = true;
                        category = DocumentContext.CurrentDocument.CurrentCategory();
                        e.Result = string.Empty;
                        if (category != null)
                        {
                            e.Result = string.Format("{0} - {1}", DocumentContext.CurrentDocument.DocumentName, category.LocalizedDisplayName());
                        }
                        else if (!string.IsNullOrWhiteSpace(DocumentContext.CurrentDocument.GetStringValue("NewsTitle", string.Empty)))
                        {
                            e.Result = DocumentContext.CurrentDocument.GetStringValue("NewsTitle", string.Empty);
                        }
                        else
                        {
                            e.Result = DocumentContext.CurrentDocument.DocumentName;
                        }
                        break;
                    case "categorydescription":
                        e.Result = string.Empty;
                        category = DocumentContext.CurrentDocument.CurrentCategory();
                        if (category != null)
                            e.Result = category.LocalizedDescription();
                        e.Match = true;
                        break;
                    case "isstoriespath":
                        if (!DocumentContext.CurrentDocument.NodeAliasPath.ToLower().Contains("newsroom"))
                        {
                            e.Result = "";
                        }
                        else
                        {
                            e.Result = "";
                        }
                        break;
                    case "isnotnews":
                        if (DocumentContext.CurrentDocument.NodeClassName.ToLower() != "crc.news")
                        {
                            e.Result = true;
                        }
                        else
                        {
                            e.Result = false;
                        }
                        break;
                    case "getcanonicallink":
                        if (DocumentContext.CurrentDocument.NodeAliasPath != "/Donate/Donate-Online/Donate-to-the-Canadian-Red-Cross")
                        {
                            PageInfo currentPage = DocumentContext.CurrentPageInfo;
                            e.Match = true;
                            // check if the current page is a linked doc.  If it is, obatin the URL of the document it's pointing to 
                            if (DocumentContext.CurrentDocument.IsLink)
                            {
                                e.Result = "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(TreePathUtils.GetDocumentUrl(DocumentContext.CurrentDocument.DocumentID)).ToLower() + "\" />";
                            }
                            else
                            {
                                string aliasPath = DocumentContext.CurrentDocument.NodeAliasPath;
                                e.Result = "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(RequestContext.CurrentRelativePath).ToLower() + "\" />";
                            }
                            if (currentPage.NodeAlias == "Home" || currentPage.DocumentUrlPath == "/default.aspx")
                            {
                                // e.Result = "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl("/") + "\" />";
                                e.Result = "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(RequestContext.CurrentRelativePath).ToLower() + "\" />";
                            }

                        }
                        break;
                    case "getcanonicalurl":
                        e.Match = true;
                        PageInfo thisPage = DocumentContext.CurrentPageInfo;
                        var relativeUrl = URLHelper.ResolveUrl(RequestContext.CurrentRelativePath).ToLower();
                        if ((thisPage.NodeAlias == "Home" || thisPage.DocumentUrlPath == "/default.aspx") && (thisPage.NodeLevel == 1))
                        {
                            relativeUrl = string.Empty;
                        }
                        e.Result = DocumentContext.CurrentDocument.DocumentCulture == "fr-CA"
                            ? "http://www.croixrouge.ca" + relativeUrl
                            : "http://www.redcross.ca" + relativeUrl;

                        var customtag = DocumentContext.CurrentDocument.GetStringValue("CanonicalTag", string.Empty);
                        e.Result = string.IsNullOrWhiteSpace(customtag) ? e.Result : customtag;
                        break;
                    case "getregioncode":
                        e.Match = true;
                        var ProvinceCode = CMSAppAppCode.Old_App_Code.CRC.Variables.RegionCode;
                        e.Result = ProvinceCode;
                        break;

                    case "rssfeedtitle":
                        e.Match = true;
                        e.Result = ResHelper.GetString(String.Format("CRC.RssTitle.{0}", DocumentContext.CurrentDocument.NodeAlias.Replace("-", string.Empty)));
                        break;
                    case "rssfeeddescription":
                        e.Result = ResHelper.GetString(String.Format("CRC.RssDescription.{0}", DocumentContext.CurrentDocument.NodeAlias.Replace("-", string.Empty)));
                        e.Match = true;
                        break;
                    case "blogpostpath":
                        e.Match = true;
                        e.Result = DocumentContext.CurrentDocument.ClassName.Equals("CRC.Page", StringComparison.OrdinalIgnoreCase) ? "/Blog/%" : "./%";
                        break;
                    case "blogimageurl":
                        e.Match = true;
                        var imageUrl = LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase)
                                       ? URLHelper.GetAbsoluteUrl("/crc/assets/logo-fr-fb.gif")
                                       : URLHelper.GetAbsoluteUrl("/crc/assets/header_logo-fb.gif");
                        var imageGuid = DocumentContext.CurrentDocument.GetGuidValue("BlogPostTeaser", Guid.Empty);
                        var image = AttachmentInfoProvider.GetAttachmentInfo(imageGuid, SiteContext.CurrentSiteName);
                        if (image != null && !string.IsNullOrWhiteSpace(image.AttachmentUrl))
                            imageUrl = URLHelper.GetAbsoluteUrl(image.AttachmentUrl);
                        else if (image != null)
                            imageUrl = URLHelper.GetAbsoluteUrl(string.Format("/CMSPages/GetFile.aspx?guid={0}", image.AttachmentGUID));

                        e.Result = imageUrl;
                        break;
                    case "bloghomepage":
                        e.Match = true;
                        blogNode = TransformationHelper.GetSubSiteNode();
                        e.Result = blogNode == null ? "/blog" : blogNode.DocumentUrlPath;
                        break;
                    case "bloglanguageurl":
                        e.Match = true;
                        var domain = TransformationHelper.GetAlternateLanguageDomain();
                        blogNode = TransformationHelper.GetSubSiteNode();
                        var langNode = TreeHelper.GetDocument(SiteContext.CurrentSiteName, blogNode == null ? "/blog" : blogNode.NodeAliasPath, LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase)
                                                               ? "en-CA"
                                                               : "fr-CA", false, blogNode == null ? "CMS.Blog" : blogNode.ClassName, true);
                        e.Result = string.Format("http://{0}{1}", domain, langNode.DocumentUrlPath);
                        break;
                    case "featuredpost":
                        e.Match = true;
                        featuredNode = new TreeProvider().SelectSingleNode(ValidationHelper.GetGuid(CMSApp.CRC.TransformationHelper.GetSubSiteNode().GetStringValue("FeaturedPost", string.Empty), Guid.Empty), LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName);
                        documentId = featuredNode == null || HideFeaturedPost ? 0 : featuredNode.DocumentID;
                        e.Result = string.Format("DocumentID = {0}", documentId);
                        break;
                    case "excludefeaturedpost":
                        e.Match = true;
                        featuredNode = new TreeProvider().SelectSingleNode(ValidationHelper.GetGuid(CMSApp.CRC.TransformationHelper.GetSubSiteNode().GetStringValue("FeaturedPost", string.Empty), Guid.Empty), LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName);
                        documentId = featuredNode == null || HideFeaturedPost ? 0 : featuredNode.DocumentID;
                        e.Result = string.Format("DocumentID <> {0}", documentId);
                        break;
                    case "sitename":
                        e.Match = true;
                        var node = TransformationHelper.GetSubSiteNode(new List<string> { "cms.blog", "crctimeline.rootnode" });
                        if (node == null) e.Result = ResHelper.GetString("CRC.Master.SiteName");
                        if (node != null)
                            switch (node.ClassName.ToLower())
                            {
                                case "cms.blog":
                                    e.Result = ResHelper.GetString("CRC.Blog.SiteName");
                                    break;
                                case "crctimeline.rootnode":
                                    e.Result = ResourceStringHelper.GetString("CRC.Timeline.SiteName", defaultText: "Canadian Red Cross Timeline");
                                    break;
                                default:
                                    e.Result = ResHelper.GetString("CRC.Master.SiteName");
                                    break;
                            }
                        break;
                    case "getcustompagetitle":
                        // this macro replaces the pagetitle_orelse_name used in the Kentico Page Title settings section
                        e.Match = true;
                        string title = "";
                        if (DocumentContext.CurrentDocument != null)
                        {
                            if (DocumentContext.CurrentDocument.NodeAliasPath.ToLower().Contains("in-your-community"))
                            {

                                // if the page is greater than 2nd level, output the "title - province/location", else "title"
                                // CRC_responsive-914 Rules based Title tags in IYC
                                if (DocumentContext.CurrentDocument.NodeLevel > 2)
                                {
                                    // level 3 parent is the Province/Location we want to output, so traverse parents up, till level 3
                                    TreeNode parent = DocumentContext.CurrentDocument.Parent;
                                    while (parent.NodeLevel > 2)
                                    {
                                        parent = parent.Parent;
                                    }
                                    string pageTitle = GetPageTitle(DocumentContext.CurrentDocument, false);
                                    title = pageTitle + " - " + ResHelper.LocalizeString(parent.DocumentName);
                                }
                                else
                                {
                                    title = GetPageTitle(DocumentContext.CurrentDocument, true, true);
                                }

                            }
                            else
                            {
                                title = GetPageTitle(DocumentContext.CurrentDocument, true, true);

                            }
                        }

                        var pageNumber = QueryHelper.GetInteger("page", 0);

                        if (pageNumber > 1)
                            title = string.Format("{0} ({1} {2})", title, ResourceStringHelper.GetString("CRC.Page", defaultText: "Page"), pageNumber);

                        e.Result = title;
                        break;
                }
            }
        }

        private bool HideFeaturedPost
        {
            get
            {
                return !string.IsNullOrWhiteSpace(QueryHelper.GetString("tag", string.Empty))
                    || !string.IsNullOrWhiteSpace(QueryHelper.GetString("category", string.Empty))
                    || !string.IsNullOrWhiteSpace(QueryHelper.GetString("author", string.Empty))
                    || !DocumentContext.CurrentDocument.ClassName.Equals("CRC.Page", StringComparison.OrdinalIgnoreCase);
            }
        }

        private string GetPageTitle(TreeNode contentDoc, bool isUseMetaTitleIfExists, bool includeCategory = false)
        {
            string title = DocumentContext.CurrentPageInfo.DocumentPageTitle;
            if (title == "" || isUseMetaTitleIfExists == false) title = contentDoc.DocumentName;

            if (includeCategory && contentDoc.CurrentCategory() != null &&
                !string.IsNullOrWhiteSpace(contentDoc.CurrentCategory().LocalizedDisplayName()))
                title = string.Format("{0} - {1}", title, contentDoc.CurrentCategory().LocalizedDisplayName());

            return title;
        }
    }

    #endregion
}
