﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CMS.Base;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.MacroEngine;
using CMS.SiteProvider;
using CMS.Taxonomy;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC;


[MacroNamespaceLoader]
public partial class CMSModuleLoader
{
    /// <summary>
    /// Attribute class that ensures the registration of custom macro namespaces.
    /// </summary>
    private class MacroNamespaceLoaderAttribute : CMSLoaderAttribute
    {
        /// <summary>
        /// Called automatically when the application starts.
        /// </summary>
        public override void Init()
        {
            // Registers "CustomNamespace" into the macro engine
            MacroContext.GlobalResolver.SetNamedSourceData("CRCMacroResolver", CRCMacroResolver.Instance);
            MacroContext.GlobalResolver.SetNamedSourceData("CRCMacroMethods", CRCMacroMethods.Instance);
        }
    }
}

[Extension(typeof(CustomMacroFields))]
public class CRCMacroResolver : MacroNamespace<CRCMacroResolver>
{
}

public class CustomMacroFields : MacroFieldContainer
{
    protected override void RegisterFields()
    {
        base.RegisterFields();
        RegisterField(new MacroField("CategoryId", () =>
            {
                var category = DocumentContext.CurrentDocument.CurrentCategory();
                return category != null ? category.CategoryID : 0;
            }));

        RegisterField(new MacroField("ProvinceCategory", () => DocumentContext.CurrentDocument.ProvinceCategory()));

        RegisterField(new MacroField("CategoryNewsTitle", () =>
            {
                var category = DocumentContext.CurrentDocument.CurrentCategory();
                if (category != null)
                    return string.Format("{0} - {1}", DocumentContext.CurrentDocument.DocumentName, category.LocalizedDisplayName());

                if (!string.IsNullOrWhiteSpace(DocumentContext.CurrentDocument.GetStringValue("NewsTitle", string.Empty)))
                    return DocumentContext.CurrentDocument.GetStringValue("NewsTitle", string.Empty);

                return DocumentContext.CurrentDocument.DocumentName;
            }));

        RegisterField(new MacroField("CategoryDescription", () =>
            {
                var category = DocumentContext.CurrentDocument.CurrentCategory();
                return category != null ? category.LocalizedDescription() : string.Empty;
            }));

        RegisterField(new MacroField("IsStoriesPath", () => !DocumentContext.CurrentDocument.NodeAliasPath.ToLower().Contains("newsroom")));

        RegisterField(new MacroField("IsNotNews", () => DocumentContext.CurrentDocument.NodeClassName.ToLower() != "crc.news"));

        RegisterField(new MacroField("GetCanonicalLink", () =>
            {
                if (DocumentContext.CurrentDocument.NodeAliasPath.Equals("/Donate/Donate-Online/Donate-to-the-Canadian-Red-Cross", StringComparison.OrdinalIgnoreCase))
                    return string.Empty;


                var currentPage = DocumentContext.CurrentPageInfo;

                // check if the current page is a linked doc.  If it is, obatin the URL of the document it's pointing to 
                if (DocumentContext.CurrentDocument.IsLink)
                    return "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(TreePathUtils.GetDocumentUrl(DocumentContext.CurrentDocument.DocumentID)).ToLower() + "\" />";

                var relativeUrl = RequestContext.CurrentRelativePath;
                if (QueryHelper.GetInteger("page", 0) > 0)
                    relativeUrl = RequestContext.CurrentURL;

                if (currentPage.NodeAlias == "Home" || currentPage.DocumentUrlPath == "/default.aspx")
                    return "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(relativeUrl).ToLower() + "\" />";

                return "<link rel=\"canonical\" href=\"" + URLHelper.GetAbsoluteUrl(relativeUrl).ToLower() + "\" />";
            }));

        RegisterField(new MacroField("GetRegionCode", () => CMSAppAppCode.Old_App_Code.CRC.Variables.RegionCode));
        RegisterField(new MacroField("RssFeedTitle", () => ResourceStringHelper.GetString(String.Format("CRC.RssTitle.{0}", DocumentContext.CurrentDocument.NodeAlias.Replace("-", string.Empty)), defaultText: DocumentContext.CurrentDocument.DocumentName)));

        RegisterField(new MacroField("RssFeedDescription", () => ResourceStringHelper.GetString(String.Format("CRC.RssDescription.{0}", DocumentContext.CurrentDocument.NodeAlias.Replace("-", string.Empty)), defaultText: DocumentContext.CurrentDocument.DocumentName)));

        RegisterField(new MacroField("BlogPostPath", () => DocumentContext.CurrentDocument.ClassName.Equals("CRC.Page", StringComparison.OrdinalIgnoreCase) ? "/Blog/%" : "./%"));

        RegisterField(new MacroField("BlogImageUrl", () =>
            {
                var imageUrl = LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase)
                                      ? URLHelper.GetAbsoluteUrl("/crc/assets/logo-fr-fb.gif")
                                      : URLHelper.GetAbsoluteUrl("/crc/assets/header_logo-fb.gif");
                var imageGuid = DocumentContext.CurrentDocument.GetGuidValue("BlogPostTeaser", Guid.Empty);
                var image = AttachmentInfoProvider.GetAttachmentInfo(imageGuid, SiteContext.CurrentSiteName);
                if (image != null && !string.IsNullOrWhiteSpace(image.AttachmentUrl))
                    imageUrl = URLHelper.GetAbsoluteUrl(image.AttachmentUrl);
                else if (image != null)
                    imageUrl = URLHelper.GetAbsoluteUrl(string.Format("/CMSPages/GetFile.aspx?guid={0}", image.AttachmentGUID));

                return imageUrl;
            }));

        RegisterField(new MacroField("GetCanonicalUrl", () =>
        {
            var relativeUrl = URLHelper.ResolveUrl(RequestContext.CurrentRelativePath).ToLower();
            if ((DocumentContext.CurrentPageInfo.NodeAlias == "Home" || DocumentContext.CurrentPageInfo.DocumentUrlPath == "/default.aspx") && (DocumentContext.CurrentPageInfo.NodeLevel == 1))
            {
                relativeUrl = string.Empty;
            }

            if (QueryHelper.GetInteger("page", 0) > 0)
                relativeUrl = RequestContext.CurrentURL;

            var canonical = DocumentContext.CurrentPageInfo.DocumentCulture == "fr-CA"
                ? "http://www.croixrouge.ca" + relativeUrl
                : "http://www.redcross.ca" + relativeUrl;
            var customtag = DocumentContext.CurrentDocument.GetStringValue("CanonicalTag", canonical);
            return string.IsNullOrWhiteSpace(customtag) ? canonical : customtag;
        }));


        RegisterField(new MacroField("BlogHomePage", () =>
            {
                var blogNode = TransformationHelper.GetSubSiteNode();
                return blogNode == null ? "/blog" : blogNode.DocumentUrlPath;
            }));

        RegisterField(new MacroField("BlogLanguageUrl", () =>
            {
                var domain = TransformationHelper.GetAlternateLanguageDomain();
                var blogNode = TransformationHelper.GetSubSiteNode();
                var langNode = TreeHelper.GetDocument(SiteContext.CurrentSiteName, blogNode == null ? "/blog" : blogNode.NodeAliasPath, LocalizationContext.PreferredCultureCode.Equals("fr-CA", StringComparison.OrdinalIgnoreCase)
                                                       ? "en-CA"
                                                       : "fr-CA", false, blogNode == null ? "CMS.Blog" : blogNode.ClassName, true);
                return string.Format("http://{0}{1}", domain, langNode.DocumentUrlPath);
            }));

        RegisterField(new MacroField("FeaturedPost", () =>
            {
                var featuredNode = new TreeProvider().SelectSingleNode(ValidationHelper.GetGuid(TransformationHelper.GetSubSiteNode().GetStringValue("FeaturedPost", string.Empty), Guid.Empty), LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName);
                var documentId = featuredNode == null || HideFeaturedPost ? 0 : featuredNode.DocumentID;
                return string.Format("DocumentID = {0}", documentId);
            }));

        RegisterField(new MacroField("ExcludeFeaturedPost", () =>
        {
            var featuredNode = new TreeProvider().SelectSingleNode(ValidationHelper.GetGuid(TransformationHelper.GetSubSiteNode().GetStringValue("FeaturedPost", string.Empty), Guid.Empty), LocalizationContext.PreferredCultureCode, SiteContext.CurrentSiteName);
            var documentId = featuredNode == null || HideFeaturedPost ? 0 : featuredNode.DocumentID;
            return string.Format("DocumentID <> {0}", documentId);
        }));

        RegisterField(new MacroField("SiteName", () =>
        {
            var node = TransformationHelper.GetSubSiteNode(new List<string> { "cms.blog", "crctimeline.rootnode" });
            if (node == null) return ResHelper.GetString("CRC.Master.SiteName");
            switch (node.ClassName.ToLower())
            {
                case "cms.blog":
                    return ResHelper.GetString("CRC.Blog.SiteName");
                case "crctimeline.rootnode":
                    return ResourceStringHelper.GetString("CRC.Timeline.SiteName", defaultText: "Canadian Red Cross Timeline");
                default:
                    return ResHelper.GetString("CRC.Master.SiteName");
            }
        }));

        // this macro replaces the pagetitle_orelse_name used in the Kentico Page Title settings section
        RegisterField(new MacroField("GetCustomPageTitle", GetCustomPageTitle));

        RegisterField(new MacroField("ShowTwitterOnPage", () => DocumentContext.CurrentDocument.GetInheritedFieldValue("ShowTwitter")));
        RegisterField(new MacroField("ShowPromotionalBannerOnPage", () => DocumentContext.CurrentDocument.GetInheritedFieldValue("ShowPromotionalBanner")));
        RegisterField(new MacroField("ShowRosBannerOnPage", () => DocumentContext.CurrentDocument.GetInheritedFieldValue("ShowRosBanner")));

        RegisterField(new MacroField("ShowMobileShare", () => DocumentContext.CurrentDocument.GetInheritedFieldValue("ShowMobileShare")));

        RegisterField(new MacroField("RelevantAppealUrl", GetRelevantAppealUrl));

        RegisterField(new MacroField("AutoPageDescription", GetPageDescription));
    }

    private string GetPageDescription()
    {
        var content = DocumentContext.CurrentDocument.DocumentContent == null || !DocumentContext.CurrentDocument.DocumentContent.ColumnNames.Any() ? string.Empty : DocumentContext.CurrentDocument.DocumentContent.ColumnNames.Select(
                columnName => DocumentContext.CurrentDocument.DocumentContent[columnName]).Join(string.Empty);

        content = GetFirstNonEmptyValue(new[]
                {
                    DocumentContext.CurrentDocument.GetStringValue("OGDescription", string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("NewsSummary",string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("Summary", string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("ShortDescription", string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("Description", string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("LongDescription", string.Empty),
                    DocumentContext.CurrentDocument.GetStringValue("BlogPostSummary", string.Empty),
                    content
                }, DocumentContext.CurrentDocument.DocumentName);

        return TextHelper.LimitLength(content, 160, wholeWords: true);
    }

    private string GetFirstNonEmptyValue(IEnumerable<string> values, string defaultValue)
    {
        foreach (var value in values.Where(value => !string.IsNullOrWhiteSpace(value)))
        {
            return HttpContext.Current.Server.HtmlEncode(HTMLHelper.StripTags(value).Replace("\"", "").Replace(Environment.NewLine, " "));
        }
        return HttpContext.Current.Server.HtmlEncode(HTMLHelper.StripTags(defaultValue).Replace("\"", ""));
    }

    private string GetCustomPageTitle()
    {
        var title = "";
        if (DocumentContext.CurrentDocument != null)
        {
            if (DocumentContext.CurrentDocument.NodeAliasPath.ToLower().Contains("in-your-community"))
            {
                // if the page is greater than 3rd level, output the "title - province/location", else "title"
                if (DocumentContext.CurrentDocument.NodeLevel > 2)
                {
                    // level 3 parent is the Province/Location we want to output, so traverse parents up, till level 3
                    TreeNode parent = DocumentContext.CurrentDocument.Parent;
                    while (parent.NodeLevel > 2)
                    {
                        parent = parent.Parent;
                    }
                    string pageTitle = GetPageTitle(DocumentContext.CurrentDocument, true);
                    title = pageTitle + " - " + ResHelper.LocalizeString(parent.DocumentName);
                }
                else
                {
                    title = GetPageTitle(DocumentContext.CurrentDocument, true, true);
                }
            }
            else
            {
                title = GetPageTitle(DocumentContext.CurrentDocument, true, true);

            }
        }

        var pageNumber = QueryHelper.GetInteger("page", 0);

        if (pageNumber > 1)
            title = string.Format("{0} ({1} {2})", title, ResourceStringHelper.GetString("CRC.Page", defaultText: "Page"), pageNumber);

        return title;
    }

    private bool HideFeaturedPost
    {
        get
        {
            return !string.IsNullOrWhiteSpace(QueryHelper.GetString("tag", string.Empty))
                || !string.IsNullOrWhiteSpace(QueryHelper.GetString("category", string.Empty))
                || !string.IsNullOrWhiteSpace(QueryHelper.GetString("author", string.Empty))
                || !DocumentContext.CurrentDocument.ClassName.Equals("CRC.Page", StringComparison.OrdinalIgnoreCase);
        }
    }

    private string GetPageTitle(TreeNode contentDoc, bool isUseMetaTitleIfExists, bool includeCategory = false)
    {
        var title = DocumentContext.CurrentPageInfo.DocumentPageTitle;
        if (title == "" || isUseMetaTitleIfExists == false)
            title = contentDoc.DocumentName;

        if (includeCategory && contentDoc.CurrentCategory() != null &&
            !string.IsNullOrWhiteSpace(contentDoc.CurrentCategory().LocalizedDisplayName()))
            title = string.Format("{0} - {1}", title, contentDoc.CurrentCategory().LocalizedDisplayName());

        return title;
    }

    /// <summary>
    /// The following function finds the top first appeal document who's category matches the current document's category.  
    /// </summary>
    /// <returns></returns>
    private string GetRelevantAppealUrl()
    {
        var donateUrl = LocalizationContext.PreferredCultureCode.Equals("fr-CA") ? "/faites-un-don" : "/donate";

        if (DocumentContext.CurrentDocument.ClassName.Equals("CRC.Appeal", StringComparison.OrdinalIgnoreCase))
            return string.Format("{0}#selected-appeal-{1}", donateUrl, DocumentContext.CurrentDocument.DocumentGUID);

        DataRow displayedAppeal = null;

        const int regionID = 5;
        const int topicID = 7;
        const int eventID = 59;
        var topicMatch = false;
        var eventMatch = false;
        var provinceCode = QueryHelper.GetString("province", string.Empty);
        DataSet releventAppealDocs;

        var categoryQueryParamRegion = new QueryDataParameters { { "@Lang", DocumentContext.CurrentDocumentCulture.CultureCode } };

        if (string.IsNullOrWhiteSpace(provinceCode))
        {
            releventAppealDocs = CacheHelper.Cache(() =>
            {
                categoryQueryParamRegion.Add("@DocumentID", DocumentContext.CurrentDocument.DocumentID);
                return ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByCurrentDocumentCategory",
                                                     categoryQueryParamRegion, null, null);
            }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("CRC.Appeal.SelectByCurrentDocumentCategory_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, DocumentContext.CurrentDocument.DocumentID)));
        }
        else
        {
            releventAppealDocs = CacheHelper.Cache(() =>
            {
                categoryQueryParamRegion.Add("@CategoryName", provinceCode);
                return ConnectionHelper.ExecuteQuery("CRC.Appeal.SelectByProvince", categoryQueryParamRegion, null, null);
            }, new CacheSettings(Variables.CacheTimeInMinutes, string.Format("CRC.Appeal.SelectByProvince_{0}_{1}", DocumentContext.CurrentDocumentCulture.CultureCode, provinceCode)));
        }

        if (!DataHelper.DataSourceIsEmpty(releventAppealDocs))
        {
            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {

                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == eventID)
                {
                    displayedAppeal = appeal;
                    eventMatch = true;
                    break;
                }
            }

            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {

                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == topicID)
                {
                    if (eventMatch && ValidationHelper.GetInteger(displayedAppeal["DocumentID"], 0) == ValidationHelper.GetInteger(appeal["DocumentID"], 0))
                    {
                        displayedAppeal = appeal;
                        topicMatch = true;
                        break;
                    }
                }
                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && eventMatch == false)
                {
                    displayedAppeal = appeal;
                    break;
                }
            }
            foreach (DataRow appeal in releventAppealDocs.Tables[0].Rows)
            {
                if (ValidationHelper.GetBoolean(appeal["IsActive"], false) && ValidationHelper.GetInteger(appeal["CategoryParentID"], 0) == regionID)
                {

                    if (topicMatch && ValidationHelper.GetInteger(displayedAppeal["DocumentID"], 0) == ValidationHelper.GetInteger(appeal["DocumentID"], 0))
                    {
                        displayedAppeal = appeal;
                        break;
                    }
                    if (topicMatch == false & eventMatch == false)
                    {
                        displayedAppeal = appeal;
                        break;
                    }
                }
            }
        }
        return displayedAppeal == null
            ? donateUrl
            : string.Format("{0}#selected-appeal-{1}", donateUrl, displayedAppeal["DocumentGUID"]); //DocumentURLProvider.GetUrl(ValidationHelper.GetString(displayedAppeal["NodeAliasPath"], string.Empty), ValidationHelper.GetString(displayedAppeal["DocumentUrlPath"], string.Empty));
    }
}

[Extension(typeof(CustomMacroMethods))]
public class CRCMacroMethods : MacroNamespace<CRCMacroMethods>
{
}

public class CustomMacroMethods : MacroMethodContainer
{
    [MacroMethod(typeof(string), "Get Inherited Field Value", 1)]
    [MacroMethodParam(0, "param1", typeof(string), "Field name")]
    public static object GetInheritedFieldValue(EvaluationContext context, params object[] parameters)
    {
        switch (parameters.Length)
        {
            case 1:
                // Overload with one parameter
                return DocumentContext.CurrentDocument.GetInheritedFieldValue(ValidationHelper.GetString(parameters[0], string.Empty));

            default:
                // No other overloads are supported
                throw new NotSupportedException();
        }
    }

    [MacroMethod(typeof(string), "Limit text to n number of characters", 1)]
    [MacroMethodParam(0, "param1", typeof(string), "text/content")]
    [MacroMethodParam(1, "param2", typeof(int), "Number of characters")]
    [MacroMethodParam(2, "param3", typeof(bool), "Break on word")]
    public static object LimitText(EvaluationContext context, params object[] parameters)
    {
        if (parameters.Length < 1) return string.Empty;

        var content = HTMLHelper.StripTags(ValidationHelper.GetString(parameters[0], string.Empty), false);

        int charLength;
        switch (parameters.Length)
        {
            case 2:
                charLength = ValidationHelper.GetInteger(parameters[1], 0);
                if (charLength < content.Length && charLength > 0)
                    return TextHelper.LimitLength(content, charLength);
                break;
            case 3:
                charLength = ValidationHelper.GetInteger(parameters[1], 0);
                var breakOnWord = ValidationHelper.GetBoolean(parameters[2], false);
                if (charLength < content.Length && charLength > 0)
                    return TextHelper.LimitLength(content, charLength, null, breakOnWord);

                break;
            default:
                return parameters[0];
        }

        return string.Empty;
    }

    [MacroMethod(typeof(string), "Get localized string value", 1)]
    [MacroMethodParam(0, "param1", typeof(string), "UI string key")]
    [MacroMethodParam(1, "param2", typeof(string), "Default text if UI string doesn't exist")]
    [MacroMethodParam(2, "param3", typeof(string), "Culture code")]
    public static object ResString(EvaluationContext context, params object[] parameters)
    {
        if (parameters.Length < 1) return string.Empty;
        var contents = parameters.Select(item => ValidationHelper.GetString(item, string.Empty)).ToList();

        switch (contents.Count())
        {
            case 1:
                return ResourceStringHelper.GetString(contents[0]);
            case 2:
                return ResourceStringHelper.GetString(contents[0], defaultText: contents[1]);
            case 3:
                return ResourceStringHelper.GetString(contents[0], defaultText: contents[1], cultureCode: contents[2]);
            default:
                return parameters[0];
        }
    }
}