﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.Base;
using CMS.EventLog;
using CMS.Newsletters;
using CMS.SiteProvider;
using CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu;

[PreCacheMobileMenuModule]
public partial class CMSModuleLoader
{
    public class PreCacheMobileMenuModule : CMSLoaderAttribute
    {
        public override void Init()
        {
            base.Init();
            ApplicationEvents.PostStart.Execute += (sender, args) =>
            {
                var supportedCultures = new List<string> { "en-CA", "fr-CA" };
                supportedCultures.ForEach(lang =>
                {
                    var navOptions = new NavigationRequestOptions
                    {
                        Path = "/",
                        OrderByCondition = "NodeOrder ASC",
                        ClassNames = new List<string> { "CRC.Page", "CRC.Appeal", "CRC.Province", "CRC.IYCBranchFolder", "CMS.Blog", "CRC.Quiz" },
                        BackButtonResourceString = "crc.nav.back",
                        CheckPermissions = false,
                        UseCustomFields = false,
                        Lang = lang,
                        SiteName = SiteContext.CurrentSiteName
                    };
                    NavigationService.BuildNav(navOptions);
                });

            };
        }
    }
}

/*
 *  var options = {
            path: path,
            orderByCondition: "NodeOrder ASC",
            //andWhereCondition: ["ExcludeInMobileNav <> 1"],
            orWhereCondition: [],
            classNames: ["CRC.Page", "CRC.Appeal", "CRC.Province", "CRC.IYCBranchFolder", "CMS.Blog", "CRC.Quiz"],
            backButtonResourceString: 'crc.nav.back',
            checkPermissions: false,
            useCustomFields: false,
            lang: lang
         };
*/
