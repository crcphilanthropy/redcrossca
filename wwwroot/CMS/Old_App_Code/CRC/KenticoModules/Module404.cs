using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using CMS.EventLog;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMS.Base;
using CMS.Helpers;
using CMS.Localization;
using CMS.URLRewritingEngine;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;

[Module404]
public partial class CMSModuleLoader
{
    public class Module404 : CMSLoaderAttribute
    {
        const string BeginsWithPathMatch = "(^{0})(/)+.*.*";
        private const string ExactPathMatch = "^({0})$";
        public override void Init()
        {
            base.Init();
            RequestEvents.End.Execute += delegate
            {
                ErrorRedirect();
            };

        }

        private void ErrorRedirect()
        {
            try
            {
                if (HttpContext.Current.Response.StatusCode < 400) return;

                var paths = new List<IErrorRedirectItem>();
                if (HttpContext.Current.Response.StatusCode < 500)
                {
                    paths.AddRange(CustomTableHelper.GetItems<ErrorRedirectItem>("CRC.ErrorRedirects", "StatusCode = 404 OR StatusCode = -1"));
                    paths.AddRange(CustomTableHelper.GetItems<ArchivePageRedirectItem>("CRC.ArchivePageRedirects"));
                }
                else if (HttpContext.Current.Response.StatusCode >= 500)
                {
                    paths.AddRange(CustomTableHelper.GetItems<ErrorRedirectItem>("CRC.ErrorRedirects", "StatusCode = 500 OR StatusCode = -1"));
                }

                paths.Where(item => item.Path.Equals("/")).ToList().ForEach(item =>
                {
                    item.Order = 999999;
                });

                paths = paths.OrderBy(item => item.Order).ToList();

                var currentItem = paths.FirstOrDefault(item => Regex.IsMatch(RequestContext.CurrentRelativePath, string.Format(ExactPathMatch, item.Path), RegexOptions.IgnoreCase) && item.IsExactMatch) ??
                                  paths.FirstOrDefault(item => Regex.IsMatch(RequestContext.CurrentRelativePath, string.Format(BeginsWithPathMatch, item.Path), RegexOptions.IgnoreCase) && !item.IsExactMatch);

                if (currentItem == null) return;

                if (currentItem.IsTransfer)
                {
                    HttpContext.Current.Server.Transfer(ResHelper.GetString(currentItem.RedirectTo, LocalizationContext.PreferredCultureCode));
                }
                else
                {
                    HttpContext.Current.Response.Redirect(ResHelper.GetString(currentItem.RedirectTo, LocalizationContext.PreferredCultureCode), false);
                }

            }
            catch (Exception ex)
            {
                EventLogProvider.LogWarning("Module404", "CMSRequestEvents.End.After", ex, SiteContext.CurrentSiteID, null);
            }
        }
    }
}