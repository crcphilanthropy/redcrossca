﻿using CMS.Base;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.SiteProvider;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

[WistiaLoaderAttribiute]
public partial class CMSModuleLoader
{
    private class WistiaLoaderAttribiute : CMSLoaderAttribute
    {
        public override void Init()
        {
            // Assigns a handler to the Insert.Before event for the DocumentEvents
            // This event occurs before the creation of every new video document
            DocumentEvents.Insert.Before += Insert_Before;
            WorkflowEvents.SaveVersion.Before += SaveVersion_Before;
        }

        private void SaveVersion_Before(object sender, WorkflowEventArgs e)
        {
            var node = e.Document;
            var classname = node.ClassName;

            switch (classname.ToLower())
            {
                case "crc.videodetail":
                    using (var client = new HttpClient())
                    {
                        var tempurl = "{0}{1}{2}";
                        try
                        {
                            if (node != null)
                            {
                                //get document embed id
                                var embedId = ValidationHelper.GetString(node.GetValue("VideoEmbedID"), string.Empty);
                                var originalEmbedId = ValidationHelper.GetString(node.GetOriginalValue("VideoEmbedID"), string.Empty);
                                if (originalEmbedId != embedId)
                                {
                                    //get wistia api details from kentico settings & set up api url
                                    var token = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CRCSettings.WistiaAPIToken");
                                    var url = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CRCSettings.WistiaAPIURL");
                                    var apiurl = string.Format(tempurl, url, embedId + ".json?", "api_password=" + token);

                                    //query api for video details
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                    var response = client.GetAsync(apiurl).Result;
                                    //Get data from api and update document
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var details = response.Content.ReadAsStringAsync().Result;
                                        var tempdetail = JObject.Parse(details);
                                        var duration = ValidationHelper.GetDouble(tempdetail["duration"], double.NaN);
                                        var detailsUrl = ValidationHelper.GetString(tempdetail["assets"][0]["url"], string.Empty);

                                        // Ensures that a new version of the updated page is created
                                        // Checks out the page if using content locking
                                        //node.CheckOut();

                                        node.SetValue("VideoLength", duration);
                                        node.SetValue("DownloadLink", detailsUrl);

                                        //node.CreateNewVersion();
                                        //update page database
                                        //node.Update(true);
                                        // Creates a new version of the updated page
                                        // Checks in the page if using content locking
                                        // node.CheckIn();

                                        //log successful update
                                        EventLogProvider.LogInformation("DocumentEvents", "UpdateBefore", "Updated video documents: video length & DownloadLink; Guid = " + node.DocumentGUID);
                                    }
                                    else
                                    {
                                        e.Cancel();
                                        EventLogProvider.LogException("DocumentEvents", "UpdateBefore", null, 0, "Update of video documents: video length & DownloadLink failed; Guid = " + node.DocumentGUID);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventLogProvider.LogException("DocumentEvents", "EXCEPTION", ex, SiteContext.CurrentSite.SiteID, ex.Message);
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        private void Insert_Before(object sender, DocumentEventArgs e)
        {
            SaveVideosLengthAndDownloadLink(e);
        }


        private void SaveVideosLengthAndDownloadLink(DocumentEventArgs e)
        {
            var node = e.Node;
            var classname = node.ClassName;

            switch (classname.ToLower())
            {
                case "crc.videodetail":
                    using (var client = new HttpClient())
                    {
                        var tempurl = "{0}{1}{2}";
                        try
                        {
                            if (node != null)
                            {
                                //get document embed id
                                var embedId = ValidationHelper.GetString(node.GetValue("VideoEmbedID"), string.Empty);
                                //get wistia api details from kentico settings & set up api url
                                var token = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CRCSettings.WistiaAPIToken");
                                var url = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".CRCSettings.WistiaAPIURL");
                                var apiurl = string.Format(tempurl, url, embedId + ".json?", "api_password=" + token);

                                //query api for video details
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var response = client.GetAsync(apiurl).Result;
                                //Get data from api and update document
                                if (response.IsSuccessStatusCode)
                                {
                                    var details = response.Content.ReadAsStringAsync().Result;
                                    var tempdetail = JObject.Parse(details);
                                    var duration = ValidationHelper.GetDouble(tempdetail["duration"], double.NaN);
                                    var detailsUrl = ValidationHelper.GetString(tempdetail["assets"][0]["url"], string.Empty);
                                    //add duration and url to video details

                                    node.SetValue("VideoLength", duration);
                                    node.SetValue("DownloadLink", detailsUrl);
                                    //log successful update
                                    EventLogProvider.LogInformation("DocumentEvents", "InsertBefore", "Updated video documents: video length & DownloadLink; Guid = " + node.DocumentGUID);
                                }
                                else
                                {
                                    e.Cancel();
                                    EventLogProvider.LogException("DocumentEvents", "InsertBefore", null, 0, "Update of video documents: video length & DownloadLink failed; Guid = " + node.DocumentGUID);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            EventLogProvider.LogException("DocumentEvents", "EXCEPTION", ex, SiteContext.CurrentSite.SiteID, ex.Message);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
