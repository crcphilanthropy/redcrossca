﻿using System.Net.Http;
using System.Web;
using System.Web.Routing;
using System.Web.Http;
using System.Web.SessionState;
using CMS.Base;
using CMS.SettingsProvider;

/// <summary>
/// Responsible for loading up CH routes 
/// </summary>
[AppRoutingLoader]
public partial class CMSModuleLoader
{
    public class AppRoutingLoaderAttribute : CMSLoaderAttribute
    {
        public override void Init()
        {
            // Ideally we would set these up once here at application start .... 
            ApplicationEvents.PreInitialized.Execute += (sender, args) => EnsureRoutes();




            /* ... however clearing the CMS cache has the side effect of deleting the route
             * so we rebuild them here. Ideally we would hook into the cache clearing event
             * (can't find it) instead of checking here on every request. 
             */
            RequestEvents.Begin.Execute += (sender, args) => EnsureRoutes();
        }

        public void EnsureRoutes()
        {
            if (RouteTable.Routes["DefaultApiWithId"] == null)
                using (RouteTable.Routes.GetWriteLock())
                    RouteTable.Routes.MapHttpRoute("DefaultApiWithId", "Api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });

            if (RouteTable.Routes["DefaultApiWithAction"] == null)
                using (RouteTable.Routes.GetWriteLock())
                    RouteTable.Routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");

            if (RouteTable.Routes["DefaultApiGet"] == null)
                using (RouteTable.Routes.GetWriteLock())
                    RouteTable.Routes.MapHttpRoute("DefaultApiGet", "Api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) });

            if (RouteTable.Routes["DefaultApiPost"] == null)
                using (RouteTable.Routes.GetWriteLock())
                    RouteTable.Routes.MapHttpRoute("DefaultApiPost", "Api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post.ToString()) });

            if (HttpContext.Current.Request.Path.ToLower().StartsWith("/api/") || HttpContext.Current.Request.Path.ToLower().StartsWith("/timeline/api/"))
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
    }
}


