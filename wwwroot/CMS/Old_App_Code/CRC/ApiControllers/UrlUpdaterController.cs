﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using CMSApp.CRC;

namespace CMSAppAppCode.Old_App_Code.CRC.ApiControllers
{
    [Authorize]
    public class UrlUpdaterController : ApiController
    {
        public string GetLogItems()
        {
            return CustomTableHelper.GetItemValue("CRC.SerializedObjects", "Name = 'UrlUpdaterLogItems'", "Value", "ItemModifiedWhen Desc") as string;
        }
    }
}