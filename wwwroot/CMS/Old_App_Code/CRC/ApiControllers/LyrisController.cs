﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.ApiControllers
{
    public class LyrisController : ApiController
    {
        [HttpPost]
        [ValidateModel]
        [HandleException]
        public HttpResponseMessage Subscribe(SubscriptionItem<string> item)
        {
            var result = CRCEmailClientProvider.EmailClientResult.NotCreated;            
            if (CRCEmailClientProvider.IsInTestMode)
            {
                if (item.Email.Contains("@gmail.com"))
                    result = CRCEmailClientProvider.EmailClientResult.Subscribed;

                if (item.Email.Contains("@ecentricarts.com"))
                    result = CRCEmailClientProvider.EmailClientResult.Duplicate;

                if (item.Email.Contains("@test.com"))
                    throw new Exception("This is in test mode, '@test.com' simulates that an error has occurred.");
            }
            else
            {
                result = CRCEmailClientProvider.Subscribe(item);    
            }

            switch (result)
            {
                case CRCEmailClientProvider.EmailClientResult.Subscribed:
                    CRCEmailClientProvider.SaveToCustomTable(item);
                    return new HttpResponseMessage(HttpStatusCode.Created);
                case CRCEmailClientProvider.EmailClientResult.NotCreated:
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                case CRCEmailClientProvider.EmailClientResult.Duplicate:
                    CRCEmailClientProvider.SaveToCustomTable(item);
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                case CRCEmailClientProvider.EmailClientResult.Error:
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}