﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.ApiControllers
{
    public class EmailClientController : ApiController
    {
        [HttpPost]
        [ValidateModel]
        [HandleException]
        public HttpResponseMessage Subscribe(SubscriptionItem<string> item)
        {
            if (CRCEmailClientProvider.IsInTestMode)
            {
                item.ListKey = "91388018c19eded9f08615bc975f82ad";
            }

            var result = CRCEmailClientProvider.Subscribe(item);

            switch (result)
            {
                case CRCEmailClientProvider.EmailClientResult.Subscribed:
                    CRCEmailClientProvider.SaveToCustomTable(item);
                    return new HttpResponseMessage(HttpStatusCode.Created);
                case CRCEmailClientProvider.EmailClientResult.NotCreated:
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                case CRCEmailClientProvider.EmailClientResult.Duplicate:
                    CRCEmailClientProvider.SaveToCustomTable(item);
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                case CRCEmailClientProvider.EmailClientResult.Error:
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}