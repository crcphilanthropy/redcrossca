﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Interfaces;
using CMSAppAppCode.Old_App_Code.CRC.Models;
using CMSAppAppCode.Old_App_Code.CRC.Providers;

namespace CMSAppAppCode.Old_App_Code.CRC.ApiControllers
{
    public class IYCController : ApiController
    {
        public IPageMenu GetProvince(string id)
        {
            var provinceProvider = new ProvinceProvider();
            var province =  provinceProvider.GetAllProvinces()
                    .FirstOrDefault(item => item.ProvinceCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            return province ?? provinceProvider.GetInYourCommunityDocument();
        }

        public IEnumerable<IService> GetServices()
        {
            return new ServiceProvider().Search(new ServiceSearchConfig
                {
                    MaxDistance = 100,
                    Latitude = 43.637713m,
                    Longitude = -79.397650m
                });
        }

        public IEnumerable<IBranch> GetBranches()
        {
            return new BranchProvider().Search(43.637713m, -79.397650m, 100);
        }

        /// <summary>
        /// Return Sub-Categories base on Category Guid
        /// </summary>
        /// <param name="categoryGuid">Category Guid</param>
        /// <returns>returns IEnumerable of IPageMenu</returns>
        public IEnumerable<IPageMenu> GetSubCategories(Guid categoryGuid)
        {
            return new ServiceCategoryProvider().GetSubCategories(categoryGuid).OrderBy(item => item.NodeOrder);
        }

        public string GetIYCLogItems()
        {
            return CustomTableHelper.GetItemValue("CRC.SerializedObjects", "Name = 'IYCLogItems'", "Value", "ItemModifiedWhen Desc") as string;
        }

        public string GetIYCImportStats()
        {
            return CustomTableHelper.GetItemValue("CRC.SerializedObjects", "Name = 'IYCImportStats'", "Value", "ItemModifiedWhen Desc") as string;
        }
    }
}