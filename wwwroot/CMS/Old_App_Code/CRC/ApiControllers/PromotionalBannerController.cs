﻿using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using CMS.Helpers;
using CMSAppAppCode.Old_App_Code.CRC.Models;

namespace CMSAppAppCode.Old_App_Code.CRC.ApiControllers
{
    public class PromotionalBannerController : ApiController
    {
        // POST api/<controller>
        public void Post([FromUri]string path, [FromUri]int hidePromotionNDays, [FromUri]string cookieKey = "")
        {
            SessionHelper.SetValue("PromoBannerClosed", true);
            var serializer = new JavaScriptSerializer();

            if (string.IsNullOrWhiteSpace(cookieKey))
                cookieKey = "promo_banner";

            var cookie = HttpContext.Current.Request.Cookies[cookieKey];

            if (cookie == null) return;
            var promoCookie = serializer.Deserialize<PromotionalCookie>(cookie.Value);
            promoCookie.CloseCounter += 1;
            cookie.Expires = promoCookie.DateTime.AddDays(hidePromotionNDays);
            cookie.Value = serializer.Serialize(promoCookie);

            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}