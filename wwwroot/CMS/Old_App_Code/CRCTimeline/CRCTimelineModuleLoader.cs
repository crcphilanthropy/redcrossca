﻿using CMS.Base;
using CMS.MacroEngine;


[CRCTimelineModuleLoader]
public partial class CMSModuleLoader
{
    private class CRCTimelineModuleLoader : CMSLoaderAttribute
    {
        public override void Init()
        {
            MacroContext.GlobalResolver.SetNamedSourceData("CRCTimeline", CRCTimelineNamespace.Instance);
        }
    }
}
