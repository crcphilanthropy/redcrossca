﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.SiteProvider;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Providers
{
    public class TimelineProvider
    {
        private static int CacheMinutes
        {
            get { return CMS.DataEngine.SettingsKeyInfoProvider.GetIntValue("CacheTimeInMinutes"); }
        }

        private static string Language
        {
            get
            {
                var toReturn = CMSHttpContext.Current.Request.QueryString["Lang"];
                return string.IsNullOrWhiteSpace(toReturn) ? LocalizationContext.PreferredCultureCode : toReturn;
            }
        }

        private static string SiteName
        {
            get
            {
                var toReturn = CMSHttpContext.Current.Request.QueryString["SiteName"];
                return string.IsNullOrWhiteSpace(toReturn) ? SiteContext.CurrentSiteName : toReturn;
            }
        }

        private static CMSCacheDependency CacheDependency
        {
            get
            {
                return CacheHelper.GetCacheDependency(new List<string>
                    {
                        string.Format("nodes|{0}|crctimeline.timelineitem|all", SiteName).ToLower(), 
                        string.Format("nodes|{0}|crctimeline.timelinetopic|all", SiteName).ToLower(), 
                        string.Format("nodes|{0}|crctimeline.supportingasset|all", SiteName).ToLower(), 
                        "customtableitem.crctimeline.timelineitemtypes|all"
                    });
            }
        }

        public static IEnumerable<Guid> GetAvailableItemTypes(IEnumerable<Guid> topics)
        {
            return TimelineItems.Where(item => item.Topics.Any(topics.Contains)).Select(item => item.ItemType).Distinct();
        }

        public static IEnumerable<Guid> GetAvailableTopics(IEnumerable<Guid> itemTypes)
        {
            var timelineItems = TimelineItems.Where(item => itemTypes.Contains(item.ItemType)).Select(item => item.Topics);

            var topics = new ConcurrentBag<Guid>();
            Parallel.ForEach(timelineItems, items => Parallel.ForEach(items, topics.Add));
            return topics.Distinct();
        }

        public static IEnumerable<TimelineItem> GetFilteredItems(IList<Guid> topics, IList<Guid> itemTypes)
        {
            topics = topics.Where(item => !item.Equals(Guid.Empty)).ToList();
            itemTypes = itemTypes.Where(item => !item.Equals(Guid.Empty)).ToList();

            return TimelineItems.Where(item => (item.Topics.Any(topics.Contains) || !topics.Any()) && (itemTypes.Contains(item.ItemType) || !itemTypes.Any()));
        }

        public static IEnumerable<TimelineTopic> TimelineTopics
        {
            get
            {
                return CacheHelper.Cache<IEnumerable<TimelineTopic>>(cs =>
                {
                    var toReturn = new List<TimelineTopic>();
                    var ds = TreeHelper.GetDocuments(SiteName, "/%",
                                                     Language, false,
                                                     "CRCTimeline.TimelineTopic",
                                                     null, "NodeOrder ASC", -1, true, -1, "NodeGuid, DocumentGuid, CodeName, DisplayName, CodeName, NodeOrder");

                    if (DataHelper.DataSourceIsEmpty(ds)) return toReturn;

                    toReturn.AddRange(ds.Tables[0].Rows.Cast<DataRow>().Select(item => new TimelineTopic
                    {
                        DocumentGuid = ValidationHelper.GetGuid(item["DocumentGuid"], Guid.Empty),
                        CodeName = ValidationHelper.GetString(item["CodeName"], string.Empty),
                        DisplayName = ValidationHelper.GetString(item["DisplayName"], string.Empty),
                        NodeOrder = ValidationHelper.GetInteger(item["NodeOrder"], -1)
                    }));

                    return toReturn.OrderBy(i => i.NodeOrder);
                },
                new CacheSettings(CacheMinutes, string.Format("TimelineTopics_{0}_{1}", SiteName, Language)) { CacheDependency = CacheDependency });
            }
        }

        public static IEnumerable<TimelineItem> TimelineItems
        {
            get
            {
                return CacheHelper.Cache<IEnumerable<TimelineItem>>(cs =>
                {
                    var toReturn = new List<TimelineItem>();
                    DataSet ds = TreeHelper.GetDocuments(SiteName, "/%", Language, false, "CRCTimeline.TimelineItem", null, null, -1, true, -1,
                        "NodeId,DocumentId, DocumentGuid,DocumentUrlPath, NodeAlias, NodeAliasPath, Title, EditorialDate, ShortDescription, SortingDate, ItemType, Location, ThumbnailImage,ThumbnailImageAlt,Topics");

                    if (DataHelper.DataSourceIsEmpty(ds)) return toReturn;

                    toReturn.AddRange(ds.Tables[0].Rows.Cast<DataRow>().Select(item => new TimelineItem
                    {
                        DocumentGuid = ValidationHelper.GetGuid(item["DocumentGuid"], Guid.Empty),
                        Url = new Uri(URLHelper.GetAbsoluteUrl(DocumentURLProvider.GetUrl(ValidationHelper.GetString(item["NodeAliasPath"], string.Empty), ValidationHelper.GetString(item["DocumentUrlPath"], string.Empty), SiteName))).AbsolutePath,
                        AbsoluteUrl = URLHelper.GetAbsoluteUrl(DocumentURLProvider.GetUrl(ValidationHelper.GetString(item["NodeAliasPath"], string.Empty), ValidationHelper.GetString(item["DocumentUrlPath"], string.Empty), SiteName)),
                        Title = ValidationHelper.GetString(item["Title"], string.Empty),
                        SortingDate = ValidationHelper.GetDate(item["SortingDate"], DateTime.MinValue).Year,
                        EditorialDate = ValidationHelper.GetString(item["EditorialDate"], string.Empty),
                        ShortDescription = TextHelper.LimitLength(ValidationHelper.GetString(item["ShortDescription"], string.Empty), 190, wholeWords: true),
                        ItemType = ValidationHelper.GetGuid(item["ItemType"], Guid.Empty),
                        ThumbnailImage = new TimelineImage
                        {
                            AltText = ValidationHelper.GetString(item["ThumbnailImageAlt"], string.Empty),
                            FilePath = ValidationHelper.GetString(item["ThumbnailImage"], string.Empty).Replace("~", string.Empty)
                        },
                        Topics = ValidationHelper.GetString(item["Topics"], string.Empty).Split(new[] { ';', '|', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(topic => ValidationHelper.GetGuid(topic, Guid.Empty))
                    }));

                    return toReturn.OrderBy(item => item.SortingDate);
                },
                    new CacheSettings(CacheMinutes, string.Format("TimelineItems_{0}_{1}", SiteName, Language)) { CacheDependency = CacheDependency });
            }
        }

        public static IEnumerable<TimelineItemType> TimelineItemTypes
        {
            get
            {
                return CacheHelper.Cache(cs =>
                    {
                        var items = CustomTableHelper.GetItems<TimelineItemType>("CRCTimeline.ItemType", null, "DisplayName", "ItemGuid, DisplayName, CodeName, ItemOrder").ToList();
                        items.ForEach(item =>
                            {
                                item.DisplayName = ResourceStringHelper.LocalizeString(item.DisplayName, Language);
                                item.CodeName = ResourceStringHelper.LocalizeString(item.CodeName, Language);
                            });

                        return items.OrderBy(i => i.ItemOrder);
                    },
               new CacheSettings(CacheMinutes, string.Format("TimelineItemTypes_{0}_{1}", SiteName, Language)) { CacheDependency = CacheDependency });
            }
        }

    }
}