﻿using CMS.Base;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using System;

[CRCTimelineCustomDocumentEvents]
public partial class CMSModuleLoader
{
    private class CRCTimelineCustomDocumentEventsAttribute : CMSLoaderAttribute
    {
        CMS.DocumentEngine.TreeProvider treeProvider;

        public override void Init()
        {
            treeProvider = new TreeProvider();
            DocumentEvents.Insert.After += Document_Insert_After;
        }

        private void Document_Insert_After(object sender, DocumentEventArgs e)
        {
            if (e.Node.ClassName == "CRCTimeline.TimelineItem")
            {
                CreateSupportingAssetsFolder(e.Node);
            }
        }

        private void CreateSupportingAssetsFolder(TreeNode treeNode)
        {
            if (treeProvider != null)
            {
                var node = TreeNode.New("CRCTimeline.SupportingAssetsFolder", treeProvider);

                node.DocumentName = "Supporting Assets";
                node.DocumentCulture = treeNode.DocumentCulture;

                node.Insert(treeNode);
            }
        }
    }
}

