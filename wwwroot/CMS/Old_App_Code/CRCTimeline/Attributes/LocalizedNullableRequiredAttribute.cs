﻿using System;
using System.ComponentModel.DataAnnotations;
using CMSApp.CRC;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes
{
    public class LocalizedNullableRequiredAttribute : ValidationAttribute
    {
        public string DefaultText { get; set; }
        public string ResourceStringKey { get; set; }

        public override string FormatErrorMessage(string name)
        {
            ErrorMessage = ResourceStringHelper.GetString(ResourceStringKey, defaultText: DefaultText);
            return base.FormatErrorMessage(name);
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            if (value is string)
                return !string.IsNullOrWhiteSpace(value as string);

            if (value is int)
                return Convert.ToInt16(value) != 0;
            
            return true;
        }
    }
}