﻿using System.Text.RegularExpressions;
using CMSApp.CRC;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes
{
    public class LocalizedRegularExpressionAttribute : System.ComponentModel.DataAnnotations.RegularExpressionAttribute
    {
        public string DefaultText { get; set; }
        public string ResourceStringKey { get; set; }
        public RegexOptions RegexOptions { get; set; }

        public LocalizedRegularExpressionAttribute(string pattern) : base(pattern) { }

        public override string FormatErrorMessage(string name)
        {
            ErrorMessage = ResourceStringHelper.GetString(ResourceStringKey, defaultText: DefaultText);
            return base.FormatErrorMessage(name);
        }

        public override bool IsValid(object value)
        {
            return Regex.IsMatch(value as string ?? string.Empty, Pattern, RegexOptions);
        }
    }
}
