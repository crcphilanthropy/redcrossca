﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Script.Serialization;
using CMS.EventLog;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes
{
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception == null) return;
            var exception = actionExecutedContext.Exception;
            EventLogProvider.LogException(actionExecutedContext.ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionExecutedContext.ActionContext.ActionDescriptor.ActionName, exception);

            if (exception.InnerException != null)
                exception = exception.InnerException;

            var response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    ReasonPhrase = exception.Message,
                    Content = new StringContent(new JavaScriptSerializer().Serialize(new
                        {
                            exception.Message,
                            exception.Source
                        }))
                };
            actionExecutedContext.Response = response;
        }
    }
}
