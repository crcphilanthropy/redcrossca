﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using CMSApp.CRC;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes
{
    public class LocalizedRequiredAttribute : RequiredAttribute
    {
        public string DefaultText { get; set; }
        public string ResourceStringKey { get; set; }

        public override string FormatErrorMessage(string name)
        {
            ErrorMessage = ResourceStringHelper.GetString(ResourceStringKey, defaultText: DefaultText);
            return base.FormatErrorMessage(name);
        }
    }
}
