﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Providers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AttributeRouting.Web.Http;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.ApiControllers
{
    public class TimelineController : ApiController
    {
        private JsonMediaTypeFormatter _jsonFormatter;
        private JsonMediaTypeFormatter JsonFormatter
        {
            get
            {
                if (_jsonFormatter != null) return _jsonFormatter;

                _jsonFormatter = new JsonMediaTypeFormatter();
                var settings = _jsonFormatter.SerializerSettings;
                settings.Formatting = Formatting.None;
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                return _jsonFormatter;
            }
        }

        [GET("api/timeline/getall")]
        public HttpResponseMessage GetAll()
        {
            var timelineContext = new TimelineContext
                {
                    Items = TimelineProvider.TimelineItems,
                    Topics = TimelineProvider.TimelineTopics,
                    ItemTypes = TimelineProvider.TimelineItemTypes
                };

            timelineContext.Topics.ToList().ForEach(topic =>
                {
                    var itemTypes = TimelineProvider.GetAvailableItemTypes(new List<Guid> { topic.DocumentGuid });
                    topic.ItemTypes = itemTypes.Select(item => new ItemCount { Key = item, Count = TimelineProvider.TimelineItems.Count(x => x.ItemType.Equals(item)) }).Distinct();
                });

            timelineContext.ItemTypes.ToList().ForEach(itemType =>
                {
                    var topics = TimelineProvider.GetAvailableTopics(new List<Guid> { itemType.ItemGuid });
                    itemType.Topics = topics.Select(topic => new ItemCount { Key = topic, Count = TimelineProvider.TimelineItems.Count(x => x.Topics.Contains(topic)) }).Distinct();
                });


            return Request.CreateResponse(HttpStatusCode.OK, timelineContext, JsonFormatter);
        }
        [GET("api/timeline/getbyid")]
        public HttpResponseMessage GetById(Guid id)
        {
            var timelineItem = TimelineProvider.TimelineItems.FirstOrDefault(item => item.DocumentGuid.Equals(id));
            return timelineItem == null ? Request.CreateResponse(HttpStatusCode.NotFound) : Request.CreateResponse(HttpStatusCode.OK, timelineItem, JsonFormatter);
        }
        [GET("api/timeline/getbytopic")]
        public HttpResponseMessage GetByTopic(Guid topicGuid)
        {
            var timelineItems = TimelineProvider.TimelineItems.Where(item => item.Topics.Contains(topicGuid));
            return Request.CreateResponse(HttpStatusCode.OK, timelineItems, JsonFormatter);
        }
        [GET("api/timeline/getbytype")]
        public HttpResponseMessage GetByType(Guid itemType)
        {
            var timelineItems = TimelineProvider.TimelineItems.Where(item => item.ItemType.Equals(itemType));
            return Request.CreateResponse(HttpStatusCode.OK, timelineItems, JsonFormatter);
        }
        [GET("api/timeline/getavailabletopics")]
        public HttpResponseMessage GetAvailableTopics([FromUri] List<Guid> itemtypes)
        {
            var topics = TimelineProvider.GetAvailableTopics(itemtypes);
            var toReturn = topics.Select(topic => new ItemCount { Key = topic, Count = TimelineProvider.TimelineItems.Count(x => itemtypes.Contains(x.ItemType) && x.Topics.Contains(topic)) }).Distinct();
            return Request.CreateResponse(HttpStatusCode.OK, toReturn, JsonFormatter);
        }
        [GET("api/timeline/getavailableitemtypes")]
        public HttpResponseMessage GetAvailableItemTypes([FromUri] List<Guid> topics)
        {
            var itemTypes = TimelineProvider.GetAvailableItemTypes(topics);
            var toReturn = itemTypes.Select(item => new ItemCount { Key = item, Count = TimelineProvider.TimelineItems.Count(x => x.Topics.Any(topics.Contains) && x.ItemType.Equals(item)) }).Distinct();

            return Request.CreateResponse(HttpStatusCode.OK, toReturn, JsonFormatter);
        }
        [GET("api/timeline/getfiltereditems")]
        public HttpResponseMessage GetFilteredItems([FromUri] List<Guid> topics, [FromUri] List<Guid> itemTypes)
        {
            var filteredItems = TimelineProvider.GetFilteredItems(topics, itemTypes);
            return Request.CreateResponse(HttpStatusCode.OK, filteredItems.OrderBy(item => item.SortingDate), JsonFormatter);
        }
        [GET("api/timeline/getavailablefilters")]
        public HttpResponseMessage GetAvailableFilters([FromUri] List<Guid> topics, [FromUri] List<Guid> itemTypes)
        {
            var filteredItems = TimelineProvider.GetFilteredItems(topics, itemTypes).ToList();
            itemTypes = filteredItems.Select(item => item.ItemType).Distinct().ToList();

            var topicsBag = new ConcurrentBag<Guid>();
            Parallel.ForEach(filteredItems, item => Parallel.ForEach(item.Topics, topicsBag.Add));
            topics = topicsBag.Distinct().ToList();
            var toReturn = new Dictionary<string, IEnumerable<ItemCount>>
                {
                    {
                        "topics",
                        topics.Select(topic =>new ItemCount{Key = topic,Count = filteredItems.Count(x => x.Topics.Contains(topic))}).Distinct()
                    },
                    {
                        "itemtypes",
                        itemTypes.Select(item =>new ItemCount{Key = item,Count =filteredItems.Count(x => x.ItemType.Equals(item))}).Distinct()
                    }
                };

            return Request.CreateResponse(HttpStatusCode.OK, toReturn, JsonFormatter);
        }
    }
}