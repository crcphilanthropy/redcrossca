﻿using CMS.Base;
using CMS.MacroEngine;

[Extension(typeof(CustomMacroFields))]
[Extension(typeof(CustomMacroMethods))]
public class CRCTimelineNamespace : MacroNamespace<CRCTimelineNamespace> { }
