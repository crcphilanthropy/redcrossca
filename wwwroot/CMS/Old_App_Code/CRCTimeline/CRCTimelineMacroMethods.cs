﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CMS;
using CMS.MacroEngine;
using CMS.Helpers;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Providers;


[assembly: RegisterExtension(typeof(CRCTimelineMacroMethods), typeof(CRCTimelineNamespace))]
public class CRCTimelineMacroMethods : MacroMethodContainer
{
    [MacroMethod(typeof(string), "Resolve an Object Type Guid string to readble text", 1)]
    [MacroMethodParam(0, "objectTypeGuid", typeof(string), "String of Item Type Guid")]
    public static object ResolveItemItemType(EvaluationContext context, params object[] parameters)
    {
        var itemTypeGuid = ValidationHelper.GetGuid(parameters[0], Guid.Empty);

        if(itemTypeGuid != Guid.Empty) {

            var itemType = TimelineProvider.TimelineItemTypes.Where(i => i.ItemGuid == itemTypeGuid).FirstOrDefault();

            if (itemType != null)
            {
                string rootNode = ResHelper.GetString("crc.timeline.rootNodeUrl");
                string paramName = ResHelper.GetString("crc.timeline.locationContextKey.itemTypes");
                string url = string.Format("{0}/#/?&{1}={2}", rootNode, paramName, itemType.CodeName);
                string itemTypeUrl = string.Format(@"<a href=""{0}"" class=""cp-track"" data-event=""generic-event"" data-category=""timeline item"" data-action=""filter click:itemTypes"" data-label=""{1}"">{1}</a>", url, itemType.DisplayName);
                
                return itemTypeUrl;
            }

        }

        return string.Empty;
        
    }

    [MacroMethod(typeof(string), "Resolve a string of Topic Guids to readble text", 1)]
    [MacroMethodParam(0, "topicGuids", typeof(string), "String of Topics Guids")]
    public static object ResolveItemTopics(EvaluationContext context, params object[] parameters)
    {
        var urlString = "";

        var topicGuids = ValidationHelper.GetString(parameters[0], string.Empty).Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

        if (topicGuids.Length > 0)
        {
            foreach (var guid in topicGuids)
            {
                var topicGuid = ValidationHelper.GetGuid(guid, Guid.Empty);
                var topic = TimelineProvider.TimelineTopics.Where(t => t.DocumentGuid == topicGuid).FirstOrDefault();

                if (topic != null)
                {
                    string rootNode = ResHelper.GetString("crc.timeline.rootNodeUrl");
                    string paramName = ResHelper.GetString("crc.timeline.locationContextKey.topics");
                    string url = string.Format("{0}/#/?&{1}={2}", rootNode, paramName, topic.CodeName);
                    string itemTopicUrl = string.Format(@"<a href=""{0}"" class=""cp-track"" data-event=""generic-event"" data-category=""timeline item"" data-action=""filter click:topics"" data-label=""{1}"">{1}</a> ", url, topic.DisplayName);

                    urlString += itemTopicUrl;
                }
            }
        }

        return urlString;
    }

    [MacroMethod(typeof(string), "Resolve filters for a Hero Slide", 1)]
    [MacroMethodParam(0, "topics", typeof(string), "")]
    [MacroMethodParam(1, "itemTypes", typeof(string), "")]
    public static object ResolveSlideFilters(EvaluationContext context, params object[] parameters)
    {
        var topics = ValidationHelper.GetString(parameters[0], string.Empty).Replace("|", ",");
        var itemTypes = ValidationHelper.GetString(parameters[1], string.Empty).Replace("|", ",");

        var urlParams = "/#/?";

        if (topics.Length > 0)
        {
            urlParams += string.Format("&{0}={1}", ResHelper.GetString("crc.timeline.locationContextKey.topics"), topics);
        }

        if (itemTypes.Length > 0)
        {
            urlParams += string.Format("&{0}={1}", ResHelper.GetString("crc.timeline.locationContextKey.itemTypes"), itemTypes);
        }


        return urlParams;
    }

}

