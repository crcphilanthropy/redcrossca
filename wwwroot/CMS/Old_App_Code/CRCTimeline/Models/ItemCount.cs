﻿using System;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class ItemCount
    {
        public Guid Key { get; set; }        
        public int Count { get; set; }
    }
}