﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineItemType
    {
        public Guid ItemGuid { get; set; }
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<ItemCount> Topics { get; set; }

        [JsonIgnore]
        public int ItemOrder { get; set; }
    }
}