﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineTopic
    {
        public Guid DocumentGuid { get; set; }
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<ItemCount> ItemTypes { get; set; }

        [JsonIgnore]
        public TimelineAsset Image { get; set; }

        [JsonIgnore]
        public int NodeOrder { get; set; }
    }
}