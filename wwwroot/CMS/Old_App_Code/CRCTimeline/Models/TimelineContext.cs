﻿using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineContext
    {
        public IEnumerable<TimelineItem> Items { get; set; }
        public IEnumerable<TimelineTopic> Topics { get; set; }
        public IEnumerable<TimelineItemType> ItemTypes { get; set; }
    }
}