﻿using System;
using Newtonsoft.Json;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineAsset
    {
        public string AssetName { get; set; }
        public string AssetType { get; set; }
        public string AssetFile { get; set; }
        public string AssetAltText { get; set; }
        public string Caption { get; set; }

        [JsonIgnore]
        public Guid DocumentGuid { get; set; }

        [JsonIgnore]
        public int NodeParentId { get; set; }

        [JsonIgnore]
        public string NodeAliasPath { get; set; }
    }
}