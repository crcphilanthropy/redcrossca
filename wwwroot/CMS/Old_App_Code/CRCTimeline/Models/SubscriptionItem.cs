﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Attributes;
using CRC.EmailClient.Interfaces;
using System.Linq;
using CRC.EmailClient.Models;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class SubscriptionItem<T> : IEmailContact<T>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }


        //[LocalizedRequired(ResourceStringKey = "CRCTimeline.Subscription.EmailRequired", DefaultText = "Please enter a valid Email")]
        [LocalizedRegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])+", ResourceStringKey = "CRCTimeline.Subscription.EmailRequired", DefaultText = "Please enter a valid Email", RegexOptions = RegexOptions.IgnoreCase)]
        public string Email { get; set; }

        [LocalizedNullableRequiredAttribute(ResourceStringKey = "CRCTimeline.Subscription.ListRequired", DefaultText = "No subscription list was added for this email widget")]
        public T ListKey { get; set; }

        public bool LearnMore { get; set; }

        public string List
        {
            get
            {
                if (ListKey is string) return ListKey as string;

                var listKeys = new List<object>();
                if (ListKey is IEnumerable)
                {
                    listKeys.AddRange((ListKey as IEnumerable).Cast<object>());
                }
                return ListKey is IEnumerable ? string.Join(",", listKeys) : ListKey.ToString();
            }
        }

        public List<EmailContactCustomField> CustomFields { get; set; }

        public static SubscriptionItem<T> Convert<TV>(SubscriptionItem<TV> item, T listKeys)
        {
            return new SubscriptionItem<T>
                {
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    FullName = item.FullName,
                    Email = item.Email,
                    CustomFields = item.CustomFields,
                    LearnMore = item.LearnMore,
                    ListKey = listKeys
                };
        }
    }
}