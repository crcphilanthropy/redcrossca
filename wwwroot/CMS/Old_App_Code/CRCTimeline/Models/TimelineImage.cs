﻿namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineImage
    {
        public string FilePath { get; set; }
        public string AltText { get; set; }
    }
}