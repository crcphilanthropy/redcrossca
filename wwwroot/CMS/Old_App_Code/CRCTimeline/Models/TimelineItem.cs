﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.CRCTimeline.Models
{
    public class TimelineItem
    {
        public Guid DocumentGuid { get; set; }
        public string Url { get; set; }
        public string AbsoluteUrl { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string EditorialDate { get; set; }
        public int SortingDate { get; set; }
        public TimelineImage ThumbnailImage { get; set; }
        public Guid ItemType { get; set; }
        public IEnumerable<Guid> Topics { get; set; }
    }    
}