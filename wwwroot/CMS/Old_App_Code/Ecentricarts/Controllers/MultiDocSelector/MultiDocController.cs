﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.Taxonomy;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ecentricarts.Controllers.MultiDocSelector
{
    public class MultiDocController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetDocuments(string options)
        {
            var jsonFormatter = new JsonMediaTypeFormatter();
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.None;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var filter = JsonConvert.DeserializeObject<MdFilter>(options, jsonFormatter.SerializerSettings);

            var documents = GetDocuments(filter, false, filter.TopN > 0);

            return Request.CreateResponse(documents.Any() ? HttpStatusCode.OK : HttpStatusCode.NoContent, documents, jsonFormatter);
        }

        public static IOrderedEnumerable<MdDocument> GetDocuments(MdFilter filter, bool refrechCache = true, bool skipCache = false)
        {
            filter.SelectedDocuments = filter.SelectedDocuments ?? new List<Guid>();

            var columns = new List<string>
                {
                    "DocumentID",
                    "DocumentName",
                    "DocumentNamePath",
                    "DocumentUrlPath",
                    "DocumentCulture",
                    "NodeAliasPath",
                    "DocumentGuid",
                    "NodeGuid",
                    "ClassName",
                    "NodeOwnerFullName",
                    "DocumentLastPublished",
                    "DocumentModifiedWhen",
                    "(select top 1 stepname from CMS_WorkflowStep where StepID = DocumentWorkflowStepID) as WorkflowStepName"
                };

            columns.AddRange(filter.AdditionalFields);

            var documents = new ConcurrentBag<MdDocument>();
            Parallel.ForEach(filter.SiteNames, siteName =>
            {
                var site = SiteInfoProvider.GetSiteInfo(siteName);

                var ds = GetSiteDocuments(filter, site, columns, refrechCache, skipCache);

                if (DataHelper.DataSourceIsEmpty(ds)) return;

                Parallel.ForEach(ds.Tables.Cast<DataTable>(), dt => Parallel.ForEach(dt.Rows.Cast<DataRow>(), row =>
                {
                    var guid = ValidationHelper.GetGuid(row[filter.FieldToSave], ValidationHelper.GetGuid(row["NodeGUID"], Guid.Empty));
                    documents.Add(new MdDocument
                    {
                        DocumentName = ValidationHelper.GetString(row["DocumentName"], string.Empty),
                        DocumentNamePath = ValidationHelper.GetString(row["DocumentNamePath"], string.Empty),
                        DocumentUrl = URLHelper.GetAbsoluteUrl(DocumentURLProvider.GetUrl(ValidationHelper.GetString(row["NodeAliasPath"], string.Empty), ValidationHelper.GetString(row["DocumentUrlPath"], string.Empty), site.SiteName), site.DomainName).ToLower(),
                        DocumentGuid = guid,
                        SiteName = siteName,
                        DocumentCultures = ValidationHelper.GetString(row["DocumentCulture"], string.Empty).Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries),
                        Author = ValidationHelper.GetString(row["NodeOwnerFullName"], string.Empty),
                        Categories = filter.IncludeCategories ? CategoryInfoProvider.GetCategories(
                            "CategoryID in (Select dc.CategoryID From CMS_DocumentCategory dc where dc.DocumentID =" +row["DocumentID"]+")", null, -1).Select(category => ResHelper.LocalizeString(category.CategoryDisplayName, filter.Lang, false, true)).ToList() : new List<string>(),
                        ClassName = ValidationHelper.GetString(row["ClassName"], string.Empty),
                        Selected = filter.SelectedDocuments.Contains(guid),
                        WorkflowStep = ValidationHelper.GetString(row["WorkflowStepName"], "published"),
                        Order = filter.SelectedDocuments.IndexOf(guid) + 1,
                        CustomData = filter.AdditionalFields.Select(field => ValidationHelper.GetString(row[field], string.Empty)).Where(content => !string.IsNullOrWhiteSpace(content)),
                        PublishedDate = ValidationHelper.GetDateTime(row["DocumentLastPublished"], ValidationHelper.GetDateTime(row["DocumentModifiedWhen"], DateTime.Now))
                    });
                }));
            });

            return documents.OrderBy(item => item.Order).ThenBy(item => item.DocumentName);
        }

        private static DataSet GetSiteDocuments(MdFilter filter, SiteInfo site, IEnumerable<string> columns, bool refreshCache, bool skipCache)
        {
            var cacheformat = string.Format("{0}_{1}", filter.CacheKey, site.SiteName);
            var cacheKey = cacheformat;

            if (refreshCache)
                CacheHelper.Remove(cacheKey);

            if (skipCache)
                return string.IsNullOrWhiteSpace(filter.QueryName) ? GetData(filter, site.SiteName, columns) : GetDataFromCustomQuery(filter, site.SiteID, columns);

            return CacheHelper.Cache(() => string.IsNullOrWhiteSpace(filter.QueryName) ? GetData(filter, site.SiteName, columns) : GetDataFromCustomQuery(filter, site.SiteID, columns), new CacheSettings(10, cacheKey));


        }

        private static DataSet GetData(MdFilter filter, string siteName, IEnumerable<string> columns)
        {
            var query = DocumentHelper.GetDocuments()
                                   .Columns(columns)
                                   .Path(filter.Path)
                                   .Culture(filter.Lang)
                                   .CombineWithDefaultCulture(false)
                                   .NestingLevel(filter.MaxRelativeLevel)
                                   .Published(false)
                                   .OnSite(siteName)
                                   .TopN(filter.TopN)
                                   .OrderBy("DocumentName");

            if (filter.AdditionalFields.Any())
                query = query.Types(filter.ClassNames.ToArray());
            else
            {
                filter.Where = string.IsNullOrWhiteSpace(filter.Where)
                    ? "ClassName in ("+filter.ClassNames.Select(item => "'"+item+"'").Join(",")+")"
                    : "(ClassName in ("+filter.ClassNames.Select(item => "'"+item+"'").Join(",")+")) AND ("+filter.Where+")";
            }

            if (!string.IsNullOrWhiteSpace(filter.Where))
                query.WhereCondition = filter.Where;

            var toReturn = query.Execute();

            return toReturn;
        }

        private static DataSet GetDataFromCustomQuery(MdFilter filter, int siteId, IEnumerable<string> columns)
        {
            var where = new List<string>();
            where.Add(filter.Where);

            where.Add("NodeSiteID ="+ siteId+"");
            where.Add("NodeAliasPath LIKE"+ "'"+filter.Path+"'"+"");
            where.Add("ClassName in ("+filter.ClassNames.Select(item => "'"+item+"'").Join(",")+")");


            var query = new DataQuery(filter.QueryName)
                                   .TopN(filter.TopN)
                                   .OrderBy("DocumentName");

            if (!string.IsNullOrWhiteSpace(filter.Lang))
            {
                where.Add("DocumentCulture ="+ "'"+filter.Lang+"'"+"");
                query = query.Columns(columns);
            }

            query.WhereCondition = where.Where(item => !string.IsNullOrWhiteSpace(item)).Join(" AND ");

            return query.Execute();
        }

        public class MdDocument
        {
            public string DocumentName { get; set; }
            public string DocumentNamePath { get; set; }
            public string DocumentUrl { get; set; }
            public string SiteName { get; set; }
            public IEnumerable<string> DocumentCultures { get; set; }
            public Guid DocumentGuid { get; set; }
            public bool Selected { get; set; }
            public string WorkflowStep { get; set; }
            public string ClassName { get; set; }
            public string Author { get; set; }
            public IEnumerable<string> Categories { get; set; }
            public int Order { get; set; }
            public IEnumerable<string> CustomData { get; set; }
            public DateTime PublishedDate { get; set; }
        }

        public class MdFilter
        {
            public List<string> SiteNames { get; set; }
            public string Path { get; set; }
            public string Lang { get; set; }
            public string FieldToSave { get; set; }
            public List<string> ClassNames { get; set; }
            public List<Guid> SelectedDocuments { get; set; }
            public string Where { get; set; }
            public int MaxRelativeLevel { get; set; }
            public List<string> AdditionalFields { get; set; }

            public bool IncludeCategories { get; set; }

            public int TopN { get; set; }

            public string QueryName { get; set; }

            public string CacheKey {
                get {
                    return string.Format("mdSelector_{0}_{1}_{2}_{3}_{4}", Path, Lang, FieldToSave, Where, ClassNames.Join("_"));
                }
            }
        }
    }
}
