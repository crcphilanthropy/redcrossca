﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using CMS.Localization;
using CMS.Membership;
using CMS.SiteProvider;
using CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ecentricarts.Controllers.MobileMenu
{
    public class MenuController : ApiController
    {
        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(string options)
        {
            //set json response to camel case
            var jsonFormatter = new JsonMediaTypeFormatter();
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.None;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var navigationRequestOptions = JsonConvert.DeserializeObject<NavigationRequestOptions>(options, jsonFormatter.SerializerSettings);

            var mobileMenuService = new NavigationService(navigationRequestOptions)
            {
                SiteName = SiteContext.CurrentSiteName,
                IsAuthenticated = false
            };

            navigationRequestOptions.SiteName = SiteContext.CurrentSiteName;
            var navigationLevel = mobileMenuService.GetNavigationLevelAtPath();

            return Request.CreateResponse(HttpStatusCode.OK, navigationLevel, jsonFormatter);
        }

    }
}