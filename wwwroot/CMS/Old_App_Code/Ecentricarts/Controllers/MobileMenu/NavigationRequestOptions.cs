﻿using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu
{
    public class NavigationRequestOptions
    {
        public List<string> ClassNames { get; set; }
        public List<string> AndWhereCondition { get; set; }
        public List<string> OrWhereCondition { get; set; }
        public string OrderByCondition { get; set; }
        public string Path { get; set; }
        public bool CheckPermissions { get; set; }
        public string BackButtonResourceString { get; set; }
        public string Lang { get; set; }
        internal string SiteName { get; set; }

        private string _whereCondition;
        public string WhereCondition
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_whereCondition)) return _whereCondition;

                AndWhereCondition.Add("DocumentMenuItemHideInNavigation = 0 AND (DocumentWorkflowStepID > 1 OR DocumentWorkflowStepID IS NULL)");
                OrWhereCondition.Add(string.Format("({0})", string.Join(" AND ", AndWhereCondition)));

                return _whereCondition = string.Join(" OR ", OrWhereCondition);
            }
        }

        public bool UseCustomFields { get; set; }

        public NavigationRequestOptions()
        {
            ClassNames = new List<string>();
            AndWhereCondition = new List<string>();
            OrWhereCondition = new List<string>();
        }
    }
}