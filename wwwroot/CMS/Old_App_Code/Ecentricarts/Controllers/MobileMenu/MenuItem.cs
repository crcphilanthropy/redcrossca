﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Ecentricarts.Controllers.MobileMenu
{
    /// <summary>
    /// This class matches the JSON format expected by the front end of the menu
    /// </summary>
    [DataContract]
    public class MenuItem
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember] 
        public string title { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        [DataMember]
        public string url { get; set; }

        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [DataMember]
        public List<MenuItem> children { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [hide link].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hide link]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool hideLink { get; set; }
    }
}