﻿namespace CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu
{
    public class NavigationItem
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public bool HasChildren { get; set; }
        public int NodeOrder { get; set; }
        public NavigationItem()
        {
            HasChildren = false;
        }
    }
}