﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;

namespace CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu
{
    public class NavigationService
    {
        public string SiteName { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Columns { get; set; }
        public int CacheTime { get; set; }
        private readonly NavigationRequestOptions _navigationRequestOptions;

        /// <summary>
        /// Constructor to set defaults
        /// </summary>
        public NavigationService(NavigationRequestOptions options)
        {
            _navigationRequestOptions = options;
            SiteName = string.Empty;
            IsAuthenticated = false;
            //Columns = "DocumentName, DocumentUrlPath, NodeAliasPath, DocumentId, DocumentGuid, NodeGuid, DocumentMenuItemHideInNavigation, DocumentMenuCaption";
            CacheTime = 360;
        }

        /// <summary>
        /// Get the navigation level based on request parameters.
        /// </summary>
        /// <returns>Navigation level</returns>
        public NavigationLevel GetNavigationLevelAtPath()
        {
            var navigationLevel = CacheHelper.Cache(AquireNavigationViaCache, new CacheSettings(CacheTime, GetNavigationLevelCacheKey()));
            return navigationLevel;
        }

        /// <summary>
        /// Kentico cache management to set/get the navigation level and configure cache dependencies.
        /// </summary>
        /// <param name="cs">Cache settings</param>
        /// <returns>Navigation level</returns>
        public NavigationLevel AquireNavigationViaCache(CacheSettings cs)
        {
            var navigationLevel = BuildNavigationLevelAtPath();

            if ((navigationLevel != null) && cs.Cached)
            {
                var cacheKeys = string.Format("node|{0}|{1}|childnodes", SiteName, navigationLevel.CurrentNode.Path);
                cs.CacheDependency = CacheHelper.GetCacheDependency(cacheKeys.ToLower());
            }

            if (_navigationRequestOptions.Path.Equals("/") || _navigationRequestOptions.Path.Equals("/home", StringComparison.OrdinalIgnoreCase))
                BuildNav(_navigationRequestOptions);

            return navigationLevel;
        }

        /// <summary>
        /// Called when the requested navigation level is not in cache.  Runs SQL query on Tree view.
        /// </summary>
        /// <returns>NavigationLevel object containing the navigation items for this request.</returns>
        public NavigationLevel BuildNavigationLevelAtPath()
        {
            bool getParent;
            var navLevel = new NavigationLevel();
            MultiDocumentQuery query;
            do
            {
                query = GetQuery(_navigationRequestOptions.Path);
                getParent = false;
                //Add current Node and parent node for back button
                var currentNode = GetCurrentNode();
                if (currentNode != null)
                {
                    navLevel.CurrentNode = new NavigationItem
                    {
                        Title = string.IsNullOrWhiteSpace(currentNode.DocumentName) && navLevel.CurrentNode != null ? navLevel.CurrentNode.Title : GetDocumentDisplayTitle(currentNode),
                        HasChildren = false,
                        Path = currentNode.NodeAliasPath,
                        Url = DocumentURLProvider.GetUrl(currentNode.NodeAliasPath, currentNode.DocumentUrlPath)
                                                   .Trim(new[] { '~' })
                                                   .ToLower()
                    };

                    navLevel.ParentNode = null;
                    if (currentNode.Parent != null)
                    {
                        navLevel.ParentNode = new NavigationItem
                        {
                            Title =
                                    string.IsNullOrWhiteSpace(_navigationRequestOptions.BackButtonResourceString)
                                        ? GetDocumentDisplayTitle(currentNode.Parent)
                                        : ResHelper.GetString(_navigationRequestOptions.BackButtonResourceString, _navigationRequestOptions.Lang),
                            HasChildren = true,
                            Path = currentNode.Parent.NodeAliasPath,
                            Url =
                                    DocumentURLProvider.GetUrl(currentNode.Parent.NodeAliasPath,
                                                               currentNode.Parent.DocumentUrlPath)
                                                       .Trim(new[] { '~' })
                                                       .ToLower()
                        };
                    }
                }

                if (!query.Any(node => node.NodeAliasPath != _navigationRequestOptions.Path) && navLevel.ParentNode != null)
                {
                    getParent = true;
                    _navigationRequestOptions.Path = navLevel.ParentNode.Path;
                    if (currentNode != null)
                        navLevel.CurrentStateNode = new NavigationItem
                        {
                            Title = string.IsNullOrWhiteSpace(currentNode.DocumentName) && navLevel.CurrentNode != null ? navLevel.CurrentNode.Title : currentNode.DocumentName,
                            HasChildren = false,
                            Path = currentNode.NodeAliasPath,
                            Url = DocumentURLProvider.GetUrl(currentNode.NodeAliasPath, currentNode.DocumentUrlPath).Trim(new[] { '~' }).ToLower()
                        };
                }

                if (!getParent && navLevel.CurrentStateNode == null)
                    navLevel.CurrentStateNode = navLevel.CurrentNode;

            } while (getParent);

            var navItems = new ConcurrentBag<NavigationItem>();
            Parallel.ForEach(query.Where(node => node.NodeAliasPath != _navigationRequestOptions.Path), node => navItems.Add(new NavigationItem
            {
                Title = GetDocumentDisplayTitle(node),
                HasChildren = node.NodeHasChildren && GetQuery(node.NodeAliasPath).Any(),
                Path = node.NodeAliasPath,
                Url = DocumentURLProvider.GetUrl(node.NodeAliasPath, node.DocumentUrlPath).Trim(new[] { '~' }).ToLower(),
                NodeOrder = node.NodeOrder
            }));

            navLevel.NavigationItemList = navItems.OrderBy(item => item.NodeOrder).ToList();

            return navLevel;
        }

        private TreeNode GetCurrentNode()
        {
            return DocumentHelper.GetDocument(SiteName, _navigationRequestOptions.Path, _navigationRequestOptions.Lang, false,
                                              null, null, null, -1, true,
                                              Columns, new TreeProvider());
        }

        private string GetDocumentDisplayTitle(TreeNode node)
        {
            return string.IsNullOrWhiteSpace(node.DocumentMenuCaption) ? node.DocumentName : node.DocumentMenuCaption;
        }

        /// <summary>
        /// Get a unique cache key for the requested navigation level.
        /// </summary>
        /// <returns>A unique cache key for the requested navigation level</returns>
        public string GetNavigationLevelCacheKey()
        {
            return string.Format("eca_mobilemenu_{0}_{1}_{2}_{3}", _navigationRequestOptions.Path, _navigationRequestOptions.Lang, SiteName, IsAuthenticated).ToLower();
        }

        private MultiDocumentQuery GetQuery(string path)
        {
            //Build document query
            var query = DocumentHelper.GetDocuments()
                                      .Types(_navigationRequestOptions.ClassNames.ToArray())
                                      .OnSite(SiteName)
                                      .Path(path, PathTypeEnum.Children)
                                      .CombineWithDefaultCulture(false)
                                      .Published()
                                      .NestingLevel(1)
                                      .Culture(_navigationRequestOptions.Lang);

            //include page type fields
            if (_navigationRequestOptions.UseCustomFields)
            {
                query.WithCoupledColumns();
            }

            //Set optional WhereCondition
            if (!string.IsNullOrWhiteSpace(_navigationRequestOptions.WhereCondition))
            {
                query.WhereCondition = _navigationRequestOptions.WhereCondition;
            }

            //Set optional Columns
            if (!string.IsNullOrEmpty(Columns))
            {
                query.Columns(Columns);
            }

            //Set optional OrderByCondition
            if (!string.IsNullOrEmpty(_navigationRequestOptions.OrderByCondition))
            {
                query.OrderBy(_navigationRequestOptions.OrderByCondition);
            }

            if (_navigationRequestOptions.CheckPermissions)
            {
                query.CheckPermissions();
            }

            return query;
        }

        public static void BuildNav(NavigationRequestOptions options)
        {
            var t = new Thread(delegate (object o)
            {
                EventLogProvider.LogInformation("NavigationService","Pre-Cache-Nav", "Cache Mobile Nav");
                PreCacheNav((NavigationRequestOptions)o);
            });

            t.Start(options);
        }

        private static void PreCacheNav(NavigationRequestOptions options)
        {
            if (options == null) return;
            var mobileMenuService = new NavigationService(options)
            {
                SiteName = options.SiteName,
                IsAuthenticated = false
            };

            var navigationLevel = mobileMenuService.GetNavigationLevelAtPath();

            //Parallel.ForEach(navigationLevel.NavigationItemList, item =>
            foreach (var item in navigationLevel.NavigationItemList)
            {
                var tempNavOptions = new NavigationRequestOptions
                {
                    AndWhereCondition = options.AndWhereCondition,
                    BackButtonResourceString = options.BackButtonResourceString,
                    CheckPermissions = options.CheckPermissions,
                    ClassNames = options.ClassNames,
                    Lang = options.Lang,
                    OrderByCondition = options.OrderByCondition,
                    OrWhereCondition = options.OrWhereCondition,
                    Path = item.Path,
                    SiteName = options.SiteName,
                    UseCustomFields = options.UseCustomFields
                };

                if (item.HasChildren)
                    PreCacheNav(tempNavOptions);
            }
        }
    }
}