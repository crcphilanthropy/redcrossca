-----------------------------------------------------------------------------------------------------

Ecentricarts Mobile Menu Ednpoint

-----------------------------------------------------------------------------------------------------

--------------------------------- FILES -------------------------------------------------------------

The following lists of files will have been created by the NuGet package 

I. File List
-----------------------------------------------------------------------------------------------------
All module custom files should be in this directory : ~/Old_App_Code/ECA/Controllers/MobileMenu

/MenuController.cs
/MenuItem.cs


--------------------------------- INSTALLATION ------------------------------------------------------

I. Dependencies
-----------------------------------------------------------------------------------------------------

There is one Nuget package that must be installed along with this package

App Routing Loader

It can be found on the Ecentricarts Nuget feed

If the intended project uses Security and Permissions, add CheckMenuPermissions to the Custom Settings.