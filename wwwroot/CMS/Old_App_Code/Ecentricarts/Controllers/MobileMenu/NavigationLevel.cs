﻿using System.Collections.Generic;

namespace CMSAppAppCode.Old_App_Code.Ecentricarts.Controllers.MobileMenu
{
    public class NavigationLevel
    {
        public List<NavigationItem> NavigationItemList { get; set; }
        public NavigationItem ParentNode { get; set; }
        public NavigationItem CurrentNode { get; set; }
        public NavigationItem CurrentStateNode { get; set; }

        public NavigationLevel()
        {
            NavigationItemList = new List<NavigationItem>();
        }
    }
}