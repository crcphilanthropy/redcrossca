using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using CRCWebBanners;

[assembly: RegisterObjectType(typeof(BannerInfo), BannerInfo.OBJECT_TYPE)]
    
namespace CRCWebBanners
{
    /// <summary>
    /// BannerInfo data container class.
    /// </summary>
	[Serializable]
    public class BannerInfo : AbstractInfo<BannerInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "crcwebbanners.banner";


        /// <summary>
        /// Type information.
        /// </summary>
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(BannerInfoProvider), OBJECT_TYPE, "CRCWebBanners.Banner", "BannerID", null, null, "BannerID", "Title", null, null, "BannerSetID", BannerSetInfo.OBJECT_TYPE)
        {
			ModuleName = "CRCWebBanners",
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    new ObjectDependency("BannerSetID", "crcwebbanners.bannerset", ObjectDependencyEnum.RequiredHasDefault), 
            },
            OrderColumn = "BannerOrder",
            ImportExportSettings =
            {
                IsExportable = true,
                AllowSingleExport = true,
                IncludeToExportParentDataSet = IncludeToParentEnum.Incremental,
                ObjectTreeLocations = new List<ObjectTreeLocation>()
                {
                  // Adds the custom class into a new category in the Global objects section of the export tree
                  new ObjectTreeLocation(GLOBAL, "CRCWebBanners"),
                }
            },
            LogSynchronization = SynchronizationTypeEnum.LogSynchronization, // Enables logging of staging tasks for changes made to Office objects
            IncludeToSynchronizationParentDataSet = IncludeToParentEnum.Incremental,
            SynchronizationObjectTreeLocations = new List<ObjectTreeLocation>()
            {
                //Creates a new category in the 'Global objects' section of the staging object tree
                new ObjectTreeLocation(GLOBAL, "CRCWebBanners")
            }
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Banner ID
        /// </summary>
        [DatabaseField]
        public virtual int BannerID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("BannerID"), 0);
            }
            set
            {
                SetValue("BannerID", value);
            }
        }


        /// <summary>
        /// Banner type
        /// </summary>
        [DatabaseField]
        public virtual string BannerType
        {
            get
            {
                return ValidationHelper.GetString(GetValue("BannerType"), String.Empty);
            }
            set
            {
                SetValue("BannerType", value);
            }
        }


        /// <summary>
        /// Banner set ID
        /// </summary>
        [DatabaseField]
        public virtual int BannerSetID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("BannerSetID"), 0);
            }
            set
            {
                SetValue("BannerSetID", value);
            }
        }


        /// <summary>
        /// Title
        /// </summary>
        [DatabaseField]
        public virtual string Title
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title"), String.Empty);
            }
            set
            {
                SetValue("Title", value, String.Empty);
            }
        }


        /// <summary>
        /// Title FR
        /// </summary>
        [DatabaseField]
        public virtual string Title_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title_FR"), String.Empty);
            }
            set
            {
                SetValue("Title_FR", value, String.Empty);
            }
        }


        /// <summary>
        /// Image
        /// </summary>
        [DatabaseField]
        public virtual string Image
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Image"), String.Empty);
            }
            set
            {
                SetValue("Image", value);
            }
        }


        /// <summary>
        /// Alt text
        /// </summary>
        [DatabaseField]
        public virtual string AltText
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AltText"), String.Empty);
            }
            set
            {
                SetValue("AltText", value, String.Empty);
            }
        }


        /// <summary>
        /// Alt text FR
        /// </summary>
        [DatabaseField]
        public virtual string AltText_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AltText_FR"), String.Empty);
            }
            set
            {
                SetValue("AltText_FR", value, String.Empty);
            }
        }


        /// <summary>
        /// Tracking code
        /// </summary>
        [DatabaseField]
        public virtual string TrackingCode
        {
            get
            {
                return ValidationHelper.GetString(GetValue("TrackingCode"), String.Empty);
            }
            set
            {
                SetValue("TrackingCode", value, String.Empty);
            }
        }


        /// <summary>
        /// Embed link
        /// </summary>
        [DatabaseField]
        public virtual string EmbedLink
        {
            get
            {
                return ValidationHelper.GetString(GetValue("EmbedLink"), String.Empty);
            }
            set
            {
                SetValue("EmbedLink", value, String.Empty);
            }
        }

        [DatabaseField]
        public virtual int BannerOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("BannerOrder"), 0);
            }
            set
            {
                SetValue("BannerOrder", value, 0);
            }
        }

        /// <summary>
        /// Archive
        /// </summary>
        [DatabaseField]
        public virtual bool Archive
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Archive"), false);
            }
            set
            {
                SetValue("Archive", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            BannerInfoProvider.DeleteBannerInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            BannerInfoProvider.SetBannerInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public BannerInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty BannerInfo object.
        /// </summary>
        public BannerInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new BannerInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public BannerInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}