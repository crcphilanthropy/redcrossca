using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using CRCWebBanners;
using System.Collections.Generic;

[assembly: RegisterObjectType(typeof(CompanyInfo), CompanyInfo.OBJECT_TYPE)]
    
namespace CRCWebBanners
{
    /// <summary>
    /// CompanyInfo data container class.
    /// </summary>
	[Serializable]
    public class CompanyInfo : AbstractInfo<CompanyInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "crcwebbanners.company";


        /// <summary>
        /// Type information.
        /// </summary>
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(CompanyInfoProvider), OBJECT_TYPE, "CRCWebBanners.Company", "CompanyID", null, null, null, null, null, null, null, null)
        {
			ModuleName = "CRCWebBanners",
			TouchCacheDependencies = true,
            //ImportExportSettings =
            //{
            //    IsExportable = true,
            //    AllowSingleExport = true,
            //    ObjectTreeLocations = new List<ObjectTreeLocation>()
            //    {
            //      // Adds the custom class into a new category in the Global objects section of the export tree
            //      new ObjectTreeLocation(GLOBAL, "CRCWebBanners"),
            //    }
            //},
            //LogSynchronization = SynchronizationTypeEnum.LogSynchronization, // Enables logging of staging tasks for changes made to Office objects
            //SynchronizationObjectTreeLocations = new List<ObjectTreeLocation>()
            //{
            //    //Creates a new category in the 'Global objects' section of the staging object tree
            //    new ObjectTreeLocation(GLOBAL, "CRCWebBanners")
            //}
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Company ID
        /// </summary>
        [DatabaseField]
        public virtual int CompanyID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("CompanyID"), 0);
            }
            set
            {
                SetValue("CompanyID", value);
            }
        }


        /// <summary>
        /// Company name
        /// </summary>
        [DatabaseField]
        public virtual string CompanyName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("CompanyName"), String.Empty);
            }
            set
            {
                SetValue("CompanyName", value, String.Empty);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            CompanyInfoProvider.DeleteCompanyInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            CompanyInfoProvider.SetCompanyInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public CompanyInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty CompanyInfo object.
        /// </summary>
        public CompanyInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new CompanyInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public CompanyInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}