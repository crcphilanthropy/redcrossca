using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace CRCWebBanners
{    
    /// <summary>
    /// Class providing BannerSetInfo management.
    /// </summary>
    public class BannerSetInfoProvider : AbstractInfoProvider<BannerSetInfo, BannerSetInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public BannerSetInfoProvider()
            : base(BannerSetInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the BannerSetInfo objects.
        /// </summary>
        public static ObjectQuery<BannerSetInfo> GetBannerSets()
        {
            return ProviderObject.GetBannerSetsInternal();
        }


        /// <summary>
        /// Returns BannerSetInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerSetInfo ID</param>
        public static BannerSetInfo GetBannerSetInfo(int id)
        {
            return ProviderObject.GetBannerSetInfoInternal(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified BannerSetInfo.
        /// </summary>
        /// <param name="infoObj">BannerSetInfo to be set</param>
        public static void SetBannerSetInfo(BannerSetInfo infoObj)
        {
            ProviderObject.SetBannerSetInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified BannerSetInfo.
        /// </summary>
        /// <param name="infoObj">BannerSetInfo to be deleted</param>
        public static void DeleteBannerSetInfo(BannerSetInfo infoObj)
        {
            ProviderObject.DeleteBannerSetInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes BannerSetInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerSetInfo ID</param>
        public static void DeleteBannerSetInfo(int id)
        {
            BannerSetInfo infoObj = GetBannerSetInfo(id);
            DeleteBannerSetInfo(infoObj);
        }

        public static void MoveBannerSetUp(BannerSetInfo info)
        {
            info.Generalized.MoveObjectUp();
        }

        public static void MoveBannerSetDown(BannerSetInfo info)
        {
            info.Generalized.MoveObjectDown();
        }

        #endregion


        #region "Internal methods - Basic"

        /// <summary>
        /// Returns a query for all the BannerSetInfo objects.
        /// </summary>
        protected virtual ObjectQuery<BannerSetInfo> GetBannerSetsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns BannerSetInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerSetInfo ID</param>        
        protected virtual BannerSetInfo GetBannerSetInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified BannerSetInfo.
        /// </summary>
        /// <param name="infoObj">BannerSetInfo to be set</param>        
        protected virtual void SetBannerSetInfoInternal(BannerSetInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified BannerSetInfo.
        /// </summary>
        /// <param name="infoObj">BannerSetInfo to be deleted</param>        
        protected virtual void DeleteBannerSetInfoInternal(BannerSetInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}