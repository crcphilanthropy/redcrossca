using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace CRCWebBanners
{    
    /// <summary>
    /// Class providing BannerInfo management.
    /// </summary>
    public class BannerInfoProvider : AbstractInfoProvider<BannerInfo, BannerInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public BannerInfoProvider()
            : base(BannerInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the BannerInfo objects.
        /// </summary>
        public static ObjectQuery<BannerInfo> GetBanners()
        {
            return ProviderObject.GetBannersInternal();
        }


        /// <summary>
        /// Returns BannerInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerInfo ID</param>
        public static BannerInfo GetBannerInfo(int id)
        {
            return ProviderObject.GetBannerInfoInternal(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified BannerInfo.
        /// </summary>
        /// <param name="infoObj">BannerInfo to be set</param>
        public static void SetBannerInfo(BannerInfo infoObj)
        {
            ProviderObject.SetBannerInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified BannerInfo.
        /// </summary>
        /// <param name="infoObj">BannerInfo to be deleted</param>
        public static void DeleteBannerInfo(BannerInfo infoObj)
        {
            ProviderObject.DeleteBannerInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes BannerInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerInfo ID</param>
        public static void DeleteBannerInfo(int id)
        {
            BannerInfo infoObj = GetBannerInfo(id);
            DeleteBannerInfo(infoObj);
        }

        public static void MoveBannertUp(BannerInfo info)
        {
            info.Generalized.MoveObjectUp();
        }

        public static void MoveBannerDown(BannerInfo info)
        {
            info.Generalized.MoveObjectDown();
        }

        #endregion


        #region "Internal methods - Basic"

        /// <summary>
        /// Returns a query for all the BannerInfo objects.
        /// </summary>
        protected virtual ObjectQuery<BannerInfo> GetBannersInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns BannerInfo with specified ID.
        /// </summary>
        /// <param name="id">BannerInfo ID</param>        
        protected virtual BannerInfo GetBannerInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified BannerInfo.
        /// </summary>
        /// <param name="infoObj">BannerInfo to be set</param>        
        protected virtual void SetBannerInfoInternal(BannerInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified BannerInfo.
        /// </summary>
        /// <param name="infoObj">BannerInfo to be deleted</param>        
        protected virtual void DeleteBannerInfoInternal(BannerInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}