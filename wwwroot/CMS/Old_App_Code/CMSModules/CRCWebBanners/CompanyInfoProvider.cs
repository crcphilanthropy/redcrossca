using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace CRCWebBanners
{    
    /// <summary>
    /// Class providing CompanyInfo management.
    /// </summary>
    public class CompanyInfoProvider : AbstractInfoProvider<CompanyInfo, CompanyInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public CompanyInfoProvider()
            : base(CompanyInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the CompanyInfo objects.
        /// </summary>
        public static ObjectQuery<CompanyInfo> GetCompanies()
        {
            return ProviderObject.GetCompaniesInternal();
        }


        /// <summary>
        /// Returns CompanyInfo with specified ID.
        /// </summary>
        /// <param name="id">CompanyInfo ID</param>
        public static CompanyInfo GetCompanyInfo(int id)
        {
            return ProviderObject.GetCompanyInfoInternal(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified CompanyInfo.
        /// </summary>
        /// <param name="infoObj">CompanyInfo to be set</param>
        public static void SetCompanyInfo(CompanyInfo infoObj)
        {
            ProviderObject.SetCompanyInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified CompanyInfo.
        /// </summary>
        /// <param name="infoObj">CompanyInfo to be deleted</param>
        public static void DeleteCompanyInfo(CompanyInfo infoObj)
        {
            ProviderObject.DeleteCompanyInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes CompanyInfo with specified ID.
        /// </summary>
        /// <param name="id">CompanyInfo ID</param>
        public static void DeleteCompanyInfo(int id)
        {
            CompanyInfo infoObj = GetCompanyInfo(id);
            DeleteCompanyInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the CompanyInfo objects.
        /// </summary>
        protected virtual ObjectQuery<CompanyInfo> GetCompaniesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns CompanyInfo with specified ID.
        /// </summary>
        /// <param name="id">CompanyInfo ID</param>        
        protected virtual CompanyInfo GetCompanyInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified CompanyInfo.
        /// </summary>
        /// <param name="infoObj">CompanyInfo to be set</param>        
        protected virtual void SetCompanyInfoInternal(CompanyInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified CompanyInfo.
        /// </summary>
        /// <param name="infoObj">CompanyInfo to be deleted</param>        
        protected virtual void DeleteCompanyInfoInternal(CompanyInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}