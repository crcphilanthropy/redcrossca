using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using CRCPhotosForMedia;
using System.Collections.Generic;

[assembly: RegisterObjectType(typeof(PhotoInfo), PhotoInfo.OBJECT_TYPE)]
    
namespace CRCPhotosForMedia
{
    /// <summary>
    /// PhotoInfo data container class.
    /// </summary>
	[Serializable]
    public class PhotoInfo : AbstractInfo<PhotoInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "crcphotosformedia.photo";


        /// <summary>
        /// Type information.
        /// </summary>
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(PhotoInfoProvider), OBJECT_TYPE, "CRCPhotosForMedia.Photo", "PhotoID", null, null, "PhotoID", "Title_EN", null, null, null, null)
        {
			ModuleName = "CRCPhotosForMedia",
			TouchCacheDependencies = true,
            OrderColumn = "PhotoOrder",
            ImportExportSettings =
            {
                IsExportable = true,
                AllowSingleExport = true,
                ObjectTreeLocations = new List<ObjectTreeLocation>()
                {
                  // Adds the custom class into a new category in the Global objects section of the export tree
                  new ObjectTreeLocation(GLOBAL, "CRCPhotosForMedia"),
                }
            },
            LogSynchronization = SynchronizationTypeEnum.LogSynchronization, // Enables logging of staging tasks for changes made to Office objects
            SynchronizationObjectTreeLocations = new List<ObjectTreeLocation>()
            {
                //Creates a new category in the 'Global objects' section of the staging object tree
                new ObjectTreeLocation(GLOBAL, "CRCPhotosForMedia")
            }
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Photo ID
        /// </summary>
        [DatabaseField]
        public virtual int PhotoID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PhotoID"), 0);
            }
            set
            {
                SetValue("PhotoID", value);
            }
        }


        /// <summary>
        /// Title EN
        /// </summary>
        [DatabaseField]
        public virtual string Title_EN
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title_EN"), String.Empty);
            }
            set
            {
                SetValue("Title_EN", value);
            }
        }


        /// <summary>
        /// Title FR
        /// </summary>
        [DatabaseField]
        public virtual string Title_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Title_FR"), String.Empty);
            }
            set
            {
                SetValue("Title_FR", value);
            }
        }


        /// <summary>
        /// Description EN
        /// </summary>
        [DatabaseField]
        public virtual string Description_EN
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Description_EN"), String.Empty);
            }
            set
            {
                SetValue("Description_EN", value);
            }
        }


        /// <summary>
        /// Description FR
        /// </summary>
        [DatabaseField]
        public virtual string Description_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Description_FR"), String.Empty);
            }
            set
            {
                SetValue("Description_FR", value);
            }
        }


        /// <summary>
        /// Photo credits EN
        /// </summary>
        [DatabaseField]
        public virtual string PhotoCredits_EN
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PhotoCredits_EN"), String.Empty);
            }
            set
            {
                SetValue("PhotoCredits_EN", value);
            }
        }


        /// <summary>
        /// Photo credits FR
        /// </summary>
        [DatabaseField]
        public virtual string PhotoCredits_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PhotoCredits_FR"), String.Empty);
            }
            set
            {
                SetValue("PhotoCredits_FR", value);
            }
        }


        /// <summary>
        /// Alt text EN
        /// </summary>
        [DatabaseField]
        public virtual string AltText_EN
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AltText_EN"), String.Empty);
            }
            set
            {
                SetValue("AltText_EN", value);
            }
        }


        /// <summary>
        /// Alt text FR
        /// </summary>
        [DatabaseField]
        public virtual string AltText_FR
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AltText_FR"), String.Empty);
            }
            set
            {
                SetValue("AltText_FR", value);
            }
        }


        /// <summary>
        /// Thumbnail
        /// </summary>
        [DatabaseField]
        public virtual string Thumbnail
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Thumbnail"), String.Empty);
            }
            set
            {
                SetValue("Thumbnail", value);
            }
        }


        /// <summary>
        /// Image
        /// </summary>
        [DatabaseField]
        public virtual string Image
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Image"), String.Empty);
            }
            set
            {
                SetValue("Image", value);
            }
        }


        /// <summary>
        /// High res image
        /// </summary>
        [DatabaseField]
        public virtual string HighResImage
        {
            get
            {
                return ValidationHelper.GetString(GetValue("HighResImage"), String.Empty);
            }
            set
            {
                SetValue("HighResImage", value, String.Empty);
            }
        }


        /// <summary>
        /// Download
        /// </summary>
        [DatabaseField]
        public virtual bool Download
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Download"), true);
            }
            set
            {
                SetValue("Download", value);
            }
        }


        /// <summary>
        /// Categories
        /// </summary>
        [DatabaseField]
        public virtual string Categories
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Categories"), String.Empty);
            }
            set
            {
                SetValue("Categories", value, String.Empty);
            }
        }


        /// <summary>
        /// Regions
        /// </summary>
        [DatabaseField]
        public virtual string Regions
        {
            get
            {
                return ValidationHelper.GetString(GetValue("Regions"), String.Empty);
            }
            set
            {
                SetValue("Regions", value, String.Empty);
            }
        }

        [DatabaseField]
        public virtual int PhotoOrder
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("PhotoOrder"), 0);
            }
            set
            {
                SetValue("PhotoOrder", value, 0);
            }
        }

        /// <summary>
        /// Archive
        /// </summary>
        [DatabaseField]
        public virtual bool Archive
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Archive"), false);
            }
            set
            {
                SetValue("Archive", value);
            }
        }


        /// <summary>
        /// Show english
        /// </summary>
        [DatabaseField]
        public virtual bool ShowEnglish
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("ShowEnglish"), true);
            }
            set
            {
                SetValue("ShowEnglish", value);
            }
        }


        /// <summary>
        /// Show french
        /// </summary>
        [DatabaseField]
        public virtual bool ShowFrench
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("ShowFrench"), true);
            }
            set
            {
                SetValue("ShowFrench", value);
            }
        }

        /// <summary>
        /// Date created
        /// </summary>
        [DatabaseField]
        public virtual DateTime DateCreated
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("DateCreated"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("DateCreated", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            PhotoInfoProvider.DeletePhotoInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            PhotoInfoProvider.SetPhotoInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public PhotoInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty PhotoInfo object.
        /// </summary>
        public PhotoInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new PhotoInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public PhotoInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}