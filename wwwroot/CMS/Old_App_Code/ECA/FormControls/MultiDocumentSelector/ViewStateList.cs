﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace ECA.FormControls.MultiDocumentSelector
{
    public class ViewStateList<T> : IList<T>, IList
    {
        private readonly StateBag _viewState;
        private readonly string _key;
        private readonly List<T> _list;

        public ViewStateList(StateBag viewState, string key)
        {
            _viewState = viewState;
            _key = key;
            _list = _viewState[_key] as List<T> ?? new List<T>();
            _viewState[_key] = _list;
        }

        public ViewStateList(StateBag viewState, string key, IEnumerable<T> collection)
        {
            _viewState = viewState;
            _key = key;
            _list = new List<T>(collection);
            _viewState[_key] = _list;
        }

        private TResult Func<TResult>(Func<List<T>, TResult> func)
        {
            return func(_list);
        }

        private TResult MutateFunc<TResult>(Func<List<T>, TResult> func)
        {
            var tmp = func(_list);
            _viewState[_key] = _list;
            return tmp;
        }

        private void Action(Action<List<T>> action)
        {
            action(_list);
        }

        private void MutateAction(Action<List<T>> action)
        {
            action(_list);
            _viewState[_key] = _list;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Func(x => x.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Func(x => x.GetEnumerator());
        }

        public void Add(T item)
        {
            MutateAction(x => x.Add(item));
        }

        int IList.Add(object value)
        {
            return MutateFunc(x => ((IList)x).Add(value));
        }

        bool IList.Contains(object value)
        {
            return Func(x => ((IList)x).Contains(value));
        }

        public void Clear()
        {
            MutateAction(x => x.Clear());
        }

        int IList.IndexOf(object value)
        {
            return Func(x => ((IList)x).IndexOf(value));
        }

        void IList.Insert(int index, object value)
        {
            MutateAction(x => ((IList)x).Insert(index, value));
        }

        void IList.Remove(object value)
        {
            MutateAction(x => ((IList)x).Remove(value));
        }

        public bool Contains(T item)
        {
            return Func(x => x.Contains(item));
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Action(x => x.CopyTo(array, arrayIndex));
        }

        public bool Remove(T item)
        {
            return MutateFunc(x => x.Remove(item));
        }

        void ICollection.CopyTo(Array array, int index)
        {
            Action(x => ((ICollection)x).CopyTo(array, index));
        }

        public int Count
        {
            get
            {
                return Func(x => x.Count);
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                return Func(x => ((ICollection)x).SyncRoot);
            }
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return Func(x => ((ICollection)x).IsSynchronized);
            }
        }

        bool ICollection<T>.IsReadOnly
        {
            get
            {
                return Func(x => ((ICollection<T>)x).IsReadOnly);
            }
        }

        bool IList.IsReadOnly
        {
            get
            {
                return Func(x => ((IList)x).IsReadOnly);
            }
        }

        bool IList.IsFixedSize
        {
            get
            {
                return Func(x => ((IList)x).IsFixedSize);
            }
        }

        public int IndexOf(T item)
        {
            return Func(x => x.IndexOf(item));
        }

        public void Insert(int index, T item)
        {
            MutateAction(x => x.Insert(index, item));
        }

        public void RemoveAt(int index)
        {
            MutateAction(x => x.RemoveAt(index));
        }

        object IList.this[int index]
        {
            get { return Func(x => x[index]); }
            set { MutateAction(x => ((IList)x)[index] = value); }
        }

        public T this[int index]
        {
            get { return Func(x => x[index]); }
            set { MutateAction(x => x[index] = value); }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            MutateAction(x => x.AddRange(collection));
        }

        public int RemoveAll(Predicate<T> match)
        {
            return MutateFunc(x => x.RemoveAll(match));
        }
    }
}