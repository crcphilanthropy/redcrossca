﻿using System;

namespace ECA.FormControls.MultiDocumentSelector
{
    [Serializable]
    public class SortableDocument
    {
        /// <summary>
        /// Order used by ajaxtoolkit reorderlist control.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// NodeGUID field of the document.
        /// </summary>
        public string NodeGUID { get; set; }

        /// <summary>
        /// Node name of the document
        /// </summary>
        public string DocumentName { get; set; }

        /// <summary>
        /// Node name of the document name path
        /// </summary>
        public string DocumentNamePath { get; set; }
    }
}
