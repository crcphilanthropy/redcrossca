using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using CMS.DocumentEngine;
using CMS.DataEngine;
using CMS.Localization;
using CMS.SiteProvider;
using CMS.Helpers;

namespace ECA.FormControls.MultiDocumentSelector
{
    public class SortableDocumentProvider
    {
        /// <summary>
        /// Get a dataset from the GUID list.  This is not sorted in the order specified by the GUIDs but rather the default order via SelectNodes.
        /// </summary>
        /// <param name="guidList">List of TreeNode GUIDs.</param>
        /// <param name="docClassNames">Class names to limit by (if known).</param>
        /// <param name="cultureCode">Site culture we are interested in.</param>
        /// <param name="siteName">The site we are interested in.</param>
        /// <param name="columns">Columns to be returned</param>
        /// <param name="where">Where clause</param>
        /// <param name="publishedOnly">Returns only published documents/pages</param>
        /// <returns>Unordered dataset populated via TreeProvider.SelectNodes.</returns>
        public static TreeNodeDataSet GetUnsortedTreeNodeDataSet(string guidList, string docClassNames = null, string cultureCode = null, string siteName = null, string columns = null, string where = null, bool publishedOnly = true)
        {
            if (string.IsNullOrWhiteSpace(guidList))
                return new TreeNodeDataSet();

            if (string.IsNullOrWhiteSpace(cultureCode))
                cultureCode = string.IsNullOrWhiteSpace(LocalizationContext.PreferredCultureCode) ? "en-US" : LocalizationContext.PreferredCultureCode;

            if (string.IsNullOrWhiteSpace(siteName))
                siteName = SiteContext.CurrentSiteName;

            var classNames = string.IsNullOrWhiteSpace(docClassNames) ? GetDocumentClassNameList(guidList) : docClassNames;
            var nodeGuids = guidList.Split(';').Select(item => string.Format("NodeGUID='{0}'", item)).ToList();

            var whereCondition = new List<string>();
            if (nodeGuids.Any())
                whereCondition.Add(nodeGuids.Join(" OR "));

            if (!string.IsNullOrWhiteSpace(where))
                whereCondition.Add(where);

            var tp = new TreeProvider { MergeResults = true };

            if (!string.IsNullOrWhiteSpace(columns))
            {
                return tp.SelectNodes(siteName, "/%", cultureCode, true, classNames, whereCondition.Select(item => string.Format("({0})", item)).Join(" AND "), null, -1, publishedOnly, -1, columns);
            }

            return tp.SelectNodes(siteName, "/%", cultureCode, true, classNames, whereCondition.Select(item => string.Format("({0})", item)).Join(" AND "), null, -1, publishedOnly);
        }

        /// <summary>
        /// Get a list of TreeNodes from the GUID list.  This is not sorted in the order specified by the GUIDs but rather the default order via SelectNodes.
        /// </summary>
        /// <param name="guidList">List of TreeNode GUIDs.</param>
        /// <param name="docClassNames">Class names to limit by (if known).</param>
        /// <param name="cultureCode">Site culture we are interested in.</param>
        /// <param name="siteName">The site we are interested in.</param>
        /// <param name="columns">Columns to be returned</param>
        /// <param name="where">Where clause</param>
        /// <param name="publishedOnly">Returns only published documents/pages</param>
        /// <returns>Unordered dataset populated via TreeProvider.SelectNodes.</returns>
        public static List<TreeNode> GetUnsortedTreeNodes(string guidList, string docClassNames = null, string cultureCode = null, string siteName = null, string columns = null, string where = null, bool publishedOnly = true)
        {
            return GetUnsortedTreeNodeDataSet(guidList, docClassNames, cultureCode, siteName, columns, where).ToList();
        }

        /// <summary>
        /// Get a dataset from the GUID list.  This is not sorted in the order specified by the GUIDs but rather the default order via SelectNodes.
        /// </summary>
        /// <param name="guidList">List of TreeNode GUIDs.</param>
        /// <param name="docClassNames">Class names to limit by (if known).</param>
        /// <param name="cultureCode">Site culture we are interested in.</param>
        /// <param name="siteName">The site we are interested in.</param>
        /// <param name="columns">Columns to be returned</param>
        /// <param name="where">Where clause</param>
        /// <param name="publishedOnly">Returns only published documents/pages</param>
        /// <returns>Unordered dataset populated via TreeProvider.SelectNodes.</returns>
        public static TreeNodeDataSet GetSortedTreeNodeDataSet(string guidList, string docClassNames = null, string cultureCode = null, string siteName = null, string columns = null, string where = null, bool publishedOnly = true)
        {
            var unsortedDataSet = GetUnsortedTreeNodeDataSet(guidList, docClassNames, cultureCode, siteName, columns, where, publishedOnly);

            if (DataHelper.DataSourceIsEmpty(unsortedDataSet))
            {
                return new TreeNodeDataSet();
            }

            var dataTable = unsortedDataSet.Tables[0];

            var dataSet = unsortedDataSet.Clone();
            dataSet.Tables[0].Rows.Clear();

            foreach (var nodeGuid in guidList.Split(';'))
            {
                Guid guid;
                if (Guid.TryParse(nodeGuid, out guid))
                {
                    var resultRow = dataTable.AsEnumerable().FirstOrDefault(r => r.Field<Guid>("NodeGUID") == guid);
                    dataSet.Tables[0].ImportRow(resultRow);
                }
            }

            return dataSet;
        }

        public static List<TreeNode> GetSortedTreeNodes(string guidList, string docClassNames = null, string cultureCode = null, string siteName = null, string columns = null, string where = null, bool publishedOnly = true)
        {
            return GetSortedTreeNodeDataSet(guidList, docClassNames, cultureCode, siteName, columns, where, publishedOnly).ToList();
        }

        /// <summary>
        /// Get list of documents.  Sorted by order in the GUID list.
        /// </summary>
        /// <param name="guidList">List of TreeNode GUIDs.</param>
        /// <param name="docClassNames">Class names to limit by (if known).</param>
        /// <param name="cultureCode">Site culture we are interested in.</param>
        /// <param name="siteName">The site we are interested in.</param>
        /// <param name="where">Where clause</param>
        /// <param name="publishedOnly">Returns only published documents/pages</param>
        /// <returns>List of SortableDocuments.</returns>
        public static List<SortableDocument> GetSortableDocuments(string guidList, string docClassNames = null, string cultureCode = null, string siteName = null, string where = null, bool publishedOnly = true)
        {
            var i = 0;
            return GetSortedTreeNodes(guidList, docClassNames, cultureCode, siteName, "NodeGUID, NodeID, DocumentName, DocumentCulture, DocumentNamePath", where, publishedOnly)
                    .Select(x => new SortableDocument
                    {
                        DocumentName = x.DocumentName,
                        DocumentNamePath = x.DocumentNamePath,
                        NodeGUID = x.NodeGUID.ToString(),
                        Order = i++
                    })
                    .ToList();
        }

        /// <summary>
        /// Reverse engineer the class names from the GUID list to limit the impact of querying every class.
        /// The SQL query is not registered in the CMS desk to limit installation steps.  Also a query
        /// on this view is not available anyway in the defaults.
        /// </summary>
        /// <param name="guidList">List of GUIDs</param>
        /// <returns>Class names separated by ';'</returns>
        public static string GetDocumentClassNameList(string guidList)
        {
            var classnames = string.Empty;

            using (var connection = new SqlConnection(ConnectionHelper.ConnectionString))
            {
                connection.Open();
                guidList = ("'" + guidList + "'").Replace(";", "','");
                var query = "SELECT DISTINCT ClassName FROM dbo.View_CMS_Tree_Joined WHERE NodeGUID IN (" + guidList + ")";

                using (var command = new SqlCommand(query, connection))
                using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        classnames += reader.GetString(0) + ";";
                    }
                }
            }

            return classnames;
        }

        /// <summary>
        /// Get the node name of a document based on GUID.
        /// </summary>
        /// <param name="guid">GUID of a page.</param>
        /// <param name="cultureCode"></param>
        /// <param name="siteName"></param>
        /// <returns>Node name.</returns>
        public static string GetDocumentNodeName(string guid, string cultureCode = null, string siteName = null)
        {
            if (!string.IsNullOrWhiteSpace(guid))
            {
                if (string.IsNullOrWhiteSpace(cultureCode))
                    cultureCode = string.IsNullOrWhiteSpace(LocalizationContext.PreferredCultureCode) ? "en-US" : LocalizationContext.PreferredCultureCode;

                if (string.IsNullOrWhiteSpace(siteName))
                    siteName = SiteContext.CurrentSiteName;


                var tp = new TreeProvider { MergeResults = true };
                DataSet ds = tp.SelectNodes(siteName, "/%", cultureCode, true, null, "NodeGUID='" + guid + "'", null, -1, false);

                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    Guid searchGuid;
                    if (Guid.TryParse(guid, out searchGuid))
                    {
                        var resultRow = ds.Tables[0].AsEnumerable().FirstOrDefault(r => r.Field<Guid>("NodeGUID") == searchGuid);

                        if (resultRow != null && resultRow["DocumentName"] != null)
                        {
                            return resultRow["DocumentName"].ToString();
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Determine if the document is a member of a valid class.
        /// </summary>
        /// <param name="nodeGuid">GUID for target node.</param>
        /// <param name="classNameList">List of class names to check.</param>
        /// <param name="cultureCode">Culture we are interested in.</param>
        /// <param name="siteName">ID of the site we are targeting.</param>
        /// <returns>True/false if the document is a member of a valid class name.</returns>
        public static bool DocumentIsValidClass(Guid nodeGuid, string classNameList, string cultureCode = null, string siteName = null)
        {
            if (string.IsNullOrWhiteSpace(classNameList))
            {
                return true;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(cultureCode))
                    cultureCode = string.IsNullOrWhiteSpace(LocalizationContext.PreferredCultureCode) ? "en-US" : LocalizationContext.PreferredCultureCode;

                if (string.IsNullOrWhiteSpace(siteName))
                    siteName = SiteContext.CurrentSiteName;

                var tree = new TreeProvider();
                var node = tree.SelectSingleNode(nodeGuid, cultureCode, siteName);
                if (classNameList.ToLower().Trim().Contains(node.NodeClassName.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
