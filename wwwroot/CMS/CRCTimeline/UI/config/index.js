const path = require('path');

const config = {
  env: process.env.NODE_ENV || 'development',
  base: path.resolve(__dirname, '..'),
  client: '_src',
  public: 'public',
  dist: 'jvs',

  compiler: {
    cssnano: {
      discardComments: {
        removeAll: true
      },
      discardUnused: false,
      mergeIdents: false,
      reduceIdents: false,
      safe: true,
      sourcemap: true
    },
    eslint: {
      fix: false,
      failOnError: false
    },
    uglify: {
      sourceMap: true,
      mangle: {
        toplevel: true,
        sort: true,
        eval: true,
        properties: true,
        except: ['webpackJsonp', '$']
      },
      compress: {
        unused: true,
        dead_code: true,
        warnings: false
      }
    },
    devtool: process.env.NODE_ENV === 'development' ? false : 'cheap-module-source-map',
    hash_type: 'hash',
    quiet: false,
    stats: {
      chunks: false,
      chunkModules: false,
      colors: true
    },
    public_path: '/CRCTimeline/UI/jvs/',
    vendors: [
      'react'
    ],
    extensions: ['.js', '.scss']
  }
};

config.paths = {
  base: base,
  client: base.bind(null, config.client),
  public: base.bind(null, config.public),
  dist: base.bind(null, config.dist)
};

config.globals = {
  'process.env': {
    'NODE_ENV': JSON.stringify(config.env)
  },
  'NODE_ENV': config.env,
  '__DEV__': config.env === 'development',
  '__PROD__': config.env === 'production',
  '__BASE__': JSON.stringify(process.env.BASENAME || '')
};

function base() {
  const args = [config.base].concat([].slice.call(arguments));
  return path.resolve.apply(path, args);
}

module.exports = config;
