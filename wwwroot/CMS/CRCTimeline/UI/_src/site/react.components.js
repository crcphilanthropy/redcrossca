import React from 'react';
import { render } from 'react-dom';

import Gallery from 'site/containers/gallery';
/**
 * Method to create a gallery in Vanilla Javascript based on the paramaters below
 * This method is being added as a jQuery plugin for easy usage throughout the site
 *
 * @param  {array}  images        An array of image objects {src, caption}
 * @param  {element}  container   Container element that is to be used to attach the React Component
 * @param  {number}  currentImage Index of the current image selected
 * @param  {boolean} isOpen       If the Gallery should be open on initialize
 * @return {object}               React Object
 */
export const createGallery = (images, container, currentImage, isOpen) =>
  render(
    <Gallery
      containerClass={'gallery'}
      isOpen={isOpen}
      images={images}
      currentImage={currentImage}
    />,
    container
  );
