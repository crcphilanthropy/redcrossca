import React from 'react';
import PropTypes from 'prop-types';

export const Image = ({
  image,
  text,
  hasCaption
}) => (
  <figure className={'figure'}>
    <img
      className={'image'}
      src={image}
      alt={text}
    />
    <Caption
      hasCaption={hasCaption}
      text={text}
    />
  </figure>
);

Image.propTypes = {
  image: PropTypes.string,
  text: PropTypes.string,
  hasCaption: PropTypes.bool
};

Image.defaultProps = {
  hasCaption: true,
  image: '',
  text: ''
};

const Caption = ({
  hasCaption = true,
  text
}) => {
  if (!hasCaption) {
    return null;
  }

  return (
    <figcaption className={'caption'}>
      {text}
    </figcaption>
  );
};

Caption.propTypes = {
  hasCaption: PropTypes.bool,
  text: PropTypes.string
};

Caption.defaultProps = {
  hasCaption: true,
  text: ''
};

export default Image;
