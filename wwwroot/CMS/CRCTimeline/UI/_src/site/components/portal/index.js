import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';

export class Portal extends Component {
  constructor() {
    super();
    this.portalElement = null;
  }

  componentDidMount() {
    const p = document.createElement('div');
    document.body.appendChild(p);
    this.portalElement = p;
    this.componentDidUpdate();
  }

  componentDidUpdate() {
    const {
      children,
      ...others
    } = this.props;

    render(
      <div {...others}>
        {children}
      </div>,
      this.portalElement
    );
  }

  componentWillUnmount() {
    document.body.removeChild(this.portalElement);
  }

  render() {
    return null;
  }
}

Portal.propTypes = {
  children: PropTypes.node.isRequired
};

export default Portal;
