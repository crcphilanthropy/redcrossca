import React from 'react';
import PropTypes from 'prop-types';

export const ImageCount = ({
  isHidden,
  text,
  current,
  total
}) => {
  if (isHidden) {
    return null;
  }

  return (
    <div className={'image-count'}>
      <div className={'text'}>
        {text}
      </div>
      <div className={'value'}>
        {current} of {total}
      </div>
    </div>
  );
};

ImageCount.propTypes = {
  isHidden: PropTypes.bool,
  text: PropTypes.string,
  current: PropTypes.number,
  total: PropTypes.number
};

ImageCount.defaultProps = {
  isHidden: false,
  text: '',
  current: 0,
  total: 0
};

export default ImageCount;
