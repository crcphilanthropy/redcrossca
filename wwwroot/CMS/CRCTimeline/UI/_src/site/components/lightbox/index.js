import $ from 'jquery';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Image from '../image';
import ImageCount from '../image-count';

/**
 * Prevent browser from scrolling base on true|false
 * @param  {Boolean} value true|false value to determine if the browser should be scrollable
 */
const setBrowserScrollable = (isScrollable) => {
  if (typeof isScrollable !== 'boolean') {
    isScrollable = false;
  }

  $('body').toggleClass('no-scroll', !isScrollable);

  // document.body.style['overflow'] = !value ? 'hidden' : 'auto';
  // document.body.style['position'] = !value ? 'fixed' : 'inherit';
};

/**
 * Stop all events
 */
const stopEvents = (event) => {
  event.preventDefault();
  event.stopPropagation();
};

export class Lightbox extends Component {

  constructor(props) {
    super(props);

    this.keyboardDirectionsEvents = this.keyboardDirectionsEvents.bind(this);
    this.closeOnEscapeEvents = this.closeOnEscapeEvents.bind(this);
    this.openOnEnterEvents = this.openOnEnterEvents.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.next = this.next.bind(this);
    this.previous = this.next.bind(this);
  }

  componentWillMount() {
    const {
      closeOnEscape,
      keyboardDirections,
      openOnEnter
    } = this.props;

    /**
     * Keyboard Directions
     */
    if (keyboardDirections) {
      window.addEventListener('keydown', this.keyboardDirectionsEvents);
    }

    /**
     * closeOnEscape
     */
    if (closeOnEscape) {
      window.addEventListener('keydown', this.closeOnEscapeEvents);
    }

    /**
     * openOnEnter
     */
    if (openOnEnter) {
      window.addEventListener('keydown', this.openOnEnterEvents);
    }
  }

  openOnEnterEvents(e) {
    const {
      onOpen,
      isOpen
    } = this.props;

    // enter key && prevent re-opening when it is already opened
    if (e.keyCode === 13 && !isOpen) {
      onOpen();
      setBrowserScrollable(false);
    }
  }

  closeOnEscapeEvents(e) {
    const {
      onClose
    } = this.props;
    // escape key
    if (e.keyCode === 27) {
      onClose();
    }
  }

  keyboardDirectionsEvents(e) {
    const {
      onNext,
      onPrevious
    } = this.props;

    switch (e.keyCode) {
      // previous
      case 37: {
        onPrevious();
        return true;
      }
      // next
      case 39: {
        onNext();
        return true;
      }
      default:
        return false;
    }
  }

  open() {
    const {
      onOpen
    } = this.props;

    onOpen();
    setBrowserScrollable(false);
  }

  close(e) {
    const {
      onClose
    } = this.props;

    if (e.target !== e.currentTarget) {
      return;
    }

    onClose();
    setBrowserScrollable(true);
  }

  next(e) {
    const {
      onNext
    } = this.props;

    stopEvents(e);
    onNext();
  }

  previous(e) {
    const {
      onPrevious
    } = this.props;

    stopEvents(e);
    onPrevious();
  }

  renderEmpty() {
    const {
      containerClass
    } = this.props;

    return (
      <div className={classNames(containerClass, 'closed')} />
    );
  }

  renderLightbox() {
    const {
      containerClass,
      images,
      currentImage,
      showCloseButton,
      showImageCount
    } = this.props;

    return (
      <div className={classNames(containerClass, 'container')} onClick={this.close}>
        <div className={'content'}>
          <CloseButton
            isHidden={!showCloseButton}
            onClose={this.close}
          />
          <Image
            hasCaption={images[currentImage].caption.length > 0}
            image={images[currentImage].src}
            text={images[currentImage].caption}
          />
          <ImageCount
            isHidden={!showImageCount}
            text="Image"
            current={currentImage + 1}
            total={images.length}
          />
          <PreviousArrowButton
            isHidden={images.length <= 1}
            onPrevious={this.previous}
          />
          <NextArrowButton
            isHidden={images.length <= 1}
            onNext={this.next}
          />
          <PreviousButton
            isHidden={images.length <= 1}
            onPrevious={this.previous}
          />
          <NextButton
            isHidden={images.length <= 1}
            onNext={this.next}
          />
        </div>
      </div>
    );
  }

  render() {
    const { isOpen } = this.props;

    setBrowserScrollable(!isOpen);

    return isOpen ? this.renderLightbox() : this.renderEmpty();
  }
}

Lightbox.propTypes = {
  containerClass: PropTypes.string,
  images: PropTypes.arrayOf(PropTypes.object),
  isOpen: PropTypes.bool,
  showCloseButton: PropTypes.bool,
  showImageCount: PropTypes.bool,
  closeOnEscape: PropTypes.bool,
  openOnEnter: PropTypes.bool,
  keyboardDirections: PropTypes.bool,
  currentImage: PropTypes.number,
  onNext: PropTypes.func,
  onPrevious: PropTypes.func,
  onClose: PropTypes.func,
  onOpen: PropTypes.func
};

Lightbox.defaultProps = {
  containerClass: '',
  images: [],
  isOpen: false,
  showCloseButton: true,
  showImageCount: true,
  closeOnEscape: true,
  openOnEnter: false,
  keyboardDirections: true,
  currentImage: 0,
  onNext: () => {},
  onPrevious: () => {},
  onClose: () => {},
  onOpen: () => {}
};

export default Lightbox;

/**
 * Renders Close button based on isHidden Property
 * @return {object} Either null or JSX to be rendered
 */
const CloseButton = ({ isHidden = false, onClose }) => {
  if (isHidden) {
    return null;
  }

  return (
    <button
      onClick={onClose}
      className={classNames('close')}
    />
  );
};

CloseButton.propTypes = {
  isHidden: PropTypes.bool,
  onClose: PropTypes.func
};

CloseButton.defaultProps = {
  isHidden: false,
  onClose: () => {}
};

/**
 * Renders Previous Button based on isHidden Property
 * @return {object} Either null or JSX to be rendered
 */
const PreviousButton = ({ isHidden = false, onPrevious }) => {
  if (isHidden) {
    return null;
  }

  return (
    <button
      type="button"
      onClick={onPrevious}
      className={classNames('direction', 'previous')}
    >
      Previous
    </button>
  );
};

PreviousButton.propTypes = {
  isHidden: PropTypes.bool,
  onPrevious: PropTypes.func
};

PreviousButton.defaultProps = {
  isHidden: false,
  onPrevious: () => {}
};

/**
 * Renders Next Button based on isHidden Property
 * @return {object} Either null or JSX to be rendered
 */
const NextButton = ({ isHidden = false, onNext }) => {
  if (isHidden) {
    return null;
  }

  return (
    <button
      type="button"
      onClick={onNext}
      className={classNames('direction', 'next')}
    >
      Next
    </button>
  );
};

NextButton.propTypes = {
  isHidden: PropTypes.bool,
  onNext: PropTypes.func
};

NextButton.defaultProps = {
  isHidden: false,
  onNext: () => {}
};

/**
 * Renders Previous Arrow Button based on isHidden Property
 * @return {object} Either null or JSX to be rendered
 */
const PreviousArrowButton = ({ isHidden = false, onPrevious }) => {
  if (isHidden) {
    return null;
  }

  return (
    <button
      onClick={onPrevious}
      className={classNames('arrow-direction', 'previous')}
    />
  );
};

PreviousArrowButton.propTypes = {
  isHidden: PropTypes.bool,
  onPrevious: PropTypes.func
};

PreviousArrowButton.defaultProps = {
  isHidden: false,
  onPrevious: () => {}
};

/**
 * Renders Next Arrow Button based on isHidden Property
 * @return {object} Either null or JSX to be rendered
 */
const NextArrowButton = ({ isHidden = false, onNext }) => {
  if (isHidden) {
    return null;
  }

  return (
    <button
      onClick={onNext}
      className={classNames('arrow-direction', 'next')}
    />
  );
};

NextArrowButton.propTypes = {
  isHidden: PropTypes.bool,
  onNext: PropTypes.func
};

NextArrowButton.defaultProps = {
  isHidden: false,
  onNext: () => {}
};
