import 'babel-polyfill';
import 'es6-promise/auto';

import $ from 'jquery';
import _ from 'lodash';
import enquire from 'enquire.js';
import bowser from 'bowser';

function centerSlickArrowNavigation($el) {
  let slideHeight = $('.slide img').height(),
    topPosition = slideHeight / 2;

  $el.css({ top: topPosition });
}

function processCopyLink(target, message) {
  let $target = $(target),
    $tooltip = $(`<div class="clip-tooltip">${message}</div>`);

  $tooltip.appendTo('body');

  $tooltip
    .css({
      top: $target.offset().top - ($target.outerHeight() + 16),
      left: $target.offset().left - $tooltip.width() / 2,
    })
    .fadeIn(150);

  _.delay(() => {
    $tooltip.fadeOut(() => {
      // $tooltip.remove();
    });
  }, 500);
}

$(() => {
  const buttons = $('.btn.sticky');
  const buttonPos = buttons.offset();

  const stickyButton = function () {
    const windowTop = $(window).scrollTop();

    if (windowTop >= buttonPos.top) {
      buttons.addClass('stick');
    } else {
      buttons.removeClass('stick');
    }
  };

  if (buttons.length !== 0) {
    $(window).scroll(stickyButton);
    stickyButton();
  }
});

$(() => {
  /**
   * Not in Used anymore, switch back to AddThis
   */
  // clipboard btns
  // new Clipboard('.btn-clip')
  //     .on('success', function(e) {

  //         console.log('copied successfully');
  //         var el = e.trigger,
  //             msg = (!!window.timelineWebsiteRuntimeConfig) ? window.timelineWebsiteRuntimeConfig.uiStrings.linkCopied : 'Copied to clipboard';

  //         processCopyLink(el, msg);

  //     })
  //     .on('error', function(e) {

  //         console.log('unable to copy');
  //         var el = e.trigger,
  //             msg = (!!window.timelineWebsiteRuntimeConfig) ? window.timelineWebsiteRuntimeConfig.uiStrings.linkCopiedError : 'Unable to copy';

  //         processCopyLink(el, msg);

  //     });

  // Sticky content
  const $stickyContent = $('[data-sticky-container] > [data-sticky]');
  if ($stickyContent.length) {
    $(window).on(
      'resize',
      _.debounce(
        () => {
          $stickyContent.foundation('_setSticky').foundation('_calc', false);
        },
        100,
        { leading: false, trailing: true },
      ),
    );
  }

  // fitVids
  const $supportingAsset = $('.supporting-asset-viewer');

  if ($($supportingAsset).length) {
    $supportingAsset.fitVids();
  }

  // slider //

  const $slider = $('.slider');

  $slider.on('init', (evt, slick) => {
    let slideCount = slick.$slides.length,
      $dots = $(slick.$dots),
      btnWrapperClass = `slick-dots-count-${slideCount}`;

    $dots.wrap(`<div class='slick-dots-wrapper' id=${btnWrapperClass}></div>`);
  });

  $slider.on('setPosition', () => {
    const $slickArrows = $('.slick-arrow');

    centerSlickArrowNavigation($slickArrows);

    $(window).on(
      'resize',
      _.debounce(
        () => {
          centerSlickArrowNavigation($slickArrows);
        },
        250,
        { leading: false, trailing: true },
      ),
    );
  });

  // init slider
  $slider.slick({
    dots: true,
    arrows: true,
    mobileFirst: true,
    infinite: true,
    rows: 0,
    lazyLoad: 'progressive',
    customPaging(slider, i) {
      let $el = $(slider.$slides[i]),
        buttonText = $el.data('button-text') || '',
        buttonMarkup = '<button type="button" class="cp-track" data-role="none" data-event="generic-event" data-category="carousel" data-action="thumbnail click" data-label="{buttonTextLabel}">\n    <div class="vertical-align">{buttonText}</div>\n</button>'
          .replace('{buttonText}', buttonText)
          .replace('{buttonTextLabel}', buttonText);

      return buttonMarkup;
    },
  });

  // Set timeline filter based on banner sselection
  const $timelineFilteredSliders = $slider.find('.slide > a[data-timeline-filter=True]');

  if ($timelineFilteredSliders.length > 0) {
    let isDragged = false;

    // Modify URL for filtered view to match the filtered items
    _.each($timelineFilteredSliders, ($slide) => {
      var $slide = $($slide),
        topics = $slide.data('timelineTopics'),
        types = $slide.data('timelineItemTypes');

      const newUrl = [];

      // Add location
      newUrl.push('{location}#/?'.replace('{location}', window.location.pathname));

      if (!_.isEmpty(topics)) {
        newUrl.push('&topics={topics}'.replace('{topics}', topics.split('|').join(',')));
      }

      if (!_.isEmpty(types)) {
        newUrl.push('&itemTypes={itemTypes}'.replace('{itemTypes}', types.split('|').join(',')));
      }

      // Build URL & set new attributes
      $slide.attr('href', newUrl.join(''));
    });

    $timelineFilteredSliders.on({
      mousedown() {
        isDragged = false;
      },
      mousemove() {
        isDragged = true;
      },
      click(e) {
        e.preventDefault();

        if (isDragged) {
          return;
        }

        let $slide = $(this),
          topics = $slide.data('timelineTopics'),
          types = $slide.data('timelineItemTypes'),
          $resetButton = '.filters button.reset-btn',
          $filtersTopics = '.filters .topics li>button[data-codename={codeName}]',
          $filtersObject = '.filters .objects li>button[data-codename={codeName}]',
          actions = [];

        const scrollToTimeline = function () {
          $('html, body').animate({ scrollTop: $('#timeline-container').offset().top }, 1000);
        };

        // Execute Reset Button
        $($resetButton).first().click();
        scrollToTimeline();
        if (!_.isEmpty(topics)) {
          topics = topics.split('|');
          _.each(topics, (topic) => {
            actions.push($filtersTopics.replace('{codeName}', topic));
          });
        }

        if (!_.isEmpty(types)) {
          types = types.split('|');
          _.each(types, (type) => {
            actions.push($filtersObject.replace('{codeName}', type));
          });
        }

        // execute actions
        let executeActions;

        executeActions = setTimeout(() => {
          if (
            (window.location.hash.indexOf('&topic=') ||
              window.location.hash.indexOf('&itemType=')) === -1
          ) {
            $(actions.join(',')).click();
            executeActions = null;
          }
        }, 50);
      },
    });
  }

  setTimeout(() => {
    // adding tab keyboard function to social icons
    $('.social-share').each(function () {
      const socialIcons = this;

      $(socialIcons).find('a').focus(() => {
        $(socialIcons).parent().parent().find('.item').addClass('hover');
      });

      $(socialIcons).find('a').blur(() => {
        $(socialIcons).parent().parent().find('.item').removeClass('hover');
      });
    });

    $('.share').hover(
      function () {
        $(this).parent().find('.item').addClass('hover');
      },
      function () {
        $(this).parent().find('.item').removeClass('hover');
      },
    );
  }, 2000);

  // Mobile only supporting asset viewer
  let $mobileAssetViewer = $('.supporting-asset-viewer'),
    $filterButton = $('.filter-button'),
    $filtersContainer = $('.filters-container'),
    $filters = $('.filters'),
    $filtersOverlay = $('<div class="filters-overlay"></div>'),
    $closeFilters = $('#close-filters'),
    $filtersCloseBtn = $('.close-modal'),
    $mainNav = $('.main-nav'),
    $mainNavOverlay = $('.menu-overlay'),
    $menuBtnOpen = $('.menu-btn-open'),
    $menuBtnClose = $mainNav.find('.menu-btn-close'),
    $itemsContainer = $('#items-grid');

  $filtersContainer.attr('data-closemenuonclick', true);
  $filters.fixTo('body', { useNativeSticky: false });

  hideFilters();

  function hideFilters() {
    if ($($filters).hasClass('closed')) {
      setTimeout(() => {
        $('.topics, .objects, .btns-container, #close-filters').addClass('display-none');
      }, 400);
    } else {
      $('.topics, .objects, .btns-container, #close-filters').removeClass('display-none');
    }
  }

  function enableDesktopJS() {
    const ua = bowser._detect(navigator.userAgent);

    if (ua.ios !== undefined || ua.android !== undefined) {
      $('.timeline').addClass('display-none');
      $('.items').addClass('no-hover');
      $('.related-items').addClass('no-hover');
      $('.share').addClass('no-hover');
    }

    $filterButton.unbind('click');

    $filtersContainer.append($filtersOverlay);

    $closeFilters.click(() => {
      $filters.addClass('closed');
      hideFilters();

      // GA Tag Manager Code //
      if (window.dataLayer) {
        window.dataLayer.push({
          event: 'generic-event',
          eventCategory: 'timeline filter',
          eventAction: 'open filter',
          eventLabel: 'not set',
        });
      }
    });

    // filter toggle
    $filterButton.click(() => {
      $filters.toggleClass('closed');

      let filtersAreOpen = !$filters.hasClass('closed'),
        filtersOffset = $('.filters-container').offset().top;

      if (!$filters.hasClass('fixto-fixed')) {
        $('body, html').animate({ scrollTop: filtersOffset });
      }

      if (filtersAreOpen) {
        $filtersOverlay.show();
      } else {
        $filtersOverlay.hide();
      }

      hideFilters();
    });

    $filterButton.hover(() => {
      $filters.toggleClass('hovered');
    });

    $('.timeline').fixTo('body', { useNativeSticky: false });

    $(document).scroll((e) => {
      // grab the scroll amount and the window height
      let scrollAmount = $(window).scrollTop(),
        documentHeight = $(document).height(),
        disruptiveEmailWasClosed = sessionStorage.disruptiveEmailWasClosed;

      // calculate the percentage the user has scrolled down the page
      const scrollPercent = scrollAmount / documentHeight * 100;

      if (scrollPercent > 50 && !disruptiveEmailWasClosed) {
        showDisruptiveEmailSignup();
      }

      function showDisruptiveEmailSignup() {
        const $disruptiveEmail = $('.disruptive-email');
        $disruptiveEmail.addClass('show');
      }
    });

    $('.disruptive-email .close').click(function () {
      $(this).parent().removeClass('show');
      sessionStorage.disruptiveEmailWasClosed = 'yes';
    });

    // filters overlay //

    $filtersOverlay.on('click', (evt) => {
      $filtersOverlay.hide();
      $filters.addClass('closed');
      $('body').removeClass('no-scroll');
      hideFilters();
    });
  }

  function enableMobileJS() {
    $filtersCloseBtn.trigger('click');
    $filters.removeClass('hovered');
    $filters.addClass('closed');

    $filtersOverlay.remove();

    $filtersContainer.on('click', (evt) => {
      let target = $(evt.target),
        closeMenuOnClick = target.data('closemenuonclick');

      if (closeMenuOnClick) {
        $filtersCloseBtn.trigger('click');
      }
    });

    // filter toggle
    $filterButton.unbind('click mouseenter mouseleave');

    $filterButton.click(() => {
      $filtersContainer.addClass('mobile-overlay');
      $itemsContainer.fadeOut(150);

      /**
       * Added delay to prevent iOS from crashing
       * position:fixed would repaint every object which cause the browser to crash
       * but removing position: fixed would make the background scrollable on iOS
       *
       * We've added a small delay to make sure that the items are hidden first before
       * position: fixed is place in the body, every item that has display: none in there
       * will not be repainted by the browser.                 *
       */
      _.delay(() => {
        $('body').addClass('no-scroll');
      }, 250);

      $('.topics, .objects, .btns-container, #close-filters').removeClass('display-none');
    });

    $filtersCloseBtn.click(() => {
      $filtersContainer.removeClass('mobile-overlay');
      $itemsContainer.fadeIn(150);
      $('body').removeClass('no-scroll');
      hideFilters();
      $filters.fixTo('refresh');
    });
  }

  enquire.register('screen and (min-width: 961px)', {
    // Code for Desktop //
    match() {
      enableDesktopJS();
    },
    unmatch() {
      enableMobileJS();
    },
  });

  enquire.register('screen and (max-width: 960px)', {
    // Code for Mobile //
    match() {
      enableMobileJS();
    },
    unmatch() {
      enableDesktopJS();
    },
  });

  enquire.register('screen and (max-width: 639px)', {
    // Code for Mobile //
    match() {
      $mobileAssetViewer.slick({
        dots: false,
        arrows: true,
        mobileFirst: true,
        infinite: true,
        lazyLoad: 'progressive',
        adaptiveHeight: true,
      });
    },
    unmatch() {
      $mobileAssetViewer.slick('unslick');
    },
  });

  // mobile menu button
  $mainNavOverlay.attr('data-closemenuonclick', true);

  $menuBtnOpen.click(() => {
    $mainNav.addClass('open');
    $mainNavOverlay.addClass('open');
    $('body').addClass('no-scroll');
  });

  $menuBtnClose.click(() => {
    $mainNav.removeClass('open');
    $mainNavOverlay.removeClass('open');
    $('body').removeClass('no-scroll');
  });

  $mainNavOverlay.on('click', (evt) => {
    let target = $(evt.target),
      closeMenuOnClick = target.data('closemenuonclick');

    if (closeMenuOnClick) {
      $menuBtnClose.trigger('click');
    }
  });

  // file upload form input with custom styles
  $('.file-upload').each(function () {
    let $input = $(this),
      $label = $input.next('label'),
      labelVal = $label.html();

    $input.on('change', (e) => {
      const fileName = e.target.value ? e.target.value.split('\\').pop() : '';

      if (fileName) {
        $('.file-uploaded').text(fileName);
      } else {
        $label.html(labelVal);
      }
    });

    // Firefox bug fix
    $input
      .on('focus', () => {
        $input.addClass('has-focus');
      })
      .on('blur', () => {
        $input.removeClass('has-focus');
      });
  });

  // submit email subscription

  $('.timeline-email-widget').each(function () {
    let $emailWidget = $(this),
      widgetType = $emailWidget.hasClass('disruptive-email') ? 'widget' : 'in line form';

    $emailWidget.find('button.close').click(function () {
      $emailWidget.hide();

      dataLayer.push({
        event: 'generic-event',
        eventCategory: 'newsletter submission',
        eventAction: 'close modal',
        eventLabel: 'true',
      });

      const path = $(this).data('path');
      const hidePromotionNDays = $(this).data('hidepromotionndays');
      const cookieKey = $(this).data('cookie-key');

      $.post(
        `/api/promotionalbanner/post?path=${
          path
          }&hidePromotionNDays=${
          hidePromotionNDays
          }&cookieKey=${
          cookieKey}`,
      );

      return false;
    });

    $emailWidget.find('input[type="text"]').keydown((event) => {
      if (event.keyCode == 13) {
        $('button[type="submit"].red-btn').click();
        event.preventDefault();
      }
    });

    $emailWidget.find('button[type="submit"].red-btn').click(() => {
      const dataToSubmit = {};
      const emailclientType = $emailWidget.data('emailclient-type');
      dataToSubmit.ListKey =
        emailclientType == 'campaigner'
          ? (`${$emailWidget.data('mailing-list')}`).split(',')
          : $emailWidget.find('input[type="hidden"]').val();
      dataToSubmit.CustomFields = $emailWidget.data('custom-fields');
      dataToSubmit.Email = $emailWidget.find('input[type="text"]').val();
      dataToSubmit.LearnMore = $emailWidget.find('input[type="checkbox"]').prop('checked');

      $('label.error').hide();
      $emailWidget.find('input[type="text"]').removeClass('error');

      $.ajax({
        type: 'POST',
        url: '/api/emailclient/subscribe',
        data: dataToSubmit,
        success(data, statusText, xhr) {},
        error(xhr, status) {},
        statusCode: {
          201() {
            $emailWidget.find('div.input').hide();
            $emailWidget.find('div.thank-you-message').show();
          },
          202() {
            $emailWidget.find('div.input').hide();
            $emailWidget.find('div.duplicate-message').show();
          },
          204() {
            $emailWidget.find('div.input').hide();
            $emailWidget.find('div.thank-you-message').show();
          },
          400(response) {
            const listError = response.responseJSON.ModelState['item.List'];
            if (listError != undefined) {
              $emailWidget.find('label.list-error').text(listError);
              $emailWidget.find('label.list-error').show();
            }

            const emailError = response.responseJSON.ModelState['item.Email'];
            if (emailError != undefined) {
              $emailWidget.find('input[type="text"]').addClass('error');
              $emailWidget.find('label.email-error').text(emailError);
              $emailWidget.find('label.email-error').show();
            }
          },
          500(response) {
            $emailWidget.find('label.list-error').text(response.statusText);
            $emailWidget.find('label.list-error').show();
          },
        },
        complete(event) {
          if (event.status >= 200 && event.status < 300) {
            timeGAPushDataLayer(
              'generic-event',
              'newsletter submission',
              'complete',
              `timeline:${widgetType}`,
            );
            if (dataToSubmit.LearnMore) {
              timeGAPushDataLayer(
                'generic-event',
                'newsletter submission',
                'interested in learning more',
                'true',
              );
            }
          }
        },
        dataType: 'JSON',
      });

      return false;
    });
  });

  function initializeLightboxViewer() {
    // Get list of images on the pagge
    const images = $('.supporting-asset-viewer:not(.slick-slider) img');

    // If not images are found
    if (_.isEmpty(images)) {
      return;
    }

    return import(/* webpackChunkName: "lightbox-gallery" */ 'site/react.components').then(
      ({ createGallery }) => {
        // Ready image information
        const imagesInfo = [...images].reduce((source, value) => {
          source.push({
            src: value.src,
            caption: value.alt,
          });
          return source;
        }, []);

        // Create Lightbox Gallery Div
        const lightbox = $('<div>', {
          id: 'lightbox-root',
        });

        $('body').append(lightbox);

        // Initialize lightBox
        const lightboxObj = createGallery(imagesInfo, lightbox[0], 0, false);

        return { obj: lightboxObj, images };
      },
    );
  }

  function enableLightboxViewer(lightboxViewer) {
    lightboxViewer.images.css('cursor', 'pointer');

    // Set images click event
    lightboxViewer.images.on('click', (e) => {
      e.preventDefault();

      const selectedImage = $(e.target);
      const currentImage = lightboxViewer.images.index(selectedImage[0]);

      lightboxViewer.obj.open(currentImage);
    });
  }

  function disableLightboxViewer(lightboxViewer) {
    // Close lightbox
    lightboxViewer.obj.close();

    // unbind events
    lightboxViewer.images.off('click');

    lightboxViewer.images.css('cursor', false);
  }

  const lightboxViewer = initializeLightboxViewer();

  if (lightboxViewer !== undefined) {
    lightboxViewer.then((lightbox) => {
      if ($(window).width() >= 640 && !_.isUndefined(lightbox)) {
        enableLightboxViewer(lightbox);
      }

      /**
       * Enable/Disable lightbox on mobile
       */
      if (!_.isUndefined(lightbox)) {
        $(window).on(
          'resize',
          _.debounce(
            (e) => {
              if ($(window).width() >= 640) {
                enableLightboxViewer(lightbox);
              } else {
                disableLightboxViewer(lightbox);
              }
            },
            100,
            { leading: false, trailing: true },
          ),
        );
      }
    });
  }

  function timeGAPushDataLayer(eventName, eventCategory, eventAction, eventLabel) {
    dataLayer.push({
      event: eventName,
      eventCategory,
      eventAction,
      eventLabel,
    });
  }

  $('.new-window a').attr('target', '_blank');

  $(document).foundation();
});
