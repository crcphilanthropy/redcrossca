import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Lightbox from 'site/components/lightbox';

export class Gallery extends Component {
  constructor(props) {
    super(props);

    const {
      isOpen,
      currentImage
    } = props;

    this.state = {
      isOpen: isOpen === undefined ? false : isOpen,
      currentImage: currentImage || 0
    };

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }

  open(index = 0) {
    this.setState({
      currentImage: index,
      isOpen: true
    });
  }

  close() {
    this.setState({
      currentImage: 0,
      isOpen: false
    });
  }

  next() {
    const { images } = this.props;

    this.setState({
      currentImage: this.state.currentImage === (images.length - 1)
        ? 0
        : this.state.currentImage + 1
    });
  }

  previous() {
    const { images } = this.props;

    this.setState({
      currentImage: this.state.currentImage === 0
        ? images.length - 1
        : this.state.currentImage - 1
    });
  }

  render() {
    const { images, containerClass } = this.props;

    return (
      <Lightbox
        containerClass={containerClass}
        images={images}
        isOpen={this.state.isOpen}
        currentImage={this.state.currentImage}
        showCloseButton
        showImageCount
        onNext={this.next}
        onPrevious={this.previous}
        onClose={this.close}
        onOpen={this.open}
        enableKeyboardDirection
        openOnEnter
      />
    );
  }
}

Gallery.propTypes = {
  containerClass: PropTypes.string,
  isOpen: PropTypes.bool,
  images: PropTypes.arrayOf(PropTypes.object).isRequired,
  currentImage: PropTypes.number.isRequired
};

Gallery.defaultProps = {
  containerClass: '',
  isOpen: false
};

export default Gallery;
