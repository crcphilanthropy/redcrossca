(function() {

    if(window.detect) {

        var ua = window.detect.parse(navigator.userAgent),
            isUnsupportedBrowser = (ua && ua.browser.family === 'IE' && ua.browser.version <= 9);

        if (isUnsupportedBrowser) {

            if (!!window.timelineWebsiteRuntimeConfig) {
                location.replace(window.timelineWebsiteRuntimeConfig.fallbackPageUrl);
            }
        }
    }

}());