import $ from 'jquery';
function updateLayout(items) {
    
    items.each(function() {

        var $item= $(this),
            $itemDetails= $item.find('.item-details'),
            $itemBgImage= $item.find('.item-bg-img');

        // align elements //
        var width = $item.outerWidth(),
            height = $item.outerHeight(),
            minHeight = 520;

        $itemDetails.css({
            top: 42
        });

        if(height > minHeight) {

            $itemBgImage.css({
                height: '100%',
                left: width / 2 - 600
            });

        }
        else {

            $itemBgImage.css({
                width: '100%'
            });

        }

        // --- //

        // $item.find('a').on('click', function() {
        //     return false;
        // });

    });
}

$(function() {

    var $items= $('.timeline-item');
    updateLayout($items);

});