import $ from 'jquery';
import _ from 'lodash';
import ko from 'knockout';

import CRCTimelineConfig from './core/timeline.core';
import CRCTimelineUtils from './core/timeline.utils';
import CRCTimelineStateManager from './core/timeline.stateManager';

const CRCTimelineItemBackButton = (function() {

    'use strict';

    return function(runtimeConfig) {

        var self = this,
            config = new CRCTimelineConfig(runtimeConfig),
            stateManager = new CRCTimelineStateManager({ locationContextKeys: config.Settings.locationContextKeys }),
            dataContext = {
                filters: null,
                activeDate: null
            },
            hasActiveFilters = false,
            events = {
                dataContextReady: new CRCTimelineUtils.Signal()
            },
            urlRoot = config.Settings.siteUrlRootPath;

        self.backToUrl = null;

        // ----------------- //

        function generateBackToUrl() {

            var url = '',
                topicsArr = _.map(dataContext.filters.topics, 'codeName') || [],
                itemTypesArr = _.map(dataContext.filters.itemTypes, 'codeName') || [],
                activeYear = dataContext.activeDate.year,
                topics = topicsArr.join(','),
                itemTypes = itemTypesArr.join(',');

            if(topics.length > 0 && itemTypes.length > 0) {
                url = '#/?&' + config.Settings.locationContextKeys.topics + '=' + topics + '&' + config.Settings.locationContextKeys.itemTypes + '=' + itemTypes;
            }
            else if(topics.length > 0 && itemTypes.length === 0) {
                url = '#/?&' + config.Settings.locationContextKeys.topics + '=' + topics;
            }
            else if(topics.length === 0 && itemTypes.length > 0) {
                url = '#/?&' + config.Settings.locationContextKeys.itemTypes + '=' + itemTypes
            }
            else {
                url = '#/?';
            }

            // if(!!(activeYear)) {

            //     if(url.length > 4) {
            //         url += '&' + config.Settings.locationContextKeys.date + '=' + activeYear;
            //     }
            //     else {
            //         url = '#/?&' + config.Settings.locationContextKeys.date + '=' + activeYear;
            //     }

            // }

            return urlRoot + url;
        }

        function onDataContextReady() {

            if(hasActiveFilters) {
                self.backToUrl = generateBackToUrl();
            }
            else {
                self.backToUrl = urlRoot + '/#/?';
            }

            initializeKO();
        }

        function registerEventListeners() {

            events.dataContextReady.add(onDataContextReady);

        }

        function initializeKO() {

            ko.applyBindings(self, document.getElementById(config.Settings.parentElementId));
        }

        function init() {

            registerEventListeners();

            stateManager.loadSessionStorageContext(function(context) {

                var hasActiveFiltersContext = !!(context) && context.hasOwnProperty('activeFilters');

                if(hasActiveFiltersContext) {

                    dataContext.filters = context.activeFilters.data;
                    dataContext.activeDate = context.activeDate.data;
                    hasActiveFilters = !!(dataContext.filters);
                }

                stateManager.setSessionContext({ backToTimeline: true });

                events.dataContextReady.dispatch();

            });

        }

        // ----------------- //

        init();

    };

}());

//----------------------------------------- //

$(function () {

    try {

        new CRCTimelineItemBackButton(crcItemBackButtonRunTimeConfig);

    }
    catch (ex) {

        // error initializing application.
        console.error('Error Initializing Timeline Application \n', ex);

    }

});
