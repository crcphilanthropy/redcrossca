import $ from 'jquery';
import _ from 'lodash';
import ko from 'knockout';

import CRCTimelineConfig from './core/timeline.core';
import CRCTimelineUtils from './core/timeline.utils';
import CRCTimelineModels from './core/timeline.models';
import CRCTimelineStateManager from './core/timeline.stateManager';
import CRCTimelineDataStore from './core/timeline.datastore';

const CRCTimelinePrevNextItem = (function() {

    'use strict';

    return function(runtimeConfig) {

        var self = this,
            config = new CRCTimelineConfig(runtimeConfig),
            stateManager = new CRCTimelineStateManager({ locationContextKeys: config.Settings.locationContextKeys }),
            events = {
                dataContextReady: new CRCTimelineUtils.Signal()
            },
            dataStore = new CRCTimelineDataStore({
                url: config.Settings.dataUrls.timeline,
                urlParams: {
                    siteName: config.Settings.siteName,
                    lang: config.Settings.currentCulture
                }
            }),
            previousItem = null,
            nextItem = null,
            dataContext = [];

        // ----------------- //

        self.errorOccurred = false;
        self.previousItemUrl = null;
        self.nextItemUrl = null;

        // ----------------- //

        function prepareData() {

            // prep items & dates
            _.each(dataStore.context.timelineItems, function(item) {
                dataContext.push(new CRCTimelineModels.Item(item));
            });

            stateManager.setSessionContext({ activeItems: dataContext });

            events.dataContextReady.dispatch();

        }

        function initializeKO() {
            ko.applyBindings(self, document.getElementById(config.Settings.parentElementId));
        }

        function onDataLoaded(evt) {

            if (evt.status === 'success') {

                // parse data
                prepareData();

            }
        }

        function onDataLoadingError(evt) {

            if(evt.status === 'error') {

                self.encounteredError = true;

                // ToDo: Add Error Handling Logic
                console.error('Error Initializing Application');
            }

            initializeKO();

        }

        function onDataContextReady() {

            var itemIndex = _.findKey(dataContext, { id: config.Settings.currentDocumentGuid }) * 1,
                totalItems = dataContext.length;

            if(_.isNumber(itemIndex) && itemIndex <= totalItems ) {

                previousItem = dataContext[itemIndex - 1];
                nextItem = dataContext[itemIndex + 1];

                self.previousItemUrl = !!(previousItem) ? previousItem.url : null;
                self.nextItemUrl = !!(nextItem) ? nextItem.url : null;

            }
            else {

                self.errorOccurred = true;

            }

            initializeKO();

        }

        function registerEventListeners() {
            dataStore.events.data.loaded.add(onDataLoaded);
            dataStore.events.data.error.add(onDataLoadingError);
            events.dataContextReady.add(onDataContextReady);
        }

        function init() {

            registerEventListeners();

            stateManager.loadSessionStorageContext(function(context) {

                var hasActiveItemsContext = !!context && context.hasOwnProperty('activeItems');

                if(hasActiveItemsContext) {

                    if (_.isEmpty(context.activeItems.data)) {

                        dataStore.loadData();

                    }
                    else {

                        dataContext = context.activeItems.data;
                        events.dataContextReady.dispatch();

                    }
                }

            });

        }

        // ----------------- //

        init();

    };

}());

//----------------------------------------- //

$(function () {

    try {

        new CRCTimelinePrevNextItem(crcPrevNextItemRuntimeConfig);

    }
    catch (ex) {

        // error initializing application.
        console.error('Error Initializing Timeline Application \n', ex);

    }

});
