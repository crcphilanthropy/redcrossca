import $ from 'jquery';
import _ from 'lodash';

import CRCTimelineUtils from './timeline.utils.js';

export default (function () {

    'use strict';

    return function (opts) {

        var self = this,
            settings = _.extend({url: null, urlParams: null, supportsSessionStorage: false, useSessionCaching: false}, opts),
            cacheData = !!(settings.supportsSessionStorage && settings.useSessionCaching);

        // ------------- //

        self.context = {
            timelineItems: [],
            timelineTopics: [],
            timelineItemTypes: []
        };

        self.events = {
            data: {
                loaded: new CRCTimelineUtils.Signal(),
                error: new CRCTimelineUtils.Signal()
            }
        };

        self.loadData = function () {

            var cachedContext = (cacheData) ? sessionStorage.getItem('timeline.datacontext') : null;

            if (!cachedContext) {

                $.ajax({
                    url: settings.url,
                    data: settings.urlParams,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data, status) {

                        self.context.timelineItems = data.items;
                        self.context.timelineTopics = data.topics;
                        self.context.timelineItemTypes = data.itemTypes;

                        if (cacheData) {
                            sessionStorage.removeItem('timeline.datacontext');
                            sessionStorage.setItem('timeline.datacontext', JSON.stringify(self.context));
                        }

                        self.events.data.loaded.dispatch({status: 'success'});

                    },
                    error: function (evt, status) {

                        self.context = null;
                        self.events.data.error.dispatch({status: 'error'});

                    }
                });

            }
            else {

                self.context = JSON.parse(cachedContext);
                self.events.data.loaded.dispatch({status: 'success'});

            }

        };

    };

}());

// ----------------------------------------- //
