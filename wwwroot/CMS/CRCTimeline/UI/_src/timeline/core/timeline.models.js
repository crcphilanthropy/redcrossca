import _ from 'lodash';
import ko from 'knockout';

export default (function () {

    'use strict';

    // --------------- //

    var timelineFilterTypes = {
        topics: 'topics',
        itemTypes: 'objects'
    };

    function parseRelations(relationships) {

        var filterRelationships = [];

        _.each(relationships, function(relationshipInfo) {

            filterRelationships.push(new FilterRelationshipInfo(relationshipInfo))

        });

        return filterRelationships;

    }

    function resolveItemDecade(year) {
        return year - (year % 10);
    }

    // --------------- //

    function Item(dataObj) {

        var self = this,
            props = dataObj || {};

        function resolveThumbnailInfo(infoObj) {

            if(infoObj) {

                var data = {};
                data.src = infoObj.filePath.replace('~', '');
                data.alt = infoObj.altText;

                return data;

            }

            return null;

        }

        // ------------ //

        self.id = props.documentGuid || null;
        self.title = props.title || null;
        self.url = props.url || null;
        self.absoluteUrl = props.absoluteUrl || null;
        self.thumbnail = resolveThumbnailInfo(props.thumbnailImage);
        self.editorialDate = props.editorialDate || null;
        self.year = props.sortingDate || null;
        self.decade = resolveItemDecade(self.year);
        self.description = props.shortDescription || null;
        self.url = props.url || null;
        self.itemType = props.itemType || null;
        self.topics = props.topics || null;
    }

    function TimelineFilter(dataObj) {

        var self = this,
            props = dataObj || {};

        // ------------ //

        self.id = props.id || null;
        self.codeName = props.codeName || null;
        self.type = props.type || null;
        self.displayName = props.displayName || null;
        self.selected = ko.observable(false);
        self.enabled = ko.observable(true);
        self.filterRelationships = parseRelations(props.filterRelationships);

    }

    function TimelineDate(dataObj) {

        var self = this,
            props = dataObj || {};

        // ------------ //

        self.year = props.year || null;
        self.displayName = props.year + 's';
        self.isDisabled = ko.observable(false);
        self.isActive = ko.observable(false);

    }

    function FilterRelationshipInfo(dataObj) {

        var self = this,
            props = dataObj || {};

        self.id = props.key || null;
        self.count = props.count || 0;

    }

    // ------------------ //

    return {
        Item: Item,
        TimelineDate: TimelineDate,
        TimelineFilter: TimelineFilter,
        TimelineFilterTypes: timelineFilterTypes
    };

}());

// ----------------------------------------- //