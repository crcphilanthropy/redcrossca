import _ from 'lodash';
import signals from 'signals';

export default (function () {

    'use strict';

    return {
        Signal: signals.Signal,
        valueInArray: function(value, arr) {
            return (_.indexOf(arr, value) !== -1);
        },
        valuesInArray: function(values, arr) {

            for (var i = 0; i < values.length; i++) {

                if (_.indexOf(arr, values[i]) !== -1) {
                    return true;
                }

            }

            return false;
        },
        executeCallback: function (fn, params) {

            if (fn && _.isFunction(fn)) {
                fn((params || {}));
            }

        },
        isElementInViewport: function(el) {

            if (typeof jQuery === "function" && el instanceof jQuery) {
                el = el[0];
            }

            var rect = el.getBoundingClientRect();

            return (
                rect.top >= -50 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        },
        googleDataLayer: {
            push: function(obj) {

                if((!!window.dataLayer) === false) {
                    window.dataLayer = [];
                }

                window.dataLayer.push(obj);

            },
            pushFiltersData: function(activeFilters, eventAction) {
                this.push({
                    'event': 'generic-event',
                    'eventCategory': 'timeline filter',
                    'eventAction': eventAction,
                    'eventLabel': (activeFilters !== "") ? activeFilters : 'not set'
                });

            }
        },
        profileMethod: function(callback) {

            console.info(new Date().getTime());

            if(callback && _.isFunction(callback)) {
                callback();
            }

            console.info(new Date().getTime());

        }
    };

}());

// ----------------------------------------- //
