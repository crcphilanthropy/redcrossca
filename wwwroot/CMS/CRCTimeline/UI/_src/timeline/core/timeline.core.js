import _ from 'lodash';

// ----------------------------------------- //

export default (function () {

    'use strict';

    return function Config(runtimeConfig) {

        var self = this,
            defaults = {
                supportsSessionStorage: !!(window.sessionStorage),
                useSessionCaching: false,
                parentElementId: null,
                currentCulture: 'en-CA',
                siteName: 'crc',
                siteUrlRootPath: '',
                dataUrls: {
                    timeline: '/api/timeline/getall'
                },
                locationContextKeys: {
                    topics: 'topics',
                    itemTypes: 'itemTypes',
                    date: 'date'
                }
            };

        self.Settings = Object.assign(defaults, runtimeConfig);

        // ------------------- //

    };

}());

// ----------------------------------------- //
