import $ from 'jquery';
import _ from 'lodash';

import Shuffle from 'shufflejs';
import 'velocity-animate';

import CRCTimelineUtils from './timeline.utils.js';

export default (function () {

  'use strict';

  return function () {

    var self = this,
      $appRoot = $('#timeline-container'),
      $loadingBg = $('<div class="loading"></div>'),
      $grid = $('#items-grid');


    self.initialTransitionIn = function (callback) {

      // animation logic
      $appRoot.append($loadingBg);

      // handle callback
      if (callback && _.isFunction(callback)) {
        CRCTimelineUtils.executeCallback(callback);
      }

    };

    self.initialTransitionOut = function () {

      // animation logic
      $loadingBg.slideUp(750, function () {
        $loadingBg.remove();
      });

    };

    self.initializeShuffle = function () {
      $.shuffleObj = new Shuffle($grid);
    };

    self.shuffle = function (callback) {
      $.shuffleObj.filter(callback);
    };

    self.shuffleAll = function () {
      $.shuffleObj.filter('all');
    };

    self.scrollToDate = function (date) {

      // find first $el with decade
      var selector = '.item-container.' + date.year + ':not(.concealed)',
        $elems = $(selector),
        $el = $elems.first();

      // find position on page
      if ($el !== null && $el.length > 0) {

        // scroll to position
        var animateTo = $el.offset().top;
        $('body, html').animate({ scrollTop: animateTo });

      }

    };
  };

}());

// ----------------------------------------- //