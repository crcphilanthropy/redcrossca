import _ from 'lodash';
import createHistory from 'history/createBrowserHistory';
import queryString from 'query-string';

import CRCTimelineUtils from './timeline.utils.js';

const history = createHistory();
const location = history.location;

export default (function() {

    'use strict';

    function LocationContext(configuration) {

        var self = this,
            config = _.extend({ contextKeys: { topics: 'topics', itemTypes: 'itemTypes', date: 'date' } }, configuration),
            context = {
                topics: {
                    key: config.contextKeys.topics,
                    data: []
                },
                itemTypes: {
                    key: config.contextKeys.itemTypes,
                    data: []
                },
                date: {
                    key: config.contextKeys.date,
                    data: null
                }
            };

        function setValue(key, value) {

            if(context.hasOwnProperty(key)) {
                context[key].data = value;
            }
            else {
                throw new Error('Unable to update: context[' + key + '] is not a valid property');
            }
        }

        function getValue(key) {

            if(context.hasOwnProperty(key)) {
                return context[key].data;
            }
            else {
                throw new Error('Unable to retrieve: context[' + key + '] is not a valid property');
            }

        }

        function parseContextItemData(data, key) {

            var value = null;

            if(_.isObject(data)) {

                value = _.map(data, key);

                if(value.length === 0)
                    value = null;
            }
            else {
                value = (data !== null) ? data : null;
            }

            return value;

        }

        function updateLocation() {

            _.delay(function() {

                var urlQueryParam = '',
                    locationInfo = '',
                    hasUrlQueryData = false,
                    objValue = null;

                _.forIn(context, function(obj, key) {

                    objValue = parseContextItemData(obj.data || null, 'codeName');

                    if(objValue !== null) {

                        hasUrlQueryData = true;
                        locationInfo += '&' + obj.key + "=" + objValue;

                    }

                });

                urlQueryParam = (locationInfo) ? '?' : '';

                history.push({
                    pathname: location.pathname,
                    hash: urlQueryParam + locationInfo
                });

            }, 150);

        }

        function parseDataString(dataStr) {

            var dataArr = !!(dataStr) ? dataStr.split(',') : '',
                isValid = _.isArray(dataArr) && dataArr.length > 0 && dataArr[0] !== '';

            return (isValid) ? dataArr : null;
        }

        function parseLocationData() {
            let queryParams = queryString.parse(location.hash);
            var params = {
                    topics: queryParams[context.topics.key] || null,
                    itemTypes: queryParams[context.itemTypes.key] || null,
                    date: queryParams[context.date.key] || null
                };

            return { topics: parseDataString(params.topics), itemTypes: parseDataString(params.itemTypes), date: params.date };

        }

        // ------------------ //

        self.events = {
            updated: new CRCTimelineUtils.Signal()
        };

        self.events.updated.add(function(evt) {

            updateLocation();

        });

        // ------------------ //

        self.loadContext = function() {

            var locationData = parseLocationData();

            context.topics.data = locationData.topics;
            context.itemTypes.data = locationData.itemTypes;
            context.date.data = locationData.date;

        };

        self.getContext = function() {
            return context;
        };

        self.setContext = function(opts) {

            _.forIn(opts, function(value, key) {
                setValue(key, value);
            });

            self.events.updated.dispatch();

        };

        self.setValue = function(key, value) {

            setValue(key, value);
            self.events.updated.dispatch();

        };

        self.getValue = function(key) {
            return getValue(key);
        };

    }

    function SessionStorageContext() {

        var self = this,
            context = {
                activeFilters: {
                    key: 'timeline.activeFilters',
                    data: {
                        topics: [],
                        itemTypes: []
                    }
                },
                activeItems: {
                    key: 'timeline.activeItems',
                    data: []
                },
                activeDate: {
                    key: 'timeline.activeDate',
                    data: null
                },
                backToTimeline: {
                    key: 'timeline.backToTimeline',
                    data: null
                }
            };

        function setValue(key, value) {

            if(context.hasOwnProperty(key)) {
                context[key].data = value;

            }
            else {
                throw new Error('Unable to update: context[' + key + '] is not a valid property');
            }

        }

        function getValue(key) {

            if(context.hasOwnProperty(key)) {
                return context[key];
            }
            else {
                throw new Error('Unable to retrieve: context[' + key + '] is not a valid property');
            }

        }

        function updateSessionStorage(contextKey) {

            var c = context[contextKey];

            if(!!(c)) {

                if(c.data !== null) {
                    sessionStorage.setItem(c.key, JSON.stringify(c.data))
                }
                else {
                    sessionStorage.removeItem(c.key);
                }
            }
        }

        // ------------------ //

        self.events = {
            updated: new CRCTimelineUtils.Signal()
        };

        self.events.updated.add(function(evt) {

            updateSessionStorage(evt.contextItemKey);

        });

        // ------------------ //

        self.loadContext = function() {

            _.forIn(context, function(contextItem) {

                contextItem.data = JSON.parse(sessionStorage.getItem(contextItem.key));

            });

        };

        self.getContext = function() {
            return context;
        };

        self.setContext = function(opts) {

            _.forIn(opts, function(value, key) {

                setValue(key, value);
                self.events.updated.dispatch({ contextItemKey: key });

            });

        };

        self.setValue = function(key, value) {
            setValue(key, value);
            self.events.updated.dispatch();
        };

        self.getValue = function(key) {
            return getValue(key);
        };

    }

    return function(configuration) {

        var self = this,
            config = configuration || null,
            locationContext = new LocationContext({ contextKeys: config.locationContextKeys }),
            sessionStorageContext = new SessionStorageContext();

        // ------------------ //

        self.getLocationStateContext = function(key) {
            return !!(key) ? locationContext.getValue(key) : locationContext.getContext();
        };

        self.loadLocationContext = function(callback) {

            locationContext.loadContext();

            CRCTimelineUtils.executeCallback(callback({
                topics: locationContext.getValue('topics'),
                itemTypes: locationContext.getValue('itemTypes'),
                date: locationContext.getValue('date')
            }));

        };

        self.setLocationContext = function(opts) {
            locationContext.setContext(opts);
        };

        self.setSessionContext = function(opts) {
            sessionStorageContext.setContext(opts);
        };

        self.loadSessionStorageContext = function(callback) {

            sessionStorageContext.loadContext();

            CRCTimelineUtils.executeCallback(callback, {
                activeItems: sessionStorageContext.getValue('activeItems'),
                activeFilters: sessionStorageContext.getValue('activeFilters'),
                activeDate: sessionStorageContext.getValue('activeDate')
            });

        };

        self.getSessionStorageContextValue = function(key) {

            var contextItem = sessionStorageContext.getValue(key);
            return JSON.parse(sessionStorage.getItem(contextItem.key));

        };

    };

}());