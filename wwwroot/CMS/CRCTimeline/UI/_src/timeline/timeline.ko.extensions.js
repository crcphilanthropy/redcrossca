import $ from 'jquery';
import _ from 'lodash';
import ko from 'knockout';

/**
 * Custom Knockout JS Biding - Wrapper for jQuery Lazy Load 1.9.7 plugin
 */
ko.bindingHandlers.lazyLoadImages = {
    update: function(element, valueAccessor) {

        var $el = $(element),
            opts = ko.unwrap(valueAccessor()),
            settings = _.extend({threshold : 200}, opts);

        $el.find('img.lazy').lazyload(settings);

        $(document).on('crc.timeline.filterUpdate', function() {
            console.log('crc.timeline.filterUpdate');
            $el.find('img.lazy').lazyload(settings);
        });

    }
};