import $ from 'jquery';
import _ from 'lodash';

import CRCTimelineUtils from '../core/timeline.utils.js';
import CRCTimelinePresenter from '../core/timeline.presenter';

export default (function() {

    'use strict';

    return function() {

        var self = this,
            presenter = new CRCTimelinePresenter(),
            filteredItems = [];

        function matchFilters(el, items, filters) {

            var data = el.data(),
                activeTopics = _.map(filters.topics, 'id') || [],
                activeItemTypes = _.map(filters.itemTypes, 'id') || [],
                item = _.find(items, { id: data.itemid }),
                noTopicMatchFound = activeTopics.length > 0 && !CRCTimelineUtils.valuesInArray(_.split(item.topics, ','), activeTopics),
                noItemTypeMatchFound =  activeItemTypes.length > 0 && !CRCTimelineUtils.valueInArray(item.itemType, activeItemTypes);

            if(!(noTopicMatchFound || noItemTypeMatchFound)) {
                filteredItems.push(item);
            }

            return !(noTopicMatchFound || noItemTypeMatchFound);

        }

        function filter(items, filters, callback) {

            if(filters.areActive) {

                filteredItems = [];

                presenter.shuffle(function($el) {

                    return matchFilters($($el), items, filters);

                });

                CRCTimelineUtils.executeCallback(callback, { filtered: true });

            }
            else {

                presenter.shuffleAll();
                CRCTimelineUtils.executeCallback(callback, { filtered: false });
            }

        }

        // --------------- //

        self.events = {
            items: {
                initialized: new CRCTimelineUtils.Signal(),
                filtered: new CRCTimelineUtils.Signal()
            }
        };

        self.items = [];

        self.update = function(opts) {

            var filters = { areActive: opts.filtersAreActive, topics: opts.topics, itemTypes: opts.itemTypes };

            filter(self.items, filters, function(params) {
                self.events.items.filtered.dispatch({ filtered: params.filtered, activeItems:  filteredItems });
            });

        };

        self.initialize = function(opts) {

            var filters = { areActive: opts.filtersAreActive, topics: opts.topics, itemTypes: opts.itemTypes };

            filter(self.items, filters, function(params) {
                self.events.items.initialized.dispatch({ filtered: params.filtered, activeItems:  filteredItems });
            });

        };

    }

}());