import _ from 'lodash';

import CRCTimelineUtils from '../core/timeline.utils';
import CRCTimelinePresenter from '../core/timeline.presenter';
import CRCTimelineModels from '../core/timeline.models';

export default (function() {

    'use strict';

    return function() {

        var self = this,
            presenter = new CRCTimelinePresenter(),
            _dates = [],
            dateMod = 10;

        function resolveDate(dateObj) {

            var year = dateObj || null,
                date = year - (year % dateMod);

            return (!_.isNaN(date)) ? date : null;
        }

        function resetDates(dates) {

            _.each(dates, function(date) {
                date.isDisabled(false);
                date.isActive(false);
            });

        }

        function clearDates(dates) {
            _.each(dates, function(date) {
                date.isActive(false);
            });
        }

        function enableActiveDates(dates, items) {

            var activeDates = _.map(items, 'decade');

            _.each(dates, function(date) {

                date.isDisabled(!CRCTimelineUtils.valueInArray(date.year, activeDates))

            });

        }

        function enableAllDates(dates) {

            _.each(dates, function(date) {

                date.isDisabled(false)

            });

        }

        function updateActiveDate(dates, activeYear, callback) {

            var availableDates = getAvailableDates(dates),
                activeDate = null,
                animateToDate = false;

            if(availableDates.length > 0) {

                activeDate = _.find(availableDates, { year: activeYear });

                if (!!(activeDate)) {
                    activeDate.isActive(true);
                    animateToDate = true;
                }
                else {
                    activeDate = availableDates[0];
                    activeDate.isActive(true);
                }

            }

            CRCTimelineUtils.executeCallback(callback, { activeDate: activeDate, animateToDate: animateToDate });

        }

        function getAvailableDates(dates, callback) {

            var availableDates = _.filter(dates, function(date) {
                return date.isDisabled() === false;
            });

            return availableDates;

        }

        function selectDate(date, opts) {

            var animate = opts.animate || false;

            if(!date.isDisabled() && !date.isActive()) {

                clearDates(self.dates);
                date.isActive(true);

                if(animate) {
                    presenter.scrollToDate(date);
                }

            }

        }

        // --------------- //

        self.events = {
            initialized: new CRCTimelineUtils.Signal(),
            updated: new CRCTimelineUtils.Signal()
        };

        self.dates = [];

        self.addDate = function(dateObj) {

            var year = resolveDate(dateObj);

            if(year && _.indexOf(_dates, year) === -1) {
                _dates.push(year);
                self.dates.push(new CRCTimelineModels.TimelineDate({ year: year }));
            }

        };

        self.initialize = function(opts) {

            var filtered = opts.filtered || false,
                activeItems = opts.activeItems || [],
                activeYear = opts.activeDate * 1;

            resetDates(self.dates);

            if(filtered) {
                enableActiveDates(self.dates, activeItems);
            }
            else {
                enableAllDates(self.dates);
            }

            updateActiveDate(self.dates, activeYear, function(evt) {

                if(evt.animateToDate) {
                    presenter.scrollToDate(evt.activeDate);
                }

                self.events.initialized.dispatch({ activeDate: evt.activeDate, animateToDate: evt.animateToDate });

            });

        };

        self.update = function(opts) {

            var filtered = opts.filtered || false,
                activeItems = opts.activeItems || [];

            resetDates(self.dates);

            if(filtered) {
                enableActiveDates(self.dates, activeItems);
            }
            else {
                enableAllDates(self.dates);
            }

            updateActiveDate(self.dates, null, function(evt) {
                self.events.updated.dispatch({ activeDate: evt.activeDate });
            });


        };

        self.selectDate = function(dateObj) {

            selectDate(dateObj, { animate: true });
            self.events.updated.dispatch({ activeDate: dateObj, animate: true });

        };

        self.selectDateByYear = function(year) {

            var date = _.find(self.dates, { year: year });

            selectDate(date, { animate: false });
            self.events.updated.dispatch({ activeDate: date, animate: false });

        };

        // --------------- //

    }

}());