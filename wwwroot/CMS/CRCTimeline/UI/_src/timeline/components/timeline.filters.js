import _ from 'lodash';

import CRCTimelineUtils from '../core/timeline.utils';
import CRCTimelineModels from '../core/timeline.models';

export default (function() {

    'use strict';

    return function() {

        var self = this,
            activeTopics = [],
            activeItemTypes = [];

        function updateActiveFilters(filter, activeFilterSet) {

            filter.selected(!filter.selected());
            var index = _.findIndex(activeFilterSet, function(i) { return i.id === filter.id });

            if(index >= 0) {
                _.pullAt(activeFilterSet, index);
            }
            else {
                activeFilterSet.push({ id: filter.id, codeName: filter.codeName });
            }

        }

        function updateFilterState(filter) {

            if(filter.type === CRCTimelineModels.TimelineFilterTypes.topics) {
                updateActiveFilters(filter, activeTopics);
            }

            if(filter.type === CRCTimelineModels.TimelineFilterTypes.itemTypes) {
                updateActiveFilters(filter, activeItemTypes);
            }

        }

        function updateFilterSetState(filterSet) {

            _.each(filterSet, function(filter){

                updateFilterState(filter);

            });

        }

        function parseFilterRelationships(targetFilterSet, relationshipFilterSet, activeFilterSet) {

            _.each(targetFilterSet, function(filter) {

                var count = 0;

                if(activeFilterSet.length > 0) {

                    _.each(activeFilterSet, function(activeFilter) {

                        var rel = _.find(relationshipFilterSet, { id: activeFilter.id });

                        _.each(rel.filterRelationships, function(item) {

                            if(filter.id === item.id) {
                                count += item.count;
                            }

                        });

                    });

                    filter.enabled((count > 0));

                }
                else {

                    filter.enabled(true);

                }

            });

        }

        function updateAvailableFilters() {

            parseFilterRelationships(self.topics, self.itemTypes, activeItemTypes);
            parseFilterRelationships(self.itemTypes, self.topics, activeTopics);

        }

        function resetFilterSet(filters) {

            _.each(filters, function(filter) {

                filter.selected(false);
                filter.enabled(true);

            });

            activeTopics = [];
            activeItemTypes = [];

        }

        function validateFilterSet(values, filters) {

            var arr = _.map(filters, 'id');
            return CRCTimelineUtils.valuesInArray(values, arr);
        }

        // --------------- //

        self.events = {
            filters: {
                initialized: new CRCTimelineUtils.Signal(),
                updated: new CRCTimelineUtils.Signal()
            }
        };
        self.topics = [];
        self.itemTypes = [];

        self.getActiveFiltersForGA = function() {

            var topics = _.map(activeTopics, function(topic) {
                    return 'Topic: ' + topic.codeName;
                }),
                itemTypes = _.map(activeItemTypes, function(itemType) {
                    return 'Object: ' + itemType.codeName;
                });

            return _.union(topics, itemTypes);

        };

        self.filtersAreActive = function() {
            return activeTopics.length > 0 || activeItemTypes.length > 0;
        };

        self.validateActiveFilters = function(topicValues, itemTypeValues) {

            var topicsAreValid = validateFilterSet(topicValues, self.topics),
                itemTypesAreValid = validateFilterSet(itemTypeValues, self.itemTypes);

            return { topicsAreValid: topicsAreValid, itemTypesAreValid: itemTypesAreValid };

        };

        self.update = function(opts) {

            var activeFilter = opts.activeFilter || null;

            if(activeFilter !== null) {
                updateFilterState(activeFilter);
                updateAvailableFilters();
            }

            self.events.filters.updated.dispatch({ filtersAreActive: self.filtersAreActive(), topics: activeTopics, itemTypes: activeItemTypes });

        };

        self.reset = function() {

            if(self.filtersAreActive()) {

                resetFilterSet(self.topics);
                resetFilterSet(self.itemTypes);

                self.events.filters.updated.dispatch({ filtersAreActive: false, topics: activeTopics, itemTypes: activeItemTypes });
            }

        };

        self.initialize = function(opts) {

            var preSelectedTopics = _.filter(self.topics, function(topic) {
                return CRCTimelineUtils.valueInArray(topic.codeName, opts.topics);
            });

            var preSelectedItemTypes = _.filter(self.itemTypes, function(itemType) {
                return CRCTimelineUtils.valueInArray(itemType.codeName, opts.itemTypes);
            });

            if(!!preSelectedTopics) {
                updateFilterSetState(preSelectedTopics);
            }

            if(!!preSelectedItemTypes) {
                updateFilterSetState(preSelectedItemTypes);
            }

            updateAvailableFilters();

            self.events.filters.initialized.dispatch({
                filtersAreActive: self.filtersAreActive(),
                topics: activeTopics,
                itemTypes: activeItemTypes
            });

        }
    }

}());