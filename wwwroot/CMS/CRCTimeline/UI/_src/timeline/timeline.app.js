import $ from 'jquery';
import _ from 'lodash';
import ko from 'knockout';

import CRCTimelineConfig from './core/timeline.core';
import CRCTimelineDataStore from './core/timeline.datastore';
import CRCTimelinePresenter from './core/timeline.presenter';
import CRCTimelineStateManager from './core/timeline.stateManager';
import CRCTimelineUtils from './core/timeline.utils';
import CRCTimelineModels from './core/timeline.models';

import CRCTimelineComponentsTimelineFilters from './components/timeline.filters'
import CRCTimelineComponentsTimelineDateSelector from './components/timeline.date-selector'
import CRCTimelineComponentsTimelineItemList from './components/timeline.item-list'

import 'site/libs/fixto.min.js';
import 'velocity-animate';

const CRCTimelineApp = (function () {

    'use strict';

    return function (runtimeConfig) {

        var self = this,
            config = new CRCTimelineConfig(runtimeConfig),
            presenter = new CRCTimelinePresenter(),
            stateManager = new CRCTimelineStateManager({ locationContextKeys: config.Settings.locationContextKeys }),
            dataStore = new CRCTimelineDataStore({
                url: config.Settings.dataUrls.timeline,
                urlParams: {
                    siteName: config.Settings.siteName,
                    lang: config.Settings.currentCulture
                }
            }),
            isClickAction = false,
            isBackToTimeline = false;

        // ----------------- //

        self.timelineFilters = new CRCTimelineComponentsTimelineFilters();
        self.timelineDateSelector = new CRCTimelineComponentsTimelineDateSelector();
        self.timelineItemList = new CRCTimelineComponentsTimelineItemList();

        self.encounteredError = false;

        self.filtersAreActive = ko.observable(false);

        self.onFilterChange = function(activeFilter) {

            if(activeFilter.enabled()) {

                self.timelineFilters.update({ activeFilter: activeFilter });

            }
            else if(activeFilter.selected() && !activeFilter.enabled()) {

                self.timelineFilters.update({ activeFilter: activeFilter });

            }

            CRCTimelineUtils.googleDataLayer.push({
                'event': 'generic-event',
                'eventCategory': 'timeline filter',
                'eventAction': activeFilter.type,
                'eventLabel': activeFilter.codeName
            });

        };

        self.onTimelineDateClick = function(activeDate) {

            isClickAction = true;
            self.timelineDateSelector.selectDate(activeDate);

            CRCTimelineUtils.googleDataLayer.push({
                'event': 'generic-event',
                'eventCategory': 'timeline filter',
                'eventAction': 'timeline click',
                'eventLabel': activeDate.year
            });

        };

        self.onFiltersReset = function() {

            self.timelineFilters.reset();

            CRCTimelineUtils.googleDataLayer.push({
                'event': 'generic-event',
                'eventCategory': 'timeline filter',
                'eventAction': 'reset filter',
                'eventLabel': 'not set'
            })

        };

        self.onFiltersApply = function() {

            var gaEventAction = 'apply filter';
            GA_PushFilterData(gaEventAction);

        };

        self.onFilterPanelToggle = function() {

            var $filters = $('.filters'),
                gaEventAction = $filters.hasClass('closed') ? 'close filter' : 'open filter';

            GA_PushFilterData(gaEventAction);

        };

        // ----------------- //

        function GA_PushFilterData(eventAction) {

            var activeFilters = self.timelineFilters.getActiveFiltersForGA().toString();
            CRCTimelineUtils.googleDataLayer.pushFiltersData(activeFilters, eventAction);

        }

        function onDataLoaded(evt) {

            if (evt.status === 'success') {

                // parse data
                prepareData();

                // initialize knockout
                initializeKO();

                // initialize app ui
                initializeUI();
            }
        }

        function onDataLoadingError(evt) {

            if(evt.status === 'error') {

                self.encounteredError = true;

                // ToDo: Add Error Handling Logic
                console.error('Error Initializing Application');
            }

            initializeKO();

        }

        function registerEventListeners() {

            dataStore.events.data.loaded.add(onDataLoaded);

            dataStore.events.data.error.add(onDataLoadingError);

            // -- Filters -- //

            self.timelineFilters.events.filters.initialized.add(function(evt) {

                stateManager.setLocationContext({ topics: evt.topics, itemTypes: evt.itemTypes });
                stateManager.setSessionContext({ activeFilters: { topics: evt.topics, itemTypes: evt.itemTypes } });

                self.timelineItemList.initialize({ filtersAreActive: evt.filtersAreActive, topics: evt.topics, itemTypes: evt.itemTypes });
                self.filtersAreActive(evt.filtersAreActive);

            });

            self.timelineFilters.events.filters.updated.add(function(evt) {

                stateManager.setLocationContext({ topics: evt.topics, itemTypes: evt.itemTypes });
                stateManager.setSessionContext({ activeFilters: { topics: evt.topics, itemTypes: evt.itemTypes } });

                self.timelineItemList.update({ filtersAreActive: evt.filtersAreActive, topics: evt.topics, itemTypes: evt.itemTypes });
                self.filtersAreActive(evt.filtersAreActive);

            });

            // -- Items -- //

            self.timelineItemList.events.items.initialized.add(function(evt) {

                var sessionActiveItems = evt.filtered ? evt.activeItems : self.timelineItemList.items;
                var activeDate = stateManager.getLocationStateContext('date');

                stateManager.setSessionContext({ activeItems: sessionActiveItems });

                self.timelineDateSelector.initialize({ filtered: evt.filtered, activeItems: evt.activeItems, activeDate: activeDate });

            });

            self.timelineItemList.events.items.filtered.add(function(evt) {

                var sessionActiveItems = evt.filtered ? evt.activeItems : self.timelineItemList.items;

                stateManager.setSessionContext({ activeItems: sessionActiveItems });

                self.timelineDateSelector.update({ filtered: evt.filtered, activeItems: evt.activeItems });

            });

            // -- Date Selector -- //

            self.timelineDateSelector.events.initialized.add(function(evt) {

                stateManager.setSessionContext({ activeDate: evt.activeDate });

                if(evt.animateToDate) {
                    stateManager.setLocationContext({ date: evt.activeDate.year });
                }

            });

            self.timelineDateSelector.events.updated.add(function(evt) {

                if(!!(evt.activeDate)) {
                    stateManager.setLocationContext({date: evt.activeDate.year});
                    stateManager.setSessionContext({activeDate: evt.activeDate});
                }

                if(evt.animate) {

                    // Need to apply a small delay in order to ensure the dom is properly updated before we animate - a bit of a hack, but effective
                    _.delay(function () {
                        presenter.scrollToDate(evt.activeDate);
                    }, 250);
                }

            });

            // -- Window -- //

            $(document).scroll(_.debounce(function() {

                if(!isClickAction && !isBackToTimeline) {

                    var $items = $('.shuffle-item.filtered'),
                        $item = null,
                        foundItem = false;

                    $items.each(function (i, el) {

                        if (foundItem) return false;

                        var isVisible = CRCTimelineUtils.isElementInViewport(el);

                        if (isVisible) {
                            foundItem = true;
                            $item = $(el);
                        }

                    });

                    if (foundItem) {

                        var itemData = $item.data(),
                            year = itemData.decade;

                        self.timelineDateSelector.selectDateByYear(year);

                    }
                }
                else {
                    isClickAction = false;
                    isBackToTimeline = false;
                }

            }, 100, { leading: false, trailing: true }));

        }

        function prepareData() {

            // prep items & dates
            _.each(dataStore.context.timelineItems, function(item) {

                self.timelineItemList.items.push(new CRCTimelineModels.Item(item));
                self.timelineDateSelector.addDate(item.sortingDate);

            });

            // prep topics
            _.each(dataStore.context.timelineTopics, function(topic) {

                self.timelineFilters.topics.push(new CRCTimelineModels.TimelineFilter({
                    type: CRCTimelineModels.TimelineFilterTypes.topics,
                    id: topic.documentGuid,
                    codeName: topic.codeName,
                    displayName: topic.displayName,
                    filterRelationships: topic.itemTypes
                }));

            });

            // prep object type
            _.each(dataStore.context.timelineItemTypes, function(itemType) {

                self.timelineFilters.itemTypes.push(new CRCTimelineModels.TimelineFilter({
                    type: CRCTimelineModels.TimelineFilterTypes.itemTypes,
                    id: itemType.itemGuid,
                    codeName: itemType.codeName,
                    displayName: itemType.displayName,
                    filterRelationships: itemType.topics
                }));

            });

        }

        function initializeKO() {

            ko.applyBindings(self, document.getElementById(config.Settings.parentElementId));
            presenter.initialTransitionOut();

        }

        function initializeUI() {

            if(window.addthis) {

                _.delay(function() {

                    var script = 'http://s7.addthis.com/js/250/addthis_widget.js?_=1466021641364#domready=1';

                    if (window.addthis) {
                        window.addthis = null;
                    }

                    $.getScript( script, function() { addthis.init(); });

                }, 1000);
            }

            presenter.initializeShuffle();

            stateManager.loadLocationContext(function(context) {

                isBackToTimeline = stateManager.getSessionStorageContextValue('backToTimeline');
                self.timelineFilters.initialize({ topics: context.topics, itemTypes: context.itemTypes, date: context.date });
            });

        }

        function init() {

            registerEventListeners();

            presenter.initialTransitionIn(function() {

                dataStore.loadData();

            });
        }

        // ----------------- //

        init();
    };

}());

//----------------------------------------- //

$(function () {

    try {

        new CRCTimelineApp(crcTimelineAppRuntimeConfig);

    }
    catch (ex) {

        // error initializing application.
        console.error('Error Initializing Timeline Application \n', ex);

    }

});
