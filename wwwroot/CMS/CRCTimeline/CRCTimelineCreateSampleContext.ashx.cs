﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Providers;

namespace CMSApp.CRCTimeline
{
    /// <summary>
    /// Summary description for CRCTimelineCreateSampleContext
    /// </summary>
    public class CRCTimelineCreateSampleContext : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            var rnd = new Random();
            var totalTopics = TimelineProvider.TimelineTopics.Count();
            var totalItemTypes = TimelineProvider.TimelineItemTypes.Count();

            List<string> itemNamePrefixes = new List<string> { "Community", "War", "Awesome", "Vintage", "Mysterious", "Classic", "Old-Timey", "Modern", "Historic", "Epic", "New-Age", "Heroic", "Futuristic", "Average", "Red Cross", "Soldier" };
            List<string> itemNameTypes = new List<string> { "First Aid Kit", "Wheel Chair", "Bucket", "Gas Mask", "Poster", "Medal", "Smart Phone", "Postcard", "Bottle", "Biohazard Suit", "Typewriter", "Walkie Talkie", "Replicator" };

            var simulateActions = CMS.Helpers.ValidationHelper.GetBoolean(context.Request.QueryString.Get("simulate"), true);
            var userInfo = CMS.Membership.UserInfoProvider.GetUserInfo("emarchioni");
            var treeProvider = new CMS.DocumentEngine.TreeProvider(userInfo);
            var parentNode = treeProvider.SelectSingleNode("CRC", "/history/Items", "en-CA");

            if (parentNode != null)
            {                
                var items = new List<CMS.DocumentEngine.TreeNode>();
                var startYear = CMS.Helpers.ValidationHelper.GetInteger(context.Request.QueryString.Get("startYear"), 1896);
                var endYear = CMS.Helpers.ValidationHelper.GetInteger(context.Request.QueryString.Get("endYear"), 2016);
                var numberOfItems = CMS.Helpers.ValidationHelper.GetInteger(context.Request.QueryString.Get("numberOfItems"), 3);
                var documentCulture = CMS.Helpers.ValidationHelper.GetString(context.Request.QueryString.Get("documentCulture"), "en-CA");
                var shortDescription = "Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.";
                var longDescription = @"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>";
                var thumbnailImage = "https://placehold.it/300x370?text=Thumbnail+Image";
                var thumbnailImageAlt = "Thumbnail Image AltText";
                var mainImage = "https://placehold.it/600x740?text=Main+Image";
                var mainImageAltText = "Main Image Alt Text";
                var mainImageCaption = "Main Image Caption";

                var itemTemplateId = CMS.PortalEngine.PageTemplateInfoProvider.GetPageTemplateInfo("CRC.Timeline.ItemDetailsPageTemplate");

                for (int i = 1; i < numberOfItems + 1; i++)
                {                    
                    var item = CMS.DocumentEngine.TreeNode.New("CRCTimeline.TimelineItem");
                    var itemDate = GenerateDate(rnd.Next(startYear, endYear));
                    var itemName = itemNamePrefixes[rnd.Next(0, itemNamePrefixes.Count)] + " " + itemNameTypes[rnd.Next(0, itemNameTypes.Count)];

                    item.DocumentCulture = documentCulture;
                    item.DocumentName = itemName;
                    item.SetValue("Title", itemName);
                    item.SetValue("SortingDate", itemDate);
                    item.SetValue("EditorialDate", itemDate.Year);
                    item.SetValue("Topics", GetItemTopics(rnd.Next(0, totalTopics)));
                    item.SetValue("ItemType", GetItemItemType(rnd.Next(0, totalItemTypes)));
                    item.SetValue("ShortDescription", shortDescription);
                    item.SetValue("LongDescription", longDescription);
                    item.SetValue("ThumbnailImage", thumbnailImage);
                    item.SetValue("ThumbnailImageAlt", thumbnailImageAlt);
                    item.SetValue("MainImage", mainImage);
                    item.SetValue("MainImageAlt", mainImageAltText);
                    item.SetValue("MainImageCaption", mainImageCaption);

                    item.NodeInheritPageTemplate = false;
                    item.DocumentPageTemplateID = itemTemplateId.PageTemplateId;

                    items.Add(item);

                }

                // ------------------------ //


                if (!simulateActions)
                {
                    context.Response.Write(string.Format("Create {0} sample content items\n", numberOfItems));
                    context.Response.Write("Content created under: " + parentNode.NodeAliasPath + "\n\n");

                    foreach (var item in items)
                    {
                        item.Insert(parentNode);
                    }
                }
                else
                {
                    context.Response.Write(string.Format("Content Creation Simulation.\nUse flag 'simulate=false' to write content to the database.\n\nCreated {0} sample content items\n\n", numberOfItems));
                }

                foreach (var item in items)
                {
                    context.Response.Write(item.DocumentName + ": ");
                    context.Response.Write(item.GetValue("EditorialDate"));
                    context.Response.Write("\n");
                }

                context.Response.Write("\nDone!");
            }
            else
            {
                context.Response.Write("Parent Node Not Found!\n");
                context.Response.Write("\nFailed to create sample content.");
            }



            // ------------- //

            

        }

        private string GetItemItemType(int index)
        {
            var itemType = TimelineProvider.TimelineItemTypes.ToList()[index];
            return (itemType != null) ? itemType.ItemGuid.ToString() : null;            
        }

        private string GetItemTopics(int index)
        {
            var topic = TimelineProvider.TimelineTopics.ToList()[index];
            return (topic != null) ? topic.DocumentGuid.ToString() : null;
        }

        private DateTime GenerateDate(int year)
        {
            var date = new DateTime(year, 1, 1);
            return date;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}