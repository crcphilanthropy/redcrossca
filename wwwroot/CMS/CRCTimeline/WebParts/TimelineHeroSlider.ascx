<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="CRCTimeline_WebParts_TimelineHeroSlider"
    CodeBehind="~/CRCTimeline/WebParts/TimelineHeroSlider.ascx.cs" %>

<div class="slider-container">
   <div class="slider home-slider">

        <cms:CMSRepeater runat="server" ID="rptSlides" 
            Path="./Hero-Slides/%" 
            ClassNames="CRCTimeline.HeroSlide" 
            TransformationName="CRCTimeline.HeroSlide.HeroSlide"
            SelectOnlyPublished="true"
            TopN="4"
            OrderBy="NodeOrder ASC"
            Columns="NavigationText,Image,ImageAltText,Title,Description,Url,LinkText,NodeOrder,TimelineFilter,TimelineTopics,TimelineItemTypes,TextLocation, TextColor, LinkBackgroundColor"
            CacheMinutes="60" />
    </div>
</div>
