<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="CRCTimeline_WebParts_TimelineItemBackButton" 
    CodeBehind="TimelineItemBackButton.ascx.cs" %>

<asp:PlaceHolder ID="phlTimelineList" runat="server">

    <div id="koItemBackBtn" class="row">
        <div class="column">
            <a class="back" data-bind="attr: { href: backToUrl }">            
                <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.backToTimeline" />
            </a>
        </div>
    </div>

</asp:PlaceHolder>