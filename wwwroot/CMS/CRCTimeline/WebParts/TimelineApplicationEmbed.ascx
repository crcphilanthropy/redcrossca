<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="CTimeline_WebParts_TimelineApplicationEmbed" 
    CodeBehind="TimelineApplicationEmbed.ascx.cs" %>

<%@ Register Src="~/CRCTimeline/WebParts/Timeline/Controls/TimelineList.ascx" TagPrefix="crc" TagName="TimelineList" %>
<%@ Register Src="~/CRCTimeline/WebParts/Timeline/Controls/TimelineFilters.ascx" TagPrefix="crc" TagName="TimelineFilters" %>
<%@ Register Src="~/CRCTimeline/WebParts/Timeline/Controls/TimelineDateSelector.ascx" TagPrefix="crc" TagName="TimelineDateSelector" %>


<asp:PlaceHolder ID="phlTimelineList" runat="server">

    <div id="timeline-container">
        
        <div class="app" data-bind="visible: !(encounteredError)">
            <crc:TimelineFilters runat="server" ID="crcTimelineFilters" />
            <crc:TimelineList runat="server" ID="crcTimelineList" />
            <crc:TimelineDateSelector runat="server" ID="crcTimelineDateSelector" />
        </div>

        <div class="error" data-bind="visible: (encounteredError)">
            <p>
                We've encountered an error and are working on fixing it.
            </p>
        </div>
        
    </div>

</asp:PlaceHolder>