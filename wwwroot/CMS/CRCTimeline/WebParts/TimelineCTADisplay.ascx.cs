﻿using System.Collections.Generic;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using CMS.SiteProvider;

namespace CMSApp.CRCTimeline.WebParts
{
    public partial class TimelineCTADisplay : CMSAbstractWebPart
    {
        public string TransformationName
        {
            get { return ValidationHelper.GetString(GetValue("TransformationName"), string.Empty); }
            set { SetValue("TransformationName", value); }
        }

        public string Path
        {
            get { return ValidationHelper.GetString(GetValue("Path"), string.Empty); }
            set { SetValue("Path", value); }
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            base.OnPreRender(e);
            InitCTA();
        }

        private void InitCTA()
        {
            if (!string.IsNullOrWhiteSpace(TransformationName))
                rptCTA.TransformationName = TransformationName;

            var data = CacheHelper.Cache(() => TreeHelper.GetDocuments(
                SiteContext.CurrentSiteName,
                Path,
                DocumentContext.CurrentDocumentCulture.CultureCode,
                false,
                "CRCTimeline.GenericCTA",
                null,
                null,
                TreeProvider.ALL_LEVELS,
                true,
                1, "Title, Description, ButtonText, DestinationURL, URLTarget, Image, ImageAltText, AdditionalContent"), new CacheSettings(60, string.Format("initcta_{0}_{1}", LocalizationContext.PreferredCultureCode, Path.ToLower()))
                    {
                        CacheDependency = CacheHelper.GetCacheDependency(new List<string> { string.Format("node|{0}|{1}", SiteContext.CurrentSiteName, Path).ToLower() })
                    });

            rptCTA.DataSource = data;
            rptCTA.DataBind();
        }
    }
}