﻿using System;
using System.Collections.Generic;
using CMS.Helpers;
using CMS.Localization;
using CMS.PortalControls;
using CMSApp.CRC;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CRC.EmailClient.Models;
using Newtonsoft.Json;

namespace CMSApp.CRCTimeline.WebParts
{
  public partial class TimelineEmailWidget : CMSAbstractWebPart
  {
    public string Title
    {
      get { return ValidationHelper.GetString(GetValue("Title"), string.Empty); }
      set { SetValue("Title", value); }
    }

    public string Description
    {
      get { return ValidationHelper.GetString(GetValue("Description"), string.Empty); }
      set { SetValue("Description", value); }
    }

    public string LearnMoreText
    {
      get { return ValidationHelper.GetString(GetValue("LearnMoreText"), string.Empty); }
      set { SetValue("LearnMoreText", value); }
    }

    public string ButtonText
    {
      get { return ValidationHelper.GetString(GetValue("ButtonText"), string.Empty); }
      set { SetValue("ButtonText", value); }
    }

    public string PrivacyPolicyText
    {
      get { return ValidationHelper.GetString(GetValue("PrivacyPolicyText"), string.Empty); }
      set { SetValue("PrivacyPolicyText", value); }
    }

    public string PrivacyPolicyLink
    {
      get { return ValidationHelper.GetString(GetValue("PrivacyPolicyLink"), string.Empty); }
      set { SetValue("PrivacyPolicyLink", value); }
    }

    public string List
    {
      get { return ValidationHelper.GetString(GetValue("List"), string.Empty); }
      set { SetValue("List", value); }
    }

    public string ThankYouMessage
    {
      get { return ValidationHelper.GetString(GetValue("ThankYouMessage"), ResHelper.GetString("CRC.Form.Newsletter.ThankYou")); }
      set { SetValue("ThankYouMessage", value); }
    }

    public string DuplicateEmailMessage
    {
      get { return ValidationHelper.GetString(GetValue("DuplicateEmailMessage"), ResHelper.GetString("CRC.Form.Newsletter.ThankYou")); }
      set { SetValue("DuplicateEmailMessage", value); }
    }

    public bool IsDisruptiveEmailWidget
    {
      get { return ValidationHelper.GetBoolean(GetValue("IsDisruptiveEmailWidget"), false); }
      set { SetValue("IsDisruptiveEmailWidget", value); }
    }

    public int HideAfterNTimesClosed
    {
      get { return ValidationHelper.GetInteger(GetValue("HideAfterNTimesClosed"), 3); }
      set { SetValue("HideAfterNTimesClosed", value); }
    }

    public int HideAfterNDays
    {
      get { return ValidationHelper.GetInteger(GetValue("HideAfterNDays"), 30); }
      set { SetValue("HideAfterNDays", value); }
    }

    public string CustomFields
    {
      get
      {
        return Server.HtmlEncode(JsonConvert.SerializeObject(new List<EmailContactCustomField> {
                    new EmailContactCustomField { Key = "SiteSection", Value = "History" },
                    new EmailContactCustomField {Key = "DateJoined", Value = DateTime.Today.Date.ToString("yyyy/MM/dd")}
                }));
      }
    }

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      DataBind();
      mvEmailWidget.SetActiveView(IsDisruptiveEmailWidget ? vDisruptiveWidget : vInlineWidget);
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      if (!IsDisruptiveEmailWidget) return;
      var disruptiveCTAProvider = new DisruptiveCTAProvider(HideAfterNTimesClosed, HideAfterNDays, "timeline-email-widget");
      Visible = disruptiveCTAProvider.ShowDisruptiveCTA() && CurrentDocument.GetInheritedFieldValue("ShowDisruptiveEmailWidget") == 1;
    }
  }
}