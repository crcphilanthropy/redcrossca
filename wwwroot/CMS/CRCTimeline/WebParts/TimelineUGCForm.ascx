﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelineUGCForm.ascx.cs" Inherits="CMSApp.CRCTimeline.WebParts.TimelineUGCForm" %>
<%@ Import Namespace="CMSApp.CRC" %>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $.validator.methods.email = function (value, element) {
        return this.optional(element) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(value);
    }
</script>
<div action="submit" id="ugcForm" runat="server" clientidmode="Static">
    <div class="field left">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.FirstName",defaultText:"First Name") %> </label>
        <input type="text" name="firstName" id="txtFirstName" runat="server" clientidmode="Static" required />
    </div>
    <div class="field">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.LastName",defaultText:"Last Name") %></label>
        <input type="text" name="lastName" id="txtLastName" runat="server" clientidmode="Static" required />
    </div>
    <div class="field checkbox">
        <input id="displayName" type="checkbox" runat="server" clientidmode="Static" />
        <label for="displayName">
            <p><%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.DisplayFirstName",defaultText:"Display my first name on the website") %></p>
        </label>
    </div>
    <div class="field">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.Email",defaultText:"Email") %></label>
        <input type="text" name="email" id="txtEmail" runat="server" clientidmode="Static" required />
    </div>
    <div class="field checkbox">
        <input id="subscribe" type="checkbox" clientidmode="Static" runat="server" />
        <label for="subscribe">
            <p><%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.Subscribe",defaultText:"Yes, I want to subscribe to receive newsletter from the Canadian Red Cross.") %></p>
        </label>
    </div>
    <div class="field">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemDescription",defaultText:"Item Description (XX characters)") %></label>
        <input type="text" name="itemDesc" id="txtItemDescription" clientidmode="Static" runat="server" required maxlength="100" />
    </div>
    <div class="field text-area">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemMeaning",defaultText:"What does this item mean to you? (XX characters)") %></label>
        <textarea name="itemMeaning" id="itemMeaning" clientidmode="Static" runat="server" required maxlength="250"></textarea>
    </div>
    <div class="field">
        <label>* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemYear",defaultText:"Year or Time Period (Example formats: 1996, 1960s or 1980-1990)") %></label>
        <input type="text" name="itemYear" id="txtItemYear" runat="server" clientidmode="Static" required />
    </div>
    <div class="field file-uploader">
        <label for="fileUpload">* <%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.FileUpload",defaultText:"Upload your object image (JPG or PNG)") %></label>
        <label for="fileUpload" class="browse small-btn btn"><%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.Browse",defaultText:"Browse") %></label>
        <input type="file" name="fileUpload" id="fileUpload" class="file-upload" accept="image/*" clientidmode="Static" runat="server" required />

        <span class="file-uploaded"></span>
    </div>
    <div class="field checkbox">
        <input id="contact" type="checkbox" clientidmode="Static" runat="server" />
        <label for="contact">
            <p><%# ResourceStringHelper.GetString("CRCTimeline.UgcForm.Contact",defaultText:"If Red Cross has questions about my item, they can contact me for more information.") %></p>
        </label>
    </div>

    <div class="btn-field" id="ugc-form-submit-btn">
        <asp:Button runat="server" CssClass="red-btn" ID="btnSubmit" OnClientClick="return processForm(ugcFormIsValid);" />
        <span class="btn-loader"></span>
    </div>

</div>

<script>

    function processForm(callback) {

        var $form = $('#form'),
            $btn = $('#ugc-form-submit-btn'),
            formIsValid = $form.valid();
            userOptIn = $('input#contact').prop('checked'),
            userNewsletterOptIn = $('input#subscribe').prop('checked');

        if (formIsValid) {

            window.dataLayer.push({
                'event': 'generic-event',
                'eventCategory': 'user generated content',
                'eventAction': 'ugc submission',
                'eventLabel': 'not set',
                'nonInteractive': true
            });

            if (userNewsletterOptIn) {

                window.dataLayer.push({
                    'event': 'generic-event',
                    'eventCategory': 'newsletter submission',
                    'eventAction': 'complete',
                    'eventLabel': 'timeline: ugc submission',
                    'nonInteractive': true
                });

            }

            if (userOptIn) {

                window.dataLayer.push({
                    'event': 'generic-event',
                    'eventCategory': 'user generated content',
                    'eventAction': 'opt in to contact',
                    'eventLabel': 'not set',
                    'nonInteractive': true
                });

            }

            $btn.addClass('processing');

        }

        _.delay(function () {
            
            if (callback && _.isFunction(callback)) {
                callback();
            }

        }, 150);

    }

    function ugcFormIsValid() {

        var $form = $('#form'),
            formIsValid = $form.valid();

        return formIsValid;
               
    }
</script>