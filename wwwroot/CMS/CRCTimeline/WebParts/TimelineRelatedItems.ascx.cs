﻿using System;
using System.Linq;
using CMS.Helpers;
using CMS.PortalControls;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Providers;

namespace CMSApp.CRCTimeline.WebParts
{
    public partial class TimelineRelatedItems : CMSAbstractWebPart
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var relatedGuids = CurrentDocument.GetValue("RelatedItems", string.Empty).Split(new[] { ";", "|", "," }, StringSplitOptions.RemoveEmptyEntries).Select(item => ValidationHelper.GetGuid(item, Guid.Empty)).ToList();
            var relatedItems = TimelineProvider.TimelineItems.Where(item => relatedGuids.Contains(item.DocumentGuid)).OrderBy(item => relatedGuids.IndexOf(item.DocumentGuid));

            rptItems.DataSource = relatedItems;
            Visible = relatedItems.Any();
            DataBind();
        }
    }
}