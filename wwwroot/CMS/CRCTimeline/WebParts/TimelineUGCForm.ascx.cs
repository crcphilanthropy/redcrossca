﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using CMS.MediaLibrary;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMSApp.CRC;
using System.IO;
using CMS.Base;
using CMSAppAppCode.Old_App_Code.CRC.Providers;
using CMSAppAppCode.Old_App_Code.CRCTimeline.Models;
using CRC.EmailClient.Models;

namespace CMSApp.CRCTimeline.WebParts
{
    public partial class TimelineUGCForm : CMSAbstractWebPart
    {
        public string ParentPath
        {
            get { return ValidationHelper.GetString(GetValue("ParentPath"), string.Empty); }
            set { SetValue("ParentPath", value); }
        }


        public string ThankYouPagePath
        {
            get { return ValidationHelper.GetString(GetValue("ThankYouPagePath"), "/"); }
            set { SetValue("ThankYouPagePath", value); }
        }

        public string LibraryName
        {
            get { return ValidationHelper.GetString(GetValue("LibraryName"), "CRC.Timeline.Media"); }
            set { SetValue("LibraryName", value); }
        }

        public string FolderName
        {
            get { return ValidationHelper.GetString(GetValue("FolderName"), "UGCItemImages"); }
            set { SetValue("FolderName", value); }
        }

        public string AllowedExtensions
        {
            get
            {
                return ValidationHelper.GetString(GetValue("AllowedExtensions"), "bmp;gif;jpg;jpeg;png");
            }
            set { SetValue("AllowedExtensions", value); }
        }

        /// <summary>
        /// Gets or sets the name of the english list.
        /// </summary>
        /// <value>
        /// The name of the english list.
        /// </value>
        public string EnglishListName
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue("ListName"), string.Empty);
            }
            set
            {
                SetValue("ListName", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the french list.
        /// </summary>
        /// <value>
        /// The name of the french list.
        /// </value>
        public string FrenchListName
        {
            get
            {
                return DataHelper.GetNotEmpty(GetValue("ListNameFr"), string.Empty);
            }
            set
            {
                SetValue("ListNameFr", value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ugcForm.DataBind();
            btnSubmit.Text = ResourceStringHelper.GetString("CRC.Timeline.UgcFormSubmit", defaultText: "Submit");
            btnSubmit.Click += BtnSubmitServerClick;

            var errorMessages = new Dictionary<string, string>
                {
                    {txtFirstName.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.FirstNameErrorMessage", defaultText:"Please enter your First Name")},
                    {txtLastName.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.LastNameErrorMessage", defaultText: "Please enter your Last Name")},
                    {txtEmail.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.EmailErrorMessage", defaultText: "Please enter a valid Email")},
                    {txtItemDescription.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemDescriptionErrorMessage", defaultText: "Please enter an Item Description")},
                    {itemMeaning.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemMeaningErrorMessage", defaultText: "This field is required")},
                    {txtItemYear.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.ItemYearErrorMessage", defaultText: "Please enter a time period")},
                    {fileUpload.UniqueID, ResourceStringHelper.GetString("CRCTimeline.UgcForm.fileUploadErrorMessage", defaultText: "Please upload an image")}
                };

            var rules = new List<string>
                {
                    string.Format("{0}:{{required: true, email:true, maxlength: 150}}", txtEmail.UniqueID),
                    string.Format("{0}:{{required: true, maxlength: 100}}", txtFirstName.UniqueID),
                    string.Format("{0}:{{required: true, maxlength: 100}}", txtLastName.UniqueID),
                    string.Format("{0}:{{required: true, maxlength: 100}}", txtItemDescription.UniqueID),
                    string.Format("{0}:{{required: true, maxlength: 250}}", itemMeaning.UniqueID),
                    string.Format("{0}:{{required: true, maxlength: 50}}", txtItemYear.UniqueID)
                };

            TransformationHelper.RegisterStartUpScript(string.Format("$('#form').validate({{rules: {{{0}}}, messages: {1}}});", rules.Join(","), new JavaScriptSerializer().Serialize(errorMessages)), Page, "ugcFormErrorMessages");
        }

        protected void BtnSubmitServerClick(object sender, EventArgs e)
        {
            var treeProvider = new TreeProvider();

            var parentNode = treeProvider.SelectSingleNode(SiteContext.CurrentSiteName, ParentPath, LocalizationContext.PreferredCultureCode);
            var node = TreeNode.New("CRCTimeline.UGCItem", treeProvider);
            node.NodeName = txtItemDescription.Value;
            node.NodeAlias = txtItemDescription.Value;
            node.DocumentName = txtItemDescription.Value;
            node.DocumentCulture = LocalizationContext.PreferredCultureCode;
            node.SetValue("ItemName", txtItemDescription.Value);
            node.SetValue("DocumentName", txtItemDescription.Value);
            node.SetValue("FirstName", txtFirstName.Value);
            node.SetValue("LastName", txtLastName.Value);
            node.SetValue("Email", txtEmail.Value);
            node.SetValue("ItemDescription", itemMeaning.Value);
            node.SetValue("ItemTimePeriod", txtItemYear.Value);
            node.SetValue("ShowFirstName", displayName.Checked);
            node.SetValue("Subscribed", subscribe.Checked);
            node.SetValue("AllowContact", contact.Checked);

            node.Insert(parentNode);


            if (fileUpload.PostedFile != null)
            {
                var libraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(LibraryName, SiteContext.CurrentSiteName);
                if (libraryInfo != null)
                {
                    var folderPath = string.Format("/{0}/{1}/UGCItemImages", SiteContext.CurrentSiteName, libraryInfo.LibraryFolder);
                    if (!Directory.Exists(Server.MapPath(folderPath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(folderPath));
                    }

                    var fileNameAndPath = String.Format("{0}/{1}", folderPath, fileUpload.PostedFile.FileName);
                    var mappedPath = Server.MapPath(fileNameAndPath);
                    fileUpload.PostedFile.SaveAs(mappedPath);

                    var imagePath = AddFileToMediaLibrary(mappedPath, libraryInfo);

                    node.SetValue("ItemImage", imagePath.Equals(mappedPath, StringComparison.OrdinalIgnoreCase) ? fileNameAndPath : imagePath);
                    node.Update();
                }
            }
            CRCDocumentHelper.SetWorkFlow(node, treeProvider, "approval");
            AddToEmailClient();

            Response.Redirect(TransformationHelper.getCultureDocumentUrl(ThankYouPagePath), false);
        }

        private void AddToEmailClient()
        {
            try
            {
                var subscriptionItem = new SubscriptionItem<string>
                {
                    FirstName = txtFirstName.Value,
                    LastName = txtLastName.Value,
                    Email = txtEmail.Value,
                    FullName = string.Format("{0} {1}", txtFirstName.Value, txtLastName.Value),
                    LearnMore = subscribe.Checked,
                    ListKey = CRCEmailClientProvider.IsInTestMode ? "91388018c19eded9f08615bc975f82ad" : string.Equals(LocalizationContext.PreferredCultureCode, "en-CA") ? EnglishListName : FrenchListName,
                    CustomFields = new CMSList<EmailContactCustomField>
                        {
                            new EmailContactCustomField { Key = "SiteSection", Value = "History"}
                        }
                };

                if (subscribe.Checked)
                {
                    var result = CRCEmailClientProvider.Subscribe(subscriptionItem);

                    if (result == CRCEmailClientProvider.EmailClientResult.Subscribed || result == CRCEmailClientProvider.EmailClientResult.Duplicate)
                        CRCEmailClientProvider.SaveToCustomTable(subscriptionItem);
                }
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("TimelineUGCForm", "AddToEmailClient", ex);
            }
        }

        private string AddFileToMediaLibrary(string srcFile, MediaLibraryInfo libraryInfo)
        {
            if (string.IsNullOrWhiteSpace(srcFile)) return null;

            var siteInfo = SiteContext.CurrentSite;
            var srcFileInfo = new FileInfo(srcFile);

            var fileInfo = new MediaFileInfo(srcFileInfo.FullName, libraryInfo.LibraryID, FolderName) { FileSiteID = siteInfo.SiteID, FileGUID = Guid.NewGuid() };

            var tempFileInfo = MediaFileInfoProvider.GetMediaFileInfo(fileInfo.FileGUID, siteInfo.SiteName);
            if (tempFileInfo != null)
                return MediaFileURLProvider.GetMediaFileUrl(fileInfo, SiteContext.CurrentSiteName, libraryInfo.LibraryFolder);
            try
            {
                MediaFileInfoProvider.SetMediaFileInfo(fileInfo);
            }
            catch (CodeNameNotUniqueException ex)
            {
                return srcFile;
            }


            if (fileInfo.FilePath.ToLower().Contains("images"))
                return string.Format("{0}?width={1}&height={2}&ext={3}", MediaFileURLProvider.GetMediaFileUrl(fileInfo, SiteContext.CurrentSiteName, libraryInfo.LibraryFolder), fileInfo.FileImageWidth, fileInfo.FileImageHeight, fileInfo.FileExtension);

            return string.Format("{0}?ext={1}", MediaFileURLProvider.GetMediaFileUrl(fileInfo, SiteContext.CurrentSiteName, libraryInfo.LibraryFolder), fileInfo.FileExtension);
        }
    }
}