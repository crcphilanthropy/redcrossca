using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.Helpers;
using CMS.SiteProvider;
using CMS.DocumentEngine;

public partial class CRCTimeline_WebParts_TimelineItemBackButton : CMSAbstractWebPart
{
    #region "Properties"

    public string ApplicationSourceFile
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ApplicationSourceFile"), string.Empty);
        }

        set
        {
            SetValue("ApplicationSourceFile", value);

        }
    }

    public string ApplicatonRuntimeConfig
    {
        get
        {
            var v = ValidationHelper.GetString(GetValue("ApplicatonRuntimeConfig"), "{}");

            return !string.IsNullOrEmpty(v) ? v : "{}";
        }

        set
        {
            SetValue("ApplicatonRuntimeConfig", value);

        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {
            var scriptConfig = string.Format(@"<script>var crcItemBackButtonRunTimeConfig = {0};</script>", ApplicatonRuntimeConfig);
            Page.ClientScript.RegisterStartupScript(typeof(Control), "crc-timeline-item-back-btn-config", scriptConfig);

            var scriptApplicationSource = string.Format(@"<script src=""{0}""></script>", ApplicationSourceFile);
            Page.ClientScript.RegisterStartupScript(typeof(Control), "crc-timeline-item-back-btn", scriptApplicationSource);
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



