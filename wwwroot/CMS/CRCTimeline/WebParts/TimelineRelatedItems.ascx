﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelineRelatedItems.ascx.cs" Inherits="CMSApp.CRCTimeline.WebParts.TimelineRelatedItems" %>
<%@ Import Namespace="CMSApp.CRC" %>
<div class="row">
    <div class="columns">
        <h2><%# ResourceStringHelper.GetString("CRCTimeline.ItemDetail.RelatedItems", defaultText:"Related Items") %></h2>
        
        <asp:Repeater runat="server" ID="rptItems">
            <ItemTemplate>
                <div class="item-container related-items">
                    <a href="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" class="item cp-track" data-event="generic-event" data-category="timeline item" data-action="related item" data-label="<%# Eval("Title") %>">
                    
                            <div class="item-img">
                                <img src="<%# Eval("ThumbnailImage.FilePath") %>" alt="<%# Eval("ThumbnailImage.AltText") %>" />
                            </div>
                            <div class="item-desc">
                                <h3><%# Eval("Title") %></h3>
                                <span class="divider"></span>
                                <p><%# Eval("EditorialDate") %></p>
                            </div>

                            <div class="overlay">
                                <div class="item-desc">
                                    <h3><%# Eval("Title") %></h3>
                                    <span class="divider"></span>
                                    <p><%# Eval("EditorialDate") %></p>
                                </div>
                                <p><%# Eval("ShortDescription") %></p>
                                <span>
                           
                                        <%# ResourceStringHelper.GetString("CRCTimeline.ItemDetail.ReadMore", defaultText:"Read More") %>
                                 </span>
                            </div>
                    </a>
                    <div class="share">
                        
                        <p><%# ResourceStringHelper.GetString("CRCTimeline.ItemDetail.ShareThisItem", defaultText:"Share This Item") %></p>
                        <div class="addthis_toolbox">
                            <ul class="social-icons inline-list">
                                <li>
                                    <a class="addthis_button_twitter cp-social" data-event="social-event" data-social-network="twitter" data-social-action="share" data-social-target="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" addthis:url="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" addthis:title="<%# EvalText("Title") %> <%# ResHelper.GetString("crc.timeline.addThis.twitterHashtag") %>" addthis:description="<%# EvalText("ShortDescription") %>">
                                        <div class="svg-wrapper twitter">
                                            <img src="/crc/images/icon-twitter.svg" alt="Twitter Icon">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="addthis_button_facebook cp-social" data-event="social-event" data-social-network="facebook" data-social-action="share" data-social-target="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" addthis:url="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" addthis:title="<%# EvalText("Title") %>" addthis:description="<%# EvalText("ShortDescription") %>">
                                        <div class="svg-wrapper facebook">
                                            <img src="/crc/images/icon-facebook.svg" alt="Facebook Icon">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="addthis_button_link btn-clip" addthis:url="<%# URLHelper.GetAbsoluteUrl(EvalText("Url")) %>" addthis:title="<%# EvalText("Title") %>">
                                        <div class="svg-wrapper share-link">
                                            <img src="/CRCTimeline/UI/img/icon-share.svg" alt="Share Icon">
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
               </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>