﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelineEmailWidget.ascx.cs" Inherits="CMSApp.CRCTimeline.WebParts.TimelineEmailWidget" %>
<%@ Import Namespace="CMSAppAppCode.Old_App_Code.CRC.Providers" %>
<asp:MultiView runat="server" ID="mvEmailWidget">
    <asp:View runat="server" ID="vInlineWidget">
        <div class="small-12 medium-6 large-4 columns timeline-email-widget" data-mailing-list="<%# List %>" data-custom-fields="<%# CustomFields %>">
            <div class="panel email-cta" data-equalizer-watch>
                <h2><%# Title %></h2>
                <p><%# Description %> </p>

                <div id="form" class="input">
                    <div class="field">
                        <label for="email">* Email</label>
                        <input type="text" name="email" id="txtSubscribeEmail" required>
                        <label id="txtEmail-error" class="email-error error display-none" for="email"></label>
                        <label id="Label1" class="list-error error display-none" for="email"></label>
                    </div>
                    <div class="field checkbox">
                        <input type="checkbox" runat="server" id="learnMore">
                        <label for="<%# learnMore.ClientID %>">
                            <p><%# LearnMoreText %></p>
                        </label>
                    </div>
                    <input type="hidden" value="<%# List %>" id="hidEmailList" />
                    <button type="submit" class="red-btn"><%# ButtonText %></button>
                </div>
                <div class="row align-center align-middle thank-you thank-you-message" style="display: none">
                    <div class="columns">
                        <p><%# ThankYouMessage %></p>
                    </div>
                </div>
                <div class="row align-center align-middle thank-you duplicate-message" style="display: none">
                    <div class="columns">
                        <p><%# DuplicateEmailMessage %></p>
                    </div>
                </div>
                <a href="<%# PrivacyPolicyLink %>" class="secondary-link"><%# PrivacyPolicyText %></a>
            </div>

        </div>
    </asp:View>
    <asp:View runat="server" ID="vDisruptiveWidget">
        <div class="disruptive-email hide-for-small show-for-large timeline-email-widget" data-mailing-list="<%# List %>" data-custom-fields="<%# CustomFields %>" data-emailclient-type="<%# CRCEmailClientProvider.CurrentEmailClient.ToString().ToLower() %>">
            <button class="close" data-path="/" data-cookie-key="timeline-email-widget" data-hidepromotionndays="<%# HideAfterNDays %>"></button>
            <div class="input">
                <h2><%# Title %></h2>
                <input type="text" id="email" name="email" placeholder="<%# Description %>" id="txtSubscribeEmail" required="" aria-required="true">
                <input type="hidden" value="<%# List %>" id="hidEmailList" />
                <button type="submit" class="red-btn"><%# ButtonText %></button>
                <label id="Label2" class="email-error error display-none" for="email"></label>
                <label id="Label3" class="list-error error display-none" for="email"></label>

                <div class="field checkbox">
                    <input type="checkbox" id="learnMore">
                    <label for="learnMore">
                        <p><%# LearnMoreText %></p>
                    </label>
                </div>
            </div>
            <div class="row align-center align-middle thank-you thank-you-message" style="display: none">
                <div class="columns">
                    <p><%# ThankYouMessage %></p>
                </div>
            </div>
            <div class="row align-center align-middle thank-you duplicate-message" style="display: none">
                <div class="columns">
                    <p><%# DuplicateEmailMessage %></p>
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>
