<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="CRCTimeline_WebParts_TimelineItemPrevNextItem" 
    CodeBehind="TimelinePrevNextItem.ascx.cs" %>

<asp:PlaceHolder ID="phlTimelineList" runat="server">

    <div id="koPrevNextItemBtn" class="row" data-bind="visible: !errorOccurred">
        <div class="column">
            <a class="previous float-left" data-bind="attr: { href: previousItemUrl }, visible: !!previousItemUrl"><cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.previousItem" /></a>
            <a class="next float-right" data-bind="attr: { href: nextItemUrl }, visible: !!nextItemUrl"><cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.nextItem" /></a>
        </div>
    </div>

</asp:PlaceHolder>