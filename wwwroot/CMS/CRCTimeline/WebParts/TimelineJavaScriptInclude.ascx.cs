using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.Helpers;

public partial class CRCTimeline_WebParts_TimelineJavaScriptInclude : CMSAbstractWebPart
{
    #region "Properties"

    public string ScriptSourceFile
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ScriptSourceFile"), string.Empty);
        }

        set
        {
            SetValue("ScriptSourceFile", value);

        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {

        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!string.IsNullOrEmpty(ScriptSourceFile))
        {
            //var scriptName = "jvs-script-include-" + this.ClientID;
            var scriptApplicationSource = string.Format(@"<script src=""{0}""></script>", ScriptSourceFile);

            Page.Header.Controls.Add(new LiteralControl(scriptApplicationSource));
            //Page.ClientScript.RegisterStartupScript(typeof(Control), scriptName, scriptApplicationSource);
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



