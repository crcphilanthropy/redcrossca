﻿<%@ Control Language="C#" AutoEventWireup="true" 
    CodeBehind="TimelineFilters.ascx.cs" 
    Inherits="CMSApp.CRCTimeline.WebParts.Timeline.Controls.TimelineFilters" %>

<div class="filters-container">
    <button class="menu-btn-close close-modal hide-for-large" data-bind="click: onFiltersApply" onclick="return false;"></button>
    <div class="filters closed">
        <div class="btns-container top hide-for-large">
            <button class="red-btn close-modal" data-bind="click: onFiltersApply, css: { 'disabled': (filtersAreActive() === false) }, enable: filtersAreActive" onclick="return false;">
                <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.filters.apply" />
            </button>
            <button class="reset-btn" data-bind="click: onFiltersReset, css: { 'disabled': (filtersAreActive() === false) }, enable: filtersAreActive" onclick="return false;">
                <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.filters.reset" />
            </button>
        </div>

        <div class="row">
            
            <button class="filter-button red-btn" id="filter-btn" data-bind="click: onFilterPanelToggle" onclick="return false;">
                <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.itemFilter" />
            </button>

            <div class="topics">
                <p>
                    <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.filtersLabel.topics" />
                </p>
                <ul data-bind="foreach: { data: timelineFilters.topics, as: 'topic' }">
                    <li data-bind="css: { selected: topic.selected(), disabled: !topic.enabled() }">
                        <button onclick="return false;" data-bind="click: $root.onFilterChange, attr: { 'data-codeName': topic.codeName }">
                            <span data-bind="text: topic.displayName"></span>
                        </button>
                    </li>
                </ul>
            </div>
            <div class="objects">
                <p>
                    <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.filtersLabel.objects" />
                </p>
                <ul data-bind="foreach: { data: timelineFilters.itemTypes, as: 'itemType' }">
                    <li data-bind="css: { selected: itemType.selected(), disabled: !itemType.enabled() }">
                        <button onclick="return false;" data-bind="click: $root.onFilterChange, attr: { 'data-codeName': itemType.codeName }">
                            <span data-bind="text: itemType.displayName"></span>
                        </button>
                    </li>
                </ul>
            </div>
      

        </div>

         <div class="btns-container bottom row align-right">
            <button class="red-btn close-modal hide-for-large" data-bind="click: onFiltersApply, css: { 'disabled': (filtersAreActive() === false) }, enable: filtersAreActive" onclick="return false;">
                <cms:LocalizedLiteral ID="LocalizedLiteral1" runat="server" ResourceString="crc.timeline.filters.apply" />
            </button>
            <button class="reset-btn" data-bind="click: onFiltersReset, css: { 'disabled': (filtersAreActive() === false) }, enable: filtersAreActive" onclick="return false;">
                <cms:LocalizedLiteral ID="LocalizedLiteral2" runat="server" ResourceString="crc.timeline.filters.reset" />
            </button>
        </div>

        <button id="close-filters" class="skip" id="skip" title="Close Filters" onclick="return false;">Close Filters</button>

    </div>

</div>