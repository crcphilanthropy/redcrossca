﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelineDateSelector.ascx.cs" Inherits="CMSApp.CRCTimeline.WebParts.Timeline.Controls.TimelineDateSelector" %>

<div class="timeline show-for-large" data-bind="visible: timelineDateSelector.dates.length > 0">
    <h4>
        <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.timelineByDecade" />
    </h4>
    <ul data-bind="foreach: { data: timelineDateSelector.dates, as: 'date' }">
        <li>
            <a href="#" data-bind="css: { disabled: date.isDisabled, active: date.isActive }, click: $root.onTimelineDateClick">
                <span class="tooltip-text" data-bind="text: date.displayName"></span>
            </a>
        </li>
    </ul>
</div>