﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeBehind="TimelineList.ascx.cs"
    Inherits="CMSApp.CRCTimeline.WebParts.Timeline.Controls.TimelineList" %>

<div class="items-row clearfix">

    <div id="items-grid" class="items" data-bind="foreach: { data: timelineItemList.items, as: 'item' }">

        <div data-bind="attr: { 'class': 'item-container ' + item.decade, 'data-decade': item.decade, 'data-itemid': item.id, 'data-itemtype': item.itemType, 'data-topics': item.topics }">

            <a data-bind="attr: { 'href': item.url, 'class': 'item' }">

                <div class="item-img">
                    <img class="lazy" src="/CRCTimeline/UI/img/timeline/blank.gif" 
                        data-bind="attr: { 'src': item.thumbnail.src, alt: item.thumbnail.alt }" />
                </div>

                <div class="item-desc">
                    <h3 data-bind="text: item.title"></h3>
                    <span class="divider"></span>
                    <p data-bind="text: item.editorialDate"></p>
                </div>

                <div class="overlay">
            
                    <div class="item-desc">
                        <h3 data-bind="text: item.title"></h3>
                        <span class="divider"></span>
                        <p data-bind="text: item.editorialDate"></p>
                    </div>
            
                    <p data-bind="text: item.description"></p>
                    <span>
                        <cms:LocalizedLiteral ID="LocalizedLiteral1" runat="server" ResourceString="crc.timeline.readMore" />
                    </span>

                </div>

            </a>

            <div class="share">
              <p>
                  <cms:LocalizedLiteral runat="server" ResourceString="crc.timeline.shareThisItem" />
              </p>
              <div class="addthis_toolbox">
                <ul class="social-icons inline-list">
                    <li>
                        <a href="#/?" class="addthis_button_twitter cp-social" data-event="social-event" data-social-network="twitter" data-social-action="share" data-bind="attr: { 'data-social-target': item.absoluteUrl, 'addthis:url': item.absoluteUrl, 'addthis:title': item.title + ' ' + window.timelineWebsiteRuntimeConfig.uiStrings.timelineHashtag, 'addthis:description': item.description }">
                            <div class="svg-wrapper twitter">
                                <img src="/crc/images/icon-twitter.svg" alt="Twitter Icon" onerror="this.onerror=null; this.src='/crc/images/icon-twitter.png'" >
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#/?" class="addthis_button_facebook cp-social" data-event="social-event" data-social-network="facebook" data-social-action="share" data-bind="attr: { 'data-social-target': item.absoluteUrl, 'addthis:url': item.absoluteUrl, 'addthis:title': item.title, 'addthis:description': item.description }">
                            <div class="svg-wrapper facebook">
                                <img src="/crc/images/icon-facebook.svg" alt="Facebook Icon" onerror="this.onerror=null; this.src='/crc/images/icon-facebook.png'" >
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#/?" class="addthis_button_link btn-clip" data-bind="attr: { 'addthis:url': item.absoluteUrl, 'addthis:title': item.title }" onclick="return false;">
                            <div class="svg-wrapper share-link">
                                <img src="/CRCTimeline/UI/img/icon-share.svg" alt="Copy Link Icon" >
                            </div>
                        </a>
                    </li>
                </ul>
              </div>
            </div>

        </div>

    </div>
</div>
 