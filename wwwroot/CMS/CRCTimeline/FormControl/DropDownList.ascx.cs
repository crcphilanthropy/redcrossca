﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.FormControls;
using CMS.Helpers;
using CMS.Localization;
using CMS.MacroEngine;
using CMSApp.CRC;

namespace CMSApp.CRCTimeline.FormControl
{
    public partial class DropDownList : FormEngineUserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            LoadList();
        }

        private void LoadList()
        {
            if (IsPostBack)
                Value = Request.Form[ddlList.UniqueID];


            var data = ConnectionHelper.ExecuteQuery(MacroContext.CurrentResolver.ResolveMacros(Query), null,
                                                     QueryTypeEnum.SQLQuery);
            var dictionary = new Dictionary<string, string>();
            if (!DataHelper.DataSourceIsEmpty(data))
            {
                data.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(row =>
                    {
                        if (!dictionary.ContainsKey(ValidationHelper.GetString(row[0], string.Empty)))
                            dictionary.Add(ValidationHelper.GetString(row[0], string.Empty),
                                           ResourceStringHelper.LocalizeString(
                                               ValidationHelper.GetString(row[1], string.Empty),
                                               LocalizationContext.PreferredCultureCode));
                    });
            }

            ddlList.DataSource = dictionary;
            ddlList.DataValueField = "Key";
            ddlList.DataTextField = "Value";
            ddlList.DataBind();


            ddlList.SelectedIndex = ddlList.Items.IndexOf(ddlList.Items.FindByValue(Value == null ? string.Empty : Value.ToString(), true));
        }

        public override bool IsValid()
        {
            if (!FieldInfo.AllowEmpty && String.IsNullOrWhiteSpace(Value as string))
            {
                ValidationError += ResourceStringHelper.GetString("BasicForm.ErrorEmptyValue");
                return false;
            }

            return base.IsValid();
        }

        public override object Value { get; set; }

        public string Query
        {
            get { return ValidationHelper.GetString(GetValue("Query"), String.Empty); }
            set { SetValue("Query", value); }
        }
    }
}