(function ($) {
	"use strict";
	var itsTime = false,
		loaded = false,
		lang = document.getElementsByTagName('html')[0].getAttribute('lang'),
		browserSupports,
		// multi-lingual copy definitions
		i18n = {
			"en": {
				"loading-box":		"When a family is torn apart, a photo is often all they have to keep hope alive. With your help we can piece these families back together.",
				"title":			"When you give, we give.",
				"subtitle":			"Every day families are separated because of war and disaster.<br>Every day we work to reunite them.",
				"quote":			"I said &#8216;Mom, is this you?&#8217;<br>She asked &#8216;Who are you?&#8217;<br>I told her it was her daughter calling.<br>She cried and I cried.<br>She whispered &#8216;I thought you were dead.&#8217;",
				"story-teaser":		"<strong>Amina J.</strong>Reunited with her mother by the Canadian Red<br>Cross after 17 years of separation.",
				"story-button":		"See the full Story",
				"donate-text":		"You have the power to reunite more families<br>like Amina&#8217;s. Refuse to ignore people in crisis.",
				"donate-button":	"Donate Now",
				"story-title":		"Amina&#8217;s Story",
				"story-subtitle":	"Reunited with her mother by<br>the Canadian Red Cross<br>after 17 years of separation",
				"story-content":	"<p>Amina never expected to hear her mother’s voice again. Nearly 20 years had passed since Amina had last seen her mother, just before a bomb destroyed her family home. The attack sent the 11-year-old girl running for her life through the war-torn streets of Mogadishu, Somalia.</p><p>From Somalia, Amina ended up in Winnipeg as a refugee with no idea where her family may be or if they were even alive. Eventually, she sought help from the Canadian Red Cross’ Restoring Family Links program.</p><p>Within months, the 28 year-old mother of two received a small piece of paper from the Canadian Red Cross – and on it was her mother’s phone number.</p><p>Amina spoke into the phone that day and said four words her mother had been waiting nearly two decades to hear. Four words that brought her family back together: &#8216;Mama, I am alive.&#8217;</p>",
				"solved-title":		"Congratulations!",
				"solved-subtitle":	"It took <span>X</span> steps to restore this photo. &nbsp;Take one more step and<br>you can restore a family.",
				"solved-content":	"<p>You’ve completed one puzzle, now take the next step and put the pieces of a shattered family back together. By making a donation today, you enable our Restoring Family Links program to reunite loved ones separated when disaster strikes.</p><p>Each year, the Canadian Red Cross makes over 1,100 family reconnections. This program is working and with your help, more mothers can hear from their daughters, more husbands can tell their wives that they are alive, more family members can send a reassuring message of love.</p><p>You might have a photo of a loved one on your desk right now. Maybe it’s in your wallet or kept on your phone. Have a look at it and just think about being unable to talk to that person no matter how hard you tried. Now imagine someone giving you the power to reconnect.</p><p>You can help the Red Cross provide that link. You can help end that anguish and worrying.</p>",
				"resign-title":		"Nice Try!",
				"resign-subtitle":	"Glad we could help you restore this photo. Please return the favour and help us restore a family.",
				"help-button":		"Help <i class='icon-help'></i>",
				"help-tooltip":		"To begin, click on a puzzle piece that is next to the empty square. Continue moving pieces to restore the photo." //You can restore this family photo by clicking on a puzzle piece to slide it to the empty space
			},
			"fr": {
				"loading-box":		"Lorsqu’une famille est dispersée par l’adversité, une photo est souvent la seule façon d’entretenir l’espoir. Avec votre soutien, nous pouvons aider à réunifier cette famille.",
				"title":			"Lorsque vous donnez, nous donnons.",
				"subtitle":			"Chaque jour, des familles sont séparées par la guerre et les catastrophes.<br>Chaque jour, nous mettons tout en œuvre pour les réunir.",
				"quote":			"J’ai dit: &nbsp;« Maman, c’est<br>bien toi ? »<br>Elle a répondu:<br>« Qui est-ce ? »<br>Je lui ai dit que j'étais<br>sa fille.<br>Elle a pleuré, j'ai pleuré,<br>puis elle a chuchoté:<br>« Je te croyais morte. »",
				"story-teaser":		"<strong>Amina J.</strong>Réunie avec sa mère par la Croix-Rouge canadienne<br>après 17 ans de séparation.",
				"story-button":		"Lisez l’histoire complète",
				"donate-text":		"Vous avez le pouvoir de réunir d’autres familles comme celle d’Amina. Ne fermez pas les yeux sur la détresse des gens.",
				"donate-button":	"Faites un don",
				"story-title":		"L’histoire d’Amina",
				"story-subtitle":	"Réunie avec sa mère par la<br>Croix-Rouge canadienne<br>après 17 ans de séparation",
				"story-content":	"<p>Amina ne pensait plus jamais entendre la voix de sa mère. Elle avait perdu toute trace d’elle près de vingt ans plus tôt lorsque la maison familiale fut détruite par une bombe. Amina, alors âgée de 11 ans, s’était enfuie dans les rues ravagées de Mogadiscio, en Somalie, pour échapper à la mort.</p><p>Après la Somalie, Amina fut accueillie à Winnipeg en tant que réfugiée. Elle n’avait aucune idée de l’endroit où se trouvait sa famille, ni même si elle avait survécu aux attaques. Puis, elle fit appel au Programme de rétablissement des liens familiaux de la Croix-Rouge canadienne.</p><p>Quelques mois plus tard, Amina, maintenant âgée de 28 ans et mère de deux enfants, reçut un petit bout de papier de la Croix-Rouge canadienne : le numéro de téléphone de sa mère y était inscrit.</p><p>Amina l’a appelée le jour même et lui a dit les cinq mots que sa mère espérait entendre depuis près de deux décennies. Cinq mots qui leur ont permis de renouer avec le passé : « Maman, je suis en vie. »</p>",
				"solved-title":		"Félicitations!",
				"solved-subtitle":	"Vous avez rétabli cette image en <span>X</span> étapes. Une étape de plus et vous pourriez faire de même pour une famille.",
				"solved-content":	"<p>Maintenant que vous avez complété le casse-tête, vous pourriez relever un défi encore plus grand. En appuyant notre Programme de rétablissement des liens familiaux dès maintenant, vous aiderez à réunir les familles dont les membres ont été dispersés par une catastrophe.</p><p>Chaque année, la Croix-Rouge canadienne effectue plus de 1 100 rétablissements familiaux. Ce programme a fait ses preuves. Avec votre aide, il permettra d’aider encore plus de mères à retrouver leur fille disparue; d’hommes à donner signe de vie à leur épouse rongée par l’inquiétude; de membres d’une famille à échanger des messages rassurants.</p><p>Vous gardez peut-être une photo d’un être cher sur votre bureau, dans votre portefeuille ou dans votre téléphone. Jetez-y un coup d’œil et imaginez qu’il vous soit impossible de communiquer avec cette personne, même si vous remuez ciel et terre. Maintenant, imaginez que quelqu’un vous donne le pouvoir de rétablir le contact.</p><p>Vous pouvez aider la Croix-Rouge à rétablir ce lien. Vous pouvez mettre un terme à l’angoisse et à la détresse.</p>",
				"resign-title":		"Bel essai!",
				"resign-subtitle":	"Nous sommes ravis de vous avoir aidé à rétablir cette image. Rendez-nous la pareille en nous aidant à rétablir des liens familiaux.",
				"help-button":		"Aide <i class='icon-help'></i>",
				"help-tooltip":		"Pour débuter, cliquez sur une pièce près de la case vide. Déplacez ainsi les autres pièces pour rétablir la photo."
			},
			"error":	"failed to load copy"
		},
		puzzle = {
			rows: 3,
			cols: 3,
			shuffle: true,
			numbers: false,
			language: lang.split('-')[0],
			control: {
				timer: false
			},
			style: {
				gridSize: 1
			},
			success: {
				callback: function (results) {
					var $container = $('#puzzle-solved'),
						$title = $container.find('h2'),
						$subtitle = $container.find('h3');
					if (!results.resigned){
						$title.html(i18n[lang]['solved-title']);
						$subtitle.html(i18n[lang]['solved-subtitle']);
						$subtitle.find('span').html(results.moves);
					} else {
						$title.html(i18n[lang]['resign-title']);
						$subtitle.html(i18n[lang]['resign-subtitle']);
					}
					$('#background2').fadeIn(2500);
					$('#family-story, #btnSolve, #btnHelp, #tooltip').fadeOut(1250);
					$container.fadeIn(1000, function () {
						$('#ps').tinyscrollbar();
					});
				}
			}
		};
	browserSupports = (function () { // lightweight boolean test for css support
		var div = document.createElement('div'),
			vendors = 'Khtml Ms O Moz Webkit'.split(' '),
			numVds = vendors.length;
		return function(prop){
			if ( prop in div.style ) return true;
			prop = prop.replace(/^[a-z]/, function(val) {
				return val.toUpperCase();
			});
			for(var i = 0; i < numVds; i += 1) {
				if ( vendors[i] + prop in div.style ) return true;
			}
			return false;
		};
	})();
	// Map select strings from a JSON object to DOM elements, using html data-attributes
	function setLang(str) {
		var $copyElements = $('[data-i18n]'); // store in memory: DOM elements requiring translation
		lang = i18n.hasOwnProperty(lang = str.replace('-', '_').toLowerCase().split('_')[0]) ? lang : 'en';	//  catch en & fr locales
		$('body').addClass(lang); // for CSS flexibility (ex. reducing font size for longer french copy)	// (fallback to english)
		$copyElements.each(function () {
			var prop = $(this).data('i18n'),
				language = i18n[lang];
			if (language.hasOwnProperty(prop)) { $(this).html(language[prop]); }
			else { $(this).html(i18n.error); } /* use for debugging */
		});
	}
	// Initialize UI elements
	function displayUI () {
		var $html = $('html'), $tooltip = $('#tooltip');
		if ( $html.hasClass('ie7') || $html.hasClass('ie8') ) {
			$('.button, .puzzleButton').addClass('loading');
		}
		$('#puzzle').fadeIn(100, function () {
			if (!browserSupports('transition')){
				$(this).css('top','1500px').animate({'top':'0px'}, 1000, function () {
					$('#puzzleBtns').fadeIn(500, function(){
						if ( $html.hasClass('ie7') || $html.hasClass('ie8') ) {
							$('.button, .puzzleButton').removeClass('loading');
							$tooltip.addClass('loading');
						}
						$tooltip.fadeIn(800, function () {
							if ( $html.hasClass('ie7') || $html.hasClass('ie8') ) {
								$tooltip.removeClass('loading');
							}
						});
					});
				});
			} else {
				setTimeout(function () {
					$('#puzzleBtns').fadeIn(500, function () {
						$tooltip.fadeIn(800);
					});
				}, 1000);
			}
			$(this).removeClass('flipped shifted');
			$(this).find('img').jqPuzzle(puzzle);
		});
		$('#content').fadeIn(1000);
		$('#loading-box').fadeOut(1000);
	}
	// Add/Remove CSS Classes in IE on window.resize
	function responsiveIE () {
		var $html = $('html'), $body = $('body');
		if ($html.hasClass('ie8')) {
			if ( $(window).height() <= 890 ){
				$body.removeClass('ie-resp-728').addClass('ie-resp-890');
			}
			if ( $(window).height() <= 728 ) {
				$body.removeClass('ie-resp-890').addClass('ie-resp-728');
			}
			if ( $(window).height() > 890 ) {
				$body.removeClass('ie-resp-890 ie-resp-728');
			}
		}
	}
	// DOM Ready
	$(function () {
		responsiveIE();
		setLang(lang);
		$('#background').removeClass('zoom');
		setTimeout(function () {
			itsTime = true;
			if (loaded) { displayUI(); }
		}, 4500);
		$('#preloader').imagesLoaded(function () {
			loaded = true;
			if (itsTime) { displayUI(); }
		});
		// Button Events
		$('#btn-story').on('click', function (e) {
			e.preventDefault();
			$('#family-story').fadeIn(1000, function () {
				$('#fs').tinyscrollbar();
			});
		});
		$('#btnHelp').on('click', function (e) {
			e.preventDefault();
			$('#tooltip').fadeIn(500);
		});
		$('.icon-close').on('click', function () {
			$(this).parent().fadeOut(500);
		});
		window.onresize = function(e){
			responsiveIE();
		};
	});
})(window.jQuery);