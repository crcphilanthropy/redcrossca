	var iterate = function (a, n) {
		var current = 0,
			l = a.length;
		return function () {
			var end = current + n,
				part = a.slice(current, end);
			current =  end < l ? end : 0;
			return part;
		};
	};

(function(){
	puzzle: {
		rows: 3,
		cols: 3,
		shuffle: true,
		numbers: false,
		language: lang,
		control: {
			timer: false
		},
		success: {
			callback: function (results) {
				if (!results.resigned){
					$('#puzzle-solved h3').find('span').html(results.moves);
				} else {
					$('#puzzle-solved h2').html(i18n[lang]['resign-title']);
					$('#puzzle-solved h3').html(i18n[lang]['resign-subtitle']);
				}
				$('#background2').fadeIn(2500);
				$('#family-story').fadeOut(1250);
				$('#puzzle-solved').fadeIn(1000, function () {
					$('#ps').tinyscrollbar();
				});
			}
		}
	}

	// multi-lingual copy definitions
	i18n: {
		"en": {
			"title":			"When you give, we give.",
			"subtitle":			"Every day families are separated because of war and disaster.<br>Every day we work to reunite them.",
			"quote":			"I said &#8216;Mom, is this you?&#8217;<br>She asked &#8216;Who are you?&#8217;<br>I told her it was her daughter calling.<br>She cried and I cried.<br>She whispered &#8216;I thought you were dead.&#8217;",
			"story-teaser":		"<strong>Ayaan Ahmed Jama</strong>Reunited with her mother by the Canadian Red<br>Cross after 17 years of separation.",
			"story-button":		"See the full Story","donate-text":"You have the power to reunite more families like<br>Ayaan&#8217;s. Help us refuse to ignore people in crisis.",
			"donate-button":	"Donate Now",
			"help-button":		"Help <i class='help'></i>",
			"help-tooltip":		"You can restore this family photo by clicking on a puzzle piece to slide it to the empty space",
			"story-title":		"Ayaan&#8217;s Story",
			"story-subtitle":	"Reunited with her mother by<br>the Canadian Red Cross<br>after 17 years of separation",
			"story-content":	"<p>Ayaan never expected to hear her mother’s voice again. Nearly 20 years had passed since Ayaan had last seen her mother, just before a bomb destroyed her family home. The attack sent the 11-year-old girl running for her life through the war-torn streets of Mogadishu, Somalia.</p><p>From Somalia, Ayaan ended up in Winnipeg as a refugee with no idea where her family may be or if they were even alive. Eventually, she sought help from the Canadian Red Cross’ Restoring Family Links program.</p><p>Within months, the 28 year-old mother of two received a small piece of paper from the Canadian Red Cross – and on it was her mother’s phone number.</p><p>Ayaan spoke into the phone that day and said four words her mother had been waiting nearly two decades to hear. Four words that brought her family back together: &#8216;Mama, I am alive.&#8217;</p>",
			"story-footer":		"You have the power to reunite families like<br>Ayaan's. &nbsp;Refuse to ignore people in crisis.",
			"solved-title":		"Congratulations!",
			"solved-subtitle":	"It took <span>X</span> steps to restore this photo. &nbsp;Take one more step and help restore a family in need.",
			"solved-content":	"<p>You’ve completed one puzzle, now take the next step and put the pieces of a shattered family back together. By making a donation today, you enable our Restoring Family Links program to reunite loved ones separated when disaster strikes.</p><p>Each year, the Canadian Red Cross makes over 1,100 family reconnections. This program is working and with your help, more mothers can hear from their daughters, more husbands can tell their wives that they are alive, more family members can send a reassuring message of love.</p><p>You might have a photo of a loved one on your desk right now. Maybe it’s in your wallet or kept on your phone. Have a look at it and just think about being unable to talk to that person no matter how hard you tried. Now imagine someone giving you the power to reconnect.</p><p>You can help the Red Cross provide that link. You can help end that anguish and worrying.</p>",
			"solved-footer":	"You have the power to reunite families like<br>Ayaan's. &nbsp;refuse to ignore people in crisis.",
			"resign-title":		"Nice Try!",
			"resign-subtitle":	"Reconnecting a Family can be tough.  but with your help... something"
		},
		"fr":{

		}
	}