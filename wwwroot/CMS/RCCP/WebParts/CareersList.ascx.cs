﻿using System;
using CMS.GlobalHelper;
using CMS.Helpers;
using CMS.PortalControls;


    public partial class CMSApp_NewCo_WebParts_CareersList : CMSAbstractWebPart
    {
        public string Title
        {
            get { return ValidationHelper.GetString(GetValue("Title"), String.Empty); }
            set { SetValue("Title", value); }
        }

        public string Transformation
        {
            get { return ValidationHelper.GetString(GetValue("Transformation"), String.Empty); }
            set { SetValue("Transformation", value); }
        }

        public string Path
        {
            get { return ValidationHelper.GetString(GetValue("Path"), String.Empty); }
            set { SetValue("Path", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupHeader();
            SetupRepeater();
        }

        private void SetupRepeater()
        {
            rptCareersList.TransformationName = Transformation;
            rptCareersList.Path = Path;
            rptCareersList.DataBind();

            rptCareersList.Visible = rptCareersList.HasData();
        }

        private void SetupHeader()
        {
            hdrTitle.Visible = !String.IsNullOrEmpty(Title);
            hdrTitle.InnerText = Title;
        }

        protected void SortCareerList(object sender, EventArgs e)
        {
            rptCareersList.OrderBy = drpSortBy.SelectedValue;
            rptCareersList.ReloadData(true);
        }
    }
