﻿using System;
using CMS.CustomTables;
using CMS.Helpers;
using CMS.PortalControls;

namespace CMSApp.NewCo.WebParts
{
    public partial class GoogleMaps : CMSAbstractWebPart
    {
        private string _kmlFile;
        public string KMLFile
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["KMLFilePath"]))
                    return Request.QueryString["KMLFilePath"];

                return _kmlFile ?? (_kmlFile = GetKmlFileUrl());
            }
        }

        private string GetKmlFileUrl()
        {
            var relPath = GetCustomTableItemDisplayName(CurrentDocument.GetProperty(CatchmentField), "RedCross.JobCatchment", "KmlFile");
            if (!string.IsNullOrWhiteSpace(relPath))
                return URLHelper.GetAbsoluteUrl(relPath);

            return string.Empty;
        }

        public double Longitude
        {
            get
            {
                return ValidationHelper.GetDouble(CurrentDocument.GetProperty("Longitude"), 0);
            }
        }

        public double Latitude
        {
            get
            {
                return ValidationHelper.GetDouble(CurrentDocument.GetProperty("Latitude"), 0);
            }
        }

        public string MarkerTitle
        {
            get
            {
                return ValidationHelper.GetString(CurrentDocument.GetProperty("Title"), string.Empty);
            }
        }

        public string CatchmentField
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue("CatchmentField"), "Catchment");
            }
            set
            {
                this.SetValue("CatchmentField", value);
            }
        }

        private string GetCustomTableItemDisplayName(object itemGuid, string className, string columnName)
        {
            var guid = ValidationHelper.GetGuid(itemGuid, Guid.Empty);
            return guid == Guid.Empty ? string.Empty : CustomTableItemProvider.GetItem(guid, className).GetStringValue(columnName, string.Empty);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Latitude == 0 && Longitude == 0 && string.IsNullOrWhiteSpace(KMLFile))
                this.Visible = false;

            DataBind();
        }

        public string AddKMLOverlay()
        {
            if (string.IsNullOrWhiteSpace(KMLFile))
                return string.Empty;

            return string.Format("var ctaLayer = new google.maps.KmlLayer('{0}'); ctaLayer.setMap(mmap);", KMLFile);
        }

        public string AddMarker()
        {
            if (Longitude == 0 && Latitude == 0)
                return string.Empty;

            return string.Format("var marker = new google.maps.Marker({{ position: center, map: mmap, title: '{0}' }});  mmap.setCenter(marker.getPosition());", MarkerTitle);
        }
    }
}