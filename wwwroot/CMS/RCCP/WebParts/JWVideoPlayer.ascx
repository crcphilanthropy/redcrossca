﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JWVideoPlayer.ascx.cs"
    Inherits="CMSApp.NewCo.WebParts.JWVideoPlayer" %>
<script type='text/javascript' src='/newco/mediaplayer-5.10/jwplayer.js'></script>
<h4><%# VideoTitle %></h4>
<div id='<%# VideoID %>'></div>
<script type='text/javascript'>
    jwplayer('<%# VideoID %>').setup({
        'flashplayer': '/newco/mediaplayer-5.10/player54-yt-as3.swf',
        'file': '<%# JWVideoUrl %>',
        'controlbar': 'bottom',
        'width': '<%# Width %>',
        'height': '<%# Height %>'
    });

</script>
