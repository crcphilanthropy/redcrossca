﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleMaps.ascx.cs"
    Inherits="CMSApp.NewCo.WebParts.GoogleMaps" %>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<asp:Panel ID="pnlMap" runat="server" CssClass="map_canvas">
</asp:Panel>
<script type="text/javascript">

        var center = new google.maps.LatLng(<%# Latitude %>, <%# Longitude %>);
        var mapOptions = {
          zoom:10,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var mmap = new google.maps.Map(document.getElementById("<%# pnlMap.ClientID %>"), mapOptions);

        <%# AddKMLOverlay() %>
        <%# AddMarker() %>

</script>
