﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareersList.ascx.cs" CodeFile="~/NewCo/WebParts/CareersList.ascx.cs" Inherits="CMSApp_NewCo_WebParts_CareersList" %>

<asp:UpdatePanel  ID="plhContainer" runat="server" class="careers-list">
    <ContentTemplate>

        <h6 id="hdrTitle" runat="server" class="uppercase"></h6>
    
        <span>Sort by </span>
        <asp:DropDownList ID="drpSortBy" runat="server" OnSelectedIndexChanged="SortCareerList" AutoPostBack="True">
            <asp:ListItem Value="Title">Title</asp:ListItem>
            <asp:ListItem Value="Type">Type</asp:ListItem>
            <asp:ListItem Value="DocumentCreatedWhen">Date Posted</asp:ListItem>
            <asp:ListItem Value="Location">Location</asp:ListItem>
        </asp:DropDownList>
    
        <cms:CMSRepeater ID="rptCareersList" runat="server" 
                        ClassNames="RedCross.Job"
                        OrderBy="DocumentCreatedWhen DESC, Title">
            <HeaderTemplate>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th>Job Title</th>
                             <th>Type</th>
                             <th>Location</th>
                             <th>Date Posted</th>
                         </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </cms:CMSRepeater>
    </ContentTemplate>
</asp:UpdatePanel>

