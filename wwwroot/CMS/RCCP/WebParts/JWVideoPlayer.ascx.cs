﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;
using CMS.PortalControls;
using CMS.GlobalHelper;

namespace CMSApp.NewCo.WebParts
{
    public partial class JWVideoPlayer : CMSAbstractWebPart
    {
        private string _videoId = string.Empty;
        public string VideoID
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_videoId))
                    _videoId = string.Format("video-id-{0}", Guid.NewGuid());

                return _videoId;
            }
        }

        public string VideoTitle
        {
            get { return ValidationHelper.GetString(GetValue("VideoTitle"), string.Empty); }
            set { SetValue("VideoTitle", value); }
        }

        public string VideoUrl
        {
            get { return ValidationHelper.GetString(GetValue("VideoUrl"), string.Empty); }
            set { SetValue("VideoUrl", value); }
        }

        public string YoutubeUrl
        {
            get { return ValidationHelper.GetString(GetValue("YoutubeUrl"), string.Empty); }
            set { SetValue("YoutubeUrl", value); }
        }

        public string JWVideoUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(VideoUrl))
                    return URLHelper.GetAbsoluteUrl(VideoUrl);

                return YoutubeUrl;
            }
        }

        public int Width
        {
            get { return ValidationHelper.GetInteger(GetValue("Width"), 470); }
            set { SetValue("Width", value); }
        }

        public int Height
        {
            get { return ValidationHelper.GetInteger(GetValue("Height"), 320); }
            set { SetValue("Height", value); }
        }

        #region "Events"

        /// <summary>
        /// Content loaded event handler.
        /// </summary>
        public override void OnContentLoaded()
        {
            base.OnContentLoaded();
            SetupControl();
        }


        /// <summary>
        /// Reloads data.
        /// </summary>
        public override void ReloadData()
        {
            base.ReloadData();
            SetupControl();
        }

        #endregion

        /// <summary>
        /// Initializes the control properties.
        /// </summary>
        public void SetupControl()
        {
            if (this.StopProcessing) return;

            DataBind();
        }
    }
}