/* trigger when page is ready */
$(document).ready(function (){
  
    var location;
    var geocoder;
    var map;
    
    
    if ($('dl.posting dt')) {
        $('dl.posting dt').filter(function() {
            if ($(this).text() == "Region:") {
                location = $(this).next().text();
                if (location.indexOf(',') > 0) {
                    location = location.slice(location, location.indexOf(',')) + ', Ontario';
                }
            }
        });
    
        var geocoder;
        var map;

        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
 
        geocoder.geocode( {
            'address': location
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    
    }
});