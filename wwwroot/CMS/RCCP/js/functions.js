/* trigger when page is ready */
$(document).ready(function (){
 
    //Last-child fallbacks     
    $('footer ul li:last-child').addClass('last-child');
    $('ul.breadcrumb li:last-child').addClass('last-child');
    
    //Demo Form on Style Guide Page 
    var $myform = $('#my-form').idealforms({ 
    

        });
    //Remove stack class from Forms    
    if ($('#my-form')) {
        $('#my-form').removeClass('stack');
    }  
    
    // Remove right-hand margin for every other service listed  
    if ($('ul.services')) {      
        $('ul.services li:odd').addClass('last');
    }
    
    //FAQ Page Scrolling
    $('a.top').on('click', function(e){
        $('html, body').animate({
            scrollTop: $('div.container').offset().top - 30
        }, 500);
        e.preventDefault();
    });
    
    $('a.faq').on('click', function(e){
        var question = 'h6.' + $(this).attr('class').split(' ')[1];
        $('html, body').animate({
            scrollTop: $(question).offset().top - 30
        }, 500);
        e.preventDefault();
    });
   
});