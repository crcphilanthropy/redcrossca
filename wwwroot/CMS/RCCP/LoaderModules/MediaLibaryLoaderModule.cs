﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using CMS.Base;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.FormEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.WorkflowEngine;

[MediaLibaryLoaderModule]
public partial class CMSModuleLoader
{
    private class MediaLibaryLoaderModuleAttribute : CMSLoaderAttribute
    {

        public const string XPATH_MEDIA_NODE = "//form/field[settings/controlname[text()=\"mediaselectioncontrol\"]]";

        public override void Init()
        {
            DocumentEvents.GetContent.Execute += Document_GetContent_Execute;
        }

        /// <summary>
        /// Attempt to retrieve content for recognized media types
        /// </summary>
        private void Document_GetContent_Execute(object sender, DocumentSearchEventArgs e)
        {
            string path;
            if (ParseMediaNode(e.Node, out path))
            {
                //Note: Can't use Server.MapPath as there's no HttpContext.Current in a thread.

                path = path.Contains("?") ? path.Substring(0, path.IndexOf("?")) : path;
                path = path.StartsWith("~/") ? path.Replace("~/", "") : path;
                path = HttpRuntime.AppDomainAppPath + path.Replace("/", "\\");

                e.Content += MediaSearchHelper.ReadKnownFile(path);
            }
        }


        /// <summary>
        /// Determine if the nodeType uses a mediaselectioncontrol
        /// </summary>
        /// <param name="node">Kentico TreeNode</param>
        /// <param name="path">URL of the associated file</param>
        /// <returns>Whether a media item is found</returns>
        private bool ParseMediaNode(TreeNode node, out string path)
        {
            string pathField = string.Empty;

            //TODO: check cache[node.NodeclassName, pathField] here and skip if found.

            string xml = DataClassInfoProvider.GetDataClassInfo(node.NodeClassName).ClassFormDefinition;
            if (!string.IsNullOrEmpty(xml))
            {
                XDocument xdoc = XDocument.Parse(xml);
                XElement pathNode = ((IEnumerable)xdoc.XPathEvaluate(XPATH_MEDIA_NODE)).Cast<XElement>().FirstOrDefault();

                //If it's a valid media item extract the appropriate column name.
                pathField = pathNode != null ? (string)pathNode.Attribute("column") : string.Empty;
            }

            //TODO: If not found in cache, update cache with value here.

            path = !string.IsNullOrEmpty(pathField) ? (string)node.GetValue(pathField) : string.Empty;
            return !string.IsNullOrEmpty(path);
        }
    }
}
