﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class="news-brief">
<h5><a href="<%# GetDocumentUrl() %>"><%# Eval("NewsTitle") %></a></h5>
<p><%# Eval("NewsSummary") %></p>
</article>