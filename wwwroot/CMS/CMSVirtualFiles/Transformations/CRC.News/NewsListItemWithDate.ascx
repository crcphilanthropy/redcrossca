﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article>
  <p class="h4"><%# GetDateTime("NewsReleaseDate", CMSApp.CRC.TransformationHelper.DateFormat) %></p>
  <a href="<%# GetDocumentUrl() %>"><%# Eval("NewsTitle") %></a>
</article>