﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='news<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "")? " with-image " : " " %> clearfix'>
  <%# IfEmpty(Eval("NewsThumbnail"), IfEmpty(Eval("BraftonLargeImageUrl"), "", "<a href=\""+ GetDocumentUrl()+"\"><img class='left' src='" + (Eval("BraftonLargeImageUrl").ToString().StartsWith("http://") ? Eval("BraftonLargeImageUrl") : "~/CRC/Brafton/" + Eval("BraftonLargeImageUrl")) + "' style='width:69px;' alt=\"" + EvalText("ImageAltText") + "\" title=\"" + EvalText("ImageAltText") + "\" /></a>"),"<a href=\""+ GetDocumentUrl()+"\">"+ValidationHelper.GetString(GetImage("NewsThumbnail", Eval("ImageAltText")),"")+"</a>") %>   
  <div class="content">
        <h3 class="h4"><a href="<%# GetDocumentUrl()%>"><%# Eval("NewsTitle") %></a></h3>
        <div class="TextContent"><strong><%# GetDateTime("NewsReleaseDate", "d") %></strong> - <%# Eval("NewsSummary") %></div>
        <a href="<%# GetDocumentUrl()%>"><%# ResHelper.GetString("CRC.News.ReadFullStory") %></a>
    </div>
</div>