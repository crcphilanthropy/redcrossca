﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><!--<%# Eval("NewsTitle",true) %>-->

<p>
    <%# CMSApp.CRC.TransformationHelper.GetNewsTopics(ValidationHelper.GetInteger(Eval("DocumentID"),0)) %>
    <%# IfEmpty(Eval("Author"), string.Empty, string.Format("{0} | ",Eval("Author"))) %>
    <%# GetDateTime("NewsReleaseDate", CMSApp.CRC.TransformationHelper.DateFormat) %>
</p>

<div class="rich-text">
    
    <%# IfEmpty(Eval("NewsImage"), IfEmpty(Eval("BraftonLargeImageUrl"), "", "<img class='left' src='" + (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),String.Empty).StartsWith("http://") ? Eval("BraftonLargeImageUrl") : "~/CRC/Brafton/" + ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),string.Empty)) + "' alt=\"" + EvalText("ImageAltText") + "\" title=\"" + EvalText("ImageAltText") + "\" class='imgLeft' />"), string.Format("<span class='imgLeft'>{0}</span>", GetImage("NewsImage",null, null, null,Eval("ImageAltText")))) %>
    <%# Eval("NewsText") %>

</div>