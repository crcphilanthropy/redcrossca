﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits=" CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>


<div class="clearfix listing <%# !IsFirst() ? "border-top" : "" %>">
    <asp:PlaceHolder runat="server" Visible='<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "") %>'>
        <div class="columns medium-3 large-4">
            <a href="<%# GetDocumentUrl()%>">
                <img alt="<%# EvalText("ThumbnailAltText") %>" src="<%# GetNewsThumbnailImage(EvalGuid("NewsThumbnail"),EvalText("BraftonLargeImageUrl"), EvalText("NodeAliasPath"),true) %>" />
            </a>
        </div>
    </asp:PlaceHolder>
    <div class="<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "") ? "columns medium-9 large-8" : "" %> desc">
        <a href="<%# GetDocumentUrl()%>"><%# Eval("NewsTitle") %></a>
        <p>
            <strong><%# GetDateTime("NewsReleaseDate", CMSApp.CRC.TransformationHelper.DateFormat) %></strong> -  <%# Eval("NewsSummary") %>

            <br />
            <a href="<%# GetDocumentUrl()%>"><%# ResHelper.GetString("CRC.News.ReadFullStory") %></a>
        </p>
    </div>
</div>

