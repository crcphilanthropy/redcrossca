﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits=" CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>


<div class="clearfix listing <%# !IsFirst() ? "border-top" : "" %>">
    <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "") %>'>
        <div class="columns medium-3 large-4">
            <a href="<%# GetDocumentUrl()%>">
                <img alt="<%# EvalText("ThumbnailAltText") %>" src="<%# GetNewsThumbnailImage(EvalGuid("NewsThumbnail"),EvalText("BraftonLargeImageUrl"), EvalText("NodeAliasPath"),true) %>" />
            </a>
        </div>
    </asp:PlaceHolder>
    <div class="<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "") ? "columns medium-9 large-8" : "" %> desc">
        <a href="<%# GetDocumentUrl()%>"><%# Eval("NewsTitle") %></a>
        <p>
            <%# Eval("NewsSummary") %>
        </p>
    </div>
</div>