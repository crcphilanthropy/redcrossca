﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>


<div class="featured-list">

      <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),"") != "" | ValidationHelper.GetString(Eval("NewsThumbnail"),"") != "") %>'>
            <a href="<%# GetDocumentUrl()%>">
                <img alt="<%# EvalText("ThumbnailAltText") %>" src="<%# GetNewsThumbnailImage(EvalGuid("NewsThumbnail"),EvalText("BraftonLargeImageUrl"), EvalText("NodeAliasPath")) %>" />
            </a>

    </asp:PlaceHolder>

    <a href="<%# GetDocumentUrl() %>">
        <h3><%# Eval("NewsTitle") %></h3>
    </a>
             <p><%# Eval("NewsSummary") %> </p>  

</div>

