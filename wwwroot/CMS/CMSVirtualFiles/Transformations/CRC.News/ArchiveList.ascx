﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><a href='<%# Eval("Url")%>?month=<%# Eval("Month") %>-<%# Eval("Year") %>'>
    <%# System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((int)Eval("Month")) %> 
    <%# Eval("Year") %> (<%# Eval("Count") %>)
</a>