﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class='<%# DisplayIndex % 5 == 0 ? "first" : ""%><%#  (DataItemIndex==(DataRowView.DataView.Count-1)) ? " last" : "" %>'>
    <a href="<%# GetDocumentUrl() %>"><%# IfEmpty(Eval("NewsThumbnail"), IfEmpty(Eval("BraftonLargeImageUrl"), "", "<img src='" + (ValidationHelper.GetString(Eval("BraftonLargeImageUrl"),string.Empty).StartsWith("http://") ? Eval("BraftonLargeImageUrl") : "~/CRC/Brafton/" + Eval("BraftonLargeImageUrl")) + "' style='width:146px;' alt=\"" + EvalText("ImageAltText") + "\" title=\"" + EvalText("ImageAltText") + "\" />"), GetImage("NewsThumbnail", 146, 88)) %></a>
    <div class="link-text"><a href="<%# GetDocumentUrl() %>"><%# Eval("NewsTitle") %></a></div>
</article>




