﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class='<%# DisplayIndex % 3 == 0 ? " first" : ""%><%# DisplayIndex - 1 % 3 == 0 ? " last" : "" %>'>
        <h6 title="<%# Eval("NewsTitle") %>"><%# TextHelper.LimitLength(Eval("NewsTitle").ToString(),25,"...",false) %></h6>
    <p title="<%# Eval("NewsSummary") %>"><%# TextHelper.LimitLength(Eval("NewsSummary").ToString(),50,"...",false) %></p>
    <a href="<%# GetDocumentUrl() %>"><%# ResHelper.GetString("CRC.News.ReadFullStory") %></a>       
</article>
