﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="video-grid-item columns small-6 medium-3">
  <div class="thumbnail">
    <img src="<%# Eval("Thumbnail") %>" alt="">
    <span class="video-length"><%# Eval("VideoLength") %><span>
  </div>
  <h3><%# Eval("Name") %></h3>
  <div class="tags">
    <%# Eval("Categories") %>
    <%# Eval("Regions") %>
  </div>
</div>