﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Video Summary:</td>
    <td><%# Eval("VideoSummary") %></td>
  </tr>
  <tr>
    <td>Video Length:</td>
    <td><%# Eval("VideoLength") %></td>
  </tr>
  <tr>
    <td>Subtitles:</td>
    <td><cms:CMSCheckBox ID="CheckBox3" Runat="server" Checked='<%# Eval("Subtitles") %>' Text="Subtitles" /></td>
  </tr>
  <tr>
    <td>Transcript:</td>
    <td><%# Eval("Transcript") %></td>
  </tr>
</table>
