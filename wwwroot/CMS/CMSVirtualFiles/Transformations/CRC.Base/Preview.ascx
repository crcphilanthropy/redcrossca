﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Facebook Title:</td>
    <td><%# Eval("OGTitle") %></td>
  </tr>
  <tr>
    <td>Facebook Description:</td>
    <td><%# Eval("OGDescription") %></td>
  </tr>
  <tr>
    <td>Facebook Image:</td>
    <td><%# Eval("OGImage") %></td>
  </tr>
</table>
