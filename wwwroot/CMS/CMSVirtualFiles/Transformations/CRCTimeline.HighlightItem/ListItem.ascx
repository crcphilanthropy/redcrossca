﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="highlight">
    <img data-interchange="[<%# EvalText("SmallImage").Replace("~",string.Empty) %>, small], [<%# EvalText("Image").Replace("~",string.Empty) %>, large]" alt="<%# EvalText("ImageAltText") %>" />
    <noscript>
        <img src="<%# EvalText("Image").Replace("~",string.Empty) %>" alt="<%# EvalText("ImageAltText") %>"></noscript>
    <div class="row <%# (DataItemIndex % 2) > 0 ? "align-right" : "" %>">
        <div class="columns large-6 details">
            <h2><%# Eval("Title") %></h2>
            <%# Eval("Description") %>
        </div>
    </div>
</div>
