﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>OG Title:</td>
    <td><%# Eval("OGTitle") %></td>
  </tr>
  <tr>
    <td>OG Description:</td>
    <td><%# Eval("OGDescription") %></td>
  </tr>
  <tr>
    <td>OG Image:</td>
    <td><%# Eval("OGImage") %></td>
  </tr>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Image:</td>
    <td><%# Eval("Image") %></td>
  </tr>
  <tr>
    <td>Image Alt Text:</td>
    <td><%# Eval("ImageAltText") %></td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><%# Eval("Description") %></td>
  </tr>
</table>
