﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Quote Text:</td>
    <td><%# Eval("QuoteText") %></td>
  </tr>
  <tr>
    <td>Image:</td>
    <td><%# Eval("Image") %></td>
  </tr>
  <tr>
    <td>Quoter:</td>
    <td><%# Eval("Quoter") %></td>
  </tr>
  <tr>
    <td>Quote Type:</td>
    <td><%# Eval("QuoteType") %></td>
  </tr>
</table>
