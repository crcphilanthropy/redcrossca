﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMS.ExtendedControls" %>

<div class="blog-post <%# string.IsNullOrWhiteSpace(GetSearchImageUrl("",90) ?? string.Empty) || (GetSearchImageUrl("",90) ?? string.Empty).Contains(Guid.Empty.ToString()) ? string.Empty : "with-thumbnail" %>">
    <div class="post-body">
        <div class="post-header">
            <h2>
                <a href="<%# SearchResultUrl(true) %>"><%#SearchHighlight(HTMLHelper.HTMLEncode(ControlsHelper.RemoveDynamicControls(DataHelper.GetNotEmpty(Eval("Title"), "/"))), "<span class='high-light'>", "</span>")%></a>
            </h2>
            <%# CMSApp.CRC.TransformationHelper.GetBlogOwnerInfo(DataItem) %>
        </div>
        <div class="post-content">
            <p><%#SearchHighlight(HTMLHelper.HTMLEncode(TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(ControlsHelper.RemoveDynamicControls(GetSearchedContent(DataHelper.GetNotEmpty(Eval("Content"), ""))), false, " ")), 280, "...")), "<span class='high-light'>", "</span>")%></p>
        </div>
    </div>
    <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrWhiteSpace(GetSearchImageUrl("",90) ?? string.Empty) && !(GetSearchImageUrl("",90) ?? string.Empty).Contains(Guid.Empty.ToString())  %>'>
        <div class="post-image">
            <%# IfEmpty( GetSearchImageUrl("",90), string.Empty, "<img src=\"" +  GetSearchImageUrl("",0) + "\" />") %>
        </div>
    </asp:PlaceHolder>
</div>

