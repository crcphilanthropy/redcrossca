﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="post-header">
    <p class="post-metadata"><%# ResHelper.GetString("CRC.Blog.Posted")%> <%# GetDateTime("BlogPostDate", CMSApp.CRC.TransformationHelper.DateFormat) %>
    <asp:placeholder runat="server" visible='<%# !String.IsNullOrWhiteSpace(EvalText("NodeOwnerFullName")) %>'>
     <%# ResHelper.GetString("CRC.Blog.By")%> <a href="<%# CMSApp.CRC.TransformationHelper.GetBlogFilterPath("author",EvalText("NodeOwnerUserName")) %>"><%# EvalText("NodeOwnerFullName") %></a>
    </asp:placeholder>
    <asp:placeholder runat="server" visible='<%# !String.IsNullOrWhiteSpace(EvalText("BloggerSignature")) %>'>
    - <%# Eval("BloggerSignature") %>
    </asp:placeholder>
    </p>
</div>
<div class="post-body">
    <div class="post-content">
        <%# Eval("BlogPostBody") %>
    </div>
</div>

                    