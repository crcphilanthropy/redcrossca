﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="post-header">
    <h4>
        <a href="<%# GetDocumentUrl() %>"><%# Eval("BlogPostTitle") %></a>
    </h4>
    <p class="post-metadata">      
      <%# GetDateTime("BlogPostDate", ResHelper.GetString("CRC.Blog.DateTimeFormat")) %>, 
      <%# ResHelper.GetString("CRC.Blog.By")%> 
      <a href="<%# CMSApp.CRC.TransformationHelper.GetBlogFilterPath("author",EvalText("NodeOwnerUserName")) %>">
        <%# EvalText("NodeOwnerFullName") %>
      </a>
    </p>
</div>