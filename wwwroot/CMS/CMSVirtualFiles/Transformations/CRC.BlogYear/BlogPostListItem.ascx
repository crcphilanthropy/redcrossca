﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Register Src="~/CRC/webparts/Blogs/BlogCategoriesAndTags.ascx" TagPrefix="uc1" TagName="BlogCategoriesAndTags" %>
<div class="blog-post <%# IfImage("BlogPostTeaser", "with-thumbnail", string.Empty) %>">

                            <div class="post-body">

                                <div class="post-header">
                                    <h2>
                                        <a href="<%# GetDocumentUrl() %>"><%# EvalText("BlogPostTitle") %></a>
                                    </h2>
                                    <p class="post-metadata"><%# ResHelper.GetString("CRC.Blog.Posted")%> <%# GetDateTime("BlogPostDate", CMSApp.CRC.TransformationHelper.DateFormat) %>
                                      <asp:placeholder runat="server" visible='<%# !String.IsNullOrWhiteSpace(EvalText("NodeOwnerFullName")) %>'>
                                       <%# ResHelper.GetString("CRC.Blog.By")%> <a href="<%# CMSApp.CRC.TransformationHelper.GetBlogFilterPath("author",EvalText("NodeOwnerUserName")) %>"><%# EvalText("NodeOwnerFullName") %></a>
                                      </asp:placeholder>
                                      <asp:placeholder runat="server" visible='<%# !String.IsNullOrWhiteSpace(EvalText("BloggerSignature")) %>'>
                                      - <%# Eval("BloggerSignature") %>
                                      </asp:placeholder>
                                      </p>
                                </div>
                                <div class="post-image-inline hide-for-medium-up">
                                <%# IfImage("BlogPostTeaser", GetImage("BlogPostTeaser"), string.Empty) %>
                                </div>

                                <div class="post-content">
                                    <p><%# Eval("BlogPostSummary") %></p>
                                </div>

                            </div>

                            <div class="post-image hide-for-small-only">
                              <%# IfImage("BlogPostTeaser", GetImage("BlogPostTeaser"), string.Empty) %>
                            </div>

                            <uc1:BlogCategoriesAndTags runat="server" ID="BlogCategoriesAndTags" DocumentID='<%# EvalInteger("DocumentID")%>' />

                        </div>