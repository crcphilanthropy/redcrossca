﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Tile") %></td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><%# Eval("Description") %></td>
  </tr>
  <tr>
    <td>Image:</td>
    <td><%# Eval("Image") %></td>
  </tr>
</table>
