﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Import Namespace="CMSAppAppCode.Old_App_Code.CRC" %>
<!-- Map Start -->

<div class="gmaps">
    <a href="https://maps.google.com/maps?daddr=<%# CurrentBranch.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")) %>,<%# CurrentBranch.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")) %>" target="_blank">
        <img src="<%# string.Format(Variables.StaticGoogleMap, CurrentBranch.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentBranch.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),650,411) %>"
            srcset='<%# string.Format(Variables.StaticGoogleMap, CurrentBranch.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentBranch.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),650,411) %> 1024w, <%# string.Format(Variables.StaticGoogleMap, CurrentBranch.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentBranch.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),550,347) %> 640w, <%# string.Format(Variables.StaticGoogleMap, CurrentBranch.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentBranch.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),450,284) %> 320w'
            sizes="(min-width: 960px) 100vw"
            alt="<%# CurrentBranch.BranchAddress %>">
    </a>
    <div class="desc clearfix">
        <div class="column large-6 medium-6 small-12">
            <p>
                <strong><%# CurrentBranch.Name %></strong>
                <br />
                <%# TransformationHelper.ReplaceNewLine(CurrentBranch.BranchAddress) %><br />
                <%# TransformationHelper.AddClickToCall(TransformationHelper.ReplaceNewLine(CurrentBranch.BranchContact)) %>
            </p>
        </div>
        <div class="column large-6 medium-6 small-12">
            <p>
                <strong><%# ResourceStringHelper.GetString("CRC.IYC.HoursOfOperation", defaultText:"Hours of Operation") %> </strong>
                <br />
                <%# TransformationHelper.ReplaceNewLine(CurrentBranch.BranchHours) %>
            </p>
        </div>
    </div>
</div>

<asp:Repeater runat="server" ID="rptBranchServices">
    <HeaderTemplate>
        <div class="panel-wrap">
            <h3 class="h2"><%# ResourceStringHelper.GetString("CRC.IYC.ServicesOfferedAtBranch", defaultText:"Service Offered at this Branch") %></h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <a href='<%# Eval("DocumentLiveUrl") %>'><%# Eval("Name") %> </a>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </div>
    </FooterTemplate>
</asp:Repeater>
