﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><h4><%# Eval("Title") %></h4>
<p class="posted">Posted <%# FormatDateTime(Eval("ReleaseDate"), "dd-MMM-yyyy")%></p>
<%# Eval("BodyText") %>
