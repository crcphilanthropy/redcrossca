﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Ttile") %></td>
  </tr>
  <tr>
    <td>Location:</td>
    <td><%# Eval("Location") %></td>
  </tr>
  <tr>
    <td>Release Date:</td>
    <td><%# Eval("ReleaseDate") %></td>
  </tr>
  <tr>
    <td>Summary:</td>
    <td><%# Eval("Summary") %></td>
  </tr>
  <tr>
    <td>News Body:</td>
    <td><%# Eval("BodyText") %></td>
  </tr>
</table>
