﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="widget">
    <h5>News</h5>
    <a href="<%# Eval("NodeAliasPath") %>"><%# Eval("Title") %></a>
    <p class="posted">Posted <%# FormatDateTime(Eval("ReleaseDate"), "dd-MMM-yyyy") %></p>
    <%# Eval("Summary") %>
    <a href="/About-Us/News">More News</a>
</div>

