﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><h6><%# Eval("Title") %></h6>
<ul class="unstyled">
    <li><%# GetImageByUrl(Eval("Image"),147,148) %></li>
    <li><%# Eval("Summary") %> </li>    
    <li><a href="<%# Eval("NodeAliasPath") %>" class="readMore">Read More &raquo;</a></li>
</ul>

