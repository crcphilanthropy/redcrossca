﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="TextContent"><%# Eval("NewsText") %></div>

<div id="page-0" class="pages" style="display: block;">
  <div class="question" id="q-0">
    <p><%# Eval("Question") %></p>
  </div>
  <div class="option" id="o-0">
    <div>
      <input type="radio" class="select" name="option" value="Coyote">
      <label for="o-0"><%# Eval("Response1") %></label></div>
    <div>
      <input type="radio" class="select" name="option" value="Gizelle">
      <label for="o-1"><%# Eval("Response2") %></label>
    </div>
    <div>
      <input type="radio" class="select" name="option" value="Cheetah">
      <label for="o-2"><%# Eval("Response3") %></label>
    </div>
    <div>
      <input type="radio" class="select" name="option" value="Dog">
      <label for="o-3"><%# Eval("Response4") %></label>
    </div>
    <div>
      <input type="radio" class="select" name="option" value="Dog">
      <label for="o-4"><%# Eval("Response5") %></label>
    </div>
    <div>
      <input type="radio" class="select" name="option" value="Dog">
      <label for="o-5"><%# Eval("Response6") %></label>
    </div>
  </div>
  <div class="explaination hide" id="e-0">
    <input type="hidden" name="a-0" value="3" class="answer">
    <hr><%# Eval("ResponseRationale") %></div>
  <br><br></div>