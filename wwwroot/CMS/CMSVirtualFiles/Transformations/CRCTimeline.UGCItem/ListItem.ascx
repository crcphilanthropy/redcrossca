﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMSApp.CRC" %>
<div class="item" data-equalizer-watch>
    <div class="item-img">
        <img src="<%# Eval("ItemImage") %>" alt="<%# Eval("ItemName") %>" />
    </div>
    <div class="item-desc">
        <h3><%# Eval("ItemName") %></h3>
        <span class="divider"></span>
        <p><strong><%# Eval("ItemTimePeriod") %></strong></p>
        <p><%# Eval("ItemDescription") %></p>
        <p class="submitted-by">
            <%# EvalBool("ShowFirstName") ? String.Format("{0}: {1}, ", ResourceStringHelper.GetString("CRC.Timeline.SubmittedBy",defaultText:"Submitted by"),Eval("FirstName")) : string.Empty %> <%# FormatDateTime(Eval("DocumentCreatedWhen"),"MMMM yyyy") %>
        </p>
    </div>
</div>
