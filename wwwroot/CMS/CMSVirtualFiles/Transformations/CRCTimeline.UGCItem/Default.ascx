﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>First Name :</td>
    <td><%# Eval("FirstName") %></td>
  </tr>
  <tr>
    <td>Last Name :</td>
    <td><%# Eval("LastName") %></td>
  </tr>
  <tr>
    <td>Item Name:</td>
    <td><%# Eval("ItemName") %></td>
  </tr>
  <tr>
    <td>Item Description:</td>
    <td><%# Eval("ItemDescription") %></td>
  </tr>
  <tr>
    <td>Item Time Period:</td>
    <td><%# Eval("ItemTimePeriod") %></td>
  </tr>
  <tr>
    <td>Item Image:</td>
    <td><%# Eval("ItemImage") %></td>
  </tr>
</table>
