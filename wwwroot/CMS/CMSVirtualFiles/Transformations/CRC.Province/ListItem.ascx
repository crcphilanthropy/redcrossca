﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMSApp.CRC" %>
<li>
    <a href="<%# GetDocumentUrl() %>">
        <img src="<%# Eval("IconPath") %>" alt="<%# EvalText("ThumbnailAltText") %>" class="svg" />
        <p><%# Eval("Name") %></p>
    </a>
</li>
