﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article>
	<a href="<%# SearchResultUrl(true) %>" style="display:none;"><img src="<%# GetSearchImageUrl("~/App_Themes/Default/Images/CMSModules/CMS_SmartSearch/no_image.gif",56) %>" alt="" width="56" height="56"  /></a>
    
	<a href="<%# SearchResultUrl(true) %>"><h5>
      <asp:placeholder runat="server" visible='<%# (GetSearchValue("ClassName") ?? string.Empty).ToString().Equals("CMS.BlogPost", StringComparison.OrdinalIgnoreCase)%>'>
        <span>
      <%# ResHelper.GetString("CRC.FromTheBlogSearch") %>:&nbsp;
          </span>
    </asp:placeholder>
      <%#SearchHighlight(HTMLHelper.HTMLEncode(CMS.ExtendedControls.ControlsHelper.RemoveDynamicControls(DataHelper.GetNotEmpty(Eval("Title"), "/"))), "<span class='high-light'>", "</span>")%></h5></a>
    <asp:placeholder runat="server" visible='<%# (GetSearchValue("ClassName") ?? string.Empty).ToString().Equals("CMS.BlogPost", StringComparison.OrdinalIgnoreCase)%>'>
    <p class="post-metadata">      
      <%# FormatDateTime(GetSearchValue("BlogPostDate"), ResHelper.GetString("CRC.Blog.DateTimeFormat")) %>, 
      <%# ResHelper.GetString("CRC.Blog.By")%>&nbsp;
        <%# GetSearchValue("NodeOwnerFullName") %>
    </p>
      </asp:placeholder>
	<p>
		<%#SearchHighlight(HTMLHelper.HTMLEncode(TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(ControlsHelper.RemoveDynamicControls(GetSearchedContent(DataHelper.GetNotEmpty(Eval("Content"), ""))), false, " ")), 280, "...")), "<span class='high-light'>", "</span>")%>
	</p>
</article>