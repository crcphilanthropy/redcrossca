﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='<%# IsFirst() ? "" : "border-top" %> faq'>
    <div class="question">
        <a><%# Eval("FAQQuestion",true) %></a>
    </div>
    <div title="header" class="answer">
        <div>
            <%# Eval("FAQAnswer") %>
        </div>
    </div>
</div>

