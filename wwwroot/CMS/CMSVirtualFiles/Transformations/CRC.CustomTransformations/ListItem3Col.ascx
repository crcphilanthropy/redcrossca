﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><li class="<%# (DisplayIndex >= 8 )? "column3" : (DisplayIndex >= 4 )? "column2" : "column1" %> <%# ((DisplayIndex % 4) == 0 & DisplayIndex > 0 )? " reset" : ""  %>">
  <a href="<%# GetDocumentUrl() %>"><%# Eval("DocumentName") %></a>
</li>