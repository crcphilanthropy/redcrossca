﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="pagination-wrap text-center border-top">
    <ul class="pagination">
        <li class="arrow <%# EvalText("Page").Equals("1") ? "unavailable" : "" %>"><a href='<%# Eval("PreviousURL", true) %>'>&laquo;</a></li>
        
