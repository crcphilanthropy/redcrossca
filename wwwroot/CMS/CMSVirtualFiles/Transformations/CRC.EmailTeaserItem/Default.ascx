﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Link:</td>
    <td><%# Eval("Link") %></td>
  </tr>
  <tr>
    <td>Teaser:</td>
    <td><%# Eval("Teaser") %></td>
  </tr>
</table>
