﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Top Header:</td>
    <td><%# Eval("TopHeader") %></td>
  </tr>
  <tr>
    <td>Logo Selector:</td>
    <td><%# Eval("LogoSelector") %></td>
  </tr>
  <tr>
    <td>Button 1 Text:</td>
    <td><%# Eval("Button1Text") %></td>
  </tr>
  <tr>
    <td>Button 1 Color:</td>
    <td><%# Eval("Button1Color") %></td>
  </tr>
  <tr>
    <td>Button 1 Link:</td>
    <td><%# Eval("Button1Link") %></td>
  </tr>
  <tr>
    <td>Button 1 Enable in Mobile:</td>
    <td><%# Eval("Button1EnableInMobile") %></td>
  </tr>
  <tr>
    <td>Button 2 Text:</td>
    <td><%# Eval("Button2Text") %></td>
  </tr>
  <tr>
    <td>Button 2 Color:</td>
    <td><%# Eval("Button2Color") %></td>
  </tr>
  <tr>
    <td>Button 2 Link:</td>
    <td><%# Eval("Button2Link") %></td>
  </tr>
  <tr>
    <td>Button 2 Enable in Mobile:</td>
    <td><%# Eval("Button2EnableInMobile") %></td>
  </tr>
  <tr>
    <td>Footer Content:</td>
    <td><%# Eval("FDFooterContent") %></td>
  </tr>
  <tr>
    <td>Right Image:</td>
    <td><%# Eval("RightImage") %></td>
  </tr>
  <tr>
    <td>Enable Image On Mobile:</td>
    <td><%# Eval("EnableImageOnMobile") %></td>
  </tr>
  <tr>
    <td>Footer Content:</td>
    <td><%# Eval("FooterContent") %></td>
  </tr>
</table>
