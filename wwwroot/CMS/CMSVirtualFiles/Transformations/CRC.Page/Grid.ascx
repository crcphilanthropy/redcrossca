﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class='<%# DisplayIndex % 3 == 0 ? " first" : ""%><%# DisplayIndex + 1 % 3 == 0 ? " last" : "" %>'>
    <a href="<%# GetDocumentUrl()%>"><%# GetImage("Thumbnail",-1,-1,-1,ValidationHelper.GetString(Eval("Name"),"")) %></a>
    <h5><%# Eval("Name") %></h5>
    <p><%# Eval("Summary") %></p>
    <a href="<%# GetDocumentUrl()%>">More &raquo;</a>
</article>
