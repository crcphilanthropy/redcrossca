﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class="full-repeater clearfix">
  <asp:PlaceHolder runat="server" Visible='<%# ValidationHelper.GetBoolean(IfEmpty(Eval("Thumbnail"), false,true), false) %>'>
    <div class="columns  medium-3 large-4 icon">
      <%# IfEmpty(Eval("Thumbnail"), "", "<a href=\""+ GetDocumentUrl()+"\">"+ValidationHelper.GetString(GetImage("Thumbnail",-1,146,-1,ValidationHelper.GetString(Eval("ThumbnailAltText"),"")),"")+"</a>") %>
     </div>
  </asp:PlaceHolder>
     <div class="columns <%# IfEmpty(Eval("Thumbnail"),"medium-12 large-12","medium-9 large-8") %>">
       <h3>
         <%# Eval("Name") %>
       </h3>
       <p>
         <%# Eval("Summary") %>
       </p>
       <a href="<%# GetDocumentUrl() %>">
         <%# Localize("{$CRC.Explore$}") %> <%# Eval("Name") %> <i class="fa fa-chevron-right"></i>
       </a>
      </div>
</article>

