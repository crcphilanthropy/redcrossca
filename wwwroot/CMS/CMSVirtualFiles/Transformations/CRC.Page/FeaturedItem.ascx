﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="featured-list">

    <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# ValidationHelper.GetBoolean(IfEmpty(Eval("Thumbnail"), false,true), false) %>'>
        <%# IfEmpty(Eval("Thumbnail"), "", "<a href=\""+ GetDocumentUrl()+"\">"+ValidationHelper.GetString(GetImage("Thumbnail",-1,-1,-1,ValidationHelper.GetString(Eval("ThumbnailAltText"),"")),"")+"</a>") %>
    </asp:PlaceHolder>

    <a href="<%# GetDocumentUrl() %>">
        <h3><%# Eval("Name") %></h3>
    </a>
    <%# Eval("Summary") %>
</div>

<hr />