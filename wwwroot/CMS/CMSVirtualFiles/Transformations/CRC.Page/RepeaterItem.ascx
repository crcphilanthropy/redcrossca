﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article>
    <img src="<%# Eval("Thumbnail") %>" alt="<%# Eval("Name") %>">
    <h3><%# Eval("Name") %></h3>
    <%# Eval("Summary") %>
</article>