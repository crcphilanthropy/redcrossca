﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Document name:</td>
    <td><%# Eval("Name") %></td>
  </tr>
  <tr>
    <td>Thumbnail Image:</td>
    <td><%# Eval("Thumbnail") %></td>
  </tr>
  <tr>
    <td>Menu Group:</td>
    <td><%# Eval("Group") %></td>
  </tr>
</table>
