﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class='story-cta <%# (DisplayIndex == 0)? "noBorder" : "" %>  clearfix'>
    <%# IfEmpty(Eval("Thumbnail"), "", "<a href=\""+ GetDocumentUrl()+"\">"+ValidationHelper.GetString(GetImage("Thumbnail",-1,-1,-1,ValidationHelper.GetString(Eval("Name"),"")),"")+"</a>") %>
    <div class="content">
        <h5><a href="<%# GetDocumentUrl()%>"><%# Eval("Name", true) %></a></h5>
        <p><%# Eval("Summary", true) %></p>
    </div>
</article>