﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Show ROS Banner:</td>
    <td><%# Eval("ShowRosBanner") %></td>
  </tr>
  <tr>
    <td>Show Twitter:</td>
    <td><%# Eval("ShowTwitter") %></td>
  </tr>
  <tr>
    <td>Show Promotional Banner:</td>
    <td><%# Eval("ShowPromotionalBanner") %></td>
  </tr>
  <tr>
    <td>Show Share Buttons:</td>
    <td><%# Eval("ShowMobileShare") %></td>
  </tr>
  <tr>
    <td>Show mobile email sign up widget:</td>
    <td><%# Eval("ShowMobileEmailSignUp") %></td>
  </tr>
  <tr>
    <td>Facebook Title:</td>
    <td><%# Eval("OGTitle") %></td>
  </tr>
  <tr>
    <td>Facebook Description:</td>
    <td><%# Eval("OGDescription") %></td>
  </tr>
  <tr>
    <td>Facebook Image <br />(min. size 600px x 315px):</td>
    <td><%# Eval("OGImage") %></td>
  </tr>
</table>
