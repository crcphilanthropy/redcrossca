﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Course Title:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Course Duration:</td>
    <td><%# Eval("Duration") %></td>
  </tr>
  <tr>
    <td>Short Description:</td>
    <td><%# Eval("ShortDescription") %></td>
  </tr>
  <tr>
    <td>Full Description:</td>
    <td><%# Eval("FullDescription") %></td>
  </tr>
</table>
