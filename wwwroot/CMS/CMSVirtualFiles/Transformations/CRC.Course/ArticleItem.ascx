﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article class="columns">
    <h3><a href="<%# GetDocumentUrl()%>"><%# Eval("Name") %></a></h3>
       
    <p><%# (ValidationHelper.GetString(Eval("Duration"),"") != "") ? ResHelper.GetString("CRC.Duration")+": "+ Eval("Duration")+"<br />": ""  %></p>
            
    <%# Eval("Summary") %>
    
    <p><a href="<%# GetDocumentUrl()%>"><%# ResHelper.GetString("CRC.MoreAboutThisCourse") %></a></p>
</article>
