﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
<xsl:param name="html-content-type" />
<xsl:template match="CMS.CellPhone|CMS.Laptop|CMS.Pda|cms.cellphone|cms.laptop|cms.pda">
<div style="border: 1px solid #CCCCCC;padding:15px 50px 15px 15px">
<h1>Product name: <xsl:value-of disable-output-escaping="yes" select="SKUName"/></h1>
<strong>Short description: </strong>
<br /><xsl:value-of disable-output-escaping="yes" select="SKUDescription"/><br />
<strong>Price: <xsl:value-of disable-output-escaping="yes" select="SKUPrice"/></strong>
</div>
<br /><br />
</xsl:template>
</xsl:stylesheet>