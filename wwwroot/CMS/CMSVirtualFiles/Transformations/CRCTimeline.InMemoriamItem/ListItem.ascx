﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CMSApp.CRC" %>
<div class="columns small-12 medium-6 large-3">
    <div class="gift" data-equalizer-watch>
        <p class="tribute"><%# ResourceStringHelper.GetString("CRCTimeline.InMemoriam.GiftInTributeOf",defaultText:"A Gift In Tribute Of") %></p>
        <h2><%# Eval("HonoreeName") %></h2>
        <span class="divider"></span>
        <p class="amount"><%# Eval("GiftAmount","{0:C0}") %></p>
        <p><%# Eval("Message") %></p>
        <p class="submitted-by"><%# Eval("DonorName") %>, <%# FormatDateTime(Eval("GiftDate"), CMSApp.CRC.TransformationHelper.DateFormat) %></p>
    </div>
</div>
