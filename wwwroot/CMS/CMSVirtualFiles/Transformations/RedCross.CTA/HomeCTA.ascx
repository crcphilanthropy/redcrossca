﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="span4">
    <a href="<%# Eval("LinkTo") %>"><h4><%# Eval("Title") %></h4></a>    
    <a href="<%# Eval("LinkTo") %>"><%# GetImageByUrl(Eval("Image"),300,180) %></a>

    <p><%# Eval("Content") %></p>
    <a class="readMore" href="<%# Eval("LinkTo") %>"><%# Eval("LinkText") %></a>
    
</div>

