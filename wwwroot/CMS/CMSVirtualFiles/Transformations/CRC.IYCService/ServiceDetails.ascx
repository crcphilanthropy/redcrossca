﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CMSApp.CRC" %>
<%@ Import Namespace="CMSAppAppCode.Old_App_Code.CRC" %>
<!-- Map Start -->

<div class="gmaps">
        <a href="https://maps.google.com/maps?daddr=<%# CurrentService.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")) %>,<%# CurrentService.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")) %>" target="_blank">
        <img src="<%# string.Format(Variables.StaticGoogleMap, CurrentService.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentService.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),650,411) %>"
            srcset='<%# string.Format(Variables.StaticGoogleMap, CurrentService.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentService.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),650,411) %> 1024w, <%# string.Format(Variables.StaticGoogleMap, CurrentService.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentService.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),550,275) %> 640w, <%# string.Format(Variables.StaticGoogleMap, CurrentService.Latitude.ToString(CultureInfo.GetCultureInfo("en-CA")),CurrentService.Longitude.ToString(CultureInfo.GetCultureInfo("en-CA")),250,150) %> 320w'
            sizes="(min-width: 960px) 100vw"
            alt="<%# CurrentService.Address %>">
    </a>
    <div class="desc clearfix">
        <div class="column large-6 medium-6 small-12">
            <p>
                <strong><%# CurrentService.Branch.Name %></strong>
                <br />

                 <%# TransformationHelper.ReplaceNewLine(CurrentService.Address) %><br />
                <%# TransformationHelper.AddClickToCall(TransformationHelper.ReplaceNewLine(CurrentService.Contact)) %>
            </p>
        </div>
        <div class="column large-6 medium-6 small-12">
            <p>
                <strong><%# ResourceStringHelper.GetString("CRC.IYC.HoursOfOperation", defaultText:"Hours of Operation") %> </strong>
                <br />
                <%# TransformationHelper.ReplaceNewLine(CurrentService.HoursOfOperation) %>
            </p>
        </div>
    </div>
</div>

<p>
    <%# CurrentService.Description %>
</p>

<asp:Repeater runat="server" ID="rptBranchServices">
    <HeaderTemplate>
        <div class="panel-wrap">
            <h3 class="h2"><%# ResourceStringHelper.GetString("CRC.IYC.OtherServicesOfferedAtBranch", defaultText:"Other Services Offered at this Branch") %></h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:PlaceHolder runat="server" Visible='<%# CurrentService.DocumentGuid.Equals(Eval("DocumentGuid")) %>'>
            <li>
                <%# Eval("Name") %>
            </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" Visible='<%# !CurrentService.DocumentGuid.Equals(Eval("DocumentGuid")) %>'>
            <li>
                <a href='<%# Eval("DocumentLiveUrl") %>'><%# Eval("Name") %> </a>
            </li>
        </asp:PlaceHolder>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </div>
    </FooterTemplate>
</asp:Repeater>
