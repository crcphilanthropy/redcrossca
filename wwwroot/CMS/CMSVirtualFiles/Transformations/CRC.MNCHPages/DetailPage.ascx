﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="row">
  <div class="columns large-12">    
    <h1><%# Eval("Header")%></h1>
  </div>
</div>
  <div class="row">
    <div class="large-12 column">
      <div class="banner">
        <img src="<%# Eval("BannerImage")%>" alt="<%# Eval("BannerCaption")%>" />
        <span class="caption column"><%# Eval("BannerCaption")%></span>
      </div>
    </div>               
    <div class="large-4 columns">
      <div class="panel-wrap">
        <%# Eval("SideBarContent")%>
        <a href="<%# Eval("SideBarCalloutLink")%>" target="<%# (ValidationHelper.GetString(Eval("SideBarLinkNewWindow"),"") != "False") ? "_blank" : "_self" %>" class="callout-link"><%# Eval("SideBarCalloutLinkText")%></a>
      </div>
    </div>
    <div class="large-8 columns">
      <%# Eval("BodyContent")%>             
      <a href="<%# Eval("BodyCalloutLink")%>" target="<%# (ValidationHelper.GetString(Eval("BodyCalloutLinkNewWindow"),"") != "False") ? "_blank" : "_self" %>" class="callout-link"><%# Eval("BodyCalloutLinkText")%></a>
    </div>
  </div>
</div>
      