﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Banner Image:</td>
    <td><%# Eval("BannerImage") %></td>
  </tr>
  <tr>
    <td>Banner Caption:</td>
    <td><%# Eval("BannerCaption") %></td>
  </tr>
  <tr>
    <td>Side Column:</td>
    <td><%# Eval("SideColumn") %></td>
  </tr>
  <tr>
    <td>Body Text:</td>
    <td><%# Eval("BodyText") %></td>
  </tr>
</table>
