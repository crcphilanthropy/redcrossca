﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Created when:</td>
    <td><%# Eval("ItemCreatedWhen") %></td>
  </tr>
  <tr>
    <td>GUID:</td>
    <td><%# Eval("ItemGUID") %></td>
  </tr>
  <tr>
    <td>Source Id:</td>
    <td><%# Eval("SourceId") %></td>
  </tr>
  <tr>
    <td>Target Id:</td>
    <td><%# Eval("TargetId") %></td>
  </tr>
  <tr>
    <td>Target Url:</td>
    <td><%# Eval("TargetUrl") %></td>
  </tr>
  <tr>
    <td>Lang Code:</td>
    <td><%# Eval("LangCode") %></td>
  </tr>
</table>
<cc1:CMSEditModeButtonEditDelete runat="server" id="btnEditDeleteAutoInsert" Path='<%# Eval("NodeAliasPath") %>' AddedAutomatically="True" EnableByParent="True"   />