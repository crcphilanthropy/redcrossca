﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><h4><%# Eval("Title") %></h4>
<p class="posted">Posted <%# FormatDateTime(Eval("DocumentCreatedWhen"),"dd-MMM-yyyy") %></p>

<dl class="posting">
<dt>Position:</dt>
<dd><%# Eval("Title") %></dd>
<dt>Region:</dt>
<dd><%# Eval("Location") %></dd>
<dt>Type:</dt>
<dd><%# Eval("Type") %></dd>
</dl>

<%# Eval("KeyInformation") %>
<%# Eval("Description") %>
<%# Eval("Contact") %>