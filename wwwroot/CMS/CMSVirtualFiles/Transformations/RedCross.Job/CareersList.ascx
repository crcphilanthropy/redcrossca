﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><tr>
    <td width="200"><a href="<%# Eval("NodeAliasPath") %>"><%# Eval("Title") %></a></td>
    <td width="100"><%# Eval("Type") %></td>
    <td width="250"><%# Eval("Location") %></td>
    <td width="200"><%# FormatDateTime(Eval("DocumentCreatedWhen"),"dd-MMM-yyyy") %></td>
</tr>
