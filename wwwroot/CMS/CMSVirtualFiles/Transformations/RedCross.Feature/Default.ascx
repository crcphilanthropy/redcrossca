﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Ttile") %></td>
  </tr>
  <tr>
    <td>Subtitle:</td>
    <td><%# Eval("Subtitle") %></td>
  </tr>
  <tr>
    <td>Summary:</td>
    <td><%# Eval("Summary") %></td>
  </tr>
  <tr>
    <td>Button Text:</td>
    <td><%# Eval("LinkText") %></td>
  </tr>
  <tr>
    <td>Link To:</td>
    <td><%# Eval("LinkTo") %></td>
  </tr>
  <tr>
    <td>Image:</td>
    <td><%# Eval("Image") %></td>
  </tr>
</table>
