﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="banner" role="banner">
	<%# GetImageByUrl(Eval("Image"),960,297) %><br />
	<div class="caption">
		<h1><%# Eval("Title") %></h1>
		<p><%# Eval("Summary") %></p>
                <asp:Placeholder runat="server" Visible='<%# !String.IsNullOrEmpty(Eval("LinkTo").ToString()) %>'>
                    <a class="readMore" href="<%# Eval("LinkTo") %>"><%# Eval("LinkText") %></a>
                </asp:Placeholder>
	</div>
</div>
