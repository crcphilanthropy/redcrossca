﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='text-CTA'>
    <h3 class="h2"><%# Eval("Title") %></h3>
    
    <p>
    <%# Eval("Description") %>
    </p>
    <p> 
    <a class="button small cp-track" href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %> ><%# ( ValidationHelper.GetString(Eval("LinkedText"),"") != "") ? Eval("LinkedText") : "Submit" %></a>
    </p>
</div>


