﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>
<div class="large-6 medium-6 small-12 column">
    <div class="large-10 medium-10 small-12">
        <p class="h2"><%# Eval("Title") %></p>
        <p><%# Eval("Description") %></p>
    </div>
</div>
<div class="large-6 medium-6 small-12 column">
    <fieldset>
        <label>* Email</label>
        <div class="clearfix">
            <input type="text" placeholder='<%# ResHelper.GetString("CRC.EnterYourEmailHere") %>' runat="server" id="SignUpEmailCTATextBox" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="PromoEmailSignUpCTA" class="trim" />
            <div class="error-group">
                <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailCTATextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="PromoEmailSignUpCTA" CssClass="text-error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailCTATextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="PromoEmailSignUpCTA" CssClass="text-error" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
            </div>
            <asp:Button ID="btnSubscribe" runat="server" CssClass="button lyrisForm"  data-event-action="signup" data-validation-group="PromoEmailSignUpCTA"  data-event-label="signup" ValidationGroup="PromoEmailSignUpCTA" OnClick="BtnSignUpServerClick" Text='<%# Eval("LinkedText") %>'></asp:Button>
        </div>
    </fieldset>
</div>

<asp:HiddenField runat="server" ID="hidSubscriptionEmailURL" Value='<%# EvalText("URL") %>' />
