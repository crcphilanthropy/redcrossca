﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAppAppCode.Old_App_Code.CRC.WebControls.BaseTransformation" %>
<asp:Panel runat="server" CssClass="subscribe bg" DefaultButton="btnSubscribe">
    <div class="row">
        <div class="columns medium-6 ">
            <h3><%# Eval("Title") %></h3>
            <p><%# Eval("Description") %></p>
        </div>
        <div class="columns medium-6">
            <input type="text" placeholder='<%# Eval("SubTitle") %>' runat="server" id="SignUpEmailCTATextBox" clientidmode="Static" data-input-type="email" data-change-type="true" validationgroup="EmailSignUpCTA" class="trim" />
            <div class="error-group">
                <asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" ControlToValidate="SignUpEmailCTATextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUpCTA" CssClass="text-error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regExEmailAddress" ControlToValidate="SignUpEmailCTATextBox" ErrorMessage='<%# ResHelper.GetString("CRC.Error.Email") %>' ValidationGroup="EmailSignUpCTA" CssClass="text-error" Display="Dynamic" ValidationExpression='<%# ValidationHelper.EmailRegExp.ToString() %>'></asp:RegularExpressionValidator>
            </div>
            <asp:HiddenField runat="server" ID="hidSubscriptionEmailURL" Value='<%# EvalText("URL") %>'/>
            <asp:LinkButton ID="btnSubscribe" runat="server" CssClass="button lyrisForm" ValidationGroup="EmailSignUpCTA" OnClick="BtnSignUpServerClick"  data-event-action="signup" data-validation-group="EmailSignUpCTA" data-event-label="mobile footer banner" ><%# Eval("LinkedText") %></asp:LinkButton>
        </div>
    </div>
</asp:Panel>