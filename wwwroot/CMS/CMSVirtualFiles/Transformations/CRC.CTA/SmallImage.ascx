﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article>
    <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# IfEmpty(Eval("Thumbnail"), "", GetImage("Thumbnail", 69, 69, 50, ValidationHelper.GetString(Eval("Title"),"")) )%></a>
        <%# IfEmpty(Eval("Thumbnail"), "",string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), 69),EvalText("ImageAltText")) )%></a>
     <h4><a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("Title") %></a></h4>
    <p>
        <%# Eval("Description") %>
    </p>
</article>

