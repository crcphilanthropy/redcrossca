﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%# DisplayIndex == 0 ? "<div class=\"medium-cta clearfix font-size\">"  : "" %>
<%# DisplayIndex == 4 ? "</div><div class=\"small-cta clearfix font-size\">"  : "" %>
<article>
<%# string.Format(DisplayIndex < 4 ? "<h2>{0}</h2>"  : "<h4>{0}</h4>", Eval("Title").ToString()) %>
<a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label=""  <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
    <%# DisplayIndex < 4 ? IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), 164),EvalText("ImageAltText")) ): IfEmpty(Eval("Thumbnail"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Thumbnail"), 50),EvalText("ThumbnailAltText")))%></a>
<p>
    <%# Eval("Description") %>
    <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url"))%>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label=""  <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("Title").ToString() %></a>
</p>
</article>
<%# DataItemIndex == DataRowView.DataView.Count - 1 ? "</div>" : "" %>