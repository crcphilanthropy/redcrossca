﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="large-8 medium-7 small-12 column">
  <p class="h2"><%# Eval("Title") %></p>
  <p><%# Eval("Description") %></p>
</div>
<div class="large-3 medium-4 small-12 right">
  <a class="button cp-track" href="<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>" 
  data-event="generic-event" data-category="Top Promo Banner" data-action='<%# Eval("Title") %>' data-label='<%# Eval("LinkedText") %>'
  <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("LinkedText") %></a>
</div>
