﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="medium-6 columns">
  <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' x-ga-event-category="CTAs" x-ga-event-action="<%# Eval("Title") %>" class="cta" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>> 
    <div class="panel" data-equalizer-watch>
      <%# IfEmpty(Eval("Image"), "", GetImage("Image", 245, 146, 245, ValidationHelper.GetString(Eval("Title"),"")) )%>
      <h3><%# Eval("Title") %></h3>
      <p><%# Eval("Description") %></p>
      <p><span><%# Eval("LinkedText") %> <i class="fa fa-chevron-right"></i></span></p>
    </div>
  </a>
</div>