﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="testimonial-CTA">
  <div class="job-quote">
    
      <div class="quotes">
      <%# IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), -1),EvalText("ImageAltText"))) %>
          <%# Eval("Description") %>
    
    <div class="quote-title">
      <%# Eval("Title") %>
    </div>
    </div>

  </div>
</div>