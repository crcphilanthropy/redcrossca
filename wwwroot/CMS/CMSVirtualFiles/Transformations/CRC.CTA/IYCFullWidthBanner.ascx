﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="banner bottom">
    <%# IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), -1),EvalText("ImageAltText")) )%>
    <div class="clearfix <%# Eval("BackgroundClass") %>">
        <div class="column large-8 medium-8 small-12 content">
            <%# Eval("Description") %>
        </div>
        <div class="column large-4 medium-4 small-12">
            <asp:PlaceHolder ID="bannerbutton" runat="server" Visible='<%# !String.IsNullOrWhiteSpace(EvalText("LinkedText")) %>'>
            <a class="button cp-track" href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %> ' data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
                <span><%# Eval("LinkedText") %></span>
            </a>
            </asp:PlaceHolder>
        </div>
    </div>
</div>
