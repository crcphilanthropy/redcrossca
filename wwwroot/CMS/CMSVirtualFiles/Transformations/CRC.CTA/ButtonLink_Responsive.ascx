﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='columns medium-6 large-12<%# DisplayIndex % 3 == 0 ? " first" : ""%><%# DisplayIndex + 1 % 3 == 0 ? " last" : "" %>' >
  <div class="cta">
    <h3><%# Eval("Title") %></h3>
    
    <p>
      <%# Eval("Description") %>
    </p>
    <p> 
      <a class="button small" href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' x-ga-event-category="CTAs" x-ga-event-action="<%# Eval("Title") %>" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %> > <span> <%# ( ValidationHelper.GetString(Eval("LinkedText"),"") != "") ? Eval("LinkedText") : "Submit" %> </span> </a
    </p>
       
  </div>
</div>