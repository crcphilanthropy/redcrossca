﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='standard-column font-size<%# DisplayIndex % 3 == 0 ? " first" : ""%><%# DisplayIndex + 1 % 3 == 0 ? " last" : "" %>'>
    <h2><a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("Title") %></a></h2>
    <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
        <%# IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), 310),EvalText("ImageAltText")) ) %></a>
    <p>
        <%# Eval("Description") %>
        <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("LinkedText") %></a>
    </p>
</div>
