﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="medium-6 small-12 columns">
    
    <div class="border-top rich-text">
        <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url"))%>' class="cp-track sm-cta" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><h3><%# Eval("Title").ToString() %></h3>
        
        
            <%# IfEmpty(Eval("Thumbnail"), "", string.Format("<img class='hide-for-medium-down left' src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Thumbnail"), 69),EvalText("ThumbnailAltText")))%>
            </a>
        
            <div class="">
               <%# Eval("Description") %> 
                <p>
                    <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url"))%>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
                        <span><%# Eval("LinkedText").ToString() %> <i class="fa fa-chevron-right"></i></span>

                    </a>
                </p>

            </div>

       
    </div>
   
</div>