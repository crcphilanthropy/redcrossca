﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><li>
  <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cta cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
    <div class="panel" data-equalizer-watch>
      <%# IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), -1),EvalText("ImageAltText")) )%>
      <h3><%# Eval("Title") %></h3>
      <p><%# Eval("Description") %></p>
      <p><span><%# Eval("LinkedText") %> <i class="fa fa-chevron-right"></i></span></p>
    </div>
  </a>
</li>