﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div>
    <%# IfEmpty(Eval("Image"), "","<a href=\""+CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url"))+"\" data-event=\"generic-event\" data-category=\"CTAs\" data-action=\""+ Eval("Title") +"\">"+ string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), 146),EvalText("ImageAltText"))+"</a>" )%>
    <h2><%# Eval("Title") %></h2>
    <p><%# Eval("Description") %></p>
    <a href="<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>" class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("LinkedText") %></a>
</div>
