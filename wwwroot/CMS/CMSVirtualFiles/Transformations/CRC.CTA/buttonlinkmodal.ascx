﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class='side-cta modal-cta '>
    <h5 class="modal-external-title"><%# Eval("Title") %></h5>
    <h4 class="modal-title hidden"><%# CMSApp.CRC.TransformationHelper.getDocumentNameByAliasPath(ValidationHelper.GetString(Eval("Url"),""))%></h4>
    <p>
    <%# Eval("Description") %>
    </p>
    <p> 
    <a data-toggle="modal" data-target="#myModal" class="submit-btn no-margin modal-btn" href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>'><%# ( ValidationHelper.GetString(Eval("LinkedText"),"") != "") ? Eval("LinkedText") : "Submit" %></a>
    </p>
</div>