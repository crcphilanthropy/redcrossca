﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><article>
    <div class="columns medium-5">
      <a class="ctaImage cp-class" href="<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
          <%# IfEmpty(Eval("Image"), "", string.Format("<img src='{0}' alt='{1}' />",CMSApp.CRC.TransformationHelper.GetResizedImage(EvalText("Image"), -1),EvalText("ImageAltText"))) %></a>
    </div>
    <div class="columns medium-7 desc">
      <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>>
        <h3><%# Eval("Title") %></h3>
      </a>

      <div>
        <p>
        <%# Eval("Description") %>
        </p>
          <br />
        <a href='<%# CMSApp.CRC.TransformationHelper.getCultureDocumentUrl(Eval("Url")) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Title") %>' data-label="" <%# CMSApp.CRC.TransformationHelper.targetBlankHTML(Eval("targetBlank")) %>><%# Eval("LinkedText") %> <i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
  </article>

