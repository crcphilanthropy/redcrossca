﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMSApp.CRC" %>
<div class="row event">
    <div class="columns small-12 medium-4 large-4 hide-for-small-only">
        <img src="<%#Eval("Thumbnail") %>" alt="<%# Eval("ThumbnailAltText") %>" />
    </div>
    <div class="columns small-12 medium-8 large-8">
        <h3><%#EvalText("Title") %></h3>
        <span class="date"><%# FormatDateTime(Eval("Date"),CMSApp.CRC.TransformationHelper.DateFormat) %></span>
        <p><%#Eval("Description") %></p>
        <asp:PlaceHolder runat="server" Visible='<%# !String.IsNullOrWhiteSpace(EvalText("TargetUrl")) %>'>
            <a href="<%# EvalText("TargetUrl") %>" target="<%# EvalText("TargetUrlWindow") %>" class="more"><%# ResourceStringHelper.GetString("CRC.Timeline.FindOutMore", defaultText:"Find out more") %></a>
        </asp:PlaceHolder>
    </div>
</div>
