﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="stl-quote">
    <img src="<%# Eval("Image")%>" alt="<%# Eval("ImageAlt")%>" />
    <div class="quote-text <%# Eval("QuoteLocation")%>">
        <p><%# Eval("Quote")%>
            <br /><span><%# Eval("Author")%></span>
        </p>
    </div>
</div>
