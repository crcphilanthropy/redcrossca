﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="slide <%# Eval("TextLocation") %> <%# Eval("TextBackground") %>">
    <a class="cp-track" data-event="generic-event" data-category="carousel" data-action="cta click" data-label="<%# Eval("NodeAlias") %><%# DisplayIndex+1 %>" href='<%# Eval("Url") %>'>
        <img src="<%# CMSApp.CRC.TransformationHelper.GetResizedImage(Eval("Image") as string,640) %>"
            srcset="<%# Eval("Image") %> 1024w, <%# CMSApp.CRC.TransformationHelper.GetResizedImage(Eval("Image") as string,950) %> 950w, <%# CMSApp.CRC.TransformationHelper.GetResizedImage(Eval("Image") as string,640) %> 640w"
            alt="<%# EvalText("ImageAltText") %>">
        <div class="row">
            <div class="columns">
                <div class="caption">
                    <h1><%# Eval("Content") %></h1>

                    <span class="button"><%# Eval("LinkText") %> </span>

                </div>
            </div>
        </div>
    </a>
</div>
