﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Flickr Collection Url:</td>
    <td><%# Eval("Url") %></td>
  </tr>
  <tr>
    <td>Collection Name:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Thumbnail (157 x 94):</td>
    <td><%# Eval("Thumbnail") %></td>
  </tr>
</table>
