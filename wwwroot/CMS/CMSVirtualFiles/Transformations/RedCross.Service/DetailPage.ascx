﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><h4><%# Eval("Title") %></h4>

<img src="<%# Eval("MainImage") %>" alt="<%# Eval("Title") %>"/>

<%# Eval("Description") %>

<!--
<div class="content-box access">
	<img src="/NewCo/images/telephone.png" width="67" height="53" alt="Telephone"/>
	<h6>How to Access Services</h6>
	<%# Eval("AccessService") %>
</div>
-->