﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><li>
<span class="title-container">
<a href="<%# Eval("NodeAliasPath") %>"><h4><%# Eval("Title") %></h4></a></span>
<a href="<%# Eval("NodeAliasPath") %>"><img src="<%# Eval("SummaryThumbnail") %>" width="260" height="160" /></a>
<p><%# Eval("SummaryText") %></p>
<a class="readMore" href="<%# Eval("NodeAliasPath") %>">Read More &raquo;</a>
</li>

