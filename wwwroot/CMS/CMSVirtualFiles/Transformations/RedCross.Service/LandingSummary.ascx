﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><li>
	<span class="title-container"><a href="<%# GetDocumentUrl() %>"><h4><%# Eval("Title") %></h4></a></span>
	<a href="<%# GetDocumentUrl() %>"><img src="<%# Eval("SummaryThumbnail") %>" width="260" height="160" alt="<%# Eval("Title") %>"/></a>
	<p><%# Eval("SummaryText") %></p>
	<a class="readMore" href="<%# GetDocumentUrl() %>">Read More &raquo;</a>
</li>