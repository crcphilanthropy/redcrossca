﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Ttile") %></td>
  </tr>
  <tr>
    <td>Summary Thumbnail:</td>
    <td><%# Eval("SummaryThumbnail") %></td>
  </tr>
  <tr>
    <td>Summary Text:</td>
    <td><%# Eval("SummaryText") %></td>
  </tr>
  <tr>
    <td>Main Image:</td>
    <td><%# Eval("MainImage") %></td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><%# Eval("Description") %></td>
  </tr>
  <tr>
    <td>How to access service:</td>
    <td><%# Eval("AccessService") %></td>
  </tr>
</table>
