﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>GUID:</td>
    <td><%# Eval("GUID") %></td>
  </tr>
  <tr>
    <td>Code Name:</td>
    <td><%# Eval("CodeName") %></td>
  </tr>
  <tr>
    <td>Display Name:</td>
    <td><%# Eval("DisplayName") %></td>
  </tr>
</table>
