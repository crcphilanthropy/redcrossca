﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Hide ROS Banner:</td>
    <td><cms:CMSCheckBox ID="CheckBox1" Runat="server" Checked='<%# Eval("HideRosBanner") %>' Text="Hide ROS Banner" /></td>
  </tr>
  <tr>
    <td>Facebook Title:</td>
    <td><%# Eval("OGTitle") %></td>
  </tr>
  <tr>
    <td>Facebook Description:</td>
    <td><%# Eval("OGDescription") %></td>
  </tr>
  <tr>
    <td>Facebook Image <br />(min. size 600px x 315px):</td>
    <td><%# Eval("OGImage") %></td>
  </tr>
  <tr>
    <td>Document name:</td>
    <td><%# Eval("Name") %></td>
  </tr>
  <tr>
    <td>Summary:</td>
    <td><%# Eval("Summary") %></td>
  </tr>
  <tr>
    <td>Thumbnail Image (146 x 88):</td>
    <td><%# Eval("Thumbnail") %></td>
  </tr>
  <tr>
    <td>Thumbnail Alt Text:</td>
    <td><%# Eval("ThumbnailAltText") %></td>
  </tr>
  <tr>
    <td>Menu Group:</td>
    <td><%# Eval("Group") %></td>
  </tr>
</table>
