﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Location Postcode:</td>
    <td><%# Eval("LocationPostcode") %></td>
  </tr>
  <tr>
    <td>Latitude:</td>
    <td><%# Eval("Latitude") %></td>
  </tr>
  <tr>
    <td>Longitude:</td>
    <td><%# Eval("Longitude") %></td>
  </tr>
  <tr>
    <td>Catchment:</td>
    <td><%# Eval("Catchment") %></td>
  </tr>
  <tr>
    <td>Key information:</td>
    <td><%# Eval("KeyInformation") %></td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><%# Eval("Description") %></td>
  </tr>
  <tr>
    <td>Contact:</td>
    <td><%# Eval("Contact") %></td>
  </tr>
  <tr>
    <td>Application deadline:</td>
    <td><%# Eval("ApplicationDeadline") %></td>
  </tr>
  <tr>
    <td>Is volunteer position:</td>
    <td><%# Eval("IsVolunteer") %></td>
  </tr>
</table>
