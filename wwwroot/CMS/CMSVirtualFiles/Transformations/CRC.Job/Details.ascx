﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><p class="posted"><strong><%# ResHelper.GetString("CRC.Posted") %>:</strong> <%# FormatDateTime(Eval("DocumentCreatedWhen"),CMSApp.CRC.TransformationHelper.DateFormat) %></p>

<p><strong><%# ResHelper.GetString("CRC.Region") %>:</strong> <%# Eval("Location") %></p>
<asp:placeholder runat="server" visible='<%# !string.IsNullOrWhiteSpace(EvalText("ApplicationDeadline")) %>'>
  <p><strong><%# ResHelper.GetString("CRC.JobApplicationDeadline") %>:</strong> <%# FormatDateTime(Eval("ApplicationDeadline"),CMSApp.CRC.TransformationHelper.DateFormat) %></p>
</asp:placeholder>
<div class="job-content text">
<%# Eval("KeyInformation") %>
</div>
<div class="job-content text">
<%# Eval("Description") %>
</div>
<div class="job-content text">
<%# Eval("Contact") %>
</div>