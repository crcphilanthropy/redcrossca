﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMSApp.CRC" %>

<tr>
    <td data-content="<%# ResourceStringHelper.GetString("CRC.JobTitle") %>"><a href="<%# GetDocumentUrl() %>"><%# Eval("Title") %></a></td>
    <asp:PlaceHolder runat="server" ID="phType" Visible="False">
        <td  data-content="<%# ResourceStringHelper.GetString("CRC.Type") %>"><%# Eval("CareerTypeName") %></td>
    </asp:PlaceHolder>
    <td data-content="<%# ResourceStringHelper.GetString("CRC.DatePosted") %>"><%# FormatDateTime(Eval("DocumentCreatedWhen"), CMSApp.CRC.TransformationHelper.DateFormat) %></td>
    <td data-content="<%# ResourceStringHelper.GetString("CRC.Location") %>"><%# Eval("Location") %></td>
    <td data-content="<%# ResourceStringHelper.GetString("CRC.ApplicationDeadline") %>"><%# FormatDateTime(Eval("ApplicationDeadline"), CMSApp.CRC.TransformationHelper.DateFormat) %></td>
</tr>

