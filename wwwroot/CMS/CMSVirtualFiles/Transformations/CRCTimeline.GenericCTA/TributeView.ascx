﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><div class="panel grey-cta tribute">
    <h2><%# Eval("Title") %></h2>
    <p><%# Eval("Description") %></p>
    <a class="red-btn btn" href="<%# Eval("DestinationUrl") %>" target="<%# Eval("URLTarget") %>"><%# Eval("ButtonText") %></a>
    <%# Eval("AdditionalContent") %>
</div>
