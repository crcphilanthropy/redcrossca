﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><table>
  <tr>
    <td>Title:</td>
    <td><%# Eval("Title") %></td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><%# Eval("Description") %></td>
  </tr>
  <tr>
    <td>Button Text:</td>
    <td><%# Eval("ButtonText") %></td>
  </tr>
  <tr>
    <td>Destination URL:</td>
    <td><%# Eval("DestinationURL") %></td>
  </tr>
  <tr>
    <td>Open Link in New Window?:</td>
    <td><cms:CMSCheckBox ID="CheckBox5" Runat="server" Checked='<%# Eval("URLTarget") %>' Text="Open Link in New Window?" /></td>
  </tr>
</table>
