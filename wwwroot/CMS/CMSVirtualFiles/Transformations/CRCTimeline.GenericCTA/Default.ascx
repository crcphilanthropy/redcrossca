﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><%@ Import Namespace="CMSApp.CRC" %>
<div class="small-12 medium-6 large-4 columns">
    <div class="panel" data-equalizer-watch>
        <img src="<%# Eval("Image") %>" alt="<%# EvalText("ImageAltText") %>">
        <div class="cta-content">
            <div class="desc">
                <h2><%# EvalText("Title") %></h2>
                <p><%# EvalText("Description") %></p>
            </div>
        </div>
        <a class="red-btn btn" href="<%# Eval("DestinationUrl") %>" target="<%# Eval("URLTarget") %>"><%# Eval("ButtonText") %></a>
    </div>
</div>
