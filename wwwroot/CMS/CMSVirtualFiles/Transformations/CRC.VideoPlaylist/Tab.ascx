﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMS.Controls.CMSTransformation" %><%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<%@ Register TagPrefix="cc1" Namespace="CMS.Controls" Assembly="CMS.Controls" %><li class='<%# DisplayIndex == 0 ? "active" : "" %>'>
    <a href="javascript:void(0);" 
       data-playlist='/CRC/video-playlist/<%# EvalText("PlaylistID") %>.xml'
       data-description='<%# Eval("Description", true) %>' class="cp-track" data-event="generic-event" data-category="CTAs" data-action='<%# Eval("Tab") %>' data-label="" >
           <%# Eval("Tab") %>
    </a>
</li>