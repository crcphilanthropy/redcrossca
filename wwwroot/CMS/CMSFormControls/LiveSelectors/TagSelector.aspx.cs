using System;
using System.Collections;
using System.Data;
using System.Linq;

using CMS.Helpers;
using CMS.Base;
using CMS.UIControls;
using CMS.Taxonomy;

public partial class CMSFormControls_LiveSelectors_TagSelector : CMSLiveModalPage
{
    #region "Variables"

    private int groupId;
    private string textBoxId;
    private string oldTags;
    private Hashtable selectedTags;
    private Hashtable mDialogProperties;
    private string mDialogIdentifier;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets dialog identifier.
    /// </summary>
    private string DialogIdentifier
    {
        get
        {
            return mDialogIdentifier ?? (mDialogIdentifier = QueryHelper.GetString("params", null));
        }
    }


    /// <summary>
    /// Dialog properties
    /// </summary>
    private Hashtable DialogProperties
    {
        get
        {
            if (mDialogProperties == null)
            {
                var identifier = QueryHelper.GetString("params", null);
                if (!String.IsNullOrEmpty(identifier))
                {
                    mDialogProperties = WindowHelper.GetItem(identifier) as Hashtable;
                }
            }

            return mDialogProperties;
        }
    }

    #endregion


    #region "Page Events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register jQuery
        ScriptHelper.RegisterJQuery(Page);

        PrepareParameters();

        // Setup UniGrid
        gridElem.ZeroRowsText = GetString("tags.tagselector.noold");
        gridElem.GridView.ShowHeader = false;
        gridElem.OnBeforeDataReload += gridElem_OnBeforeDataReload;
        gridElem.OnAfterDataReload += gridElem_OnAfterDataReload;
        gridElem.OnExternalDataBound += gridElem_OnExternalDataBound;

        // Page title
        PageTitle.TitleText = GetString("tags.tagselector.title");

        btnOk.Click += btnOk_Click;
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        string retval = "";

        // Append selected tags which are already in DB
        var items = gridElem.SelectedItems;
        if (items.Count > 0)
        {
            items.Sort();

            foreach (string tagName in items)
            {
                if (tagName.Contains(" "))
                {
                    retval = (retval + ", \"" + tagName.Trim('"') + "\"");
                }
                else
                {
                    retval = (retval + ", " + tagName);
                }
            }
        }

        // Remove
        if (retval != "")
        {
            retval = retval.Substring(2);
        }

        // Update parameters
        UpdateParameters(retval);

        ltlScript.Text = ScriptHelper.GetScript("wopener.setTagsToTextBox(" + ScriptHelper.GetString(textBoxId) + ", " + ScriptHelper.GetString(retval) + "); CloseDialog();");
    }

    #endregion


    #region "UniGrid Events"

    protected void gridElem_OnBeforeDataReload()
    {
        // Filter records by tag group ID
        string where = "(TagGroupID = " + groupId + ")";
        if (!String.IsNullOrEmpty(gridElem.CompleteWhereCondition))
        {
            where += " AND (" + gridElem.CompleteWhereCondition + ")";
        }
        gridElem.WhereCondition = where;
    }


    protected void gridElem_OnAfterDataReload()
    {
        if (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            // Fill list with tags and trim quotes and spaces
            var selection = (from string tag in selectedTags.Values select tag.Trim('"').Trim()).ToList();

            if (!URLHelper.IsPostback())
            {
                gridElem.SelectedItems = selection;
            }
        }
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        if (sourceName.ToLowerCSafe() == "tagname")
        {
            DataRowView drv = (DataRowView)parameter;
            string tagName = ValidationHelper.GetString(drv["TagName"], "");
            string tagId = ValidationHelper.GetString(drv["TagID"], "");
            if ((tagName != "") && (tagName != tagId))
            {
                string tagCount = ValidationHelper.GetString(drv["TagCount"], "");
                string tagText = HTMLHelper.HTMLEncode(tagName) + " (" + tagCount + ")";

                // Create link with onclick event which call onclick event of checkbox in the same row
                return "<a href=\"#\" onclick=\"var c=$cmsj(this).parents('tr:first').find('input:checkbox'); c.attr('checked', !c.attr('checked')).get(0).onclick(); return false;\">" + tagText + "</a>";
            }
        }
        return "";
    }

    #endregion


    #region "Private methods"

    private void UpdateParameters(string tags)
    {
        DialogProperties["tags"] = tags;
        WindowHelper.Add(DialogIdentifier, DialogProperties);
    }


    private void PrepareParameters()
    {
        var props = DialogProperties;
        if (props == null)
        {
            return;
        }

        // Get group ID
        groupId = props["group"].ToInteger(0);

        // Get id of the base selector textbox
        textBoxId = props["textbox"].ToString(String.Empty);

        // Get selected tags
        oldTags = props["tags"].ToString(String.Empty);
        selectedTags = TagHelper.GetTags(oldTags);
    }

    #endregion
}