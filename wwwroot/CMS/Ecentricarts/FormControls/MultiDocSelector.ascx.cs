﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CMS.FormControls;
using CMS.Helpers;
using CMS.SiteProvider;
using Ecentricarts.Controllers.MultiDocSelector;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.Localization;
using CMS.Taxonomy;
using LinqToExcel.Extensions;

namespace CMSApp.Ecentricarts.FormControls.MultiDocSelector
{
    public partial class MultiDocSelector : FormEngineUserControl
    {
        public const int UnlimitedSelection = 9999999;
        private readonly string[] _separators = { "|", ";", "," };

        public string SiteNames
        {
            get { return ValidationHelper.GetString(GetValue("SiteNames"), SiteContext.CurrentSiteName); }
            set { SetValue("SiteNames", value); }
        }

        public string Path
        {
            get
            {
                var path = ValidationHelper.GetString(GetValue("Path"), "/%");
                if (path.Equals("../%") && Form.EditedObject is CMS.DocumentEngine.TreeNode)
                {
                    var parentPath = ((CMS.DocumentEngine.TreeNode) Form.EditedObject)?.Parent?.NodeAliasPath;
                    path = $"{parentPath}/%";
                }else if (path.Equals("./%") && Form.EditedObject is CMS.DocumentEngine.TreeNode)
                {
                    var parentPath = ((CMS.DocumentEngine.TreeNode)Form.EditedObject)?.NodeAliasPath;
                    path = $"{parentPath}/%";
                }

                return path;
            }
            set
            {
                SetValue("Path", value);
            }
        }

        public string ClassNames
        {
            get { return ValidationHelper.GetString(GetValue("ClassNames"), string.Empty); }
            set { SetValue("ClassNames", value); }
        }

        public string FieldToSave
        {
            get { return ValidationHelper.GetString(GetValue("FieldToSave"), string.Empty); }
            set { SetValue("FieldToSave", value); }
        }

        public int MaxSize
        {
            get { return ValidationHelper.GetInteger(GetValue("MaxSize"), UnlimitedSelection); }
            set { SetValue("MaxSize", value); }
        }

        public IEnumerable<Guid> SelectedDocuments
        {
            get
            {
                return (Value as string ?? string.Empty).Split(_separators, StringSplitOptions.RemoveEmptyEntries).Select(item => ValidationHelper.GetGuid(item, Guid.Empty));
            }
        }

        public string Where
        {
            get { return ValidationHelper.GetString(GetValue("Where"), string.Empty); }
            set { SetValue("Where", value); }
        }

        public string AdditionalFields
        {
            get { return ValidationHelper.GetString(GetValue("AdditionalFields"), string.Empty); }
            set { SetValue("AdditionalFields", value); }
        }

        public int MaxRelativeLevel
        {
            get { return ValidationHelper.GetInteger(GetValue("MaxRelativeLevel"), -1); }
            set { SetValue("MaxRelativeLevel", value); }
        }

        public string QueryName
        {
            get { return ValidationHelper.GetString(GetValue("QueryName"), string.Empty); }
            set { SetValue("QueryName", value); }
        }

        public bool ShowAuthorFilter
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowAuthorFilter"), false); }
            set { SetValue("ShowAuthorFilter", value); }
        }

        public bool ShowCategoryFilter
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowCategoryFilter"), false); }
            set { SetValue("ShowCategoryFilter", value); }
        }

        public bool ShowPageTypeFilter
        {
            get { return ValidationHelper.GetBoolean(GetValue("ShowPageTypeFilter"), false); }
            set { SetValue("ShowPageTypeFilter", value); }
        }

        public bool IncludeAllCulture
        {
            get { return ValidationHelper.GetBoolean(GetValue("IncludeAllCulture"), false); }
            set { SetValue("IncludeAllCulture", value); }
        }

        public string Options
        {
            get
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(FilterOptions);
            }
        }

        private MultiDocController.MdFilter FilterOptions
        {
            get
            {
                return new MultiDocController.MdFilter
                {
                    Lang = IncludeAllCulture ? null : LocalizationContext.PreferredCultureCode,
                    ClassNames = ClassNames.Split(_separators, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim()).ToList(),
                    Path = Path,
                    SiteNames = SiteNames.Split(_separators, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim()).ToList(),
                    FieldToSave = FieldToSave,
                    Where = Where,
                    AdditionalFields = AdditionalFields.Split(_separators, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim()).ToList(),
                    MaxRelativeLevel = MaxRelativeLevel,
                    SelectedDocuments = SelectedDocuments.ToList(),
                    IncludeCategories = ShowCategoryFilter,
                    QueryName = QueryName
                };
            }
        }

        public override object Value { get { return hidSelectedDocuments.Text; } set { hidSelectedDocuments.Text = value as string; } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var t = new Thread(RegisterDocuments);
            t.Start(FilterOptions);

            BindSites();
            BindCultures();
            BindPageTypes();
            BindCategories();
            RegisterMultiDocScripts();

            pAuthour.Visible = ShowAuthorFilter;
            pCategories.Visible = ShowCategoryFilter;
            pPageTypes.Visible = ShowPageTypeFilter;
        }

        public override bool IsValid()
        {
            if (FieldInfo.AllowEmpty && !SelectedDocuments.Any()) return true;
            if (SelectedDocuments.Count() <= MaxSize) return base.IsValid();

            ValidationError = $"Maximum number of pages exceeded: {MaxSize}";
            return false;
        }

        private static void RegisterDocuments(object mdFilter)
        {
            var options = (MultiDocController.MdFilter)mdFilter;

            MultiDocController.GetDocuments(options);
        }
        private void BindCultures()
        {
            phCultures.Visible = IncludeAllCulture;
            if (!IncludeAllCulture) return;

            ddlCultures.DataSource = CultureSiteInfoProvider.GetSiteCultures(SiteContext.CurrentSiteName).Cast<CultureInfo>().OrderBy(item => item.CultureName);
            ddlCultures.DataTextField = "CultureName";
            ddlCultures.DataValueField = "CultureCode";
            ddlCultures.DataBind();
            ddlCultures.Items.Insert(0, new ListItem("All Culture...", string.Empty));
        }

        private void BindSites()
        {
            var sites = SiteInfoProvider.GetSites().AsQueryable();
            var siteNames = SiteNames.Split(_separators, StringSplitOptions.RemoveEmptyEntries)
                         .Select(item => item.Trim())
                         .ToList();

            if (siteNames.Any())
                sites = sites.Where(site => siteNames.Contains(site.SiteName));

            ddlSites.DataSource = sites.Select(site => new { site.SiteName, DisplayName = ResHelper.LocalizeString(site.DisplayName, LocalizationContext.PreferredCultureCode, false, true) });
            ddlSites.DataTextField = "DisplayName";
            ddlSites.DataValueField = "SiteName";
            ddlSites.DataBind();
            phSites.Visible = ddlSites.Items.Count > 1;
        }

        private void BindCategories()
        {
            var categories = CategoryInfoProvider.GetCategories().Select(item => ResHelper.LocalizeString(item.CategoryDisplayName, null, false, true)).OrderBy(item => item);
            ddlCategories.DataSource = categories;
            ddlCategories.DataBind();
        }

        private void BindPageTypes()
        {
            var classNames = ClassNames.Split(_separators, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim());

            var ds = DataClassInfoProvider.GetClasses().Where(string.Format("ClassName in ({0})", string.Join(",", classNames.Select(className => string.Format("'{0}'", className))))).Execute();
            ddlPageTypes.DataSource = ds;
            ddlPageTypes.DataTextField = "ClassDisplayName";
            ddlPageTypes.DataValueField = "ClassName";
            ddlPageTypes.DataBind();
            if (Page.Form.Attributes["data-ng-app"] == null)
                Page.Form.Attributes.Add("data-ng-app", "multiDocSelectorApp");
        }

        private void RegisterMultiDocScripts()
        {
            var sm = ScriptManager.GetCurrent(Page);
            RegisterStartUpScript(sm, "<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js'></script>", "angular.min.js", false);
            RegisterStartUpScript(sm, "<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.min.js'></script>", "angular-animate.min.js", false);
            RegisterStartUpScript(sm, "<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.min.js'></script>", "angular-touch.min.js", false);
            RegisterStartUpScript(sm, "<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js'></script>", "angular-sanitize.js", false);
            RegisterStartUpScript(sm, "<script src='https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.2.5.min.js'></script>", "ui-bootstrap-tpls-1.2.5.min.js", false);
            RegisterStartUpScript(sm, "<script src='/Ecentricarts/Scripts/src/eca/angular/multidoc/multi-doc-controller.js'></script>", "multi-doc-controller.js", false);
        }

        private void RegisterStartUpScript(ScriptManager sm, string script, string scriptName = "", bool includeScript = true)
        {
            if (string.IsNullOrWhiteSpace(scriptName))
                scriptName = Guid.NewGuid().ToString();

            if (Page.ClientScript.IsStartupScriptRegistered(scriptName)) return;

            if (sm == null)
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), scriptName, script, includeScript);
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), scriptName, script, includeScript);
            }
        }
    }
}
