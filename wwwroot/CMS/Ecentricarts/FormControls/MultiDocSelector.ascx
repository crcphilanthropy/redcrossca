﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MultiDocSelector.ascx.cs" Inherits="CMSApp.Ecentricarts.FormControls.MultiDocSelector.MultiDocSelector" %>
<%@ Import Namespace="CMS.SiteProvider" %>
<%@ Import Namespace="CMS.Localization" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>

<script type="text/javascript">
    // Limit scope pollution from any deprecated API
    (function () {

        var matched, browser;

        // Use of jQuery.browser is frowned upon.
        // More details: http://api.jquery.com/jQuery.browser
        // jQuery.uaMatch maintained for back-compat
        jQuery.uaMatch = function (ua) {
            ua = ua.toLowerCase();

            var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
                /(webkit)[ \/]([\w.]+)/.exec(ua) ||
                /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
                /(msie) ([\w.]+)/.exec(ua) ||
                ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
                [];

            return {
                browser: match[1] || "",
                version: match[2] || "0"
            };
        };

        matched = jQuery.uaMatch(navigator.userAgent);
        browser = {};

        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if (browser.chrome) {
            browser.webkit = true;
        } else if (browser.webkit) {
            browser.safari = true;
        }

        jQuery.browser = browser;

        jQuery.sub = function () {
            function jQuerySub(selector, context) {
                return new jQuerySub.fn.init(selector, context);
            }
            jQuery.extend(true, jQuerySub, this);
            jQuerySub.superclass = this;
            jQuerySub.fn = jQuerySub.prototype = this();
            jQuerySub.fn.constructor = jQuerySub;
            jQuerySub.sub = this.sub;
            jQuerySub.fn.init = function init(selector, context) {
                if (context && context instanceof jQuery && !(context instanceof jQuerySub)) {
                    context = jQuerySub(context);
                }

                return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
            };
            jQuerySub.fn.init.prototype = jQuerySub.fn;
            var rootjQuerySub = jQuerySub(document);
            return jQuerySub;
        };
    })();
</script>

<style>
    .multi-doc.container-fluid {
        padding: 10px;
    }

    .multi-doc .clickable {
        cursor: pointer;
    }

    .multi-doc .sort-arrow {
        float: right;
        display: inline !important;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 12px;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        background-color: #777;
        border-radius: 10px;
    }

    .multi-doc .list-group-items {
        min-height: 300px;
        border: 1px solid #ddd;
    }

    .multi-doc .list-group-item {
        position: relative;
        display: block;
        padding: 5px 5px;
        list-style: none;
        margin-left: -40px;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid #ddd;
    }

        .multi-doc .list-group-item i {
            float: right;
        }

            .multi-doc .list-group-item i.NodeLink {
                margin-top: 5px;
            }

        .multi-doc .list-group-item span.badge {
            float: right;
        }

    .multi-doc #sortable .list-group-item {
        cursor: ns-resize;
    }

    .multi-doc .clear-input {
        text-indent: -1000em;
        width: 16px;
        height: 16px;
        display: inline-block;
        background-image: url(http://p.yusukekamiyamane.com/icons/search/fugue/icons/cross.png);
        background-repeat: no-repeat;
        position: relative;
        left: -10px;
        top: -23px;
        float: right;
    }

    .cms-bootstrap .editing-form-value-cell .multi-doc .form-control {
        max-width: 100%;
    }

    .multi-doc .col-md-6 {
        padding-bottom: 10px;
    }

    .pagination {
        height: 36px;
        margin: 18px 0;
    }

        .pagination ul {
            display: inline-block;
            *display: inline;
            /* IE7 inline-block hack */
            *zoom: 1;
            margin-left: -35px;
            margin-bottom: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .pagination li {
            display: inline;
        }

        .pagination a {
            float: left;
            padding: 0 14px;
            line-height: 34px;
            text-decoration: none;
            border: 1px solid #ddd;
            border-left-width: 0;
        }

            .pagination a:hover,
            .pagination .active a {
                background-color: #f5f5f5;
            }

        .pagination .active a {
            color: #999999;
            cursor: default;
        }

        .pagination .disabled span,
        .pagination .disabled a,
        .pagination .disabled a:hover {
            color: #999999;
            background-color: transparent;
            cursor: default;
        }

        .pagination li:first-child a {
            border-left-width: 1px;
            -webkit-border-radius: 3px 0 0 3px;
            -moz-border-radius: 3px 0 0 3px;
            border-radius: 3px 0 0 3px;
        }

        .pagination li:last-child a {
            -webkit-border-radius: 0 3px 3px 0;
            -moz-border-radius: 0 3px 3px 0;
            border-radius: 0 3px 3px 0;
        }

    .pagination-centered {
        text-align: center;
    }

    .pagination-right {
        text-align: right;
    }

    .pager {
        margin-left: 0;
        margin-bottom: 18px;
        list-style: none;
        text-align: center;
        *zoom: 1;
    }

        .pager:before,
        .pager:after {
            display: table;
            content: "";
        }

        .pager:after {
            clear: both;
        }

        .pager li {
            display: inline;
        }

        .pager a {
            display: inline-block;
            padding: 5px 14px;
            background-color: #fff;
            border: 1px solid #ddd;
            -webkit-border-radius: 15px;
            -moz-border-radius: 15px;
            border-radius: 15px;
        }

            .pager a:hover {
                text-decoration: none;
                background-color: #f5f5f5;
            }

        .pager .next a {
            float: right;
        }

        .pager .previous a {
            float: left;
        }

        .pager .disabled a,
        .pager .disabled a:hover {
            color: #999999;
            background-color: #fff;
            cursor: default;
        }

    .badge.badge-all {
        display: none;
    }

    .badge.badge-loader i {
        margin-top: -5px;
    }

    .max-reached ul .clickable {
        color: #ccc;
        cursor: not-allowed;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="multi-doc container-fluid">
    <section data-ng-controller="MultiDocCTRL" data-ng-init='options=<%= Options %>;init("multi-doc-<%=ClientID %>","<%= SiteContext.CurrentSiteName %>", <%=MaxSize %>);' id="multi-doc-<%=ClientID %>">
        <div class="row" data-ng-cloak="">
            <div class="col-md-6" data-ng-class="maxSize > selectedItems.length ? '' : 'max-reached'">
                <h5>Available Items</h5>
                <div class="row">
                    <div class="col-md-4">
                        <p>
                            <input type="text" class="form-control deletable" id="txtSearchText" placeholder="Search Text" data-ng-model="search.$">
                            <%--<a class="clickable clear-input" data-ng-click="search.$ = ''">Clear results</a>--%>
                        </p>
                    </div>
                    <asp:PlaceHolder runat="server" ID="phSites">
                        <div class="col-md-4">
                            <p>
                                <asp:DropDownList runat="server" ID="ddlSites" data-ng-model="search.siteName" class="form-control">
                                </asp:DropDownList>
                            </p>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phCultures">
                        <div class="col-md-4">
                            <p>
                                <asp:DropDownList runat="server" ID="ddlCultures" data-ng-model="search.documentCultures" class="form-control">
                                </asp:DropDownList>
                            </p>
                        </div>
                    </asp:PlaceHolder>
                    <div class="col-md-4">
                        <p>
                            <a class="clickable" data-ng-click="showAdvanceFilter = !showAdvanceFilter">Advance Filters <i aria-hidden="true" class="{{showAdvanceFilter ? 'icon-chevron-up' : 'icon-chevron-down'}}"></i></a>
                        </p>
                    </div>
                </div>
                <section class="advance-filter" data-ng-show="showAdvanceFilter">
                    <p>
                        <input type="text" class="form-control deletable" id="Text1" placeholder="Document Name" data-ng-model="search.documentName">
                    </p>
                    <p id="pAuthour" runat="server" visible="False">
                        <input type="text" class="form-control deletable" id="Text2" placeholder="Author" data-ng-model="search.author">
                    </p>
                    <p id="pCategories" runat="server" visible="False">
                        <asp:DropDownList runat="server" ID="ddlCategories" data-ng-model="search.categories" class="form-control" AppendDataBoundItems="True">
                            <asp:ListItem Text="Categories..." Value="" />
                        </asp:DropDownList>
                    </p>
                    <p id="pPageTypes" runat="server" visible="False">
                        <asp:DropDownList runat="server" ID="ddlPageTypes" data-ng-model="search.className" class="form-control" AppendDataBoundItems="True">
                            <asp:ListItem Text="Page Types..." Value="" />
                        </asp:DropDownList>
                    </p>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="<%=ClientID %>_startDate">Published From</label>
                                <input type="date" data-ng-model="startDate" id="<%=ClientID %>_startDate" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="<%=ClientID %>_endDate">Published To</label>
                                <input type="date" data-ng-model="endDate" id="<%=ClientID %>_endDate" class="form-control" />
                            </div>
                        </div>
                    </div>
                </section>
                <ul class="list-group">
                    <li class="list-group-item"><span class="badge badge-loader clickable glyphicon glyphicon-forward" data-ng-click="addAllFiltered()">Loading... <i class="fa fa-spinner fa-spin" style="font-size: 24px; color: red"></i></span><span class="badge badge-all clickable glyphicon glyphicon-forward" data-ng-click="maxSize > selectedItems.length && addAllFiltered()"><i class="fa fa-plus-square" aria-hidden="true"></i></span>All</li>
                </ul>
                <ul class="list-group list-group-items">
                    <li class="list-group-item {{item.workflowStep}}" data-ng-repeat="item in (filteredItems = (documents | filter: search | filterDateRange:startDate:endDate | orderBy : 'documentName')).slice(pagerStart, pagerEnd)" data-ng-switch="item.workflowStep">
                        <span class="badge clickable glyphicon glyphicon-arrow-right" data-ng-click="maxSize > selectedItems.length && addItem(item)"><i class="fa fa-plus-square-o" aria-hidden="true"></i></span>
                        <i data-ng-switch-when="archived" aria-hidden="true" class="NodeLink icon-circle tn color-gray-100" title="Archived page"></i>
                        <i data-ng-switch-when="edit" aria-hidden="true" class="NodeLink icon-diamond tn color-orange-80" title="New version not yet in the Published step"></i>
                        <i data-ng-switch-default aria-hidden="true" class="NodeLink icon-check-circle tn color-green-100" title="Published page"></i>
                        <%--<span class="badge pull-right">
                            <img data-ng-repeat="flag in item.documentCultures" class="icon-only" src="/CMSPages/GetResource.ashx?image=%5bImages.zip%5d%2fFlags%2f16x16%2f{{flag}}.png">
                        </span>--%>
                        <a href="{{item.documentUrl}}" target="_blank" title="{{item.author}}">{{item.documentName}}</a>
                        
                    </li>
                </ul>
                <div class="pagination pagination-centered" data-ng-show="totalItems > itemsPerPage">
                    <uib-pagination boundary-links="true" total-items="totalItems" max-size="5" boundary-links="true" force-ellipses="true" items-per-page="itemsPerPage" ng-model="currentPage" class="pagination-sm pagination-centered" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" ng-change="pageChanged()"></uib-pagination>
                </div>
            </div>
            <div class="col-md-6">
                <h5>Selected Items <span class="badge pull-right" data-ng-show="maxSize < <%= UnlimitedSelection %>">Max: {{maxSize}}</span></h5>
                <p>
                    <input type="text" class="form-control" id="txtSearchText2" placeholder="Search Text" data-ng-model="searchText2">
                </p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge badge-loader clickable glyphicon glyphicon-backward" data-ng-click="removeAll()">Loading... <i class="fa fa-spinner fa-spin" style="font-size: 24px; color: red"></i></span><span class="badge badge-all clickable glyphicon glyphicon-backward" data-ng-click="removeAll()"><i class="fa fa-minus-square" aria-hidden="true"></i></span>All
                    </li>
                </ul>
                <ul class="list-group list-group-items" id="sortable" ui-sortable>
                    <li class="list-group-item {{item.workflowStep}}" data-ng-repeat="item in (filteredSelectedItems = (selectedItems | filter: searchText2 | orderBy : 'order'))" data-ng-switch="item.workflowStep">

                        <span class="badge clickable glyphicon glyphicon-arrow-left" data-ng-click="removeItem(item)"><i class="fa fa-minus-square-o" aria-hidden="true"></i></span>
                        <i data-ng-switch-when="archived" aria-hidden="true" class="NodeLink icon-circle tn color-gray-100" title="Archived page"></i>
                        <i data-ng-switch-when="edit" aria-hidden="true" class="NodeLink icon-diamond tn color-orange-80" title="New version not yet in the Published step"></i>
                        <i data-ng-switch-default aria-hidden="true" class="NodeLink icon-check-circle tn color-green-100" title="Published page"></i>
                        {{item.order}}. <a href="{{item.documentUrl}}" target="_blank" title="{{item.author}}">{{item.documentName}}</a>
                    </li>
                </ul>
            </div>
        </div>
        <asp:TextBox runat="server" ID="hidSelectedDocuments" data-ng-model="selectedItemsStr" Style="display: none" />
    </section>
</div>


