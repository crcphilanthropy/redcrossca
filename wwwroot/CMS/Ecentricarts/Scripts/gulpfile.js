var 
	argv = require('yargs').argv,
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    plumber  = require('gulp-plumber'),
    clean = require('gulp-clean'),
    inject = require('gulp-inject-string'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    config = require('./gulpfile-config.js'),
    autoprefixer = require('gulp-autoprefixer');

// ============================== //

config.init(argv['build-mode']);

// ============================== //


// Timeline Tasks //

gulp.task('eca:mobilemenu', function() {

    return gulp.src(config.sourcePaths.mobileMenu.app)
        .pipe(plumber())
        .pipe(gulpif(config.buildMode.isDevelopment, sourcemaps.init()))
        .pipe(concat('eca.mobilemenu.js'))
        .pipe(gulpif(!config.buildMode.isDevelopment, uglify()))
        .pipe(gulpif(config.buildMode.isDevelopment, sourcemaps.write('./')))
        .pipe(gulp.dest(config.buildPaths.mobileMenu.app));

});

gulp.task('eca.mobilemenu:build', ['eca:mobilemenu']);
gulp.task('build', ['eca.mobilemenu:build']);

gulp.task('eca.mobilemenu:watch', ['eca.mobilemenu:build'], function() {
    gulp.watch(['src/eca/*.js', 'src/eca/kentico/*.js', 'e2e/*.js'], ['eca:mobilemenu']);
});


// ============================== //

gulp.task('default', ['build']);
