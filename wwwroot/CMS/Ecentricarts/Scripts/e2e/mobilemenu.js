﻿window.onload = initE2eTest;

function initE2eTest() {
    ECA.kentico.mobilemenu.serviceUrl = '/API/Menu';

    var backButtonTemplate = "<a class='get-menu nav-back nav-title' data-path='<%this.path%>'><%this.title%></a>";
    var currentNodeTemplate = "<li class=\"parent\"><a data-path='<%this.path%>' href='<%this.url%>'> <%this.title%></a></li>";
    var template = "<%for(var index in this.navigationItemList) {%>" +
        "<li>" +
        "<% if(this.navigationItemList[index].hasChildren) {%>" +
        "<a class='get-menu' href='<%this.navigationItemList[index].url%>' data-path='<%this.navigationItemList[index].path%>'><%this.navigationItemList[index].title%></a>" +
        "<%}else{%>" +
        "<a data-path='<%this.path%>' href='<%this.navigationItemList[index].url%>'><%this.navigationItemList[index].title%></a>" +
        "<%}%>" +
        "</li>" +
        "<%}%>";
    var lang = $('body').hasClass('FRCA') ? 'fr-CA' : 'en-CA';
    var isIE8 = $('body').hasClass('IE8');

    function loadMenu() {
        if (isIE8) return;

        var path = navPath;
        try {
            if (typeof this.getAttribute === "function") {
                path = this.getAttribute('data-path');
            }
        }catch (ex){}

        var options = {
            path: path,
            orderByCondition: "NodeOrder ASC",
            //andWhereCondition: ["ExcludeInMobileNav <> 1"],
            orWhereCondition: [],
            classNames: ["CRC.Page", "CRC.Appeal", "CRC.Province", "CRC.IYCBranchFolder", "CMS.Blog", "CRC.Quiz"],
            backButtonResourceString: 'crc.nav.back',
            checkPermissions: false,
            useCustomFields: false,
            lang: lang
         };
         ECA.kentico.mobilemenu.getMenuAtPath(path, options, function (data) {
            var html = '';
            if (data.parentNode != undefined) {

                // Slide Animation
                document.getElementById('menu').classList.remove("toggle");

                setTimeout(function () {
                    document.getElementById('menu').classList.add("toggle");
                }, 50);

                // Back Button JS
                document.getElementById('Back').innerHTML = ECA.template.render(backButtonTemplate, data.parentNode);

            } else {

                // Slide Animation
                document.getElementById('menu').classList.remove("toggle");

                setTimeout(function () {
                    document.getElementById('menu').classList.add("toggle");
                }, 50);

                // Back Button JS
                document.getElementById('Back').innerHTML = "";

            }
            if (data.currentNode != undefined && data.parentNode != undefined)
                html += ECA.template.render(currentNodeTemplate, data.currentNode);

            html += ECA.template.render(template, data); 
            document.getElementById('menu').innerHTML = html;
            
            
            // Loop navigation items to current item 
            var currentStateNode = data.currentStateNode.url,
                navItems = document.getElementById('menu');
             
            var currentItem = $('li', navItems).map(function(){ // loop all nav items
               var path = $('a', this).attr('href').replace(/\.[^/.]+$/, ""); // Replace .aspx extesions
               if (path === currentStateNode){
                  return this; // Set currentItem to this
               }
            });
            
            if(currentItem) // Add class to current item
               currentItem.addClass('current');
            

            // Add click events to items with lower level menus
            var elements = document.getElementsByClassName("get-menu");
            for (var i = 0; i < elements.length; i++) {               
                
                elements[i].addEventListener('click', function (e) {
                    e.preventDefault();
                    this.classList.add('loading');
                });

                elements[i].addEventListener("click", loadMenu);

            }
            
           // Dispatch a menu onLoaded event 
           if ($.isFunction($.fn.ecaMenuLoaded)) {
               $.fn.ecaMenuLoaded();
           }          
         
            
        });
    }

    try {
        loadMenu();
    } catch(e) {}

}