﻿"use strict";

if (typeof ECA === 'undefined' || !ECA) {
    var ECA = {};
}

window.ECA = Object.assign(ECA, {
    namespace: 'ECA',
    addModule: function (namespace, module) {
        var parts = namespace.split('.'),
            parent = window.ECA,
            i;

        var namespaceTarget = parts[parts.length - 1];

        if (parts[0] === ECA.namespace) {
            parts = parts.slice(1);
        }

        for (i = 0; i < parts.length; i += 1) {
            if (typeof parent[parts[i]] === "undefined") {
                parent[parts[i]] = {};
            }

            if (parts[i] === namespaceTarget) {
                parent[parts[i]] = module;
            }

            parent = parent[parts[i]];
        }

        return parent;
    }
})