﻿var multiDocSelectorApp = angular.module('multiDocSelectorApp', ['ui.bootstrap']);

multiDocSelectorApp.factory('multiDocFactory', ['$http', function ($http) {
    return {
        getAllItems: function (options) {
            return $http.get("/api/multidoc?options=" + JSON.stringify(options));
        },
    };
}])
.filter("filterDateRange", function () {
    return function (items, from, to) {
        if (from == undefined && to == undefined)
            return items;

        var df = new Date(from);
        var dt = new Date(to);

        if (df == 'Invalid Date')
            df = new Date('1/1/1970');

        if (dt == 'Invalid Date')
            dt = new Date('1/1/3000');

        var arrayToReturn = [];
        for (var i = 0; i < items.length; i++) {
            var tf = new Date(items[i].publishedDate);
            if (tf >= df && tf <= dt) {
                arrayToReturn.push(items[i]);
            }
        }

        return arrayToReturn;

    };
}).controller('MultiDocCTRL', function ($scope, $filter, multiDocFactory) {
    $scope.multiDocFactory = multiDocFactory;
    $scope.search = new Object();
    $scope.options = '';
    $scope.selectedItems = [];
    $scope.filteredItems = [];
    $scope.documents = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 9999999;
    $scope.itemsPerPage = 10;
    $scope.pagerStart = 0;
    $scope.pagerEnd = $scope.itemsPerPage;

    $scope.selectedTotalItems = 0;
    $scope.selectedCurrentPage = 1;
    $scope.selectedPagerStart = 0;
    $scope.selectedPagerEnd = $scope.itemsPerPage;

    $scope.init = function (multidocid, siteName, maxSize) {
        if (maxSize > 0)
            $scope.maxSize = maxSize;

        $scope.search.siteName = siteName;

        $scope.options.topN = -1;
        $scope.multiDocFactory.getAllItems($scope.options).then(function (result) {
            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].selected)
                    $scope.selectedItems.push(result.data[i]);
                else
                    $scope.documents.push(result.data[i]);
            }

            if ($scope.options.topN > 0) {
                $scope.options.topN = -1;
                $scope.multiDocFactory.getAllItems($scope.options)
                    .then(function (result) {
                        $scope.selectedItems = [];
                        $scope.filteredItems = [];
                        $scope.documents = [];
                        for (var i = 0; i < result.data.length; i++) {
                            if (result.data[i].selected)
                                $scope.selectedItems.push(result.data[i]);
                            else
                                $scope.documents.push(result.data[i]);
                        }
                        $scope.setOrders();
                        $('.badge-loader').hide();
                        $('.badge-all').show();
                    });
                $('.badge-loader').fadeTo("fast", 0.33);
            } else {
                $('.badge-loader').hide();
                $('.badge-all').show();
            }

        });

        var sortableEle = $('#' + multidocid + ' #sortable').sortable({
            start: $scope.dragStart,
            update: $scope.dragEnd
        });
    };

    $scope.order = function (item, direction, index) {
        if (index === 0 && direction === -1 || (index === $scope.selectedItems.length - 1 && direction === 1)) return;

        var currentOrder = item.order;
        var destOrder = $scope.selectedItems[index + direction].order;
        $scope.selectedItems[index + direction].order = currentOrder;
        item.order = destOrder;
    };

    $scope.addItem = function (item) {
        item.order = $scope.selectedItems.length + 1;
        item.selected = true;
        $scope.selectedItems.push(item);
        $scope.documents.splice($scope.documents.indexOf(item), 1);
        $scope.setOrders();
    };

    $scope.addAllFiltered = function () {
        for (var i = 0; i < $scope.filteredItems.length && $scope.selectedItems.length < $scope.maxSize; i++) {
            $scope.addItem($scope.filteredItems[i]);
        }
    };

    $scope.removeItem = function (item) {
        item.order = 1000;
        item.selected = false;

        $scope.documents.push(item);
        $scope.selectedItems.splice($scope.selectedItems.indexOf(item), 1);
        $scope.setOrders();
    };

    $scope.removeAll = function () {
        for (var i = 0; i < $scope.filteredSelectedItems.length; i++) {
            $scope.removeItem($scope.filteredSelectedItems[i]);
        }
    };

    $scope.setOrders = function () {
        for (var i = 0; i < $scope.selectedItems.length; i++) {
            if ($scope.selectedItems[i] != undefined)
                $scope.selectedItems[i].order = i + 1;
        }
    };

    $scope.dragStart = function (e, ui) {
        ui.item.data('start', ui.item.index());
    };
    $scope.dragEnd = function (e, ui) {

        $scope.$apply(function () {
            var start = ui.item.data('start'),
            end = ui.item.index();
            var item = $scope.selectedItems.splice(start, 1);
            $scope.selectedItems.splice(end, 0, item[0]);
            $scope.setOrders();
        });
    };

    $scope.pageChanged = function () {
        $scope.pagerStart = $scope.itemsPerPage * ($scope.currentPage - 1);
        $scope.pagerEnd = $scope.itemsPerPage * $scope.currentPage;
    };

    $scope.selectedPageChanged = function () {
        $scope.selectedPagerStart = $scope.itemsPerPage * ($scope.selectedCurrentPage - 1);
        $scope.selectedPagerEnd = $scope.itemsPerPage * $scope.selectedCurrentPage;
    };

    $scope.$watch('search.categories', function (newValue, oldValue) {
        if (newValue == "")
            $scope.search.categories = undefined;

    }, true);

    $scope.$watch('selectedItems', function (newValues, oldValues) {
        if (newValues == undefined) return;
        var items = [];
        for (var i = 0; i < newValues.length; i++) {
            items.push(newValues[i].documentGuid);
        }
        $scope.selectedItemsStr = items.join(';');
    }, true);

    $scope.$watch('filteredSelectedItems', function (newValues, oldValues) {
        if (newValues == undefined || oldValues == undefined) return;
        $scope.selectedTotalItems = newValues.length;
        if (newValues.length != oldValues.length) {
            $scope.selectedCurrentPage = 1;
            $scope.selectedPagerStart = 0;
            $scope.selectedPagerEnd = $scope.itemsPerPage;
        }
    }, true);

    $scope.$watch('filteredItems', function (newValues, oldValues) {
        if (newValues == undefined || oldValues == undefined) return;
        $scope.totalItems = newValues.length;
    }, true);

    $scope.$watch('search', function (newValue, oldValue) {
        if (newValue == undefined || oldValue == undefined) return;
        $scope.totalItems = $scope.filteredItems.length;
        if (newValue != oldValue) {
            $scope.currentPage = 1;
            $scope.pagerStart = 0;
            $scope.pagerEnd = $scope.itemsPerPage;
        }
    }, true);

});

//angular.bootstrap(document, ['multiDocSelectorApp']);