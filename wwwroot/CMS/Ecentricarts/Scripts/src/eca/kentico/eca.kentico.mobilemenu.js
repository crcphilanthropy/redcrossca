﻿"use strict";

ECA.addModule('kentico.mobilemenu', (function () {

    var serviceUrl = '';
    var menuStore = [];

    var getMenuAtPath = function (path, options, callback) {

        var response = findMenuAtPath(path);
        
        if (response === null) {
            callServiceUrl(path, options, callback);
        } else {
            callback(response);
        }
    };
    

    var findMenuAtPath = function (path) {
        if (menuStore.length > 0) {
            for (var i = 0; i < menuStore.length; i++) {
                if (path === menuStore[i].path) {
                    console.log("Load from cache");
                    return menuStore[i];
                }
            }
        }
        return null;
    };

    var callServiceUrl = function(path, options, callback) {
        var requestUrl = ECA.kentico.mobilemenu.serviceUrl + '?options=' + JSON.stringify(options);

        ECA.http.get({
            url: requestUrl,
            onSuccess: function (data) {
                var responseObject = JSON.parse(data);
                responseObject.path = path;
                menuStore.push(responseObject);
                callback(responseObject);
            }
        });
    };

    return {
        getMenuAtPath : getMenuAtPath,
        serviceUrl: serviceUrl
    };

})());