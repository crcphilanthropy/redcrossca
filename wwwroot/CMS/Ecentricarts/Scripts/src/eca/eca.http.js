﻿"use strict";

ECA.http = (function () {

    var globalBasicAuth = null;

    var getXHR = function () {
        if (typeof XMLHttpRequest !== 'undefined') {
            return new XMLHttpRequest();
        }
        var versions = [
            "MSXML2.XmlHttp.5.0",
            "MSXML2.XmlHttp.4.0",
            "MSXML2.XmlHttp.3.0",
            "MSXML2.XmlHttp.2.0",
            "Microsoft.XmlHttp"
        ];

        var xhr;
        for (var i = 0; i < versions.length; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            } catch (e) {
            }
        }
        return xhr;
    };

    var send = function (options) {
        var defaults = {
            url: undefined,
            method: 'GET',
            data: null,
            onSuccess: function () { },
            onError: function () { },
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var x = getXHR();

        x.open(params.method, params.url, true);


        for (var key in params.headers) {
            if (params.headers.hasOwnProperty(key)) {
                x.setRequestHeader(key, params.headers[key]);
            }
        }

        if (ECA.http.globalBasicAuth !== null) {
            x.setRequestHeader("Authorization", 'Basic ' + ECA.http.globalBasicAuth);
        }

        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                if (isResponseSuccessful(x)) {
                    params.onSuccess(x.responseText);
                }
                else {
                    params.onError(x.statusText);
                }
            }
        };
        if (params.method == 'POST') {
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        x.send(params.data);
    };

    var isResponseSuccessful = function (httpRequest) {
        var success = (httpRequest.status == 0 ||
            (httpRequest.status >= 200 && httpRequest.status < 300) ||
            httpRequest.status == 304 || httpRequest.status == 1223);
        return success;
    };

    var get = function (options) {
        var defaults = {
            url: undefined,
            data: null,
            onSuccess: undefined,
            onError: undefined,
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var query = [];
        for (var key in params.data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(params.data[key]));
        }

        send(params);
    };

    var post = function (options) {
        var defaults = {
            url: undefined,
            data: null,
            onSuccess: undefined,
            onError: undefined,
            headers: undefined
        };

        var params = ECA.util.extend({}, defaults, options);

        var formattedData = [];
        for (var key in params.data) {
            formattedData.push(encodeURIComponent(key) + '=' + encodeURIComponent(params.data[key]));
        }

        params.data = formattedData;

        send(params);
    };

    return {
        globalBasicAuth: globalBasicAuth,
        getXHR: getXHR,
        send: send,
        get: get,
        post: post
    };

})();


