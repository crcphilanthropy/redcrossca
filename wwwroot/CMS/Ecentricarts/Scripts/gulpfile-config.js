module.exports = (function () {

    var chalk = require('gulp-util').colors;

    function GulpConfig() {

        var self = this,
            buildModeTypes = {
                dev: {
                    id: 1,
                    displayName: '\n=========================\n Build Mode: Development \n=========================\n',
                    isDevelopment: true,
                    cssOutputStyle: 'expanded'
                },
                release: {
                    id: 2,
                    displayName: '\n=====================\n Build Mode: Release \n=====================\n',
                    isDevelopment: false,
                    cssOutputStyle: 'compressed'
                }
            };

        // ----------------------------- //

        self.sourcePaths = {
            mobileMenu: {
                app: [
                    'src/eca/eca.js',
                    'src/eca/eca.util.js',
                    'src/eca/eca.http.js',
                    'src/eca/kentico/eca.kentico.mobilemenu.js',
                    'src/eca/eca.template.js',
                    'e2e/mobilemenu.js'
                ]
            }
        };

        self.buildPaths = {
            mobileMenu: {
                app: 'eca.mobilemenu'
            }
        };

        self.buildMode = {
            id: null,
            displayName: '',
            isDevelopment: true,
            cssOutputStyle: null
        };

        self.init = function (buildModeArg) {

            self.buildMode = buildModeTypes[buildModeArg] || buildModeTypes.dev;
            console.log(chalk.yellow(self.buildMode.displayName));

        };

        return self;
    }

    return new GulpConfig();

}());
