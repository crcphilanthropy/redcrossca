﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMS.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultiMediaApi.Models;
using System.Web.Http.Results;
using System.Web.Http;

namespace MultiMediaApiTest
{
    [TestClass()]
    public class CatalogControllerTests
    {
        private readonly CatalogController _controller;
        private readonly int categoryCount;
        private readonly int regionCount;

        public CatalogControllerTests()
        {
            _controller = new CatalogController();
            categoryCount = 14;
            regionCount = 17;
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod()]
        public void GetCategoriesTest()
        {
            var controller = _controller;
            var result = controller.GetCategories() as List<Category>;
            Assert.AreEqual(categoryCount, result.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetCategoriesNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;

            var result = controller.GetCategories() as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void GetRegionsTest()
        {
            var controller = _controller;
            var result = controller.GetRegions() as List<Region>;
            Assert.AreEqual(regionCount, result.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetRegionsNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;

            var result = controller.GetRegions() as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void GetTagsTest()
        {
            var controller = _controller;
            var result = controller.GetTags() as Tags;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetTagsNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.GetTags() as Tags;
                Assert.IsNotNull(result);
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual("Not found", ex.Message);
            }
            
        }



        private List<Category> CategoryList()
        {
            var testCategories = new List<Category>();
            testCategories.Add(new Category
            {
                CategoryID = 28,
                CategoryDisplayName = "Community Health"
            });

            testCategories.Add(new Category
            {
                CategoryID = 40,
                CategoryDisplayName = "Youth"
            });

            return testCategories;
        }

        private List<Region> RegionList()
        {
            var testCategories = new List<Region>();
            testCategories.Add(new Region
            {
                CategoryID = 22,
                CategoryDisplayName = "Alberta"
            });

            testCategories.Add(new Region
            {
                CategoryID = 43,
                CategoryDisplayName = "Manitoba"
            });

            return testCategories;
        }

    }
}