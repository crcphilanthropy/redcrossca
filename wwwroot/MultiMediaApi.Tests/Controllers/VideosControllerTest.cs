﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CMS.Controllers;
using System.Web.Http;
using MultiMediaApi.Models;
using System.Web.Http.Results;

namespace MultiMediaApiTest
{
    /// <summary>
    /// Summary description for VideosControllerTest
    /// </summary>
    [TestClass]
    public class VideosControllerTest
    {
        private readonly VideosController _controller;
        private readonly List<string> categories;
        private readonly List<string> regions;
        private readonly string path;
        public VideosControllerTest()
        {
            _controller = new VideosController();
            categories = new List<string>();
            regions = new List<string>();
            path = "/Who-We-Are/Media-News/Photos-Videos-for-Media/Videos-for-Media/";
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetAllVideos_ShouldReturnAllVideos()
        {
            var controller = _controller;
            var result = controller.Get(path, categories, regions) as List<Video>;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetAllVideosNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;

            var result = controller.Get(path, categories, regions) as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetVideo_ShouldReturnCorrectVideo()
        {
            var controller = _controller;
            var result = controller.Get(1, path) as Video;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Invalid Id")]
        public void GetVideoInvalidID_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(-1, path) as Video;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Invalid Id", ex.Message);
            }
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetVideoNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(1000, path) as Video;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Not found", ex.Message);
            }
        }

        [TestMethod]
        public void GetVideoQuery_ShouldReturnFilteredPhotoList()
        {
            var controller = _controller;
            var category = "community health";
            var region = "Alberta";
            categories.Add(category); regions.Add(region);

            var result = controller.Get(path, categories, regions) as OkNegotiatedContentResult<Video>;
            Assert.IsNotNull(result);
        }
    }
}
