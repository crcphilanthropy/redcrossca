﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MultiMediaApi.Models;
using System.Web.Http.Results;

using CMS.Controllers;
using System.Net.Http;
using System;
using System.Web.Http;

namespace MultiMediaApiTest
{
    /// <summary>
    /// Summary description for TestBannerSetController
    /// </summary>
    [TestClass]
    public class TestBannerSetController
    {
        private readonly BannerSetsController _controller;

        public TestBannerSetController()
        {
            _controller = new BannerSetsController();
        }
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void GetAllBannerSets_ShouldReturnAllBannerSets()
        {
            var testBannerSets = GetTestBannerSets();
            var controller = _controller;
            var result = controller.Get() as List<BannerSet>;

            Assert.IsNotNull(result);
            Assert.AreEqual(testBannerSets.Count, result.Count);
            foreach (var item in result)
            {
                foreach (var banner in item.Banners)
                {
                    Assert.AreEqual(string.Empty, banner.EmbedLink);
                    Assert.AreEqual(string.Empty, banner.TrackingCode);
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetAllBannerSetsNotFound_ShouldReturnCorrectExceptionMessage()
        {
            var controller = _controller;

            var result = controller.Get() as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetBannerSet_ShouldReturnCorrectBannerSet()
        {
            var testBannerSets = GetTestBannerSets();
            var controller = _controller;

            var result = controller.Get(3) as BannerSet;
            Assert.IsNotNull(result);
            Assert.AreEqual(testBannerSets[3].Title, result.Title);
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Invalid Id")]
        public void GetBannerSetInvalidId_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(-1) as BannerSet;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Invalid Id", ex.Message);
            }
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetBannerSetNotfound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(1000) as BannerSet;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Not found", ex.Message);
            }
        }

        [TestMethod]
        public void PostBannerSet_ShouldReturnAllBannerSetsWithLinks()
        {
            var controller = _controller;

            var result = controller.Post("ECA") as OkNegotiatedContentResult<BannerSet>;
            Assert.IsNotNull(result);
            foreach (var item in result.Content.Banners)
            {
                Assert.AreNotEqual(string.Empty, item.EmbedLink);
                Assert.AreNotEqual(string.Empty, item.TrackingCode);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "No company name entered")]
        public void PostBannerSetNoCompanyName_ShouldReturnCorrectExceptionMessage()
        {
            var controller = _controller;

            var result = controller.Post("") as NegotiatedContentResult<BannerSet>;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void PostBannerSetNotFound_ShouldReturnCorrectExceptionMessage()
        {
            var controller = _controller;
            
            var result = controller.Post("ECA") as NotFoundResult;
            Assert.IsNotNull(result);
        }

        private List<BannerSet> GetTestBannerSets()
        {
            var testBannerSets = new List<BannerSet>();
            testBannerSets.Add(new BannerSet
            {
                BannerSetID = 1,
                Title = "Test Banner Set",
                Description = "Hi I'm a crazy banner set",
                Banners = new List<Banner>() {
                    new Banner()
                    { BannerID = 4, BannerSetID = 1, BannerType = "728 * 90", EmbedLink="",
                    Image ="/crc/img/Who-We-Are/About/Board%20of%20Directors/Alan-Pearson.jpg?ext=.jpg",
                    Title="Bannerset 1 Banner test", TrackingCode="" }
                }
            });
            testBannerSets.Add(new BannerSet { BannerSetID = 3, Title = "banner set 3", Description = "hi", Banners = { } });

            return testBannerSets;
        }
    }
}
