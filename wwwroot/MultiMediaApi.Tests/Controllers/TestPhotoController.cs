﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MultiMediaApi.Models;
using System.Web.Http.Results;
using CMS.Controllers;
using System.Web.Http;
using System;

namespace MultiMediaApiTest
{
    /// <summary>
    /// Summary description for TestBannerSetController
    /// </summary>
    [TestClass]
    public class TestPhotoController
    {
        private readonly PhotosController _controller;
        private readonly List<string> categories;
        private readonly List<string> regions;

        public TestPhotoController()
        {
            _controller = new PhotosController();
            categories = new List<string>();
            regions = new List<string>();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void GetAllPhotos_ShouldReturnAllPhotos()
        {
            var controller = _controller;
            var result = controller.Get(categories, regions) as List<Photo>;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetAllPhotosNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            var result = controller.Get(categories, regions) as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPhoto_ShouldReturnCorrectPhoto()
        {
            var photos = GetTestPhotos();
            var controller = _controller;

            var result = controller.Get(1) as Photo;
            Assert.IsNotNull(result);
            Assert.AreEqual(photos[0].Title, result.Title);
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Invalid Id")]
        public void GetPhotoInvalidID_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(-1) as Photo;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Invalid Id", ex.Message);
            }
        }

        [TestMethod]
        //[ExpectedException(typeof(HttpResponseException), "Not found")]
        public void GetPhotoNotFound_ShouldReturnCorrectErrorMessage()
        {
            var controller = _controller;
            try
            {
                var result = controller.Get(1000) as Photo;
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual("Not found", ex.Message);
            }
        }

        [TestMethod]
        public void GetPhotoQuery_ShouldReturnFilteredPhotoList()
        {
            var photos = GetTestPhotos();
            var controller = _controller;
            var category = "community health";
            var region = "Alberta";
            categories.Add(category); regions.Add(region); 

            var result = controller.Get(categories, regions) as OkNegotiatedContentResult<Photo>;
            Assert.IsNotNull(result);
        }

        private List<Photo> GetTestPhotos()
        {
            var testPhotos = new List<Photo>();
            testPhotos.Add(new Photo
            {
                PhotoID = 1,
                Title = "Demo1",
                Description = "TEsting api",
                Image = "",
                Download = true,
                PhotoCredits = "Hello world",
                Categories = new List<Category> { new Category() { } },
                Regions = new List<Region>{ new Region(){ CategoryID=22, CategoryDisplayName= "Alberta" } }
            });

            return testPhotos;
        }
    }
}
