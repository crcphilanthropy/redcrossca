﻿using System.Collections.Generic;
using Ecentricarts.GoogleSiteSearch.Interfaces;
using Ecentricarts.GoogleSiteSearch.Model;
using Ecentricarts.RestClient;

namespace Ecentricarts.GoogleSiteSearch
{
    public class SearchClient : ISearchClient
    {
        #region "Private fields"

        protected readonly string ServiceUrl;
        protected readonly string LookupMethod;

        #endregion

        #region "Public methods"

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchClient" /> class.
        /// </summary>
        /// <param name="serviceUrl">The service URL.</param>
        /// <param name="lookupMethod">The lookup method.</param>
        public SearchClient(string serviceUrl, string lookupMethod)
        {
            ServiceUrl = serviceUrl;
            LookupMethod = lookupMethod;
        }

        /// <summary>
        /// Gets the results.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public Search GetResults(Dictionary<string, string> parameters)
        {
            var result = new Search();

            if (!string.IsNullOrWhiteSpace(ServiceUrl))
            {
                var client = new RestServiceClient(ServiceUrl);

                var request = client.BuildGetRequest(LookupMethod, parameters);

                result = client.SendRequest<Search>(request);
            }

            return result;
        }

        #endregion
    }
}