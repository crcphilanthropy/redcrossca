﻿using System.Collections.Generic;
using Ecentricarts.GoogleSiteSearch.Model;

namespace Ecentricarts.GoogleSiteSearch.Interfaces
{
    public interface ISearchClient
    {
        Search GetResults(Dictionary<string, string> parameters);
    }
}