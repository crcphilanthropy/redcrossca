﻿using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Query
    {
        [JsonProperty("count")]
        public virtual int? Count
        {
            get;
            set;
        }

        [JsonProperty("cr")]
        public virtual string Cr
        {
            get;
            set;
        }

        [JsonProperty("cref")]
        public virtual string Cref
        {
            get;
            set;
        }

        [JsonProperty("cx")]
        public virtual string Cx
        {
            get;
            set;
        }

        [JsonProperty("dateRestrict")]
        public virtual string DateRestrict
        {
            get;
            set;
        }

        [JsonProperty("disableCnTwTranslation")]
        public virtual string DisableCnTwTranslation
        {
            get;
            set;
        }

        [JsonProperty("exactTerms")]
        public virtual string ExactTerms
        {
            get;
            set;
        }

        [JsonProperty("excludeTerms")]
        public virtual string ExcludeTerms
        {
            get;
            set;
        }

        [JsonProperty("fileType")]
        public virtual string FileType
        {
            get;
            set;
        }

        [JsonProperty("filter")]
        public virtual string Filter
        {
            get;
            set;
        }

        [JsonProperty("gl")]
        public virtual string Gl
        {
            get;
            set;
        }

        [JsonProperty("googleHost")]
        public virtual string GoogleHost
        {
            get;
            set;
        }

        [JsonProperty("highRange")]
        public virtual string HighRange
        {
            get;
            set;
        }

        [JsonProperty("hl")]
        public virtual string Hl
        {
            get;
            set;
        }

        [JsonProperty("hq")]
        public virtual string Hq
        {
            get;
            set;
        }

        [JsonProperty("imgColorType")]
        public virtual string ImgColorType
        {
            get;
            set;
        }

        [JsonProperty("imgDominantColor")]
        public virtual string ImgDominantColor
        {
            get;
            set;
        }

        [JsonProperty("imgSize")]
        public virtual string ImgSize
        {
            get;
            set;
        }

        [JsonProperty("imgType")]
        public virtual string ImgType
        {
            get;
            set;
        }

        [JsonProperty("inputEncoding")]
        public virtual string InputEncoding
        {
            get;
            set;
        }

        [JsonProperty("language")]
        public virtual string Language
        {
            get;
            set;
        }

        [JsonProperty("linkSite")]
        public virtual string LinkSite
        {
            get;
            set;
        }

        [JsonProperty("lowRange")]
        public virtual string LowRange
        {
            get;
            set;
        }

        [JsonProperty("orTerms")]
        public virtual string OrTerms
        {
            get;
            set;
        }

        [JsonProperty("outputEncoding")]
        public virtual string OutputEncoding
        {
            get;
            set;
        }

        [JsonProperty("relatedSite")]
        public virtual string RelatedSite
        {
            get;
            set;
        }

        [JsonProperty("rights")]
        public virtual string Rights
        {
            get;
            set;
        }

        [JsonProperty("safe")]
        public virtual string Safe
        {
            get;
            set;
        }

        [JsonProperty("searchTerms")]
        public virtual string SearchTerms
        {
            get;
            set;
        }

        [JsonProperty("searchType")]
        public virtual string SearchType
        {
            get;
            set;
        }

        [JsonProperty("siteSearch")]
        public virtual string SiteSearch
        {
            get;
            set;
        }

        [JsonProperty("siteSearchFilter")]
        public virtual string SiteSearchFilter
        {
            get;
            set;
        }

        [JsonProperty("sort")]
        public virtual string Sort
        {
            get;
            set;
        }

        [JsonProperty("startIndex")]
        public virtual int? StartIndex
        {
            get;
            set;
        }

        [JsonProperty("startPage")]
        public virtual int? StartPage
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public virtual string Title
        {
            get;
            set;
        }

        [JsonProperty("totalResults")]
        public virtual long? TotalResults
        {
            get;
            set;
        }

        /// <summary>The ETag of the item.</summary>
        public virtual string ETag
        {
            get;
            set;
        }
    }
}
