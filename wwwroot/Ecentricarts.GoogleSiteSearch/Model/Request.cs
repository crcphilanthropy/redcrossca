﻿using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Request
    {
        [JsonProperty("q")]
        public virtual string SearchTerms
        {
            get;
            set;
        }

        [JsonProperty("key")]
        public virtual string ApiKey
        {
            get;
            set;
        }

        [JsonProperty("num")]
        public virtual int NumResults
        {
            get;
            set;
        }

        [JsonProperty("start")]
        public virtual int StartIndex
        {
            get;
            set;
        }

        [JsonProperty("lr")]
        public virtual string Language
        {
            get;
            set;
        }

        [JsonProperty("safe")]
        public virtual string Safe
        {
            get;
            set;
        }

        [JsonProperty("cx")]
        public virtual string Cx
        {
            get;
            set;
        }

        [JsonProperty("cref")]
        public virtual string Cref
        {
            get;
            set;
        }

        [JsonProperty("sort")]
        public virtual string Sort
        {
            get;
            set;
        }

        [JsonProperty("filter")]
        public virtual string Filter
        {
            get;
            set;
        }

        [JsonProperty("gl")]
        public virtual string Gl
        {
            get;
            set;
        }

        [JsonProperty("cr")]
        public virtual string Cr
        {
            get;
            set;
        }

        [JsonProperty("googleHost")]
        public virtual string GoogleHost
        {
            get;
            set;
        }

        [JsonProperty("c2coff")]
        public virtual string DisableCnTwTranslation
        {
            get;
            set;
        }

        [JsonProperty("hq")]
        public virtual string Hq
        {
            get;
            set;
        }

        [JsonProperty("hl")]
        public virtual string Hl
        {
            get;
            set;
        }

        [JsonProperty("siteSearch")]
        public virtual string SiteSearch
        {
            get;
            set;
        }

        [JsonProperty("siteSearchFilter")]
        public virtual string SiteSearchFilter
        {
            get;
            set;
        }

        [JsonProperty("exactTerms")]
        public virtual string ExactTerms
        {
            get;
            set;
        }

        [JsonProperty("excludeTerms")]
        public virtual string ExcludeTerms
        {
            get;
            set;
        }

        [JsonProperty("linkSite")]
        public virtual string LinkSite
        {
            get;
            set;
        }

        [JsonProperty("orTerms")]
        public virtual string OrTerms
        {
            get;
            set;
        }

        [JsonProperty("relatedSite")]
        public virtual string RelatedSite
        {
            get;
            set;
        }

        [JsonProperty("dateRestrict")]
        public virtual string DateRestrict
        {
            get;
            set;
        }

        [JsonProperty("lowRange")]
        public virtual string LowRange
        {
            get;
            set;
        }

        [JsonProperty("highRange")]
        public virtual string HighRange
        {
            get;
            set;
        }

        [JsonProperty("searchType")]
        public virtual string SearchType
        {
            get;
            set;
        }

        [JsonProperty("fileType")]
        public virtual string FileType
        {
            get;
            set;
        }

        [JsonProperty("rights")]
        public virtual string Rights
        {
            get;
            set;
        }

        [JsonProperty("imgSize")]
        public virtual string ImageSize
        {
            get;
            set;
        }

        [JsonProperty("imgType")]
        public virtual string ImageType
        {
            get;
            set;
        }

        [JsonProperty("imgColorType")]
        public virtual string ImageColorType
        {
            get;
            set;
        }

        [JsonProperty("imgDominantColor")]
        public virtual string ImageDominantColor
        {
            get;
            set;
        }
    }
}
