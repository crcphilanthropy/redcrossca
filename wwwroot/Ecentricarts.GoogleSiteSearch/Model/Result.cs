﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Result
    {
        public class ImageData
        {
            [JsonProperty("byteSize")]
            public virtual int? ByteSize
            {
                get;
                set;
            }

            [JsonProperty("contextLink")]
            public virtual string ContextLink
            {
                get;
                set;
            }

            [JsonProperty("height")]
            public virtual int? Height
            {
                get;
                set;
            }

            [JsonProperty("thumbnailHeight")]
            public virtual int? ThumbnailHeight
            {
                get;
                set;
            }

            [JsonProperty("thumbnailLink")]
            public virtual string ThumbnailLink
            {
                get;
                set;
            }

            [JsonProperty("thumbnailWidth")]
            public virtual int? ThumbnailWidth
            {
                get;
                set;
            }

            [JsonProperty("width")]
            public virtual int? Width
            {
                get;
                set;
            }
        }

        public class LabelsData
        {
            [JsonProperty("displayName")]
            public virtual string DisplayName
            {
                get;
                set;
            }

            [JsonProperty("label_with_op")]
            public virtual string LabelWithOp
            {
                get;
                set;
            }

            [JsonProperty("name")]
            public virtual string Name
            {
                get;
                set;
            }
        }

        [JsonProperty("cacheId")]
        public virtual string CacheId
        {
            get;
            set;
        }

        [JsonProperty("displayLink")]
        public virtual string DisplayLink
        {
            get;
            set;
        }

        [JsonProperty("fileFormat")]
        public virtual string FileFormat
        {
            get;
            set;
        }

        [JsonProperty("formattedUrl")]
        public virtual string FormattedUrl
        {
            get;
            set;
        }

        [JsonProperty("htmlFormattedUrl")]
        public virtual string HtmlFormattedUrl
        {
            get;
            set;
        }

        [JsonProperty("htmlSnippet")]
        public virtual string HtmlSnippet
        {
            get;
            set;
        }

        [JsonProperty("htmlTitle")]
        public virtual string HtmlTitle
        {
            get;
            set;
        }

        [JsonProperty("image")]
        public virtual ImageData Image
        {
            get;
            set;
        }

        [JsonProperty("kind")]
        public virtual string Kind
        {
            get;
            set;
        }

        [JsonProperty("labels")]
        public virtual IList<LabelsData> Labels
        {
            get;
            set;
        }

        [JsonProperty("link")]
        public virtual string Link
        {
            get;
            set;
        }

        [JsonProperty("mime")]
        public virtual string Mime
        {
            get;
            set;
        }

        [JsonProperty("pagemap")]
        public virtual IDictionary<string, IList<IDictionary<string, object>>> Pagemap
        {
            get;
            set;
        }

        [JsonProperty("snippet")]
        public virtual string Snippet
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public virtual string Title
        {
            get;
            set;
        }

        /// <summary>The ETag of the item.</summary>
        public virtual string ETag
        {
            get;
            set;
        }
    }
}
