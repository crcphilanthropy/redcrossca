﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Promotion
    {
        public class BodyLinesData
        {
            [JsonProperty("htmlTitle")]
            public virtual string HtmlTitle
            {
                get;
                set;
            }

            [JsonProperty("link")]
            public virtual string Link
            {
                get;
                set;
            }

            [JsonProperty("title")]
            public virtual string Title
            {
                get;
                set;
            }

            [JsonProperty("url")]
            public virtual string Url
            {
                get;
                set;
            }
        }

        public class ImageData
        {
            [JsonProperty("height")]
            public virtual int? Height
            {
                get;
                set;
            }

            [JsonProperty("source")]
            public virtual string Source
            {
                get;
                set;
            }

            [JsonProperty("width")]
            public virtual int? Width
            {
                get;
                set;
            }
        }

        [JsonProperty("bodyLines")]
        public virtual IList<BodyLinesData> BodyLines
        {
            get;
            set;
        }

        [JsonProperty("displayLink")]
        public virtual string DisplayLink
        {
            get;
            set;
        }

        [JsonProperty("htmlTitle")]
        public virtual string HtmlTitle
        {
            get;
            set;
        }

        [JsonProperty("image")]
        public virtual ImageData Image
        {
            get;
            set;
        }

        [JsonProperty("link")]
        public virtual string Link
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public virtual string Title
        {
            get;
            set;
        }

        /// <summary>The ETag of the item.</summary>
        public virtual string ETag
        {
            get;
            set;
        }
    }
}
