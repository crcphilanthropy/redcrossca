﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Search
    {
        public class SearchInformationData
        {
            [JsonProperty("formattedSearchTime")]
            public virtual string FormattedSearchTime
            {
                get;
                set;
            }

            [JsonProperty("formattedTotalResults")]
            public virtual string FormattedTotalResults
            {
                get;
                set;
            }

            [JsonProperty("searchTime")]
            public virtual double? SearchTime
            {
                get;
                set;
            }

            [JsonProperty("totalResults")]
            public virtual long? TotalResults
            {
                get;
                set;
            }
        }

        public class SpellingData
        {
            [JsonProperty("correctedQuery")]
            public virtual string CorrectedQuery
            {
                get;
                set;
            }

            [JsonProperty("htmlCorrectedQuery")]
            public virtual string HtmlCorrectedQuery
            {
                get;
                set;
            }
        }

        public class UrlData
        {
            [JsonProperty("template")]
            public virtual string Template
            {
                get;
                set;
            }

            [JsonProperty("type")]
            public virtual string Type
            {
                get;
                set;
            }
        }

        [JsonProperty("context")]
        public virtual Context Context
        {
            get;
            set;
        }

        [JsonProperty("items")]
        public virtual IList<Result> Items
        {
            get;
            set;
        }

        [JsonProperty("kind")]
        public virtual string Kind
        {
            get;
            set;
        }

        [JsonProperty("promotions")]
        public virtual IList<Promotion> Promotions
        {
            get;
            set;
        }

        [JsonProperty("queries")]
        public virtual IDictionary<string, IList<Query>> Queries
        {
            get;
            set;
        }

        [JsonProperty("searchInformation")]
        public virtual SearchInformationData SearchInformation
        {
            get;
            set;
        }

        [JsonProperty("spelling")]
        public virtual SpellingData Spelling
        {
            get;
            set;
        }

        [JsonProperty("url")]
        public virtual UrlData Url
        {
            get;
            set;
        }

        /// <summary>The ETag of the item.</summary>
        public virtual string ETag
        {
            get;
            set;
        }
    }
}
