﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ecentricarts.GoogleSiteSearch.Model
{
    public class Context
    {
        public class FacetsData
        {
            [JsonProperty("anchor")]
            public virtual string Anchor
            {
                get;
                set;
            }

            [JsonProperty("label")]
            public virtual string Label
            {
                get;
                set;
            }

            [JsonProperty("label_with_op")]
            public virtual string LabelWithOp
            {
                get;
                set;
            }
        }

        [JsonProperty("facets")]
        public virtual IList<IList<FacetsData>> Facets
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public virtual string Title
        {
            get;
            set;
        }

        /// <summary>The ETag of the item.</summary>
        public virtual string ETag
        {
            get;
            set;
        }
    }
}
