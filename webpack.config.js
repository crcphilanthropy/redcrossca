const merge = require('webpack-merge');
const base = require('./config/webpack.base');

module.exports = [
  merge(base, require('./config/webpack.crc')),
  merge(base, require('./config/webpack.crcblog')),
  merge(base, require('./config/webpack.crctimeline')),
  merge(base, require('./config/webpack.crcholiday17')),
];
