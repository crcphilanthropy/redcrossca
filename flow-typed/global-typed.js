declare var __DEV__: boolean;
declare var __PROD__: boolean;
declare var __TEST__: boolean;
declare var __SERVER__: boolean;
declare var __STATS__: boolean;

declare var module: {
  hot: {
    accept(path: string, callback: () => void): void,
    accept(callback: () => void): void,
    accept(): void,
  },
};

declare module 'CSSModule' {
  declare var exports: { [key: string]: string };
}
