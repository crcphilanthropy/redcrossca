const crc = require('./config/project.crc.config');
const crctimeline = require('./config/project.crc.config');

module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'plugin:flowtype/recommended'],
  plugins: ['prettier', 'import', 'react', 'jsx-a11y', 'flowtype'],
  env: {
    browser: true,
    node: true,
    mocha: true
  },
  globals: {
    sinon: false,
    expect: false,
    __SERVER__: false,
    __DEV__: false,
    __TEST__: false,
    __PROD__: false,
    __COVERAGE__: false
  },
  rules: {
    'no-console': 0,
    'global-require': 0,
    'comma-dangle': 0,
    'class-methods-use-this': 0,
    'no-underscore-dangle': 0,
    'new-cap': 0,
    'no-duplicate-imports': [0],
    'no-mixed-operators': 0,
    'import/extensions': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default': 0,
    'import/no-unresolved': 0,
    'import/prefer-default-export': 0,
    'import/newline-after-import': 0,
    'jsx-a11y/img-has-alt': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-quotes': 0,
    'react/jsx-filename-extension': 0,
    'react/prop-types': 0, // using Flow
    'no-param-reassign': 0,
    'react/no-did-mount-set-state': 0,
    'max-len': 0,
    'linebreak-style': 0,
    'react/prefer-stateless-function':0,
    'no-useless-constructor':0,
    'no-unused-vars':0,
    'react/jsx-equals-spacing': 0,
    'arrow-body-style': 0,
    'react/jsx-curly-spacing': 0,
    'padded-blocks': 0,
    'prefer-template': 0,
    'object-curly-spacing': 0,
    'react/self-closing-comp': 0,
    'react/jsx-space-before-closing': 0,
    'arrow-parens': 0,
    'spaced-comment': 0,
    'no-var': 0,
    'vars-on-top': 0,
    'react/sort-comp': 0,
    'quote-props': 0,
    'no-plusplus': 0,
    'dot-notation': 0,
    'space-before-blocks': 0,
    'no-prototype-builtins': 0,
    'prefer-arrow-callback': 0,
    'no-nested-ternary': 0

  },
  settings: {
    'import/resolver': {
      node: {
        paths: [crc.paths.client(), crctimeline.paths.client()]
      }
    }
  }
};
