const config = {
  env: {
    node: process.env.NODE_ENV || 'development'
  }
};

config.globals = {
  'process.env': {
    NODE_ENV: JSON.stringify(config.env.node)
  },
  NODE_ENV: config.env.node,
  __DEV__: config.env.node === 'development',
  __PROD__: config.env.node === 'production',
  __TEST__: config.env.node === 'test',
  __STATS__: process.env.VIEW_STATS === 'true',
  __BASE__: JSON.stringify(process.env.BASENAME || '')
};

config.compiler = {
  extensions: ['.js', '.css', '.scss', '.json', '.png', '.jpg', '.svg'],
  public_path: '/CRC',
  stats: {
    chunks: false,
    chunkModules: false,
    colors: true
  },
  devtool: config.globals.__DEV__
    ? 'cheap-module-eval-source-map'
    : 'source-map',
  quiet: false,
  cssnano: {
    autoprefixer: false,
    discardComments: {
      removeAll: true
    },
    discardUnused: false,
    mergeIdents: false,
    reduceIdents: false,
    safe: true,
    sourcemap: true
  }
};
module.exports = config;
