const config = require('./project.base.config');
const path = require('path');

const PROJECT_NAME = 'CRC';

const base = (...args) => path.resolve(...[config.base].concat(args));

const dir = {
  base: path.resolve(__dirname, `../wwwroot/CMS/${PROJECT_NAME}/`),
  client: 'holiday-2017/src',
  dist: 'holiday-2017/build',
  public: 'holiday-2017/public'
};

const paths = {
  base,
  client: base.bind(null, dir.client),
  dist: base.bind(null, dir.dist),
  public: base.bind(null, dir.public)
};

module.exports = Object.assign(config, dir, {
  paths,
  compiler: {
    public_path: '/crc/holiday-2017/build/'
  }
});
