const config = require('./project.base.config');
const postcss = require('./postcss');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const __PROD__ = config.globals.__PROD__;

const cssBase = {
  test: /\.(sass|scss)$/,
  exclude: /node_modules/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          minimize: false,
          url: false,
          localIdentName: __PROD__ ? '[hash:base64:5]' : '[path][name]__[local]--[hash:base64:5]'
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: postcss,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          includePaths: [],
        },
      },
    ],
  }),
};

exports.scss = (includePaths = []) => ({
  test: /\.(sass|scss)$/,
  exclude: /node_modules/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          minimize: false,
          url: false,
          localIdentName: __PROD__ ? '[hash:base64:5]' : '[path][name]__[local]--[hash:base64:5]'
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: postcss,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          includePaths,
        },
      },
    ],
  }),
});

exports.css = cssBase;

// Load Babel Loader for transpiling ES6 to ES5
exports.js = {
  test: /\.(js|jsx)$/,
  exclude: /node_modules/,
  loader: 'babel-loader',
};

exports.eslint = {
  test: /\.(js|jsx)$/,
  loader: 'eslint-loader',
  options: config.compiler.eslint,
};

exports.image = {
  test: /\.(png|jpg|jpeg|gif|svg)$/,
  loader: 'url-loader?prefix=img/&limit=5000',
};
