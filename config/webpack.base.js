const loaders = require('./loaders');
const config = require('./project.base.config');

module.exports = {
  resolve: {
    modules: ['node_modules', 'wwwroot/CMS', 'wwwroot/CMS/CRC'],
    extensions: config.compiler.extensions
  },
  stats: config.compiler.stats,
  output: {
    filename: '[name].js',
    publicPath: config.compiler.public_path
  },
  module: {
    rules: [loaders.js, loaders.image]
  },
  externals: {}
};
