const webpack = require('webpack');
const config = require('./project.base.config');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const __PROD__ = config.globals.__PROD__;
const __STATS__ = config.globals.__STATS__;

const base = [
  new webpack.DefinePlugin(config.globals),
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
  }),
];

const client = base.concat([
  new BundleAnalyzerPlugin({
    analyzerMode: __STATS__ ? 'static' : 'disabled',
  }),
]);

const dev = [new webpack.NamedModulesPlugin()];

const prod = [
  new UglifyJSPlugin({
    mangle: {
      except: ['$super', '$', 'exports', 'require'],
    },
    compress: {
      warnings: false,
      screw_ie8: true,
      conditionals: true,
      unused: true,
      comparisons: true,
      sequences: true,
      dead_code: true,
      evaluate: true,
      join_vars: true,
      if_return: true,
    },
    output: {
      comments: false,
    },
  }),
  new webpack.optimize.AggressiveMergingPlugin(),
  new CompressionPlugin({
    assets: '[path].gz[query]',
    algorithm: 'gzip',
    test: /\.js$|\.css$|\.html$/,
    threshold: 10240,
    minRatio: 0.8,
  }),
];

exports.client = __PROD__ ? client.concat(prod) : client.concat(dev);
