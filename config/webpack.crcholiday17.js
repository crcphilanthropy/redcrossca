const webpack = require('webpack');
const config = require('./project.crcholiday17.config');
const plugins = require('./plugins');
const loaders = require('./loaders');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: config.paths.client('app'),
  },
  resolve: {
    modules: [config.paths.client()],
  },
  output: {
    path: config.paths.dist(),
    publicPath: config.compiler.public_path,
  },
  module: {
    rules: [
      Object.assign({}, loaders.eslint, { enforce: 'pre' }),
      loaders.scss([
        'node_modules',
        'wwwroot/CMS/crc/bower_components/foundation/scss',
        'wwwroot/CMS/crc/holiday-2017/src/scss',
      ]),
      {
        test: require.resolve('jquery/src/jquery'),
        loader: 'expose-loader?jQuery!expose-loader?$',
      },
      {
        test: require.resolve('fastclick'),
        loader: 'expose-loader?FastClick',
      },
    ],
  },
  plugins: plugins.client.concat([
    new ExtractTextPlugin({
      filename: '[name].css',
      allChunks: true,
    })
  ]),
  // devtool: config.compiler.devtool
};
