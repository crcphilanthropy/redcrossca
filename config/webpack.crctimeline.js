const webpack = require('webpack');
const config = require('./project.crctimeline.config');
const plugins = require('./plugins');
const loaders = require('./loaders');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: config.paths.client('app.js'),
    timeline: config.paths.client('timeline.js'),
    'fallback-page': config.paths.client('fallback-page.js'),
    'timeline/crc.timeline': config.paths.client('crc.timeline.js'),
    'timeline/crc.timeline.component-libs': config.paths.client('crc.timeline.component-libs.js'),
    'timeline/crc.timeline.item-back-btn': config.paths.client('crc.timeline.item-back-btn.js'),
    'timeline/crc.timeline.prev-next-item': config.paths.client('crc.timeline.prev-next-item.js'),
    'site.libs': [
      'jquery/src/jquery',
      'foundation-sites/dist/js/foundation',
      'knockout/build/output/knockout-latest',
      'slick-carousel',
      'velocity-animate',
      'jquery-validation',
      'site/libs/fixto.min',
      'site/libs/jquery.fitvids',
    ],
  },
  resolve: {
    modules: [config.paths.client()],
    alias: {
      jquery: 'jquery/src/jquery',
      jQuery: 'jquery/src/jquery',
      $: 'jquery/src/jquery',
      knockout: 'knockout/build/output/knockout-latest',
    },
  },
  output: {
    chunkFilename: 'crc.timeline.[name].js',
    path: config.paths.dist(),
    publicPath: config.compiler.public_path,
  },
  module: {
    rules: [
      Object.assign({}, loaders.eslint, { enforce: 'pre' }),
      loaders.scss(['node_modules', 'wwwroot/CMS/crctimeline/UI/_src/scss']),
      {
        test: require.resolve('jquery/src/jquery'),
        loader: 'expose-loader?jQuery!expose-loader?$',
      },
    ],
  },
  plugins: plugins.client.concat([
    new ExtractTextPlugin({
      filename: '../css/[name].css',
      allChunks: true,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['site.libs'],
      filename: '[name].js',
      chunks: [
        'app',
        'timeline/crc.timeline',
        'timeline/crc.timeline.component-libs',
        'timeline/crc.timeline.item-back-btn',
        'timeline/crc.timeline.prev-next-item',
      ],
      // minChunks(module, count) {
      //   const context = module.context;
      //   return (context && context.indexOf('node_modules') >= 0) || count >= 2;
      // },
    }),
  ]),
  externals: {
    foundation: 'Foundation',
  },
  devtool: false, // config.compiler.devtool
};
