// const webpack = require('webpack');
const config = require('./project.crcblog.config');
const plugins = require('./plugins');
const loaders = require('./loaders');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    'red-cross-blog': config.paths.client('app'),
    web: config.paths.client('web'),
    print: config.paths.client('print'),
    // vendor: [
    //   'jquery/src/jquery',
    //   // 'foundation-sites/dist/js/plugins/foundation.core',
    //   // 'foundation-sites/dist/js/plugins/foundation.util.mediaQuery',
    //   // 'foundation-sites/dist/js/plugins/foundation.util.imageLoader',
    //   // 'foundation-sites/dist/js/plugins/foundation.equalizer',
    //   // 'fastclick',
    //   // 'legacy/libs/jquery.fitvids.js',
    //   // 'legacy/libs/jquery-accessibleMegaMenu.js',
    //   // 'legacy/libs/slick/slick.js',
    //   // 'Ecentricarts/Scripts/src/eca/eca.js',
    //   // 'Ecentricarts/Scripts/src/eca/eca.util.js',
    //   // 'Ecentricarts/Scripts/src/eca/eca.http.js',
    //   // 'Ecentricarts/Scripts/src/eca/kentico/eca.kentico.mobilemenu.js',
    //   // 'Ecentricarts/Scripts/src/eca/eca.template.js',
    //   // 'Ecentricarts/Scripts/e2e/mobilemenu.js',
    // ],
  },
  resolve: {
    modules: [config.paths.client()],
  },
  output: {
    path: config.paths.dist(),
    publicPath: config.compiler.public_path,
  },
  module: {
    rules: [
      Object.assign({}, loaders.eslint, { enforce: 'pre' }),
      loaders.scss(['node_modules', 'wwwroot/CMS/crc/blog/sass']),
      {
        test: require.resolve('jquery/src/jquery'),
        loader: 'expose-loader?jQuery!expose-loader?$',
      },
      {
        test: require.resolve('fastclick'),
        loader: 'expose-loader?FastClick',
      },
    ],
  },
  plugins: plugins.client.concat([
    new ExtractTextPlugin({
      filename: '../css/[name].css',
      allChunks: true,
    }),
  ]),
  externals: {
    jquery: 'jQuery',
    Modernizr: 'Modernizr',
  },
};
