using System;
using System.Data;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.MediaLibrary;
using CMS.Search;
using CMS.Helpers;


/// <summary>
/// Class used by scheduler to execute the task.
/// </summary>  
public class MediaSearchTaskExecutor : ITask
{
    /// <summary>
    /// Processes search tasks (starts indexer).
    /// </summary>
    /// <param name="task">Task to start</param>
    public string Execute(TaskInfo task)
    {
        try
        {
            // Get all search tasks with ready status
            const string @where = "SearchTaskStatus = N'" + MediaSearchHelper.MYMEDIAINDEX_STATUS_READY + "'";

            // Load top X tasks from database ordered by priority
            var ds = SearchTaskInfoProvider.GetSearchTasks(null, where, "SearchTaskPriority DESC, SearchTaskID", MediaSearchHelper.TASK_INDEXER_RETRIEVE_ROWS_COUNT);
            var sii = SearchIndexInfoProvider.GetSearchIndexInfo(MediaSearchHelper.MYMEDIASEARCH_NAME);

            if (sii != null)
            {
                // If dataset empty nothing to do
                while (!DataHelper.DataSourceIsEmpty(ds))
                {
                    // Go through all search tasks
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        // Create task info obejct
                        var sti = new SearchTaskInfo(dr);
                        var delete = false;

                        switch (sti.GetStringValue("SearchTaskType", ""))
                        {
                            case MediaSearchHelper.MYMEDIAINDEX_TASK_TYPE_UPDATE:
                                var fileId = ValidationHelper.GetInteger(sti.SearchTaskValue, 0);
                                if (fileId > 0)
                                {
                                    try
                                    {
                                        // Get media file info
                                        var mfi = MediaFileInfoProvider.GetMediaFileInfo(fileId);
                                        // Update media file info in index file
                                        MediaSearchHelper.UpdateIndexItem(mfi, sii);

                                        // Field was updated successfully - detete it
                                        delete = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        EventLogProvider.LogException("MediaSearchTaskExecutor", "Execute", ex);
                                    }
                                }
                                break;
                        }

                        if (delete)
                        {
                            // Delete processed task
                            SearchTaskInfoProvider.DeleteSearchTaskInfo(sti);
                        }
                        else
                        {
                            // Set status of task to process
                            sti.SetValue("SearchTaskStatus", MediaSearchHelper.MYMEDIAINDEX_STATUS_PROCESS);
                            SearchTaskInfoProvider.SetSearchTaskInfo(sti);
                        }
                    }

                    ds.Dispose();
                    // Retrieve new dataset with tasks
                    ds = SearchTaskInfoProvider.GetSearchTasks(null, where, "SearchTaskPriority DESC, SearchTaskID", MediaSearchHelper.TASK_INDEXER_RETRIEVE_ROWS_COUNT);
                }
                return string.Empty;
            }
            return "MyMediaIndex not found!";
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }
}
