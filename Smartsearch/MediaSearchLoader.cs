using CMS.Base;

/// <summary>
/// Sample class loader module class. Partial class ensures correct registration.
/// </summary>
[MediaSearchLoader]
public partial class CMSModuleLoader
{
    /// <summary>
    /// Module registration
    /// </summary>
    private class MediaSearchLoaderAttribute : CMSLoaderAttribute
    {
        /// <summary>
        /// Initializes the module
        /// </summary>
        public override void Init()
        {
            // -- This line provides the ability to register the classes via web.config cms.extensibility section from App_Code
            ClassHelper.OnGetCustomClass += ClassHelper_OnGetCustomClass;
        }


        /// <summary>
        /// Gets the custom class object based on the given parameters
        /// </summary>
        private void ClassHelper_OnGetCustomClass(object sender, ClassEventArgs e)
        {
            if (e.Object == null)
            {
                // Provide your custom classes
                switch (e.ClassName)
                {
                    // Class MyMediaSearchIndex implementing ICustomSearchIndex
                    case "MediaSearchIndex":
                        e.Object = new MediaSearchIndex();
                        break;

                    // Class MyMediaSearchAnalyzer implementing Analyzer
                    case "MediaSearchAnalyzer":
                        e.Object = new MediaSearchAnalyzer();
                        break;

                    // Class MyMediaSearchAnalyzer implementing ITask
                    case "MediaSearchTaskExecutor":
                        e.Object = new MediaSearchTaskExecutor();
                        break;
                }
            }
        }
    }
}
