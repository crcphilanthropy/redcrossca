using System.IO;
using CMS.DataEngine;
using CMS.Search;
using Lucene.Net.Analysis;

/// <summary>
/// My media search analyzer.
/// </summary>
public class MediaSearchAnalyzer : Analyzer, ISearchAnalyzer
{
    /// <summary>
    /// Token stream.
    /// </summary>
    /// <param name="fieldName">Field name</param>
    /// <param name="reader">Text reader</param>
    public override TokenStream TokenStream(string fieldName, TextReader reader)
    {
        // Do not split word in search mode
        if (fieldName == SearchFieldsConstants.CONTENT)
        {
            // Get original text
            string text = reader.ReadToEnd();
            // Create new TextReader instance
            var sr = new StringReader(text);
            reader = sr;
        }

        // Use media tokenizer
        TokenStream result = new MediaTokenizer(reader);
        // Use lowercase filter and return token stream
        return new LowerCaseFilter(result);
    }


    /// <summary>
    /// Construct a new MyMediaSearchAnalyzer.
    /// </summary>
    public MediaSearchAnalyzer()
    {
    }
}

