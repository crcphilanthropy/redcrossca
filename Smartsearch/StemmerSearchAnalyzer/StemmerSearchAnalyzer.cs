using System;
using System.IO;

using Lucene.Net.Analysis;

namespace CMS.CustomSearchAnalyzer
{
    /// <summary>
    /// Custom search analyzer.
    /// </summary>
    public class StemmerSearchAnalyzer : Analyzer
    {
        /// <summary>
        /// Token stream.
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="reader">Text reader</param>
        public override TokenStream TokenStream(string fieldName, TextReader reader)
        {
            return new StemmerTokenizer(reader);
        }


        /// <summary>
        /// Construct a new CustomSearchAnalyzer.
        /// </summary>
        public StemmerSearchAnalyzer()
        {
        }
    }
}