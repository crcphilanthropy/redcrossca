using System;
using Lucene.Net.Analysis;

/// <summary>
/// A media library is a tokenizer that divides text at space.
/// </summary>
public class MediaTokenizer : Tokenizer
{
    #region "Variables"

    private int _offset = 0;
    private int _bufferIndex = 0;
    private int _dataLen = 0;
    private const int MAX_WORD_LEN = 255;
    private const int IO_BUFFER_SIZE = 1024;
    private readonly char[] _buffer = new char[MAX_WORD_LEN];
    private readonly char[] _ioBuffer = new char[IO_BUFFER_SIZE];

    #endregion


    #region "Methods"

    /// <summary>
    /// Construct a new MyMediaTokenizer.
    /// </summary>
    public MediaTokenizer(System.IO.TextReader inRenamed)
        : base(inRenamed)
    {
    }


    /// <summary>
    /// Returns the next token in the stream, or null at EOS.
    /// </summary>
    private Token Next()
    {
        // Initialize length & offset
        var length = 0;
        var start = _offset;

        // Loop until break
        while (true)
        {
            // Current character

            // Increase offset
            _offset++;
            // Read characterst to the buffer
            if (_bufferIndex >= _dataLen)
            {
                _dataLen = input.Read(_ioBuffer, 0, _ioBuffer.Length);
                _bufferIndex = 0;
            }

            // Check whether at least one character is in buffer
            if (_dataLen <= 0)
            {
                if (length > 0)
                {
                    break;
                }
                return null;
            }

            var c = _ioBuffer[_bufferIndex++];

            // Check whether char is not space
            if (!c.Equals(' '))
            {
                // if it's a token char
                if (length == 0)
                {
                    // start of token
                    start = _offset - 1;
                }

                // buffer it
                _buffer[length++] = c;

                // buffer overflow
                if (length == MAX_WORD_LEN)
                    break;
            }
            else if (length > 0)
            {
                // at non-Letter w/ chars
                break; // return 'em
            }
        }

        // Return new token
        return new Token(new String(_buffer, 0, length), start, start + length);
    }

    #endregion

    public override bool IncrementToken()
    {
        Next();
        return true;
    }
}

