using System;
using CMS.MediaLibrary;
using CMS.Base;
using CMS.DataEngine;
using CMS.Search;

//using org.pdfbox.pdmodel;
//using org.pdfbox.util;

/// <summary>
/// Sample MyMediaFileInfoHandler class.
/// </summary>
[MyMediaFileInfoHandler]
public partial class CMSModuleLoader
{
    #region "Macro methods loader attribute"

    /// <summary>
    /// Module registration
    /// </summary>
    private class MyMediaFileInfoHandler : CMSLoaderAttribute
    {
        /// <summary>
        /// Initializes the module
        /// </summary>
        public override void Init()
        {
            // Insert After handler
            MediaFileInfo.TYPEINFO.Events.Insert.After += Update_Index;
            // Update After handler
            MediaFileInfo.TYPEINFO.Events.Update.After += Update_Index;
            // Delete After handler
            MediaFileInfo.TYPEINFO.Events.Delete.After += Delete_After;
        }


        /// <summary>
        /// After Update MediaFileInfo event handler
        /// </summary>
        void Delete_After(object sender, ObjectEventArgs e)
        {
            // Update search index for media library info if smart search is enabled and custom search index exists 
            if (SearchIndexInfoProvider.SearchEnabled && SearchIndexInfoProvider.SearchTypeEnabled(SearchHelper.CUSTOM_SEARCH_INDEX))
            {
                // Get media file info
                var mfi = e.Object as MediaFileInfo;

                var searchTaskCreationParameters = new SearchTaskCreationParameters
                    {
                        ObjectField = MediaSearchHelper.CUSTOM_ID_FIELD,
                        ObjectType = SearchHelper.CUSTOM_SEARCH_INDEX,
                        RelatedObjectID = mfi.FileID,
                        TaskType = SearchTaskTypeEnum.Delete,
                        TaskValue = mfi.FileID.ToString()
                    };
                // Create delete task
                //SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Delete, SearchHelper.CUSTOM_SEARCH_INDEX, MediaSearchHelper.CUSTOM_ID_FIELD, mfi.FileID.ToString());
                SearchTaskInfoProvider.CreateTask(searchTaskCreationParameters);
            }
        }


        /// <summary>
        /// Update index MediaFileInfo event handler
        /// </summary>
        void Update_Index(object sender, ObjectEventArgs e)
        {
            // Update search index for media library info if smart search is enabled and custom search index exists 
            if (SearchIndexInfoProvider.SearchEnabled && SearchIndexInfoProvider.SearchTypeEnabled(SearchHelper.CUSTOM_SEARCH_INDEX))
            {
                // Get media file info
                var mfi = e.Object as MediaFileInfo;

                // Create new UPDATE search task
                var sti = new SearchTaskInfo();
                sti.SetValue("SearchTaskType", MediaSearchHelper.MYMEDIAINDEX_TASK_TYPE_UPDATE);
                sti.SetValue("SearchTaskStatus", MediaSearchHelper.MYMEDIAINDEX_STATUS_READY);
                sti.SearchTaskObjectType = SearchHelper.CUSTOM_SEARCH_INDEX;
                sti.SearchTaskField = MediaSearchHelper.CUSTOM_ID_FIELD;
                if (mfi != null) sti.SearchTaskValue = mfi.FileID.ToString();
                // If rebuild then 1 (higher priority)
                sti.SearchTaskPriority = 0;
                // Set creation time
                sti.SetValue("SearchTaskCreated", DateTime.Now);

                // Insert task into database
                sti.Generalized.InsertData();
            }
        }
    }

    #endregion
}
