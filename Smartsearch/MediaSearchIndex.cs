using System;
using System.Linq;
using System.Data;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.MediaLibrary;
using CMS.Search;
using CMS.Helpers;
using CMS.Base;

//using org.pdfbox.pdmodel;
//using org.pdfbox.util;

/// <summary>
/// Sample of custom smart search index. 
/// In this example media library files from specified (if not all files from all media libraries are indexed) media library are indexed.
/// </summary>
public class MediaSearchIndex : ICustomSearchIndex
{
    #region ICustomSearchIndex Members

    /// <summary>
    /// Implementation of rebuild method.
    /// </summary>
    /// <param name="srchInfo">Search index info</param>
    public void Rebuild(SearchIndexInfo srchInfo)
    {
        // Check whether index info object is defined
        if (srchInfo != null)
        {
            // Get index writer for current index
            var iw = srchInfo.Provider.GetWriter(true);

            // Check whether writer is defined
            if (iw != null)
            {
                try
                {
                    //Check whether exist index settings
                    if ((srchInfo.IndexSettings != null))
                    {
                        // Get settings info
                        var sisi = srchInfo.IndexSettings.Items[SearchHelper.CUSTOM_INDEX_DATA];
                        if (sisi != null)
                        {
                            var dci = DataClassInfoProvider.GetDataClassInfo(PredefinedObjectType.MEDIAFILE);
                            if (dci != null)
                            {
                                // Get primary key
                                var primaryKey = DataClassFactory.NewDataClass(dci.ClassName).IDColumn;

                                if (!String.IsNullOrEmpty(primaryKey))
                                {
                                    // Get custom data from settings info
                                    var whereCondition = Convert.ToString(sisi.GetValue("CustomData"));

                                    var queryName = dci.ClassName + ".selectall";

                                    var ds = ConnectionHelper.ExecuteQuery(queryName, null, whereCondition, "", -1);

                                    if (!DataHelper.DataSourceIsEmpty(ds))
                                    {
                                        var errorCount = 0;
                                        foreach (var mfi in from DataRow dr in ds.Tables[0].Rows select new MediaFileInfo(dr))
                                        {
                                            // Create document from MediaFileInfo
                                            switch (mfi.FileExtension)
                                            {
                                                case ".pdf":
                                                case ".doc":
                                                case ".docx":
                                                case ".xls":
                                                case ".xlsx":
                                                case ".ppt":
                                                case ".pptx":
                                                case ".txt":
                                                    try
                                                    {
                                                        var doc = MediaSearchHelper.CreateMediaDoc(mfi, srchInfo);
                                                        if (doc == null) continue;

                                                        //Add meta data to custom table
                                                        MediaSearchHelper.SaveDocMetaData(doc, mfi);

                                                        iw.AddDocument(doc);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        if (errorCount >= 10)
                                                            throw;

                                                        errorCount++;
                                                        EventLogProvider.LogException("Smart search", "rebuild " + mfi.FileName, ex);
                                                    }
                                                    break;
                                            }
                                        }
                                    }

                                }
                            }

                            // Optimize index
                            srchInfo.IndexStatus = IndexStatusEnum.OPTIMIZING;

                            if (SystemContext.IsRunningOnAzure)
                            {
                                SearchIndexInfoProvider.SetSearchIndexInfo(srchInfo);
                            }

                            iw.Optimize();
                            iw.Flush();
                        }
                    }
                }

                catch (Exception ex)
                {
                    EventLogProvider.LogException("Smart search", "rebuild", ex);

                    // Set statuses to error
                    srchInfo.IndexStatus = IndexStatusEnum.ERROR;

                    if (SystemContext.IsRunningOnAzure)
                    {
                        SearchIndexInfoProvider.SetSearchIndexInfo(srchInfo);
                    }
                }
                finally
                {
                    // Close index
                    iw.Close();
                }
            }
        }
    }

    #endregion
}
