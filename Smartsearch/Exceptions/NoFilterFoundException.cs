﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSI.Exceptions
{
    public class NoFilterFoundException : Exception
    {
        public NoFilterFoundException(string fileName)
            : base(string.Format("no filter defined for: {0}", fileName))
        {

        }
    }
}
