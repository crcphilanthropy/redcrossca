using System;
using System.Text;
using CMS.Base;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.MediaLibrary;
using EPocalipse.IFilter;
using System.IO;
using CMS.Search;
using CMS.Helpers;
using CMS.CustomTables;
using MSI.Exceptions;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

public class MediaSearchHelper
{
    #region "Constants"

    /// <summary>
    /// Code name for my custom media index.
    /// </summary>
    public const string MYMEDIASEARCH_NAME = "MyMediaIndex";

    /// <summary>
    ///  Name of custom id field in iDocument. This is necessary for delete index item operation.
    /// </summary>
    public const string CUSTOM_ID_FIELD = "_myid";


    public const string CUSTOM_FILETYPE = "_myfiletype";

    /// <summary>
    ///  Name of custom update status.
    /// </summary>
    public const string MYMEDIAINDEX_TASK_TYPE_UPDATE = "MyMediaIndex_UPDATE";


    /// <summary>
    ///  Custom ready task status.
    /// </summary>
    public const string MYMEDIAINDEX_STATUS_READY = "MyMediaIndex_READY";


    /// <summary>
    ///  Custom process task status.
    /// </summary>
    public const string MYMEDIAINDEX_STATUS_PROCESS = "MyMediaIndex_PROCESS";


    /// <summary>
    /// Taskindexer will retrieve search tasks from database by count of 10.
    /// </summary>
    public const int TASK_INDEXER_RETRIEVE_ROWS_COUNT = 10;

    /// <summary>
    /// The name of the custom table that holds the media file meta data
    /// </summary>
    public const string CUSTOM_TABLE_MEDIA_META = "crc.media_meta";

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Implementation of get content method.
    /// </summary>
    /// <param name="mfi">MediaFileInfo</param>
    public static string GetContent(MediaFileInfo mfi)
    {
        if (mfi != null)
        {
            // Get file location
            var path = MediaFileInfoProvider.GetMediaFilePath(mfi.FileLibraryID, mfi.FilePath);

            try
            {
                switch (mfi.FileExtension.ToLower())
                {
                    case ".doc":
                    case ".docx":
                    case ".xls":
                    case ".xlsx":
                    case ".ppt":
                    case ".pptx":
                        //case ".pdf":
                        return SearchCrawler.HtmlToPlainText(ReadKnownFile(path));

                    case ".pdf":
                        return SearchCrawler.HtmlToPlainText(ReadPdfFile(path));

                    case ".txt":
                        var fi = CMS.IO.FileInfo.New(path);
                        return SearchCrawler.HtmlToPlainText(fi.OpenText().ReadToEnd());
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("Smart search", "UpdateContentField", ex);
            }
        }
        return string.Empty;
    }


    public static string ReadPdfFile(string filePath)
    {
        var text = new StringBuilder();
        try
        {
            if (File.Exists(filePath))
            {
                using (var pdfReader = new PdfReader(filePath))
                {
                    for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                    {
                        var strategy = new SimpleTextExtractionStrategy();
                        var currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                        currentText = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                        text.Append(currentText);
                    }
                    pdfReader.Close();
                }
            }
        }
        catch (IndexOutOfRangeException ex) { }
        return text.ToString();
    }

    /// <summary>
    /// Implementation of Update index item method.
    /// </summary>
    /// <param name="mfi">MediaFileInfo</param>
    /// <param name="sii">SearchIndexInfo</param>
    public static void UpdateIndexItem(MediaFileInfo mfi, SearchIndexInfo sii)
    {
        if (mfi != null && sii != null)
        {
            // Get Document object 
            var iDoc = CreateMediaDoc(mfi, sii);

            // Update custom smart search index
            SearchHelper.Update(iDoc, sii);
        }
    }


    /// <summary>
    /// Implementation of GetMediaFileUrl method.
    /// </summary>
    /// <param name="mfi">MediaFileInfo</param>
    public static string GetMediaFileUrl(MediaFileInfo mfi)
    {
        // Get Site info
        var si = SiteInfoProvider.GetSiteInfo(mfi.FileSiteID);
        // Get media library info

        var lfi = MediaLibraryInfoProvider.GetMediaLibraryInfo(mfi.FileLibraryID);

        if (lfi != null && si != null)
        {
            return MediaFileURLProvider.GetMediaFileUrl(mfi, si.SiteName, lfi.LibraryFolder);
        }

        return MediaFileInfoProvider.GetMediaFileUrl(mfi.FileGUID, mfi.FileName);
    }


    /// <summary>
    /// Implementation of create document method.
    /// </summary>
    /// <param name="mfi">MediaFileInfo</param>
    /// <param name="searchIndexInfo"></param>
    public static ISearchDocument CreateMediaDoc(MediaFileInfo mfi, SearchIndexInfo searchIndexInfo)
    {
        // Get conetnt of the media file
        var fileContent = GetContent(mfi);

        if (string.IsNullOrWhiteSpace(fileContent)) return null;

        fileContent = TextHelper.RemoveDiacritics(fileContent);

        var sdparams = new SearchDocumentParameters
            {
                Index = searchIndexInfo,
                Type = SearchHelper.CUSTOM_SEARCH_INDEX,
                Id = mfi.FileID.ToString(),
                Created = mfi.FileCreatedWhen
            };

        // Creates a new Lucene.Net search document for the current text file.
        var doc = SearchHelper.CreateDocument(sdparams);


        // Adds a content field. This field is processed when the search looks for matching results.
        doc.AddGeneralField(SearchFieldsConstants.CONTENT, fileContent, SearchHelper.StoreContentField, true);

        // Adds a title field. The value of this field is used for the search result title.
        doc.AddGeneralField(SearchFieldsConstants.CUSTOM_TITLE, string.IsNullOrWhiteSpace(mfi.FileTitle) ? mfi.FileName : mfi.FileTitle, true, false);

        // Adds a content field. The value of this field is used for the search result excerpt.
        doc.AddGeneralField(SearchFieldsConstants.CUSTOM_CONTENT, string.IsNullOrWhiteSpace(mfi.FileDescription) ? TextHelper.LimitLength(fileContent, 200) : TextHelper.LimitLength(mfi.FileDescription, 200) , true, false);

        // Adds a date field. The value of this field is used for the date in the search results.
        doc.AddGeneralField(SearchFieldsConstants.CUSTOM_DATE, mfi.FileCreatedWhen, true, false);

        // Adds a url field. The value of this field is used for link urls in the search results.
        doc.AddGeneralField(SearchFieldsConstants.CUSTOM_URL, GetMediaFileUrl(mfi), true, false);

        doc.AddGeneralField("_myfiletype", mfi.FileExtension.Replace(".", string.Empty).ToLower(), true, true);


        return doc;
    }

    /// <summary>
    /// Save meta data to the custom table. The meta data is needed since not all information can be retrieved from the index,
    /// such as file content in original case.
    /// </summary>
    public static void SaveDocMetaData(ISearchDocument doc, MediaFileInfo mfi)
    {
        try
        {

            var customTable = DataClassInfoProvider.GetDataClassInfo(CUSTOM_TABLE_MEDIA_META);

            if (customTable != null)
            {
                // Search for item to see if it exists                
                var where = string.Format("CustomID = '{0}'", doc.Get(SearchFieldsConstants.ID));
                const int topN = 1;
                const string columns = "*";

                var dataSet = CustomTableItemProvider.GetItems(CUSTOM_TABLE_MEDIA_META, where, null, topN, columns);

                if (DataHelper.DataSourceIsEmpty(dataSet)) // insert
                {
                    // Creates new custom table item
                    var newCustomTableItem = CustomTableItem.New(CUSTOM_TABLE_MEDIA_META);

                    // Custom ID
                    newCustomTableItem.SetValue("CustomID", doc.Get(SearchFieldsConstants.ID));
                    // Title
                    newCustomTableItem.SetValue("Title", string.IsNullOrEmpty(mfi.FileTitle) ? mfi.FileName : mfi.FileTitle);
                    // Content
                    newCustomTableItem.SetValue("Content",
                                                string.IsNullOrEmpty(mfi.FileDescription)
                                                    ? TextHelper.LimitLength(GetContent(mfi), 200)
                                                    : TextHelper.LimitLength(mfi.FileDescription, 200));

                    // Inserts the custom table item into database
                    newCustomTableItem.Insert();
                }
                else // update
                {
                    var itemID = ValidationHelper.GetInteger(dataSet.Tables[0].Rows[0][0], 0);
                    var updateCustomTableItem = CustomTableItemProvider.GetItem(itemID, CUSTOM_TABLE_MEDIA_META);
                    if (updateCustomTableItem != null)
                    {
                        updateCustomTableItem.SetValue("CustomID", doc.Get(SearchFieldsConstants.ID));
                        updateCustomTableItem.SetValue("Title", string.IsNullOrEmpty(mfi.FileTitle) ? mfi.FileName : mfi.FileTitle);
                        updateCustomTableItem.SetValue("Content",
                                                       string.IsNullOrEmpty(mfi.FileDescription)
                                                           ? TextHelper.LimitLength(GetContent(mfi), 200)
                                                           : TextHelper.LimitLength(mfi.FileDescription, 200));

                        updateCustomTableItem.Update();
                    }
                }
            }
            else
            {
                throw new InvalidDataException(string.Format("Custom table {0} does not exist", CUSTOM_TABLE_MEDIA_META));
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Unexpected error on saving document meta data to custom table", ex);
        }
    }


    /// <summary>
    /// Extract the text content of a MS Office document
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string ReadKnownFile(string fileName)
    {
        var sb = new StringBuilder();

        if (File.Exists(fileName))
        {
            try
            {
                TextReader reader = new FilterReader(fileName);

                sb.Append(reader.ReadToEnd());

                if (sb.Length == 0)
                {
                    EventLogProvider.LogEvent("I", "ReadKnownFile", "Smart search index",
                                              string.Format("Media file {0} has no content", Path.GetFileName(fileName)));
                }
            }
            catch (NoFilterFoundException ex)
            {
                EventLogProvider.LogWarning("Smart search: No FIlter Found", "ReadKnownFile", ex, SiteContext.CurrentSiteID, string.Empty);
                sb.Clear();
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("Smart search", "ReadKnownFile", ex);
                sb.Clear();
            }
        }
        else
        {
            EventLogProvider.LogEvent("I", "ReadKnownFile", "Smart search index", string.Format("Media file {0} does not exist", Path.GetFileName(fileName)));
        }
        return sb.ToString();
    }

    /// <summary>
    /// Delete meta data records that do not exist in the media library anymore
    /// </summary>
    public static void PurgeOldMetaData()
    {

    }


    #endregion
}